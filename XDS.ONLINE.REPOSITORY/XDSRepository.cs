﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;


namespace XDS.ONLINE.REPOSITORY
{
    public class XDSRepository : XDS.ONLINE.REPOSITORY.IXDSRepository
    {
        private XDSReportServiceRef.XDSConnectWSSoapClient _XDSConnectWS ;
       
        public XDSRepository(){
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            _XDSConnectWS = new XDSReportServiceRef.XDSConnectWSSoapClient();
        }

        ~XDSRepository()
        {
            //TODO: Error Logging
            try
            {
                _XDSConnectWS.Close();
                _XDSConnectWS = null;
            }
            catch (CommunicationException e)
            {
                logerror(e);
                _XDSConnectWS.Abort();
            }
            catch (TimeoutException e)
            {
                logerror(e);
                _XDSConnectWS.Abort();
            }
            catch (Exception e)
            {
                logerror(e);
                _XDSConnectWS.Abort();
                throw;
            }
   
        }

        private void logerror(Exception e)
        {  
            //TODO: Error Logging
        }

        public string ConnectGetCreditAssesment(
                    string ConnectTicket,
                    string EnquiryReason,
                    int ProductId,
                    string IdNumber,
                    string PassportNo,
                    string FirstName,
                    string Surname,
                    string BirthDate,
                    string MaritalStatus,
                    string SpouseFirstName,
                    string SpouseSurname,
                    string ResidentialAddressLine1,
                    string ResidentialAddressLine2,
                    string ResidentialAddressLine3,
                    string ResidentialAddressLine4,
                    string ResidentialPostalCode,
                    string PostalAddressLine1,
                    string PostalAddressLine2,
                    string PostalAddressLine3,
                    string PostalAddressLine4,
                    string PostalPostalCode,
                    string HomeTelCode,
                    string HomeTelNo,
                    string WorkTelCode,
                    string WorkTelNo,
                    string CellularCode,
                    string CellularNo,
                    string EmailAddress,
                    string TotalNetMonthlyIncome,
                    string Employer,
                    string JobTitle,
                    string YourReference,
                    string VoucherCode)
        {
            return _XDSConnectWS.ConnectGetCreditAssesment(ConnectTicket, EnquiryReason, ProductId, IdNumber, PassportNo, FirstName, Surname, BirthDate, MaritalStatus, SpouseFirstName, SpouseSurname, ResidentialAddressLine1, ResidentialAddressLine2, ResidentialAddressLine3, ResidentialAddressLine4, ResidentialPostalCode, PostalAddressLine1, PostalAddressLine2, PostalAddressLine3, PostalAddressLine4, PostalPostalCode, HomeTelCode, HomeTelNo, WorkTelCode, WorkTelNo, CellularCode, CellularNo, EmailAddress, TotalNetMonthlyIncome, Employer, JobTitle, YourReference, VoucherCode);
        }

        public System.Threading.Tasks.Task<string> ConnectGetCreditAssesmentAsync(
                    string ConnectTicket,
                    string EnquiryReason,
                    int ProductId,
                    string IdNumber,
                    string PassportNo,
                    string FirstName,
                    string Surname,
                    string BirthDate,
                    string MaritalStatus,
                    string SpouseFirstName,
                    string SpouseSurname,
                    string ResidentialAddressLine1,
                    string ResidentialAddressLine2,
                    string ResidentialAddressLine3,
                    string ResidentialAddressLine4,
                    string ResidentialPostalCode,
                    string PostalAddressLine1,
                    string PostalAddressLine2,
                    string PostalAddressLine3,
                    string PostalAddressLine4,
                    string PostalPostalCode,
                    string HomeTelCode,
                    string HomeTelNo,
                    string WorkTelCode,
                    string WorkTelNo,
                    string CellularCode,
                    string CellularNo,
                    string EmailAddress,
                    string TotalNetMonthlyIncome,
                    string Employer,
                    string JobTitle,
                    string YourReference,
                    string VoucherCode)
        {
            return _XDSConnectWS.ConnectGetCreditAssesmentAsync(ConnectTicket, EnquiryReason, ProductId, IdNumber, PassportNo, FirstName, Surname, BirthDate, MaritalStatus, SpouseFirstName, SpouseSurname, ResidentialAddressLine1, ResidentialAddressLine2, ResidentialAddressLine3, ResidentialAddressLine4, ResidentialPostalCode, PostalAddressLine1, PostalAddressLine2, PostalAddressLine3, PostalAddressLine4, PostalPostalCode, HomeTelCode, HomeTelNo, WorkTelCode, WorkTelNo, CellularCode, CellularNo, EmailAddress, TotalNetMonthlyIncome, Employer, JobTitle, YourReference, VoucherCode);
        }

        public string ConnectNLRLoanClosures(
                    string ConnectTicket,
                    int ProductID,
                    string SupplierRefNo,
                    string LoanRegNo,
                    string EnquiryRefNo,
                    string IdNumber,
                    string PassportNo,
                    string Birthdate,
                    string Surname,
                    string Forename1,
                    string Forename2,
                    string Forename3,
                    string BranchCode,
                    string Accno,
                    string SubAccno,
                    string DateOfClosure,
                    string StatusCode,
                    string VoucherCode,
                    string ExternalReference)
        {
            return _XDSConnectWS.ConnectNLRLoanClosures(ConnectTicket, ProductID, SupplierRefNo, LoanRegNo, EnquiryRefNo, IdNumber, PassportNo, Birthdate, Surname, Forename1, Forename2, Forename3, BranchCode, Accno, SubAccno, DateOfClosure, StatusCode, VoucherCode, ExternalReference);
        }

        public System.Threading.Tasks.Task<string> ConnectNLRLoanClosuresAsync(
                    string ConnectTicket,
                    int ProductID,
                    string SupplierRefNo,
                    string LoanRegNo,
                    string EnquiryRefNo,
                    string IdNumber,
                    string PassportNo,
                    string Birthdate,
                    string Surname,
                    string Forename1,
                    string Forename2,
                    string Forename3,
                    string BranchCode,
                    string Accno,
                    string SubAccno,
                    string DateOfClosure,
                    string StatusCode,
                    string VoucherCode,
                    string ExternalReference)
        {
            return _XDSConnectWS.ConnectNLRLoanClosuresAsync(ConnectTicket, ProductID, SupplierRefNo, LoanRegNo, EnquiryRefNo, IdNumber, PassportNo, Birthdate, Surname, Forename1, Forename2, Forename3, BranchCode, Accno, SubAccno, DateOfClosure, StatusCode, VoucherCode, ExternalReference);
        }

        public XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationSummary ConnectAuthenticationGetSummary(string ConnectTicket, int EnquiryID, int EnquiryResultID)
        {
            return _XDSConnectWS.ConnectAuthenticationGetSummary(ConnectTicket, EnquiryID, EnquiryResultID);
        }

        public System.Threading.Tasks.Task<XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationSummary> ConnectAuthenticationGetSummaryAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID)
        {
            return _XDSConnectWS.ConnectAuthenticationGetSummaryAsync(ConnectTicket, EnquiryID, EnquiryResultID);
        }

        public XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationProcess ConnectGetAuthenticationQuestions(string ConnectTicket, int EnquiryID, int EnquiryResultID)
        {
            return _XDSConnectWS.ConnectGetAuthenticationQuestions(ConnectTicket, EnquiryID, EnquiryResultID);
        }

        public System.Threading.Tasks.Task<XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationProcess> ConnectGetAuthenticationQuestionsAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID)
        {
            return _XDSConnectWS.ConnectGetAuthenticationQuestionsAsync(ConnectTicket, EnquiryID, EnquiryResultID);
        }

        public XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationProcess ConnectAuthenticationProcess(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationProcessAction ProcesAction, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationProcess MyAuthenticationProcess, string Comment)
        {
            return _XDSConnectWS.ConnectAuthenticationProcess(ConnectTicket, ProcesAction, MyAuthenticationProcess, Comment);
        }

        public System.Threading.Tasks.Task<XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationProcess> ConnectAuthenticationProcessAsync(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationProcessAction ProcesAction, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationProcess MyAuthenticationProcess, string Comment)
        {
            return _XDSConnectWS.ConnectAuthenticationProcessAsync(ConnectTicket, ProcesAction, MyAuthenticationProcess, Comment);
        }

        public string ConnectFraudSavePersonalQuestions(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationProcess MyAuthenticationProcess, string Comment)
        {
            return _XDSConnectWS.ConnectFraudSavePersonalQuestions(ConnectTicket, MyAuthenticationProcess, Comment);
        }

        public System.Threading.Tasks.Task<string> ConnectFraudSavePersonalQuestionsAsync(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationProcess MyAuthenticationProcess, string Comment)
        {
            return _XDSConnectWS.ConnectFraudSavePersonalQuestionsAsync(ConnectTicket, MyAuthenticationProcess, Comment);
        }

        public string ConnectFraudGetBlockedConsumers(string ConnectTicket)
        {
            return _XDSConnectWS.ConnectFraudGetBlockedConsumers(ConnectTicket);
        }

        public System.Threading.Tasks.Task<string> ConnectFraudGetBlockedConsumersAsync(string ConnectTicket)
        {
            return _XDSConnectWS.ConnectFraudGetBlockedConsumersAsync(ConnectTicket);
        }

        public string ConnectFraudUnBlockConsumer(string ConnectTicket, int BlockID, string Comment)
        {
            return _XDSConnectWS.ConnectFraudUnBlockConsumer(ConnectTicket, BlockID, Comment);
        }

        public System.Threading.Tasks.Task<string> ConnectFraudUnBlockConsumerAsync(string ConnectTicket, int BlockID, string Comment)
        {
            return _XDSConnectWS.ConnectFraudUnBlockConsumerAsync(ConnectTicket, BlockID, Comment);
        }

        public string ConnectGetBiometricsResult(string ConnectTicket, string ReferenceNo, string IDNumber, int StatusID, System.DateTime Date)
        {
            return _XDSConnectWS.ConnectGetBiometricsResult(ConnectTicket, ReferenceNo, IDNumber, StatusID, Date);
        }

        public System.Threading.Tasks.Task<string> ConnectGetBiometricsResultAsync(string ConnectTicket, string ReferenceNo, string IDNumber, int StatusID, System.DateTime Date)
        {
            return _XDSConnectWS.ConnectGetBiometricsResultAsync(ConnectTicket, ReferenceNo, IDNumber, StatusID, Date);
        }

        public string ConnectBiometricsGetStatus(string ConnectTicket, string ReferenceNo)
        {
            return _XDSConnectWS.ConnectBiometricsGetStatus(ConnectTicket, ReferenceNo);
        }

        public System.Threading.Tasks.Task<string> ConnectBiometricsGetStatusAsync(string ConnectTicket, string ReferenceNo)
        {
            return _XDSConnectWS.ConnectBiometricsGetStatusAsync(ConnectTicket, ReferenceNo);
        }

        public string ConnectBiometricsSaveTransaction(string ConnectTicket, string ReferenceNo, string IDNumber, int StatusID)
        {
            return _XDSConnectWS.ConnectBiometricsSaveTransaction(ConnectTicket, ReferenceNo, IDNumber, StatusID);
        }

        public System.Threading.Tasks.Task<string> ConnectBiometricsSaveTransactionAsync(string ConnectTicket, string ReferenceNo, string IDNumber, int StatusID)
        {
            return _XDSConnectWS.ConnectBiometricsSaveTransactionAsync(ConnectTicket, ReferenceNo, IDNumber, StatusID);
        }

        public string ConnectBiometricsSMSAlert(string ConnectTicket, string MobileNo, int SubscriberID, int SubscriberAuthenticationID, int SystemUserID, int ProductId)
        {
            return _XDSConnectWS.ConnectBiometricsSMSAlert(ConnectTicket, MobileNo, SubscriberID, SubscriberAuthenticationID, SystemUserID, ProductId);
        }

        public System.Threading.Tasks.Task<string> ConnectBiometricsSMSAlertAsync(string ConnectTicket, string MobileNo, int SubscriberID, int SubscriberAuthenticationID, int SystemUserID, int ProductId)
        {
            return _XDSConnectWS.ConnectBiometricsSMSAlertAsync(ConnectTicket, MobileNo, SubscriberID, SubscriberAuthenticationID, SystemUserID, ProductId);
        }

        public string ConnectCredit4LifeSMSAlert(string ConnectTicket, string MobileNo, string Message, int SubscriberID, int ChangeLogID, int SystemUserID, int ProductId)
        {
            return _XDSConnectWS.ConnectCredit4LifeSMSAlert(ConnectTicket, MobileNo, Message, SubscriberID, ChangeLogID, SystemUserID, ProductId);
        }

        public System.Threading.Tasks.Task<string> ConnectCredit4LifeSMSAlertAsync(string ConnectTicket, string MobileNo, string Message, int SubscriberID, int ChangeLogID, int SystemUserID, int ProductId)
        {
            return _XDSConnectWS.ConnectCredit4LifeSMSAlertAsync(ConnectTicket, MobileNo, Message, SubscriberID, ChangeLogID, SystemUserID, ProductId);
        }

        public string ConnectGetPersonalQuestionAnswers(string ConnectTicket, int SubscriberID, int ConsumerID)
        {
            return _XDSConnectWS.ConnectGetPersonalQuestionAnswers(ConnectTicket, SubscriberID, ConsumerID);
        }

        public System.Threading.Tasks.Task<string> ConnectGetPersonalQuestionAnswersAsync(string ConnectTicket, int SubscriberID, int ConsumerID)
        {
            return _XDSConnectWS.ConnectGetPersonalQuestionAnswersAsync(ConnectTicket, SubscriberID, ConsumerID);
        }

        public string ConnectSavePersonalQuestionAnswers(string ConnectTicket, int SubscriberID, int ConsumerID, string sPersonalQuestions)
        {
            return _XDSConnectWS.ConnectSavePersonalQuestionAnswers(ConnectTicket, SubscriberID, ConsumerID, sPersonalQuestions);
        }

        public System.Threading.Tasks.Task<string> ConnectSavePersonalQuestionAnswersAsync(string ConnectTicket, int SubscriberID, int ConsumerID, string sPersonalQuestions)
        {
            return _XDSConnectWS.ConnectSavePersonalQuestionAnswersAsync(ConnectTicket, SubscriberID, ConsumerID, sPersonalQuestions);
        }

        public string ConnectCustomConsumerMatch(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationSearchType SearchType, string BranchCode, int PurposeID, string IdNumber, string PassportNo, string Surname, string FirstName, string Maidenname, string CellularCode, string CellularNo, string AccountNo, string SubaccountNo, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectCustomConsumerMatch(ConnectTicket, SearchType, BranchCode, PurposeID, IdNumber, PassportNo, Surname, FirstName, Maidenname, CellularCode, CellularNo, AccountNo, SubaccountNo, YourReference, VoucherCode);
        }

        public System.Threading.Tasks.Task<string> ConnectCustomConsumerMatchAsync(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationSearchType SearchType, string BranchCode, int PurposeID, string IdNumber, string PassportNo, string Surname, string FirstName, string Maidenname, string CellularCode, string CellularNo, string AccountNo, string SubaccountNo, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectCustomConsumerMatchAsync(ConnectTicket, SearchType, BranchCode, PurposeID, IdNumber, PassportNo, Surname, FirstName, Maidenname, CellularCode, CellularNo, AccountNo, SubaccountNo, YourReference, VoucherCode);
        }

      
        public byte[] ConnectGetConsumerCreditReportBinary(string ConnectTicket, string IdNumber, string PassportNo)
        {
            XDS.ONLINE.REPOSITORY.XDSReportServiceRef.ConnectGetConsumerCreditReportBinaryRequest inValue = new XDS.ONLINE.REPOSITORY.XDSReportServiceRef.ConnectGetConsumerCreditReportBinaryRequest();
            inValue.ConnectTicket = ConnectTicket;
            inValue.IdNumber = IdNumber;
            inValue.PassportNo = PassportNo;
            XDS.ONLINE.REPOSITORY.XDSReportServiceRef.ConnectGetConsumerCreditReportBinaryResponse retVal = ((XDS.ONLINE.REPOSITORY.XDSReportServiceRef.XDSConnectWSSoap)(this)).ConnectGetConsumerCreditReportBinary(inValue);
            return retVal.ConnectGetConsumerCreditReportBinaryResult;
        }

         public System.Threading.Tasks.Task<XDS.ONLINE.REPOSITORY.XDSReportServiceRef.ConnectGetConsumerCreditReportBinaryResponse> ConnectGetConsumerCreditReportBinaryAsync(string ConnectTicket, string IdNumber, string PassportNo)
        {
            XDS.ONLINE.REPOSITORY.XDSReportServiceRef.ConnectGetConsumerCreditReportBinaryRequest inValue = new XDS.ONLINE.REPOSITORY.XDSReportServiceRef.ConnectGetConsumerCreditReportBinaryRequest();
            inValue.ConnectTicket = ConnectTicket;
            inValue.IdNumber = IdNumber;
            inValue.PassportNo = PassportNo;
            return ((XDS.ONLINE.REPOSITORY.XDSReportServiceRef.XDSConnectWSSoap)(this)).ConnectGetConsumerCreditReportBinaryAsync(inValue);
        }


        public byte[] ConnectGetResultBinary(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML)
        {
            XDS.ONLINE.REPOSITORY.XDSReportServiceRef.ConnectGetResultBinaryRequest inValue = new XDS.ONLINE.REPOSITORY.XDSReportServiceRef.ConnectGetResultBinaryRequest();
            inValue.ConnectTicket = ConnectTicket;
            inValue.EnquiryID = EnquiryID;
            inValue.EnquiryResultID = EnquiryResultID;
            inValue.ProductID = ProductID;
            inValue.BonusXML = BonusXML;
            XDS.ONLINE.REPOSITORY.XDSReportServiceRef.ConnectGetResultBinaryResponse retVal = ((XDS.ONLINE.REPOSITORY.XDSReportServiceRef.XDSConnectWSSoap)(this)).ConnectGetResultBinary(inValue);
            return retVal.ConnectGetResultBinaryResult;
        }

       
        public System.Threading.Tasks.Task<XDS.ONLINE.REPOSITORY.XDSReportServiceRef.ConnectGetResultBinaryResponse> ConnectGetResultBinaryAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML)
        {
            XDS.ONLINE.REPOSITORY.XDSReportServiceRef.ConnectGetResultBinaryRequest inValue = new XDS.ONLINE.REPOSITORY.XDSReportServiceRef.ConnectGetResultBinaryRequest();
            inValue.ConnectTicket = ConnectTicket;
            inValue.EnquiryID = EnquiryID;
            inValue.EnquiryResultID = EnquiryResultID;
            inValue.ProductID = ProductID;
            inValue.BonusXML = BonusXML;
            return ((XDS.ONLINE.REPOSITORY.XDSReportServiceRef.XDSConnectWSSoap)(this)).ConnectGetResultBinaryAsync(inValue);
        }

        public string ConnectUpdatedemographicInfo(
                    string ConnectTicket,
                    int ProductID,
                    int SubscriberEnquiryID,
                    int SubscriberEnquiryResultID,
                    string HomeTelephoneNumber,
                    string WorkTelephoneNumber,
                    string CellphoneNumber,
                    string PhysicalAddress1,
                    string PhysicalAddress2,
                    string PhysicalAddress3,
                    string PhysicalAddress4,
                    string PhysicalPostCode,
                    string PostalAddress1,
                    string PostalAddress2,
                    string PostalAddress3,
                    string PostalAddress4,
                    string PostalPostCode,
                    string EmailAddress,
                    string ExternalReference)
        {
            return _XDSConnectWS.ConnectUpdatedemographicInfo(ConnectTicket, ProductID, SubscriberEnquiryID, SubscriberEnquiryResultID, HomeTelephoneNumber, WorkTelephoneNumber, CellphoneNumber, PhysicalAddress1, PhysicalAddress2, PhysicalAddress3, PhysicalAddress4, PhysicalPostCode, PostalAddress1, PostalAddress2, PostalAddress3, PostalAddress4, PostalPostCode, EmailAddress, ExternalReference);
        }

        public System.Threading.Tasks.Task<string> ConnectUpdatedemographicInfoAsync(
                    string ConnectTicket,
                    int ProductID,
                    int SubscriberEnquiryID,
                    int SubscriberEnquiryResultID,
                    string HomeTelephoneNumber,
                    string WorkTelephoneNumber,
                    string CellphoneNumber,
                    string PhysicalAddress1,
                    string PhysicalAddress2,
                    string PhysicalAddress3,
                    string PhysicalAddress4,
                    string PhysicalPostCode,
                    string PostalAddress1,
                    string PostalAddress2,
                    string PostalAddress3,
                    string PostalAddress4,
                    string PostalPostCode,
                    string EmailAddress,
                    string ExternalReference)
        {
            return _XDSConnectWS.ConnectUpdatedemographicInfoAsync(ConnectTicket, ProductID, SubscriberEnquiryID, SubscriberEnquiryResultID, HomeTelephoneNumber, WorkTelephoneNumber, CellphoneNumber, PhysicalAddress1, PhysicalAddress2, PhysicalAddress3, PhysicalAddress4, PhysicalPostCode, PostalAddress1, PostalAddress2, PostalAddress3, PostalAddress4, PostalPostCode, EmailAddress, ExternalReference);
        }

        public System.Threading.Tasks.Task<string> ConnectUpdateEmploymentInfoAsync(string ConnectTicket, int ProductID, int SubscriberEnquiryID, int SubscriberEnquiryResultID, string EmployerName, string EmployerTelephoneNumber, string EmploymentDate, string ExternalReference)
        {
            return _XDSConnectWS.ConnectUpdateEmploymentInfoAsync(ConnectTicket, ProductID, SubscriberEnquiryID, SubscriberEnquiryResultID, EmployerName, EmployerTelephoneNumber, EmploymentDate, ExternalReference);
        }

        public string ConnectGetCampaignCount(string ConnectTicket, string Command)
        {
            return _XDSConnectWS.ConnectGetCampaignCount(ConnectTicket, Command);
        }

        public System.Threading.Tasks.Task<string> ConnectGetCampaignCountAsync(string ConnectTicket, string Command)
        {
            return _XDSConnectWS.ConnectGetCampaignCountAsync(ConnectTicket, Command);
        }

        public string Login(string strUser, string strPwd)
        {
            return _XDSConnectWS.Login(strUser, strPwd);
        }

        public System.Threading.Tasks.Task<string> LoginAsync(string strUser, string strPwd)
        { 
                return _XDSConnectWS.LoginAsync(strUser, strPwd);
          
        }

        public bool IsTicketValid(string XDSConnectTicket)
        {
            return _XDSConnectWS.IsTicketValid(XDSConnectTicket);
        }

        public System.Threading.Tasks.Task<bool> IsTicketValidAsync(string XDSConnectTicket)
        {
            return _XDSConnectWS.IsTicketValidAsync(XDSConnectTicket);
        }

        public string ConnectConsumerMatch(string ConnectTicket, string EnquiryReason, int ProductId, string IdNumber, string PassportNo, string FirstName, string Surname, string BirthDate, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectConsumerMatch(ConnectTicket, EnquiryReason, ProductId, IdNumber, PassportNo, FirstName, Surname, BirthDate, YourReference, VoucherCode);
        }

        public System.Threading.Tasks.Task<string> ConnectConsumerMatchAsync(string ConnectTicket, string EnquiryReason, int ProductId, string IdNumber, string PassportNo, string FirstName, string Surname, string BirthDate, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectConsumerMatchAsync(ConnectTicket, EnquiryReason, ProductId, IdNumber, PassportNo, FirstName, Surname, BirthDate, YourReference, VoucherCode);
        }

        public string ConnectLinkageDetectorAsync(string ConnectTicket, string EnquiryReason, string YourReference, string VoucherCode, int ProductID, string POIdNumber, string POPassportNumber, string POFirstName, string POSurname, string PODateOfBirth, string ClaimNumber, string TPIdNumber, string TPPassportNumber, string TPFirstName, string TPSurname, string TPDateOfBirth)
        {
            return "<LinkageSearchResult><ListOfConsumers><ConsumerDetails><ConsumerID>1</ConsumerID><FirstName>John</FirstName><SecondName /><ThirdName /><Surname>Doe</Surname><IDNo>7503075082086</IDNo><PassportNo /><BirthDate>1975-03-07T00:00:00+02:00</BirthDate><GenderInd>M</GenderInd><BonusXML /><TempReference /><EnquiryID>222</EnquiryID><EnquiryResultID>333</EnquiryResultID><Reference>C11111111-1111111</Reference></ConsumerDetails><ConsumerDetails><ConsumerID>2</ConsumerID><FirstName>John</FirstName><SecondName /><ThirdName /><Surname>Doe</Surname><IDNo>7503075082086</IDNo><PassportNo /><BirthDate>1975-03-07T00:00:00+02:00</BirthDate><GenderInd>M</GenderInd><BonusXML /><TempReference /><EnquiryID>222</EnquiryID><EnquiryResultID>333</EnquiryResultID><Reference>C11111111-1111111</Reference></ConsumerDetails></ListOfConsumers><ListOfConsumersOther><ConsumerDetails><ConsumerID>3</ConsumerID><FirstName>John</FirstName><SecondName /><ThirdName />	<Surname>Doe</Surname><IDNo>7503075082086</IDNo><PassportNo /><BirthDate>1975-03-07T00:00:00+02:00</BirthDate><GenderInd>M</GenderInd><BonusXML /><TempReference /><EnquiryID>222</EnquiryID><EnquiryResultID>333</EnquiryResultID><Reference>C11111111-1111111</Reference></ConsumerDetails><ConsumerDetails><ConsumerID>4</ConsumerID><FirstName>John</FirstName><SecondName /><ThirdName /><Surname>Doe</Surname><IDNo>7503075082086</IDNo><PassportNo /><BirthDate>1975-03-07T00:00:00+02:00</BirthDate><GenderInd>M</GenderInd><BonusXML /><TempReference /><EnquiryID>222</EnquiryID><EnquiryResultID>333</EnquiryResultID><Reference>C11111111-1111111</Reference></ConsumerDetails></ListOfConsumersOther></LinkageSearchResult>";
        }

        public string GetLinkageDetectorTraceReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML)
        {
            return "<Linkage><ReportInformation><ReportID>2</ReportID><ReportName>Consumer Trace Report</ReportName></ReportInformation><SubscriberInputDetails><EnquiryDate>2015-02-03T11:21:09.0678548+02:00</EnquiryDate><EnquiryType>Consumer Trace</EnquiryType><SubscriberName>Shandon Business Solutions</SubscriberName><SubscriberUserName>Shandon Business Solutions</SubscriberUserName><EnquiryInput>0000000000000</EnquiryInput><EnquiryReason /></SubscriberInputDetails><SubscriberInputDetailsOther><EnquiryDate>2015-02-03T11:21:09.0678548+02:00</EnquiryDate><EnquiryType>Consumer Trace</EnquiryType><SubscriberName>Shandon Business Solutions</SubscriberName><SubscriberUserName>Shandon Business Solutions</SubscriberUserName><EnquiryInput>0000000000000</EnquiryInput><EnquiryReason /></SubscriberInputDetailsOther><ConfirmedLinkages><ConfirmedLinkage><LinkageCategory>Contact No</LinkageCategory><ConsumerRecordDate>2011/05/05</ConsumerRecordDate><ConsumerRecordType>Work</ConsumerRecordType><OtherRecordDate>2010/04/03</OtherRecordDate><OtherRecordType>Home</OtherRecordType><MatchedData>0115750900</MatchedData></ConfirmedLinkage><ConfirmedLinkage><LinkageCategory>Address</LinkageCategory><ConsumerRecordDate>2011/05/05</ConsumerRecordDate><ConsumerRecordType>Work</ConsumerRecordType><OtherRecordDate>2010/04/03</OtherRecordDate><OtherRecordType>Work</OtherRecordType><MatchedData>57 Sloane Street, Bryanston, Johannesburg, 2095</MatchedData></ConfirmedLinkage><ConfirmedLinkage><LinkageCategory>Employer</LinkageCategory><ConsumerRecordDate>2011/05/05</ConsumerRecordDate><ConsumerRecordType>Work</ConsumerRecordType><OtherRecordDate>2010/04/03</OtherRecordDate><OtherRecordType>Work</OtherRecordType><MatchedData>Shandon Business Solutions</MatchedData></ConfirmedLinkage></ConfirmedLinkages><Consumer><ConsumerDetail><DisplayText>Consumer Detail</DisplayText><ConsumerID>81227833</ConsumerID><Initials>JJ</Initials><FirstName>JOHN</FirstName><SecondName>J</SecondName><ThirdName /><Surname>MOKOENA</Surname><IDNo>0000000000083</IDNo><PassportNo /><BirthDate>1980-01-01</BirthDate><Gender>Male</Gender><TitleDesc>Mister</TitleDesc><MaritalStatusDesc>Unknown</MaritalStatusDesc><PrivacyStatus>ACCEPTS CONTACTS</PrivacyStatus><ResidentialAddress>17 DE BUSSY STREET, DELFT SOUTH, 7100</ResidentialAddress><PostalAddress>PO BOX 174 TSABONG BOTSWA</PostalAddress><HomeTelephoneNo>0314624836</HomeTelephoneNo><WorkTelephoneNo>0319078721</WorkTelephoneNo><CellularNo>0824881921</CellularNo><EmailAddress>johan@optic1.co.za</EmailAddress><EmployerDetail>CAPE BANQUETS</EmployerDetail><ReferenceNo>C50790953-81227833</ReferenceNo><ExternalReference /></ConsumerDetail><ConsumerAddressHistory><DisplayText>Consumer Address History</DisplayText><ConsumerAddressID>403738644</ConsumerAddressID><AddressType>Postal</AddressType><AddressTypeInd>P</AddressTypeInd><Address1>PO BOX 174</Address1><Address2>TSABONG</Address2><Address3>BOTSWA</Address3><Address4 /><PostalCode /><Address>PO BOX 174 TSABONG BOTSWA</Address><LastUpdatedDate>2013-01-07</LastUpdatedDate><FirstReportedDate>2013-01-07</FirstReportedDate></ConsumerAddressHistory><ConsumerAddressHistory><DisplayText>Consumer Address History</DisplayText><ConsumerAddressID>403738644</ConsumerAddressID><AddressType>Postal</AddressType><AddressTypeInd>P</AddressTypeInd><Address1>PO BOX 174</Address1><Address2>TSABONG</Address2><Address3>BOTSWA</Address3><Address4 /><PostalCode /><Address>PO BOX 174 TSABONG BOTSWA</Address><LastUpdatedDate>2013-01-07</LastUpdatedDate><FirstReportedDate>2013-01-07</FirstReportedDate></ConsumerAddressHistory><ConsumerTelephoneHistory><DisplayText>Consumer Telephone History</DisplayText><ConsumerTelephoneID>390213666</ConsumerTelephoneID><TelephoneType>Cellular</TelephoneType><TelephoneTypeInd>C</TelephoneTypeInd><TelCode>082</TelCode><TelNo>4881921</TelNo><TelephoneNo>0824881921</TelephoneNo><EmailAddress /><LastUpdatedDate>2012-01-19</LastUpdatedDate><FirstReportedDate>2001-11-27</FirstReportedDate></ConsumerTelephoneHistory><ConsumerTelephoneHistory><DisplayText>Consumer Telephone History</DisplayText><ConsumerTelephoneID>390196340</ConsumerTelephoneID><TelephoneType>Work</TelephoneType><TelephoneTypeInd>W</TelephoneTypeInd><TelCode>031</TelCode><TelNo>9078721</TelNo><TelephoneNo>0319078721</TelephoneNo><EmailAddress /><LastUpdatedDate>2012-01-19</LastUpdatedDate><FirstReportedDate>2001-11-27</FirstReportedDate></ConsumerTelephoneHistory><ConsumerEmploymentHistory><DisplayText>Consumer Employment History</DisplayText><EmployerDetail>CAPE BANQUETS</EmployerDetail><Designation /><LastUpdatedDate>2009-04-27</LastUpdatedDate><FirstReportedDate>2008-10-24</FirstReportedDate></ConsumerEmploymentHistory><ConsumerEmploymentHistory><DisplayText>Consumer Employment History</DisplayText><EmployerDetail>MUNISIPALITEIT</EmployerDetail><Designation /><LastUpdatedDate>2008-10-18</LastUpdatedDate><FirstReportedDate>2008-10-18</FirstReportedDate></ConsumerEmploymentHistory></Consumer><ConsumerOther><ConsumerDetail><DisplayText>Consumer Detail</DisplayText><ConsumerID>81227833</ConsumerID><Initials>JJ</Initials><FirstName>JOHN</FirstName><SecondName>J</SecondName><ThirdName /><Surname>MOKOENA</Surname><IDNo>0000000000083</IDNo><PassportNo /><BirthDate>1980-01-01</BirthDate><Gender>Male</Gender><TitleDesc>Mister</TitleDesc><MaritalStatusDesc>Unknown</MaritalStatusDesc><PrivacyStatus>ACCEPTS CONTACTS</PrivacyStatus><ResidentialAddress>17 DE BUSSY STREET, DELFT SOUTH, 7100</ResidentialAddress><PostalAddress>PO BOX 174 TSABONG BOTSWA</PostalAddress><HomeTelephoneNo>0314624836</HomeTelephoneNo><WorkTelephoneNo>0319078721</WorkTelephoneNo><CellularNo>0824881921</CellularNo><EmailAddress>johan@optic1.co.za</EmailAddress><EmployerDetail>CAPE BANQUETS</EmployerDetail><ReferenceNo>C50790953-81227833</ReferenceNo><ExternalReference /></ConsumerDetail><ConsumerAddressHistory><DisplayText>Consumer Address History</DisplayText><ConsumerAddressID>403738644</ConsumerAddressID><AddressType>Postal</AddressType><AddressTypeInd>P</AddressTypeInd><Address1>PO BOX 174</Address1><Address2>TSABONG</Address2><Address3>BOTSWA</Address3><Address4 /><PostalCode /><Address>PO BOX 174 TSABONG BOTSWA</Address><LastUpdatedDate>2013-01-07</LastUpdatedDate><FirstReportedDate>2013-01-07</FirstReportedDate></ConsumerAddressHistory><ConsumerAddressHistory><DisplayText>Consumer Address History</DisplayText><ConsumerAddressID>403738644</ConsumerAddressID><AddressType>Postal</AddressType><AddressTypeInd>P</AddressTypeInd><Address1>PO BOX 174</Address1><Address2>TSABONG</Address2><Address3>BOTSWA</Address3><Address4 /><PostalCode /><Address>PO BOX 174 TSABONG BOTSWA</Address><LastUpdatedDate>2013-01-07</LastUpdatedDate><FirstReportedDate>2013-01-07</FirstReportedDate></ConsumerAddressHistory><ConsumerTelephoneHistory><DisplayText>Consumer Telephone History</DisplayText><ConsumerTelephoneID>390213666</ConsumerTelephoneID><TelephoneType>Cellular</TelephoneType><TelephoneTypeInd>C</TelephoneTypeInd><TelCode>082</TelCode><TelNo>4881921</TelNo><TelephoneNo>0824881921</TelephoneNo><EmailAddress /><LastUpdatedDate>2012-01-19</LastUpdatedDate><FirstReportedDate>2001-11-27</FirstReportedDate></ConsumerTelephoneHistory><ConsumerTelephoneHistory><DisplayText>Consumer Telephone History</DisplayText><ConsumerTelephoneID>390196340</ConsumerTelephoneID><TelephoneType>Work</TelephoneType><TelephoneTypeInd>W</TelephoneTypeInd><TelCode>031</TelCode><TelNo>9078721</TelNo><TelephoneNo>0319078721</TelephoneNo><EmailAddress /><LastUpdatedDate>2012-01-19</LastUpdatedDate><FirstReportedDate>2001-11-27</FirstReportedDate></ConsumerTelephoneHistory></ConsumerOther></Linkage>";
        }

        public string ConnectConsumerMatchKYC(string ConnectTicket, string EnquiryReason, int ProductId, string IdNumber, string PassportNo, string Surname, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectConsumerMatchKYC(ConnectTicket, EnquiryReason, ProductId, IdNumber, PassportNo, Surname, YourReference, VoucherCode);
        }

        public System.Threading.Tasks.Task<string> ConnectConsumerMatchKYCAsync(string ConnectTicket, string EnquiryReason, int ProductId, string IdNumber, string PassportNo, string Surname, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectConsumerMatchKYCAsync(ConnectTicket, EnquiryReason, ProductId, IdNumber, PassportNo, Surname, YourReference, VoucherCode);
        }

        public string ConnectConsumerMatchCreditAssesment(
                    string ConnectTicket,
                    string EnquiryReason,
                    int ProductId,
                    string IdNumber,
                    string PassportNo,
                    string FirstName,
                    string Surname,
                    string BirthDate,
                    string MaritalStatus,
                    string SpouseFirstName,
                    string SpouseSurname,
                    string ResidentialAddressLine1,
                    string ResidentialAddressLine2,
                    string ResidentialAddressLine3,
                    string ResidentialAddressLine4,
                    string ResidentialPostalCode,
                    string PostalAddressLine1,
                    string PostalAddressLine2,
                    string PostalAddressLine3,
                    string PostalAddressLine4,
                    string PostalPostalCode,
                    string HomeTelCode,
                    string HomeTelNo,
                    string WorkTelCode,
                    string WorkTelNo,
                    string CellularCode,
                    string CellularNo,
                    string EmailAddress,
                    string TotalNetMonthlyIncome,
                    string Employer,
                    string JobTitle,
                    string YourReference,
                    string VoucherCode)
        {
            return _XDSConnectWS.ConnectConsumerMatchCreditAssesment(ConnectTicket, EnquiryReason, ProductId, IdNumber, PassportNo, FirstName, Surname, BirthDate, MaritalStatus, SpouseFirstName, SpouseSurname, ResidentialAddressLine1, ResidentialAddressLine2, ResidentialAddressLine3, ResidentialAddressLine4, ResidentialPostalCode, PostalAddressLine1, PostalAddressLine2, PostalAddressLine3, PostalAddressLine4, PostalPostalCode, HomeTelCode, HomeTelNo, WorkTelCode, WorkTelNo, CellularCode, CellularNo, EmailAddress, TotalNetMonthlyIncome, Employer, JobTitle, YourReference, VoucherCode);
        }

        public System.Threading.Tasks.Task<string> ConnectConsumerMatchCreditAssesmentAsync(
                    string ConnectTicket,
                    string EnquiryReason,
                    int ProductId,
                    string IdNumber,
                    string PassportNo,
                    string FirstName,
                    string Surname,
                    string BirthDate,
                    string MaritalStatus,
                    string SpouseFirstName,
                    string SpouseSurname,
                    string ResidentialAddressLine1,
                    string ResidentialAddressLine2,
                    string ResidentialAddressLine3,
                    string ResidentialAddressLine4,
                    string ResidentialPostalCode,
                    string PostalAddressLine1,
                    string PostalAddressLine2,
                    string PostalAddressLine3,
                    string PostalAddressLine4,
                    string PostalPostalCode,
                    string HomeTelCode,
                    string HomeTelNo,
                    string WorkTelCode,
                    string WorkTelNo,
                    string CellularCode,
                    string CellularNo,
                    string EmailAddress,
                    string TotalNetMonthlyIncome,
                    string Employer,
                    string JobTitle,
                    string YourReference,
                    string VoucherCode)
        {
            return _XDSConnectWS.ConnectConsumerMatchCreditAssesmentAsync(ConnectTicket, EnquiryReason, ProductId, IdNumber, PassportNo, FirstName, Surname, BirthDate, MaritalStatus, SpouseFirstName, SpouseSurname, ResidentialAddressLine1, ResidentialAddressLine2, ResidentialAddressLine3, ResidentialAddressLine4, ResidentialPostalCode, PostalAddressLine1, PostalAddressLine2, PostalAddressLine3, PostalAddressLine4, PostalPostalCode, HomeTelCode, HomeTelNo, WorkTelCode, WorkTelNo, CellularCode, CellularNo, EmailAddress, TotalNetMonthlyIncome, Employer, JobTitle, YourReference, VoucherCode);
        }

        public string ConnectConsumerMatchNoTracePlus(
                    string ConnectTicket,
                    string EnquiryReason,
                    int ProductId,
                    string IdNumber,
                    string PassportNo,
                    string FirstName,
                    string Surname,
                    string BirthDate,
                    string AccountNo,
                    string SubAccountNo,
                    string Province,
                    string City,
                    string Suburb,
                    bool PostalMatch,
                    string StreetName_PostalNo,
                    string PostalCode,
                    string StreetNo,
                    string TelephoneCode,
                    string TelephoneNo,
                    string SearchType,
                    int Age,
                    int Year,
                    int Deviation,
                    string YourReference,
                    string VoucherCode)
        {
            return _XDSConnectWS.ConnectConsumerMatchNoTracePlus(ConnectTicket, EnquiryReason, ProductId, IdNumber, PassportNo, FirstName, Surname, BirthDate, AccountNo, SubAccountNo, Province, City, Suburb, PostalMatch, StreetName_PostalNo, PostalCode, StreetNo, TelephoneCode, TelephoneNo, SearchType, Age, Year, Deviation, YourReference, VoucherCode);
        }

        public System.Threading.Tasks.Task<string> ConnectConsumerMatchNoTracePlusAsync(
                    string ConnectTicket,
                    string EnquiryReason,
                    int ProductId,
                    string IdNumber,
                    string PassportNo,
                    string FirstName,
                    string Surname,
                    string BirthDate,
                    string AccountNo,
                    string SubAccountNo,
                    string Province,
                    string City,
                    string Suburb,
                    bool PostalMatch,
                    string StreetName_PostalNo,
                    string PostalCode,
                    string StreetNo,
                    string TelephoneCode,
                    string TelephoneNo,
                    string SearchType,
                    int Age,
                    int Year,
                    int Deviation,
                    string YourReference,
                    string VoucherCode)
        {
            return _XDSConnectWS.ConnectConsumerMatchNoTracePlusAsync(ConnectTicket, EnquiryReason, ProductId, IdNumber, PassportNo, FirstName, Surname, BirthDate, AccountNo, SubAccountNo, Province, City, Suburb, PostalMatch, StreetName_PostalNo, PostalCode, StreetNo, TelephoneCode, TelephoneNo, SearchType, Age, Year, Deviation, YourReference, VoucherCode);
        }

        public string ConnectAccountMatch(string ConnectTicket, string AccountNo, string SubAccountNo, string Surname, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectAccountMatch(ConnectTicket, AccountNo, SubAccountNo, Surname, YourReference, VoucherCode);
        }

        public System.Threading.Tasks.Task<string> ConnectAccountMatchAsync(string ConnectTicket, string AccountNo, string SubAccountNo, string Surname, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectAccountMatchAsync(ConnectTicket, AccountNo, SubAccountNo, Surname, YourReference, VoucherCode);
        }

        public string ConnectAddressMatch(string ConnectTicket, string Province, string City, string Suburb, bool PostalMatch, string StreetName_PostalNo, string PostalCode, string StreetNo, string Surname, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectAddressMatch(ConnectTicket, Province, City, Suburb, PostalMatch, StreetName_PostalNo, PostalCode, StreetNo, Surname, YourReference, VoucherCode);
        }

        public System.Threading.Tasks.Task<string> ConnectAddressMatchAsync(string ConnectTicket, string Province, string City, string Suburb, bool PostalMatch, string StreetName_PostalNo, string PostalCode, string StreetNo, string Surname, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectAddressMatchAsync(ConnectTicket, Province, City, Suburb, PostalMatch, StreetName_PostalNo, PostalCode, StreetNo, Surname, YourReference, VoucherCode);
        }

        public string ConnectTelephoneMatch(string ConnectTicket, string TelephoneCode, string TelephoneNo, string Surname, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectTelephoneMatch(ConnectTicket, TelephoneCode, TelephoneNo, Surname, YourReference, VoucherCode);
        }

        public System.Threading.Tasks.Task<string> ConnectTelephoneMatchAsync(string ConnectTicket, string TelephoneCode, string TelephoneNo, string Surname, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectTelephoneMatchAsync(ConnectTicket, TelephoneCode, TelephoneNo, Surname, YourReference, VoucherCode);
        }

        public string ConnectAddressIDMatch(string ConnectTicket, int ConsumerAddressID, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectAddressIDMatch(ConnectTicket, ConsumerAddressID, YourReference, VoucherCode);
        }

        public System.Threading.Tasks.Task<string> ConnectAddressIDMatchAsync(string ConnectTicket, int ConsumerAddressID, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectAddressIDMatchAsync(ConnectTicket, ConsumerAddressID, YourReference, VoucherCode);
        }

        public string ConnectTelephoneIDMatch(string ConnectTicket, int ConsumerTelephoneID, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectTelephoneIDMatch(ConnectTicket, ConsumerTelephoneID, YourReference, VoucherCode);
        }

        public System.Threading.Tasks.Task<string> ConnectTelephoneIDMatchAsync(string ConnectTicket, int ConsumerTelephoneID, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectTelephoneIDMatchAsync(ConnectTicket, ConsumerTelephoneID, YourReference, VoucherCode);
        }

        public string ConnectEasyMatch(string ConnectTicket, int ProductId, string SearchType, string Surname, string FirstName, int Age, int Year, int Deviation, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectEasyMatch(ConnectTicket, ProductId, SearchType, Surname, FirstName, Age, Year, Deviation, YourReference, VoucherCode);
        }

        public System.Threading.Tasks.Task<string> ConnectEasyMatchAsync(string ConnectTicket, int ProductId, string SearchType, string Surname, string FirstName, int Age, int Year, int Deviation, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectEasyMatchAsync(ConnectTicket, ProductId, SearchType, Surname, FirstName, Age, Year, Deviation, YourReference, VoucherCode);
        }

        public string ConnectIDVerification(string ConnectTicket, string IdNumber, string FirstName, string Surname, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectIDVerification(ConnectTicket, IdNumber, FirstName, Surname, YourReference, VoucherCode);
        }

        public System.Threading.Tasks.Task<string> ConnectIDVerificationAsync(string ConnectTicket, string IdNumber, string FirstName, string Surname, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectIDVerificationAsync(ConnectTicket, IdNumber, FirstName, Surname, YourReference, VoucherCode);
        }

        public string ConnectBusinessMatch(string ConnectTicket, string Reg1, string Reg2, string Reg3, string BusinessName, string VatNo, string SolePropIDNo, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectBusinessMatch(ConnectTicket, Reg1, Reg2, Reg3, BusinessName, VatNo, SolePropIDNo, YourReference, VoucherCode);
        }

        public System.Threading.Tasks.Task<string> ConnectBusinessMatchAsync(string ConnectTicket, string Reg1, string Reg2, string Reg3, string BusinessName, string VatNo, string SolePropIDNo, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectBusinessMatchAsync(ConnectTicket, Reg1, Reg2, Reg3, BusinessName, VatNo, SolePropIDNo, YourReference, VoucherCode);
        }

        public string ConnectDirectorMatch(string ConnectTicket, string IdNumber, string FirstName, string Surname, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectDirectorMatch(ConnectTicket, IdNumber, FirstName, Surname, YourReference, VoucherCode);
        }

 

        public System.Threading.Tasks.Task<string> ConnectDirectorMatchAsync(string ConnectTicket, string IdNumber, string FirstName, string Surname, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectDirectorMatchAsync(ConnectTicket, IdNumber, FirstName, Surname, YourReference, VoucherCode);
        }

        //public string ConnectDeedsMatch(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.SearchtypeInd intSearchTypeINd, string DeedsOffice, string IDNo, string FirstName, string SurName, string RegNo1, string RegNo2, string RegNo3, string BusinessName, string TownshipName, string ErfNo, string PortionNo, string TitleDeedNo, string BondAccno, string YourReference, string VoucherCode)
        //{
        //    return _XDSConnectWS.ConnectDeedsMatch(ConnectTicket, intSearchTypeINd, DeedsOffice, IDNo, FirstName, SurName, RegNo1, RegNo2, RegNo3, BusinessName, TownshipName, ErfNo, PortionNo, TitleDeedNo, BondAccno, YourReference, VoucherCode);
        //}


        //public System.Threading.Tasks.Task<string> ConnectDeedsMatchAsync(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.SearchtypeInd intSearchTypeINd, string DeedsOffice, string IDNo, string FirstName, string SurName, string RegNo1, string RegNo2, string RegNo3, string BusinessName, string TownshipName, string ErfNo, string PortionNo, string TitleDeedNo, string BondAccno, string YourReference, string VoucherCode)
        //{
        //    return _XDSConnectWS.ConnectDeedsMatchAsync(ConnectTicket, intSearchTypeINd, DeedsOffice, IDNo, FirstName, SurName, RegNo1, RegNo2, RegNo3, BusinessName, TownshipName, ErfNo, PortionNo, TitleDeedNo, BondAccno, YourReference, VoucherCode);
        //}


        public string ConnectIDTelephoneMatch(string ConnectTicket, int ProductId, string IdNumber, string PassportNo, string TelephoneCode, string TelephoneNo, string YourReference, string VoucherCode, bool IsIdNumberMatch)
        {
            return _XDSConnectWS.ConnectIDTelephoneMatch(ConnectTicket, ProductId, IdNumber, PassportNo, TelephoneCode, TelephoneNo, YourReference, VoucherCode, IsIdNumberMatch);
        }

        public System.Threading.Tasks.Task<string> ConnectIDTelephoneMatchAsync(string ConnectTicket, int ProductId, string IdNumber, string PassportNo, string TelephoneCode, string TelephoneNo, string YourReference, string VoucherCode, bool IsIdNumberMatch)
        {
            return _XDSConnectWS.ConnectIDTelephoneMatchAsync(ConnectTicket, ProductId, IdNumber, PassportNo, TelephoneCode, TelephoneNo, YourReference, VoucherCode, IsIdNumberMatch);
        }

        public string ConnectGetBonusSegments(string ConnectTicket, int EnquiryResultID)
        {
            return _XDSConnectWS.ConnectGetBonusSegments(ConnectTicket, EnquiryResultID);
        }

        public System.Threading.Tasks.Task<string> ConnectGetBonusSegmentsAsync(string ConnectTicket, int EnquiryResultID)
        {
            return _XDSConnectWS.ConnectGetBonusSegmentsAsync(ConnectTicket, EnquiryResultID);
        }

        public string ConnectGetResult(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML)
        {
            return _XDSConnectWS.ConnectGetResult(ConnectTicket, EnquiryID, EnquiryResultID, ProductID, BonusXML);
        }

        public System.Threading.Tasks.Task<string> ConnectGetResultAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML)
        {
            return _XDSConnectWS.ConnectGetResultAsync(ConnectTicket, EnquiryID, EnquiryResultID, ProductID, BonusXML);
        }

        public string ConnectGetEnquiryReasons(string ConnectTicket)
        {
            return _XDSConnectWS.ConnectGetEnquiryReasons(ConnectTicket);
        }

        public System.Threading.Tasks.Task<string> ConnectGetEnquiryReasonsAsync(string ConnectTicket)
        {
            return _XDSConnectWS.ConnectGetEnquiryReasonsAsync(ConnectTicket);
        }

        public string ConnectBusinessInvestigativeEnquiry(
                    string ConnectTicket,
                    string Country,
                    string ReportType,
                    string TimeFrame,
                    string Reg1,
                    string Reg2,
                    string Reg3,
                    string BusinessName,
                    string IDNo,
                    string AdditionalInfo,
                    string CompanyContactNo,
                    string strTerms,
                    double dAmount,
                    string FirstName,
                    string SurName,
                    string TelNo,
                    string EmailAddress,
                    string CompanyName,
                    bool bSendEmail,
                    string VoucherCode,
                    string YourReference)
        {
            return _XDSConnectWS.ConnectBusinessInvestigativeEnquiry(ConnectTicket, Country, ReportType, TimeFrame, Reg1, Reg2, Reg3, BusinessName, IDNo, AdditionalInfo, CompanyContactNo, strTerms, dAmount, FirstName, SurName, TelNo, EmailAddress, CompanyName, bSendEmail, VoucherCode, YourReference);
        }

        public System.Threading.Tasks.Task<string> ConnectBusinessInvestigativeEnquiryAsync(
                    string ConnectTicket,
                    string Country,
                    string ReportType,
                    string TimeFrame,
                    string Reg1,
                    string Reg2,
                    string Reg3,
                    string BusinessName,
                    string IDNo,
                    string AdditionalInfo,
                    string CompanyContactNo,
                    string strTerms,
                    double dAmount,
                    string FirstName,
                    string SurName,
                    string TelNo,
                    string EmailAddress,
                    string CompanyName,
                    bool bSendEmail,
                    string VoucherCode,
                    string YourReference)
        {
            return _XDSConnectWS.ConnectBusinessInvestigativeEnquiryAsync(ConnectTicket, Country, ReportType, TimeFrame, Reg1, Reg2, Reg3, BusinessName, IDNo, AdditionalInfo, CompanyContactNo, strTerms, dAmount, FirstName, SurName, TelNo, EmailAddress, CompanyName, bSendEmail, VoucherCode, YourReference);
        }

        public string ConnectCustomBusinessInvestigativeEnquiry(
                    string ConnectTicket,
                    string Country,
                    string ReportType,
                    string TimeFrame,
                    string Reg1,
                    string Reg2,
                    string Reg3,
                    string BusinessName,
                    string IDNo,
                    string AdditionalInfo,
                    string CompanyContactNo,
                    string strTerms,
                    double dAmount,
                    string FirstName,
                    string SurName,
                    string TelNo,
                    string EmailAddress,
                    string CompanyName,
                    bool bSendEmail,
                    int ProductID,
                    string VoucherCode,
                    string YourReference)
        {
            return _XDSConnectWS.ConnectCustomBusinessInvestigativeEnquiry(ConnectTicket, Country, ReportType, TimeFrame, Reg1, Reg2, Reg3, BusinessName, IDNo, AdditionalInfo, CompanyContactNo, strTerms, dAmount, FirstName, SurName, TelNo, EmailAddress, CompanyName, bSendEmail, ProductID, VoucherCode, YourReference);
        }

        public System.Threading.Tasks.Task<string> ConnectCustomBusinessInvestigativeEnquiryAsync(
                    string ConnectTicket,
                    string Country,
                    string ReportType,
                    string TimeFrame,
                    string Reg1,
                    string Reg2,
                    string Reg3,
                    string BusinessName,
                    string IDNo,
                    string AdditionalInfo,
                    string CompanyContactNo,
                    string strTerms,
                    double dAmount,
                    string FirstName,
                    string SurName,
                    string TelNo,
                    string EmailAddress,
                    string CompanyName,
                    bool bSendEmail,
                    int ProductID,
                    string VoucherCode,
                    string YourReference)
        {
            return _XDSConnectWS.ConnectCustomBusinessInvestigativeEnquiryAsync(ConnectTicket, Country, ReportType, TimeFrame, Reg1, Reg2, Reg3, BusinessName, IDNo, AdditionalInfo, CompanyContactNo, strTerms, dAmount, FirstName, SurName, TelNo, EmailAddress, CompanyName, bSendEmail, ProductID, VoucherCode, YourReference);
        }

        public string ConnectGetBusinessInvestigativeEnquiryResult(string ConnectTicket, int ReferenceNo)
        {
            return _XDSConnectWS.ConnectGetBusinessInvestigativeEnquiryResult(ConnectTicket, ReferenceNo);
        }

        public System.Threading.Tasks.Task<string> ConnectGetBusinessInvestigativeEnquiryResultAsync(string ConnectTicket, int ReferenceNo)
        {
            return _XDSConnectWS.ConnectGetBusinessInvestigativeEnquiryResultAsync(ConnectTicket, ReferenceNo);
        }

        public string ConnectDeedsMatch(
                    string ConnectTicket,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.SearchtypeInd intSearchTypeINd,
                    string DeedsOffice,
                    string IDNo,
                    string FirstName,
                    string SurName,
                    string RegNo1,
                    string RegNo2,
                    string RegNo3,
                    string BusinessName,
                    string TownshipName,
                    string ErfNo,
                    string PortionNo,
                    string TitleDeedNo,
                    string BondAccno,
                    string YourReference,
                    string VoucherCode)
        {
            return _XDSConnectWS.ConnectDeedsMatch(ConnectTicket, intSearchTypeINd, DeedsOffice, IDNo, FirstName, SurName, RegNo1, RegNo2, RegNo3, BusinessName, TownshipName, ErfNo, PortionNo, TitleDeedNo, BondAccno, YourReference, VoucherCode);
        }

        public System.Threading.Tasks.Task<string> ConnectDeedsMatchAsync(
                    string ConnectTicket,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.SearchtypeInd intSearchTypeINd,
                    string DeedsOffice,
                    string IDNo,
                    string FirstName,
                    string SurName,
                    string RegNo1,
                    string RegNo2,
                    string RegNo3,
                    string BusinessName,
                    string TownshipName,
                    string ErfNo,
                    string PortionNo,
                    string TitleDeedNo,
                    string BondAccno,
                    string YourReference,
                    string VoucherCode)
        {
            return _XDSConnectWS.ConnectDeedsMatchAsync(ConnectTicket, intSearchTypeINd, DeedsOffice, IDNo, FirstName, SurName, RegNo1, RegNo2, RegNo3, BusinessName, TownshipName, ErfNo, PortionNo, TitleDeedNo, BondAccno, YourReference, VoucherCode);
        }


        public System.Threading.Tasks.Task<string> ConnectGetSearchHistoryAsync(string ConnectTicket, int ProductID, string Username)
        {
            return _XDSConnectWS.ConnectGetSearchHistoryAsync(ConnectTicket, ProductID, Username);
        }

                       
        public string ConnectBankCodesRequest(
                    string ConnectTicket,
                    string BankCodeRequestType,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.TypeofVerificationenum verficationType,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.Entity EntityType,
                    string IDno,
                    string Reg1,
                    string Reg2,
                    string Reg3,
                    string TrustNo,
                    string BankName,
                    string BranchName,
                    string BranchCode,


                    string AccNo,
                    string AccHolderName,
                    double Amount,
                    string Terms,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.Region BankCodeRegion,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.CountryCode BankCodeCountryCode,
                    string AdditionalInfo,
                    string FirstName,
                    string SurName,
                    string CompanyName,
                    string TelNo,
                    string EmailAddress,
                    bool bSendEmail,
                    string VoucherCode,
                    string YourReference)
        {
            return _XDSConnectWS.ConnectBankCodesRequest(ConnectTicket, BankCodeRequestType, verficationType, EntityType, IDno, Reg1, Reg2, Reg3, TrustNo, BankName, BranchName, BranchCode, AccNo, AccHolderName, Amount, Terms, BankCodeRegion, BankCodeCountryCode, AdditionalInfo, FirstName, SurName, CompanyName, TelNo, EmailAddress, bSendEmail, VoucherCode, YourReference);
        }

        public System.Threading.Tasks.Task<string> ConnectBankCodesRequestAsync(
                    string ConnectTicket,
                    string BankCodeRequestType,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.TypeofVerificationenum verficationType,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.Entity EntityType,
                    string IDno,
                    string Reg1,
                    string Reg2,
                    string Reg3,
                    string TrustNo,
                    string BankName,
                    string BranchName,
                    string BranchCode,
                    string AccNo,
                    string AccHolderName,
                    double Amount,
                    string Terms,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.Region BankCodeRegion,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.CountryCode BankCodeCountryCode,
                    string AdditionalInfo,
                    string FirstName,
                    string SurName,
                    string CompanyName,
                    string TelNo,
                    string EmailAddress,
                    bool bSendEmail,
                    string VoucherCode,
                    string YourReference)
        {
            return _XDSConnectWS.ConnectBankCodesRequestAsync(ConnectTicket, BankCodeRequestType, verficationType, EntityType, IDno, Reg1, Reg2, Reg3, TrustNo, BankName, BranchName, BranchCode, AccNo, AccHolderName, Amount, Terms, BankCodeRegion, BankCodeCountryCode, AdditionalInfo, FirstName, SurName, CompanyName, TelNo, EmailAddress, bSendEmail, VoucherCode, YourReference);
        }

        public string ConnectAccountVerification(
                    string ConnectTicket,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.TypeofVerificationenum VerificationType,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.Entity Entity,
                    string Initials,
                    string SurName,
                    string IDNo,
                    string CompanyName,
                    string Reg1,
                    string Reg2,
                    string Reg3,
                    string TrustNo,
                    string AccNo,
                    string BranchCode,
                    string Acctype,
                    string BankName,
                    string ClientFirstName,
                    string ClientSurName,
                    string EmailAddress,
                    bool bSendEmail,
                    string VoucherCode,
                    string YourReference)
        {
            return _XDSConnectWS.ConnectAccountVerification(ConnectTicket, VerificationType, Entity, Initials, SurName, IDNo, CompanyName, Reg1, Reg2, Reg3, TrustNo, AccNo, BranchCode, Acctype, BankName, ClientFirstName, ClientSurName, EmailAddress, bSendEmail, VoucherCode, YourReference);
        }

        public System.Threading.Tasks.Task<string> ConnectAccountVerificationAsync(
                    string ConnectTicket,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.TypeofVerificationenum VerificationType,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.Entity Entity,
                    string Initials,
                    string SurName,
                    string IDNo,
                    string CompanyName,
                    string Reg1,
                    string Reg2,
                    string Reg3,
                    string TrustNo,
                    string AccNo,
                    string BranchCode,
                    string Acctype,
                    string BankName,
                    string ClientFirstName,
                    string ClientSurName,
                    string EmailAddress,
                    bool bSendEmail,
                    string VoucherCode,
                    string YourReference)
        {
            return _XDSConnectWS.ConnectAccountVerificationAsync(ConnectTicket, VerificationType, Entity, Initials, SurName, IDNo, CompanyName, Reg1, Reg2, Reg3, TrustNo, AccNo, BranchCode, Acctype, BankName, ClientFirstName, ClientSurName, EmailAddress, bSendEmail, VoucherCode, YourReference);
        }

        public string ConnectAccountVerificationRealTime(
                    string ConnectTicket,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.TypeofVerificationenum VerificationType,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.Entity Entity,
                    string Initials,
                    string SurName,
                    string IDNo,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AVSIDType IDType,
                    string CompanyName,
                    string Reg1,
                    string Reg2,
                    string Reg3,
                    string TrustNo,
                    string AccNo,
                    string BranchCode,
                    string Acctype,
                    string BankName,
                    string VoucherCode,
                    string YourReference)
        {
            return _XDSConnectWS.ConnectAccountVerificationRealTime(ConnectTicket, VerificationType, Entity, Initials, SurName, IDNo, IDType, CompanyName, Reg1, Reg2, Reg3, TrustNo, AccNo, BranchCode, Acctype, BankName, VoucherCode, YourReference);
        }

        public System.Threading.Tasks.Task<string> ConnectAccountVerificationRealTimeAsync(
                    string ConnectTicket,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.TypeofVerificationenum VerificationType,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.Entity Entity,
                    string Initials,
                    string SurName,
                    string IDNo,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AVSIDType IDType,
                    string CompanyName,
                    string Reg1,
                    string Reg2,
                    string Reg3,
                    string TrustNo,
                    string AccNo,
                    string BranchCode,
                    string Acctype,
                    string BankName,
                    string VoucherCode,
                    string YourReference)
        {
            return _XDSConnectWS.ConnectAccountVerificationRealTimeAsync(ConnectTicket, VerificationType, Entity, Initials, SurName, IDNo, IDType, CompanyName, Reg1, Reg2, Reg3, TrustNo, AccNo, BranchCode, Acctype, BankName, VoucherCode, YourReference);
        }

        public string ConnectGetBankNames(string ConnectTicket)
        {
            return _XDSConnectWS.ConnectGetBankNames(ConnectTicket);
        }

        public System.Threading.Tasks.Task<string> ConnectGetBankNamesAsync(string ConnectTicket)
        {
            return _XDSConnectWS.ConnectGetBankNamesAsync(ConnectTicket);
        }

        //public System.Threading.Tasks.Task<string> ConnectBCGetBankNamesAsync(string ConnectTicket)
        //{
        //    return _XDSConnectWS.ConnectBCGetBankNames(ConnectTicket);
        //}


        public string ConnectGetDeedsOffices(string ConnectTicket)
        {
            return _XDSConnectWS.ConnectGetDeedsOffices(ConnectTicket);
        }


        public System.Threading.Tasks.Task<string> ConnectGetDeedsOfficesAsync(string ConnectTicket)
        {
            return _XDSConnectWS.ConnectGetDeedsOfficesAsync(ConnectTicket);
        }

        public string ConnectGetProductList(string ConnectTicket)
        {
            return _XDSConnectWS.ConnectGetProductList(ConnectTicket);
        }


        public System.Threading.Tasks.Task<string> ConnectGetProductListAsync(string ConnectTicket)
        {
            return _XDSConnectWS.ConnectGetProductListAsync(ConnectTicket);
        }

        public string ConnectBCGetBankNames(string ConnectTicket)
        {
            return _XDSConnectWS.ConnectBCGetBankNames(ConnectTicket);
        }

        public System.Threading.Tasks.Task<string> ConnectBCGetBankNamesAsync(string ConnectTicket)
        {
            return _XDSConnectWS.ConnectBCGetBankNamesAsync(ConnectTicket);
        }

        public string ConnectGetRegion(string ConnectTicket)
        {
            return _XDSConnectWS.ConnectGetRegion(ConnectTicket);
        }

        public System.Threading.Tasks.Task<string> ConnectGetRegionAsync(string ConnectTicket)
        {
            return _XDSConnectWS.ConnectGetRegionAsync(ConnectTicket);
        }

        public string ConnectGetAccountTypes(string ConnectTicket)
        {
            return _XDSConnectWS.ConnectGetAccountTypes(ConnectTicket);
        }

        public System.Threading.Tasks.Task<string> ConnectGetAccountTypesAsync(string ConnectTicket)
        {
            return _XDSConnectWS.ConnectGetAccountTypesAsync(ConnectTicket);
        }

        public string ConnectGetTerms(string ConnectTicket, int ProductID)
        {
            return _XDSConnectWS.ConnectGetTerms(ConnectTicket, ProductID);
        }

        public System.Threading.Tasks.Task<string> ConnectGetTermsAsync(string ConnectTicket, int ProductID)
        {
            return _XDSConnectWS.ConnectGetTermsAsync(ConnectTicket, ProductID);
        }

        public string ConnectGetCountryCodes(string ConnectTicket)
        {
            return _XDSConnectWS.ConnectGetCountryCodes(ConnectTicket);
        }

        public System.Threading.Tasks.Task<string> ConnectGetCountryCodesAsync(string ConnectTicket)
        {
            return _XDSConnectWS.ConnectGetCountryCodesAsync(ConnectTicket);
        }

        public string ConnectGetReportTypes(string ConnectTicket, int CountryID)
        {
            return _XDSConnectWS.ConnectGetReportTypes(ConnectTicket, CountryID);
        }

        public System.Threading.Tasks.Task<string> ConnectGetReportTypesAsync(string ConnectTicket, int CountryID)
        {
            return _XDSConnectWS.ConnectGetReportTypesAsync(ConnectTicket, CountryID);
        }

        public string ConnectGetReportTimeFrames(string ConnectTicket, int ReportID)
        {
            return _XDSConnectWS.ConnectGetReportTimeFrames(ConnectTicket, ReportID);
        }

        public System.Threading.Tasks.Task<string> ConnectGetReportTimeFramesAsync(string ConnectTicket, int ReportID)
        {
            return _XDSConnectWS.ConnectGetReportTimeFramesAsync(ConnectTicket, ReportID);
        }

        public string ConnectGetBankCodeRequestType(string ConnectTicket)
        {
            return _XDSConnectWS.ConnectGetBankCodeRequestType(ConnectTicket);
        }

        public System.Threading.Tasks.Task<string> ConnectGetBankCodeRequestTypeAsync(string ConnectTicket)
        {
            return _XDSConnectWS.ConnectGetBankCodeRequestTypeAsync(ConnectTicket);
        }

        public string ConnectGetAccountVerificationResult(string ConnectTicket, int EnquiryLogID)
        {
            return _XDSConnectWS.ConnectGetAccountVerificationResult(ConnectTicket, EnquiryLogID);
        }

        public System.Threading.Tasks.Task<string> ConnectGetAccountVerificationResultAsync(string ConnectTicket, int EnquiryLogID)
        {
            return _XDSConnectWS.ConnectGetAccountVerificationResultAsync(ConnectTicket, EnquiryLogID);
        }

        public string ConnectHomeAffairsIDVerification(string ConnectTicket, int ProductId, string IdNumber, string FirstName, string Surname, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectHomeAffairsIDVerification(ConnectTicket, ProductId, IdNumber, FirstName, Surname, YourReference, VoucherCode);
        }

        public System.Threading.Tasks.Task<string> ConnectHomeAffairsIDVerificationAsync(string ConnectTicket, int ProductId, string IdNumber, string FirstName, string Surname, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectHomeAffairsIDVerificationAsync(ConnectTicket, ProductId, IdNumber, FirstName, Surname, YourReference, VoucherCode);
        }

        public string ConnectRequestBirthorDeathCertificate(string ConnectTicket, int ProductId, string IdNumber, string FirstName, string Surname, string ClientFirstname, string ClientSurname, string Company, string ContactNo, string EmailAddress, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectRequestBirthorDeathCertificate(ConnectTicket, ProductId, IdNumber, FirstName, Surname, ClientFirstname, ClientSurname, Company, ContactNo, EmailAddress, YourReference, VoucherCode);
        }

        public System.Threading.Tasks.Task<string> ConnectRequestBirthorDeathCertificateAsync(string ConnectTicket, int ProductId, string IdNumber, string FirstName, string Surname, string ClientFirstname, string ClientSurname, string Company, string ContactNo, string EmailAddress, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectRequestBirthorDeathCertificateAsync(ConnectTicket, ProductId, IdNumber, FirstName, Surname, ClientFirstname, ClientSurname, Company, ContactNo, EmailAddress, YourReference, VoucherCode);
        }

        public string AdminSubscriber(string ConnectTicket)
        {
            return _XDSConnectWS.AdminSubscriber(ConnectTicket);
        }

        public System.Threading.Tasks.Task<string> AdminSubscriberAsync(string ConnectTicket)
        {
            return _XDSConnectWS.AdminSubscriberAsync(ConnectTicket);
        }

        public string AdminSystemUser(string ConnectTicket, bool GetAll)
        {
            return _XDSConnectWS.AdminSystemUser(ConnectTicket, GetAll);
        }

        public System.Threading.Tasks.Task<string> AdminSystemUserAsync(string ConnectTicket, bool GetAll)
        {
            return _XDSConnectWS.AdminSystemUserAsync(ConnectTicket, GetAll);
        }

        public string AdminProduct(string ConnectTicket)
        {
            return _XDSConnectWS.AdminProduct(ConnectTicket);
        }

        public System.Threading.Tasks.Task<string> AdminProductAsync(string ConnectTicket)
        {
            return _XDSConnectWS.AdminProductAsync(ConnectTicket);
        }

        public string AdminEnquiryHistory(string ConnectTicket, int EnquiryID, int ProductID, bool GetAll)
        {
            return _XDSConnectWS.AdminEnquiryHistory(ConnectTicket, EnquiryID, ProductID, GetAll);
        }

        public System.Threading.Tasks.Task<string> AdminEnquiryHistoryAsync(string ConnectTicket, int EnquiryID, int ProductID, bool GetAll)
        {
            return _XDSConnectWS.AdminEnquiryHistoryAsync(ConnectTicket, EnquiryID, ProductID, GetAll);
        }

        public string AdminEnquiryResult(string ConnectTicket, int EnquiryResultID)
        {
            return _XDSConnectWS.AdminEnquiryResult(ConnectTicket, EnquiryResultID);
        }

        public System.Threading.Tasks.Task<string> AdminEnquiryResultAsync(string ConnectTicket, int EnquiryResultID)
        {
            return _XDSConnectWS.AdminEnquiryResultAsync(ConnectTicket, EnquiryResultID);
        }

        public string AdminEnquiryResultHistory(string ConnectTicket, int EnquiryID, int ProductID, bool GetAll, string username)
        {
            return _XDSConnectWS.AdminEnquiryResultHistory(ConnectTicket, EnquiryID, ProductID, GetAll, username);
        }

        public System.Threading.Tasks.Task<string> AdminEnquiryResultHistoryAsync(string ConnectTicket, int EnquiryID, int ProductID, bool GetAll, string username)
        {
            return _XDSConnectWS.AdminEnquiryResultHistoryAsync(ConnectTicket, EnquiryID, ProductID, GetAll, username);
        }

        public string AdminRequestHistory(string ConnectTicket, int ProductID, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.RequestStatus RequestStatus)
        {
            return _XDSConnectWS.AdminRequestHistory(ConnectTicket, ProductID, RequestStatus);
        }

        public System.Threading.Tasks.Task<string> AdminRequestHistoryAsync(string ConnectTicket, int ProductID, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.RequestStatus RequestStatus)
        {
            return _XDSConnectWS.AdminRequestHistoryAsync(ConnectTicket, ProductID, RequestStatus);
        }

        public XDS.ONLINE.REPOSITORY.XDSReportServiceRef.RequestFile AdminGetReportFile(string ConnectTicket, int SubscriberEnquiryLogID)
        {
            return _XDSConnectWS.AdminGetReportFile(ConnectTicket, SubscriberEnquiryLogID);
        }

        public System.Threading.Tasks.Task<XDS.ONLINE.REPOSITORY.XDSReportServiceRef.RequestFile> AdminGetReportFileAsync(string ConnectTicket, int SubscriberEnquiryLogID)
        {
            return _XDSConnectWS.AdminGetReportFileAsync(ConnectTicket, SubscriberEnquiryLogID);
        }

        public string ConnectFraudGetProfile(string ConnectTicket)
        {
            return _XDSConnectWS.ConnectFraudGetProfile(ConnectTicket);
        }

        public System.Threading.Tasks.Task<string> ConnectFraudGetProfileAsync(string ConnectTicket)
        {
            return _XDSConnectWS.ConnectFraudGetProfileAsync(ConnectTicket);
        }

        public string ConnectFraudConsumerMatch(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationSearchType SearchType, string BranchCode, int PurposeID, string IdNumber, string PassportNo, string CellularCode, string CellularNo, string AccountNo, string SubaccountNo, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectFraudConsumerMatch(ConnectTicket, SearchType, BranchCode, PurposeID, IdNumber, PassportNo, CellularCode, CellularNo, AccountNo, SubaccountNo, YourReference, VoucherCode);
        }

        public System.Threading.Tasks.Task<string> ConnectFraudConsumerMatchAsync(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationSearchType SearchType, string BranchCode, int PurposeID, string IdNumber, string PassportNo, string CellularCode, string CellularNo, string AccountNo, string SubaccountNo, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectFraudConsumerMatchAsync(ConnectTicket, SearchType, BranchCode, PurposeID, IdNumber, PassportNo, CellularCode, CellularNo, AccountNo, SubaccountNo, YourReference, VoucherCode);
        }

        public XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationProcess ConnectFraudGetQuestions(string ConnectTicket, int EnquiryID, int EnquiryResultID)
        {
            return _XDSConnectWS.ConnectFraudGetQuestions(ConnectTicket, EnquiryID, EnquiryResultID);
        }

        public System.Threading.Tasks.Task<XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationProcess> ConnectFraudGetQuestionsAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID)
        {
            return _XDSConnectWS.ConnectFraudGetQuestionsAsync(ConnectTicket, EnquiryID, EnquiryResultID);
        }

        public string ConnectGetHistoryReportAsync(string ConnectTicket, int EnquiryResultID)
        {
            return _XDSConnectWS.ConnectGetHistoryReport(ConnectTicket, EnquiryResultID);
        }

        public XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationProcess ConnectFraudProcess(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationProcessAction ProcesAction, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationProcess MyAuthenticationProcess, string Comment)
        {
            return _XDSConnectWS.ConnectFraudProcess(ConnectTicket, ProcesAction, MyAuthenticationProcess, Comment);
        }

        public System.Threading.Tasks.Task<XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationProcess> ConnectFraudProcessAsync(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationProcessAction ProcesAction, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationProcess MyAuthenticationProcess, string Comment)
        {
            return _XDSConnectWS.ConnectFraudProcessAsync(ConnectTicket, ProcesAction, MyAuthenticationProcess, Comment);
        }

        public string ConnectSendEmail(string ConnectTicket, int EnquiryResultID, string FromName, string ReplytoEmailIDs, string EmailToAddresses, string Emailbody)
        {
            return _XDSConnectWS.ConnectSendEmail(ConnectTicket, EnquiryResultID, FromName, ReplytoEmailIDs, EmailToAddresses, Emailbody);
        }

        public System.Threading.Tasks.Task<string> ConnectSendEmailAsync(string ConnectTicket, int EnquiryResultID, string FromName, string ReplytoEmailIDs, string EmailToAddresses, string Emailbody)
        {
            return _XDSConnectWS.ConnectSendEmailAsync(ConnectTicket, EnquiryResultID, FromName, ReplytoEmailIDs, EmailToAddresses, Emailbody);
        }

        public System.Data.DataSet ConnectCustomVettingA(
                    string ConnectTicket,
                    string Unique_ref,
                    int QueryNumber,
                    string EnquirerName,
                    string ContactPhone,
                    double EnquiryAmount,
                    int TransactionFlag1,
                    string Surname,
                    string Forename1,
                    string DateOfBirth,
                    string IDNumber1,
                    string Gender,
                    string ResAddressLine1,
                    string ResAddressSuburb,
                    string ResAddressCity,
                    string ResAddressPostCode,
                    string PrevResAddressLine1,
                    string PrevResAddressSuburb,
                    string PrevResAddressCity,
                    string HomePhoneNumber,
                    string WorkPhoneNumber,
                    double MonthlySalary,
                    string Username,
                    string Password)
        {
            return _XDSConnectWS.ConnectCustomVettingA(ConnectTicket, Unique_ref, QueryNumber, EnquirerName, ContactPhone, EnquiryAmount, TransactionFlag1, Surname, Forename1, DateOfBirth, IDNumber1, Gender, ResAddressLine1, ResAddressSuburb, ResAddressCity, ResAddressPostCode, PrevResAddressLine1, PrevResAddressSuburb, PrevResAddressCity, HomePhoneNumber, WorkPhoneNumber, MonthlySalary, Username, Password);
        }

        public System.Threading.Tasks.Task<System.Data.DataSet> ConnectCustomVettingAAsync(
                    string ConnectTicket,
                    string Unique_ref,
                    int QueryNumber,
                    string EnquirerName,
                    string ContactPhone,
                    double EnquiryAmount,
                    int TransactionFlag1,
                    string Surname,
                    string Forename1,
                    string DateOfBirth,
                    string IDNumber1,
                    string Gender,
                    string ResAddressLine1,
                    string ResAddressSuburb,
                    string ResAddressCity,
                    string ResAddressPostCode,
                    string PrevResAddressLine1,
                    string PrevResAddressSuburb,
                    string PrevResAddressCity,
                    string HomePhoneNumber,
                    string WorkPhoneNumber,
                    double MonthlySalary,
                    string Username,
                    string Password)
        {
            return _XDSConnectWS.ConnectCustomVettingAAsync(ConnectTicket, Unique_ref, QueryNumber, EnquirerName, ContactPhone, EnquiryAmount, TransactionFlag1, Surname, Forename1, DateOfBirth, IDNumber1, Gender, ResAddressLine1, ResAddressSuburb, ResAddressCity, ResAddressPostCode, PrevResAddressLine1, PrevResAddressSuburb, PrevResAddressCity, HomePhoneNumber, WorkPhoneNumber, MonthlySalary, Username, Password);
        }

        public string ConnectCustomVettingAString(
                    string ConnectTicket,
                    string Unique_ref,
                    int QueryNumber,
                    string EnquirerName,
                    string ContactPhone,
                    double EnquiryAmount,
                    int TransactionFlag1,
                    string Surname,
                    string Forename1,
                    string DateOfBirth,
                    string IDNumber1,
                    string Gender,
                    string ResAddressLine1,
                    string ResAddressSuburb,
                    string ResAddressCity,
                    string ResAddressPostCode,
                    string PrevResAddressLine1,
                    string PrevResAddressSuburb,
                    string PrevResAddressCity,
                    string HomePhoneNumber,
                    string WorkPhoneNumber,
                    double MonthlySalary,
                    string Username,
                    string Password)
        {
            return _XDSConnectWS.ConnectCustomVettingAString(ConnectTicket, Unique_ref, QueryNumber, EnquirerName, ContactPhone, EnquiryAmount, TransactionFlag1, Surname, Forename1, DateOfBirth, IDNumber1, Gender, ResAddressLine1, ResAddressSuburb, ResAddressCity, ResAddressPostCode, PrevResAddressLine1, PrevResAddressSuburb, PrevResAddressCity, HomePhoneNumber, WorkPhoneNumber, MonthlySalary, Username, Password);
        }

        public System.Threading.Tasks.Task<string> ConnectCustomVettingAStringAsync(
                    string ConnectTicket,
                    string Unique_ref,
                    int QueryNumber,
                    string EnquirerName,
                    string ContactPhone,
                    double EnquiryAmount,
                    int TransactionFlag1,
                    string Surname,
                    string Forename1,
                    string DateOfBirth,
                    string IDNumber1,
                    string Gender,
                    string ResAddressLine1,
                    string ResAddressSuburb,
                    string ResAddressCity,
                    string ResAddressPostCode,
                    string PrevResAddressLine1,
                    string PrevResAddressSuburb,
                    string PrevResAddressCity,
                    string HomePhoneNumber,
                    string WorkPhoneNumber,
                    double MonthlySalary,
                    string Username,
                    string Password)
        {
            return _XDSConnectWS.ConnectCustomVettingAStringAsync(ConnectTicket, Unique_ref, QueryNumber, EnquirerName, ContactPhone, EnquiryAmount, TransactionFlag1, Surname, Forename1, DateOfBirth, IDNumber1, Gender, ResAddressLine1, ResAddressSuburb, ResAddressCity, ResAddressPostCode, PrevResAddressLine1, PrevResAddressSuburb, PrevResAddressCity, HomePhoneNumber, WorkPhoneNumber, MonthlySalary, Username, Password);
        }

        public string ConnectCustomVettingBString(string inputXMLstring)
        {
            return _XDSConnectWS.ConnectCustomVettingBString(inputXMLstring);
        }

        public System.Threading.Tasks.Task<string> ConnectCustomVettingBStringAsync(string inputXMLstring)
        {
            return _XDSConnectWS.ConnectCustomVettingBStringAsync(inputXMLstring);
        }

        public System.Data.DataSet ConnectCustomVettingB(string inputXMLstring)
        {
            return _XDSConnectWS.ConnectCustomVettingB(inputXMLstring);
        }

        public System.Threading.Tasks.Task<System.Data.DataSet> ConnectCustomVettingBAsync(string inputXMLstring)
        {
            return _XDSConnectWS.ConnectCustomVettingBAsync(inputXMLstring);
        }

        public string ConnectCustomVettingC(
                    string ConnectTicket,
                    string IdNumber,
                    string PassportNo,
                    string FirstName,
                    string Surname,
                    string DateOfBirth,
                    string Gender,
                    string HomePhoneNumber,
                    string WorkPhoneNumber,
                    double MonthlySalary,
                    string BankAccountType,
                    string BankName,
                    string AccountHolderName,
                    string BranchCode,
                    string BankAccountNumber,
                    string CreditCardNumber,
                    string PostalCode,
                    int EmploymentDuration,
                    string YourReference,
                    string VoucherCode)
        {
            return _XDSConnectWS.ConnectCustomVettingC(ConnectTicket, IdNumber, PassportNo, FirstName, Surname, DateOfBirth, Gender, HomePhoneNumber, WorkPhoneNumber, MonthlySalary, BankAccountType, BankName, AccountHolderName, BranchCode, BankAccountNumber, CreditCardNumber, PostalCode, EmploymentDuration, YourReference, VoucherCode);
        }

        public System.Threading.Tasks.Task<string> ConnectCustomVettingCAsync(
                    string ConnectTicket,
                    string IdNumber,
                    string PassportNo,
                    string FirstName,
                    string Surname,
                    string DateOfBirth,
                    string Gender,
                    string HomePhoneNumber,
                    string WorkPhoneNumber,
                    double MonthlySalary,
                    string BankAccountType,
                    string BankName,
                    string AccountHolderName,
                    string BranchCode,
                    string BankAccountNumber,
                    string CreditCardNumber,
                    string PostalCode,
                    int EmploymentDuration,
                    string YourReference,
                    string VoucherCode)
        {
            return _XDSConnectWS.ConnectCustomVettingCAsync(ConnectTicket, IdNumber, PassportNo, FirstName, Surname, DateOfBirth, Gender, HomePhoneNumber, WorkPhoneNumber, MonthlySalary, BankAccountType, BankName, AccountHolderName, BranchCode, BankAccountNumber, CreditCardNumber, PostalCode, EmploymentDuration, YourReference, VoucherCode);
        }

        public XDS.ONLINE.REPOSITORY.XDSReportServiceRef.ConnectCustomVettingDResponseConnectCustomVettingDResult ConnectCustomVettingD(
                    string CompanyName,
                    string Branch,
                    string User,
                    string Password,
                    string RuleSet,
                    string Title,
                    string FirstName,
                    string SecondName,
                    string LastName,
                    string BirthDate,
                    string IDType,
                    string IDNumber,
                    string Gender,
                    string AddressLine1,
                    string AddressLine2,
                    string Suburb,
                    string City,
                    string PhysicalPostalCode,
                    string ProvinceCode,
                    string OwnerTenant,
                    string PostalAddressLine1,
                    string PostalAddressLine2,
                    string PostalSuburb,
                    string PostalCity,
                    string PostalCode,
                    string PostalProvinceCode,
                    string HomeTelephoneCode,
                    string HomeTelephoneNo,
                    string WorkTelephoneCode,
                    string WorkTelephoneNo,
                    string YearsatCurrentEmployer,
                    string BankAccountType,
                    string Income,
                    string SFID)
        {
            return _XDSConnectWS.ConnectCustomVettingD(CompanyName, Branch, User, Password, RuleSet, Title, FirstName, SecondName, LastName, BirthDate, IDType, IDNumber, Gender, AddressLine1, AddressLine2, Suburb, City, PhysicalPostalCode, ProvinceCode, OwnerTenant, PostalAddressLine1, PostalAddressLine2, PostalSuburb, PostalCity, PostalCode, PostalProvinceCode, HomeTelephoneCode, HomeTelephoneNo, WorkTelephoneCode, WorkTelephoneNo, YearsatCurrentEmployer, BankAccountType, Income, SFID);
        }

        public System.Threading.Tasks.Task<XDS.ONLINE.REPOSITORY.XDSReportServiceRef.ConnectCustomVettingDResponseConnectCustomVettingDResult> ConnectCustomVettingDAsync(
                    string CompanyName,
                    string Branch,
                    string User,
                    string Password,
                    string RuleSet,
                    string Title,
                    string FirstName,
                    string SecondName,
                    string LastName,
                    string BirthDate,
                    string IDType,
                    string IDNumber,
                    string Gender,
                    string AddressLine1,
                    string AddressLine2,
                    string Suburb,
                    string City,
                    string PhysicalPostalCode,
                    string ProvinceCode,
                    string OwnerTenant,
                    string PostalAddressLine1,
                    string PostalAddressLine2,
                    string PostalSuburb,
                    string PostalCity,
                    string PostalCode,
                    string PostalProvinceCode,
                    string HomeTelephoneCode,
                    string HomeTelephoneNo,
                    string WorkTelephoneCode,
                    string WorkTelephoneNo,
                    string YearsatCurrentEmployer,
                    string BankAccountType,
                    string Income,
                    string SFID)
        {
            return _XDSConnectWS.ConnectCustomVettingDAsync(CompanyName, Branch, User, Password, RuleSet, Title, FirstName, SecondName, LastName, BirthDate, IDType, IDNumber, Gender, AddressLine1, AddressLine2, Suburb, City, PhysicalPostalCode, ProvinceCode, OwnerTenant, PostalAddressLine1, PostalAddressLine2, PostalSuburb, PostalCity, PostalCode, PostalProvinceCode, HomeTelephoneCode, HomeTelephoneNo, WorkTelephoneCode, WorkTelephoneNo, YearsatCurrentEmployer, BankAccountType, Income, SFID);
        }

        public string ConnectCustomVettingE(string ConnectTicket, int ProductID, string IdNumber, string PassportNo, string FirstName, string Surname, string DateOfBirth, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectCustomVettingE(ConnectTicket, ProductID, IdNumber, PassportNo, FirstName, Surname, DateOfBirth, YourReference, VoucherCode);
        }

        public System.Threading.Tasks.Task<string> ConnectCustomVettingEAsync(string ConnectTicket, int ProductID, string IdNumber, string PassportNo, string FirstName, string Surname, string DateOfBirth, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectCustomVettingEAsync(ConnectTicket, ProductID, IdNumber, PassportNo, FirstName, Surname, DateOfBirth, YourReference, VoucherCode);
        }

        public string ConnectCustomVettingF(string Username, string Password, string IdNumber, int RequestNo, string CustomerName, string CustomerSurname, int RequestMSISDN, string ContactNo)
        {
            return _XDSConnectWS.ConnectCustomVettingF(Username, Password, IdNumber, RequestNo, CustomerName, CustomerSurname, RequestMSISDN, ContactNo);
        }

        public System.Threading.Tasks.Task<string> ConnectCustomVettingFAsync(string Username, string Password, string IdNumber, int RequestNo, string CustomerName, string CustomerSurname, int RequestMSISDN, string ContactNo)
        {
            return _XDSConnectWS.ConnectCustomVettingFAsync(Username, Password, IdNumber, RequestNo, CustomerName, CustomerSurname, RequestMSISDN, ContactNo);
        }

        public string ConnectCustomVettingG(string Username, string Password, string FirstName, string SecondName, string LastName, string IDNumber, string PassportNumber, double Income, string ExternalReference)
        {
            return _XDSConnectWS.ConnectCustomVettingG(Username, Password, FirstName, SecondName, LastName, IDNumber, PassportNumber, Income, ExternalReference);
        }

        public System.Threading.Tasks.Task<string> ConnectCustomVettingGAsync(string Username, string Password, string FirstName, string SecondName, string LastName, string IDNumber, string PassportNumber, double Income, string ExternalReference)
        {
            return _XDSConnectWS.ConnectCustomVettingGAsync(Username, Password, FirstName, SecondName, LastName, IDNumber, PassportNumber, Income, ExternalReference);
        }

        public string ConnectCustomVettingH(string ConnectTicket, int EnquiryID, int EnquiryResultID)
        {
            return _XDSConnectWS.ConnectCustomVettingH(ConnectTicket, EnquiryID, EnquiryResultID);
        }

        public System.Threading.Tasks.Task<string> ConnectCustomVettingHAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID)
        {
            return _XDSConnectWS.ConnectCustomVettingHAsync(ConnectTicket, EnquiryID, EnquiryResultID);
        }

        public string ConnectCustomVettingI(string ConnectTicket, int EnquiryID, int EnquiryResultID)
        {
            return _XDSConnectWS.ConnectCustomVettingI(ConnectTicket, EnquiryID, EnquiryResultID);
        }

        public System.Threading.Tasks.Task<string> ConnectCustomVettingIAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID)
        {
            return _XDSConnectWS.ConnectCustomVettingIAsync(ConnectTicket, EnquiryID, EnquiryResultID);
        }

        public string ConnectCustomVettingJ(string ConnectTicket, int EnquiryID, int EnquiryResultID)
        {
            return _XDSConnectWS.ConnectCustomVettingJ(ConnectTicket, EnquiryID, EnquiryResultID);
        }

        public System.Threading.Tasks.Task<string> ConnectCustomVettingJAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID)
        {
            return _XDSConnectWS.ConnectCustomVettingJAsync(ConnectTicket, EnquiryID, EnquiryResultID);
        }

        public string ConnectCustomVettingK(string ConnectTicket, string EnquiryReason, int ProductID, string ReferenceNumber, string ApplicantIDNumber, string ApplicantFirstname, string ApplicantSurname, string ApplicationDealerID, string ApplicationSubChannel, string ApplicationGroup, string ApplicationChannel, System.DateTime ApplicationDateTime)
        {
            return _XDSConnectWS.ConnectCustomVettingK(ConnectTicket, EnquiryReason, ProductID, ReferenceNumber, ApplicantIDNumber, ApplicantFirstname, ApplicantSurname, ApplicationDealerID, ApplicationSubChannel, ApplicationGroup, ApplicationChannel, ApplicationDateTime);
        }

        public System.Threading.Tasks.Task<string> ConnectCustomVettingKAsync(string ConnectTicket, string EnquiryReason, int ProductID, string ReferenceNumber, string ApplicantIDNumber, string ApplicantFirstname, string ApplicantSurname, string ApplicationDealerID, string ApplicationSubChannel, string ApplicationGroup, string ApplicationChannel, System.DateTime ApplicationDateTime)
        {
            return _XDSConnectWS.ConnectCustomVettingKAsync(ConnectTicket, EnquiryReason, ProductID, ReferenceNumber, ApplicantIDNumber, ApplicantFirstname, ApplicantSurname, ApplicationDealerID, ApplicationSubChannel, ApplicationGroup, ApplicationChannel, ApplicationDateTime);
        }

        public string ConnectCustomVettingL(string ConnectTicket, int EnquiryID, int EnquiryResultID)
        {
            return _XDSConnectWS.ConnectCustomVettingL(ConnectTicket, EnquiryID, EnquiryResultID);
        }

        public System.Threading.Tasks.Task<string> ConnectCustomVettingLAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID)
        {
            return _XDSConnectWS.ConnectCustomVettingLAsync(ConnectTicket, EnquiryID, EnquiryResultID);
        }


        public string ConnectCustomVettingN(string ConnectTicket, int ProductID, string ApplicantIDNo, string ApplicantFirstname, string ApplicantSurname, string ApplicantDateOfBirth, string ApplicantGrossIncome,
        string ApplicantMobileNo, string ApplicantAlternateNo, string ApplicantEmail, string ApplicationDealerID, string ApplicationPromotionDeal, string ApplicationPromotionCode, string ApplicantUser, DateTime ApplicationDateTime, string ApplicationPromotionDealValue)
        {
            ApplicantIDNo = ApplicantIDNo == null ? "" : ApplicantIDNo;
            ApplicantFirstname = ApplicantFirstname == null ? "" : ApplicantFirstname;
            ApplicantSurname = ApplicantSurname == null ? "" : ApplicantSurname;
            ApplicantIDNo = ApplicantDateOfBirth == null ? "" : ApplicantDateOfBirth;
            ApplicantGrossIncome = ApplicantGrossIncome == null ? "" : ApplicantGrossIncome;
            ApplicantMobileNo = ApplicantMobileNo == null ? "" : ApplicantMobileNo;
            ApplicantAlternateNo = ApplicantAlternateNo == null ? "" : ApplicantAlternateNo;
            ApplicantEmail = ApplicantEmail == null ? "" : ApplicantEmail;
            ApplicationDealerID = ApplicationDealerID == null ? "" : ApplicationDealerID;
            ApplicationPromotionDeal = ApplicationPromotionDeal == null ? "" : ApplicationPromotionDeal;
            ApplicationPromotionCode = ApplicationPromotionCode == null ? "" : ApplicationPromotionCode;

            string result = _XDSConnectWS.ConnectCustomVettingN(ConnectTicket, ProductID, ApplicantIDNo, ApplicantFirstname, ApplicantSurname, ApplicantDateOfBirth, ApplicantGrossIncome,
                ApplicantMobileNo, ApplicantAlternateNo, ApplicantEmail, ApplicationDealerID, ApplicationPromotionDeal, ApplicationPromotionCode, ApplicantUser, ApplicationDateTime, ApplicationPromotionDealValue);

            return result;

        }

        public System.Threading.Tasks.Task<string> ConnectCustomVettingNAsync(string ConnectTicket, int ProductID, string ApplicantIDNo, string ApplicantFirstname, string ApplicantSurname, string ApplicantDateOfBirth, string ApplicantGrossIncome,
        string ApplicantMobileNo, string ApplicantAlternateNo, string ApplicantEmail, string ApplicationDealerID, string ApplicationPromotionDeal, string ApplicationPromotionCode, string ApplicantUser, DateTime ApplicationDateTime, string ApplicationPromotionDealValue)
        {
            ApplicantIDNo = ApplicantIDNo == null ? "" : ApplicantIDNo;
            ApplicantFirstname = ApplicantFirstname == null ? "" : ApplicantFirstname;
            ApplicantSurname = ApplicantSurname == null ? "" : ApplicantSurname;
            ApplicantDateOfBirth = ApplicantDateOfBirth == null ? "" : ApplicantDateOfBirth;
            ApplicantGrossIncome = ApplicantGrossIncome == null ? "" : ApplicantGrossIncome;
            ApplicantMobileNo = ApplicantMobileNo == null ? "" : ApplicantMobileNo;
            ApplicantAlternateNo = ApplicantAlternateNo == null ? "" : ApplicantAlternateNo;
            ApplicantEmail = ApplicantEmail == null ? "" : ApplicantEmail;
            ApplicationDealerID = ApplicationDealerID == null ? "" : ApplicationDealerID;
            ApplicationPromotionDeal = ApplicationPromotionDeal == null ? "" : ApplicationPromotionDeal;
            ApplicationPromotionCode = ApplicationPromotionCode == null ? "" : ApplicationPromotionCode;

            return _XDSConnectWS.ConnectCustomVettingNAsync(ConnectTicket, ProductID, ApplicantIDNo, ApplicantFirstname, ApplicantSurname, ApplicantDateOfBirth, ApplicantGrossIncome,
                ApplicantMobileNo, ApplicantAlternateNo, ApplicantEmail, ApplicationDealerID, ApplicationPromotionDeal, ApplicationPromotionCode, ApplicantUser, ApplicationDateTime, ApplicationPromotionDealValue);
 
        }

        public string ConnectSMSAlert(string ConnectTicket, string MobileNo, string Message, string AlertType, int ReferenceID, int ProductId)
        {
            return _XDSConnectWS.ConnectSMSAlert(ConnectTicket, MobileNo, Message, AlertType, ReferenceID, ProductId);
        }

        public System.Threading.Tasks.Task<string> ConnectSMSAlertAsync(string ConnectTicket, string MobileNo, string Message, string AlertType, int ReferenceID, int ProductId)
        {
            return _XDSConnectWS.ConnectSMSAlertAsync(ConnectTicket, MobileNo, Message, AlertType, ReferenceID, ProductId);
        }

        public XDS.ONLINE.REPOSITORY.XDSReportServiceRef.ConnectBureauDataResponseConnectBureauDataResult ConnectBureauData(string IdNumber, string PassportNo, string FirstName, string Surname, string DateOfBirth, string YourReference, string VoucherCode, string Username, string Password)
        {
            return _XDSConnectWS.ConnectBureauData(IdNumber, PassportNo, FirstName, Surname, DateOfBirth, YourReference, VoucherCode, Username, Password);
        }

        public System.Threading.Tasks.Task<XDS.ONLINE.REPOSITORY.XDSReportServiceRef.ConnectBureauDataResponseConnectBureauDataResult> ConnectBureauDataAsync(string IdNumber, string PassportNo, string FirstName, string Surname, string DateOfBirth, string YourReference, string VoucherCode, string Username, string Password)
        {
            return _XDSConnectWS.ConnectBureauDataAsync(IdNumber, PassportNo, FirstName, Surname, DateOfBirth, YourReference, VoucherCode, Username, Password);
        }

        public string ConnectLoadConsumerDefaultAlert(
                    string ConnectTicket,
                    int SubscriberEnquiryID,
                    int SubscriberEnquiryResultID,
                    string Subscribername,
                    string AccountNo,
                    string SubAccountNumber,
                    double Amount,
                    string StatusCode,
                    string Accountype,
                    string Address1,
                    string Address2,
                    string Address3,
                    string Address4,
                    string Postalcode,
                    string WorkTelephone,
                    string Hometelephone,
                    string Mobile,
                    string EmailAddress,
                    string Effectivedate,
                    string Comments,
                    bool SMSNotification,
                    bool EmailNotification,
                    bool ClientResponsibleToNotify,
                    string ClientContactDetails,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AlertType DefaultAlertType,
                    bool ConsumerNotified)
        {
            return _XDSConnectWS.ConnectLoadConsumerDefaultAlert(ConnectTicket, SubscriberEnquiryID, SubscriberEnquiryResultID, Subscribername, AccountNo, SubAccountNumber, Amount, StatusCode, Accountype, Address1, Address2, Address3, Address4, Postalcode, WorkTelephone, Hometelephone, Mobile, EmailAddress, Effectivedate, Comments, SMSNotification, EmailNotification, ClientResponsibleToNotify, ClientContactDetails, DefaultAlertType, ConsumerNotified);
        }

        public System.Threading.Tasks.Task<string> ConnectLoadConsumerDefaultAlertAsync(
                    string ConnectTicket,
                    int SubscriberEnquiryID,
                    int SubscriberEnquiryResultID,
                    string Subscribername,
                    string AccountNo,
                    string SubAccountNumber,
                    double Amount,
                    string StatusCode,
                    string Accountype,
                    string Address1,
                    string Address2,
                    string Address3,
                    string Address4,
                    string Postalcode,
                    string WorkTelephone,
                    string Hometelephone,
                    string Mobile,
                    string EmailAddress,
                    string Effectivedate,
                    string Comments,
                    bool SMSNotification,
                    bool EmailNotification,
                    bool ClientResponsibleToNotify,
                    string ClientContactDetails,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AlertType DefaultAlertType,
                    bool ConsumerNotified)
        {
            return _XDSConnectWS.ConnectLoadConsumerDefaultAlertAsync(ConnectTicket, SubscriberEnquiryID, SubscriberEnquiryResultID, Subscribername, AccountNo, SubAccountNumber, Amount, StatusCode, Accountype, Address1, Address2, Address3, Address4, Postalcode, WorkTelephone, Hometelephone, Mobile, EmailAddress, Effectivedate, Comments, SMSNotification, EmailNotification, ClientResponsibleToNotify, ClientContactDetails, DefaultAlertType, ConsumerNotified);
        }

        public string ConnectGetConsumerDefaultAlertHistory(string ConnectTicket)
        {
            return _XDSConnectWS.ConnectGetConsumerDefaultAlertHistory(ConnectTicket);
        }

        public System.Threading.Tasks.Task<string> ConnectGetConsumerDefaultAlertHistoryAsync(string ConnectTicket)
        {
            return _XDSConnectWS.ConnectGetConsumerDefaultAlertHistoryAsync(ConnectTicket);
        }

        public string ConnectRemoveConsumerDefaultAlert(string ConnectTicket, int ConsumerDefaultAlertID, string ContactNo, string EmailAddress, bool SMSNotification, bool EmailNotification, bool ClientResponsibleToNotify, string ClientContactDetails)
        {
            return _XDSConnectWS.ConnectRemoveConsumerDefaultAlert(ConnectTicket, ConsumerDefaultAlertID, ContactNo, EmailAddress, SMSNotification, EmailNotification, ClientResponsibleToNotify, ClientContactDetails);
        }

        public System.Threading.Tasks.Task<string> ConnectRemoveConsumerDefaultAlertAsync(string ConnectTicket, int ConsumerDefaultAlertID, string ContactNo, string EmailAddress, bool SMSNotification, bool EmailNotification, bool ClientResponsibleToNotify, string ClientContactDetails)
        {
            return _XDSConnectWS.ConnectRemoveConsumerDefaultAlertAsync(ConnectTicket, ConsumerDefaultAlertID, ContactNo, EmailAddress, SMSNotification, EmailNotification, ClientResponsibleToNotify, ClientContactDetails);
        }

        public string ConnectMoveConsumerDefaultAlert(string ConnectTicket, int ConsumerDefaultAlertID, string ContactNo, string EmailAddress, bool SMSNotification, bool EmailNotification, bool ClientResponsibleToNotify, string ClientContactDetails)
        {
            return _XDSConnectWS.ConnectMoveConsumerDefaultAlert(ConnectTicket, ConsumerDefaultAlertID, ContactNo, EmailAddress, SMSNotification, EmailNotification, ClientResponsibleToNotify, ClientContactDetails);
        }

        public System.Threading.Tasks.Task<string> ConnectMoveConsumerDefaultAlertAsync(string ConnectTicket, int ConsumerDefaultAlertID, string ContactNo, string EmailAddress, bool SMSNotification, bool EmailNotification, bool ClientResponsibleToNotify, string ClientContactDetails)
        {
            return _XDSConnectWS.ConnectMoveConsumerDefaultAlertAsync(ConnectTicket, ConsumerDefaultAlertID, ContactNo, EmailAddress, SMSNotification, EmailNotification, ClientResponsibleToNotify, ClientContactDetails);
        }

        public string ConnectBusinessMatchTracePlus(string ConnectTicket, string Reg1, string Reg2, string Reg3, string BusinessName, string VatNo, string SolePropIDNo, string YourReference, string VoucherCode, int ProductID)
        {
            return _XDSConnectWS.ConnectBusinessMatchTracePlus(ConnectTicket, Reg1, Reg2, Reg3, BusinessName, VatNo, SolePropIDNo, YourReference, VoucherCode, ProductID);
        }

        public System.Threading.Tasks.Task<string> ConnectBusinessMatchTracePlusAsync(string ConnectTicket, string Reg1, string Reg2, string Reg3, string BusinessName, string VatNo, string SolePropIDNo, string YourReference, string VoucherCode, int ProductID)
        {
            return _XDSConnectWS.ConnectBusinessMatchTracePlusAsync(ConnectTicket, Reg1, Reg2, Reg3, BusinessName, VatNo, SolePropIDNo, YourReference, VoucherCode, ProductID);
        }

        public string ConnectLoadCommercialDefaultAlert(
                    string ConnectTicket,
                    int SubscriberEnquiryID,
                    int SubscriberEnquiryResultID,
                    string Subscribername,
                    string VatNo,
                    string Idno,
                    string PassportNo,
                    string SurName,
                    string FirstName,
                    string SecondName,
                    string ThirdName,
                    string Gender,
                    string BirthDate,
                    string AccountNo,
                    string SubAccountNo,
                    double Amount,
                    string StatusCode,
                    string AccountType,
                    string Address1,
                    string Address2,
                    string Address3,
                    string Address4,
                    string PostalCode,
                    string HometelephoneNo,
                    string WorktelephoneNo,
                    string MobileNo,
                    string EmailAddress,
                    string Comments,
                    string EffectiveDate,
                    bool SMSNotification,
                    bool EmailNotification,
                    bool ClientResponsibleToNotify,
                    string ClientContactDetails,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AlertType DefaultAlertType,
                    bool ConsumerNotified)
        {
            return _XDSConnectWS.ConnectLoadCommercialDefaultAlert(ConnectTicket, SubscriberEnquiryID, SubscriberEnquiryResultID, Subscribername, VatNo, Idno, PassportNo, SurName, FirstName, SecondName, ThirdName, Gender, BirthDate, AccountNo, SubAccountNo, Amount, StatusCode, AccountType, Address1, Address2, Address3, Address4, PostalCode, HometelephoneNo, WorktelephoneNo, MobileNo, EmailAddress, Comments, EffectiveDate, SMSNotification, EmailNotification, ClientResponsibleToNotify, ClientContactDetails, DefaultAlertType, ConsumerNotified);
        }

        public System.Threading.Tasks.Task<string> ConnectLoadCommercialDefaultAlertAsync(
                    string ConnectTicket,
                    int SubscriberEnquiryID,
                    int SubscriberEnquiryResultID,
                    string Subscribername,
                    string VatNo,
                    string Idno,
                    string PassportNo,
                    string SurName,
                    string FirstName,
                    string SecondName,
                    string ThirdName,
                    string Gender,
                    string BirthDate,
                    string AccountNo,
                    string SubAccountNo,
                    double Amount,
                    string StatusCode,
                    string AccountType,
                    string Address1,
                    string Address2,
                    string Address3,
                    string Address4,
                    string PostalCode,
                    string HometelephoneNo,
                    string WorktelephoneNo,
                    string MobileNo,
                    string EmailAddress,
                    string Comments,
                    string EffectiveDate,
                    bool SMSNotification,
                    bool EmailNotification,
                    bool ClientResponsibleToNotify,
                    string ClientContactDetails,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AlertType DefaultAlertType,
                    bool ConsumerNotified)
        {
            return _XDSConnectWS.ConnectLoadCommercialDefaultAlertAsync(ConnectTicket, SubscriberEnquiryID, SubscriberEnquiryResultID, Subscribername, VatNo, Idno, PassportNo, SurName, FirstName, SecondName, ThirdName, Gender, BirthDate, AccountNo, SubAccountNo, Amount, StatusCode, AccountType, Address1, Address2, Address3, Address4, PostalCode, HometelephoneNo, WorktelephoneNo, MobileNo, EmailAddress, Comments, EffectiveDate, SMSNotification, EmailNotification, ClientResponsibleToNotify, ClientContactDetails, DefaultAlertType, ConsumerNotified);
        }

        public string ConnectGetCommercialDefaultAlertHistory(string ConnectTicket)
        {
            return _XDSConnectWS.ConnectGetCommercialDefaultAlertHistory(ConnectTicket);
        }

        public System.Threading.Tasks.Task<string> ConnectGetCommercialDefaultAlertHistoryAsync(string ConnectTicket)
        {
            return _XDSConnectWS.ConnectGetCommercialDefaultAlertHistoryAsync(ConnectTicket);
        }

        public string ConnectRemoveCommercialDefaultAlert(string ConnectTicket, int CommercialDefaultAlertID, string ContactNo, string EmailAddress, bool SMSNotification, bool EmailNotification, bool ClientResponsibleToNotify, string ClientContactDetails)
        {
            return _XDSConnectWS.ConnectRemoveCommercialDefaultAlert(ConnectTicket, CommercialDefaultAlertID, ContactNo, EmailAddress, SMSNotification, EmailNotification, ClientResponsibleToNotify, ClientContactDetails);
        }

        public System.Threading.Tasks.Task<string> ConnectRemoveCommercialDefaultAlertAsync(string ConnectTicket, int CommercialDefaultAlertID, string ContactNo, string EmailAddress, bool SMSNotification, bool EmailNotification, bool ClientResponsibleToNotify, string ClientContactDetails)
        {
            return _XDSConnectWS.ConnectRemoveCommercialDefaultAlertAsync(ConnectTicket, CommercialDefaultAlertID, ContactNo, EmailAddress, SMSNotification, EmailNotification, ClientResponsibleToNotify, ClientContactDetails);
        }

        public string ConnectMoveCommercialDefaultAlert(string ConnectTicket, int CommercialDefaultAlertID, string ContactNo, string EmailAddress, bool SMSNotification, bool EmailNotification, bool ClientResponsibleToNotify, string ClientContactDetails)
        {
            return _XDSConnectWS.ConnectMoveCommercialDefaultAlert(ConnectTicket, CommercialDefaultAlertID, ContactNo, EmailAddress, SMSNotification, EmailNotification, ClientResponsibleToNotify, ClientContactDetails);
        }

        public System.Threading.Tasks.Task<string> ConnectMoveCommercialDefaultAlertAsync(string ConnectTicket, int CommercialDefaultAlertID, string ContactNo, string EmailAddress, bool SMSNotification, bool EmailNotification, bool ClientResponsibleToNotify, string ClientContactDetails)
        {
            return _XDSConnectWS.ConnectMoveCommercialDefaultAlertAsync(ConnectTicket, CommercialDefaultAlertID, ContactNo, EmailAddress, SMSNotification, EmailNotification, ClientResponsibleToNotify, ClientContactDetails);
        }

        public string ConnectGetDefaultAlertStatusCode(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.Listingtype TypeOfListing)
        {
            return _XDSConnectWS.ConnectGetDefaultAlertStatusCode(ConnectTicket, TypeOfListing);
        }

        public System.Threading.Tasks.Task<string> ConnectGetDefaultAlertStatusCodeAsync(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.Listingtype TypeOfListing)
        {
            return _XDSConnectWS.ConnectGetDefaultAlertStatusCodeAsync(ConnectTicket, TypeOfListing);
        }

        public string ConnectGetDefaultAlertAccountTypes(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.Listingtype TypeOfListing)
        {
            return _XDSConnectWS.ConnectGetDefaultAlertAccountTypes(ConnectTicket, TypeOfListing);
        }

        public System.Threading.Tasks.Task<string> ConnectGetDefaultAlertAccountTypesAsync(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.Listingtype TypeOfListing)
        {
            return _XDSConnectWS.ConnectGetDefaultAlertAccountTypesAsync(ConnectTicket, TypeOfListing);
        }

        public string ConnectGetTownships(string ConnectTicket, string DeedsOffice, string TownshipName)
        {
            return _XDSConnectWS.ConnectGetTownships(ConnectTicket, DeedsOffice, TownshipName);
        }

        public System.Threading.Tasks.Task<string> ConnectGetTownshipsAsync(string ConnectTicket, string DeedsOffice, string TownshipName)
        {
            return _XDSConnectWS.ConnectGetTownshipsAsync(ConnectTicket, DeedsOffice, TownshipName);
        }

        public string ConnectGetBankCodeResult(string ConnectTicket, int EnquiryLogID)
        {
            return _XDSConnectWS.ConnectGetBankCodeResult(ConnectTicket, EnquiryLogID);
        }

        public System.Threading.Tasks.Task<string> ConnectGetBankCodeResultAsync(string ConnectTicket, int EnquiryLogID)
        {
            return _XDSConnectWS.ConnectGetBankCodeResultAsync(ConnectTicket, EnquiryLogID);
        }

        public string ConnectNLRLoanRegistration(
                    string ConnectTicket,
                    int ProductID,
                    string SubscriberCode,
                    string SupplierRefNo,
                    string EnquiryRefNo,
                    string IdNumber,
                    string PassportNo,
                    string Birthdate,
                    string Surname,
                    string Forename1,
                    string Forename2,
                    string Forename3,
                    string title,
                    string MaidenName,
                    string AliasName,
                    string Gender,
                    string MaritalStatus,
                    string SpouseCurrentSurname,
                    string SpouseForename1,
                    string SpouseForename2,
                    string ResAddress1,
                    string ResAddress2,
                    string ResAddress3,
                    string ResAddress4,
                    string ResPostalCode,
                    string PeriodAtCurretAddress,
                    string OthAddress1,
                    string OthAddress2,
                    string OthAddress3,
                    string OthAddress4,
                    string OthPostalCode,
                    string OwnerTenant,
                    string Occupation,
                    string EmployerType,
                    string Employer,
                    string EmployeeNumber,
                    string PaySlipReferenceNo,
                    string HomeTelCode,
                    string HomeTelNo,
                    string WorkTelCode,
                    string WorkTelNo,
                    string CellNo,
                    string MonthlySalary,
                    string SalaryFrequency,
                    string BranchCode,
                    string Accno,
                    string SubAccno,
                    string DateDisbursed,
                    string Loantype,
                    string LoadInd,
                    string LoanAmount,
                    string LoanAmtBalInd,
                    string CurrentBalance,
                    string currbalanceInd,
                    string InstallmentAmt,
                    string RepaymentPeriod,
                    string LoanPurpose,
                    string TotalAmountRepayable,
                    string InterestType,
                    string AnnualRateForTotalChargeOfCredit,
                    string RandValueOfInterestCharges,
                    string RandValueOfTotalChargeOfCredit,
                    string SettlementAmount,
                    string VoucherCode,
                    string ExternalReference)
        {
            return _XDSConnectWS.ConnectNLRLoanRegistration(ConnectTicket, ProductID, SubscriberCode, SupplierRefNo, EnquiryRefNo, IdNumber, PassportNo, Birthdate, Surname, Forename1, Forename2, Forename3, title, MaidenName, AliasName, Gender, MaritalStatus, SpouseCurrentSurname, SpouseForename1, SpouseForename2, ResAddress1, ResAddress2, ResAddress3, ResAddress4, ResPostalCode, PeriodAtCurretAddress, OthAddress1, OthAddress2, OthAddress3, OthAddress4, OthPostalCode, OwnerTenant, Occupation, EmployerType, Employer, EmployeeNumber, PaySlipReferenceNo, HomeTelCode, HomeTelNo, WorkTelCode, WorkTelNo, CellNo, MonthlySalary, SalaryFrequency, BranchCode, Accno, SubAccno, DateDisbursed, Loantype, LoadInd, LoanAmount, LoanAmtBalInd, CurrentBalance, currbalanceInd, InstallmentAmt, RepaymentPeriod, LoanPurpose, TotalAmountRepayable, InterestType, AnnualRateForTotalChargeOfCredit, RandValueOfInterestCharges, RandValueOfTotalChargeOfCredit, SettlementAmount, VoucherCode, ExternalReference);
        }

        public System.Threading.Tasks.Task<string> ConnectNLRLoanRegistrationAsync(
                    string ConnectTicket,
                    int ProductID,
                    string SubscriberCode,
                    string SupplierRefNo,
                    string EnquiryRefNo,
                    string IdNumber,
                    string PassportNo,
                    string Birthdate,
                    string Surname,
                    string Forename1,
                    string Forename2,
                    string Forename3,
                    string title,
                    string MaidenName,
                    string AliasName,
                    string Gender,
                    string MaritalStatus,
                    string SpouseCurrentSurname,
                    string SpouseForename1,
                    string SpouseForename2,
                    string ResAddress1,
                    string ResAddress2,
                    string ResAddress3,
                    string ResAddress4,
                    string ResPostalCode,
                    string PeriodAtCurretAddress,
                    string OthAddress1,
                    string OthAddress2,
                    string OthAddress3,
                    string OthAddress4,
                    string OthPostalCode,
                    string OwnerTenant,
                    string Occupation,
                    string EmployerType,
                    string Employer,
                    string EmployeeNumber,
                    string PaySlipReferenceNo,
                    string HomeTelCode,
                    string HomeTelNo,
                    string WorkTelCode,
                    string WorkTelNo,
                    string CellNo,
                    string MonthlySalary,
                    string SalaryFrequency,
                    string BranchCode,
                    string Accno,
                    string SubAccno,
                    string DateDisbursed,
                    string Loantype,
                    string LoadInd,
                    string LoanAmount,
                    string LoanAmtBalInd,
                    string CurrentBalance,
                    string currbalanceInd,
                    string InstallmentAmt,
                    string RepaymentPeriod,
                    string LoanPurpose,
                    string TotalAmountRepayable,
                    string InterestType,
                    string AnnualRateForTotalChargeOfCredit,
                    string RandValueOfInterestCharges,
                    string RandValueOfTotalChargeOfCredit,
                    string SettlementAmount,
                    string VoucherCode,
                    string ExternalReference)
        {
            return _XDSConnectWS.ConnectNLRLoanRegistrationAsync(ConnectTicket, ProductID, SubscriberCode, SupplierRefNo, EnquiryRefNo, IdNumber, PassportNo, Birthdate, Surname, Forename1, Forename2, Forename3, title, MaidenName, AliasName, Gender, MaritalStatus, SpouseCurrentSurname, SpouseForename1, SpouseForename2, ResAddress1, ResAddress2, ResAddress3, ResAddress4, ResPostalCode, PeriodAtCurretAddress, OthAddress1, OthAddress2, OthAddress3, OthAddress4, OthPostalCode, OwnerTenant, Occupation, EmployerType, Employer, EmployeeNumber, PaySlipReferenceNo, HomeTelCode, HomeTelNo, WorkTelCode, WorkTelNo, CellNo, MonthlySalary, SalaryFrequency, BranchCode, Accno, SubAccno, DateDisbursed, Loantype, LoadInd, LoanAmount, LoanAmtBalInd, CurrentBalance, currbalanceInd, InstallmentAmt, RepaymentPeriod, LoanPurpose, TotalAmountRepayable, InterestType, AnnualRateForTotalChargeOfCredit, RandValueOfInterestCharges, RandValueOfTotalChargeOfCredit, SettlementAmount, VoucherCode, ExternalReference);
        }

        public string ConnectGetCreditReport(string ConnectTicket, string EnquiryReason, int ProductId, string IdNumber, string PassportNo, string FirstName, string Surname, string BirthDate, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectGetCreditReport(ConnectTicket, EnquiryReason, ProductId, IdNumber, PassportNo, FirstName, Surname, BirthDate, YourReference, VoucherCode);
        }

        public System.Threading.Tasks.Task<string> ConnectGetCreditReportAsync(string ConnectTicket, string EnquiryReason, int ProductId, string IdNumber, string PassportNo, string FirstName, string Surname, string BirthDate, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectGetCreditReportAsync(ConnectTicket, EnquiryReason, ProductId, IdNumber, PassportNo, FirstName, Surname, BirthDate, YourReference, VoucherCode);
        }

        public string ConnectIDPhotoVerification(string ConnectTicket, string IdNumber, string FirstName, string Surname, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectIDPhotoVerification(ConnectTicket, IdNumber, FirstName, Surname, YourReference, VoucherCode);
        }

        public System.Threading.Tasks.Task<string> ConnectIDPhotoVerificationAsync(string ConnectTicket, string IdNumber, string FirstName, string Surname, string YourReference, string VoucherCode)
        {
            return _XDSConnectWS.ConnectIDPhotoVerificationAsync(ConnectTicket, IdNumber, FirstName, Surname, YourReference, VoucherCode);
        }


        public string ConnectFraudConsumerMatchWithOverrideOTP(
                    string ConnectTicket,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationSearchType SearchType,
                    string BranchCode,
                    int PurposeID,
                    string IdNumber,
                    string PassportNo,
                    string CellularCode,
                    string CellularNo,
                    string AccountNo,
                    string SubaccountNo,
                    bool OverrideOTP,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.OverrideOTPReasons OverrideOTPReason,
                    string OverrideOTPComment,
                    string EmailAddress,
                    string YourReference,
                    string VoucherCode)
        {
            return _XDSConnectWS.ConnectFraudConsumerMatchWithOverrideOTP(ConnectTicket, SearchType, BranchCode, PurposeID, IdNumber, PassportNo, CellularCode, CellularNo, AccountNo, SubaccountNo, OverrideOTP, OverrideOTPReason, OverrideOTPComment, EmailAddress, YourReference, VoucherCode);
        }

        public System.Threading.Tasks.Task<string> ConnectFraudConsumerMatchWithOverrideOTPAsync(
                    string ConnectTicket,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationSearchType SearchType,
                    string BranchCode,
                    int PurposeID,
                    string IdNumber,
                    string PassportNo,
                    string CellularCode,
                    string CellularNo,
                    string AccountNo,
                    string SubaccountNo,
                    bool OverrideOTP,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.OverrideOTPReasons OverrideOTPReason,
                    string OverrideOTPComment,
                    string EmailAddress,
                    string YourReference,
                    string VoucherCode)
        {
            return _XDSConnectWS.ConnectFraudConsumerMatchWithOverrideOTPAsync(ConnectTicket, SearchType, BranchCode, PurposeID, IdNumber, PassportNo, CellularCode, CellularNo, AccountNo, SubaccountNo, OverrideOTP, OverrideOTPReason, OverrideOTPComment, EmailAddress, YourReference, VoucherCode);
        }

      
    }
}
