﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDS.ONLINE.REPOSITORY
{
    public interface IXDSAuthRepository
    {

        string ConnectFraudConsumerMatch(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.AuthenticationSearchType SearchType, string BranchCode, int PurposeID, string IdNumber, string PassportNo, string CellularCode, string CellularNo, string AccountNo, bool OverrideOTP, XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.OverrideOTPReasons OverrideOTPReason, string OverrideOTPComment, string SubaccountNo, string EmailAddress, string YourReference, string VoucherCode);

        XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.AuthenticationProcess ConnectFraudGetQuestions(string ConnectTicket, int EnquiryID, int EnquiryResultID);

        XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.AuthenticationProcess ConnectFraudProcess(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.AuthenticationProcessAction ProcessAction, XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.AuthenticationProcess MyAuthenticationProcess, string Comment);

        string ConnectFraudSavePersonalQuestions(string ConnectTicket, XDSConnectAuthWS.AuthenticationProcess MyAuthenticationProcess, string Comment);

        string ConnectFraudGetProfile(string ConnectTicket);
        //System.Threading.Tasks.Task<string> ConnectFraudGetProfileAsync(string ConnectTicket);

        bool IsTicketValid(string XDSConnectTicket);

        string Login(string strUser, string strPwd);

              
        string ConnectFraudGetBlockedConsumers(string ConnectTicket);

        string ConnectFraudUnBlockConsumer(string ConnectTicket, int BlockID, string Comment);

        //Authentication V2.2
        string ConnectFraudGetBonusSegments(string ConnectTicket, int EnquiryResultID);

        string ConnectFraudGetUnblockReasons(string ConnectTicket);

        string ConnectFraudUnblockConsumerWithReason(string ConnectTicket, int BlockID, string UnblockReason, string Comment);

        XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.ConsumerInfoDocument ConnectFraudGetConsumerInfo(string ConnectTicket, long ConsumerID);

        string ConnectFraudUpdateConsumerInfo(string ConnectTicket, long ConsumerID, int EnquiryID, int EnquiryResultID, string Address1, string Address2, string Address3, string Address4, string PostalCode);

        string ConnectFraudGetAuthenticationOTPNumbers(string ConnectTicket, long SubscriberAuthenticationID);

        XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.AuthenticationProcess ConnectFraudResendOTP(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.AuthenticationProcess MyAuthenticationProcess, int EnquiryID, int EnquiryResultID);

        string ConnectFraudGetScore(string ConnectTicket,
            XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.FraudScoreTitle FraudScoreTitle, XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.FraudScoreChannel FraudScoreChannel, XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.FraudScoreStore FraudScoreStore,
            int Salary, int MonthsEmployed, int EnquiryID, int EnquiryResultID);

        string ConnectFraudGetIDPhoto(string ConnectTicket, int EnquiryID, int EnquiryResultID, string BonusXML);

        string ConnectFraudGetCreditReport(string ConnectTicket, int EnquiryID, int EnquiryResultID, string BonusXML);

        string ConnectFraudAccountVerification(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.TypeofVerificationenum VerificationType, string Initials, string SurName, string AccNo, string BranchCode,
            string Acctype, string BankName, int EnquiryID, int EnquiryResultID);

        string ConnectFraudGetAccountVerificationResult(string ConnectTicket, int EnquiryLogID);

        string AuthenticateConnectFraudCheckBlockingStatus(string ConnectTicket, int EnquiryID, int EnquiryResultID);
    }
}
