﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;


namespace XDS.ONLINE.REPOSITORY
{
    public class XDSAuthRepository : IXDSAuthRepository
    {
        private XDSConnectAuthWS.XDSConnectAuthWSSoapClient _XDSConnectAuthWS;

        public XDSAuthRepository(){
            _XDSConnectAuthWS = new XDSConnectAuthWS.XDSConnectAuthWSSoapClient();
        }

        ~XDSAuthRepository()
        {
            //TODO: Error Logging
            try
            {
                _XDSConnectAuthWS.Close();
                _XDSConnectAuthWS = null;
            }
            catch (CommunicationException e)
            {
                logerror(e);
                _XDSConnectAuthWS.Abort();
            }
            catch (TimeoutException e)
            {
                logerror(e);
                _XDSConnectAuthWS.Abort();
            }
            catch (Exception e)
            {
                logerror(e);
                _XDSConnectAuthWS.Abort();
                throw;
            }
   
        }

        private void logerror(Exception e)
        {
            //TODO: Error Logging
        }

        public string ConnectFraudConsumerMatch(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.AuthenticationSearchType SearchType, string BranchCode, int PurposeID, string IdNumber, string PassportNo, string CellularCode, string CellularNo, string AccountNo, bool OverrideOTP, XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.OverrideOTPReasons OverrideOTPReason, string OverrideOTPComment, string SubaccountNo, string EmailAddress, string YourReference, string VoucherCode)
        {
            //return _XDSConnectAuthWS.ConnectFraudConsumerMatch(ConnectTicket, SearchType, BranchCode, PurposeID, IdNumber, PassportNo, CellularCode, CellularNo, AccountNo, OverrideOTP, OverrideOTPReason, OverrideOTPComment, SubaccountNo, EmailAddress, YourReference, VoucherCode);
            return _XDSConnectAuthWS.ConnectFraudConsumerMatchWithOverrideOTP(ConnectTicket, SearchType, BranchCode, PurposeID, IdNumber, PassportNo, CellularCode, CellularNo, AccountNo, OverrideOTP, OverrideOTPReason, OverrideOTPComment, SubaccountNo, EmailAddress, YourReference, VoucherCode);
        }

        public string ConnectFraudGetProfile(string ConnectTicket)
        {
            return _XDSConnectAuthWS.ConnectFraudGetProfileSettings(ConnectTicket);
        }

        public XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.AuthenticationProcess ConnectFraudGetQuestions(string ConnectTicket, int EnquiryID, int EnquiryResultID)
        {
            return _XDSConnectAuthWS.ConnectFraudGetQuestions(ConnectTicket, EnquiryID, EnquiryResultID);
        }

        public XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.AuthenticationProcess ConnectFraudProcess(string ConnectTicket, XDSConnectAuthWS.AuthenticationProcessAction ProcessAction, XDSConnectAuthWS.AuthenticationProcess MyAuthenticationProcess, string Comment)
        {
            return _XDSConnectAuthWS.ConnectFraudProcess(ConnectTicket, ProcessAction, MyAuthenticationProcess, Comment);
        }

        public string ConnectFraudSavePersonalQuestions(string ConnectTicket, XDSConnectAuthWS.AuthenticationProcess MyAuthenticationProcess, string Comment)
        {
            return _XDSConnectAuthWS.ConnectFraudSavePersonalQuestions(ConnectTicket, MyAuthenticationProcess, Comment);
        }

        public bool IsTicketValid(string XDSConnectTicket)
        {
            return _XDSConnectAuthWS.IsTicketValid(XDSConnectTicket);
        }

        public string Login(string strUser, string strPwd)
        {
            return _XDSConnectAuthWS.Login(strUser, strPwd);
        }


        

        public string ConnectFraudGetBlockedConsumers(string ConnectTicket)
        {
            return _XDSConnectAuthWS.ConnectFraudGetBlockedConsumers(ConnectTicket);
        }




        public string ConnectFraudUnBlockConsumer(string ConnectTicket, int BlockID, string Comment)
        {
            return _XDSConnectAuthWS.ConnectFraudUnBlockConsumer(ConnectTicket, BlockID, Comment);
        }


        //Authentication V2.2
        public string ConnectFraudGetBonusSegments(string ConnectTicket, int EnquiryResultID)
        {
            return _XDSConnectAuthWS.ConnectFraudGetBonusSegments(ConnectTicket, EnquiryResultID);
        }

        public string ConnectFraudGetUnblockReasons(string ConnectTicket)
        {
            return _XDSConnectAuthWS.ConnectFraudGetUnblockReasons(ConnectTicket);
        }

        public string ConnectFraudUnblockConsumerWithReason(string ConnectTicket, int BlockID, string UnblockReason, string Comment)
        {
            return _XDSConnectAuthWS.ConnectFraudUnblockConsumerWithReason(ConnectTicket, BlockID, UnblockReason, Comment);
        }

        public XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.ConsumerInfoDocument ConnectFraudGetConsumerInfo(string ConnectTicket, long ConsumerID)
        {
            return _XDSConnectAuthWS.ConnectFraudGetConsumerInfo(ConnectTicket, ConsumerID);
        }

        public string ConnectFraudUpdateConsumerInfo(string ConnectTicket, long ConsumerID, int EnquiryID, int EnquiryResultID, string Address1, string Address2, string Address3, string Address4, string PostalCode)
        {
            XDSConnectAuthWS.ConsumerInfoDocument oConsumerInfoDocument = new XDSConnectAuthWS.ConsumerInfoDocument();

            oConsumerInfoDocument.ConsumerID = ConsumerID;
            oConsumerInfoDocument.Address1 = Address1;
            oConsumerInfoDocument.Address2 = Address2;
            oConsumerInfoDocument.Address3 = Address3;
            oConsumerInfoDocument.Address4 = Address4;
            oConsumerInfoDocument.PostalCode = PostalCode;

            return _XDSConnectAuthWS.ConnectFraudUpdateConsumerInfo(ConnectTicket, EnquiryID, EnquiryResultID, oConsumerInfoDocument);
        }

        public string ConnectFraudGetAuthenticationOTPNumbers(string ConnectTicket, long SubscriberAuthenticationID)
        {
            return _XDSConnectAuthWS.ConnectFraudGetAuthenticationOTPNumbers(ConnectTicket, SubscriberAuthenticationID);
        }

        public XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.AuthenticationProcess ConnectFraudResendOTP(string ConnectTicket, XDSConnectAuthWS.AuthenticationProcess MyAuthenticationProcess, int EnquiryID, int EnquiryResultID)
        {
            return _XDSConnectAuthWS.ConnectFraudResendOTP(ConnectTicket, MyAuthenticationProcess, EnquiryID, EnquiryResultID);
        }

        public string ConnectFraudGetScore(string ConnectTicket,
            XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.FraudScoreTitle FraudScoreTitle, XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.FraudScoreChannel FraudScoreChannel, XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.FraudScoreStore FraudScoreStore,
            int Salary, int MonthsEmployed, int EnquiryID, int EnquiryResultID)
        {
            return _XDSConnectAuthWS.ConnectFraudGetScore(ConnectTicket, FraudScoreTitle, FraudScoreChannel, FraudScoreStore, Salary, MonthsEmployed, EnquiryID, EnquiryResultID);
        }

        public string ConnectFraudGetIDPhoto(string ConnectTicket, int EnquiryID, int EnquiryResultID, string BonusXML)
        {
            return _XDSConnectAuthWS.ConnectFraudGetIDPhoto(ConnectTicket, EnquiryID, EnquiryResultID, BonusXML);
        }

        public string ConnectFraudGetCreditReport(string ConnectTicket, int EnquiryID, int EnquiryResultID, string BonusXML)
        {
            return _XDSConnectAuthWS.ConnectFraudGetCreditReport(ConnectTicket, EnquiryID, EnquiryResultID, BonusXML);
        }

        public string ConnectFraudAccountVerification(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.TypeofVerificationenum VerificationType, string Initials, string SurName, string AccNo, string BranchCode,
            string Acctype, string BankName, int EnquiryID, int EnquiryResultID)
        {
            return _XDSConnectAuthWS.ConnectFraudAccountVerification(ConnectTicket, VerificationType, Initials, SurName, AccNo, BranchCode, Acctype, BankName, EnquiryID, EnquiryResultID);
        }

        public string ConnectFraudGetAccountVerificationResult(string ConnectTicket, int EnquiryLogID)
        {
            return _XDSConnectAuthWS.ConnectFraudGetAccountVerificationResult(ConnectTicket, EnquiryLogID);
        }

        public string AuthenticateConnectFraudCheckBlockingStatus(string ConnectTicket, int EnquiryID, int EnquiryResultID)
        {
            return _XDSConnectAuthWS.ConnectFraudCheckBlockingStatus(ConnectTicket, EnquiryID, EnquiryResultID);
        }
    }
}
