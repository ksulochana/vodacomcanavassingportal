﻿using System;

namespace XDS.ONLINE.REPOSITORY
{
    public interface IXDSCustomVettingRepository
    {
        bool CustomVettingIsTicketValid(string XDSConnectTicket);
        System.Threading.Tasks.Task<bool> CustomVettingIsTicketValidAsync(string XDSConnectTicket);
        string CustomVettingLogin(string strUser, string strPwd);
        System.Threading.Tasks.Task<string> CustomVettingLoginAsync(string strUser, string strPwd);
        string CustomVettingAdminSystemUser(string XDSConnectTicket);
        System.Threading.Tasks.Task<string> CustomVettingAdminSystemUserAsync(string XDSConnectTicket);
        string GetTransaction(string ticket, long consumerID);
        string ChangeTransactionStatus(string ticket, long transactionID, int transactionStatus);
        string CustomVettingGetTransactionDetails(string XDSConnectTicket);
        string CustomVettingGetTransactionHistory(string XDSConnectTicket, string User, string IDNo, string ReferenceNo, string Status, DateTime StartDate, DateTime EndDate, bool GetAll);
        System.Threading.Tasks.Task<string> CustomVettingGetTransactionHistoryAsync(string XDSConnectTicket, string User, string IDNo, string ReferenceNo, string Status, DateTime StartDate, DateTime EndDate, bool GetAll);
        string GetTransactionEnquiry(string ticket, long EnquiryID);
        System.Threading.Tasks.Task<string> GetTransactionEnquiryAsync(string ticket, long EnquiryID);
        string GetBulletins(string XDSConnectTicket);
        System.Threading.Tasks.Task<string> GetBulletinsAsync(string XDSConnectTicket);
        string GetBulletinFile(string XDSConnectTicket, long BulletinID);
        System.Threading.Tasks.Task<string> GetBulletinFileAsync(string XDSConnectTicket, long BulletinID);
        string GetPromotionDeals(string XDSConnectTicket, long PromotionDealID);
        string GetPromotionDealsByPortfolio(string XDSConnectTicket, string Portfolio);
        string GetPortfoliosByPromotionDeal(string XDSConnectTicket, long PromotionDealID);
        System.Threading.Tasks.Task<string> GetPromotionDealsAsync(string XDSConnectTicket, long PromotionDealID);
        System.Threading.Tasks.Task<string> GetPromotionDealsByPortfolioAsync(string XDSConnectTicket, string Portfolio);
        System.Threading.Tasks.Task<string> GetPortfoliosByPromotionDealAsync(string XDSConnectTicket, long PromotionDealID);
        string GetSMSConfig(string XDSConnectTicket);
        System.Threading.Tasks.Task<string> GetSMSConfigAsync(string XDSConnectTicket);
        string LogSMS(string XDSConnectTicket, long EnquiryID, long QueueID);
        System.Threading.Tasks.Task<string> LogSMSAsync(string XDSConnectTicket, long EnquiryID, long QueueID);
        string LogTransaction(string XDSConnectTicket, string ApplicationPromotionDeal, string ApplicationDealerID, string ApplicationPromotionCode, string ApplicantFirstname,
                                string ApplicantSurname, string ApplicantBirthDate, string ApplicantIDNumber, string ApplicantGrossIncome, string ApplicantMobileNo,
                                string ApplicantAlternateNo, string ApplicantEmail, string ReferenceNumber, string Score, string PreScoringReasonCode, string DecisionDescription,
                                string Portfolio, string ScoreDate, string ReasonDescription, string CreatedByUser, string ConsumerID, string PredictedIncome, string PredictedAvailableInstalment, string Accountno, string SourceInd, string VodacomResponse, string VodacomResponseType, string EmailStatus, float Latitude, float Longitude, string City);
        System.Threading.Tasks.Task<string> LogTransactionAsync(string XDSConnectTicket, string ApplicationPromotionDeal, string ApplicationDealerID, string ApplicationPromotionCode, string ApplicantFirstname,
                                string ApplicantSurname, string ApplicantBirthDate, string ApplicantIDNumber, string ApplicantGrossIncome, string ApplicantMobileNo,
                                string ApplicantAlternateNo, string ApplicantEmail, string ReferenceNumber, string Score, string PreScoringReasonCode, string DecisionDescription,
                                string Portfolio, string ScoreDate, string ReasonDescription, string CreatedByUser, string ConsumerID, string PredictedIncome, string PredictedAvailableInstalment, string Accountno, string SourceInd, string VodacomResponse, string VodacomResponseType, string EmailStatus, float Latitude, float Longitude, string City);

        System.Threading.Tasks.Task<string> AdminChangePasswordAsync(string ConnectTicket, string CurrentPassword, string NewPassword, string ConfirmPassword);
        System.Threading.Tasks.Task<string> AdminChangeUserDetailsAsync(string ConnectTicket, string FirstName, string Surname, string Email, string JobTitle, string JobPosition, XDS.ONLINE.REPOSITORY.XDSCustomVetting.UserRole Role, bool isActive);

        bool CheckifUserExists(string XDSConnectTicket, string username, string cellularnumber);
        System.Threading.Tasks.Task<bool> CheckifUserExistsAsync(string XDSConnectTicket, string username, string cellularnumber);

        string GenerateResetPasswordCode(string XDSConnectTicket, string username, string cellularnumber, string issueduser);
        System.Threading.Tasks.Task<string> GenerateResetPasswordCodeAsync(string XDSConnectTicket, string username, string cellularnumber,  string issueduser);

        string GetPasswordResetCodeStatus(string XDSConnectTicket, string username, string cellularnumber, string link);
        System.Threading.Tasks.Task<string> GetPasswordResetCodeStatusAsync(string XDSConnectTicket, string username, string cellularnumber, string link);

        System.Threading.Tasks.Task<string> AdminResetPasswordAsync(string ConnectTicket, string Username, string Cellnumber, string NewPassword, string ConfirmPassword,string Code);

        string GetTrainingDoc(string XDSConnectTicket);
        System.Threading.Tasks.Task<string> GetTrainingDocAsync(string XDSConnectTicket);
        string GetTrainingDocFile(string XDSConnectTicket, long BulletinID);
        System.Threading.Tasks.Task<string> GetTrainingDocFileAsync(string XDSConnectTicket, long BulletinID);

    }
}
