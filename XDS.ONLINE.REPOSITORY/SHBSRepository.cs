﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace XDS.ONLINE.REPOSITORY
{
    public class SHBSRepository : XDS.ONLINE.REPOSITORY.ISHBSRepository
    {
        private XDSNewProductReportServiceRef.XDSConnectWS3SoapClient _XDSSHBSConnectWS;

        public SHBSRepository()
        {
         _XDSSHBSConnectWS = new XDSNewProductReportServiceRef.XDSConnectWS3SoapClient();
        }

        ~SHBSRepository()
        {
            //TODO: Error Logging
            try
            {
                _XDSSHBSConnectWS.Close();
                _XDSSHBSConnectWS = null;
            }
            catch (CommunicationException e)
            {
                logerror(e);
                _XDSSHBSConnectWS.Abort();
            }
            catch (TimeoutException e)
            {
                logerror(e);
                _XDSSHBSConnectWS.Abort();
            }
            catch (Exception e)
            {
                logerror(e);
                _XDSSHBSConnectWS.Abort();
                throw;
            }
        }

        private void logerror(Exception e)
        {
            //TODO: Error Logging
        }

        public string Login(string strUser, string strPwd)
        {
            return _XDSSHBSConnectWS.Login(strUser, strPwd);
        }
        public bool IsTicketValid(string XDSConnectTicket)
        {
            return _XDSSHBSConnectWS.IsTicketValid(XDSConnectTicket);
        }

         public string AdminSystemUserAsync(string ConnectTicket, bool GetAll)
        {
            return _XDSSHBSConnectWS.AdminSystemUser(ConnectTicket, GetAll);
        }

        public string ConnectLinkageDetectorAsync(string ConnectTicket, string EnquiryReason, string YourReference, string VoucherCode, int ProductID, string POIdNumber, string POPassportNumber, string POFirstName, string POSurname, string PODateOfBirth, string ClaimNumber, string TPIdNumber, string TPPassportNumber, string TPFirstName, string TPSurname, string TPDateOfBirth)
        {

            return _XDSSHBSConnectWS.ConsumerLinkageSearch(ConnectTicket, EnquiryReason, YourReference, VoucherCode, ProductID, POIdNumber, POPassportNumber, POFirstName, POSurname, PODateOfBirth, TPIdNumber, TPPassportNumber, TPFirstName, TPSurname, TPDateOfBirth);
            //return "<LinkageSearchResult><ListOfConsumers><ConsumerDetails><ConsumerID>1</ConsumerID><FirstName>John</FirstName><SecondName /><ThirdName /><Surname>Doe</Surname><IDNo>7503075082086</IDNo><PassportNo /><BirthDate>1975-03-07T00:00:00+02:00</BirthDate><GenderInd>M</GenderInd><BonusXML /><TempReference /><EnquiryID>222</EnquiryID><EnquiryResultID>333</EnquiryResultID><Reference>C11111111-1111111</Reference></ConsumerDetails><ConsumerDetails><ConsumerID>2</ConsumerID><FirstName>John</FirstName><SecondName /><ThirdName /><Surname>Doe</Surname><IDNo>7503075082086</IDNo><PassportNo /><BirthDate>1975-03-07T00:00:00+02:00</BirthDate><GenderInd>M</GenderInd><BonusXML /><TempReference /><EnquiryID>222</EnquiryID><EnquiryResultID>333</EnquiryResultID><Reference>C11111111-1111111</Reference></ConsumerDetails></ListOfConsumers><ListOfConsumersOther><ConsumerDetails><ConsumerID>3</ConsumerID><FirstName>John</FirstName><SecondName /><ThirdName />	<Surname>Doe</Surname><IDNo>7503075082086</IDNo><PassportNo /><BirthDate>1975-03-07T00:00:00+02:00</BirthDate><GenderInd>M</GenderInd><BonusXML /><TempReference /><EnquiryID>222</EnquiryID><EnquiryResultID>333</EnquiryResultID><Reference>C11111111-1111111</Reference></ConsumerDetails><ConsumerDetails><ConsumerID>4</ConsumerID><FirstName>John</FirstName><SecondName /><ThirdName /><Surname>Doe</Surname><IDNo>7503075082086</IDNo><PassportNo /><BirthDate>1975-03-07T00:00:00+02:00</BirthDate><GenderInd>M</GenderInd><BonusXML /><TempReference /><EnquiryID>222</EnquiryID><EnquiryResultID>333</EnquiryResultID><Reference>C11111111-1111111</Reference></ConsumerDetails></ListOfConsumersOther></LinkageSearchResult>";
        }

        public string GetLinkageDetectorTraceReportAsync(string ConnectTicket, int EnquiryID, int PartyAResultID, int ProductID, string BonusXML, int PartyBResultID)
        {
            return _XDSSHBSConnectWS.GetProduct(ConnectTicket, EnquiryID, PartyAResultID, ProductID, BonusXML, PartyBResultID,"");
           
        }

        public string ConnectMotorVehicleVerificationMatchAsync(string ConnectTicket, string EnquiryReason, int ProductId, string IdNumber, string PassportNo, string FirstName, string Surname, string BirthDate, string YourReference, string VoucherCode, string VIN, string RegistrationNo)
        {
            return _XDSSHBSConnectWS.ConsumerSearch(ConnectTicket, EnquiryReason, ProductId, IdNumber, PassportNo, FirstName, Surname, BirthDate, YourReference, VoucherCode,VIN,RegistrationNo);
        }

        public string LookupVehicleModelAsync(string ConnectTicket, string Make, string Year)
        {
            return _XDSSHBSConnectWS.LookupVehicleModels(ConnectTicket,Make,Year);
        }

        public string LookupVehicleVariantAsync(string ConnectTicket, string Make, string Year, string CarModel)
        {
            return _XDSSHBSConnectWS.LookupVehicleVariants(ConnectTicket, Make, Year, CarModel);
        }

        public string GetMVVSnapshotReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML, string NewVehicleCode)
        {
            return _XDSSHBSConnectWS.GetProduct(ConnectTicket, EnquiryID, EnquiryResultID, ProductID, BonusXML,0,NewVehicleCode);
        }
    }
}
