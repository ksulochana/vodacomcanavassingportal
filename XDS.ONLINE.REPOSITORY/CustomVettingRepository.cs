﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace XDS.ONLINE.REPOSITORY
{
    public class CustomVettingRepository : XDS.ONLINE.REPOSITORY.IXDSCustomVettingRepository
    {
        private XDSCustomVetting.CustomVettingWSSoapClient _XDSCustomVettingConnectWS;
        public CustomVettingRepository()
        {
            _XDSCustomVettingConnectWS = new XDSCustomVetting.CustomVettingWSSoapClient();
        }
        ~CustomVettingRepository()
        {
            //TODO: Error Logging
            try
            {
                _XDSCustomVettingConnectWS.Close();
                _XDSCustomVettingConnectWS = null;
            }
            catch (CommunicationException e)
            {
                logerror(e);
                _XDSCustomVettingConnectWS.Abort();
            }
            catch (TimeoutException e)
            {
                logerror(e);
                _XDSCustomVettingConnectWS.Abort();
            }
            catch (Exception e)
            {
                logerror(e);
                _XDSCustomVettingConnectWS.Abort();
                throw;
            }
        }
        private void logerror(Exception e)
        {
            //TODO: Error Logging
        }
        public System.Threading.Tasks.Task<string> CustomVettingLoginAsync(string strUser, string strPwd)
        {
            return _XDSCustomVettingConnectWS.LoginAsync(strUser, strPwd);
        }
        public System.Threading.Tasks.Task<bool> CustomVettingIsTicketValidAsync(string XDSConnectTicket)
        {
            return _XDSCustomVettingConnectWS.IsTicketValidAsync(XDSConnectTicket);
        }
        public System.Threading.Tasks.Task<string> CustomVettingAdminSystemUserAsync(string XDSConnectTicket)
        {
            return _XDSCustomVettingConnectWS.AdminSystemUserAsync(XDSConnectTicket);
        }
        public string CustomVettingAdminSystemUser(string XDSConnectTicket)
        {
            return _XDSCustomVettingConnectWS.AdminSystemUser(XDSConnectTicket);
        }
        public string CustomVettingLogin(string strUser, string strPwd)
        {
            return _XDSCustomVettingConnectWS.Login(strUser, strPwd);
        }
        public bool CustomVettingIsTicketValid(string XDSConnectTicket)
        {
            return _XDSCustomVettingConnectWS.IsTicketValid(XDSConnectTicket);
        }
        public string GetTransaction(string XDSConnectTicket, long consumerID)
        {
            return "";// _XDSCustomVettingConnectWS.GetTransaction(XDSConnectTicket, consumerID);
        }
        public string ChangeTransactionStatus(string XDSConnectTicket, long transactionID, int transactionStatus)
        {
            return "";// _XDSCustomVettingConnectWS.ChangeTransactionStatus(XDSConnectTicket, transactionID, transactionStatus);
        }
        public string CustomVettingGetTransactionDetails(string XDSConnectTicket)
        {
            return _XDSCustomVettingConnectWS.GetTransactionDetails(XDSConnectTicket);
        }
        public string CustomVettingGetTransactionHistory(string XDSConnectTicket, string User, string IDNo, string ReferenceNo, string Status, DateTime StartDate, DateTime EndDate, bool GetAll)
        {
            return _XDSCustomVettingConnectWS.GetTransactionHistory(XDSConnectTicket, User, IDNo, ReferenceNo, Status, StartDate, EndDate, GetAll);
        }
        public System.Threading.Tasks.Task<string> CustomVettingGetTransactionHistoryAsync(string XDSConnectTicket, string User, string IDNo, string ReferenceNo, string Status, DateTime StartDate, DateTime EndDate, bool GetAll)
        {
            return _XDSCustomVettingConnectWS.GetTransactionHistoryAsync(XDSConnectTicket, User, IDNo, ReferenceNo, Status, StartDate, EndDate, GetAll);
        }
        public string GetTransactionEnquiry(string XDSConnectTicket, long EnquiryID)
        {
            return _XDSCustomVettingConnectWS.GetTransactionEnquiry(XDSConnectTicket, EnquiryID);
        }
        public System.Threading.Tasks.Task<string> GetTransactionEnquiryAsync(string XDSConnectTicket, long EnquiryID)
        {
            return _XDSCustomVettingConnectWS.GetTransactionEnquiryAsync(XDSConnectTicket, EnquiryID);
        }
        public string GetBulletins(string XDSConnectTicket)
        {
            return _XDSCustomVettingConnectWS.GetBulletins(XDSConnectTicket);
        }
        public System.Threading.Tasks.Task<string> GetBulletinsAsync(string XDSConnectTicket)
        {
            return _XDSCustomVettingConnectWS.GetBulletinsAsync(XDSConnectTicket);
        }
        public string GetBulletinFile(string XDSConnectTicket, long BulletinID)
        {
            return _XDSCustomVettingConnectWS.GetBulletinFile(XDSConnectTicket, BulletinID);
        }
        public System.Threading.Tasks.Task<string> GetBulletinFileAsync(string XDSConnectTicket, long BulletinID)
        {
            return _XDSCustomVettingConnectWS.GetBulletinFileAsync(XDSConnectTicket, BulletinID);
        }
        public string GetPromotionDeals(string XDSConnectTicket, long PromotionDealID)
        {
            return _XDSCustomVettingConnectWS.GetPromotionDeals(XDSConnectTicket, PromotionDealID);
        }
        public string GetPromotionDealsByPortfolio(string XDSConnectTicket, string Portfolio)
        {
            return _XDSCustomVettingConnectWS.GetPromotionDealsByPortfolio(XDSConnectTicket, Portfolio);
        }
        public string GetPortfoliosByPromotionDeal(string XDSConnectTicket, long PromotionDealID)
        {
            return _XDSCustomVettingConnectWS.GetPortfoliosByPromotionDeal(XDSConnectTicket, PromotionDealID);
        }
        public System.Threading.Tasks.Task<string> GetPromotionDealsAsync(string XDSConnectTicket, long PromotionDealID)
        {
            return _XDSCustomVettingConnectWS.GetPromotionDealsAsync(XDSConnectTicket, PromotionDealID);
        }
        public System.Threading.Tasks.Task<string> GetPromotionDealsByPortfolioAsync(string XDSConnectTicket, string Portfolio)
        {
            return _XDSCustomVettingConnectWS.GetPromotionDealsByPortfolioAsync(XDSConnectTicket, Portfolio);
        }
        public System.Threading.Tasks.Task<string> GetPortfoliosByPromotionDealAsync(string XDSConnectTicket, long PromotionDealID)
        {
            return _XDSCustomVettingConnectWS.GetPortfoliosByPromotionDealAsync(XDSConnectTicket, PromotionDealID);
        }
        public string GetSMSConfig(string XDSConnectTicket)
        {
            return _XDSCustomVettingConnectWS.GetSMSConfig(XDSConnectTicket);
        }
        public Task<string> GetSMSConfigAsync(string XDSConnectTicket)
        {
            return _XDSCustomVettingConnectWS.GetSMSConfigAsync(XDSConnectTicket);
        }
        public string LogSMS(string XDSConnectTicket, long EnquiryID, long QueueID)
        {
            return _XDSCustomVettingConnectWS.LogSMS(XDSConnectTicket, EnquiryID, QueueID);
        }

        public Task<string> LogSMSAsync(string XDSConnectTicket, long EnquiryID, long QueueID)
        {
            return _XDSCustomVettingConnectWS.LogSMSAsync(XDSConnectTicket, EnquiryID, QueueID);
        }

        public string LogTransaction(string XDSConnectTicket, string ApplicationPromotionDeal, string ApplicationDealerID, string ApplicationPromotionCode, string ApplicantFirstname, string ApplicantSurname, string ApplicantBirthDate, string ApplicantIDNumber, string ApplicantGrossIncome, string ApplicantMobileNo, string ApplicantAlternateNo, string ApplicantEmail, string ReferenceNumber, string Score, string PreScoringReasonCode, string DecisionDescription, string Portfolio, string ScoreDate, string ReasonDescription, string CreatedByUser, string ConsumerID, string PredictedIncome, string PredictedAvailableInstalment, string Accountno, string SourceInd, string VodacomResponse, string VodacomResponseType, string EmailStatus, float Latitude, float Longitude, string City)
        {
            return _XDSCustomVettingConnectWS.LogTransaction(XDSConnectTicket, ApplicationPromotionDeal, ApplicationDealerID, ApplicationPromotionCode, ApplicantFirstname,
                ApplicantSurname, ApplicantBirthDate, ApplicantIDNumber, ApplicantGrossIncome, ApplicantMobileNo, ApplicantAlternateNo, ApplicantEmail, ReferenceNumber,
                Score, PreScoringReasonCode, DecisionDescription, Portfolio, ScoreDate, ReasonDescription, CreatedByUser, ConsumerID, PredictedIncome, PredictedAvailableInstalment, Accountno, SourceInd,
                VodacomResponse, VodacomResponseType, EmailStatus, Longitude, Latitude, City);
        }

        public Task<string> LogTransactionAsync(string XDSConnectTicket, string ApplicationPromotionDeal, string ApplicationDealerID, string ApplicationPromotionCode, string ApplicantFirstname, string ApplicantSurname, string ApplicantBirthDate, string ApplicantIDNumber, string ApplicantGrossIncome, string ApplicantMobileNo, string ApplicantAlternateNo, string ApplicantEmail, string ReferenceNumber, string Score, string PreScoringReasonCode, string DecisionDescription, string Portfolio, string ScoreDate, string ReasonDescription, string CreatedByUser, string ConsumerID, string PredictedIncome, string PredictedAvailableInstalment, string Accountno, string SourceInd, string VodacomResponse, string VodacomResponseType, string EmailStatus, float Latitude, float Longitude, string City)
        {
            return _XDSCustomVettingConnectWS.LogTransactionAsync(XDSConnectTicket, ApplicationPromotionDeal, ApplicationDealerID, ApplicationPromotionCode, ApplicantFirstname,
                ApplicantSurname, ApplicantBirthDate, ApplicantIDNumber, ApplicantGrossIncome, ApplicantMobileNo, ApplicantAlternateNo, ApplicantEmail, ReferenceNumber,
                Score, PreScoringReasonCode, DecisionDescription, Portfolio, ScoreDate, ReasonDescription, CreatedByUser, ConsumerID, PredictedIncome, PredictedAvailableInstalment, Accountno, SourceInd,
                VodacomResponse, VodacomResponseType, EmailStatus, Longitude, Latitude, City);
        }

        public System.Threading.Tasks.Task<string> AdminChangePasswordAsync(string ConnectTicket, string CurrentPassword, string NewPassword, string ConfirmPassword)
        {
            return _XDSCustomVettingConnectWS.AdminChangePasswordAsync(ConnectTicket, CurrentPassword, NewPassword, ConfirmPassword);
        }

        public System.Threading.Tasks.Task<string> AdminChangeUserDetailsAsync(string ConnectTicket, string FirstName, string Surname, string Email, string JobTitle, string JobPosition, XDS.ONLINE.REPOSITORY.XDSCustomVetting.UserRole Role, bool isActive)
        {
            //return _XDSConnectWS.AdminChangeUserDetailsAsync(ConnectTicket, FirstName, Surname, Email, JobTitle, JobPosition, Role, isActive);
            return _XDSCustomVettingConnectWS.AdminChangeUserDetailsAsync(ConnectTicket, FirstName, Surname, Email, Role, isActive);
        }

        public bool CheckifUserExists(string XDSConnectTicket, string username, string cellularnumber)
        {
            return _XDSCustomVettingConnectWS.CheckifUserExists(XDSConnectTicket, username, cellularnumber);
        }

        public Task<bool> CheckifUserExistsAsync(string XDSConnectTicket, string username, string cellularnumber)
        {
            return _XDSCustomVettingConnectWS.CheckifUserExistsAsync(XDSConnectTicket, username, cellularnumber);
        }

        public string GenerateResetPasswordCode(string XDSConnectTicket, string username, string cellularnumber,string issueduser)
        {
            return _XDSCustomVettingConnectWS.GenerateResetPasswordCode(XDSConnectTicket, username, cellularnumber, issueduser);
        }
        public System.Threading.Tasks.Task<string> GenerateResetPasswordCodeAsync(string XDSConnectTicket, string username, string cellularnumber, string issueduser)
        {
            return _XDSCustomVettingConnectWS.GenerateResetPasswordCodeAsync(XDSConnectTicket, username, cellularnumber,  issueduser);
        }
        public string GetPasswordResetCodeStatus(string XDSConnectTicket, string username, string cellularnumber, string Code)
        {
            return _XDSCustomVettingConnectWS.GetPasswordResetCodeStatus(XDSConnectTicket, username, cellularnumber, Code);
        }

        public System.Threading.Tasks.Task<string> GetPasswordResetCodeStatusAsync(string XDSConnectTicket, string username, string cellularnumber, string Code)
        {
            return _XDSCustomVettingConnectWS.GetPasswordResetCodeStatusAsync(XDSConnectTicket, username, cellularnumber, Code);
        }

        public System.Threading.Tasks.Task<string> AdminResetPasswordAsync(string ConnectTicket, string Username, string Cellnumber, string NewPassword, string ConfirmPassword,string Code)
        {
            return _XDSCustomVettingConnectWS.AdminResetPasswordAsync(ConnectTicket, Username,Cellnumber, NewPassword, ConfirmPassword,Code);
        }
        public string GetTrainingDoc(string XDSConnectTicket)
        {
            return _XDSCustomVettingConnectWS.GetTrainingDocuments(XDSConnectTicket);
        }
        public System.Threading.Tasks.Task<string> GetTrainingDocAsync(string XDSConnectTicket)
        {
            return _XDSCustomVettingConnectWS.GetTrainingDocumentsAsync(XDSConnectTicket);
        }
        public string GetTrainingDocFile(string XDSConnectTicket, long TrainingID)
        {
            return _XDSCustomVettingConnectWS.GetTrainingDocFile(XDSConnectTicket, TrainingID);
        }
        public System.Threading.Tasks.Task<string> GetTrainingDocFileAsync(string XDSConnectTicket, long TrainingID)
        {
            return _XDSCustomVettingConnectWS.GetTrainingDocFileAsync(XDSConnectTicket, TrainingID);
        }
    }
}