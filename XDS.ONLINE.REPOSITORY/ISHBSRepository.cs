﻿using System;

namespace XDS.ONLINE.REPOSITORY
{
 public interface ISHBSRepository
    {
        bool IsTicketValid(string XDSConnectTicket);
        //System.Threading.Tasks.Task<bool> IsTicketValidAsync(string XDSConnectTicket);
        string Login(string strUser, string strPwd);
        //System.Threading.Tasks.Task<string> LoginAsync(string strUser, string strPwd);
        string AdminSystemUserAsync(string ConnectTicket, bool GetAll);
        string ConnectLinkageDetectorAsync(string ConnectTicket, string EnquiryReason, string YourReference, string VoucherCode, int ProductID, string POIdNumber, string POPassportNumber, string POFirstName, string POSurname, string PODateOfBirth, string ClaimNumber, string TPIdNumber, string TPPassportNumber, string TPFirstName, string TPSurname, string TPDateOfBirth);
        string GetLinkageDetectorTraceReportAsync(string ConnectTicket, int EnquiryID, int PartyAResultID, int ProductID, string BonusXML, int PartyBResultID);
       string ConnectMotorVehicleVerificationMatchAsync(string ConnectTicket, string EnquiryReason, int ProductId, string IdNumber, string PassportNo, string FirstName, string Surname, string BirthDate, string YourReference, string VoucherCode, string VIN, string RegistrationNo);
       string LookupVehicleModelAsync(string ConnectTicket, string Make, string Year);
       string LookupVehicleVariantAsync(string ConnectTicket, string Make, string Year, string CarModel);
       string GetMVVSnapshotReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML, string NewVehicleCode);
    }
}
