﻿using System;
using XDS.ONLINE.MODELS;
//using XDS.ONLINE.MODELS.Report;
using System.Threading.Tasks;

namespace XDS.ONLINE.ENGINE
{
    public interface IXDSServiceEngine
    {
        bool HasError();
        string ErrorMessage();
        string ReportXML();

        #region Consumer

        //Search
        Task<ListOfConsumers> ConnectConsumerMatchAsync(string ConnectTicket, string EnquiryReason, int ProductId, string IdNumber, string PassportNo, string FirstName, string Surname, string BirthDate, string YourReference, string VoucherCode);
        Task<ListOfConsumers> ConnectTelephoneMatchAsync(string ConnectTicket, string TelephoneCode, string TelephoneNo, string Surname, string YourReference, string VoucherCode);
        Task<ListOfConsumers> ConnectAccountMatchAsync(string ConnectTicket, string AccountNo, string SubAccountNo, string Surname, string YourReference, string VoucherCode);
        Task<ListOfConsumers> ConnectEasyMatchAsync(string ConnectTicket, int ProductId, string SearchType, string Surname, string FirstName, int Age, int Year, int Deviation, string YourReference, string VoucherCode);
        Task<ListOfConsumers> ConnectAddressMatchAsync(string ConnectTicket, string Province, string City, string Suburb, bool PostalMatch, string StreetName_PostalNo, string PostalCode, string StreetNo, string Surname, string YourReference, string VoucherCode);

        //Report
        Task<ConsumerTraceReportModel> GetConsumerTraceReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML);
        Task<CollectionSnapshotReportModel> GetCollectionSnapshotReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML);

        //object CreateObject(string XMLString, object YourClassObject);
        //ConsumerTraceReportModel GetConsumerTraceReport(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML);

        #endregion

        #region Linkage Detector

        //Search
        Task<LinkageSearchResult> ConnectLinkageDetectorAsync(string ConnectTicket, string EnquiryReason, string YourReference, string VoucherCode, int ProductID, string POIdNumber, string POPassportNumber, string POFirstName, string POSurname, string PODateOfBirth, string ClaimNumber, string TPIdNumber, string TPPassportNumber, string TPFirstName, string TPSurname, string TPDateOfBirth);

        //Report
        Task<Linkage> GetLinkageDetectorTraceReportAsync(string ConnectTicket, int EnquiryID, int PartyAResultID, int ProductID, string BonusXML, int PartyBResultID);
        #endregion

        #region Motor Vehicle Verification 

        //Search
        Task<ListOfConsumers> ConnectMotorVehicleVerificationMatchAsync(string ConnectTicket, string EnquiryReason, int ProductId, string IdNumber, string PassportNo, string FirstName, string Surname, string BirthDate, string YourReference, string VoucherCode, string VIN, string RegistrationNo);
        Task<VehicleModels> LookupVehicleModelAsync(string ConnectTicket, string Make, string Year);
        Task<VehicleVariants> LookupVehicleVariantAsync(string ConnectTicket, string Make, string Year, string CarModel);
        Task<MVVSnapshotReport> GetMVVSnapshotReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML, string NewVehicleCode);


        ////Vehicle Models
        //Task<ListOfConsumers> ConnectMVVLookupVehicleModelsAsync(string ConnectTicket, string EnquiryReason, int ProductId, string IdNumber, string PassportNo, string FirstName, string Surname, string BirthDate, string YourReference, string VoucherCode, string VIN, string RegistrationNo);

        ////Vehicle Variants
        //Task<ListOfConsumers> ConnectMVVLookupVehicleVariantsAsync(string ConnectTicket, string EnquiryReason, int ProductId, string IdNumber, string PassportNo, string FirstName, string Surname, string BirthDate, string YourReference, string VoucherCode, string VIN, string RegistrationNo);

        ////Report
        //Task<CollectionSnapshotReportModel> GetMotorVehicleVerificationSnapshotReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML);

        #endregion


        #region[ Custom Vetting ]

        Task<string> CustomVettingLoginAsync(string strUser, string strPwd);

        Task<bool> CustomVettingIsTicketValidAsync(string XDSConnectTicket);

        Task<SystemUser> CustomVettingAdminSystemUserAsync(string XDSConnectTicket);

        //  SystemUser CustomVettingAdminSystemUser(string XDSConnectTicket);

        string CustomVettingLogin(string strUser, string strPwd);

        bool CustomVettingIsTicketValid(string XDSConnectTicket);

        string CustomVettingGetTransactionDetails(string XDSConnectTicket);

        ListofHistory CustomVettingGetTransactionHistory(string XDSConnectTicket, string User, string IDNo, string ReferenceNo, string Status, DateTime StartDate, DateTime EndDate, bool GetAll);

        Task<ListofHistory> CustomVettingGetTransactionHistoryAsync(string XDSConnectTicket, string User, string IDNo, string ReferenceNo, string Status, DateTime StartDate, DateTime EndDate, bool GetAll);
        CustomVetting CustomVettingGetTransactionEnquiry(string XDSConnectTicket, long EnquiryID);
        Task<CustomVetting> CustomVettingGetTransactionEnquiryAsync(string XDSConnectTicket, long EnquiryID);
        ListofBulletins CustomVettingGetBulletins(string XDSConnectTicket);
        Task<ListofBulletins> CustomVettingGetBulletinsAsync(string XDSConnectTicket);
        ListofBulletins CustomVettingGetBulletinFile(string XDSConnectTicket, long BulletinID);
        Task<ListofBulletins> CustomVettingGetBulletinFileAsync(string XDSConnectTicket, long BulletinID);
        ListofPromotionDeals GetPromotionDeals(string XDSConnectTicket, long PromotionDealID);
        ListofPromotionDeals GetPromotionDealsByPortfolio(string XDSConnectTicket, string Portfolio);
        ListofPortfolios GetPortfoliosByPromotionDeal(string XDSConnectTicket, long PromotionDealID);
        Task<ListofPromotionDeals> GetPromotionDealsAsync(string XDSConnectTicket, long PromotionDealID);
        Task<ListofPromotionDeals> GetPromotionDealsByPortfolioAsync(string XDSConnectTicket, string Portfolio);
        Task<ListofPortfolios> GetPortfoliosByPromotionDealAsync(string XDSConnectTicket, long PromotionDealID);
        ListofSMSConfig GetSMSConfig(string XDSConnectTicket);
        Task<ListofSMSConfig> GetSMSConfigAsync(string XDSConnectTicket);
        string LogTransaction(string XDSConnectTicket, string ApplicationPromotionDeal, string ApplicationDealerID, string ApplicationPromotionCode, string ApplicantFirstname,
                               string ApplicantSurname, string ApplicantBirthDate, string ApplicantIDNumber, string ApplicantGrossIncome, string ApplicantMobileNo,
                               string ApplicantAlternateNo, string ApplicantEmail, string ReferenceNumber, string Score, string PreScoringReasonCode, string DecisionDescription,
                               string Portfolio, string ScoreDate, string ReasonDescription, string CreatedByUser, string ConsumerID, string PredictedIncome, string PredictedAvailableInstalment, string Accountno, string SourceInd, string VodacomResponse, string VodacomResponseType, string EmailStatus, float Latitude, float Longitude, string City);
        Task<string> LogTransactionAsync(string XDSConnectTicket, string ApplicationPromotionDeal, string ApplicationDealerID, string ApplicationPromotionCode, string ApplicantFirstname,
                                 string ApplicantSurname, string ApplicantBirthDate, string ApplicantIDNumber, string ApplicantGrossIncome, string ApplicantMobileNo,
                                 string ApplicantAlternateNo, string ApplicantEmail, string ReferenceNumber, string Score, string PreScoringReasonCode, string DecisionDescription,
                                 string Portfolio, string ScoreDate, string ReasonDescription, string CreatedByUser, string ConsumerID, string PredictedIncome, string PredictedAvailableInstalment, string Accountno, string SourceInd, string VodacomResponse, string VodacomResponseType, string EmailStatus, float Latitude, float Longitude, string City);


        #endregion



        #region Verification

        //Search
        Task<ListOfConsumers> ConnectIDVerificationAsync(string ConnectTicket, string IdNumber, string FirstName, string Surname, string YourReference, string VoucherCode);

        //Report
        Task<IDVerificationTraceReportModel> GetIDVerificationTraceReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML);

        #endregion

        #region CreditEnquiry

        //Search
        Task<ListOfConsumers> ConnectConsumerMatchCreditAssesmentAsync(string ConnectTicket, string EnquiryReason, int ProductId, string IdNumber, string PassportNo, string FirstName, string Surname, string BirthDate, string MaritalStatus, string SpouseFirstName, string SpouseSurname, string ResidentialAddressLine1, string ResidentialAddressLine2, string ResidentialAddressLine3, string ResidentialAddressLine4, string ResidentialPostalCode, string PostalAddressLine1, string PostalAddressLine2, string PostalAddressLine3, string PostalAddressLine4, string PostalPostalCode, string HomeTelCode, string HomeTelNo, string WorkTelCode, string WorkTelNo, string CellularCode, string CellularNo, string EmailAddress, string TotalNetMonthlyIncome, string Employer, string JobTitle, string YourReference, string VoucherCode);

        Task<ListOfEnquiryReasons> ConnectGetEnquiryReasonsAsync(string connectTicket);
        string ConnectGetEnquiryReasons(string connectTicket);

        #endregion

        #region Commercial Enquiry

        //Search
        Task<ListOfDirectors> ConnectDirectorMatchAsync(string ConnectTicket, string IdNumber, string FirstName, string Surname, string YourReference, string VoucherCode);
        Task<ListOfBusiness> ConnectBusinessMatchAsync(string ConnectTicket, string Reg1, string Reg2, string Reg3, string BusinessName, string VatNo, string SolePropIDNo, string YourReference, string VoucherCode);

        //Email
        Task<BusinessInvestigateEnquiryRequest> ConnectBusinessInvestigativeEnquiryAsync(string ConnectTicket, string Country, string ReportType, string TimeFrame, string Reg1, string Reg2, string Reg3, string BusinessName, string IDNo, string AdditionalInfo, string CompanyContactNo, string strTerms, double dAmount, string FirstName, string SurName, string TelNo, string EmailAddress, string CompanyName, bool bSendEmail, string VoucherCode, string YourReference);

        //Report
        Task<PrincipalEnquiryTraceReportModel> GetDirectorEnquiryReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML);
        //Task<BusinessEnquiryTraceReportModel> GetBusinessInvestigativeEnquiryReportAsync(string ConnectTicket, int ReferenceNo);
        Task<BusinessEnquiryTraceReportModel> GetBusinessEnquiryReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML);
        Task<ACFEEnquiryTraceReportModel> GetACFEEnquiryReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML);



        #endregion



        #region Deeds Enquiry
        //Search
        Task<ListOfProperties> ConnectDeedsMatchAsync(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.SearchtypeInd intSearchTypeINd, string DeedsOffice, string IDNo, string FirstName, string SurName, string RegNo1, string RegNo2, string RegNo3, string BusinessName, string TownshipName, string ErfNo, string PortionNo, string TitleDeedNo, string BondAccno, string YourReference, string VoucherCode);

        Task<PropertyEnquiryReportModel> GetPropertyEnquiryReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML);
        #endregion


        #region Banking Enquiry

        //Search
        //Task<string> ConnectBankCodesRequestAsync(string ConnectTicket, string BankCodeRequestType, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.TypeofVerificationenum verficationType, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.Entity EntityType, string IDno, string Reg1, string Reg2, string Reg3, string TrustNo, string BankName, string BranchName, string BranchCode, string AccNo, string AccHolderName, double Amount, string Terms, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.Region BankCodeRegion, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.CountryCode BankCodeCountryCode, string AdditionalInfo, string FirstName, string SurName, string CompanyName, string TelNo, string EmailAddress, bool bSendEmail, string VoucherCode, string YourReference);

        Task<BankCodesEnquiryRequest> ConnectBankCodesRequestAsync(string ConnectTicket, string BankCodeRequestType, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.TypeofVerificationenum verficationType, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.Entity EntityType, string IDno, string Reg1, string Reg2, string Reg3, string TrustNo, string BankName, string BranchName, string BranchCode, string AccNo, string AccHolderName, double Amount, string Terms, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.Region BankCodeRegion, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.CountryCode BankCodeCountryCode, string AdditionalInfo, string FirstName, string SurName, string CompanyName, string TelNo, string EmailAddress, bool bSendEmail, string VoucherCode, string YourReference);
        Task<BankCodesEnquiryReportModel> ConnectGetBankCodeResultAsync(string ConnectTicket, int EnquiryLogID);

        Task<AccountVerification> ConnectAccountVerificationRealTimeAsync(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.TypeofVerificationenum VerificationType, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.Entity Entity, string Initials, string SurName, string IDNo, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AVSIDType IDType, string CompanyName, string Reg1, string Reg2, string Reg3, string TrustNo, string AccNo, string BranchCode, string Acctype, string BankName, string VoucherCode, string YourReference);

        //Email
        Task<AccountVerification> ConnectAccountVerificationAsync(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.TypeofVerificationenum VerificationType, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.Entity Entity, string Initials, string SurName, string IDNo, string CompanyName, string Reg1, string Reg2, string Reg3, string TrustNo, string AccNo, string BranchCode, string Acctype, string BankName, string ClientFirstName, string ClientSurName, string EmailAddress, bool bSendEmail, string VoucherCode, string YourReference);

        #endregion

        #region Fraud Enquiry

        //Search
        Task<ListOfConsumers> ConnectFraudConsumerMatchAsync(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationSearchType SearchType, string BranchCode, int PurposeID, string IdNumber, string PassportNo, string CellularCode, string CellularNo, string AccountNo, string SubaccountNo, string YourReference, string VoucherCode);
        Task<ListOfConsumers> ConnectFraudConsumerMatchWithOverrideOTPAsync(string ConnectTicket,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationSearchType SearchType,
                    string BranchCode,
                    int PurposeID,
                    string IdNumber,
                    string PassportNo,
                    string CellularCode,
                    string CellularNo,
                    string AccountNo,
                    string SubaccountNo,
                    bool OverrideOTP,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.OverrideOTPReasons OverrideOTPReason,
                    string OverrideOTPComment,
                    string EmailAddress,
                    string YourReference,
                    string VoucherCode);

        //Report
        Task<AuthenticationProcess> ConnectFraudGetQuestionsAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID);

        //ConnectGetHistoryReport
        Task<AuthenticationProcess> GetHistoryReportAsync(string ConnectTicket, int EnquiryResultID);

        //Auth Submission
        Task<AuthenticationProcess> ConnectFraudProcessAsync(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationProcessAction ProcesAction, AuthenticationProcess MyAuthenticationProcess, string Comment);

        //Search
        Task<ListOfConsumers> ConnectIDPhotoVerificationAsync(string ConnectTicket, string IdNumber, string FirstName, string Surname, string YourReference, string VoucherCode);

        //Report
        Task<IDPhotoVerificationTraceReportModel> GetIDPhotoVerificationTraceReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML);

        #endregion


        #region BonusXML
        Task<BonusSegments> GetBonusSegmentsAsync(string ConnectTicket, int EnquiryResultID);
        #endregion

        #region Lists

        Task<ListOfRequestTypes> ConnectGetBankCodeRequestTypeAsync(string ConnectTicket);
        Task<ListOfBanks> ConnectGetBankNamesAsync(string ConnectTicket);
        Task<ListOfBanks> ConnectBCGetBankNamesAsync(string ConnectTicket);
        Task<ListOfTerms> ConnectGetTermsAsync(string ConnectTicket, int ProductID);

        Task<CountryList> ConnectGetCountryCodesAsync(string ConnectTicket);
        Task<ListReportTypes> ConnectGetReportTypesAsync(string ConnectTicket, int CountryID);
        Task<ListofBankAccountTypes> ConnectGetAccountTypesAsync(string ConnectTicket);


        Task<Result> ConnectGetDeedsOfficesAsync(string ConnectTicket);
        Task<ListOfProducts> ConnectGetProductListAsync(string ConnectTicket);

        #endregion


        #region History Search
        Task<ListofHistory> ConnectGetSearchHistoryAsync(string ConnectTicket, int ProductID, string Username);
        Task<ListofHistory> AdminEnquiryResultHistoryAsync(string ConnectTicket, int EnquiryID, int ProductID, bool GetAll, string username);


        Task<Profile> ConnectFraudGetProfileAsync(string ConnectTicket);

        #endregion

        #region Unblock Consumer 
        Task<BlockedConsumer> ConnectFraudGetBlockedConsumers(string ConnectTicket);

        Task<UnBlockConsumer> ConnectFraudUnBlockConsumer(string ConnectTicket, int BlockID, string Comment);

        Task<Profile> AuthenticateConnectFraudGetProfile(string ConnectTicket);

        Task<ListOfConsumers> AutheticateConnectFraudConsumerMatch(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.AuthenticationSearchType SearchType, string BranchCode, int PurposeID, string IdNumber, string PassportNo, string CellularCode, string CellularNo, string AccountNo, bool OverrideOTP, XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.OverrideOTPReasons OverrideOTPReason, string OverrideOTPComment, string SubaccountNo, string EmailAddress, string YourReference, string VoucherCode);

        Task<AuthenticationProcess> AuthenticateConnectFraudGetQuestions(string ConnectTicket, int EnquiryID, int EnquiryResultID);

        Task<AuthenticationProcess> AuthenticateConnectFraudProcess(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.AuthenticationProcessAction ProcessAction, AuthenticationProcess MyAuthenticationProcess, string Comment);

        Task<PersonalQuestions> AuthenticateConnectFraudSavePersonalQuestions(string ConnectTicket, AuthenticationProcess MyAuthenticationProcess, string Comment);

        #endregion

        CustomVetting ConnectCustomVettingN(string ConnectTicket, int ProductID, string ApplicantIDNo, string ApplicantFirstname, string ApplicantSurname,
            string ApplicantDateOfBirth, string ApplicantGrossIncome, string ApplicantMobileNo, string ApplicantAlternateNo, string ApplicantEmail, string ApplicationDealerID, string ApplicationPromotionDeal,
            string ApplicationPromotionCode, string ApplicantUser, DateTime ApplicationDateTime, string ApplicationPromotionDealValue);

        Task<CustomVetting> ConnectCustomVettingNAsync(string ConnectTicket, int ProductID, string ApplicantIDNo, string ApplicantFirstname, string ApplicantSurname,
            string ApplicantDateOfBirth, string ApplicantGrossIncome, string ApplicantMobileNo, string ApplicantAlternateNo, string ApplicantEmail, string ApplicationDealerID, string ApplicationPromotionDeal,
            string ApplicationPromotionCode, string ApplicantUser, DateTime ApplicationDateTime, string ApplicationPromotionDealValue);

        SMSResult SMSAlert(string ConnectTicket, string MobileNo, string Message, string AlertType, int ReferenceID, int ProductId);

        Task<SMSResult> SMSAlertAsync(string ConnectTicket, string MobileNo, string Message, string AlertType, int ReferenceID, int ProductId);

        string LogSMS(string XDSConnectTicket, long EnquiryID, long QueueID);
        Task<string> LogSMSAsync(string XDSConnectTicket, long EnquiryID, long QueueID);

        //Authentication V2.2
        Task<BonusSegments> AuthenticateConnectFraudGetBonusSegments(string ConnectTicket, int EnquiryResultID);

        Task<UnblockReasons> AuthenticateConnectFraudGetUnblockReasons(string ConnectTicket);

        Task<UnBlockConsumer> AuthenticateConnectFraudUnblockConsumerWithReason(string ConnectTicket, int BlockID, string UnblockReason, string Comment);

        Task<ConsumerInfoDocument> AuthenticateConnectFraudGetConsumerInfo(string ConnectTicket, long ConsumerID);

        Task<UpdateConsumerInfo> AuthenticateConnectFraudUpdateConsumerInfo(string ConnectTicket, long ConsumerID, int EnquiryID, int EnquiryResultID, string Address1, string Address2, string Address3, string Address4, string PostalCode);

        Task<OTPNumbers> AuthenticateConnectFraudGetAuthenticationOTPNumbers(string ConnectTicket, long SubscriberAuthenticationID);

        Task<AuthenticationProcess> AuthenticateConnectFraudResendOTP(string ConnectTicket, AuthenticationProcess MyAuthenticationProcess, int EnquiryID, int EnquiryResultID);

        Task<FraudScore> AuthenticateConnectFraudGetScore(string ConnectTicket,
            XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.FraudScoreTitle FraudScoreTitle, XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.FraudScoreChannel FraudScoreChannel, XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.FraudScoreStore FraudScoreStore,
            int Salary, int MonthsEmployed, int EnquiryID, int EnquiryResultID);

        Task<IDPhotoVerificationTraceReportModel> AuthenticateConnectFraudGetIDPhoto(string ConnectTicket, int EnquiryID, int EnquiryResultID, string BonusXML);

        Task<ConsumerTraceReportModel> AuthenticateConnectFraudGetCreditReport(string ConnectTicket, int EnquiryID, int EnquiryResultID, string BonusXML);

        Task<AccountVerification> AuthenticateConnectFraudAccountVerification(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.TypeofVerificationenum VerificationType, string Initials, string SurName, string AccNo, string BranchCode,
            string Acctype, string BankName, int EnquiryID, int EnquiryResultID);

        Task<AccountVerificationResult> AuthenticateConnectFraudGetAccountVerificationResult(string ConnectTicket, int EnquiryLogID);

        Task<string> AuthenticateConnectFraudCheckBlockingStatus(string ConnectTicket, int EnquiryID, int EnquiryResultID);

        //End of Authentication V2.2

        bool IsTicketValid(string XDSConnectTicket);
        Task<bool> IsTicketValidAsync(string XDSConnectTicket);
        string Login(string strUser, string strPwd);
        System.Threading.Tasks.Task<string> LoginAsync(string strUser, string strPwd);

        Task<SystemUser> AdminSystemUserAsync(string XDSConnectTicket, bool GetAll);
        Task<Subscriber> AdminSubscriberAsync(string XDSConnectTicket);

        Task<Employmentdata> ConnectUpdateEmploymentInfoAsync(string ConnectTicket, int ProductID, int SubscriberEnquiryID, int SubscriberEnquiryResultID, string EmployerName, string EmployerTelephoneNumber, string EmploymentDate, string ExternalReference);

        Task<string> AdminChangePasswordAsync(string XDSConnectTicket, string CurrentPassword, string NewPassword, string ConfirmPassword);
        Task<string> AdminChangeUserDetailsAsync(string ConnectTicket, string FirstName, string Surname, string Email, string JobTitle, string JobPosition, XDS.ONLINE.REPOSITORY.XDSCustomVetting.UserRole Role, bool isActive);

        bool CheckifUserExists(string XDSConnectTicket, string username, string cellularnumber);
        Task<bool> CheckifUserExistsAsync(string XDSConnectTicket, string username, string cellularnumber);


        string GenerateResetPasswordCode(string ticket, string username, string cellularnumber, string issueduser);
       // Task<string> InsertPasswordResetLinkAsync(string ticket, string username, string cellularnumber, string link, string issueduser);


        string GetPasswordResetCodeStatus(string ticket, string username, string cellularnumber, string Code);
        //  Task<string> GetPasswordResetLinkStatusAsync(string ticket, string username, string cellularnumber, string link);

        Task<string> AdminResetPasswordAsync(string XDSConnectTicket, string Username, string Cellnumber, string NewPassword, string ConfirmPassword, string Code);

        ListofTrainingDoc CustomVettingGetTrainingDoc(string XDSConnectTicket);
        Task<ListofTrainingDoc> CustomVettingGetTrainingDocAsync(string XDSConnectTicket);
        ListofTrainingDoc CustomVettingGetTrainingDocFile(string XDSConnectTicket, long TrainingID);
        Task<ListofTrainingDoc> CustomVettingGetTrainingDocFileAsync(string XDSConnectTicket, long TrainingID);
    }
}
