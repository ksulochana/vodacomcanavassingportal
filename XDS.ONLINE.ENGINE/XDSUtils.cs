﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Reflection;
using System.Web;


namespace XDS.ONLINE.ENGINE
{
   //Add util classes here

    public class ViewUtils
    {
        private ICustomTypeDescriptor typeDescriptor;
        public PropertyDescriptorCollection Properties;
        private string Fullname;

        public ViewUtils(Type containerType)
        {
            typeDescriptor = GetTypeDescriptor(containerType);
            Properties = typeDescriptor.GetProperties();
            Fullname = containerType.FullName;
        }

        public string GetDisplayNameForProperty(string propertyName)
        {
            //ICustomTypeDescriptor typeDescriptor = GetTypeDescriptor(containerType);
            PropertyDescriptor propertyDescriptor = Properties.Find(propertyName, true);
            if (propertyDescriptor == null)
            {
                throw new ArgumentException(string.Format("Property not found", new object[]
                {
                    Fullname,
                    propertyName
                }));
            }
            IEnumerable<Attribute> source = propertyDescriptor.Attributes.Cast<Attribute>();
            DisplayAttribute displayAttribute = source.OfType<DisplayAttribute>().FirstOrDefault<DisplayAttribute>();
            if (displayAttribute != null)
            {
                return displayAttribute.GetName();
            }
            DisplayNameAttribute displayNameAttribute = source.OfType<DisplayNameAttribute>().FirstOrDefault<DisplayNameAttribute>();
            if (displayNameAttribute != null)
            {
                return displayNameAttribute.DisplayName;
            }
            return propertyName;
        }

        public static ICustomTypeDescriptor GetTypeDescriptor(Type type)
        {
            return new AssociatedMetadataTypeTypeDescriptionProvider(type).GetTypeDescriptor(type);
        }

    }
}
