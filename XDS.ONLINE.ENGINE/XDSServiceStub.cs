﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XDS.ONLINE.REPOSITORY;
using XDS.ONLINE.MODELS;

using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace XDS.ONLINE.ENGINE
{
    public class XDSServiceStub : IXDSServiceEngine
    {
        private readonly IXDSRepository _XDSReportService;
        private bool _hasError;
        private string _errorMessage;
        private string _reportXml;

        static readonly Task SuccessTask = Task.FromResult<object>(null);

        public XDSServiceStub()
        {
            //this._XDSReportService = XDSReportService;
        }

        public bool HasError()
        {
            return _hasError;
        }

        public string ErrorMessage()
        {
            return _errorMessage;
        }

        public string ReportXML()
        {
            return _reportXml;
        }

        public XDSServiceStub(IXDSRepository XDSReportService)
        {
            this._XDSReportService = XDSReportService;
        }

        public string Login(string strUser, string strPwd)
        {
            return _XDSReportService.Login(strUser, strPwd);
        }

        public System.Threading.Tasks.Task<string> LoginAsync(string strUser, string strPwd)
        {
            string result = "NotAuthenticated";
            return Task.FromResult<string>(result);
        }

        public System.Threading.Tasks.Task<bool> IsTicketValidAsync(string XDSConnectTicket)
        {
            //bool result = false;
            //if (XDSConnectTicket == "ok")
            //{ result = true; }
            bool result = true;
            return Task.FromResult<bool>(result);
        }

        public bool IsTicketValid(string XDSConnectTicket)
        {
            return _XDSReportService.IsTicketValid(XDSConnectTicket);
        }

        public async Task<SystemUser> AdminSystemUserAsync(string XDSConnectTicket, bool GetAll)
        {
            SystemUser rpt = new SystemUser();
            string xml = "<SystemUser><SystemUser><UserName>SBS112</UserName><FirstName>Shandon Business</FirstName><Surname>Solutions</Surname><SystemUserRole>SUBSUSERS</SystemUserRole></SystemUser></SystemUser>";
            //xml = 
            rpt = (SystemUser)CreateObject(xml, rpt);
            return rpt;
        }

        public async Task<Subscriber> AdminSubscriberAsync(string XDSConnectTicket)
        {
            Subscriber rpt = new Subscriber();
            string xml = "<SystemUser><SystemUser><UserName>SBS112</UserName><FirstName>Shandon Business</FirstName><Surname>Solutions</Surname><SystemUserRole>SUBSUSERS</SystemUserRole></SystemUser></SystemUser>";
            //xml = 
            rpt = (Subscriber)CreateObject(xml, rpt);
            return rpt;
        }


        //ConsumerTraceReport
        public async Task<ConsumerTraceReportModel> GetConsumerTraceReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML)
        {
            ConsumerTraceReportModel rpt = new ConsumerTraceReportModel();
            //Consumer rpt = new Consumer();
            _reportXml = await _XDSReportService.ConnectGetResultAsync(ConnectTicket, EnquiryID, EnquiryResultID, ProductID, BonusXML);
            rpt = (ConsumerTraceReportModel)CreateObject(_reportXml, rpt);
            return rpt;
        }

        //CollectionSnapshotReport
        public async Task<CollectionSnapshotReportModel> GetCollectionSnapshotReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML)
        {
            CollectionSnapshotReportModel rpt = new CollectionSnapshotReportModel();
            //Consumer rpt = new Consumer();
            _reportXml = await _XDSReportService.ConnectGetResultAsync(ConnectTicket, EnquiryID, EnquiryResultID, ProductID, BonusXML);
            rpt = (CollectionSnapshotReportModel)CreateObject(_reportXml, rpt);
            return rpt;
        }

        public async Task<IDVerificationTraceReportModel> GetIDVerificationTraceReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML)
        {
            IDVerificationTraceReportModel rpt = new IDVerificationTraceReportModel();
            //Consumer rpt = new Consumer();
            _reportXml = await _XDSReportService.ConnectGetResultAsync(ConnectTicket, EnquiryID, EnquiryResultID, ProductID, BonusXML);
            rpt = (IDVerificationTraceReportModel)CreateObject(_reportXml, rpt);
            return rpt;
        }

        public async Task<ListOfConsumers> ConnectMotorVehicleVerificationMatchAsync(string ConnectTicket, string EnquiryReason, int ProductId, string IdNumber, string PassportNo, string FirstName, string Surname, string BirthDate, string YourReference, string VoucherCode, string VIN, string RegistrationNo)
        {
            ListOfConsumers listOfConsumers = new ListOfConsumers();
            _reportXml = await _XDSReportService.ConnectConsumerMatchAsync(ConnectTicket, EnquiryReason, ProductId, IdNumber, PassportNo, FirstName, Surname, BirthDate, YourReference, VoucherCode);
            listOfConsumers = (ListOfConsumers)CreateObject(_reportXml, listOfConsumers);
            return listOfConsumers;
        }

        public async Task<VehicleModels> LookupVehicleModelAsync(string ConnectTicket, string Make, string Year)
        {
            VehicleModels listOfConsumers = new VehicleModels();
            //_reportXml = _XDSReportService.LookupVehicleModelAsync(ConnectTicket, Make, Year);
            listOfConsumers = (VehicleModels)CreateObject(_reportXml, listOfConsumers);
            return listOfConsumers;
        }

        public async Task<ListOfConsumers> ConnectMVVLookupVehicleModelsAsync(string ConnectTicket, string EnquiryReason, int ProductId, string IdNumber, string PassportNo, string FirstName, string Surname, string BirthDate, string YourReference, string VoucherCode, string VIN, string RegistrationNo)
        {
            ListOfConsumers listOfConsumers = new ListOfConsumers();
            _reportXml = await _XDSReportService.ConnectConsumerMatchAsync(ConnectTicket, EnquiryReason, ProductId, IdNumber, PassportNo, FirstName, Surname, BirthDate, YourReference, VoucherCode);
            listOfConsumers = (ListOfConsumers)CreateObject(_reportXml, listOfConsumers);
            return listOfConsumers;
        }

        public async Task<ListOfConsumers> ConnectMVVLookupVehicleVariantsAsync(string ConnectTicket, string EnquiryReason, int ProductId, string IdNumber, string PassportNo, string FirstName, string Surname, string BirthDate, string YourReference, string VoucherCode, string VIN, string RegistrationNo)
        {
            ListOfConsumers listOfConsumers = new ListOfConsumers();
            _reportXml = await _XDSReportService.ConnectConsumerMatchAsync(ConnectTicket, EnquiryReason, ProductId, IdNumber, PassportNo, FirstName, Surname, BirthDate, YourReference, VoucherCode);
            listOfConsumers = (ListOfConsumers)CreateObject(_reportXml, listOfConsumers);
            return listOfConsumers;
        }
        public async Task<MVVSnapshotReport> GetMVVSnapshotReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML, string NewVehicleCode)
        {
            MVVSnapshotReport rpt = new MVVSnapshotReport();
            //Consumer rpt = new Consumer();
            //_reportXml =  _XDSReportService.GetMVVSnapshotReportAsync(ConnectTicket, EnquiryID, EnquiryResultID, ProductID, BonusXML,);
            rpt = (MVVSnapshotReport)CreateObject(_reportXml, rpt);
            return rpt;
        }
        //CollectionSnapshotReport
        public async Task<CollectionSnapshotReportModel> GetMotorVehicleVerificationSnapshotReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML)
        {
            CollectionSnapshotReportModel rpt = new CollectionSnapshotReportModel();
            //Consumer rpt = new Consumer();
            _reportXml = await _XDSReportService.ConnectGetResultAsync(ConnectTicket, EnquiryID, EnquiryResultID, ProductID, BonusXML);
            rpt = (CollectionSnapshotReportModel)CreateObject(_reportXml, rpt);
            return rpt;
        }

        public async Task<ListOfConsumers> ConnectConsumerMatchAsync(string ConnectTicket, string EnquiryReason, int ProductId, string IdNumber, string PassportNo, string FirstName, string Surname, string BirthDate, string YourReference, string VoucherCode)
        {
            ListOfConsumers listOfConsumers = new ListOfConsumers();
            _reportXml = "<ListOfConsumers><ConsumerDetails><ConsumerID>1111</ConsumerID><FirstName>FirstName</FirstName><SecondName /><ThirdName /><Surname>SURNAME</Surname><IDNo>0000000000000</IDNo><PassportNo /><BirthDate>1984-03-07T00:00:00+02:00</BirthDate><GenderInd>M</GenderInd><BonusXML /><TempReference /><EnquiryID>4444444</EnquiryID><EnquiryResultID>333333333</EnquiryResultID><Reference>C4444444-3333333333</Reference></ConsumerDetails></ListOfConsumers>";
            listOfConsumers = (ListOfConsumers)CreateObject(_reportXml, listOfConsumers);
            return listOfConsumers;
        }

        public async Task<BonusSegments> GetBonusSegmentsAsync(string ConnectTicket, int EnquiryResultID)
        {
            BonusSegments rpt = new BonusSegments();
            //Consumer rpt = new Consumer();
            _reportXml = await _XDSReportService.ConnectGetBonusSegmentsAsync(ConnectTicket, EnquiryResultID);
            rpt = (BonusSegments)CreateObject(_reportXml, rpt);
            return rpt;
        }

        public async Task<VehicleVariants> LookupVehicleVariantAsync(string ConnectTicket, string Make, string Year, string CarModel)
        {
            VehicleVariants listOfVehicleVariants = new VehicleVariants();
            //_reportXml = _SHBSReportService.LookupVehicleVariantAsync(ConnectTicket, Make, Year, CarModel);
            listOfVehicleVariants = (VehicleVariants)CreateObject(_reportXml, listOfVehicleVariants);
            return listOfVehicleVariants;
        }

        //public ListOfConsumers ConnectConsumerMatch(string ConnectTicket, string EnquiryReason, int ProductId, string IdNumber, string PassportNo, string FirstName, string Surname, string BirthDate, string YourReference, string VoucherCode)
        //{
        //    ListOfConsumers listOfConsumers = new ListOfConsumers();
        //    string result = _XDSReportService.ConnectConsumerMatch(ConnectTicket, EnquiryReason, ProductId, IdNumber, PassportNo, FirstName, Surname, BirthDate, YourReference, VoucherCode);
        //    listOfConsumers = (ListOfConsumers)CreateObject(result, listOfConsumers);
        //    return listOfConsumers;
        //}



        #region[ Custom Vetting ]

        public string CustomVettingLogin(string strUser, string strPwd)
        {
            return _XDSReportService.Login(strUser, strPwd);
        }

        public System.Threading.Tasks.Task<string> CustomVettingLoginAsync(string strUser, string strPwd)
        {
            string result = "NotAuthenticated";
            return Task.FromResult<string>(result);
        }

        public System.Threading.Tasks.Task<bool> CustomVettingIsTicketValidAsync(string XDSConnectTicket)
        {
            //bool result = false;
            //if (XDSConnectTicket == "ok")
            //{ result = true; }
            bool result = true;
            return Task.FromResult<bool>(result);
        }

        public async System.Threading.Tasks.Task<SystemUser> CustomVettingAdminSystemUserAsync(string XDSConnectTicket)
        {
            SystemUser rpt = new SystemUser();
            string xml = "<SystemUser><SystemUser><UserName>SBS112</UserName><FirstName>Shandon Business</FirstName><Surname>Solutions</Surname><SystemUserRole>SUBSUSERS</SystemUserRole></SystemUser></SystemUser>";
            //xml = 
            rpt = (SystemUser)CreateObject(xml, rpt);
            return rpt;
        }

        public bool CustomVettingIsTicketValid(string XDSConnectTicket)
        {
            return _XDSReportService.IsTicketValid(XDSConnectTicket);
        }

        public string CustomVettingGetTransactionDetails(string XDSConnectTicket)
        {
            return "";
        }

        public ListofHistory CustomVettingGetTransactionHistory(string XDSConnectTicket, string User, string IDNo, string ReferenceNo, string Status, DateTime StartDate, DateTime EndDate, bool GetAll)
        {
            ListofHistory rpt = new ListofHistory();
            string xml = "";
            //xml = 
            rpt = (ListofHistory)CreateObject(xml, rpt);
            return rpt;
        }

        public async Task<ListofHistory> CustomVettingGetTransactionHistoryAsync(string XDSConnectTicket, string User, string IDNo, string ReferenceNo, string Status, DateTime StartDate, DateTime EndDate, bool GetAll)
        {
            ListofHistory rpt = new ListofHistory();
            string xml = "";
            //xml = 
            rpt = (ListofHistory)CreateObject(xml, rpt);

            return rpt;
        }

        public CustomVetting CustomVettingGetTransactionEnquiry(string XDSConnectTicket, long EnquiryID)
        {
            CustomVetting rpt = new CustomVetting();
            string xml = "";
            //xml = 
            rpt = (CustomVetting)CreateObject(xml, rpt);

            return rpt;
        }

        public async System.Threading.Tasks.Task<CustomVetting> CustomVettingGetTransactionEnquiryAsync(string XDSConnectTicket, long EnquiryID)
        {
            CustomVetting rpt = new CustomVetting();
            string xml = "";
            //xml = 
            rpt = (CustomVetting)CreateObject(xml, rpt);

            return rpt;
        }

        public ListofBulletins CustomVettingGetBulletins(string XDSConnectTicket)
        {
            ListofBulletins rpt = new ListofBulletins();
            string xml = "";
            //xml = 
            rpt = (ListofBulletins)CreateObject(xml, rpt);

            return rpt;
        }
        public async Task<ListofBulletins> CustomVettingGetBulletinsAsync(string XDSConnectTicket)
        {
            ListofBulletins rpt = new ListofBulletins();
            string xml = "";
            //xml = 
            rpt = (ListofBulletins)CreateObject(xml, rpt);

            return rpt;
        }

        public ListofBulletins CustomVettingGetBulletinFile(string XDSConnectTicket, long BulletinID)
        {
            ListofBulletins rpt = new ListofBulletins();
            string xml = "";
            //xml = 
            rpt = (ListofBulletins)CreateObject(xml, rpt);

            return rpt;
        }
        public async Task<ListofBulletins> CustomVettingGetBulletinFileAsync(string XDSConnectTicket, long BulletinID)
        {
            ListofBulletins rpt = new ListofBulletins();
            string xml = "";
            //xml = 
            rpt = (ListofBulletins)CreateObject(xml, rpt);

            return rpt;
        }

        public ListofPromotionDeals GetPromotionDeals(string XDSConnectTicket, long PromotionDealID)
        {
            ListofPromotionDeals rpt = new ListofPromotionDeals();
            string xml = "";
            //xml = 
            rpt = (ListofPromotionDeals)CreateObject(xml, rpt);

            return rpt;
        }
        public ListofPromotionDeals GetPromotionDealsByPortfolio(string XDSConnectTicket, string Portfolio)
        {
            ListofPromotionDeals rpt = new ListofPromotionDeals();
            string xml = "";
            //xml = 
            rpt = (ListofPromotionDeals)CreateObject(xml, rpt);

            return rpt;
        }
        public ListofPortfolios GetPortfoliosByPromotionDeal(string XDSConnectTicket, long PromotionDealID)
        {
            ListofPortfolios rpt = new ListofPortfolios();
            string xml = "";
            //xml = 
            rpt = (ListofPortfolios)CreateObject(xml, rpt);

            return rpt;
        }
        public async System.Threading.Tasks.Task<ListofPromotionDeals> GetPromotionDealsAsync(string XDSConnectTicket, long PromotionDealID)
        {
            ListofPromotionDeals rpt = new ListofPromotionDeals();
            string xml = "";
            //xml = 
            rpt = (ListofPromotionDeals)CreateObject(xml, rpt);

            return rpt;
        }
        public async System.Threading.Tasks.Task<ListofPromotionDeals> GetPromotionDealsByPortfolioAsync(string XDSConnectTicket, string Portfolio)
        {
            ListofPromotionDeals rpt = new ListofPromotionDeals();
            string xml = "";
            //xml = 
            rpt = (ListofPromotionDeals)CreateObject(xml, rpt);

            return rpt;
        }
        public async System.Threading.Tasks.Task<ListofPortfolios> GetPortfoliosByPromotionDealAsync(string XDSConnectTicket, long PromotionDealID)
        {
            ListofPortfolios rpt = new ListofPortfolios();
            string xml = "";
            //xml = 
            rpt = (ListofPortfolios)CreateObject(xml, rpt);

            return rpt;
        }
        public ListofSMSConfig GetSMSConfig(string XDSConnectTicket)
        {
            ListofSMSConfig rpt = new ListofSMSConfig();
            string xml = "";
            //xml = 
            rpt = (ListofSMSConfig)CreateObject(xml, rpt);

            return rpt;
        }
        public async System.Threading.Tasks.Task<ListofSMSConfig> GetSMSConfigAsync(string XDSConnectTicket)
        {
            ListofSMSConfig rpt = new ListofSMSConfig();
            string xml = "";
            //xml = 
            rpt = (ListofSMSConfig)CreateObject(xml, rpt);

            return rpt;
        }

        #endregion


        public async Task<ListOfConsumers> ConnectTelephoneMatchAsync(string ConnectTicket, string TelephoneCode, string TelephoneNo, string Surname, string YourReference, string VoucherCode)
        {
            ListOfConsumers listOfConsumers = new ListOfConsumers();
            _reportXml = await _XDSReportService.ConnectTelephoneMatchAsync(ConnectTicket, TelephoneCode, TelephoneNo, Surname, YourReference, VoucherCode);
            listOfConsumers = (ListOfConsumers)CreateObject(_reportXml, listOfConsumers);
            return listOfConsumers;
        }

        public async Task<ListOfConsumers> ConnectAccountMatchAsync(string ConnectTicket, string AccountNo, string SubAccountNo, string Surname, string YourReference, string VoucherCode)
        {
            ListOfConsumers listOfConsumers = new ListOfConsumers();
            _reportXml = await _XDSReportService.ConnectAccountMatchAsync(ConnectTicket, AccountNo, SubAccountNo, Surname, YourReference, VoucherCode);
            listOfConsumers = (ListOfConsumers)CreateObject(_reportXml, listOfConsumers);
            return listOfConsumers;
        }

        public async Task<ListOfConsumers> ConnectEasyMatchAsync(string ConnectTicket, int ProductId, string SearchType, string Surname, string FirstName, int Age, int Year, int Deviation, string YourReference, string VoucherCode)
        {
            ListOfConsumers listOfConsumers = new ListOfConsumers();
            _reportXml = await _XDSReportService.ConnectEasyMatchAsync(ConnectTicket, ProductId, SearchType, Surname, FirstName, Age, Year, Deviation, YourReference, VoucherCode);
            listOfConsumers = (ListOfConsumers)CreateObject(_reportXml, listOfConsumers);
            return listOfConsumers;
        }

        public async Task<ListOfConsumers> ConnectAddressMatchAsync(string ConnectTicket, string Province, string City, string Suburb, bool PostalMatch, string StreetName_PostalNo, string PostalCode, string StreetNo, string Surname, string YourReference, string VoucherCode)
        {
            ListOfConsumers listOfConsumers = new ListOfConsumers();
            _reportXml = await _XDSReportService.ConnectAddressMatchAsync(ConnectTicket, Province, City, Suburb, PostalMatch, StreetName_PostalNo, PostalCode, StreetNo, Surname, YourReference, VoucherCode);
            listOfConsumers = (ListOfConsumers)CreateObject(_reportXml, listOfConsumers);
            return listOfConsumers;
        }

        public async Task<ListOfConsumers> ConnectIDVerificationAsync(string ConnectTicket, string IdNumber, string FirstName, string Surname, string YourReference, string VoucherCode)
        {
            ListOfConsumers listOfConsumers = new ListOfConsumers();
            _reportXml = await _XDSReportService.ConnectIDVerificationAsync(ConnectTicket, IdNumber, FirstName, Surname, YourReference, VoucherCode);
            listOfConsumers = (ListOfConsumers)CreateObject(_reportXml, listOfConsumers);
            return listOfConsumers;
        }


        public async Task<ListOfConsumers> ConnectConsumerMatchCreditAssesmentAsync(string ConnectTicket, string EnquiryReason, int ProductId, string IdNumber, string PassportNo, string FirstName, string Surname, string BirthDate, string MaritalStatus, string SpouseFirstName, string SpouseSurname, string ResidentialAddressLine1, string ResidentialAddressLine2, string ResidentialAddressLine3, string ResidentialAddressLine4, string ResidentialPostalCode, string PostalAddressLine1, string PostalAddressLine2, string PostalAddressLine3, string PostalAddressLine4, string PostalPostalCode, string HomeTelCode, string HomeTelNo, string WorkTelCode, string WorkTelNo, string CellularCode, string CellularNo, string EmailAddress, string TotalNetMonthlyIncome, string Employer, string JobTitle, string YourReference, string VoucherCode)
        {
            ListOfConsumers listOfConsumers = new ListOfConsumers();

            _reportXml = await _XDSReportService.ConnectConsumerMatchCreditAssesmentAsync(ConnectTicket, EnquiryReason, ProductId, IdNumber, PassportNo, FirstName, Surname, BirthDate,
                MaritalStatus == null ? "" : MaritalStatus, SpouseFirstName == null ? "" : SpouseFirstName, SpouseSurname == null ? "" : SpouseSurname, ResidentialAddressLine1 == null ? "" : ResidentialAddressLine1,
                ResidentialAddressLine2 == null ? "" : ResidentialAddressLine2, ResidentialAddressLine3 == null ? "" : ResidentialAddressLine3, ResidentialAddressLine4 == null ? "" : ResidentialAddressLine4,
                ResidentialPostalCode == null ? "" : ResidentialPostalCode, PostalAddressLine1 == null ? "" : PostalAddressLine1, PostalAddressLine2 == null ? "" : PostalAddressLine2,
                PostalAddressLine3 == null ? "" : PostalAddressLine3, PostalAddressLine4 == null ? "" : PostalAddressLine4, PostalPostalCode == null ? "" : PostalPostalCode, HomeTelCode == null ? "" : HomeTelCode,
                HomeTelNo == null ? "" : HomeTelNo, WorkTelCode == null ? "" : WorkTelCode, WorkTelNo == null ? "" : WorkTelNo, CellularCode == null ? "" : CellularCode, CellularNo == null ? "" : CellularNo, EmailAddress == null ? "" : EmailAddress,
                TotalNetMonthlyIncome == null ? "" : TotalNetMonthlyIncome, Employer == null ? "" : Employer, JobTitle == null ? "" : JobTitle, YourReference, VoucherCode);

            listOfConsumers = (ListOfConsumers)CreateObject(_reportXml, listOfConsumers);
            return listOfConsumers;
        }

        public async Task<ListOfEnquiryReasons> ConnectGetEnquiryReasonsAsync(string connectTicket)
        {
            ListOfEnquiryReasons listOfReasons = new ListOfEnquiryReasons();
            try
            {
                string reasons = "";
                reasons = await _XDSReportService.ConnectGetEnquiryReasonsAsync(connectTicket);
                listOfReasons = (ListOfEnquiryReasons)CreateObject(reasons, listOfReasons);
            }
            catch (Exception)
            {

                throw;
            }

            return listOfReasons;
        }

        public string ConnectGetEnquiryReasons(string connectTicket)
        {
            return _XDSReportService.ConnectGetEnquiryReasons(connectTicket);
        }

        public async Task<ListOfDirectors> ConnectDirectorMatchAsync(string ConnectTicket, string IdNumber, string FirstName, string Surname, string YourReference, string VoucherCode)
        {
            ListOfDirectors listOfDirectors = new ListOfDirectors();
            _reportXml = await _XDSReportService.ConnectDirectorMatchAsync(ConnectTicket, IdNumber, FirstName, Surname, YourReference, VoucherCode);
            listOfDirectors = (ListOfDirectors)CreateObject(_reportXml, listOfDirectors);
            return listOfDirectors;
        }

        public async Task<ListOfProperties> ConnectDeedsMatchAsync(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.SearchtypeInd intSearchTypeINd, string DeedsOffice, string IDNo, string FirstName, string SurName, string RegNo1, string RegNo2, string RegNo3, string BusinessName, string TownshipName, string ErfNo, string PortionNo, string TitleDeedNo, string BondAccno, string YourReference, string VoucherCode)
        {
            ListOfProperties listOfProperties = new ListOfProperties();
            _reportXml = await _XDSReportService.ConnectDeedsMatchAsync(ConnectTicket, intSearchTypeINd, DeedsOffice, IDNo, FirstName, SurName, RegNo1, RegNo2, RegNo3, BusinessName, TownshipName, ErfNo, PortionNo, TitleDeedNo, BondAccno, YourReference, VoucherCode);
            listOfProperties = (ListOfProperties)CreateObject(_reportXml, listOfProperties);
            return listOfProperties;
        }


        public async Task<ListofHistory> ConnectGetSearchHistoryAsync(string ConnectTicket, int ProductID, string Username)
        {
            ListofHistory listOfHistory = new ListofHistory();
            _reportXml = await _XDSReportService.ConnectGetSearchHistoryAsync(ConnectTicket, ProductID, Username);
            listOfHistory = (ListofHistory)CreateObject(_reportXml, listOfHistory);
            return listOfHistory;
        }

        public async Task<ListofHistory> AdminEnquiryResultHistoryAsync(string ConnectTicket, int EnquiryID, int ProductID, bool GetAll, string username)
        {
            ListofHistory listOfHistory = new ListofHistory();
            _reportXml = await _XDSReportService.AdminEnquiryResultHistoryAsync(ConnectTicket, EnquiryID, ProductID, GetAll, username);
            listOfHistory = (ListofHistory)CreateObject(_reportXml, listOfHistory);
            return listOfHistory;
        }

        public async Task<PrincipalEnquiryTraceReportModel> GetDirectorEnquiryReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML)
        {
            PrincipalEnquiryTraceReportModel rpt = new PrincipalEnquiryTraceReportModel();
            //Consumer rpt = new Consumer();
            _reportXml = await _XDSReportService.ConnectGetResultAsync(ConnectTicket, EnquiryID, EnquiryResultID, ProductID, BonusXML);
            rpt = (PrincipalEnquiryTraceReportModel)CreateObject(_reportXml, rpt);
            return rpt;
        }

        public async Task<ListOfBusiness> ConnectBusinessMatchAsync(string ConnectTicket, string Reg1, string Reg2, string Reg3, string BusinessName, string VatNo, string SolePropIDNo, string YourReference, string VoucherCode)
        {
            ListOfBusiness listOfBusinesses = new ListOfBusiness();
            _reportXml = await _XDSReportService.ConnectBusinessMatchAsync(ConnectTicket, Reg1, Reg2, Reg3, BusinessName, VatNo, SolePropIDNo, YourReference, VoucherCode);
            listOfBusinesses = (ListOfBusiness)CreateObject(_reportXml, listOfBusinesses);
            return listOfBusinesses;
        }

        //public async Task<BusinessEnquiryTraceReportModel> GetBusinessInvestigativeEnquiryReportAsync(string ConnectTicket, int ReferenceNo)
        public async Task<BusinessEnquiryTraceReportModel> GetBusinessEnquiryReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML)
        {
            BusinessEnquiryTraceReportModel rpt = new BusinessEnquiryTraceReportModel();
            //Consumer rpt = new Consumer();
            //_reportXml = await _XDSReportService.ConnectGetBusinessInvestigativeEnquiryResultAsync(ConnectTicket, ReferenceNo);
            _reportXml = await _XDSReportService.ConnectGetResultAsync(ConnectTicket, EnquiryID, EnquiryResultID, ProductID, BonusXML);
            rpt = (BusinessEnquiryTraceReportModel)CreateObject(_reportXml, rpt);
            return rpt;
        }


        public async Task<PropertyEnquiryReportModel> GetPropertyEnquiryReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML)
        {
            PropertyEnquiryReportModel rpt = new PropertyEnquiryReportModel();
            //Consumer rpt = new Consumer();
            //_reportXml = await _XDSReportService.ConnectGetBusinessInvestigativeEnquiryResultAsync(ConnectTicket, ReferenceNo);
            _reportXml = await _XDSReportService.ConnectGetResultAsync(ConnectTicket, EnquiryID, EnquiryResultID, ProductID, BonusXML);
            rpt = (PropertyEnquiryReportModel)CreateObject(_reportXml, rpt);
            return rpt;
        }


        public async Task<ACFEEnquiryTraceReportModel> GetACFEEnquiryReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML)
        {
            ACFEEnquiryTraceReportModel rpt = new ACFEEnquiryTraceReportModel();
            //Consumer rpt = new Consumer();
            //_reportXml = await _XDSReportService.ConnectGetBusinessInvestigativeEnquiryResultAsync(ConnectTicket, ReferenceNo);
            _reportXml = await _XDSReportService.ConnectGetResultAsync(ConnectTicket, EnquiryID, EnquiryResultID, ProductID, BonusXML);
            rpt = (ACFEEnquiryTraceReportModel)CreateObject(_reportXml, rpt);
            return rpt;
        }




        public async Task<BusinessInvestigateEnquiryRequest> ConnectBusinessInvestigativeEnquiryAsync(string ConnectTicket, string Country, string ReportType, string TimeFrame, string Reg1, string Reg2, string Reg3, string BusinessName, string IDNo, string AdditionalInfo, string CompanyContactNo, string strTerms, double dAmount, string FirstName, string SurName, string TelNo, string EmailAddress, string CompanyName, bool bSendEmail, string VoucherCode, string YourReference)
        {
            BusinessInvestigateEnquiryRequest rpt = new BusinessInvestigateEnquiryRequest();
            //Consumer rpt = new Consumer();
            _reportXml = await _XDSReportService.ConnectBusinessInvestigativeEnquiryAsync(ConnectTicket, Country, ReportType, TimeFrame, Reg1, Reg2, Reg3, BusinessName, IDNo, AdditionalInfo, CompanyContactNo, strTerms, dAmount, FirstName, SurName, TelNo, EmailAddress, CompanyName, bSendEmail, VoucherCode, YourReference);
            rpt = (BusinessInvestigateEnquiryRequest)CreateObject(_reportXml, rpt);
            return rpt;
        }

        public async Task<ListOfRequestTypes> ConnectGetBankCodeRequestTypeAsync(string ConnectTicket)
        {
            ListOfRequestTypes requestTypes = new ListOfRequestTypes();
            string types = await _XDSReportService.ConnectGetBankCodeRequestTypeAsync(ConnectTicket);
            requestTypes = (ListOfRequestTypes)CreateObject(types, requestTypes);
            return requestTypes;
        }

        public async Task<ListOfBanks> ConnectGetBankNamesAsync(string ConnectTicket)
        {
            ListOfBanks listOfBanks = new ListOfBanks();
            string banks = await _XDSReportService.ConnectGetBankNamesAsync(ConnectTicket);
            listOfBanks = (ListOfBanks)CreateObject(banks, listOfBanks);
            return listOfBanks;
        }




        public async Task<Result> ConnectGetDeedsOfficesAsync(string ConnectTicket)
        {
            Result listOfDeedsOffice = new Result();
            string Office = await _XDSReportService.ConnectGetDeedsOfficesAsync(ConnectTicket);
            listOfDeedsOffice = (Result)CreateObject(Office, listOfDeedsOffice);
            return listOfDeedsOffice;
        }


        public async Task<ListOfProducts> ConnectGetProductListAsync(string ConnectTicket)
        {
            ListOfProducts listOfProducts = new ListOfProducts();
            string Product = await _XDSReportService.ConnectGetProductListAsync(ConnectTicket);
            listOfProducts = (ListOfProducts)CreateObject(Product, listOfProducts);
            return listOfProducts;
        }

        public async Task<ListofHistory> ConnectFraudGetBlockedConsumersAsync(string ConnectTicket)
        {
            ListofHistory listOfHistory = new ListofHistory();
            _reportXml = await _XDSReportService.ConnectFraudGetBlockedConsumersAsync(ConnectTicket);
            listOfHistory = (ListofHistory)CreateObject(_reportXml, listOfHistory);
            return listOfHistory;
        }



        public async Task<ListOfTerms> ConnectGetTermsAsync(string ConnectTicket, int ProductID)
        {
            ListOfTerms listOfTerms = new ListOfTerms();
            _reportXml = await _XDSReportService.ConnectGetTermsAsync(ConnectTicket, ProductID);
            listOfTerms = (ListOfTerms)CreateObject(_reportXml, listOfTerms);
            return listOfTerms;
        }

        public async Task<BankCodesEnquiryRequest> ConnectBankCodesRequestAsync(string ConnectTicket, string BankCodeRequestType, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.TypeofVerificationenum verficationType, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.Entity EntityType, string IDno, string Reg1, string Reg2, string Reg3, string TrustNo, string BankName, string BranchName, string BranchCode, string AccNo, string AccHolderName, double Amount, string Terms, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.Region BankCodeRegion, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.CountryCode BankCodeCountryCode, string AdditionalInfo, string FirstName, string SurName, string CompanyName, string TelNo, string EmailAddress, bool bSendEmail, string VoucherCode, string YourReference)
        {
            BankCodesEnquiryRequest rpt = new BankCodesEnquiryRequest();
            _reportXml = await _XDSReportService.ConnectBankCodesRequestAsync(ConnectTicket, BankCodeRequestType, verficationType, EntityType, IDno, Reg1, Reg2, Reg3, TrustNo, BankName, BranchName, BranchCode, AccNo, AccHolderName, Amount, Terms, BankCodeRegion, BankCodeCountryCode, AdditionalInfo, FirstName, SurName, CompanyName, TelNo, EmailAddress, bSendEmail, VoucherCode, YourReference);
            rpt = (BankCodesEnquiryRequest)CreateObject(_reportXml, rpt);
            return rpt;
        }

        /// <summary>
        /// Not being used?
        /// </summary>
        /// <returns></returns>
        public async Task<BankCodesEnquiryReportModel> ConnectGetBankCodeResultAsync(string ConnectTicket, int EnquiryLogID)
        {
            BankCodesEnquiryReportModel rpt = new BankCodesEnquiryReportModel();
            _reportXml = await _XDSReportService.ConnectGetBankCodeResultAsync(ConnectTicket, EnquiryLogID);
            rpt = (BankCodesEnquiryReportModel)CreateObject(_reportXml, rpt);
            return rpt;
        }

        public async Task<CountryList> ConnectGetCountryCodesAsync(string ConnectTicket)
        {
            CountryList listOfCountries = new CountryList();
            _reportXml = await _XDSReportService.ConnectGetCountryCodesAsync(ConnectTicket);
            listOfCountries = (CountryList)CreateObject(_reportXml, listOfCountries);
            return listOfCountries;
        }

        public async Task<ListReportTypes> ConnectGetReportTypesAsync(string ConnectTicket, int CountryID)
        {
            ListReportTypes listOfReportTypes = new ListReportTypes();
            _reportXml = await _XDSReportService.ConnectGetReportTypesAsync(ConnectTicket, CountryID);
            listOfReportTypes = (ListReportTypes)CreateObject(_reportXml, listOfReportTypes);
            return listOfReportTypes;
        }

        public async Task<ListofBankAccountTypes> ConnectGetAccountTypesAsync(string ConnectTicket)
        {
            ListofBankAccountTypes listOfBankAccountTypes = new ListofBankAccountTypes();
            string banks = await _XDSReportService.ConnectGetAccountTypesAsync(ConnectTicket);
            listOfBankAccountTypes = (ListofBankAccountTypes)CreateObject(banks, listOfBankAccountTypes);
            return listOfBankAccountTypes;
        }


        //ConnectAccountVerificationAsync
        public async Task<AccountVerification> ConnectAccountVerificationAsync(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.TypeofVerificationenum VerificationType, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.Entity Entity, string Initials, string SurName, string IDNo, string CompanyName, string Reg1, string Reg2, string Reg3, string TrustNo, string AccNo, string BranchCode, string Acctype, string BankName, string ClientFirstName, string ClientSurName, string EmailAddress, bool bSendEmail, string VoucherCode, string YourReference)
        {
            AccountVerification rpt = new AccountVerification();
            //Consumer rpt = new Consumer();
            _reportXml = await _XDSReportService.ConnectAccountVerificationAsync(ConnectTicket, VerificationType, Entity, Initials, SurName, IDNo, CompanyName, Reg1, Reg2, Reg3, TrustNo, AccNo, BranchCode, Acctype, BankName, ClientFirstName, ClientSurName, EmailAddress, bSendEmail, VoucherCode, YourReference);
            rpt = (AccountVerification)CreateObject(_reportXml, rpt);
            return rpt;
        }


        public async Task<AccountVerification> ConnectAccountVerificationRealTimeAsync(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.TypeofVerificationenum VerificationType, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.Entity Entity, string Initials, string SurName, string IDNo, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AVSIDType IDType, string CompanyName, string Reg1, string Reg2, string Reg3, string TrustNo, string AccNo, string BranchCode, string Acctype, string BankName, string VoucherCode, string YourReference)
        {
            AccountVerification rpt = new AccountVerification();
            //Consumer rpt = new Consumer();
            _reportXml = await _XDSReportService.ConnectAccountVerificationRealTimeAsync(ConnectTicket, VerificationType, Entity, Initials, SurName, IDNo, IDType, CompanyName, Reg1, Reg2, Reg3, TrustNo, AccNo, BranchCode, Acctype, BankName, VoucherCode, YourReference);
            rpt = (AccountVerification)CreateObject(_reportXml, rpt);
            return rpt;
        }


        //

        public async Task<ListOfConsumers> ConnectIDPhotoVerificationAsync(string ConnectTicket, string IdNumber, string FirstName, string Surname, string YourReference, string VoucherCode)
        {
            ListOfConsumers listOfConsumers = new ListOfConsumers();
            //_reportXml = await _XDSReportService.ConnectIDVerificationAsync(ConnectTicket, IdNumber, FirstName, Surname, YourReference, VoucherCode);
            _reportXml = await _XDSReportService.ConnectIDPhotoVerificationAsync(ConnectTicket, IdNumber, FirstName, Surname, YourReference, VoucherCode);
            listOfConsumers = (ListOfConsumers)CreateObject(_reportXml, listOfConsumers);
            return listOfConsumers;
        }

        public async Task<IDPhotoVerificationTraceReportModel> GetIDPhotoVerificationTraceReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML)
        {
            IDPhotoVerificationTraceReportModel rpt = new IDPhotoVerificationTraceReportModel();
            //Consumer rpt = new Consumer();
            _reportXml = await _XDSReportService.ConnectGetResultAsync(ConnectTicket, EnquiryID, EnquiryResultID, ProductID, BonusXML);
            rpt = (IDPhotoVerificationTraceReportModel)CreateObject(_reportXml, rpt);
            return rpt;
        }


        public async Task<Profile> ConnectFraudGetProfileAsync(string ConnectTicket)
        {
            Profile profile = new Profile();
            string profileResponse = await _XDSReportService.ConnectFraudGetProfileAsync(ConnectTicket);
            profile = (Profile)CreateObject(profileResponse, profile);
            return profile;
        }

        public async Task<ListOfConsumers> ConnectFraudConsumerMatchAsync(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationSearchType SearchType, string BranchCode, int PurposeID, string IdNumber, string PassportNo, string CellularCode, string CellularNo, string AccountNo, string SubaccountNo, string YourReference, string VoucherCode)
        {
            ListOfConsumers listOfConsumers = new ListOfConsumers();
            //_reportXml = await _XDSReportService.ConnectIDVerificationAsync(ConnectTicket, IdNumber, FirstName, Surname, YourReference, VoucherCode);
            _reportXml = await _XDSReportService.ConnectFraudConsumerMatchAsync(ConnectTicket, SearchType, BranchCode, PurposeID, IdNumber, PassportNo, CellularCode, CellularNo, AccountNo, SubaccountNo, YourReference, VoucherCode);
            listOfConsumers = (ListOfConsumers)CreateObject(_reportXml, listOfConsumers);
            return listOfConsumers;
        }


        public async Task<ListOfConsumers> ConnectFraudConsumerMatchWithOverrideOTPAsync(string ConnectTicket,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationSearchType SearchType,
                    string BranchCode,
                    int PurposeID,
                    string IdNumber,
                    string PassportNo,
                    string CellularCode,
                    string CellularNo,
                    string AccountNo,
                    string SubaccountNo,
                    bool OverrideOTP,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.OverrideOTPReasons OverrideOTPReason,
                    string OverrideOTPComment,
                    string EmailAddress,
                    string YourReference,
                    string VoucherCode)
        {
            ListOfConsumers listOfConsumers = new ListOfConsumers();
            //_reportXml = await _XDSReportService.ConnectIDVerificationAsync(ConnectTicket, IdNumber, FirstName, Surname, YourReference, VoucherCode);
            _reportXml = await _XDSReportService.ConnectFraudConsumerMatchWithOverrideOTPAsync(ConnectTicket, SearchType, BranchCode, PurposeID, IdNumber, PassportNo, CellularCode, CellularNo, AccountNo, SubaccountNo, OverrideOTP, OverrideOTPReason, OverrideOTPComment, EmailAddress, YourReference, VoucherCode);
            listOfConsumers = (ListOfConsumers)CreateObject(_reportXml, listOfConsumers);
            return listOfConsumers;
        }



        public async Task<AuthenticationProcess> ConnectFraudGetQuestionsAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID)
        {
            AuthenticationProcess rpt = new AuthenticationProcess();
            //_reportXml = await _XDSReportService.ConnectFraudGetQuestionsAsync(ConnectTicket, EnquiryID, EnquiryResultID);
            var _reportXml = await _XDSReportService.ConnectFraudGetQuestionsAsync(ConnectTicket, EnquiryID, EnquiryResultID);
            string stringResponse = string.Empty;

            XmlSerializer serializer = new XmlSerializer(_reportXml.GetType());

            using (StringWriter writer = new StringWriter())
            {
                serializer.Serialize(writer, _reportXml);

                stringResponse = writer.ToString();
            }

            //rpt = (ConnectFraudGetQuestionsResult)CreateObject(_reportXml.ToString(), rpt);
            rpt = (AuthenticationProcess)CreateObject(stringResponse.ToString(), rpt);
            return rpt;
        }

        public async Task<AuthenticationProcess> ConnectFraudProcessAsync(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationProcessAction ProcessAction, AuthenticationProcess MyAuthenticationProcess, string Comment)
        {
            AuthenticationProcess rpt = new AuthenticationProcess();
            XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationProcess sendAuthenticationProcess = new XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationProcess();

            string stringResponse = string.Empty;

            XmlSerializer serializer = new XmlSerializer(MyAuthenticationProcess.GetType());

            using (StringWriter writer = new StringWriter())
            {
                serializer.Serialize(writer, MyAuthenticationProcess);

                stringResponse = writer.ToString();
            }

            //rpt = (ConnectFraudGetQuestionsResult)CreateObject(_reportXml.ToString(), rpt);
            sendAuthenticationProcess = (XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationProcess)CreateObject(stringResponse.ToString(), sendAuthenticationProcess);
            //return rpt;

            //Send Submission
            var _reportXml = await _XDSReportService.ConnectFraudProcessAsync(ConnectTicket, ProcessAction, sendAuthenticationProcess, Comment);

            //get response
            XmlSerializer serializer1 = new XmlSerializer(_reportXml.GetType());

            using (StringWriter writer = new StringWriter())
            {
                serializer1.Serialize(writer, _reportXml);

                stringResponse = writer.ToString();
            }

            //rpt = (ConnectFraudGetQuestionsResult)CreateObject(_reportXml.ToString(), rpt);
            rpt = (AuthenticationProcess)CreateObject(stringResponse.ToString(), rpt);
            return rpt;
        }

        public async Task<LinkageSearchResult> ConnectLinkageDetectorAsync(string ConnectTicket, string EnquiryReason, string YourReference, string VoucherCode, int ProductID, string POIdNumber, string POPassportNumber, string POFirstName, string POSurname, string PODateOfBirth, string ClaimNumber, string TPIdNumber, string TPPassportNumber, string TPFirstName, string TPSurname, string TPDateOfBirth)
        {
            LinkageSearchResult listOfConsumers = new LinkageSearchResult();
            _reportXml = _XDSReportService.ConnectLinkageDetectorAsync(ConnectTicket, EnquiryReason, YourReference, VoucherCode, ProductID, POIdNumber, POPassportNumber, POFirstName, POSurname, PODateOfBirth, ClaimNumber, TPIdNumber, TPPassportNumber, TPFirstName, TPSurname, TPDateOfBirth);
            listOfConsumers = (LinkageSearchResult)CreateObject(_reportXml, listOfConsumers);
            return listOfConsumers;
        }

        public async Task<Linkage> GetLinkageDetectorTraceReportAsync(string ConnectTicket, int EnquiryID, int PartyAResultID, int ProductID, string BonusXML, int PartyBResultID)
        {
            Linkage Report = new Linkage();
            //_reportXml = _SHBSReportService.GetLinkageDetectorTraceReportAsync(ConnectTicket, EnquiryID, PartyAResultID, ProductID, BonusXML, PartyBResultID);
            Report = (Linkage)CreateObject(_reportXml, Report);
            return Report;
        }

        public async Task<ListOfBanks> ConnectBCGetBankNamesAsync(string ConnectTicket)
        {
            ListOfBanks listOfBanks = new ListOfBanks();
            string banks = await _XDSReportService.ConnectBCGetBankNamesAsync(ConnectTicket);
            listOfBanks = (ListOfBanks)CreateObject(banks, listOfBanks);
            return listOfBanks;
        }

        public CustomVetting ConnectCustomVettingN(string ConnectTicket, int ProductID, string ApplicantIDNo, string ApplicantFirstname, string ApplicantSurname,
              string ApplicantDateOfBirth, string ApplicantGrossIncome, string ApplicantMobileNo, string ApplicantAlternateNo, string ApplicantEmail, string ApplicationDealerID, string ApplicationPromotionDeal,
              string ApplicationPromotionCode, string ApplicantUser, DateTime ApplicationDateTime, string ApplicationPromotionDealValue)
        {
            CustomVetting rpt = new CustomVetting();

            _reportXml = _XDSReportService.ConnectCustomVettingN(ConnectTicket, ProductID, ApplicantIDNo, ApplicantFirstname, ApplicantSurname, ApplicantDateOfBirth, ApplicantGrossIncome,
               ApplicantMobileNo, ApplicantAlternateNo, ApplicantEmail, ApplicationDealerID, ApplicationPromotionDeal, ApplicationPromotionCode, ApplicantUser, ApplicationDateTime, ApplicationPromotionDealValue);

            rpt = (CustomVetting)CreateObject(_reportXml, rpt);

            return rpt;
        }

        public async System.Threading.Tasks.Task<CustomVetting> ConnectCustomVettingNAsync(string ConnectTicket, int ProductID, string ApplicantIDNo, string ApplicantFirstname, string ApplicantSurname,
            string ApplicantDateOfBirth, string ApplicantGrossIncome, string ApplicantMobileNo, string ApplicantAlternateNo, string ApplicantEmail, string ApplicationDealerID, string ApplicationPromotionDeal,
            string ApplicationPromotionCode, string ApplicantUser, DateTime ApplicationDateTime, string ApplicationPromotionDealValue)
        {
            CustomVetting rpt = new CustomVetting();

            _reportXml = await _XDSReportService.ConnectCustomVettingNAsync(ConnectTicket, ProductID, ApplicantIDNo, ApplicantFirstname, ApplicantSurname, ApplicantDateOfBirth, ApplicantGrossIncome,
                ApplicantMobileNo, ApplicantAlternateNo, ApplicantEmail, ApplicationDealerID, ApplicationPromotionDeal, ApplicationPromotionCode, ApplicantUser, ApplicationDateTime, ApplicationPromotionDealValue);

            rpt = (CustomVetting)CreateObject(_reportXml, rpt);

            return rpt;
        }

        #region helpers
        private Object CreateObject(string XMLString, Object YourClassObject)
        {
            if (XMLString.IndexOf("NoResult") >= 0)
            {
                //e.g. <NoResult><Error>Invalid IDno Supplied</Error></NoResult>
                //     <NoResult><NotFound>No Record Found</NotFound></NoResult>
                _hasError = true;

                XmlDocument xml = new XmlDocument();
                xml.LoadXml(XMLString);
                switch (xml.DocumentElement.FirstChild.Name)
                {
                    case "Error":
                    case "NotFound":
                        _errorMessage = xml.DocumentElement.FirstChild.InnerText;
                        break;

                    default:
                        //todo: log error - not expected.
                        _errorMessage = string.Format("No result ({0})", xml.DocumentElement.FirstChild.Name);
                        break;
                }
                xml = null;
                YourClassObject = null;
            }
            else
            {

                try
                {
                    XmlSerializer oXmlSerializer = new XmlSerializer(YourClassObject.GetType());
                    //The StringReader will be the stream holder for the existing XML file 
                    YourClassObject = oXmlSerializer.Deserialize(new StringReader(XMLString));
                    //initially deserialized, the data is represented by an object without a defined type 
                }
                catch (Exception ex)
                {

                    throw ex;
                }


            }

            return YourClassObject;
        }
        #endregion



        #region Authentication Web Service Calls

        public Task<Profile> AuthenticateConnectFraudGetProfile(string ConnectTicket)
        {
            throw new NotImplementedException();
        }

        public Task<ListOfConsumers> AutheticateConnectFraudConsumerMatch(string ConnectTicket, REPOSITORY.XDSConnectAuthWS.AuthenticationSearchType SearchType, string BranchCode, int PurposeID, string IdNumber, string PassportNo, string CellularCode, string CellularNo, string AccountNo, bool OverrideOTP, REPOSITORY.XDSConnectAuthWS.OverrideOTPReasons OverrideOTPReason, string OverrideOTPComment, string SubaccountNo, string EmailAddress, string YourReference, string VoucherCode)
        {
            throw new NotImplementedException();
        }

        public async Task<AuthenticationProcess> AuthenticateConnectFraudGetQuestions(string ConnectTicket, int EnquiryID, int EnquiryResultID)
        {
            throw new NotImplementedException();
        }

        public Task<AuthenticationProcess> AuthenticateConnectFraudProcess(string ConnectTicket, REPOSITORY.XDSConnectAuthWS.AuthenticationProcessAction ProcessAction, AuthenticationProcess MyAuthenticationProcess, string Comment)
        {
            throw new NotImplementedException();
        }

        public Task<PersonalQuestions> AuthenticateConnectFraudSavePersonalQuestions(string ConnectTicket, AuthenticationProcess MyAuthenticationProcess, string Comment)
        {
            throw new NotImplementedException();
        }

        //Authentication V2.2
        public Task<BonusSegments> AuthenticateConnectFraudGetBonusSegments(string ConnectTicket, int EnquiryResultID)
        {
            throw new NotImplementedException();
        }

        public Task<UnblockReasons> AuthenticateConnectFraudGetUnblockReasons(string ConnectTicket)
        {
            throw new NotImplementedException();
        }

        public Task<UnBlockConsumer> AuthenticateConnectFraudUnblockConsumerWithReason(string ConnectTicket, int BlockID, string UnblockReason, string Comment)
        {
            throw new NotImplementedException();
        }

        public Task<ConsumerInfoDocument> AuthenticateConnectFraudGetConsumerInfo(string ConnectTicket, long ConsumerID)
        {
            throw new NotImplementedException();
        }

        public Task<UpdateConsumerInfo> AuthenticateConnectFraudUpdateConsumerInfo(string ConnectTicket, long ConsumerID, int EnquiryID, int EnquiryResultID, string Address1, string Address2, string Address3, string Address4, string PostalCode)
        {
            throw new NotImplementedException();
        }

        public Task<OTPNumbers> AuthenticateConnectFraudGetAuthenticationOTPNumbers(string ConnectTicket, long SubscriberAuthenticationID)
        {
            throw new NotImplementedException();
        }

        public Task<AuthenticationProcess> AuthenticateConnectFraudResendOTP(string ConnectTicket, AuthenticationProcess MyAuthenticationProcess, int EnquiryID, int EnquiryResultID)
        {
            throw new NotImplementedException();
        }

        public Task<FraudScore> AuthenticateConnectFraudGetScore(string ConnectTicket,
            XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.FraudScoreTitle FraudScoreTitle, XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.FraudScoreChannel FraudScoreChannel, XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.FraudScoreStore FraudScoreStore,
            int Salary, int MonthsEmployed, int EnquiryID, int EnquiryResultID)
        {
            throw new NotImplementedException();
        }

        public Task<IDPhotoVerificationTraceReportModel> AuthenticateConnectFraudGetIDPhoto(string ConnectTicket, int EnquiryID, int EnquiryResultID, string BonusXML)
        {
            throw new NotImplementedException();
        }

        public Task<ConsumerTraceReportModel> AuthenticateConnectFraudGetCreditReport(string ConnectTicket, int EnquiryID, int EnquiryResultID, string BonusXML)
        {
            throw new NotImplementedException();
        }

        public Task<AccountVerification> AuthenticateConnectFraudAccountVerification(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.TypeofVerificationenum VerificationType, string Initials, string SurName, string AccNo, string BranchCode,
            string Acctype, string BankName, int EnquiryID, int EnquiryResultID)
        {
            throw new NotImplementedException();
        }

        public Task<AccountVerificationResult> AuthenticateConnectFraudGetAccountVerificationResult(string ConnectTicket, int EnquiryLogID)
        {
            throw new NotImplementedException();
        }

        public Task<string> AuthenticateConnectFraudCheckBlockingStatus(string ConnectTicket, int EnquiryID, int EnquiryResultID)
        {
            throw new NotImplementedException();
        }

        #endregion


        public Task<BlockedConsumer> ConnectFraudGetBlockedConsumers(string ConnectTicket)
        {
            throw new NotImplementedException();
        }

        public Task<UnBlockConsumer> ConnectFraudUnBlockConsumer(string ConnectTicket, int BlockID, string Comment)
        {
            throw new NotImplementedException();
        }

        public System.Threading.Tasks.Task<Employmentdata> ConnectUpdateEmploymentInfoAsync(string ConnectTicket, int ProductID, int SubscriberEnquiryID, int SubscriberEnquiryResultID, string EmployerName, string EmployerTelephoneNumber, string EmploymentDate, string ExternalReference)
        {
            throw new NotImplementedException();
        }

        public async Task<AuthenticationProcess> GetHistoryReportAsync(string ConnectTicket, int EnquiryResultID)
        {
            throw new NotImplementedException();
        }

        public SMSResult SMSAlert(string ConnectTicket, string MobileNo, string Message, string AlertType, int ReferenceID, int ProductId)
        {
            throw new NotImplementedException();
        }

        public Task<SMSResult> SMSAlertAsync(string ConnectTicket, string MobileNo, string Message, string AlertType, int ReferenceID, int ProductId)
        {
            throw new NotImplementedException();
        }

        public string LogSMS(string XDSConnectTicket, long EnquiryID, long QueueID)
        {
            throw new NotImplementedException();
        }

        public Task<string> LogSMSAsync(string XDSConnectTicket, long EnquiryID, long QueueID)
        {
            throw new NotImplementedException();
        }

        public string LogTransaction(string XDSConnectTicket, string ApplicationPromotionDeal, string ApplicationDealerID, string ApplicationPromotionCode, string ApplicantFirstname, string ApplicantSurname, string ApplicantBirthDate, string ApplicantIDNumber, string ApplicantGrossIncome, string ApplicantMobileNo, string ApplicantAlternateNo, string ApplicantEmail, string ReferenceNumber, string Score, string PreScoringReasonCode, string DecisionDescription, string Portfolio, string ScoreDate, string ReasonDescription, string CreatedByUser, string ConsumerID, string PredictedIncome, string PredictedAvailableInstalment, string Accountno, string SourceInd, string VodacomResponse, string VodacomResponseType, string EmailStatus, float Latitude, float Longitude, string City)
        {
            throw new NotImplementedException();
        }

        public Task<string> LogTransactionAsync(string XDSConnectTicket, string ApplicationPromotionDeal, string ApplicationDealerID, string ApplicationPromotionCode, string ApplicantFirstname, string ApplicantSurname, string ApplicantBirthDate, string ApplicantIDNumber, string ApplicantGrossIncome, string ApplicantMobileNo, string ApplicantAlternateNo, string ApplicantEmail, string ReferenceNumber, string Score, string PreScoringReasonCode, string DecisionDescription, string Portfolio, string ScoreDate, string ReasonDescription, string CreatedByUser, string ConsumerID, string PredictedIncome, string PredictedAvailableInstalment, string Accountno, string SourceInd, string VodacomResponse, string VodacomResponseType, string EmailStatus, float Latitude, float Longitude, string City)
        {
            throw new NotImplementedException();
        }

        public async System.Threading.Tasks.Task<string> AdminChangeUserDetailsAsync(string ConnectTicket, string FirstName, string Surname, string Email, string JobTitle, string JobPosition, XDS.ONLINE.REPOSITORY.XDSCustomVetting.UserRole Role, bool isActive)
        {
            throw new NotImplementedException();
        }

        public async Task<string> AdminChangePasswordAsync(string XDSConnectTicket, string CurrentPassword, string NewPassword, string ConfirmPassword)
        {
            throw new NotImplementedException();

        }
        public System.Threading.Tasks.Task<bool> CheckifUserExistsAsync(string XDSConnectTicket, string username, string cellularnumber)
        {
            throw new NotImplementedException();
        }

        public bool CheckifUserExists(string XDSConnectTicket, string username, string cellularnumber)
        {
            throw new NotImplementedException();
        }

        public string GenerateResetPasswordCode(string ticket, string username, string cellularnumber, string issueduser)
        {
            throw new NotImplementedException();
        }

        public string GetPasswordResetCodeStatus(string ticket, string username, string cellularnumber, string link)
        {
            throw new NotImplementedException();
        }
        public async Task<string> AdminResetPasswordAsync(string XDSConnectTicket, string Username, string Cellnumber, string NewPassword, string ConfirmPassword, string Code)
        {
            throw new NotImplementedException();

        }

        public ListofTrainingDoc CustomVettingGetTrainingDoc(string XDSConnectTicket)
        {
            throw new NotImplementedException();
        }
        public async Task<ListofTrainingDoc> CustomVettingGetTrainingDocAsync(string XDSConnectTicket)
        {
            throw new NotImplementedException();
        }

        public ListofTrainingDoc CustomVettingGetTrainingDocFile(string XDSConnectTicket, long TrainingID)
        {
            throw new NotImplementedException();
        }
        public async Task<ListofTrainingDoc> CustomVettingGetTrainingDocFileAsync(string XDSConnectTicket, long TrainingID)
        {
            throw new NotImplementedException();
        }
    }
}
