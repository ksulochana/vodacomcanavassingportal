﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XDS.ONLINE.REPOSITORY;
using XDS.ONLINE.MODELS;
//using XDS.ONLINE.MODELS.Report;

using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace XDS.ONLINE.ENGINE
{
    public class XDSServiceEngine : XDS.ONLINE.ENGINE.IXDSServiceEngine
    {
        private readonly IXDSRepository _XDSReportService;
        private readonly ISHBSRepository _SHBSReportService;
        private readonly IXDSAuthRepository _XDSAuthService;
        private readonly IXDSCustomVettingRepository _XDSCustomVettingService;

        private bool _hasError;
        private string _errorMessage;
        private string _reportXml;

        public XDSServiceEngine()
        {
            //this._XDSReportService = XDSReportService;
        }

        public bool HasError()
        {
            return _hasError;
        }

        public string ErrorMessage()
        {
            return _errorMessage;
        }

        public string ReportXML()
        {
            return _reportXml;
        }

        public XDSServiceEngine(IXDSRepository XDSReportService, ISHBSRepository SHBSReportService, IXDSAuthRepository XDSAuthRepository, IXDSCustomVettingRepository XDSCustomVettingService)
        {
            this._XDSReportService = XDSReportService;
            this._SHBSReportService = SHBSReportService;
            this._XDSAuthService = XDSAuthRepository;
            this._XDSCustomVettingService = XDSCustomVettingService;
        }

        public string Login(string strUser, string strPwd)
        {
            return _XDSReportService.Login(strUser, strPwd);
        }

        public System.Threading.Tasks.Task<string> LoginAsync(string strUser, string strPwd)
        {
            return _XDSReportService.LoginAsync(strUser, strPwd);
        }

        public System.Threading.Tasks.Task<bool> IsTicketValidAsync(string XDSConnectTicket)
        {
            return _XDSReportService.IsTicketValidAsync(XDSConnectTicket);
        }

        public bool IsTicketValid(string XDSConnectTicket)
        {
            return _XDSReportService.IsTicketValid(XDSConnectTicket);
        }

        public async Task<SystemUser> AdminSystemUserAsync(string XDSConnectTicket, bool GetAll)
        {
            SystemUser rpt = new SystemUser();
            string xml = "";
            xml = await _XDSReportService.AdminSystemUserAsync(XDSConnectTicket, GetAll);
            rpt = (SystemUser)CreateObject(xml, rpt);
            return rpt;
        }


        #region[ Custom Vetting ]

        public System.Threading.Tasks.Task<string> CustomVettingLoginAsync(string strUser, string strPwd)
        {
            return _XDSCustomVettingService.CustomVettingLoginAsync(strUser, strPwd);
        }

        public System.Threading.Tasks.Task<bool> CustomVettingIsTicketValidAsync(string XDSConnectTicket)
        {
            return _XDSCustomVettingService.CustomVettingIsTicketValidAsync(XDSConnectTicket);
        }

        public async System.Threading.Tasks.Task<SystemUser> CustomVettingAdminSystemUserAsync(string XDSConnectTicket)
        {
            SystemUser rpt = new SystemUser();
            string xml = "";
            xml = await _XDSCustomVettingService.CustomVettingAdminSystemUserAsync(XDSConnectTicket);
            rpt = (SystemUser)CreateObject(xml, rpt);
            return rpt;
        }
        public string CustomVettingAdminSystemUser(string XDSConnectTicket)
        {
            return _XDSCustomVettingService.CustomVettingAdminSystemUser(XDSConnectTicket);
        }

        public string CustomVettingLogin(string strUser, string strPwd)
        {
            return _XDSCustomVettingService.CustomVettingLogin(strUser, strPwd);
        }

        public bool CustomVettingIsTicketValid(string XDSConnectTicket)
        {
            return _XDSCustomVettingService.CustomVettingIsTicketValid(XDSConnectTicket);
        }

        public string CustomVettingGetTransactionDetails(string XDSConnectTicket)
        {
            return _XDSCustomVettingService.CustomVettingGetTransactionDetails(XDSConnectTicket);
        }

        public ListofHistory CustomVettingGetTransactionHistory(string XDSConnectTicket, string User, string IDNo, string ReferenceNo, string Status, DateTime StartDate, DateTime EndDate, bool GetAll)
        {
            ListofHistory rpt = new ListofHistory();

            _reportXml = _XDSCustomVettingService.CustomVettingGetTransactionHistory(XDSConnectTicket, User, IDNo, ReferenceNo, Status, StartDate, EndDate, GetAll);

            rpt = (ListofHistory)CreateObject(_reportXml, rpt);

            return rpt;
        }

        public async System.Threading.Tasks.Task<ListofHistory> CustomVettingGetTransactionHistoryAsync(string XDSConnectTicket, string User, string IDNo, string ReferenceNo, string Status, DateTime StartDate, DateTime EndDate, bool GetAll)
        {
            ListofHistory rpt = new ListofHistory();

            _reportXml = await _XDSCustomVettingService.CustomVettingGetTransactionHistoryAsync(XDSConnectTicket, User, IDNo, ReferenceNo, Status, StartDate, EndDate, GetAll);

            rpt = (ListofHistory)CreateObject(_reportXml, rpt);

            return rpt;
        }


        public CustomVetting ConnectCustomVettingN(string ConnectTicket, int ProductID, string ApplicantIDNo, string ApplicantFirstname, string ApplicantSurname,
            string ApplicantDateOfBirth, string ApplicantGrossIncome, string ApplicantMobileNo, string ApplicantAlternateNo, string ApplicantEmail, string ApplicationDealerID, string ApplicationPromotionDeal,
            string ApplicationPromotionCode, string ApplicantUser, DateTime ApplicationDateTime, string ApplicationPromotionDealValue)
        {
            CustomVetting rpt = new CustomVetting();

            _reportXml = _XDSReportService.ConnectCustomVettingN(ConnectTicket, ProductID, ApplicantIDNo, ApplicantFirstname, ApplicantSurname, ApplicantDateOfBirth, ApplicantGrossIncome,
               ApplicantMobileNo, ApplicantAlternateNo, ApplicantEmail, ApplicationDealerID, ApplicationPromotionDeal, ApplicationPromotionCode, ApplicantUser, ApplicationDateTime, ApplicationPromotionDealValue);

            rpt = (CustomVetting)CreateObject(_reportXml, rpt);

            return rpt;
        }

        public async System.Threading.Tasks.Task<CustomVetting> ConnectCustomVettingNAsync(string ConnectTicket, int ProductID, string ApplicantIDNo, string ApplicantFirstname, string ApplicantSurname,
            string ApplicantDateOfBirth, string ApplicantGrossIncome, string ApplicantMobileNo, string ApplicantAlternateNo, string ApplicantEmail, string ApplicationDealerID, string ApplicationPromotionDeal,
            string ApplicationPromotionCode, string ApplicantUser, DateTime ApplicationDateTime, string ApplicationPromotionDealValue)
        {
            CustomVetting rpt = new CustomVetting();

            _reportXml = await _XDSReportService.ConnectCustomVettingNAsync(ConnectTicket, ProductID, ApplicantIDNo, ApplicantFirstname, ApplicantSurname, ApplicantDateOfBirth, ApplicantGrossIncome,
                ApplicantMobileNo, ApplicantAlternateNo, ApplicantEmail, ApplicationDealerID, ApplicationPromotionDeal, ApplicationPromotionCode, ApplicantUser, ApplicationDateTime, ApplicationPromotionDealValue);

            rpt = (CustomVetting)CreateObject(_reportXml, rpt);

            return rpt;
        }

        public CustomVetting CustomVettingGetTransactionEnquiry(string XDSConnectTicket, long EnquiryID)
        {
            CustomVetting rpt = new CustomVetting();

            _reportXml = _XDSCustomVettingService.GetTransactionEnquiry(XDSConnectTicket, EnquiryID);

            rpt = (CustomVetting)CreateObject(_reportXml, rpt);

            return rpt;
        }

        public async System.Threading.Tasks.Task<CustomVetting> CustomVettingGetTransactionEnquiryAsync(string XDSConnectTicket, long EnquiryID)
        {
            CustomVetting rpt = new CustomVetting();

            _reportXml = await _XDSCustomVettingService.GetTransactionEnquiryAsync(XDSConnectTicket, EnquiryID);

            rpt = (CustomVetting)CreateObject(_reportXml, rpt);

            return rpt;
        }

        public ListofBulletins CustomVettingGetBulletins(string XDSConnectTicket)
        {
            ListofBulletins rpt = new ListofBulletins();

            _reportXml = _XDSCustomVettingService.GetBulletins(XDSConnectTicket);

            rpt = (ListofBulletins)CreateObject(_reportXml, rpt);

            return rpt;
        }
        public async Task<ListofBulletins> CustomVettingGetBulletinsAsync(string XDSConnectTicket)
        {
            ListofBulletins rpt = new ListofBulletins();

            _reportXml = await _XDSCustomVettingService.GetBulletinsAsync(XDSConnectTicket);

            rpt = (ListofBulletins)CreateObject(_reportXml, rpt);

            return rpt;
        }

        public ListofBulletins CustomVettingGetBulletinFile(string XDSConnectTicket, long BulletinID)
        {
            ListofBulletins rpt = new ListofBulletins();

            _reportXml = _XDSCustomVettingService.GetBulletinFile(XDSConnectTicket, BulletinID);

            rpt = (ListofBulletins)CreateObject(_reportXml, rpt);

            return rpt;
        }
        public async Task<ListofBulletins> CustomVettingGetBulletinFileAsync(string XDSConnectTicket, long BulletinID)
        {
            ListofBulletins rpt = new ListofBulletins();

            _reportXml = await _XDSCustomVettingService.GetBulletinFileAsync(XDSConnectTicket, BulletinID);

            rpt = (ListofBulletins)CreateObject(_reportXml, rpt);

            return rpt;
        }

        public ListofPromotionDeals GetPromotionDeals(string XDSConnectTicket, long PromotionDealID)
        {
            ListofPromotionDeals rpt = new ListofPromotionDeals();

            _reportXml = _XDSCustomVettingService.GetPromotionDeals(XDSConnectTicket, PromotionDealID);

            rpt = (ListofPromotionDeals)CreateObject(_reportXml, rpt);

            return rpt;
        }

        public ListofPromotionDeals GetPromotionDealsByPortfolio(string XDSConnectTicket, string Portfolio)
        {
            ListofPromotionDeals rpt = new ListofPromotionDeals();

            _reportXml = _XDSCustomVettingService.GetPromotionDealsByPortfolio(XDSConnectTicket, Portfolio);

            rpt = (ListofPromotionDeals)CreateObject(_reportXml, rpt);

            return rpt;
        }

        public ListofPortfolios GetPortfoliosByPromotionDeal(string XDSConnectTicket, long PromotionDealID)
        {
            ListofPortfolios rpt = new ListofPortfolios();

            _reportXml = _XDSCustomVettingService.GetPortfoliosByPromotionDeal(XDSConnectTicket, PromotionDealID);

            rpt = (ListofPortfolios)CreateObject(_reportXml, rpt);

            return rpt;
        }

        public async Task<ListofPromotionDeals> GetPromotionDealsAsync(string XDSConnectTicket, long PromotionDealID)
        {
            ListofPromotionDeals rpt = new ListofPromotionDeals();

            _reportXml = await _XDSCustomVettingService.GetPromotionDealsAsync(XDSConnectTicket, PromotionDealID);

            rpt = (ListofPromotionDeals)CreateObject(_reportXml, rpt);

            return rpt;
        }

        public async Task<ListofPromotionDeals> GetPromotionDealsByPortfolioAsync(string XDSConnectTicket, string Portfolio)
        {
            ListofPromotionDeals rpt = new ListofPromotionDeals();

            _reportXml = await _XDSCustomVettingService.GetPromotionDealsByPortfolioAsync(XDSConnectTicket, Portfolio);

            rpt = (ListofPromotionDeals)CreateObject(_reportXml, rpt);

            return rpt;
        }

        public async Task<ListofPortfolios> GetPortfoliosByPromotionDealAsync(string XDSConnectTicket, long PromotionDealID)
        {
            ListofPortfolios rpt = new ListofPortfolios();

            _reportXml = await _XDSCustomVettingService.GetPortfoliosByPromotionDealAsync(XDSConnectTicket, PromotionDealID);

            rpt = (ListofPortfolios)CreateObject(_reportXml, rpt);

            return rpt;
        }

        public ListofSMSConfig GetSMSConfig(string XDSConnectTicket)
        {
            ListofSMSConfig rpt = new ListofSMSConfig();

            _reportXml = _XDSCustomVettingService.GetSMSConfig(XDSConnectTicket);

            rpt = (ListofSMSConfig)CreateObject(_reportXml, rpt);

            return rpt;
        }

        public async Task<ListofSMSConfig> GetSMSConfigAsync(string XDSConnectTicket)
        {
            ListofSMSConfig rpt = new ListofSMSConfig();

            _reportXml = await _XDSCustomVettingService.GetSMSConfigAsync(XDSConnectTicket);

            rpt = (ListofSMSConfig)CreateObject(_reportXml, rpt);

            return rpt;
        }

        #endregion

        public SMSResult SMSAlert(string ConnectTicket, string MobileNo, string Message, string AlertType, int ReferenceID, int ProductId)
        {
            SMSResult rpt = new SMSResult();

            _reportXml = _XDSReportService.ConnectSMSAlert(ConnectTicket, MobileNo, Message, AlertType, ReferenceID, ProductId);

            rpt = (SMSResult)CreateObject(_reportXml, rpt);

            return rpt;
        }

        public async Task<SMSResult> SMSAlertAsync(string ConnectTicket, string MobileNo, string Message, string AlertType, int ReferenceID, int ProductId)
        {
            SMSResult rpt = new SMSResult();

            _reportXml = await _XDSReportService.ConnectSMSAlertAsync(ConnectTicket, MobileNo, Message, AlertType, ReferenceID, ProductId);

            rpt = (SMSResult)CreateObject(_reportXml, rpt);

            return rpt;
        }

        public string LogSMS(string XDSConnectTicket, long EnquiryID, long QueueID)
        {
            return _XDSCustomVettingService.LogSMS(XDSConnectTicket, EnquiryID, QueueID);
        }

        public async Task<string> LogSMSAsync(string XDSConnectTicket, long EnquiryID, long QueueID)
        {
            return await _XDSCustomVettingService.LogSMSAsync(XDSConnectTicket, EnquiryID, QueueID);
        }
        public string LogTransaction(string XDSConnectTicket, string ApplicationPromotionDeal, string ApplicationDealerID, string ApplicationPromotionCode, string ApplicantFirstname, string ApplicantSurname, string ApplicantBirthDate, string ApplicantIDNumber, string ApplicantGrossIncome, string ApplicantMobileNo, string ApplicantAlternateNo, string ApplicantEmail, string ReferenceNumber, string Score, string PreScoringReasonCode, string DecisionDescription, string Portfolio, string ScoreDate, string ReasonDescription, string CreatedByUser, string ConsumerID, string PredictedIncome, string PredictedAvailableInstalment, string Accountno, string SourceInd, string VodacomResponse, string VodacomResponseType, string EmailStatus, float Latitude, float Longitude, string City)
        {
            return _XDSCustomVettingService.LogTransaction(XDSConnectTicket, ApplicationPromotionDeal, ApplicationDealerID, ApplicationPromotionCode, ApplicantFirstname,
              ApplicantSurname, ApplicantBirthDate, ApplicantIDNumber, ApplicantGrossIncome, ApplicantMobileNo, ApplicantAlternateNo, ApplicantEmail, ReferenceNumber,
              Score, PreScoringReasonCode, DecisionDescription, Portfolio, ScoreDate, ReasonDescription, CreatedByUser, ConsumerID, PredictedIncome, PredictedAvailableInstalment, Accountno, SourceInd, VodacomResponse, VodacomResponseType, EmailStatus,
              Latitude, Longitude, City);
        }

        public async Task<string> LogTransactionAsync(string XDSConnectTicket, string ApplicationPromotionDeal, string ApplicationDealerID, string ApplicationPromotionCode, string ApplicantFirstname, string ApplicantSurname, string ApplicantBirthDate, string ApplicantIDNumber, string ApplicantGrossIncome, string ApplicantMobileNo, string ApplicantAlternateNo, string ApplicantEmail, string ReferenceNumber, string Score, string PreScoringReasonCode, string DecisionDescription, string Portfolio, string ScoreDate, string ReasonDescription, string CreatedByUser, string ConsumerID, string PredictedIncome, string PredictedAvailableInstalment, string Accountno, string SourceInd, string VodacomResponse, string VodacomResponseType, string EmailStatus, float Latitude, float Longitude, string City)
        {
            return await _XDSCustomVettingService.LogTransactionAsync(XDSConnectTicket, ApplicationPromotionDeal, ApplicationDealerID, ApplicationPromotionCode, ApplicantFirstname,
              ApplicantSurname, ApplicantBirthDate, ApplicantIDNumber, ApplicantGrossIncome, ApplicantMobileNo, ApplicantAlternateNo, ApplicantEmail, ReferenceNumber,
              Score, PreScoringReasonCode, DecisionDescription, Portfolio, ScoreDate, ReasonDescription, CreatedByUser, ConsumerID, PredictedIncome, PredictedAvailableInstalment, Accountno, SourceInd, VodacomResponse, VodacomResponseType, EmailStatus,
              Latitude, Longitude, City);
        }


        public async Task<Subscriber> AdminSubscriberAsync(string XDSConnectTicket)
        {
            Subscriber rpt = new Subscriber();
            string xml = "";
            xml = await _XDSReportService.AdminSubscriberAsync(XDSConnectTicket);
            rpt = (Subscriber)CreateObject(xml, rpt);
            return rpt;
        }

        public async Task<Employmentdata> ConnectUpdateEmploymentInfoAsync(string ConnectTicket, int ProductID, int SubscriberEnquiryID, int SubscriberEnquiryResultID, string EmployerName, string EmployerTelephoneNumber, string EmploymentDate, string ExternalReference)
        {
            Employmentdata rpt = new Employmentdata();
            //Consumer rpt = new Consumer();
            _reportXml = await _XDSReportService.ConnectUpdateEmploymentInfoAsync(ConnectTicket, ProductID, SubscriberEnquiryID, SubscriberEnquiryResultID, EmployerName, EmployerTelephoneNumber, EmploymentDate, ExternalReference);
            rpt = (Employmentdata)CreateObject(_reportXml, rpt);
            return rpt;
        }

        //ConsumerTraceReport
        public async Task<ConsumerTraceReportModel> GetConsumerTraceReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML)
        {
            ConsumerTraceReportModel rpt = new ConsumerTraceReportModel();
            //Consumer rpt = new Consumer();
            _reportXml = await _XDSReportService.ConnectGetResultAsync(ConnectTicket, EnquiryID, EnquiryResultID, ProductID, BonusXML);
            rpt = (ConsumerTraceReportModel)CreateObject(_reportXml, rpt);
            return rpt;
        }

        //CollectionSnapshotReport
        public async Task<CollectionSnapshotReportModel> GetCollectionSnapshotReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML)
        {
            CollectionSnapshotReportModel rpt = new CollectionSnapshotReportModel();
            //Consumer rpt = new Consumer();
            _reportXml = await _XDSReportService.ConnectGetResultAsync(ConnectTicket, EnquiryID, EnquiryResultID, ProductID, BonusXML);
            rpt = (CollectionSnapshotReportModel)CreateObject(_reportXml, rpt);
            return rpt;
        }

        public async Task<IDVerificationTraceReportModel> GetIDVerificationTraceReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML)
        {
            IDVerificationTraceReportModel rpt = new IDVerificationTraceReportModel();
            //Consumer rpt = new Consumer();
            _reportXml = await _XDSReportService.ConnectGetResultAsync(ConnectTicket, EnquiryID, EnquiryResultID, ProductID, BonusXML);
            rpt = (IDVerificationTraceReportModel)CreateObject(_reportXml, rpt);
            return rpt;
        }

        //public ConsumerTraceReportModel GetConsumerTraceReport(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML)
        //{
        //    ConsumerTraceReportModel rpt = new ConsumerTraceReportModel();
        //    string result = _XDSReportService.ConnectGetResult(ConnectTicket, EnquiryID, EnquiryResultID, ProductID, BonusXML);
        //    rpt = (ConsumerTraceReportModel)CreateObject(result, rpt);
        //    return rpt;
        //}

        public async Task<ListOfConsumers> ConnectConsumerMatchAsync(string ConnectTicket, string EnquiryReason, int ProductId, string IdNumber, string PassportNo, string FirstName, string Surname, string BirthDate, string YourReference, string VoucherCode)
        {
            ListOfConsumers listOfConsumers = new ListOfConsumers();
            _reportXml = await _XDSReportService.ConnectConsumerMatchAsync(ConnectTicket, EnquiryReason, ProductId, IdNumber, PassportNo, FirstName, Surname, BirthDate, YourReference, VoucherCode);
            listOfConsumers = (ListOfConsumers)CreateObject(_reportXml, listOfConsumers);
            return listOfConsumers;
        }

        public async Task<LinkageSearchResult> ConnectLinkageDetectorAsync(string ConnectTicket, string EnquiryReason, string YourReference, string VoucherCode, int ProductID, string POIdNumber, string POPassportNumber, string POFirstName, string POSurname, string PODateOfBirth, string ClaimNumber, string TPIdNumber, string TPPassportNumber, string TPFirstName, string TPSurname, string TPDateOfBirth)
        {
            LinkageSearchResult listOfConsumers = new LinkageSearchResult();
            //string testTicket = _SHBSReportService.Login("SBS112", "c@mPu$57eG");
            _reportXml = _SHBSReportService.ConnectLinkageDetectorAsync(ConnectTicket, EnquiryReason, YourReference, VoucherCode, ProductID, POIdNumber, POPassportNumber, POFirstName, POSurname, PODateOfBirth, ClaimNumber, TPIdNumber, TPPassportNumber, TPFirstName, TPSurname, TPDateOfBirth);
            listOfConsumers = (LinkageSearchResult)CreateObject(_reportXml, listOfConsumers);
            return listOfConsumers;
        }

        public async Task<Linkage> GetLinkageDetectorTraceReportAsync(string ConnectTicket, int EnquiryID, int PartyAResultID, int ProductID, string BonusXML, int PartyBResultID)
        {
            Linkage Report = new Linkage();
            //string testTicket = _SHBSReportService.Login("SBS112", "c@mPu$57eG");
            _reportXml = _SHBSReportService.GetLinkageDetectorTraceReportAsync(ConnectTicket, EnquiryID, PartyAResultID, ProductID, BonusXML, PartyBResultID);

            Report = (Linkage)CreateObject(_reportXml, Report);
            return Report;
        }


        public async Task<BonusSegments> GetBonusSegmentsAsync(string ConnectTicket, int EnquiryResultID)
        {
            BonusSegments rpt = new BonusSegments();
            //Consumer rpt = new Consumer();
            _reportXml = await _XDSReportService.ConnectGetBonusSegmentsAsync(ConnectTicket, EnquiryResultID);
            rpt = (BonusSegments)CreateObject(_reportXml, rpt);
            return rpt;
        }

        public async Task<ListOfConsumers> ConnectMotorVehicleVerificationMatchAsync(string ConnectTicket, string EnquiryReason, int ProductId, string IdNumber, string PassportNo, string FirstName, string Surname, string BirthDate, string YourReference, string VoucherCode, string VIN, string RegistrationNo)
        {
            ListOfConsumers listOfConsumers = new ListOfConsumers();
            _reportXml = _SHBSReportService.ConnectMotorVehicleVerificationMatchAsync(ConnectTicket, EnquiryReason, ProductId, IdNumber, PassportNo, FirstName, Surname, BirthDate, YourReference, VoucherCode, VIN, RegistrationNo);
            listOfConsumers = (ListOfConsumers)CreateObject(_reportXml, listOfConsumers);
            return listOfConsumers;
        }

        public async Task<VehicleModels> LookupVehicleModelAsync(string ConnectTicket, string Make, string Year)
        {
            VehicleModels listOfVehicleModels = new VehicleModels();
            _reportXml = _SHBSReportService.LookupVehicleModelAsync(ConnectTicket, Make, Year);
            listOfVehicleModels = (VehicleModels)CreateObject(_reportXml, listOfVehicleModels);
            return listOfVehicleModels;
        }

        public async Task<VehicleVariants> LookupVehicleVariantAsync(string ConnectTicket, string Make, string Year, string CarModel)
        {
            VehicleVariants listOfVehicleVariants = new VehicleVariants();
            _reportXml = _SHBSReportService.LookupVehicleVariantAsync(ConnectTicket, Make, Year, CarModel);
            listOfVehicleVariants = (VehicleVariants)CreateObject(_reportXml, listOfVehicleVariants);
            return listOfVehicleVariants;
        }

        public async Task<ListOfConsumers> ConnectMVVLookupVehicleModelsAsync(string ConnectTicket, string EnquiryReason, int ProductId, string IdNumber, string PassportNo, string FirstName, string Surname, string BirthDate, string YourReference, string VoucherCode, string VIN, string RegistrationNo)
        {
            ListOfConsumers listOfConsumers = new ListOfConsumers();
            _reportXml = await _XDSReportService.ConnectConsumerMatchAsync(ConnectTicket, EnquiryReason, ProductId, IdNumber, PassportNo, FirstName, Surname, BirthDate, YourReference, VoucherCode);
            listOfConsumers = (ListOfConsumers)CreateObject(_reportXml, listOfConsumers);
            return listOfConsumers;
        }

        public async Task<ListOfConsumers> ConnectMVVLookupVehicleVariantsAsync(string ConnectTicket, string EnquiryReason, int ProductId, string IdNumber, string PassportNo, string FirstName, string Surname, string BirthDate, string YourReference, string VoucherCode, string VIN, string RegistrationNo)
        {
            ListOfConsumers listOfConsumers = new ListOfConsumers();
            _reportXml = await _XDSReportService.ConnectConsumerMatchAsync(ConnectTicket, EnquiryReason, ProductId, IdNumber, PassportNo, FirstName, Surname, BirthDate, YourReference, VoucherCode);
            listOfConsumers = (ListOfConsumers)CreateObject(_reportXml, listOfConsumers);
            return listOfConsumers;
        }

        public async Task<MVVSnapshotReport> GetMVVSnapshotReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML, string NewVehicleCode)
        {
            MVVSnapshotReport rpt = new MVVSnapshotReport();
            //Consumer rpt = new Consumer();
            _reportXml = _SHBSReportService.GetMVVSnapshotReportAsync(ConnectTicket, EnquiryID, EnquiryResultID, ProductID, BonusXML, NewVehicleCode);
            rpt = (MVVSnapshotReport)CreateObject(_reportXml, rpt);

            if (rpt.ConsumerAccountStatus != null)
            {
                var allowedTypes = new[] { "S", "I", "Y" };

                ConsumerConsumerAccountStatus[] ConsumerAccountStatus = rpt.ConsumerAccountStatus;


                ConsumerConsumerAccountStatus[] ConsumerVehicleInsuranceInformation = (from VIS in ConsumerAccountStatus
                                                                                       where allowedTypes.Contains(VIS.AccountType) && VIS.StatusCodeDesc == "Active"
                                                                                       select VIS).ToArray();
                rpt.ConsumerVehicleAccountStatus = ConsumerVehicleInsuranceInformation;
            }

            return rpt;
        }

        //CollectionSnapshotReport
        public async Task<CollectionSnapshotReportModel> GetMotorVehicleVerificationSnapshotReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML)
        {
            CollectionSnapshotReportModel rpt = new CollectionSnapshotReportModel();
            //Consumer rpt = new Consumer();
            _reportXml = await _XDSReportService.ConnectGetResultAsync(ConnectTicket, EnquiryID, EnquiryResultID, ProductID, BonusXML);
            rpt = (CollectionSnapshotReportModel)CreateObject(_reportXml, rpt);
            return rpt;
        }

        public async Task<ListOfConsumers> ConnectTelephoneMatchAsync(string ConnectTicket, string TelephoneCode, string TelephoneNo, string Surname, string YourReference, string VoucherCode)
        {
            ListOfConsumers listOfConsumers = new ListOfConsumers();
            _reportXml = await _XDSReportService.ConnectTelephoneMatchAsync(ConnectTicket, TelephoneCode, TelephoneNo, Surname, YourReference, VoucherCode);
            listOfConsumers = (ListOfConsumers)CreateObject(_reportXml, listOfConsumers);
            return listOfConsumers;
        }

        public async Task<ListOfConsumers> ConnectAccountMatchAsync(string ConnectTicket, string AccountNo, string SubAccountNo, string Surname, string YourReference, string VoucherCode)
        {
            ListOfConsumers listOfConsumers = new ListOfConsumers();
            _reportXml = await _XDSReportService.ConnectAccountMatchAsync(ConnectTicket, AccountNo, SubAccountNo, Surname, YourReference, VoucherCode);
            listOfConsumers = (ListOfConsumers)CreateObject(_reportXml, listOfConsumers);
            return listOfConsumers;
        }

        public async Task<ListOfConsumers> ConnectEasyMatchAsync(string ConnectTicket, int ProductId, string SearchType, string Surname, string FirstName, int Age, int Year, int Deviation, string YourReference, string VoucherCode)
        {
            ListOfConsumers listOfConsumers = new ListOfConsumers();
            _reportXml = await _XDSReportService.ConnectEasyMatchAsync(ConnectTicket, ProductId, SearchType, Surname, FirstName, Age, Year, Deviation, YourReference, VoucherCode);
            listOfConsumers = (ListOfConsumers)CreateObject(_reportXml, listOfConsumers);
            return listOfConsumers;
        }

        public async Task<ListOfConsumers> ConnectAddressMatchAsync(string ConnectTicket, string Province, string City, string Suburb, bool PostalMatch, string StreetName_PostalNo, string PostalCode, string StreetNo, string Surname, string YourReference, string VoucherCode)
        {
            ListOfConsumers listOfConsumers = new ListOfConsumers();
            _reportXml = await _XDSReportService.ConnectAddressMatchAsync(ConnectTicket, Province, City, Suburb, PostalMatch, StreetName_PostalNo, PostalCode, StreetNo, Surname, YourReference, VoucherCode);
            listOfConsumers = (ListOfConsumers)CreateObject(_reportXml, listOfConsumers);
            return listOfConsumers;
        }

        public async Task<ListOfConsumers> ConnectIDVerificationAsync(string ConnectTicket, string IdNumber, string FirstName, string Surname, string YourReference, string VoucherCode)
        {
            ListOfConsumers listOfConsumers = new ListOfConsumers();
            _reportXml = await _XDSReportService.ConnectIDVerificationAsync(ConnectTicket, IdNumber, FirstName, Surname, YourReference, VoucherCode);
            listOfConsumers = (ListOfConsumers)CreateObject(_reportXml, listOfConsumers);
            return listOfConsumers;
        }


        public async Task<ListOfConsumers> ConnectConsumerMatchCreditAssesmentAsync(string ConnectTicket, string EnquiryReason, int ProductId, string IdNumber, string PassportNo, string FirstName, string Surname, string BirthDate, string MaritalStatus, string SpouseFirstName, string SpouseSurname, string ResidentialAddressLine1, string ResidentialAddressLine2, string ResidentialAddressLine3, string ResidentialAddressLine4, string ResidentialPostalCode, string PostalAddressLine1, string PostalAddressLine2, string PostalAddressLine3, string PostalAddressLine4, string PostalPostalCode, string HomeTelCode, string HomeTelNo, string WorkTelCode, string WorkTelNo, string CellularCode, string CellularNo, string EmailAddress, string TotalNetMonthlyIncome, string Employer, string JobTitle, string YourReference, string VoucherCode)
        {
            ListOfConsumers listOfConsumers = new ListOfConsumers();

            _reportXml = await _XDSReportService.ConnectConsumerMatchCreditAssesmentAsync(ConnectTicket, EnquiryReason, ProductId, IdNumber, PassportNo, FirstName, Surname, BirthDate,
                MaritalStatus == null ? "" : MaritalStatus, SpouseFirstName == null ? "" : SpouseFirstName, SpouseSurname == null ? "" : SpouseSurname, ResidentialAddressLine1 == null ? "" : ResidentialAddressLine1,
                ResidentialAddressLine2 == null ? "" : ResidentialAddressLine2, ResidentialAddressLine3 == null ? "" : ResidentialAddressLine3, ResidentialAddressLine4 == null ? "" : ResidentialAddressLine4,
                ResidentialPostalCode == null ? "" : ResidentialPostalCode, PostalAddressLine1 == null ? "" : PostalAddressLine1, PostalAddressLine2 == null ? "" : PostalAddressLine2,
                PostalAddressLine3 == null ? "" : PostalAddressLine3, PostalAddressLine4 == null ? "" : PostalAddressLine4, PostalPostalCode == null ? "" : PostalPostalCode, HomeTelCode == null ? "" : HomeTelCode,
                HomeTelNo == null ? "" : HomeTelNo, WorkTelCode == null ? "" : WorkTelCode, WorkTelNo == null ? "" : WorkTelNo, CellularCode == null ? "" : CellularCode, CellularNo == null ? "" : CellularNo, EmailAddress == null ? "" : EmailAddress,
                TotalNetMonthlyIncome == null ? "" : TotalNetMonthlyIncome, Employer == null ? "" : Employer, JobTitle == null ? "" : JobTitle, YourReference, VoucherCode);

            listOfConsumers = (ListOfConsumers)CreateObject(_reportXml, listOfConsumers);
            return listOfConsumers;
        }

        public async Task<ListOfEnquiryReasons> ConnectGetEnquiryReasonsAsync(string connectTicket)
        {
            ListOfEnquiryReasons listOfReasons = new ListOfEnquiryReasons();
            try
            {
                string reasons = "";
                reasons = await _XDSReportService.ConnectGetEnquiryReasonsAsync(connectTicket);
                listOfReasons = (ListOfEnquiryReasons)CreateObject(reasons, listOfReasons);
            }
            catch (Exception)
            {

                throw;
            }

            return listOfReasons;
        }

        public string ConnectGetEnquiryReasons(string connectTicket)
        {
            return _XDSReportService.ConnectGetEnquiryReasons(connectTicket);
        }

        public async Task<ListOfDirectors> ConnectDirectorMatchAsync(string ConnectTicket, string IdNumber, string FirstName, string Surname, string YourReference, string VoucherCode)
        {
            ListOfDirectors listOfDirectors = new ListOfDirectors();
            _reportXml = await _XDSReportService.ConnectDirectorMatchAsync(ConnectTicket, IdNumber, FirstName, Surname, YourReference, VoucherCode);
            listOfDirectors = (ListOfDirectors)CreateObject(_reportXml, listOfDirectors);
            return listOfDirectors;
        }

        public async Task<ListOfProperties> ConnectDeedsMatchAsync(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.SearchtypeInd intSearchTypeINd, string DeedsOffice, string IDNo, string FirstName, string SurName, string RegNo1, string RegNo2, string RegNo3, string BusinessName, string TownshipName, string ErfNo, string PortionNo, string TitleDeedNo, string BondAccno, string YourReference, string VoucherCode)
        {
            ListOfProperties listOfProperties = new ListOfProperties();
            _reportXml = await _XDSReportService.ConnectDeedsMatchAsync(ConnectTicket, intSearchTypeINd, DeedsOffice, IDNo, FirstName, SurName, RegNo1, RegNo2, RegNo3, BusinessName, TownshipName, ErfNo, PortionNo, TitleDeedNo, BondAccno, YourReference, VoucherCode);
            listOfProperties = (ListOfProperties)CreateObject(_reportXml, listOfProperties);
            return listOfProperties;
        }


        public async Task<ListofHistory> ConnectGetSearchHistoryAsync(string ConnectTicket, int ProductID, string Username)
        {
            ListofHistory listOfHistory = new ListofHistory();
            _reportXml = await _XDSReportService.ConnectGetSearchHistoryAsync(ConnectTicket, ProductID, Username);
            listOfHistory = (ListofHistory)CreateObject(_reportXml, listOfHistory);
            return listOfHistory;
        }

        public async Task<ListofHistory> AdminEnquiryResultHistoryAsync(string ConnectTicket, int EnquiryID, int ProductID, bool GetAll, string username)
        {
            ListofHistory listOfHistory = new ListofHistory();
            _reportXml = await _XDSReportService.AdminEnquiryResultHistoryAsync(ConnectTicket, EnquiryID, ProductID, GetAll, username);
            listOfHistory = (ListofHistory)CreateObject(_reportXml, listOfHistory);
            return listOfHistory;
        }




        public async Task<BlockedConsumer> ConnectFraudGetBlockedConsumers(string ConnectTicket)
        {
            BlockedConsumer listofblockedConsumers = new BlockedConsumer();
            //string testTicket = _XDSAuthService.Login("SBS112", "c@mPu$57eG");
            _reportXml = _XDSAuthService.ConnectFraudGetBlockedConsumers(ConnectTicket);
            listofblockedConsumers = (BlockedConsumer)CreateObject(_reportXml, listofblockedConsumers);
            return listofblockedConsumers;
        }

        public async Task<UnBlockConsumer> ConnectFraudUnBlockConsumer(string ConnectTicket, int BlockID, string Comment)
        {
            UnBlockConsumer UnBlockComsumer = new UnBlockConsumer();
            //string testTicket = _XDSAuthService.Login("SBS112", "c@mPu$57eG");
            _reportXml = _XDSAuthService.ConnectFraudUnBlockConsumer(ConnectTicket, BlockID, Comment);
            UnBlockComsumer = (UnBlockConsumer)CreateObject(_reportXml, UnBlockComsumer);
            return UnBlockComsumer;
        }





        public async Task<PrincipalEnquiryTraceReportModel> GetDirectorEnquiryReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML)
        {
            PrincipalEnquiryTraceReportModel rpt = new PrincipalEnquiryTraceReportModel();
            //Consumer rpt = new Consumer();
            _reportXml = await _XDSReportService.ConnectGetResultAsync(ConnectTicket, EnquiryID, EnquiryResultID, ProductID, BonusXML);
            rpt = (PrincipalEnquiryTraceReportModel)CreateObject(_reportXml, rpt);
            return rpt;
        }

        public async Task<ListOfBusiness> ConnectBusinessMatchAsync(string ConnectTicket, string Reg1, string Reg2, string Reg3, string BusinessName, string VatNo, string SolePropIDNo, string YourReference, string VoucherCode)
        {
            ListOfBusiness listOfBusinesses = new ListOfBusiness();
            _reportXml = await _XDSReportService.ConnectBusinessMatchAsync(ConnectTicket, Reg1, Reg2, Reg3, BusinessName, VatNo, SolePropIDNo, YourReference, VoucherCode);
            listOfBusinesses = (ListOfBusiness)CreateObject(_reportXml, listOfBusinesses);
            return listOfBusinesses;
        }

        //public async Task<BusinessEnquiryTraceReportModel> GetBusinessInvestigativeEnquiryReportAsync(string ConnectTicket, int ReferenceNo)
        public async Task<BusinessEnquiryTraceReportModel> GetBusinessEnquiryReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML)
        {
            BusinessEnquiryTraceReportModel rpt = new BusinessEnquiryTraceReportModel();
            //Consumer rpt = new Consumer();
            //_reportXml = await _XDSReportService.ConnectGetBusinessInvestigativeEnquiryResultAsync(ConnectTicket, ReferenceNo);
            _reportXml = await _XDSReportService.ConnectGetResultAsync(ConnectTicket, EnquiryID, EnquiryResultID, ProductID, BonusXML);
            rpt = (BusinessEnquiryTraceReportModel)CreateObject(_reportXml, rpt);
            return rpt;
        }


        public async Task<PropertyEnquiryReportModel> GetPropertyEnquiryReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML)
        {
            PropertyEnquiryReportModel rpt = new PropertyEnquiryReportModel();
            //Consumer rpt = new Consumer();
            //_reportXml = await _XDSReportService.ConnectGetBusinessInvestigativeEnquiryResultAsync(ConnectTicket, ReferenceNo);
            _reportXml = await _XDSReportService.ConnectGetResultAsync(ConnectTicket, EnquiryID, EnquiryResultID, ProductID, BonusXML);
            rpt = (PropertyEnquiryReportModel)CreateObject(_reportXml, rpt);
            return rpt;
        }


        public async Task<ACFEEnquiryTraceReportModel> GetACFEEnquiryReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML)
        {
            ACFEEnquiryTraceReportModel rpt = new ACFEEnquiryTraceReportModel();
            //Consumer rpt = new Consumer();
            //_reportXml = await _XDSReportService.ConnectGetBusinessInvestigativeEnquiryResultAsync(ConnectTicket, ReferenceNo);
            _reportXml = await _XDSReportService.ConnectGetResultAsync(ConnectTicket, EnquiryID, EnquiryResultID, ProductID, BonusXML);
            rpt = (ACFEEnquiryTraceReportModel)CreateObject(_reportXml, rpt);
            return rpt;
        }




        public async Task<BusinessInvestigateEnquiryRequest> ConnectBusinessInvestigativeEnquiryAsync(string ConnectTicket, string Country, string ReportType, string TimeFrame, string Reg1, string Reg2, string Reg3, string BusinessName, string IDNo, string AdditionalInfo, string CompanyContactNo, string strTerms, double dAmount, string FirstName, string SurName, string TelNo, string EmailAddress, string CompanyName, bool bSendEmail, string VoucherCode, string YourReference)
        {
            BusinessInvestigateEnquiryRequest rpt = new BusinessInvestigateEnquiryRequest();
            //Consumer rpt = new Consumer();
            _reportXml = await _XDSReportService.ConnectBusinessInvestigativeEnquiryAsync(ConnectTicket, Country, ReportType, TimeFrame, Reg1, Reg2, Reg3, BusinessName, IDNo, AdditionalInfo, CompanyContactNo, strTerms, dAmount, FirstName, SurName, TelNo, EmailAddress, CompanyName, bSendEmail, VoucherCode, YourReference);
            rpt = (BusinessInvestigateEnquiryRequest)CreateObject(_reportXml, rpt);
            return rpt;
        }

        public async Task<ListOfRequestTypes> ConnectGetBankCodeRequestTypeAsync(string ConnectTicket)
        {
            ListOfRequestTypes requestTypes = new ListOfRequestTypes();
            string types = await _XDSReportService.ConnectGetBankCodeRequestTypeAsync(ConnectTicket);
            requestTypes = (ListOfRequestTypes)CreateObject(types, requestTypes);
            return requestTypes;
        }

        public async Task<ListOfBanks> ConnectGetBankNamesAsync(string ConnectTicket)
        {
            ListOfBanks listOfBanks = new ListOfBanks();
            string banks = await _XDSReportService.ConnectGetBankNamesAsync(ConnectTicket);
            listOfBanks = (ListOfBanks)CreateObject(banks, listOfBanks);
            return listOfBanks;
        }

        public async Task<ListOfBanks> ConnectBCGetBankNamesAsync(string ConnectTicket)
        {
            ListOfBanks listOfBanks = new ListOfBanks();
            string banks = await _XDSReportService.ConnectBCGetBankNamesAsync(ConnectTicket);
            listOfBanks = (ListOfBanks)CreateObject(banks, listOfBanks);
            return listOfBanks;
        }


        public async Task<Result> ConnectGetDeedsOfficesAsync(string ConnectTicket)
        {
            Result listOfDeedsOffice = new Result();
            string Office = await _XDSReportService.ConnectGetDeedsOfficesAsync(ConnectTicket);
            listOfDeedsOffice = (Result)CreateObject(Office, listOfDeedsOffice);
            return listOfDeedsOffice;
        }


        public async Task<ListOfProducts> ConnectGetProductListAsync(string ConnectTicket)
        {
            ListOfProducts listOfProducts = new ListOfProducts();
            string Product = await _XDSReportService.ConnectGetProductListAsync(ConnectTicket);
            listOfProducts = (ListOfProducts)CreateObject(Product, listOfProducts);
            return listOfProducts;
        }



        public async Task<ListOfTerms> ConnectGetTermsAsync(string ConnectTicket, int ProductID)
        {
            ListOfTerms listOfTerms = new ListOfTerms();
            _reportXml = await _XDSReportService.ConnectGetTermsAsync(ConnectTicket, ProductID);
            listOfTerms = (ListOfTerms)CreateObject(_reportXml, listOfTerms);
            return listOfTerms;
        }

        public async Task<BankCodesEnquiryRequest> ConnectBankCodesRequestAsync(string ConnectTicket, string BankCodeRequestType, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.TypeofVerificationenum verficationType, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.Entity EntityType, string IDno, string Reg1, string Reg2, string Reg3, string TrustNo, string BankName, string BranchName, string BranchCode, string AccNo, string AccHolderName, double Amount, string Terms, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.Region BankCodeRegion, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.CountryCode BankCodeCountryCode, string AdditionalInfo, string FirstName, string SurName, string CompanyName, string TelNo, string EmailAddress, bool bSendEmail, string VoucherCode, string YourReference)
        {
            BankCodesEnquiryRequest rpt = new BankCodesEnquiryRequest();
            _reportXml = await _XDSReportService.ConnectBankCodesRequestAsync(ConnectTicket, BankCodeRequestType, verficationType, EntityType, IDno, Reg1, Reg2, Reg3, TrustNo, BankName, BranchName, BranchCode, AccNo, AccHolderName, Amount, Terms, BankCodeRegion, BankCodeCountryCode, AdditionalInfo, FirstName, SurName, CompanyName, TelNo, EmailAddress, bSendEmail, VoucherCode, YourReference);
            rpt = (BankCodesEnquiryRequest)CreateObject(_reportXml, rpt);
            return rpt;
        }

        /// <summary>
        /// Not being used?
        /// </summary>
        /// <returns></returns>
        public async Task<BankCodesEnquiryReportModel> ConnectGetBankCodeResultAsync(string ConnectTicket, int EnquiryLogID)
        {
            BankCodesEnquiryReportModel rpt = new BankCodesEnquiryReportModel();
            _reportXml = await _XDSReportService.ConnectGetBankCodeResultAsync(ConnectTicket, EnquiryLogID);
            rpt = (BankCodesEnquiryReportModel)CreateObject(_reportXml, rpt);
            return rpt;
        }

        public async Task<CountryList> ConnectGetCountryCodesAsync(string ConnectTicket)
        {
            CountryList listOfCountries = new CountryList();
            _reportXml = await _XDSReportService.ConnectGetCountryCodesAsync(ConnectTicket);
            listOfCountries = (CountryList)CreateObject(_reportXml, listOfCountries);
            return listOfCountries;
        }

        public async Task<ListReportTypes> ConnectGetReportTypesAsync(string ConnectTicket, int CountryID)
        {
            ListReportTypes listOfReportTypes = new ListReportTypes();
            _reportXml = await _XDSReportService.ConnectGetReportTypesAsync(ConnectTicket, CountryID);
            listOfReportTypes = (ListReportTypes)CreateObject(_reportXml, listOfReportTypes);
            return listOfReportTypes;
        }

        public async Task<ListofBankAccountTypes> ConnectGetAccountTypesAsync(string ConnectTicket)
        {
            ListofBankAccountTypes listOfBankAccountTypes = new ListofBankAccountTypes();
            string banks = await _XDSReportService.ConnectGetAccountTypesAsync(ConnectTicket);
            listOfBankAccountTypes = (ListofBankAccountTypes)CreateObject(banks, listOfBankAccountTypes);
            return listOfBankAccountTypes;
        }


        //ConnectAccountVerificationAsync
        public async Task<AccountVerification> ConnectAccountVerificationAsync(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.TypeofVerificationenum VerificationType, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.Entity Entity, string Initials, string SurName, string IDNo, string CompanyName, string Reg1, string Reg2, string Reg3, string TrustNo, string AccNo, string BranchCode, string Acctype, string BankName, string ClientFirstName, string ClientSurName, string EmailAddress, bool bSendEmail, string VoucherCode, string YourReference)
        {
            AccountVerification rpt = new AccountVerification();
            //Consumer rpt = new Consumer();
            _reportXml = await _XDSReportService.ConnectAccountVerificationAsync(ConnectTicket, VerificationType, Entity, Initials, SurName, IDNo, CompanyName, Reg1, Reg2, Reg3, TrustNo, AccNo, BranchCode, Acctype, BankName, ClientFirstName, ClientSurName, EmailAddress, bSendEmail, VoucherCode, YourReference);
            rpt = (AccountVerification)CreateObject(_reportXml, rpt);
            return rpt;
        }


        public async Task<AccountVerification> ConnectAccountVerificationRealTimeAsync(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.TypeofVerificationenum VerificationType, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.Entity Entity, string Initials, string SurName, string IDNo, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AVSIDType IDType, string CompanyName, string Reg1, string Reg2, string Reg3, string TrustNo, string AccNo, string BranchCode, string Acctype, string BankName, string VoucherCode, string YourReference)
        {
            AccountVerification rpt = new AccountVerification();
            //Consumer rpt = new Consumer();
            _reportXml = await _XDSReportService.ConnectAccountVerificationRealTimeAsync(ConnectTicket, VerificationType, Entity, Initials, SurName, IDNo, IDType, CompanyName, Reg1, Reg2, Reg3, TrustNo, AccNo, BranchCode, Acctype, BankName, VoucherCode, YourReference);
            rpt = (AccountVerification)CreateObject(_reportXml, rpt);
            return rpt;
        }


        //

        public async Task<ListOfConsumers> ConnectIDPhotoVerificationAsync(string ConnectTicket, string IdNumber, string FirstName, string Surname, string YourReference, string VoucherCode)
        {
            ListOfConsumers listOfConsumers = new ListOfConsumers();
            //_reportXml = await _XDSReportService.ConnectIDVerificationAsync(ConnectTicket, IdNumber, FirstName, Surname, YourReference, VoucherCode);
            _reportXml = await _XDSReportService.ConnectIDPhotoVerificationAsync(ConnectTicket, IdNumber, FirstName, Surname, YourReference, VoucherCode);
            listOfConsumers = (ListOfConsumers)CreateObject(_reportXml, listOfConsumers);
            return listOfConsumers;
        }

        public async Task<IDPhotoVerificationTraceReportModel> GetIDPhotoVerificationTraceReportAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML)
        {
            IDPhotoVerificationTraceReportModel rpt = new IDPhotoVerificationTraceReportModel();
            //Consumer rpt = new Consumer();
            _reportXml = await _XDSReportService.ConnectGetResultAsync(ConnectTicket, EnquiryID, EnquiryResultID, ProductID, BonusXML);
            rpt = (IDPhotoVerificationTraceReportModel)CreateObject(_reportXml, rpt);
            return rpt;
        }


        public async Task<Profile> ConnectFraudGetProfileAsync(string ConnectTicket)
        {
            Profile profile = new Profile();
            string profileResponse = await _XDSReportService.ConnectFraudGetProfileAsync(ConnectTicket);
            profile = (Profile)CreateObject(profileResponse, profile);
            return profile;
        }

        public async Task<ListOfConsumers> ConnectFraudConsumerMatchAsync(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationSearchType SearchType, string BranchCode, int PurposeID, string IdNumber, string PassportNo, string CellularCode, string CellularNo, string AccountNo, string SubaccountNo, string YourReference, string VoucherCode)
        {
            ListOfConsumers listOfConsumers = new ListOfConsumers();
            //_reportXml = await _XDSReportService.ConnectIDVerificationAsync(ConnectTicket, IdNumber, FirstName, Surname, YourReference, VoucherCode);
            _reportXml = await _XDSReportService.ConnectFraudConsumerMatchAsync(ConnectTicket, SearchType, BranchCode, PurposeID, IdNumber, PassportNo, CellularCode, CellularNo, AccountNo, SubaccountNo, YourReference, VoucherCode);
            listOfConsumers = (ListOfConsumers)CreateObject(_reportXml, listOfConsumers);
            return listOfConsumers;
        }


        public async Task<ListOfConsumers> ConnectFraudConsumerMatchWithOverrideOTPAsync(string ConnectTicket,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationSearchType SearchType,
                    string BranchCode,
                    int PurposeID,
                    string IdNumber,
                    string PassportNo,
                    string CellularCode,
                    string CellularNo,
                    string AccountNo,
                    string SubaccountNo,
                    bool OverrideOTP,
                    XDS.ONLINE.REPOSITORY.XDSReportServiceRef.OverrideOTPReasons OverrideOTPReason,
                    string OverrideOTPComment,
                    string EmailAddress,
                    string YourReference,
                    string VoucherCode)
        {
            ListOfConsumers listOfConsumers = new ListOfConsumers();
            //_reportXml = await _XDSReportService.ConnectIDVerificationAsync(ConnectTicket, IdNumber, FirstName, Surname, YourReference, VoucherCode);
            _reportXml = await _XDSReportService.ConnectFraudConsumerMatchWithOverrideOTPAsync(ConnectTicket, SearchType, BranchCode, PurposeID, IdNumber, PassportNo, CellularCode, CellularNo, AccountNo, SubaccountNo, OverrideOTP, OverrideOTPReason, OverrideOTPComment, EmailAddress, YourReference, VoucherCode);
            listOfConsumers = (ListOfConsumers)CreateObject(_reportXml, listOfConsumers);
            return listOfConsumers;
        }



        public async Task<AuthenticationProcess> ConnectFraudGetQuestionsAsync(string ConnectTicket, int EnquiryID, int EnquiryResultID)
        {
            AuthenticationProcess rpt = new AuthenticationProcess();
            //_reportXml = await _XDSReportService.ConnectFraudGetQuestionsAsync(ConnectTicket, EnquiryID, EnquiryResultID);
            var _reportXml = await _XDSReportService.ConnectFraudGetQuestionsAsync(ConnectTicket, EnquiryID, EnquiryResultID);
            string stringResponse = string.Empty;

            XmlSerializer serializer = new XmlSerializer(_reportXml.GetType());

            using (StringWriter writer = new StringWriter())
            {
                serializer.Serialize(writer, _reportXml);

                stringResponse = writer.ToString();
            }

            //rpt = (ConnectFraudGetQuestionsResult)CreateObject(_reportXml.ToString(), rpt);
            rpt = (AuthenticationProcess)CreateObject(stringResponse.ToString(), rpt);
            return rpt;
        }

        public async Task<AuthenticationProcess> GetHistoryReportAsync(string ConnectTicket, int EnquiryResultID)
        {
            AuthenticationProcess rpt = new AuthenticationProcess();
            //Consumer rpt = new Consumer();
            _reportXml = _XDSReportService.ConnectGetHistoryReportAsync(ConnectTicket, EnquiryResultID);
            rpt = (AuthenticationProcess)CreateObject(_reportXml, rpt);
            return rpt;


        }

        public async Task<AuthenticationProcess> ConnectFraudProcessAsync(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationProcessAction ProcessAction, AuthenticationProcess MyAuthenticationProcess, string Comment)
        {
            AuthenticationProcess rpt = new AuthenticationProcess();
            XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationProcess sendAuthenticationProcess = new XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationProcess();

            string stringResponse = string.Empty;

            XmlSerializer serializer = new XmlSerializer(MyAuthenticationProcess.GetType());

            using (StringWriter writer = new StringWriter())
            {
                serializer.Serialize(writer, MyAuthenticationProcess);

                stringResponse = writer.ToString();
            }

            //rpt = (ConnectFraudGetQuestionsResult)CreateObject(_reportXml.ToString(), rpt);
            sendAuthenticationProcess = (XDS.ONLINE.REPOSITORY.XDSReportServiceRef.AuthenticationProcess)CreateObject(stringResponse.ToString(), sendAuthenticationProcess);
            //return rpt;

            //Send Submission
            var _reportXml = await _XDSReportService.ConnectFraudProcessAsync(ConnectTicket, ProcessAction, sendAuthenticationProcess, Comment);

            //get response
            XmlSerializer serializer1 = new XmlSerializer(_reportXml.GetType());

            using (StringWriter writer = new StringWriter())
            {
                serializer1.Serialize(writer, _reportXml);

                stringResponse = writer.ToString();
            }

            //rpt = (ConnectFraudGetQuestionsResult)CreateObject(_reportXml.ToString(), rpt);
            rpt = (AuthenticationProcess)CreateObject(stringResponse.ToString(), rpt);
            return rpt;
        }


        #region Authenticate Service

        public async Task<ListOfConsumers> AutheticateConnectFraudConsumerMatch(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.AuthenticationSearchType SearchType, string BranchCode, int PurposeID, string IdNumber, string PassportNo, string CellularCode, string CellularNo, string AccountNo, bool OverrideOTP, XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.OverrideOTPReasons OverrideOTPReason, string OverrideOTPComment, string SubaccountNo, string EmailAddress, string YourReference, string VoucherCode)
        {
            ListOfConsumers listOfConsumers = new ListOfConsumers();
            //string testTicket = _XDSAuthService.Login("SBS112", "c@mPu$57eG");
            _reportXml = _XDSAuthService.ConnectFraudConsumerMatch(ConnectTicket, SearchType, BranchCode, PurposeID, IdNumber, PassportNo, CellularCode, CellularNo, AccountNo, OverrideOTP, OverrideOTPReason, OverrideOTPComment, SubaccountNo, EmailAddress, YourReference, VoucherCode);
            listOfConsumers = (ListOfConsumers)CreateObject(_reportXml, listOfConsumers);
            return listOfConsumers;
        }

        public async Task<Profile> AuthenticateConnectFraudGetProfile(string ConnectTicket)
        {
            Profile profile = new Profile();
            //string testTicket = _XDSAuthService.Login("SBS112", "c@mPu$57eG");
            string _reportXml = _XDSAuthService.ConnectFraudGetProfile(ConnectTicket);
            profile = (Profile)CreateObject(_reportXml, profile);
            return profile;
        }

        public async Task<AuthenticationProcess> AuthenticateConnectFraudGetQuestions(string ConnectTicket, int EnquiryID, int EnquiryResultID)
        {
            AuthenticationProcess rpt = new AuthenticationProcess();

            //string testTicket = _XDSAuthService.Login("SBS112", "c@mPu$57eG");
            XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.AuthenticationProcess _reportXml = _XDSAuthService.ConnectFraudGetQuestions(ConnectTicket, EnquiryID, EnquiryResultID);
            //object _reportXml = null;
            string stringResponse = string.Empty;

            XmlSerializer serializer = new XmlSerializer(_reportXml.GetType());

            using (StringWriter writer = new StringWriter())
            {
                serializer.Serialize(writer, _reportXml);

                stringResponse = writer.ToString();
            }

            //rpt = (ConnectFraudGetQuestionsResult)CreateObject(_reportXml.ToString(), rpt);
            rpt = (AuthenticationProcess)CreateObject(stringResponse.ToString(), rpt);
            return rpt;
        }

        //        public async Task<AuthenticationProcess> AuthenticateConnectFraudProcess(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.AuthenticationProcessAction ProcessAction, XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.AuthenticationProcess MyAuthenticationProcess, string Comment)
        public async Task<AuthenticationProcess> AuthenticateConnectFraudProcess(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.AuthenticationProcessAction ProcessAction, AuthenticationProcess MyAuthenticationProcess, string Comment)
        {
            AuthenticationProcess rpt = new AuthenticationProcess();

            //  string testTicket = _XDSAuthService.Login("SBS112", "c@mPu$57eG");
            XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.AuthenticationProcess sendAuthenticationProcess = new XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.AuthenticationProcess();

            string stringResponse = string.Empty;

            XmlSerializer serializer = new XmlSerializer(MyAuthenticationProcess.GetType());

            using (StringWriter writer = new StringWriter())
            {
                serializer.Serialize(writer, MyAuthenticationProcess);

                stringResponse = writer.ToString();
            }

            //rpt = (ConnectFraudGetQuestionsResult)CreateObject(_reportXml.ToString(), rpt);
            sendAuthenticationProcess = (XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.AuthenticationProcess)CreateObject(stringResponse.ToString(), sendAuthenticationProcess);
            //return rpt;

            //Send Submission
            var _reportXml2 = _XDSAuthService.ConnectFraudProcess(ConnectTicket, ProcessAction, sendAuthenticationProcess, Comment);

            //get response
            XmlSerializer serializer1 = new XmlSerializer(_reportXml2.GetType());

            using (StringWriter writer = new StringWriter())
            {
                serializer1.Serialize(writer, _reportXml2);

                stringResponse = writer.ToString();
                _reportXml = stringResponse;
            }

            //rpt = (ConnectFraudGetQuestionsResult)CreateObject(_reportXml.ToString(), rpt);
            rpt = (AuthenticationProcess)CreateObject(stringResponse.ToString(), rpt);
            return rpt;
        }

        public async Task<PersonalQuestions> AuthenticateConnectFraudSavePersonalQuestions(string ConnectTicket, AuthenticationProcess MyAuthenticationProcess, string Comment)
        {
            PersonalQuestions response = new PersonalQuestions();

            AuthenticationProcess rpt = new AuthenticationProcess();
            //  string testTicket = _XDSAuthService.Login("SBS112", "c@mPu$57eG");
            XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.AuthenticationProcess sendAuthenticationProcess = new XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.AuthenticationProcess();

            string stringResponse = string.Empty;

            XmlSerializer serializer = new XmlSerializer(MyAuthenticationProcess.GetType());

            using (StringWriter writer = new StringWriter())
            {
                serializer.Serialize(writer, MyAuthenticationProcess);

                stringResponse = writer.ToString();
            }

            //rpt = (ConnectFraudGetQuestionsResult)CreateObject(_reportXml.ToString(), rpt);
            sendAuthenticationProcess = (XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.AuthenticationProcess)CreateObject(stringResponse.ToString(), sendAuthenticationProcess);
            //return rpt;

            string _reportXml = _XDSAuthService.ConnectFraudSavePersonalQuestions(ConnectTicket, sendAuthenticationProcess, Comment);
            response = (PersonalQuestions)CreateObject(_reportXml, response);

            if (!string.IsNullOrEmpty(ErrorMessage()))
            {
                response = new PersonalQuestions();
                response.Result = ErrorMessage();
            }

            return response;
        }

        public async Task<BonusSegments> AuthenticateConnectFraudGetBonusSegments(string ConnectTicket, int EnquiryResultID)
        {
            BonusSegments oBonusSegments = new BonusSegments();

            var _reportXml = _XDSAuthService.ConnectFraudGetBonusSegments(ConnectTicket, EnquiryResultID);

            oBonusSegments = (BonusSegments)CreateObject(_reportXml, oBonusSegments);

            return oBonusSegments;
        }

        public async Task<UnblockReasons> AuthenticateConnectFraudGetUnblockReasons(string ConnectTicket)
        {
            UnblockReasons oUnblockReasons = new UnblockReasons();

            var _reportXml = _XDSAuthService.ConnectFraudGetUnblockReasons(ConnectTicket);

            oUnblockReasons = (UnblockReasons)CreateObject(_reportXml, oUnblockReasons);

            return oUnblockReasons;
        }

        public async Task<UnBlockConsumer> AuthenticateConnectFraudUnblockConsumerWithReason(string ConnectTicket, int BlockID, string UnblockReason, string Comment)
        {
            UnBlockConsumer oUnBlockConsumer = new UnBlockConsumer();

            var _reportXml = _XDSAuthService.ConnectFraudUnblockConsumerWithReason(ConnectTicket, BlockID, UnblockReason, Comment);

            oUnBlockConsumer = (UnBlockConsumer)CreateObject(_reportXml, oUnBlockConsumer);

            return oUnBlockConsumer;
        }

        public async Task<ConsumerInfoDocument> AuthenticateConnectFraudGetConsumerInfo(string ConnectTicket, long ConsumerID)
        {
            ConsumerInfoDocument consumerInfoDocument = new ConsumerInfoDocument();
            string stringResponse = string.Empty;

            //Send Submission
            var _reportXml = _XDSAuthService.ConnectFraudGetConsumerInfo(ConnectTicket, ConsumerID);

            //get response
            XmlSerializer serializer1 = new XmlSerializer(_reportXml.GetType());

            using (StringWriter writer = new StringWriter())
            {
                serializer1.Serialize(writer, _reportXml);
                stringResponse = writer.ToString();
            }

            consumerInfoDocument = (ConsumerInfoDocument)CreateObject(stringResponse.ToString(), consumerInfoDocument);

            return consumerInfoDocument;
        }

        public async Task<UpdateConsumerInfo> AuthenticateConnectFraudUpdateConsumerInfo(string ConnectTicket, long ConsumerID, int EnquiryID, int EnquiryResultID, string Address1, string Address2, string Address3, string Address4, string PostalCode)
        {
            UpdateConsumerInfo oUpdateConsumerInfo = new UpdateConsumerInfo();

            //send update
            var _reportXml = _XDSAuthService.ConnectFraudUpdateConsumerInfo(ConnectTicket, ConsumerID, EnquiryID, EnquiryResultID, Address1, Address2, Address3, Address4, PostalCode);

            oUpdateConsumerInfo = (UpdateConsumerInfo)CreateObject(_reportXml, oUpdateConsumerInfo);

            return oUpdateConsumerInfo;
        }

        public async Task<OTPNumbers> AuthenticateConnectFraudGetAuthenticationOTPNumbers(string ConnectTicket, long SubscriberAuthenticationID)
        {
            OTPNumbers oOTPNumbers = new OTPNumbers();

            _reportXml = _XDSAuthService.ConnectFraudGetAuthenticationOTPNumbers(ConnectTicket, SubscriberAuthenticationID);

            oOTPNumbers = (OTPNumbers)CreateObject(_reportXml, oOTPNumbers);

            return oOTPNumbers;
        }

        public async Task<AuthenticationProcess> AuthenticateConnectFraudResendOTP(string ConnectTicket, AuthenticationProcess MyAuthenticationProcess, int EnquiryID, int EnquiryResultID)
        {
            AuthenticationProcess oAuthenticationProcess = new AuthenticationProcess();

            XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.AuthenticationProcess sendAuthenticationProcess = new XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.AuthenticationProcess();

            string stringResponse = string.Empty;

            XmlSerializer serializer = new XmlSerializer(MyAuthenticationProcess.GetType());

            using (StringWriter writer = new StringWriter())
            {
                serializer.Serialize(writer, MyAuthenticationProcess);

                stringResponse = writer.ToString();
            }

            sendAuthenticationProcess = (XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.AuthenticationProcess)CreateObject(stringResponse.ToString(), sendAuthenticationProcess);

            var _reportXml = _XDSAuthService.ConnectFraudResendOTP(ConnectTicket, sendAuthenticationProcess, EnquiryID, EnquiryResultID);

            //get response
            XmlSerializer serializer1 = new XmlSerializer(_reportXml.GetType());

            using (StringWriter writer = new StringWriter())
            {
                serializer1.Serialize(writer, _reportXml);

                stringResponse = writer.ToString();
            }

            oAuthenticationProcess = (AuthenticationProcess)CreateObject(stringResponse.ToString(), oAuthenticationProcess);

            return oAuthenticationProcess;
        }

        public async Task<FraudScore> AuthenticateConnectFraudGetScore(string ConnectTicket,
            XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.FraudScoreTitle FraudScoreTitle, XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.FraudScoreChannel FraudScoreChannel, XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.FraudScoreStore FraudScoreStore,
            int Salary, int MonthsEmployed, int EnquiryID, int EnquiryResultID)
        {
            FraudScore oFraudScore = new FraudScore();

            _reportXml = _XDSAuthService.ConnectFraudGetScore(ConnectTicket, FraudScoreTitle, FraudScoreChannel, FraudScoreStore, Salary, MonthsEmployed, EnquiryID, EnquiryResultID);

            oFraudScore = (FraudScore)CreateObject(_reportXml, oFraudScore);

            return oFraudScore;
        }

        public async Task<IDPhotoVerificationTraceReportModel> AuthenticateConnectFraudGetIDPhoto(string ConnectTicket, int EnquiryID, int EnquiryResultID, string BonusXML)
        {
            IDPhotoVerificationTraceReportModel oIDPhotoVerificationTraceReportModel = new IDPhotoVerificationTraceReportModel();

            var _reportXml = _XDSAuthService.ConnectFraudGetIDPhoto(ConnectTicket, EnquiryID, EnquiryResultID, BonusXML);

            oIDPhotoVerificationTraceReportModel = (IDPhotoVerificationTraceReportModel)CreateObject(_reportXml, oIDPhotoVerificationTraceReportModel);

            return oIDPhotoVerificationTraceReportModel;
        }

        public async Task<ConsumerTraceReportModel> AuthenticateConnectFraudGetCreditReport(string ConnectTicket, int EnquiryID, int EnquiryResultID, string BonusXML)
        {
            ConsumerTraceReportModel oConsumerTraceReportModel = new ConsumerTraceReportModel();

            var _reportXml = _XDSAuthService.ConnectFraudGetCreditReport(ConnectTicket, EnquiryID, EnquiryResultID, BonusXML);

            oConsumerTraceReportModel = (ConsumerTraceReportModel)CreateObject(_reportXml, oConsumerTraceReportModel);

            return oConsumerTraceReportModel;
        }

        public async Task<AccountVerification> AuthenticateConnectFraudAccountVerification(string ConnectTicket, XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.TypeofVerificationenum VerificationType, string Initials, string SurName, string AccNo, string BranchCode,
            string Acctype, string BankName, int EnquiryID, int EnquiryResultID)
        {
            AccountVerification oAccountVerification = new AccountVerification();

            var _reportXml = _XDSAuthService.ConnectFraudAccountVerification(ConnectTicket, VerificationType, Initials, SurName, AccNo, BranchCode, Acctype, BankName, EnquiryID, EnquiryResultID);

            oAccountVerification = (AccountVerification)CreateObject(_reportXml, oAccountVerification);

            return oAccountVerification;
        }

        public async Task<AccountVerificationResult> AuthenticateConnectFraudGetAccountVerificationResult(string ConnectTicket, int EnquiryLogID)
        {
            AccountVerificationResult oAccountVerificationResult = new AccountVerificationResult();

            _reportXml = _XDSAuthService.ConnectFraudGetAccountVerificationResult(ConnectTicket, EnquiryLogID);

            if (_reportXml.Contains("ACCOUNT-OPEN"))
                _reportXml = _reportXml.Replace("ACCOUNT-OPEN", "ACCOUNTOPEN");

            oAccountVerificationResult = (AccountVerificationResult)CreateObject(_reportXml, oAccountVerificationResult);

            return oAccountVerificationResult;
        }

        public async Task<string> AuthenticateConnectFraudCheckBlockingStatus(string ConnectTicket, int EnquiryID, int EnquiryResultID)
        {
            return _XDSAuthService.AuthenticateConnectFraudCheckBlockingStatus(ConnectTicket, EnquiryID, EnquiryResultID);
        }
        #endregion

        public async System.Threading.Tasks.Task<string> AdminChangeUserDetailsAsync(string ConnectTicket, string FirstName, string Surname, string Email, string JobTitle, string JobPosition, XDS.ONLINE.REPOSITORY.XDSCustomVetting.UserRole Role, bool isActive)
        {
            return await _XDSCustomVettingService.AdminChangeUserDetailsAsync(ConnectTicket, FirstName, Surname, Email, JobTitle, JobPosition, Role, isActive);
        }

        public async Task<string> AdminChangePasswordAsync(string XDSConnectTicket, string CurrentPassword, string NewPassword, string ConfirmPassword)
        {
            return await _XDSCustomVettingService.AdminChangePasswordAsync(XDSConnectTicket, CurrentPassword, NewPassword, ConfirmPassword);

        }

        public System.Threading.Tasks.Task<bool> CheckifUserExistsAsync(string XDSConnectTicket, string username, string cellularnumber)
        {
            return _XDSCustomVettingService.CheckifUserExistsAsync(XDSConnectTicket,username,cellularnumber);
        }

        public bool CheckifUserExists(string XDSConnectTicket, string username, string cellularnumber)
        {
            return _XDSCustomVettingService.CheckifUserExists(XDSConnectTicket,username,cellularnumber);
        }

        public string GenerateResetPasswordCode(string ticket, string username, string cellularnumber,  string issueduser)
        {
            return _XDSCustomVettingService.GenerateResetPasswordCode(ticket, username, cellularnumber,issueduser);
        }

        public string GetPasswordResetCodeStatus(string ticket, string username, string cellularnumber, string Code)
        {
            return _XDSCustomVettingService.GetPasswordResetCodeStatus(ticket, username, cellularnumber, Code);
        }

        public async Task<string> AdminResetPasswordAsync(string XDSConnectTicket, string Username, string Cellnumber, string NewPassword,string ConfirmPassword, string Code)
        {
            return await _XDSCustomVettingService.AdminResetPasswordAsync(XDSConnectTicket, Username,Cellnumber, NewPassword, ConfirmPassword,Code);

        }

        public ListofTrainingDoc CustomVettingGetTrainingDoc(string XDSConnectTicket)
        {
            ListofTrainingDoc rpt = new ListofTrainingDoc();

            _reportXml = _XDSCustomVettingService.GetTrainingDoc(XDSConnectTicket);

            rpt = (ListofTrainingDoc)CreateObject(_reportXml, rpt);

            return rpt;
        }
        public async Task<ListofTrainingDoc> CustomVettingGetTrainingDocAsync(string XDSConnectTicket)
        {
            ListofTrainingDoc rpt = new ListofTrainingDoc();

            _reportXml = await _XDSCustomVettingService.GetTrainingDocAsync(XDSConnectTicket);

            rpt = (ListofTrainingDoc)CreateObject(_reportXml, rpt);

            return rpt;
        }

        public ListofTrainingDoc CustomVettingGetTrainingDocFile(string XDSConnectTicket, long TrainingID)
        {
            ListofTrainingDoc rpt = new ListofTrainingDoc();

            _reportXml = _XDSCustomVettingService.GetTrainingDocFile(XDSConnectTicket, TrainingID);

            rpt = (ListofTrainingDoc)CreateObject(_reportXml, rpt);

            return rpt;
        }
        public async Task<ListofTrainingDoc> CustomVettingGetTrainingDocFileAsync(string XDSConnectTicket, long TrainingID)
        {
            ListofTrainingDoc rpt = new ListofTrainingDoc();

            _reportXml = await _XDSCustomVettingService.GetTrainingDocFileAsync(XDSConnectTicket, TrainingID);

            rpt = (ListofTrainingDoc)CreateObject(_reportXml, rpt);

            return rpt;
        }


        #region helpers
        private Object CreateObject(string XMLString, Object YourClassObject)
        {
            if (XMLString.IndexOf("NoResult") >= 0)
            {
                //e.g. <NoResult><Error>Invalid IDno Supplied</Error></NoResult>
                //     <NoResult><NotFound>No Record Found</NotFound></NoResult>
                _hasError = true;

                XmlDocument xml = new XmlDocument();
                xml.LoadXml(XMLString);
                switch (xml.DocumentElement.FirstChild.Name)
                {
                    case "Error":
                    case "NotFound":
                        _errorMessage = xml.DocumentElement.FirstChild.InnerText;
                        break;
                    case "Validate":
                        _errorMessage = xml.DocumentElement.FirstChild.InnerText;
                        break;
                    default:
                        //todo: log error - not expected.
                        _errorMessage = string.Format("No result ({0})", xml.DocumentElement.FirstChild.Name);
                        break;
                }
                xml = null;
                YourClassObject = null;
            }
            else
            {

                try
                {
                    XmlSerializer oXmlSerializer = new XmlSerializer(YourClassObject.GetType());
                    //The StringReader will be the stream holder for the existing XML file 
                    YourClassObject = oXmlSerializer.Deserialize(new StringReader(XMLString));
                    //initially deserialized, the data is represented by an object without a defined type 
                }
                catch (Exception ex)
                {

                    throw ex;
                }


            }

            return YourClassObject;
        }

        #endregion



    }
}
