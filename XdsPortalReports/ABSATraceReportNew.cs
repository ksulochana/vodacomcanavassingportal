using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace XdsPortalReports
{
    public partial class ABSATraceReport : DevExpress.XtraReports.UI.XtraReport
    {
        public ABSATraceReport(string strXML)
        {
            InitializeComponent();
            System.IO.StringReader Objsr = new System.IO.StringReader(strXML);
            DataSet dsXML = new DataSet();
            dsXML.ReadXml(Objsr);
            //NameHistory.Visible = false;
            tblNameHistH.Visible = false;
            tblNameHisC.Visible = false;
            DetailNameHistory.Visible = false;

            if (dsXML.Tables.Contains("SubscriberInputDetails"))
            {
                dsXML.Tables["SubscriberInputDetails"].Columns.Add("FEnquiryDate", System.Type.GetType("System.DateTime"), "Iif(EnquiryDate = '', '1900-01-01T00:00:00', EnquiryDate)");
                this.lblEnquiryDateValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "SubscriberInputDetails.FEnquiryDate", "{0:dd/MM/yyyy - hh:mm:ss}") });
            }

            if (!(dsXML.Tables.Contains("ConsumerDetail")))
            {
                DetailConsumerDetail.Visible = false;
                lblPersonalDetail.Text += " - (No Data Available)";
            }
           
            if (!(dsXML.Tables.Contains("ConsumerScoring")))
            {
                DetailScoring.Visible = false;
                lblScore.Text += " - (No Data Available)";
            }
      
            if (!(dsXML.Tables.Contains("ConsumerDebtSummary")))
            {
                DetailDebtSummary.Visible = false;
                lblDebtHeader.Text += " - (No Data Available)";
            }
            else
            {
               // dsXML.Tables["ConsumerDebtSummary"].Columns.Add("FMostRecentJudgmentDate", System.Type.GetType("System.DateTime"), "Iif(MostRecentJudgmentDate = '', '1900-01-01T00:00:00', MostRecentJudgmentDate)");
                dsXML.Tables["ConsumerDebtSummary"].Columns.Add("FTotalMonthlyInstallment", System.Type.GetType(" System.Single"), "Iif(TotalMonthlyInstallment = '', 0, TotalMonthlyInstallment)");
                dsXML.Tables["ConsumerDebtSummary"].Columns.Add("FTotalOutStandingDebt", System.Type.GetType(" System.Single"), "Iif(TotalOutStandingDebt = '', 0, TotalOutStandingDebt)");
                dsXML.Tables["ConsumerDebtSummary"].Columns.Add("FTotalArrearAmount", System.Type.GetType(" System.Single"), "Iif(TotalArrearAmount = '', 0, TotalArrearAmount)");
                dsXML.Tables["ConsumerDebtSummary"].Columns.Add("FHighestJudgmentAmount", System.Type.GetType(" System.Single"), "Iif(HighestJudgmentAmount = '', 0, HighestJudgmentAmount)");
                dsXML.Tables["ConsumerDebtSummary"].Columns.Add("FTotalAdverseAmount", System.Type.GetType(" System.Single"), "Iif(TotalAdverseAmount = '', 0, TotalAdverseAmount)");

                //this.lblJdgDateValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.FMostRecentJudgmentDate", "{0:dd/MM/yyyy}") });
                this.lblMonthlyinstValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.FTotalMonthlyInstallment", "R {0}") });
                this.lblOutstandingDebtValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.FTotalOutStandingDebt", "R {0}") });
                this.lblArrearamtvalue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.FTotalArrearAmount", "R {0}") });
                this.lblJudAmtValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.FHighestJudgmentAmount", "R {0}") });
                this.lblAdverseAmtValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.FTotalAdverseAmount", "R {0}") });
            }

            if (!(dsXML.Tables.Contains("ConsumerAccountGoodBadSummary")))
            {
                DetailAccountGoodBadSummary.Visible = false;
                lblCrAccStatus.Text += " - (No Data Available)";
            }

            if (!(dsXML.Tables.Contains("ConsumerAccountStatus")))
            {
                DetailAccountStatus.Visible = false;
                lblAccStatusH.Text = lblAccStatusH.Text + " - (No Data Available)";
                tblAccStatusC.Visible = false;
                MonthlyPaymentHeader.Visible = false;
                MonthlyPayment.Visible = false;
                ConsumerDefinition.Visible = false;
            }
            else
            {
               // dsXML.Tables["ConsumerAccountStatus"].Columns.Add("FDateAccountOpened", System.Type.GetType("System.DateTime"), "Iif(AccountOpenedDate='','1900-01-01T00:00:00',AccountOpenedDate)");
                dsXML.Tables["ConsumerAccountStatus"].Columns.Add("FCreditLimitAmt", System.Type.GetType("System.Single"), "Iif(CreditLimitAmt='',0,CreditLimitAmt)");
                dsXML.Tables["ConsumerAccountStatus"].Columns.Add("FCurrentBalanceAmt", System.Type.GetType("System.Single"), "Iif(CurrentBalanceAmt='',0,CurrentBalanceAmt)");
                dsXML.Tables["ConsumerAccountStatus"].Columns.Add("FMonthlyInstalmentAmt", System.Type.GetType("System.Single"), "Iif(MonthlyInstalmentAmt='',0,MonthlyInstalmentAmt)");
                dsXML.Tables["ConsumerAccountStatus"].Columns.Add("FArrearsAmt", System.Type.GetType("System.Single"), "Iif(ArrearsAmt='',0,ArrearsAmt)");

                //this.lblAccOpenDateD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerAccountStatus.FDateAccountOpened", "{0:dd/MM/yyyy}") });
                this.lblCreditLimitD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerAccountStatus.FCreditLimitAmt", "R {0}") });
                this.lblCurrBalanceD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerAccountStatus.FCurrentBalanceAmt", "R {0}") });
                this.lblInstallementD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerAccountStatus.FMonthlyInstalmentAmt", "R {0}") });
                this.lblArrearsAmtD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerAccountStatus.FArrearsAmt", "R {0}") });

            }
            if ((!(dsXML.Tables.Contains("Consumer6MonthlyPaymentWithDelinquentHeader"))) || (!(dsXML.Tables.Contains("Consumer6MonthlyPaymentWithDelinquent"))))
            {
                PageinfoMonthlyPaymentH.Format += " - (No Data Available)";
                MonthlyPaymentHeader.Visible = false;
                MonthlyPayment.Visible = false;
                ConsumerDefinition.Visible = false;
            }
            if (!(dsXML.Tables.Contains("ConsumerAdverseInfo")))
            {
                DetailAdverseInfo.Visible = false;
                lblAdverseH.Text += " - (No Data Available)";
                tblAdverseColumns.Visible = false;
            }
            else
            {
                //dsXML.Tables["ConsumerAdverseInfo"].Columns.Add("FActionDate", System.Type.GetType("System.DateTime"), "Iif(ActionDate='','1900-01-01T00:00:00',ActionDate)");
                dsXML.Tables["ConsumerAdverseInfo"].Columns.Add("FCurrentBalanceAmt", System.Type.GetType("System.Single"), "Iif(CurrentBalanceAmt='',0,CurrentBalanceAmt)");

                //this.lblDActionDate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerAdverseInfo.FActionDate", "{0:dd/MM/yyyy}") });
                this.lblDAmount.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerAdverseInfo.FCurrentBalanceAmt", "R {0}") });
     
            }

            if (!(dsXML.Tables.Contains("ConsumerJudgement")))
            {
                DetailJudgements.Visible = false;
                lblJudgmentsH.Text+= " - (No Data Available)";
                tblCJud.Visible = false;
            }
            else
            {
               // dsXML.Tables["ConsumerJudgement"].Columns.Add("FCaseFilingDate", System.Type.GetType("System.DateTime"), "Iif(CaseFilingDate='','1900-01-01T00:00:00',CaseFilingDate)");
                dsXML.Tables["ConsumerJudgement"].Columns.Add("FDisputeAmt", System.Type.GetType("System.Single"), "Iif(DisputeAmt='',0,DisputeAmt)");

                //this.lblJDIssueDate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerJudgement.FCaseFilingDate", "{0:dd/MM/yyyy}") });
                this.lblJDAmount.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerJudgement.FDisputeAmt", "R {0}") });
            }

            if (!(dsXML.Tables.Contains("ConsumerAdminOrder")))
            {
                DetailAdminOrders.Visible = false;
                lblAOH.Text+= " - (No Data Available)";
                tblCAO.Visible = false;
            }
            else
            {
                dsXML.Tables["ConsumerAdminOrder"].Columns.Add("FDisputeAmt", System.Type.GetType("System.Single"), "Iif(DisputeAmt='',0,DisputeAmt)");
                this.lblAODAmount.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerAdminOrder.FDisputeAmt", "R {0}") });

            }

            if (!(dsXML.Tables.Contains("ConsumerSequestration")))
            {
                DetailSequestration.Visible = false;
                lblSEQH.Text+= " - (No Data Available)";
                tblSEQC.Visible = false;
            }
            else
            {
                dsXML.Tables["ConsumerSequestration"].Columns.Add("FDisputeAmt", System.Type.GetType("System.Single"), "Iif(DisputeAmt='',0,DisputeAmt)");

                this.lblSEQDAmount.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerSequestration.FDisputeAmt", "R {0}") });
            }

            if (!(dsXML.Tables.Contains("ConsumerDefaultAlert")))
            {
                DetailDefaultAlert.Visible = false;
                lblDefaultAlertH.Text+= " - (No Data Available)";
                tblDefaultAlertC.Visible = false;
            }
            else
            {
               // dsXML.Tables["ConsumerDefaultAlert"].Columns.Add("FDateLoaded", System.Type.GetType("System.DateTime"), "Iif(DateLoaded='','1900-01-01T00:00:00',DateLoaded)");
                dsXML.Tables["ConsumerDefaultAlert"].Columns.Add("FAmount", System.Type.GetType("System.Single"), "Iif(Amount='',0,Amount)");

               // this.lblDADLoadeddateD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDefaultAlert.FDateLoaded", "{0:dd/MM/yyyy}") });
                this.lblDAAmountD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDefaultAlert.FAmount", "R {0}") });
            }
           

            if (!(dsXML.Tables.Contains("ConsumerDebtReviewStatus")))
            {
                DetailDebtReviewStatus.Visible = false;
                lbldbtReviewH.Text+= " - (No Data Available)";
            }
            //else
            //{
            //    //dsXML.Tables["ConsumerDebtReviewStatus"].Columns.Add("FDebtReviewStatusDate", System.Type.GetType("System.DateTime"), "Iif(DebtReviewStatusDate='','1900-01-01T00:00:00',DebtReviewStatusDate)");
            //    this.lblDbtReviewDateD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtReviewStatus.FDebtReviewStatusDate", "{0:dd/MM/yyyy}") });
            //}
            if (!(dsXML.Tables.Contains("ConsumerEnquiryHistory")))
            {
                DetailEnquiryHistory.Visible = false;
                lblEnquiryH.Text += " - (No Data Available)";
                tblEnquiryC.Visible = false;
            }
            //else
            //{
            //    dsXML.Tables["ConsumerEnquiryHistory"].Columns.Add("FEnquiryDate", System.Type.GetType("System.DateTime"), "Iif(EnquiryDate='','1900-01-01T00:00:00',EnquiryDate)");
            //    this.lblEnqDateD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerEnquiryHistory.FEnquiryDate", "{0:dd/MM/yyyy}") });
            //}


            if (!(dsXML.Tables.Contains("ConsumerPropertyInformation")))
            {
                DetailpropertyInfo.Visible = false;
                lblPropInterestsH.Text+=" - (No Data Available)";
            }
            else
            {
               // dsXML.Tables["ConsumerPropertyInformation"].Columns.Add("FPurchaseDate", System.Type.GetType("System.DateTime"), "Iif(PurchaseDate='','1900-01-01T00:00:00',PurchaseDate)");
                dsXML.Tables["ConsumerPropertyInformation"].Columns.Add("FPurchasePriceAmt", System.Type.GetType("System.Single"), "Iif(PurchasePriceAmt='',0,PurchasePriceAmt)");
                dsXML.Tables["ConsumerPropertyInformation"].Columns.Add("FBondAmt", System.Type.GetType("System.Single"), "Iif(BondAmt='',0,BondAmt)");

                //this.lblDPurchasedate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerPropertyInformation.FPurchaseDate", "{0:dd/MM/yyyy}") });
                this.lblDPurchaseprice.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerPropertyInformation.FPurchasePriceAmt", "R {0}") });
                this.lblDBondAmount.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerPropertyInformation.FBondAmt", "R {0}") });                
            }


            if (!(dsXML.Tables.Contains("ConsumerDirectorShipLink")))
            {
                DetailDirectorshipLink.Visible = false;
                lblDirectorlinksH.Text+= " - (No Data Available)";
            }
           

            if (!(dsXML.Tables.Contains("ConsumerAddressHistory")))
            {
                DetailAddressHistory.Visible = false;
                lblAddHistoryH.Text+= " - (No Data Available)";
                tblAddHC.Visible = false;
            }
           

            if (!(dsXML.Tables.Contains("ConsumerTelephoneHistory")))
            {
                DetailTelephoneHistory.Visible = false;
                lblContactNoH.Text+= " - (No Data Available)";
                tblContactNoC.Visible = false;
            }
           
            if (!(dsXML.Tables.Contains("ConsumerEmploymentHistory")))
            {
                DetailEmploymentHistory.Visible = false;
                lblEmploymentH.Text += " - (No Data Available)";
                tblEmploymentC.Visible = false;
            }
            
            this.DataSource = dsXML;
            dsXML.Dispose();
        }

        private void tblAccStatus_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (!string.IsNullOrEmpty(lblRetailG.Text))
            {
                if (Convert.ToInt16(lblRetailG.Text) > 0)
                {
                    lblRetailG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(248)))), ((int)(((byte)(192)))));
                }
            }
            if (!string.IsNullOrEmpty(lblCreditCradG.Text))
            {
                if (Convert.ToInt16(lblCreditCradG.Text) > 0)
                {
                    lblCreditCradG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(248)))), ((int)(((byte)(192)))));
                }
            }
            if (!string.IsNullOrEmpty(lblFurnAccG.Text))
            {
                if (Convert.ToInt16(lblFurnAccG.Text) > 0)
                {
                    lblFurnAccG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(248)))), ((int)(((byte)(192)))));
                }
            }
            if (!string.IsNullOrEmpty(lblInsuranceG.Text))
            {
                if (Convert.ToInt16(lblInsuranceG.Text) > 0)
                {
                    lblInsuranceG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(248)))), ((int)(((byte)(192)))));
                }
            }
            if (!string.IsNullOrEmpty(lblFinanceG.Text))
            {
                if (Convert.ToInt16(lblFinanceG.Text) > 0)
                {
                    lblFinanceG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(248)))), ((int)(((byte)(192)))));
                }
            }
            if (!string.IsNullOrEmpty(lblFinanceG.Text))
            {
                if (Convert.ToInt16(lblBankAccG.Text) > 0)
                {
                    lblBankAccG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(248)))), ((int)(((byte)(192)))));
                }
            }
            if (!string.IsNullOrEmpty(lblTelecommsG.Text))
            {
                if (Convert.ToInt16(lblTelecommsG.Text) > 0)
                {
                    lblTelecommsG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(248)))), ((int)(((byte)(192)))));
                }
            }
            if (!string.IsNullOrEmpty(lblHomeLoanG.Text))
            {
                if (Convert.ToInt16(lblHomeLoanG.Text) > 0)
                {
                    lblHomeLoanG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(248)))), ((int)(((byte)(192)))));
                }
            }
            if (!string.IsNullOrEmpty(lblVehicleFinG.Text))
            {
                if (Convert.ToInt16(lblVehicleFinG.Text) > 0)
                {
                    lblVehicleFinG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(248)))), ((int)(((byte)(192)))));
                }
            }
            if (!string.IsNullOrEmpty(lblOtherAccG.Text))
            {
                if (Convert.ToInt16(lblOtherAccG.Text) > 0)
                {
                    lblOtherAccG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(248)))), ((int)(((byte)(192)))));
                }
            }
            if (!string.IsNullOrEmpty(lblRetailB.Text))
            {
                if (Convert.ToInt16(lblRetailB.Text) > 0)
                {
                    lblRetailB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
                }
            }
            if (!string.IsNullOrEmpty(lblCreditCardB.Text))
            {
                if (Convert.ToInt16(lblCreditCardB.Text) > 0)
                {
                    lblCreditCardB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
                }
            }
            if (!string.IsNullOrEmpty(lblFurnAccB.Text))
            {
                if (Convert.ToInt16(lblFurnAccB.Text) > 0)
                {
                    lblFurnAccB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
                }
            }
            if (!string.IsNullOrEmpty(lblInsuranceB.Text))
            {
                if (Convert.ToInt16(lblInsuranceB.Text) > 0)
                {
                    lblInsuranceB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
                }
            }
            if (!string.IsNullOrEmpty(lblFinanceB.Text))
            {
                if (Convert.ToInt16(lblFinanceB.Text) > 0)
                {
                    lblFinanceB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
                }
            }
            if (!string.IsNullOrEmpty(lblBankAccB.Text))
            {
                if (Convert.ToInt16(lblBankAccB.Text) > 0)
                {
                    lblBankAccB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
                }
            }
            if (!string.IsNullOrEmpty(lblTelecommsB.Text))
            {
                if (Convert.ToInt16(lblTelecommsB.Text) > 0)
                {
                    lblTelecommsB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
                }
            }
            if (!string.IsNullOrEmpty(lblHomeLoanB.Text))
            {
                if (Convert.ToInt16(lblHomeLoanB.Text) > 0)
                {
                    lblHomeLoanB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
                }
            }
            if (!string.IsNullOrEmpty(lblVehicleFinB.Text))
            {
                if (Convert.ToInt16(lblVehicleFinB.Text) > 0)
                {
                    lblVehicleFinB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
                }
            }
            if (!string.IsNullOrEmpty(lblOtherAccB.Text))
            {
                if (Convert.ToInt16(lblOtherAccB.Text) > 0)
                {
                    lblOtherAccB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
                }
            }
        }

        private void tblDebtSummary_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (!string.IsNullOrEmpty(lblAccGoodvalue.Text))
            {
                if (Convert.ToInt16(lblAccGoodvalue.Text) > 0)
                {
                    lblAccGoodvalue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(248)))), ((int)(((byte)(192)))));
                }
            }
            if (!string.IsNullOrEmpty(lblDelinquentAccValue.Text))
            {
                if (Convert.ToInt16(lblDelinquentAccValue.Text) > 0)
                {
                    lblDelinquentAccValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
                }
            }
        }

        private void tblscrorevalues_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (lblDescription.Text.ToUpper().Contains("HIGH"))
            {
                lblDescription.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(248)))), ((int)(((byte)(192)))));
            }
            else if (lblDescription.Text.ToUpper().Contains("AVERAGE"))
            {
                lblDescription.BackColor = Color.Bisque;
            }
            else if (lblDescription.Text.ToUpper().Contains("LOW"))
            {
                lblDescription.BackColor = Color.LightCoral;
            }
        }

        private void tblMonthlyPaymentD_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (!String.IsNullOrEmpty(lblDelinquentStatusD.Text))
            {
                lblDelinquentStatusD.BackColor = Color.LightCoral;
            }
            else
            {
                lblDelinquentStatusD.BackColor = Color.White;
            }
        }

    }
}
