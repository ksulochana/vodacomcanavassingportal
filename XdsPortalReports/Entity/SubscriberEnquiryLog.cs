﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlTypes;

namespace XDSPortalEnquiry.Entity
{
    [Serializable]
    public class SubscriberEnquiryLog
    {
        public SubscriberEnquiryLog()
        {
        }
        public enum EnquiryResultInd { N, R, E};
        public enum EnquiryStatusInd { P, C };

        
        private int _SubscriberEnquiryID = 0;
        private int _SubscriberEnquiryLogID = 0;
        private string _FirstName= string.Empty;
        private string _Surname = string.Empty;
        private string _KeyType = string.Empty;
        private int _KeyID = 0;
        private string _BusRegistrationNo = string.Empty;
        private string _BusBusinessName = string.Empty;
        private string _CompanyName = string.Empty;
        private string _ContactNo = string.Empty;
        private string _EmailAddress = string.Empty;
        private int _BillingTypeID = 0;
        private double _BillingPrice = 0;
        private string _VoucherCode =string.Empty;
        private bool _Billable = false;
        private string _Reference = string.Empty;
        private string _SubscriberReference = string.Empty;
        private int _SubscriberID = 0;
        private string _SubscriberName = string.Empty;
        private int _SystemUserID = 0;
        private string _SystemUserName=string.Empty;
        private int _ExtraIntInput1 =0;
        private int _ExtraIntInput2=0;
        private string _ExtraVarInput1=string.Empty;
        private string _ExtraVarInput2=string.Empty;
        private string _ExtraVarInput3=string.Empty;
        private string _EntityType = string.Empty;
        private string  _RegNumber = string.Empty;
        private string _Trust = string.Empty;

        private string _CreatedByUser = string.Empty;
        private DateTime _CreatedOnDate = DateTime.Parse("1900/01/01");
        private string _ChangedByUser = string.Empty;
        private DateTime _ChangedOnDate = DateTime.Parse("1900/01/01");
        private DateTime _SubscriberEnquiryDate = DateTime.Parse("1900/01/01");
        private DateTime _DetailsViewedDate = DateTime.Parse("1900/01/01");
        private string _EnquiryStatusInd = string.Empty;
        private string _EnquiryResultInd = string.Empty;
        private string _ReportType = string.Empty;
        private string _TimeFrame = string.Empty;
        private string _BankCodeRequestType = string.Empty;
        private string _BankName = string.Empty;
        private string _BranchName = string.Empty;
        private string _BranchCode = string.Empty;
        private string _AccountNo = string.Empty;
        private string _AccHolder = string.Empty;
        private double _Amount = 0;
        private string _Terms = string.Empty;
        private int _ProductID = 0;
        private string _IDNo = string.Empty;
        private string _CompanyContactNo = string.Empty;
        private string _xmlData = string.Empty;

        private string _Searchinput = string.Empty;
        private string _Searchoutput = string.Empty;

        private string _ErrorDescription = string.Empty;
        private Boolean _DetailsViewedYN = false;
        private Boolean _PayAsYouGo = false;

        private string _FileName = string.Empty;
        private byte[] _ReportFile ;
        private Boolean _IsAHVRequest = false;

            

        public int SubscriberEnquiryID
        {
            get { return _SubscriberEnquiryID;}
            set { this._SubscriberEnquiryID = value; }
        }
        
        public int SubscriberEnquiryLogID
        {
            get { return _SubscriberEnquiryLogID;}
            set { this._SubscriberEnquiryLogID = value; }
        }

        public string FirstName
        {
            get { return _FirstName; }
            set {this._FirstName = value;}
        }
        public string SurName
        {
            get { return _Surname; }
            set { this._Surname = value; }
        }
        public string BusRegistrationNo
        {
            get { return _BusRegistrationNo; }
            set { this._BusRegistrationNo = value; }
        }
        public string BusBusinessName
        {
            get { return _BusBusinessName; }
            set { this._BusBusinessName = value; }
        }
        public string CompanyName
        {
            get { return _CompanyName; }
            set { this._CompanyName = value; }
        }
        public string ContactNo
        {
            get { return _ContactNo; }
            set { this._ContactNo = value; }
        }
        public string EmailAddress
        {
            get { return _EmailAddress; }
            set { this._EmailAddress = value; }
        }
        public string VoucherCode
        {
            get { return _VoucherCode; }
            set { this._VoucherCode = value; }
        }
        public string Reference
        {
            get { return _Reference; }
            set { this._Reference = value; }
        }
        public string SubscriberReference
        {
            get { return _SubscriberReference; }
            set { this._SubscriberReference = value; }
        }
        public int SubscriberID
        {
            get { return _SubscriberID; }
            set { this._SubscriberID = value; }
        }
        public string SubscriberName
        {
            get { return _SubscriberName; }
            set { this._SubscriberName = value; }
        }
        public int SystemUserID
        {
            get { return _SystemUserID; }
            set { this._SystemUserID = value; }
        }
        public string SystemUserName
        {
            get { return _SystemUserName; }
            set { this._SystemUserName = value; }
        }
        public int ExtraintInput1
        {
            get { return _ExtraIntInput1; }
            set { this._ExtraIntInput1 = value; }
        }
        public int ExtraintInput2
        {
            get { return _ExtraIntInput2; }
            set { this._ExtraIntInput2 = value; }
        }
        public string ExtraVarInput1
        {
            get { return _ExtraVarInput1; }
            set { this._ExtraVarInput1 = value; }
        }
        public string ExtraVarInput2
        {
            get { return _ExtraVarInput2; }
            set { this._ExtraVarInput2 = value; }
        }
        public string ExtraVarInput3
        {
            get { return _ExtraVarInput3; }
            set { this._ExtraVarInput3 = value; }
        }

        public string  RegNumber
        {
            get { return _RegNumber; }
            set { this._RegNumber = value; }
        }

        public string Trust
        {
            get { return _Trust; }
            set { this._Trust = value; }
        }

        public string  EntityType
        {
            get { return _EntityType; }
            set { this._EntityType = value; }
        }

        public string CreatedByUser
        {
            get { return _CreatedByUser; }
            set { this._CreatedByUser = value; }
        }
        public DateTime CreatedOnDate
        {
            get { return _CreatedOnDate; }
            set { this._CreatedOnDate = value; }
        }
        public string ChangedByUser
        {
            get { return _ChangedByUser; }
            set { this._ChangedByUser = value; }
        }
        public DateTime ChangedOnDate
        {
            get { return _ChangedOnDate; }
            set { this._ChangedOnDate = value; }
        }
        public string KeyType
        {
            get { return this._KeyType; }
            set { this._KeyType = value; }
        }
        public string EnquiryStatus
        {
            get { return this._EnquiryStatusInd; }
            set { this._EnquiryStatusInd = value; }
        }
        public string EnquiryResult
        {
            get { return this._EnquiryResultInd; }
            set { this._EnquiryResultInd = value; }
        }
        public string ReportType
        {
            get { return this._ReportType; }
            set { this._ReportType = value; }
        }
        public string TimeFrame
        {
            get { return this._TimeFrame; }
            set { this._TimeFrame = value; }
        }
        public string BankCodeRequestType
        {
            get { return this._BankCodeRequestType; }
            set { this._BankCodeRequestType = value; }
        }
        public string BankName
        {
            get { return this._BankName; }
            set { this._BankName = value; }
        }
        public string BranchName
        {
            get { return this._BranchName; }
            set { this._BranchName = value; }
        }
        public string BranchCode
        {
            get { return this._BranchCode; }
            set { this._BranchCode = value; }
        }
        public string AccountNo
        {
            get { return this._AccountNo; }
            set { this._AccountNo = value; }
        }
        public string AccHolder
        {
            get { return this._AccHolder; }
            set { this._AccHolder = value; }
        }
        public double Amount
        {
            get { return this._Amount; }
            set { this._Amount = value; }
        }
        public string Terms
        {
            get { return this._Terms; }
            set { this._Terms = value; }
        }
        public bool Billable
        {
            get {return this._Billable;}
            set {this._Billable = value;}
        }
        public double BillingPrice
        {
            get { return this._BillingPrice; }
            set { this._BillingPrice = value; }
        }
        public int ProductID
        {
            get { return this._ProductID; }
            set { this._ProductID = value; }
        }
        public string IDNo
        {
            get { return this._IDNo;}
            set { this._IDNo = value; }
        }
        public string CompanyContactNo
        {
            get { return this._CompanyContactNo; }
            set { this._CompanyContactNo = value; }
        }

        public int KeyID
        {
            get { return this._KeyID; }
            set { this._KeyID = value; }
        }
        public string XMLData
        {
            get { return this._xmlData; }
            set { this._xmlData = value; }
        }
        public string Searchinput
        {
            get { return this._Searchinput; }
            set { this._Searchinput = value; }
        }
        public string SearchOutput
        {
            get { return this._Searchoutput; }
            set { this._Searchoutput = value; }
        }
        public DateTime SubscriberEnquiryDate
        {
            get { return this._SubscriberEnquiryDate; }
            set { this._SubscriberEnquiryDate = value; }
        }
        public int BillingTypeID
        {
            get { return this._BillingTypeID; }
            set { this._BillingTypeID = value; }
        }
        public string ErrorDescription
        {
            get { return this._ErrorDescription; }
            set { this._ErrorDescription = value; }
        }
        public DateTime DetailsViewedDate
        {
            get { return this._DetailsViewedDate; }
            set { this._DetailsViewedDate = value; }
        }

        public Boolean DetailsViewedYN
        {
            get { return this._DetailsViewedYN; }
            set { this._DetailsViewedYN = value; }
        }

        public Boolean Payasyougo
        {
            get { return this._PayAsYouGo; }
            set { this._PayAsYouGo = value; }
        }
        public string FileName
        {
            get { return this._FileName; }
            set { this._FileName = value; }
        }
        public byte[] ReportFile
        {
            get { return this._ReportFile; }
            set { this._ReportFile = value; }
        }
        public Boolean IsAHVRequest
        {
            get { return this._IsAHVRequest; }
            set { this._IsAHVRequest = value; }
        }
        

    }
}
