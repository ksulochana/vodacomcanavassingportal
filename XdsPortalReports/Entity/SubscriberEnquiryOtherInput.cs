using System;
using System.Collections.Generic;
using System.Text;

namespace XDSPortalEnquiry.Entity
{
    [Serializable]
    public class SubscriberEnquiryOtherInput
    {
        
        public SubscriberEnquiryOtherInput() { }

        public enum EnquiryResultInd { M, S, N };
        public enum EnquiryStatusInd { P, C };
        public enum GenderInd { M, F };
        public enum TelephoneTypeInd {T,C };

        private string _EnquiryResultInd;
        private string _EnquiryStatusInd;
        private string _GenderInd;

        private int _SubscriberEnquiryOtherInputID;
        private int _SubscriberEnquiryID;
        private int _SubscriberID;
        private int _SystemUserID;
        private int _ProductID;
        private DateTime _SubscriberEnquiryDate = DateTime.Parse("1900/01/01");
        private string _SubscriberName;
        private string _SystemUser;
        private string _SubscriberReference;
        private string _CustomerTelephone;
        private string _IDNo;
        private string _PassportNo;
	    private string _Surname;
	    private string _FirstInitial;
	    private string _SecondInitial;
	    private string _MaidenName;
        private string _FirstName;
        private string _SecondName;
        private DateTime _BirthDate = DateTime.Parse("1900/01/01");
        private string _TelephoneCode;
        private string _TelephoneNo;
        private string _ErrorDescription;

        private string _AccountNo;
        private string _SubAccountNo;

        private string _BusRegistrationNo;
        private string _BusBusinessName;
        private string _BusVatNumber;
        private string _chkAffordabilityCalculationInd;
        private decimal _flNetMonthlyIncomeAmt;
        private decimal _flMonthlyExpenseAmt;
        private decimal _flInterestRateStressPerc;
        private string _chkCalculateNewLoanMonthlyPaymentTypeInd;
        private decimal _flNewLoanMonthlyRepaymentAmt;
        private decimal _flNewLoanAmt; 
        private decimal _flNewLoanMonthlyInterestPerc;
        private int _intNewLoanTerm;

        private string _AgeSearchTypeInd;
        private int _Age;
        private int _AgeDeviation;
        private int _AgeBirthYear;
        private string _SearchInput;
        private string _XMLDataSegment;
        private string _ExtraVarInput1;
        private string _ExtraVarInput2;
        private string _ExtraVarInput3;
        private int _ExtraIntInput1;
        private int _ExtraIntInput2;

        private string _CreatedByUser;
        private DateTime _CreatedOnDate = DateTime.Parse("1900/01/01");
        private string _ChangedByUser;
        private DateTime _ChangedOnDate = DateTime.Parse("1900/01/01");

        public int SubscriberEnquiryOtherInputID
        {
            get { return _SubscriberEnquiryOtherInputID; }
            set { _SubscriberEnquiryOtherInputID = value; }
        }

        public string CreatedByUser
        {
            get { return _CreatedByUser; }
            set { _CreatedByUser = value; }
        }

        public DateTime CreatedOnDate
        {
            get { return _CreatedOnDate; }
            set { _CreatedOnDate = value; }
        }

        public string ChangedByUser
        {
            get { return _ChangedByUser; }
            set { _ChangedByUser = value; }
        }

        public DateTime ChangedOnDate
        {
            get { return _ChangedOnDate; }
            set { _ChangedOnDate = value; }
        }

        public string AgeSearchTypeInd
        {
            get { return _AgeSearchTypeInd; }
            set { _AgeSearchTypeInd = value; }
        }

        public string XMLDataSegment
        {
            get { return _XMLDataSegment; }
            set { _XMLDataSegment = value; }
        }

        public string ExtraVarInput1
        {
            get { return _ExtraVarInput1; }
            set { _ExtraVarInput1 = value; }
        }
        public string ExtraVarInput2
        {
            get { return _ExtraVarInput2; }
            set { _ExtraVarInput2 = value; }
        }
        public string ExtraVarInput3
        {
            get { return _ExtraVarInput3; }
            set { _ExtraVarInput3 = value; }
        }

        public int ExtraIntInput1
        {
            get { return _ExtraIntInput1; }
            set { _ExtraIntInput1 = value; }
        }

        public int ExtraIntInput2
        {
            get { return _ExtraIntInput2; }
            set { _ExtraIntInput2 = value; }
        }

        public int Age
        {
            get { return _Age; }
            set { _Age = value; }
        }

        public int AgeDeviation
        {
            get { return _AgeDeviation; }
            set { _AgeDeviation = value; }
        }

        public int AgeBirthYear
        {
            get { return _AgeBirthYear; }
            set { _AgeBirthYear = value; }
        }

        public string BusRegistrationNo
        {
            get { return _BusRegistrationNo; }
            set { _BusRegistrationNo = value; }
        }

        public string ErrorDescription
        {
            get { return _ErrorDescription; }
            set { _ErrorDescription = value; }
        }

        public string BusBusinessName
        {
            get { return _BusBusinessName; }
            set { _BusBusinessName = value; }
        }

        public string BusVatNumber
        {
            get { return _BusVatNumber; }
            set { _BusVatNumber = value; }
        }

        public string SearchInput
        {
            get { return _SearchInput; }
            set { _SearchInput = value; }
        }
        

        public int NewLoanTerm
        {
            get { return _intNewLoanTerm; }
            set { _intNewLoanTerm = value; }
        }

        public string AffordabilityCalculationInd
        {
            get { return _chkAffordabilityCalculationInd; }
            set { _chkAffordabilityCalculationInd = value; }
        }

        public string CalculateNewLoanMonthlyPaymentTypeInd
        {
            get { return _chkCalculateNewLoanMonthlyPaymentTypeInd; }
            set { _chkCalculateNewLoanMonthlyPaymentTypeInd = value; }
        }

        public decimal NewLoanMonthlyRepaymentAmt
        {
            get { return _flNewLoanMonthlyRepaymentAmt; }
            set { _flNewLoanMonthlyRepaymentAmt = value; }
        }

        public decimal NewLoanAmt
        {
            get { return _flNewLoanAmt; }
            set { _flNewLoanAmt = value; }
        }

        public decimal MonthlyExpenseAmt
        {
            get { return _flMonthlyExpenseAmt; }
            set { _flMonthlyExpenseAmt = value; }
        }

        public decimal NewLoanMonthlyInterestPerc
        {
            get { return _flNewLoanMonthlyInterestPerc; }
            set { _flNewLoanMonthlyInterestPerc = value; }
        }

        public decimal InterestRateStressPerc
        {
            get { return _flInterestRateStressPerc; }
            set { _flInterestRateStressPerc = value; }
        }

        public decimal NetMonthlyIncomeAmt
        {
            get { return _flNetMonthlyIncomeAmt; }
            set { _flNetMonthlyIncomeAmt = value; }
        }


        public string EnquiryResult
        {
            get { return _EnquiryResultInd; }
            set { _EnquiryResultInd = value; }
        }

        public string EnquiryStatus
        {
            get { return _EnquiryStatusInd; }
            set { _EnquiryStatusInd = value; }
        }

        public string Gender
        {
            get { return _GenderInd; }
            set { _GenderInd = value; }
        }

        public int SubscriberEnquiryID
        {
            get { return _SubscriberEnquiryID; }
            set { _SubscriberEnquiryID = value; }
        }

        public int SubscriberID
        {
            get { return _SubscriberID; }
            set { _SubscriberID = value; }
        }

        public int SystemUserID
        {
            get { return _SystemUserID; }
            set { _SystemUserID = value; }
        }

        public int ProductID
        {
            get { return _ProductID; }
            set { _ProductID = value; }
        }

        public DateTime SubscriberEnquiryDate
        {
            get { return _SubscriberEnquiryDate; }
            set { _SubscriberEnquiryDate = value; }
        }

        public string SubscriberName
        {
            get { return _SubscriberName; }
            set { _SubscriberName = value; }
        }

        public string SystemUser
        {
            get { return _SystemUser; }
            set { _SystemUser = value; }
        }

        public string SubscriberReference
        {
            get { return _SubscriberReference; }
            set { _SubscriberReference = value; }
        }

        public string CustomerTelephone
        {
            get { return _CustomerTelephone; }
            set { _CustomerTelephone = value; }
        }

        public string IDNo
        {
            get { return _IDNo; }
            set { _IDNo = value; }
        }

        public string PassportNo
        {
            get { return _PassportNo; }
            set { _PassportNo = value; }
        }

        public string Surname
        {
            get { return _Surname; }
            set { _Surname = value; }
        }

        public string FirstInitial
        {
            get { return _FirstInitial; }
            set { _FirstInitial = value; }
        }

        public string SecondInitial
        {
            get { return _SecondInitial; }
            set { _SecondInitial = value; }
        }

        public string MaidenName
        {
            get { return _MaidenName; }
            set { _MaidenName = value; }
        }

        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }

        public string SecondName
        {
            get { return _SecondName; }
            set { _SecondName = value; }
        }

        public DateTime BirthDate
        {
            get { return _BirthDate; }
            set { _BirthDate = value; }
        }

        public string TelephoneCode
        {
            get { return _TelephoneCode; }
            set { _TelephoneCode = value; }
        }

        public string TelephoneNo
        {
            get { return _TelephoneNo; }
            set { _TelephoneNo = value; }
        }


        public string AccountNo
        {
            get { return _AccountNo; }
            set { _AccountNo = value; }
        }

        public string SubAccountNo
        {
            get { return _SubAccountNo; }
            set { _SubAccountNo = value; }
        }


    }
}
