﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XDSPortalEnquiry.Entity
{
    [Serializable]
    public class SubscriberPayAsYouGo
    {
        
        public SubscriberPayAsYouGo()
        {
        }

        private int _PayAsYouGo,_SubscriberID;
        private double _PayAsyouGoEnquiryLimit;

        public int SubscriberID
        {
            get
            {
                return this._SubscriberID;
            }
            set
            {
                this._SubscriberID = value;
            }
        }

        public int PayAsYouGo
        {
            get
            {
                return this._PayAsYouGo;
            }
            set
            {
                this._PayAsYouGo = value;
            }
        }

        public double PayAsyouGoEnquiryLimit
        {
            get
            {
                return this._PayAsyouGoEnquiryLimit;
            }
            set
            {
                this._PayAsyouGoEnquiryLimit = value;
            }
        }
    }
}
