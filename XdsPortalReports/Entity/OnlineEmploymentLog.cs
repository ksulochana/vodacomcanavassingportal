﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XDSPortalEnquiry.Entity
{
      public class OnlineEmploymentLog
    {
        public enum EnquiryResultInd { M, S, N, R, B };
        public enum EnquiryStatusInd { P, C };
        private string _employerName = string.Empty;
        private string _employerTelephoneNumber = string.Empty;
        private int _enquiryID = 0;
        private int _enquiryResultID = 0;
        private int _keyID = 0;
        private string _keyType = string.Empty;
        private int _subscriberID = 0;
        private string _subscriberName = string.Empty;
        private string _systemUsername = string.Empty;
        private int _systemUserID = 0;
        private string _subscriberReference = string.Empty;
        private string _enquiryStatusind = string.Empty;
        private string _enquiryResultInd = string.Empty;
        private string _username = string.Empty;
        private int _employmentLogID = 0;
        private string _errordescription = string.Empty;
        private string _EmploymentDate =string.Empty;



        public string employerName { get { return this._employerName; } set { this._employerName = value; } }
        public string employerTelephoneNumber { get { return this._employerTelephoneNumber; } set { this._employerTelephoneNumber = value; } }
        public int enquiryID { get { return this._enquiryID; } set { this._enquiryID = value; } }
        public int enquiryResultID { get { return this._enquiryResultID; } set { this._enquiryResultID = value; } }
        public int keyID { get { return this._keyID; } set { this._keyID = value; } }
        public string keyType { get { return this._keyType; } set { this._keyType = value; } }
        public int subscriberID { get { return this._subscriberID; } set { this._subscriberID = value; } }
        public string subscriberName { get { return this._subscriberName; } set { this._subscriberName = value; } }
        public string systemUsername { get { return this._systemUsername; } set { this._systemUsername = value; } }
        public int systemUserID { get { return this._systemUserID; } set { this._systemUserID = value; } }
        public string subscriberReference { get { return this._subscriberReference; } set { this._subscriberReference = value; } }
        public string enquiryStatusind { get { return this._enquiryStatusind; } set { this._enquiryStatusind = value; } }
        public string enquiryResultInd { get { return this._enquiryResultInd; } set { this._enquiryResultInd = value; } }
        public string username { get { return this._username; } set { this._username = value; } }
        public int employmentLogID { get { return this._employmentLogID; } set { this._employmentLogID = value; } }
        public string errordescription { get { return this._errordescription; } set { this._errordescription = value; } }
        public string employmentDate { get { return this._EmploymentDate; } set { this._EmploymentDate = value; } }

    }
}
