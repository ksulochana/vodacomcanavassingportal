﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XDSPortalEnquiry.Entity
{

    [Serializable]
    public class Subscriber
    {
        public Subscriber()
        {
        }
        private int subscriberIDField;

        private string SubscriberBusinessTypeField;

        private string subscriberNameField;

        private string authenticationSubscriberNameField;

        private string subscriberTypeIndField;

        private string statusIndField;

        private string companyRegistrationNoField;

        private string companyVATNoField;

        private string companyDetailsField;

        private string companyTradingName1Field;

        private string companyTradingName2Field;

        private string companyTelephoneCodeField;

        private string companyTelephoneNoField;

        private string companyFaxCodeField;

        private string companyFaxNoField;

        private string companyWebsiteUrlField;

        private string companyPhysicalAddress1Field;

        private string companyPhysicalAddress2Field;

        private string companyPhysicalAddress3Field;

        private string companyPhysicalPostalCodeField;

        private string companyPostalAddress1Field;

        private string companyPostalAddress2Field;

        private string companyPostalAddress3Field;

        private string companyPostalPostalCodeField;

        private int levelNoField;

        private string level1DescField;

        private string level2DescField;

        private string level3DescField;

        private string level4DescField;

        private string level5DescField;

        private int subscriberRegistrationIDField;

        private bool subscriberRegistrationIDFieldSpecified;

        private int level1SubscriberIDField;

        private bool level1SubscriberIDFieldSpecified;

        private int level2SubscriberIDField;

        private bool level2SubscriberIDFieldSpecified;

        private int level3SubscriberIDField;

        private bool level3SubscriberIDFieldSpecified;

        private int level4SubscriberIDField;

        private bool level4SubscriberIDFieldSpecified;

        private int level5SubscriberIDField;

        private bool level5SubscriberIDFieldSpecified;

        private string subscriberBillingGroupCodeField;

        private string subscriberBusinessTypeCodeField;

        private string subscriberAssociationCodeField;

        private string subscriberGroupCodeField;

        private string createdByUserField;

        private System.DateTime createdOnDateField;

        private bool createdOnDateFieldSpecified;

        private string changedByUserField;

        private System.DateTime changedOnDateField;

        private bool changedOnDateFieldSpecified;

        private int oLDSUBSIDField;

        private bool oLDSUBSIDFieldSpecified;
        private int _PayAsYouGo, _SubscriberID;
        private double _PayAsyouGoEnquiryLimit;

        private string associationTypeCodeField = "";

        private bool _SAFPSInd = false;


        /// <remarks/>
        public int SubscriberID
        {
            get
            {
                return this.subscriberIDField;
            }
            set
            {
                this.subscriberIDField = value;
            }
        }

        /// <remarks/>
        public string SubscriberBusinessType
        {
            get
            {
                return this.SubscriberBusinessTypeField;
            }
            set
            {
                this.SubscriberBusinessTypeField = value;
            }
        }

        /// <remarks/>SubscriberBusinessTypeField
        public string SubscriberName
        {
            get
            {
                return this.subscriberNameField;
            }
            set
            {
                this.subscriberNameField = value;
            }
        }

        /// <remarks/>
        public string AuthenticationSubscriberName
        {
            get
            {
                return this.authenticationSubscriberNameField;
            }
            set
            {
                this.authenticationSubscriberNameField = value;
            }
        }

        /// <remarks/>
        public string SubscriberTypeInd
        {
            get
            {
                return this.subscriberTypeIndField;
            }
            set
            {
                this.subscriberTypeIndField = value;
            }
        }

        /// <remarks/>
        public string StatusInd
        {
            get
            {
                return this.statusIndField;
            }
            set
            {
                this.statusIndField = value;
            }
        }

        /// <remarks/>
        public string CompanyRegistrationNo
        {
            get
            {
                return this.companyRegistrationNoField;
            }
            set
            {
                this.companyRegistrationNoField = value;
            }
        }

        /// <remarks/>
        public string CompanyVATNo
        {
            get
            {
                return this.companyVATNoField;
            }
            set
            {
                this.companyVATNoField = value;
            }
        }

        /// <remarks/>
        public string CompanyDetails
        {
            get
            {
                return this.companyDetailsField;
            }
            set
            {
                this.companyDetailsField = value;
            }
        }

        /// <remarks/>
        public string CompanyTradingName1
        {
            get
            {
                return this.companyTradingName1Field;
            }
            set
            {
                this.companyTradingName1Field = value;
            }
        }

        /// <remarks/>
        public string CompanyTradingName2
        {
            get
            {
                return this.companyTradingName2Field;
            }
            set
            {
                this.companyTradingName2Field = value;
            }
        }

        /// <remarks/>
        public string CompanyTelephoneCode
        {
            get
            {
                return this.companyTelephoneCodeField;
            }
            set
            {
                this.companyTelephoneCodeField = value;
            }
        }

        /// <remarks/>
        public string CompanyTelephoneNo
        {
            get
            {
                return this.companyTelephoneNoField;
            }
            set
            {
                this.companyTelephoneNoField = value;
            }
        }

        /// <remarks/>
        public string CompanyFaxCode
        {
            get
            {
                return this.companyFaxCodeField;
            }
            set
            {
                this.companyFaxCodeField = value;
            }
        }

        /// <remarks/>
        public string CompanyFaxNo
        {
            get
            {
                return this.companyFaxNoField;
            }
            set
            {
                this.companyFaxNoField = value;
            }
        }

        /// <remarks/>
        public string CompanyWebsiteUrl
        {
            get
            {
                return this.companyWebsiteUrlField;
            }
            set
            {
                this.companyWebsiteUrlField = value;
            }
        }

        /// <remarks/>
        public string CompanyPhysicalAddress1
        {
            get
            {
                return this.companyPhysicalAddress1Field;
            }
            set
            {
                this.companyPhysicalAddress1Field = value;
            }
        }

        /// <remarks/>
        public string CompanyPhysicalAddress2
        {
            get
            {
                return this.companyPhysicalAddress2Field;
            }
            set
            {
                this.companyPhysicalAddress2Field = value;
            }
        }

        /// <remarks/>
        public string CompanyPhysicalAddress3
        {
            get
            {
                return this.companyPhysicalAddress3Field;
            }
            set
            {
                this.companyPhysicalAddress3Field = value;
            }
        }

        /// <remarks/>
        public string CompanyPhysicalPostalCode
        {
            get
            {
                return this.companyPhysicalPostalCodeField;
            }
            set
            {
                this.companyPhysicalPostalCodeField = value;
            }
        }

        /// <remarks/>
        public string CompanyPostalAddress1
        {
            get
            {
                return this.companyPostalAddress1Field;
            }
            set
            {
                this.companyPostalAddress1Field = value;
            }
        }

        /// <remarks/>
        public string CompanyPostalAddress2
        {
            get
            {
                return this.companyPostalAddress2Field;
            }
            set
            {
                this.companyPostalAddress2Field = value;
            }
        }

        /// <remarks/>
        public string CompanyPostalAddress3
        {
            get
            {
                return this.companyPostalAddress3Field;
            }
            set
            {
                this.companyPostalAddress3Field = value;
            }
        }

        /// <remarks/>
        public string CompanyPostalPostalCode
        {
            get
            {
                return this.companyPostalPostalCodeField;
            }
            set
            {
                this.companyPostalPostalCodeField = value;
            }
        }

        /// <remarks/>
        public int LevelNo
        {
            get
            {
                return this.levelNoField;
            }
            set
            {
                this.levelNoField = value;
            }
        }

        /// <remarks/>
        public string Level1Desc
        {
            get
            {
                return this.level1DescField;
            }
            set
            {
                this.level1DescField = value;
            }
        }

        /// <remarks/>
        public string Level2Desc
        {
            get
            {
                return this.level2DescField;
            }
            set
            {
                this.level2DescField = value;
            }
        }

        /// <remarks/>
        public string Level3Desc
        {
            get
            {
                return this.level3DescField;
            }
            set
            {
                this.level3DescField = value;
            }
        }

        /// <remarks/>
        public string Level4Desc
        {
            get
            {
                return this.level4DescField;
            }
            set
            {
                this.level4DescField = value;
            }
        }

        /// <remarks/>
        public string Level5Desc
        {
            get
            {
                return this.level5DescField;
            }
            set
            {
                this.level5DescField = value;
            }
        }

        /// <remarks/>
        public int SubscriberRegistrationID
        {
            get
            {
                return this.subscriberRegistrationIDField;
            }
            set
            {
                this.subscriberRegistrationIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SubscriberRegistrationIDSpecified
        {
            get
            {
                return this.subscriberRegistrationIDFieldSpecified;
            }
            set
            {
                this.subscriberRegistrationIDFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int Level1SubscriberID
        {
            get
            {
                return this.level1SubscriberIDField;
            }
            set
            {
                this.level1SubscriberIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Level1SubscriberIDSpecified
        {
            get
            {
                return this.level1SubscriberIDFieldSpecified;
            }
            set
            {
                this.level1SubscriberIDFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int Level2SubscriberID
        {
            get
            {
                return this.level2SubscriberIDField;
            }
            set
            {
                this.level2SubscriberIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Level2SubscriberIDSpecified
        {
            get
            {
                return this.level2SubscriberIDFieldSpecified;
            }
            set
            {
                this.level2SubscriberIDFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int Level3SubscriberID
        {
            get
            {
                return this.level3SubscriberIDField;
            }
            set
            {
                this.level3SubscriberIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Level3SubscriberIDSpecified
        {
            get
            {
                return this.level3SubscriberIDFieldSpecified;
            }
            set
            {
                this.level3SubscriberIDFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int Level4SubscriberID
        {
            get
            {
                return this.level4SubscriberIDField;
            }
            set
            {
                this.level4SubscriberIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Level4SubscriberIDSpecified
        {
            get
            {
                return this.level4SubscriberIDFieldSpecified;
            }
            set
            {
                this.level4SubscriberIDFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int Level5SubscriberID
        {
            get
            {
                return this.level5SubscriberIDField;
            }
            set
            {
                this.level5SubscriberIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Level5SubscriberIDSpecified
        {
            get
            {
                return this.level5SubscriberIDFieldSpecified;
            }
            set
            {
                this.level5SubscriberIDFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string SubscriberBillingGroupCode
        {
            get
            {
                return this.subscriberBillingGroupCodeField;
            }
            set
            {
                this.subscriberBillingGroupCodeField = value;
            }
        }

        /// <remarks/>
        public string SubscriberBusinessTypeCode
        {
            get
            {
                return this.subscriberBusinessTypeCodeField;
            }
            set
            {
                this.subscriberBusinessTypeCodeField = value;
            }
        }

        /// <remarks/>
        public string SubscriberAssociationCode
        {
            get
            {
                return this.subscriberAssociationCodeField;
            }
            set
            {
                this.subscriberAssociationCodeField = value;
            }
        }

        /// <remarks/>
        public string SubscriberGroupCode
        {
            get
            {
                return this.subscriberGroupCodeField;
            }
            set
            {
                this.subscriberGroupCodeField = value;
            }
        }

        /// <remarks/>
        public string CreatedByUser
        {
            get
            {
                return this.createdByUserField;
            }
            set
            {
                this.createdByUserField = value;
            }
        }

        /// <remarks/>
        public System.DateTime CreatedOnDate
        {
            get
            {
                return this.createdOnDateField;
            }
            set
            {
                this.createdOnDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CreatedOnDateSpecified
        {
            get
            {
                return this.createdOnDateFieldSpecified;
            }
            set
            {
                this.createdOnDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string ChangedByUser
        {
            get
            {
                return this.changedByUserField;
            }
            set
            {
                this.changedByUserField = value;
            }
        }

        /// <remarks/>
        public System.DateTime ChangedOnDate
        {
            get
            {
                return this.changedOnDateField;
            }
            set
            {
                this.changedOnDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ChangedOnDateSpecified
        {
            get
            {
                return this.changedOnDateFieldSpecified;
            }
            set
            {
                this.changedOnDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int OLDSUBSID
        {
            get
            {
                return this.oLDSUBSIDField;
            }
            set
            {
                this.oLDSUBSIDField = value;
            }
        }

        public int PayAsYouGo
        {
            get
            {
                return this._PayAsYouGo;
            }
            set
            {
                this._PayAsYouGo = value;
            }
        }

        public double PayAsyouGoEnquiryLimit
        {
            get
            {
                return this._PayAsyouGoEnquiryLimit;
            }
            set
            {
                this._PayAsyouGoEnquiryLimit = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool OLDSUBSIDSpecified
        {
            get
            {
                return this.oLDSUBSIDFieldSpecified;
            }
            set
            {
                this.oLDSUBSIDFieldSpecified = value;
            }
        }
        public string AssociationTypeCode
        {
            get
            {
                return this.associationTypeCodeField;
            }
            set
            {
                this.associationTypeCodeField = value;
            }
        }

        public bool SAFPSIndicator
        {
            get { return this._SAFPSInd; }
            set { this._SAFPSInd = value; }
        }
    }
}
