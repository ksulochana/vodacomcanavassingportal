﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XDSPortalEnquiry.Entity
{
   
  public  class OnlineDemographicLog
    {
        public enum EnquiryResultInd { M, S, N, R, B };
        public enum EnquiryStatusInd { P, C };
        private string _homephonenumber = string.Empty;
        private string _workphonenumber = string.Empty;
        private string _cellphonenumber = string.Empty;
        private string _physicalAddress1 = string.Empty;
        private string _physicalAddress2 = string.Empty;
        private string _physicalAddress3 = string.Empty;
        private string _physicalAddress4 = string.Empty;
        private string _physicalPostCode = string.Empty;
        private string _postalAddress1 = string.Empty;
        private string _postalAddress2 = string.Empty;
        private string _postalAddress3 = string.Empty;
        private string _postalAddress4 = string.Empty;
        private string _postalPostCode = string.Empty;
        private string _emailAddress = string.Empty;
        private int _enquiryID = 0;
        private int _enquiryResultID = 0;
        private int _keyID = 0;
        private string _keyType = string.Empty;
        private int _subscriberID = 0;
        private string _subscriberName = string.Empty;
        private string _systemUsername = string.Empty;
        private int _systemUserID = 0;
        private string _subscriberReference = string.Empty;
        private string _enquiryStatusind = string.Empty;
        private string _enquiryResultInd = string.Empty;
        private string _username = string.Empty;
        private int _demographicLogID = 0;
        private string _errordescription = string.Empty;



        public string homephonenumber { get { return this._homephonenumber; } set { this._homephonenumber = value; } }
        public string workphonenumber { get { return this._workphonenumber; } set { this._workphonenumber = value; } }
        public string cellphonenumber { get { return this._cellphonenumber; } set { this._cellphonenumber = value; } }
        public string physicalAddress1 { get { return this._physicalAddress1; } set { this._physicalAddress1 = value; } }
        public string physicalAddress2 { get { return this._physicalAddress2; } set { this._physicalAddress2 = value; } }
        public string physicalAddress3 { get { return this._physicalAddress3; } set { this._physicalAddress3 = value; } }
        public string physicalAddress4 { get { return this._physicalAddress4; } set { this._physicalAddress4 = value; } }
        public string physicalPostCode { get { return this._physicalPostCode; } set { this._physicalPostCode = value; } }
        public string postalAddress1 { get { return this._postalAddress1; } set { this._postalAddress1 = value; } }
        public string postalAddress2 { get { return this._postalAddress2; } set { this._postalAddress2 = value; } }
        public string postalAddress3 { get { return this._postalAddress3; } set { this._postalAddress3 = value; } }
        public string postalAddress4 { get { return this._postalAddress4; } set { this._postalAddress4 = value; } }
        public string postalPostCode { get { return this._postalPostCode; } set { this._postalPostCode = value; } }
        public string emailAddress { get { return this._emailAddress; } set { this._emailAddress = value; } }
        public int enquiryID { get { return this._enquiryID; } set { this._enquiryID = value; } }
        public int enquiryResultID { get { return this._enquiryResultID; } set { this._enquiryResultID = value; } }
        public int keyID { get { return this._keyID; } set { this._keyID = value; } }
        public string keyType { get { return this._keyType; } set { this._keyType = value; } }
        public int subscriberID { get { return this._subscriberID; } set { this._subscriberID = value; } }
        public string subscriberName { get { return this._subscriberName; } set { this._subscriberName = value; } }
        public string systemUsername { get { return this._systemUsername; } set { this._systemUsername = value; } }
        public int systemUserID { get { return this._systemUserID; } set { this._systemUserID = value; } }
        public string subscriberReference { get { return this._subscriberReference; } set { this._subscriberReference = value; } }
        public string enquiryStatusind { get { return this._enquiryStatusind; } set { this._enquiryStatusind = value; } }
        public string enquiryResultInd { get { return this._enquiryResultInd; } set { this._enquiryResultInd = value; } }
        public string username { get { return this._username; } set { this._username = value; } }
        public int demographicLogID { get { return this._demographicLogID; } set { this._demographicLogID = value; } }
        public string errordescription { get { return this._errordescription; } set { this._errordescription = value; } }

    }
}
