﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XDSPortalEnquiry.Entity
{
    [Serializable]
   public class SubscriberProductReports
    {
       
        private int _SubscriberProductReportID;
        private int _SubscriberID;
        private int _ProductID;
        private int _ReportID =0;
        private int _BillingTypeID;
        private double _UnitPrice;

        public SubscriberProductReports()
        {
        }

        public int SubscriberProductReportID
        {
            get
            {
                return this._SubscriberProductReportID;
            }
            set
            {
                this._SubscriberProductReportID = value;
            }
        }

        public int SubscriberID
        {
            get
            {
                return this._SubscriberID;
            }
            set
            {
                this._SubscriberID = value;
            }
        }

        public int ProductID
        {
            get
            {
                return this._ProductID;
            }
            set
            {
                this._ProductID = value;
            }
        }


        public int ReportID
        {
            get
            {
                return this._ReportID;
            }
            set
            {
                this._ReportID = value;
            }
        }

        public int BillingTypeID
        {
            get
            {
                return this._BillingTypeID;
            }
            set
            {
                this._BillingTypeID = value;
            }
        }

        public double UnitPrice
        {
            get
            {
                return this._UnitPrice;
            }
            set
            {
                this._UnitPrice = value;
            }
        }

        

    }
}
