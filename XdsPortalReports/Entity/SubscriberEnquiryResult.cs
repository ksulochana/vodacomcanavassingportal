using System;
using System.Collections.Generic;
using System.Text;

namespace XDSPortalEnquiry.Entity
{
    [Serializable]
    public class SubscriberEnquiryResult
    {
        
        public SubscriberEnquiryResult() { }

        private int _SubscriberEnquiryID;
        private int _SubscriberEnquiryResultID;
        private int _ConsumerID;
        private string _EnquiryResultInd;
	    private bool _DetailsViewedYN;
        private DateTime _DetailsViewedDate = DateTime.Parse("1900/01/01");
        private string _IDNo;
        private string _PassportNo;
	    private string _FirstName;
	    private string _Surname;
        private DateTime _BirthDate = DateTime.Parse("1900/01/01");
	    private string _GenderInd;
	    private string _FirstInitial;
	    private string _SecondInitial;
        private int _XDSEnquiryID;
        private string _XMLData;
        private string _KeyType;
        private int _KeyID;
        private string _BusRegistrationNo;
        private string _BusBusinessName;
        private string _SearchOutput;
        private int _BillingTypeID;
        private double _BillingPrice;
        private string _VoucherCode;
        private string _XMLBonus;
        private bool _BonusIncluded;
        private bool _Billable;
        private string _Reference;
        private string _ExtraVarOutput1;
        private string _ExtraVarOutput2;
        private string _ExtraVarOutput3;
        private int _ExtraIntOutput1;
        private int _ExtraIntOutput2;

        private string _CreatedByUser;
        private DateTime _CreatedOnDate = DateTime.Parse("1900/01/01");
        private string _ChangedByUser;
        private DateTime _ChangedOnDate = DateTime.Parse("1900/01/01");

        private string _SecondName;
        private string _Ha_IDNo;
        private string _Ha_FirstName;
        private string _Ha_SecondName;
        private string _Ha_Surname;
        private string _Ha_DeceasedStatus;
        private string _Ha_DeceasedDate;
        private string _Ha_CauseOfDeath;

        private int _ProductID=0;

        private bool _NLRDataIncluded;

        public string SecondName
        {
            get { return _SecondName; }
            set { _SecondName = value; }
        }

        public string Ha_CauseOfDeath
        {
            get { return _Ha_CauseOfDeath; }
            set { _Ha_CauseOfDeath = value; }
        }

        public string Ha_DeceasedDate
        {
            get { return _Ha_DeceasedDate; }
            set { _Ha_DeceasedDate = value; }
        }

        public string Ha_DeceasedStatus
        {
            get { return _Ha_DeceasedStatus; }
            set { _Ha_DeceasedStatus = value; }
        }

        public string Ha_Surname
        {
            get { return _Ha_Surname; }
            set { _Ha_Surname = value; }
        }

        public string Ha_SecondName
        {
            get { return _Ha_SecondName; }
            set { _Ha_SecondName = value; }
        }

        public string Ha_FirstName
        {
            get { return _Ha_FirstName; }
            set { _Ha_FirstName = value; }
        }

        public string Ha_IDNo
        {
            get { return _Ha_IDNo; }
            set { _Ha_IDNo = value; }
        }
        
        public bool Billable
        {
            get { return _Billable; }
            set { _Billable = value; }
        }

        public string CreatedByUser
        {
            get { return _CreatedByUser; }
            set { _CreatedByUser = value; }
        }

        public DateTime CreatedOnDate
        {
            get { return _CreatedOnDate; }
            set { _CreatedOnDate = value; }
        }

        public string ChangedByUser
        {
            get { return _ChangedByUser; }
            set { _ChangedByUser = value; }
        }

        public DateTime ChangedOnDate
        {
            get { return _ChangedOnDate; }
            set { _ChangedOnDate = value; }
        }

        public string Reference
        {
            get { return _Reference; }
            set { _Reference = value; }
        }

        public int ExtraIntOutput1
        {
            get { return _ExtraIntOutput1; }
            set { _ExtraIntOutput1 = value; }
        }

        public int ExtraIntOutput2
        {
            get { return _ExtraIntOutput2; }
            set { _ExtraIntOutput2 = value; }
        }

        public string ExtraVarOutput1
        {
            get { return _ExtraVarOutput1; }
            set { _ExtraVarOutput1 = value; }
        }

        public string ExtraVarOutput2
        {
            get { return _ExtraVarOutput2; }
            set { _ExtraVarOutput2 = value; }
        }
        public string ExtraVarOutput3
        {
            get { return _ExtraVarOutput3; }
            set { _ExtraVarOutput3 = value; }
        }


        public bool BonusIncluded
        {
            get { return _BonusIncluded; }
            set { _BonusIncluded = value; }
        }
        public string VoucherCode
        {
            get { return _VoucherCode; }
            set { _VoucherCode = value; }
        }

        public string XMLBonus
        {
            get { return _XMLBonus; }
            set { _XMLBonus = value; }
        }


        public double BillingPrice
        {
            get { return _BillingPrice; }
            set { _BillingPrice = value; }
        }

        public int BillingTypeID
        {
            get { return _BillingTypeID; }
            set { _BillingTypeID = value; }
        }

        public string BusRegistrationNo
        {
            get { return _BusRegistrationNo; }
            set { _BusRegistrationNo = value; }
        }

        public string SearchOutput
        {
            get { return _SearchOutput; }
            set { _SearchOutput = value; }
        }

        public string KeyType
        {
            get { return _KeyType; }
            set { _KeyType = value; }
        }

        public string BusBusinessName
        {
            get { return _BusBusinessName; }
            set { _BusBusinessName = value; }
        }

        public int KeyID
        {
            get { return _KeyID; }
            set { _KeyID = value; }
        }

        public int SubscriberEnquiryID
        {
            get { return _SubscriberEnquiryID; }
            set { _SubscriberEnquiryID = value; }
        }

        public int XDSEnquiryID
        {
            get { return _XDSEnquiryID; }
            set { _XDSEnquiryID = value; }
        }


        public string EnquiryResult
        {
            get { return _EnquiryResultInd; }
            set { _EnquiryResultInd = value; }
        }

        public string XMLData
        {
            get { return _XMLData; }
            set { _XMLData = value; }
        }

        public int SubscriberEnquiryResultID
        {
            get { return _SubscriberEnquiryResultID; }
            set { _SubscriberEnquiryResultID = value; }
        }

        public int ConsumerID
        {
            get { return _ConsumerID; }
            set { _ConsumerID = value; }
        }

        public bool DetailsViewedYN
        {
            get { return _DetailsViewedYN; }
            set { _DetailsViewedYN = value; }
        }

        public DateTime DetailsViewedDate
        {
            get { return _DetailsViewedDate; }
            set { _DetailsViewedDate = value; }
        }

        public string IDNo
        {
            get { return _IDNo; }
            set { _IDNo = value; }
        }

        public string PassportNo
        {
            get { return _PassportNo; }
            set { _PassportNo = value; }
        }

        public string Surname
        {
            get { return _Surname; }
            set { _Surname = value; }
        }

         public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }


        public DateTime BirthDate
        {
            get { return _BirthDate; }
            set { _BirthDate = value; }
        }

        public string FirstInitial
        {
            get { return _FirstInitial; }
            set { _FirstInitial = value; }
        }

        public string SecondInitial
        {
            get { return _SecondInitial; }
            set { _SecondInitial = value; }
        }

        public string Gender
        {
            get { return _GenderInd; }
            set { _GenderInd = value; }
        }
        public int ProductID
        {
            get { return _ProductID;}
            set {this._ProductID = value;}
        }

        public bool NLRDataIncluded
        {
            get { return _NLRDataIncluded; }
            set { _NLRDataIncluded = value; }
        }
    }
}
