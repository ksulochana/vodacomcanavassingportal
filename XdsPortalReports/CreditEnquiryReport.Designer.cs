namespace XdsPortalReports
{
    partial class ConsumerTraceReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblHeaderText = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblPersonalDetail = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.PersonalDetailsSummary = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblReferenceNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblRefnoValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblExtRefNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblExternalRefNoValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblIDNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblIDnoValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPPNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPassportnoValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSurname = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSurnameValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblResAdd = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblResAddValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblFirstname = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblFirstnameValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbl = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSecondName = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSecondnameValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHomeTelNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHomeTelNoValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblTitle = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbltitlevalue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPostalAdd = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPostalAddvalue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblGender = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGenderValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbl2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbl2value = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDOB = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDOBValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblWorkTelNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblWorkTelNoValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblCurrentEmployer = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCurrentEmployerValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMobileNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMobileNoValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblEmailAddress = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblEmailAddressValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaritalStatus = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaritalStatusValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport1 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblFraudIndSummary = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblVerified = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblVerifiedVal = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDeceased = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDeceasedValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblFoundDB = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblFoundDBValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblEmployerFraudDB = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblEmployerFraudDBValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblProrectiveReg = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblProrectiveRegValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader2 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblFraudSummary = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport2 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail3 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblscrorevalues = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblUniqueIdentifierValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDateValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblScroreValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblExceptionValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader3 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblscoreColumns = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblUniqueIdentifier = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblScrore = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblException = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblScoreHeader = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport3 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail4 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblDebtSummary = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblActiveAcc = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblActiveAccValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblAccGood = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAccGoodvalue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDelinquentAcc = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDelinquentAccValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblMonthlyinst = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMonthlyinstValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblOutstandingDebt = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblOutstandingDebtValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblArrearamt = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblArrearamtvalue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblJdgDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJdgDateValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblJudAmt = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJudAmtValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDelinquentrating = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDelinquentratingValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDbtReview = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDbtReviewValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblAO = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAOValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblAdverseAmt = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAdverseAmtValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader4 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblDebtHeader = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport4 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail5 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblAccStatus = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblAccdesc = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGood = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblBad = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblRetail = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblRetailG = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblRetailB = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblCc = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCcG = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCcB = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblFurnAcc = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblFurnAccG = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblFurnAccB = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblInsurance = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblInsuranceG = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblInsuranceB = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblFinance = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblFinanceG = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblFinanceB = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblBankAcc = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblBankAccG = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblBankAccB = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblTelecomms = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTelecommsG = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTelecommsB = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblHomeLoan = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHomeLoanG = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHomeLoanB = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblVehicleFin = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblVehicleFinG = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblVehicleFinB = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblOtherAcc = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblOtherAccG = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblOtherAccB = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader5 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblCrAccStatus = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport5 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail6 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblNameHistoryValues = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow52 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblNameValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNHSurNameValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNHIDNoValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNHdatevalue = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader6 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblNameHistColumns = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow51 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblName = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNHSurName = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNHIDNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNHdate = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblNameHistoryHeader = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow50 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblpersonalConfirmationHeader = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow49 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblDetailedInfoHeader = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow48 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport6 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail7 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow55 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader7 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblConfAddHistoryC = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow54 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblCAddType = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCAddLine1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCAddLine2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCAddLine3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCAddLine4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCdate = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblAddHistoryHeader = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow53 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport7 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail8 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow58 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader8 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow57 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow56 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport8 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail9 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow61 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader9 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow60 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow59 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport9 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail10 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable30 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow65 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell203 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell200 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell191 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell204 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell201 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell192 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell205 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell202 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell193 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader10 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable31 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow64 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell206 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell207 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell208 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell209 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell210 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell211 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell212 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell213 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell214 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow63 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow62 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport10 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail11 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable33 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow68 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell217 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell195 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell218 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell196 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell219 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell220 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell197 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader11 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable32 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow67 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell198 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell199 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell190 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell215 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell216 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell194 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable29 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow66 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell188 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport11 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail12 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable36 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow71 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell225 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell227 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell224 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell226 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader12 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable35 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow70 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell228 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell222 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell229 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell223 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable34 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow69 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell221 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport12 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail13 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable39 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow74 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell234 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell235 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell236 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader13 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable38 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow73 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell231 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell232 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell233 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable37 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow72 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell230 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport13 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail14 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable14 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow76 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader14 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable13 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow77 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable12 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow75 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport14 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail15 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable16 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow79 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader15 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable15 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow78 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.DetailReport15 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail16 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable18 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow81 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader16 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable17 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow80 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.DetailReport16 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail17 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable22 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow85 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader17 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable21 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow84 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable20 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow83 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable19 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow82 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport17 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail18 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable25 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow88 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell135 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell139 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader18 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable24 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow87 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable23 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow86 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport18 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail19 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable28 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow91 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell151 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader19 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable27 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow90 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell147 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable26 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow89 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport19 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail20 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable42 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow94 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell166 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell167 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell168 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell173 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader20 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable41 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow93 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell157 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell158 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell161 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell163 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell165 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable40 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow92 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell156 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport20 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail21 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable44 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow96 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow98 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell181 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow97 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell178 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell179 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader21 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable43 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow95 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell175 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport21 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail22 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable47 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow101 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell186 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell237 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader22 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable46 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow100 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell183 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable45 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow99 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell182 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport22 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail23 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable63 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow120 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell290 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell291 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell293 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell292 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow125 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell310 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell311 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell312 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell313 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow124 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell306 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell307 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell308 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell309 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow123 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell302 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell303 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell304 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell305 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow122 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell298 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell299 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell300 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell301 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow121 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell294 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell295 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell296 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell297 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable62 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow119 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell281 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader23 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable61 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow118 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell280 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport23 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail24 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable59 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow113 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell273 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell275 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell277 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell276 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow116 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell282 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell283 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell284 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell285 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow117 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell286 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell287 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell288 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell289 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow115 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell278 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell279 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable60 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow114 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell274 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader27 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable58 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow112 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell272 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport24 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail25 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable51 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow105 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell243 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell244 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell249 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell248 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell245 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader25 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable50 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow104 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell246 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell240 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell247 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell241 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell242 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable49 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow103 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell239 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable48 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow102 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell238 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport25 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail26 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable54 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow108 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell256 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell257 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell258 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell259 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell260 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader24 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable53 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow107 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell251 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell252 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell253 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell254 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell255 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable52 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow106 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell250 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport26 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail27 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable57 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow111 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell267 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell268 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell269 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell270 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell271 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader26 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable56 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow110 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell262 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell263 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell264 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell265 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell266 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable55 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow109 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell261 = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblPersonalDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonalDetailsSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblFraudIndSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblFraudSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblscrorevalues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblscoreColumns)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblScoreHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDebtSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDebtHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAccStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblCrAccStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblNameHistoryValues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblNameHistColumns)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblNameHistoryHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblpersonalConfirmationHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDetailedInfoHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblConfAddHistoryC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAddHistoryHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Height = 15;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 30;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2,
            this.xrPageInfo1,
            this.xrTable1});
            this.ReportHeader.Height = 53;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrPageInfo2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPageInfo2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrPageInfo2.Format = "Page {0} of {1}";
            this.xrPageInfo2.Location = new System.Drawing.Point(642, 8);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo2.Size = new System.Drawing.Size(67, 33);
            this.xrPageInfo2.StylePriority.UseBorderColor = false;
            this.xrPageInfo2.StylePriority.UseBorders = false;
            this.xrPageInfo2.StylePriority.UseFont = false;
            this.xrPageInfo2.StylePriority.UseTextAlignment = false;
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrPageInfo1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPageInfo1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrPageInfo1.Format = "{0:\"Report Printed Date\" dd/MM/yyyy HH:mm:ss}";
            this.xrPageInfo1.Location = new System.Drawing.Point(401, 8);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo1.Size = new System.Drawing.Size(240, 33);
            this.xrPageInfo1.StylePriority.UseBorderColor = false;
            this.xrPageInfo1.StylePriority.UseBorders = false;
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable1
            // 
            this.xrTable1.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Font = new System.Drawing.Font("Book Antiqua", 11F, System.Drawing.FontStyle.Bold);
            this.xrTable1.Location = new System.Drawing.Point(9, 8);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable1.Size = new System.Drawing.Size(392, 33);
            this.xrTable1.StylePriority.UseBorderColor = false;
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblHeaderText});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1;
            // 
            // lblHeaderText
            // 
            this.lblHeaderText.BackColor = System.Drawing.Color.Honeydew;
            this.lblHeaderText.Font = new System.Drawing.Font("Tahoma", 18.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeaderText.Name = "lblHeaderText";
            this.lblHeaderText.StylePriority.UseBackColor = false;
            this.lblHeaderText.StylePriority.UseFont = false;
            this.lblHeaderText.StylePriority.UseTextAlignment = false;
            this.lblHeaderText.Text = "XDS Consumer Trace Report";
            this.lblHeaderText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblHeaderText.Weight = 1.9059967585089144;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblPersonalDetail,
            this.xrTable2});
            this.GroupHeader1.Height = 101;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // tblPersonalDetail
            // 
            this.tblPersonalDetail.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblPersonalDetail.Location = new System.Drawing.Point(8, 67);
            this.tblPersonalDetail.Name = "tblPersonalDetail";
            this.tblPersonalDetail.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow12});
            this.tblPersonalDetail.Size = new System.Drawing.Size(292, 25);
            this.tblPersonalDetail.StylePriority.UseFont = false;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 1;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.Text = "Personal Details Summary";
            this.xrTableCell7.Weight = 3;
            // 
            // xrTable2
            // 
            this.xrTable2.Font = new System.Drawing.Font("Book Antiqua", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable2.Location = new System.Drawing.Point(8, 17);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow11});
            this.xrTable2.Size = new System.Drawing.Size(500, 25);
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 1;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Underline);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "SUMMARISED INFORMATION";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell6.Weight = 3;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1});
            this.DetailReport.DataMember = "ConsumerDetail";
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.PersonalDetailsSummary});
            this.Detail1.Height = 278;
            this.Detail1.Name = "Detail1";
            // 
            // PersonalDetailsSummary
            // 
            this.PersonalDetailsSummary.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.PersonalDetailsSummary.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.PersonalDetailsSummary.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PersonalDetailsSummary.Location = new System.Drawing.Point(8, 17);
            this.PersonalDetailsSummary.Name = "PersonalDetailsSummary";
            this.PersonalDetailsSummary.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow4,
            this.xrTableRow3,
            this.xrTableRow13,
            this.xrTableRow10,
            this.xrTableRow9,
            this.xrTableRow8,
            this.xrTableRow7,
            this.xrTableRow6,
            this.xrTableRow5});
            this.PersonalDetailsSummary.Size = new System.Drawing.Size(692, 250);
            this.PersonalDetailsSummary.StylePriority.UseBorderColor = false;
            this.PersonalDetailsSummary.StylePriority.UseBorders = false;
            this.PersonalDetailsSummary.StylePriority.UseFont = false;
            this.PersonalDetailsSummary.StylePriority.UseTextAlignment = false;
            this.PersonalDetailsSummary.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblReferenceNo,
            this.lblRefnoValue,
            this.lblExtRefNo,
            this.lblExternalRefNoValue});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1;
            // 
            // lblReferenceNo
            // 
            this.lblReferenceNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblReferenceNo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReferenceNo.Name = "lblReferenceNo";
            this.lblReferenceNo.StylePriority.UseBackColor = false;
            this.lblReferenceNo.StylePriority.UseFont = false;
            this.lblReferenceNo.Text = "Reference No.";
            this.lblReferenceNo.Weight = 0.514218009478673;
            // 
            // lblRefnoValue
            // 
            this.lblRefnoValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.ReferenceNo", "")});
            this.lblRefnoValue.Name = "lblRefnoValue";
            this.lblRefnoValue.Weight = 0.97301591650001362;
            // 
            // lblExtRefNo
            // 
            this.lblExtRefNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblExtRefNo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExtRefNo.Name = "lblExtRefNo";
            this.lblExtRefNo.StylePriority.UseBackColor = false;
            this.lblExtRefNo.StylePriority.UseFont = false;
            this.lblExtRefNo.Text = "External Reference No.";
            this.lblExtRefNo.Weight = 0.59807413089335126;
            // 
            // lblExternalRefNoValue
            // 
            this.lblExternalRefNoValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.ExternalReference", "")});
            this.lblExternalRefNoValue.Name = "lblExternalRefNoValue";
            this.lblExternalRefNoValue.Weight = 0.91469194312796209;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblIDNo,
            this.lblIDnoValue,
            this.lblPPNo,
            this.lblPassportnoValue});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1;
            // 
            // lblIDNo
            // 
            this.lblIDNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblIDNo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIDNo.Name = "lblIDNo";
            this.lblIDNo.StylePriority.UseBackColor = false;
            this.lblIDNo.StylePriority.UseFont = false;
            this.lblIDNo.Text = "ID No.";
            this.lblIDNo.Weight = 0.514218009478673;
            // 
            // lblIDnoValue
            // 
            this.lblIDnoValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.IDNo", "")});
            this.lblIDnoValue.Name = "lblIDnoValue";
            this.lblIDnoValue.Weight = 0.97301591650001362;
            // 
            // lblPPNo
            // 
            this.lblPPNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblPPNo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPPNo.Name = "lblPPNo";
            this.lblPPNo.StylePriority.UseBackColor = false;
            this.lblPPNo.StylePriority.UseFont = false;
            this.lblPPNo.Text = "Passport or 2nd ID No.";
            this.lblPPNo.Weight = 0.59807413089335126;
            // 
            // lblPassportnoValue
            // 
            this.lblPassportnoValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.PassportNo", "")});
            this.lblPassportnoValue.Name = "lblPassportnoValue";
            this.lblPassportnoValue.Weight = 0.91469194312796209;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSurname,
            this.lblSurnameValue,
            this.lblResAdd,
            this.lblResAddValue});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1;
            // 
            // lblSurname
            // 
            this.lblSurname.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblSurname.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSurname.Name = "lblSurname";
            this.lblSurname.StylePriority.UseBackColor = false;
            this.lblSurname.StylePriority.UseFont = false;
            this.lblSurname.Text = "Surname";
            this.lblSurname.Weight = 0.514218009478673;
            // 
            // lblSurnameValue
            // 
            this.lblSurnameValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.Surname", "")});
            this.lblSurnameValue.Name = "lblSurnameValue";
            this.lblSurnameValue.Weight = 0.97301591650001362;
            // 
            // lblResAdd
            // 
            this.lblResAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblResAdd.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResAdd.Name = "lblResAdd";
            this.lblResAdd.StylePriority.UseBackColor = false;
            this.lblResAdd.StylePriority.UseFont = false;
            this.lblResAdd.Text = "Residential Address";
            this.lblResAdd.Weight = 0.59807413089335126;
            // 
            // lblResAddValue
            // 
            this.lblResAddValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.ResidentialAddress", "")});
            this.lblResAddValue.Name = "lblResAddValue";
            this.lblResAddValue.Text = "lblResAddValue";
            this.lblResAddValue.Weight = 0.91469194312796209;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblFirstname,
            this.lblFirstnameValue,
            this.lbl,
            this.lblValue});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1;
            // 
            // lblFirstname
            // 
            this.lblFirstname.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblFirstname.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFirstname.Name = "lblFirstname";
            this.lblFirstname.StylePriority.UseBackColor = false;
            this.lblFirstname.StylePriority.UseFont = false;
            this.lblFirstname.Text = "First Name";
            this.lblFirstname.Weight = 0.514218009478673;
            // 
            // lblFirstnameValue
            // 
            this.lblFirstnameValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.FirstName", "")});
            this.lblFirstnameValue.Name = "lblFirstnameValue";
            this.lblFirstnameValue.Weight = 0.97301591650001362;
            // 
            // lbl
            // 
            this.lbl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lbl.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl.Name = "lbl";
            this.lbl.StylePriority.UseBackColor = false;
            this.lbl.StylePriority.UseBorders = false;
            this.lbl.StylePriority.UseFont = false;
            this.lbl.Weight = 0.59807413089335126;
            // 
            // lblValue
            // 
            this.lblValue.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblValue.Name = "lblValue";
            this.lblValue.StylePriority.UseBorders = false;
            this.lblValue.Weight = 0.91469194312796209;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSecondName,
            this.lblSecondnameValue,
            this.lblHomeTelNo,
            this.lblHomeTelNoValue});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1;
            // 
            // lblSecondName
            // 
            this.lblSecondName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblSecondName.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSecondName.Name = "lblSecondName";
            this.lblSecondName.StylePriority.UseBackColor = false;
            this.lblSecondName.StylePriority.UseFont = false;
            this.lblSecondName.Text = "Second Name";
            this.lblSecondName.Weight = 0.514218009478673;
            // 
            // lblSecondnameValue
            // 
            this.lblSecondnameValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.SecondName", "")});
            this.lblSecondnameValue.Name = "lblSecondnameValue";
            this.lblSecondnameValue.Weight = 0.97301591650001362;
            // 
            // lblHomeTelNo
            // 
            this.lblHomeTelNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblHomeTelNo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHomeTelNo.Name = "lblHomeTelNo";
            this.lblHomeTelNo.StylePriority.UseBackColor = false;
            this.lblHomeTelNo.StylePriority.UseFont = false;
            this.lblHomeTelNo.Text = "Telephone No.(H)";
            this.lblHomeTelNo.Weight = 0.59807413089335126;
            // 
            // lblHomeTelNoValue
            // 
            this.lblHomeTelNoValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.HomeTelephoneNo", "")});
            this.lblHomeTelNoValue.Name = "lblHomeTelNoValue";
            this.lblHomeTelNoValue.Text = "lblHomeTelNoValue";
            this.lblHomeTelNoValue.Weight = 0.91469194312796209;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblTitle,
            this.lbltitlevalue,
            this.lblPostalAdd,
            this.lblPostalAddvalue});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1;
            // 
            // lblTitle
            // 
            this.lblTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblTitle.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.StylePriority.UseBackColor = false;
            this.lblTitle.StylePriority.UseFont = false;
            this.lblTitle.Text = "Title";
            this.lblTitle.Weight = 0.514218009478673;
            // 
            // lbltitlevalue
            // 
            this.lbltitlevalue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.TitleDesc", "")});
            this.lbltitlevalue.Name = "lbltitlevalue";
            this.lbltitlevalue.Weight = 0.97301591650001362;
            // 
            // lblPostalAdd
            // 
            this.lblPostalAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblPostalAdd.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPostalAdd.Name = "lblPostalAdd";
            this.lblPostalAdd.StylePriority.UseBackColor = false;
            this.lblPostalAdd.StylePriority.UseFont = false;
            this.lblPostalAdd.Text = "Postal Address";
            this.lblPostalAdd.Weight = 0.59807413089335126;
            // 
            // lblPostalAddvalue
            // 
            this.lblPostalAddvalue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.PostalAddress", "")});
            this.lblPostalAddvalue.Name = "lblPostalAddvalue";
            this.lblPostalAddvalue.Weight = 0.91469194312796209;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblGender,
            this.lblGenderValue,
            this.lbl2,
            this.lbl2value});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1;
            // 
            // lblGender
            // 
            this.lblGender.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblGender.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGender.Name = "lblGender";
            this.lblGender.StylePriority.UseBackColor = false;
            this.lblGender.StylePriority.UseFont = false;
            this.lblGender.Text = "Gender";
            this.lblGender.Weight = 0.514218009478673;
            // 
            // lblGenderValue
            // 
            this.lblGenderValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.Gender", "")});
            this.lblGenderValue.Name = "lblGenderValue";
            this.lblGenderValue.Weight = 0.97301591650001362;
            // 
            // lbl2
            // 
            this.lbl2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lbl2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl2.Name = "lbl2";
            this.lbl2.StylePriority.UseBackColor = false;
            this.lbl2.StylePriority.UseFont = false;
            this.lbl2.Weight = 0.59807413089335126;
            // 
            // lbl2value
            // 
            this.lbl2value.Name = "lbl2value";
            this.lbl2value.Weight = 0.91469194312796209;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDOB,
            this.lblDOBValue,
            this.lblWorkTelNo,
            this.lblWorkTelNoValue});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1;
            // 
            // lblDOB
            // 
            this.lblDOB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblDOB.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDOB.Name = "lblDOB";
            this.lblDOB.StylePriority.UseBackColor = false;
            this.lblDOB.StylePriority.UseFont = false;
            this.lblDOB.Text = "Date of Birth";
            this.lblDOB.Weight = 0.514218009478673;
            // 
            // lblDOBValue
            // 
            this.lblDOBValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.BirthDate", "")});
            this.lblDOBValue.Name = "lblDOBValue";
            this.lblDOBValue.Weight = 0.97301591650001362;
            // 
            // lblWorkTelNo
            // 
            this.lblWorkTelNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblWorkTelNo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWorkTelNo.Name = "lblWorkTelNo";
            this.lblWorkTelNo.StylePriority.UseBackColor = false;
            this.lblWorkTelNo.StylePriority.UseFont = false;
            this.lblWorkTelNo.Text = "Telephone No.(W)";
            this.lblWorkTelNo.Weight = 0.59807413089335126;
            // 
            // lblWorkTelNoValue
            // 
            this.lblWorkTelNoValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.WorkTelephoneNo", "")});
            this.lblWorkTelNoValue.Name = "lblWorkTelNoValue";
            this.lblWorkTelNoValue.Weight = 0.91469194312796209;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCurrentEmployer,
            this.lblCurrentEmployerValue,
            this.lblMobileNo,
            this.lblMobileNoValue});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1;
            // 
            // lblCurrentEmployer
            // 
            this.lblCurrentEmployer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblCurrentEmployer.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentEmployer.Name = "lblCurrentEmployer";
            this.lblCurrentEmployer.StylePriority.UseBackColor = false;
            this.lblCurrentEmployer.StylePriority.UseFont = false;
            this.lblCurrentEmployer.Text = "Current Employer";
            this.lblCurrentEmployer.Weight = 0.514218009478673;
            // 
            // lblCurrentEmployerValue
            // 
            this.lblCurrentEmployerValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.EmployerDetail", "")});
            this.lblCurrentEmployerValue.Name = "lblCurrentEmployerValue";
            this.lblCurrentEmployerValue.Text = "lblCurrentEmployerValue";
            this.lblCurrentEmployerValue.Weight = 0.97301591650001362;
            // 
            // lblMobileNo
            // 
            this.lblMobileNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblMobileNo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMobileNo.Name = "lblMobileNo";
            this.lblMobileNo.StylePriority.UseBackColor = false;
            this.lblMobileNo.StylePriority.UseFont = false;
            this.lblMobileNo.Text = "Cellular/Mobile";
            this.lblMobileNo.Weight = 0.59807413089335126;
            // 
            // lblMobileNoValue
            // 
            this.lblMobileNoValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.CellularNo", "")});
            this.lblMobileNoValue.Name = "lblMobileNoValue";
            this.lblMobileNoValue.Weight = 0.91469194312796209;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblEmailAddress,
            this.lblEmailAddressValue,
            this.lblMaritalStatus,
            this.lblMaritalStatusValue});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1;
            // 
            // lblEmailAddress
            // 
            this.lblEmailAddress.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblEmailAddress.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmailAddress.Name = "lblEmailAddress";
            this.lblEmailAddress.StylePriority.UseBackColor = false;
            this.lblEmailAddress.StylePriority.UseFont = false;
            this.lblEmailAddress.Text = "E-mail Address";
            this.lblEmailAddress.Weight = 0.514218009478673;
            // 
            // lblEmailAddressValue
            // 
            this.lblEmailAddressValue.Name = "lblEmailAddressValue";
            this.lblEmailAddressValue.Text = "lblEmailAddressValue";
            this.lblEmailAddressValue.Weight = 0.97301591650001362;
            // 
            // lblMaritalStatus
            // 
            this.lblMaritalStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblMaritalStatus.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaritalStatus.Name = "lblMaritalStatus";
            this.lblMaritalStatus.StylePriority.UseBackColor = false;
            this.lblMaritalStatus.StylePriority.UseFont = false;
            this.lblMaritalStatus.Text = "Marital Status";
            this.lblMaritalStatus.Weight = 0.59807413089335126;
            // 
            // lblMaritalStatusValue
            // 
            this.lblMaritalStatusValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.MaritalStatusDesc", "")});
            this.lblMaritalStatusValue.Name = "lblMaritalStatusValue";
            this.lblMaritalStatusValue.Weight = 0.91469194312796209;
            // 
            // DetailReport1
            // 
            this.DetailReport1.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail2,
            this.GroupHeader2});
            this.DetailReport1.DataMember = "ConsumerFraudIndicatorsSummary";
            this.DetailReport1.Level = 1;
            this.DetailReport1.Name = "DetailReport1";
            // 
            // Detail2
            // 
            this.Detail2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblFraudIndSummary});
            this.Detail2.Height = 155;
            this.Detail2.Name = "Detail2";
            // 
            // tblFraudIndSummary
            // 
            this.tblFraudIndSummary.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tblFraudIndSummary.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblFraudIndSummary.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblFraudIndSummary.Location = new System.Drawing.Point(8, 17);
            this.tblFraudIndSummary.Name = "tblFraudIndSummary";
            this.tblFraudIndSummary.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow15,
            this.xrTableRow19,
            this.xrTableRow18,
            this.xrTableRow17,
            this.xrTableRow16});
            this.tblFraudIndSummary.Size = new System.Drawing.Size(700, 125);
            this.tblFraudIndSummary.StylePriority.UseBorderColor = false;
            this.tblFraudIndSummary.StylePriority.UseBorders = false;
            this.tblFraudIndSummary.StylePriority.UseFont = false;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblVerified,
            this.lblVerifiedVal});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 1;
            // 
            // lblVerified
            // 
            this.lblVerified.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblVerified.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVerified.Name = "lblVerified";
            this.lblVerified.StylePriority.UseBackColor = false;
            this.lblVerified.StylePriority.UseFont = false;
            this.lblVerified.Text = "ID No. Verified Status At Home Affairs";
            this.lblVerified.Weight = 1.1457142857142857;
            // 
            // lblVerifiedVal
            // 
            this.lblVerifiedVal.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerFraudIndicatorsSummary.HomeAffairsVerificationYN", "")});
            this.lblVerifiedVal.Name = "lblVerifiedVal";
            this.lblVerifiedVal.Text = "lblVerifiedVal";
            this.lblVerifiedVal.Weight = 1.8542857142857143;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDeceased,
            this.lblDeceasedValue});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 1;
            // 
            // lblDeceased
            // 
            this.lblDeceased.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblDeceased.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDeceased.Name = "lblDeceased";
            this.lblDeceased.StylePriority.UseBackColor = false;
            this.lblDeceased.StylePriority.UseFont = false;
            this.lblDeceased.Text = "ID No.Deceased Status At Home Affairs";
            this.lblDeceased.Weight = 1.1457142857142857;
            // 
            // lblDeceasedValue
            // 
            this.lblDeceasedValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerFraudIndicatorsSummary.HomeAffairsDeceasedDate", "")});
            this.lblDeceasedValue.Name = "lblDeceasedValue";
            this.lblDeceasedValue.Text = "lblDeceasedValue";
            this.lblDeceasedValue.Weight = 1.8542857142857143;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblFoundDB,
            this.lblFoundDBValue});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 1;
            // 
            // lblFoundDB
            // 
            this.lblFoundDB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblFoundDB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFoundDB.Name = "lblFoundDB";
            this.lblFoundDB.StylePriority.UseBackColor = false;
            this.lblFoundDB.StylePriority.UseFont = false;
            this.lblFoundDB.Text = "ID No. Found on Fraud Database";
            this.lblFoundDB.Weight = 1.1457142857142857;
            // 
            // lblFoundDBValue
            // 
            this.lblFoundDBValue.Name = "lblFoundDBValue";
            this.lblFoundDBValue.Text = "lblFoundDBValue";
            this.lblFoundDBValue.Weight = 1.8542857142857143;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblEmployerFraudDB,
            this.lblEmployerFraudDBValue});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 1;
            // 
            // lblEmployerFraudDB
            // 
            this.lblEmployerFraudDB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblEmployerFraudDB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmployerFraudDB.Name = "lblEmployerFraudDB";
            this.lblEmployerFraudDB.StylePriority.UseBackColor = false;
            this.lblEmployerFraudDB.StylePriority.UseFont = false;
            this.lblEmployerFraudDB.Text = "ID No. Found on Employer Fraud Database";
            this.lblEmployerFraudDB.Weight = 1.1457142857142857;
            // 
            // lblEmployerFraudDBValue
            // 
            this.lblEmployerFraudDBValue.Name = "lblEmployerFraudDBValue";
            this.lblEmployerFraudDBValue.Text = "lblEmployerFraudDBValue";
            this.lblEmployerFraudDBValue.Weight = 1.8542857142857143;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblProrectiveReg,
            this.lblProrectiveRegValue});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 1;
            // 
            // lblProrectiveReg
            // 
            this.lblProrectiveReg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblProrectiveReg.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProrectiveReg.Name = "lblProrectiveReg";
            this.lblProrectiveReg.StylePriority.UseBackColor = false;
            this.lblProrectiveReg.StylePriority.UseFont = false;
            this.lblProrectiveReg.Text = "ID No. Found on Protective Register";
            this.lblProrectiveReg.Weight = 1.1457142857142857;
            // 
            // lblProrectiveRegValue
            // 
            this.lblProrectiveRegValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerFraudIndicatorsSummary.ProtectiveVerificationYN", "")});
            this.lblProrectiveRegValue.Name = "lblProrectiveRegValue";
            this.lblProrectiveRegValue.Text = "lblProrectiveRegValue";
            this.lblProrectiveRegValue.Weight = 1.8542857142857143;
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblFraudSummary});
            this.GroupHeader2.Height = 40;
            this.GroupHeader2.Name = "GroupHeader2";
            // 
            // tblFraudSummary
            // 
            this.tblFraudSummary.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblFraudSummary.Location = new System.Drawing.Point(8, 8);
            this.tblFraudSummary.Name = "tblFraudSummary";
            this.tblFraudSummary.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow14});
            this.tblFraudSummary.Size = new System.Drawing.Size(333, 25);
            this.tblFraudSummary.StylePriority.UseFont = false;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.Text = "Potential fraud Indicators Summary";
            this.xrTableCell1.Weight = 3;
            // 
            // DetailReport2
            // 
            this.DetailReport2.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail3,
            this.GroupHeader3});
            this.DetailReport2.DataMember = "ConsumerScoring";
            this.DetailReport2.Level = 2;
            this.DetailReport2.Name = "DetailReport2";
            // 
            // Detail3
            // 
            this.Detail3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblscrorevalues});
            this.Detail3.Height = 27;
            this.Detail3.Name = "Detail3";
            // 
            // tblscrorevalues
            // 
            this.tblscrorevalues.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tblscrorevalues.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblscrorevalues.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblscrorevalues.Location = new System.Drawing.Point(8, 0);
            this.tblscrorevalues.Name = "tblscrorevalues";
            this.tblscrorevalues.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow22});
            this.tblscrorevalues.Size = new System.Drawing.Size(700, 25);
            this.tblscrorevalues.StylePriority.UseBorderColor = false;
            this.tblscrorevalues.StylePriority.UseBorders = false;
            this.tblscrorevalues.StylePriority.UseFont = false;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblUniqueIdentifierValue,
            this.lblDateValue,
            this.lblScroreValue,
            this.lblExceptionValue});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 1;
            // 
            // lblUniqueIdentifierValue
            // 
            this.lblUniqueIdentifierValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerScoring.Unique_Identifier", "")});
            this.lblUniqueIdentifierValue.Name = "lblUniqueIdentifierValue";
            this.lblUniqueIdentifierValue.Text = "lblUniqueIdentifierValue";
            this.lblUniqueIdentifierValue.Weight = 0.6785714285714286;
            // 
            // lblDateValue
            // 
            this.lblDateValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerScoring.ScoreDate", "")});
            this.lblDateValue.Name = "lblDateValue";
            this.lblDateValue.Text = "lblDateValue";
            this.lblDateValue.Weight = 0.75571428571428567;
            // 
            // lblScroreValue
            // 
            this.lblScroreValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerScoring.FinalScore", "")});
            this.lblScroreValue.Name = "lblScroreValue";
            this.lblScroreValue.Text = "lblScroreValue";
            this.lblScroreValue.Weight = 0.78285714285714292;
            // 
            // lblExceptionValue
            // 
            this.lblExceptionValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerScoring.Exception_Code", "")});
            this.lblExceptionValue.Name = "lblExceptionValue";
            this.lblExceptionValue.Text = "lblExceptionValue";
            this.lblExceptionValue.Weight = 0.78285714285714292;
            // 
            // GroupHeader3
            // 
            this.GroupHeader3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblscoreColumns,
            this.tblScoreHeader});
            this.GroupHeader3.Height = 72;
            this.GroupHeader3.Name = "GroupHeader3";
            // 
            // tblscoreColumns
            // 
            this.tblscoreColumns.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tblscoreColumns.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.tblscoreColumns.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tblscoreColumns.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblscoreColumns.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblscoreColumns.Location = new System.Drawing.Point(8, 47);
            this.tblscoreColumns.Name = "tblscoreColumns";
            this.tblscoreColumns.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow21});
            this.tblscoreColumns.Size = new System.Drawing.Size(700, 25);
            this.tblscoreColumns.StylePriority.UseBackColor = false;
            this.tblscoreColumns.StylePriority.UseBorderColor = false;
            this.tblscoreColumns.StylePriority.UseBorders = false;
            this.tblscoreColumns.StylePriority.UseFont = false;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblUniqueIdentifier,
            this.lblDate,
            this.lblScrore,
            this.lblException});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 1;
            // 
            // lblUniqueIdentifier
            // 
            this.lblUniqueIdentifier.CanGrow = false;
            this.lblUniqueIdentifier.Name = "lblUniqueIdentifier";
            this.lblUniqueIdentifier.Text = "Unique Identifier";
            this.lblUniqueIdentifier.Weight = 0.67857142857142871;
            // 
            // lblDate
            // 
            this.lblDate.CanGrow = false;
            this.lblDate.Name = "lblDate";
            this.lblDate.Text = "Score Date";
            this.lblDate.Weight = 0.75571428571428567;
            // 
            // lblScrore
            // 
            this.lblScrore.CanGrow = false;
            this.lblScrore.Name = "lblScrore";
            this.lblScrore.Text = "Final Score";
            this.lblScrore.Weight = 0.78285714285714281;
            // 
            // lblException
            // 
            this.lblException.CanGrow = false;
            this.lblException.Name = "lblException";
            this.lblException.Text = "Exception Code";
            this.lblException.Weight = 0.78285714285714292;
            // 
            // tblScoreHeader
            // 
            this.tblScoreHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblScoreHeader.Location = new System.Drawing.Point(8, 8);
            this.tblScoreHeader.Name = "tblScoreHeader";
            this.tblScoreHeader.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow20});
            this.tblScoreHeader.Size = new System.Drawing.Size(333, 25);
            this.tblScoreHeader.StylePriority.UseFont = false;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 1;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.Text = "XDS Presage Collection Score";
            this.xrTableCell2.Weight = 3;
            // 
            // DetailReport3
            // 
            this.DetailReport3.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail4,
            this.GroupHeader4});
            this.DetailReport3.DataMember = "ConsumerDebtSummary";
            this.DetailReport3.Level = 3;
            this.DetailReport3.Name = "DetailReport3";
            // 
            // Detail4
            // 
            this.Detail4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblDebtSummary});
            this.Detail4.Height = 317;
            this.Detail4.Name = "Detail4";
            // 
            // tblDebtSummary
            // 
            this.tblDebtSummary.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tblDebtSummary.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblDebtSummary.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblDebtSummary.Location = new System.Drawing.Point(8, 8);
            this.tblDebtSummary.Name = "tblDebtSummary";
            this.tblDebtSummary.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow24,
            this.xrTableRow36,
            this.xrTableRow35,
            this.xrTableRow34,
            this.xrTableRow33,
            this.xrTableRow32,
            this.xrTableRow31,
            this.xrTableRow30,
            this.xrTableRow29,
            this.xrTableRow28,
            this.xrTableRow27,
            this.xrTableRow26});
            this.tblDebtSummary.Size = new System.Drawing.Size(700, 300);
            this.tblDebtSummary.StylePriority.UseBorderColor = false;
            this.tblDebtSummary.StylePriority.UseBorders = false;
            this.tblDebtSummary.StylePriority.UseFont = false;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblActiveAcc,
            this.lblActiveAccValue});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 1;
            // 
            // lblActiveAcc
            // 
            this.lblActiveAcc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblActiveAcc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblActiveAcc.Name = "lblActiveAcc";
            this.lblActiveAcc.StylePriority.UseBackColor = false;
            this.lblActiveAcc.StylePriority.UseFont = false;
            this.lblActiveAcc.Text = "Total No. Of Active Accounts";
            this.lblActiveAcc.Weight = 1.1071428571428572;
            // 
            // lblActiveAccValue
            // 
            this.lblActiveAccValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.NoOFActiveAccounts", "")});
            this.lblActiveAccValue.Name = "lblActiveAccValue";
            this.lblActiveAccValue.Weight = 1.8928571428571428;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblAccGood,
            this.lblAccGoodvalue});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 1;
            // 
            // lblAccGood
            // 
            this.lblAccGood.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblAccGood.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAccGood.Name = "lblAccGood";
            this.lblAccGood.StylePriority.UseBackColor = false;
            this.lblAccGood.StylePriority.UseFont = false;
            this.lblAccGood.Text = "Total No. Of Accounts In Good Standing";
            this.lblAccGood.Weight = 1.1071428571428572;
            // 
            // lblAccGoodvalue
            // 
            this.lblAccGoodvalue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.NoOfAccountInGoodStanding", "")});
            this.lblAccGoodvalue.Name = "lblAccGoodvalue";
            this.lblAccGoodvalue.Weight = 1.8928571428571428;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDelinquentAcc,
            this.lblDelinquentAccValue});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Weight = 1;
            // 
            // lblDelinquentAcc
            // 
            this.lblDelinquentAcc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblDelinquentAcc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDelinquentAcc.Name = "lblDelinquentAcc";
            this.lblDelinquentAcc.StylePriority.UseBackColor = false;
            this.lblDelinquentAcc.StylePriority.UseFont = false;
            this.lblDelinquentAcc.Text = "Total No. Of Delinquent Accounts";
            this.lblDelinquentAcc.Weight = 1.1071428571428572;
            // 
            // lblDelinquentAccValue
            // 
            this.lblDelinquentAccValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.NoOfAccountInBadStanding", "")});
            this.lblDelinquentAccValue.Name = "lblDelinquentAccValue";
            this.lblDelinquentAccValue.Weight = 1.8928571428571428;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblMonthlyinst,
            this.lblMonthlyinstValue});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 1;
            // 
            // lblMonthlyinst
            // 
            this.lblMonthlyinst.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblMonthlyinst.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMonthlyinst.Name = "lblMonthlyinst";
            this.lblMonthlyinst.StylePriority.UseBackColor = false;
            this.lblMonthlyinst.StylePriority.UseFont = false;
            this.lblMonthlyinst.Text = "Total Monthly Installments";
            this.lblMonthlyinst.Weight = 1.1071428571428572;
            // 
            // lblMonthlyinstValue
            // 
            this.lblMonthlyinstValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.TotalMonthlyInstallment", "")});
            this.lblMonthlyinstValue.Name = "lblMonthlyinstValue";
            this.lblMonthlyinstValue.Weight = 1.8928571428571428;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblOutstandingDebt,
            this.lblOutstandingDebtValue});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Weight = 1;
            // 
            // lblOutstandingDebt
            // 
            this.lblOutstandingDebt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblOutstandingDebt.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOutstandingDebt.Name = "lblOutstandingDebt";
            this.lblOutstandingDebt.StylePriority.UseBackColor = false;
            this.lblOutstandingDebt.StylePriority.UseFont = false;
            this.lblOutstandingDebt.Text = "Total Outstanding Debt";
            this.lblOutstandingDebt.Weight = 1.1071428571428572;
            // 
            // lblOutstandingDebtValue
            // 
            this.lblOutstandingDebtValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.TotalOutStandingDebt", "")});
            this.lblOutstandingDebtValue.Name = "lblOutstandingDebtValue";
            this.lblOutstandingDebtValue.Weight = 1.8928571428571428;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblArrearamt,
            this.lblArrearamtvalue});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Weight = 1;
            // 
            // lblArrearamt
            // 
            this.lblArrearamt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblArrearamt.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblArrearamt.Name = "lblArrearamt";
            this.lblArrearamt.StylePriority.UseBackColor = false;
            this.lblArrearamt.StylePriority.UseFont = false;
            this.lblArrearamt.Text = "Total Arrear Amount";
            this.lblArrearamt.Weight = 1.1071428571428572;
            // 
            // lblArrearamtvalue
            // 
            this.lblArrearamtvalue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.TotalArrearAmount", "")});
            this.lblArrearamtvalue.Name = "lblArrearamtvalue";
            this.lblArrearamtvalue.Weight = 1.8928571428571428;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblJdgDate,
            this.lblJdgDateValue});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 1;
            // 
            // lblJdgDate
            // 
            this.lblJdgDate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblJdgDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJdgDate.Name = "lblJdgDate";
            this.lblJdgDate.StylePriority.UseBackColor = false;
            this.lblJdgDate.StylePriority.UseFont = false;
            this.lblJdgDate.Text = "Most Recent Judgement Date";
            this.lblJdgDate.Weight = 1.1071428571428572;
            // 
            // lblJdgDateValue
            // 
            this.lblJdgDateValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.MostRecentJudgmentDate", "")});
            this.lblJdgDateValue.Name = "lblJdgDateValue";
            this.lblJdgDateValue.Weight = 1.8928571428571428;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblJudAmt,
            this.lblJudAmtValue});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Weight = 1;
            // 
            // lblJudAmt
            // 
            this.lblJudAmt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblJudAmt.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJudAmt.Name = "lblJudAmt";
            this.lblJudAmt.StylePriority.UseBackColor = false;
            this.lblJudAmt.StylePriority.UseFont = false;
            this.lblJudAmt.Text = "Highest Judgement Amount";
            this.lblJudAmt.Weight = 1.1071428571428572;
            // 
            // lblJudAmtValue
            // 
            this.lblJudAmtValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.HighestJudgmentAmount", "")});
            this.lblJudAmtValue.Name = "lblJudAmtValue";
            this.lblJudAmtValue.Weight = 1.8928571428571428;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDelinquentrating,
            this.lblDelinquentratingValue});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 1;
            // 
            // lblDelinquentrating
            // 
            this.lblDelinquentrating.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblDelinquentrating.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDelinquentrating.Name = "lblDelinquentrating";
            this.lblDelinquentrating.StylePriority.UseBackColor = false;
            this.lblDelinquentrating.StylePriority.UseFont = false;
            this.lblDelinquentrating.Text = "Highest Delinquent Rating (Last 24 Months)";
            this.lblDelinquentrating.Weight = 1.1071428571428572;
            // 
            // lblDelinquentratingValue
            // 
            this.lblDelinquentratingValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.HighestDelinquentRating", "")});
            this.lblDelinquentratingValue.Name = "lblDelinquentratingValue";
            this.lblDelinquentratingValue.Weight = 1.8928571428571428;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDbtReview,
            this.lblDbtReviewValue});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 1;
            // 
            // lblDbtReview
            // 
            this.lblDbtReview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblDbtReview.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDbtReview.Name = "lblDbtReview";
            this.lblDbtReview.StylePriority.UseBackColor = false;
            this.lblDbtReview.StylePriority.UseFont = false;
            this.lblDbtReview.Text = "Debt Review Status";
            this.lblDbtReview.Weight = 1.1071428571428572;
            // 
            // lblDbtReviewValue
            // 
            this.lblDbtReviewValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.DebtReviewStatus", "")});
            this.lblDbtReviewValue.Name = "lblDbtReviewValue";
            this.lblDbtReviewValue.Weight = 1.8928571428571428;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblAO,
            this.lblAOValue});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 1;
            // 
            // lblAO
            // 
            this.lblAO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblAO.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAO.Name = "lblAO";
            this.lblAO.StylePriority.UseBackColor = false;
            this.lblAO.StylePriority.UseFont = false;
            this.lblAO.Text = "Admin Order Status";
            this.lblAO.Weight = 1.1071428571428572;
            // 
            // lblAOValue
            // 
            this.lblAOValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.AdminOrderStatus", "")});
            this.lblAOValue.Name = "lblAOValue";
            this.lblAOValue.Weight = 1.8928571428571428;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblAdverseAmt,
            this.lblAdverseAmtValue});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 1;
            // 
            // lblAdverseAmt
            // 
            this.lblAdverseAmt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblAdverseAmt.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAdverseAmt.Name = "lblAdverseAmt";
            this.lblAdverseAmt.StylePriority.UseBackColor = false;
            this.lblAdverseAmt.StylePriority.UseFont = false;
            this.lblAdverseAmt.Text = "Total Adverse Amount";
            this.lblAdverseAmt.Weight = 1.1071428571428572;
            // 
            // lblAdverseAmtValue
            // 
            this.lblAdverseAmtValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.TotalAdverseAmount", "")});
            this.lblAdverseAmtValue.Name = "lblAdverseAmtValue";
            this.lblAdverseAmtValue.Weight = 1.8928571428571428;
            // 
            // GroupHeader4
            // 
            this.GroupHeader4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblDebtHeader});
            this.GroupHeader4.Height = 36;
            this.GroupHeader4.Name = "GroupHeader4";
            // 
            // tblDebtHeader
            // 
            this.tblDebtHeader.BackColor = System.Drawing.Color.Transparent;
            this.tblDebtHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblDebtHeader.Location = new System.Drawing.Point(8, 8);
            this.tblDebtHeader.Name = "tblDebtHeader";
            this.tblDebtHeader.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow23});
            this.tblDebtHeader.Size = new System.Drawing.Size(333, 25);
            this.tblDebtHeader.StylePriority.UseBackColor = false;
            this.tblDebtHeader.StylePriority.UseFont = false;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 1;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.Text = "Debt Summary";
            this.xrTableCell3.Weight = 3;
            // 
            // DetailReport4
            // 
            this.DetailReport4.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail5,
            this.GroupHeader5});
            this.DetailReport4.DataMember = "ConsumerAccountGoodBadSummary";
            this.DetailReport4.Level = 4;
            this.DetailReport4.Name = "DetailReport4";
            // 
            // Detail5
            // 
            this.Detail5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblAccStatus});
            this.Detail5.Height = 304;
            this.Detail5.Name = "Detail5";
            // 
            // tblAccStatus
            // 
            this.tblAccStatus.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tblAccStatus.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblAccStatus.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblAccStatus.Location = new System.Drawing.Point(8, 8);
            this.tblAccStatus.Name = "tblAccStatus";
            this.tblAccStatus.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow39,
            this.xrTableRow38,
            this.xrTableRow47,
            this.xrTableRow46,
            this.xrTableRow45,
            this.xrTableRow44,
            this.xrTableRow43,
            this.xrTableRow42,
            this.xrTableRow41,
            this.xrTableRow40,
            this.xrTableRow37});
            this.tblAccStatus.Size = new System.Drawing.Size(501, 275);
            this.tblAccStatus.StylePriority.UseBorderColor = false;
            this.tblAccStatus.StylePriority.UseBorders = false;
            this.tblAccStatus.StylePriority.UseFont = false;
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblAccdesc,
            this.lblGood,
            this.lblBad});
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Weight = 1;
            // 
            // lblAccdesc
            // 
            this.lblAccdesc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblAccdesc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAccdesc.Name = "lblAccdesc";
            this.lblAccdesc.StylePriority.UseBackColor = false;
            this.lblAccdesc.StylePriority.UseFont = false;
            this.lblAccdesc.Text = "Account Description";
            this.lblAccdesc.Weight = 1.6414473684210527;
            // 
            // lblGood
            // 
            this.lblGood.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGood.Name = "lblGood";
            this.lblGood.StylePriority.UseFont = false;
            this.lblGood.Text = "Good";
            this.lblGood.Weight = 0.41282894736842096;
            // 
            // lblBad
            // 
            this.lblBad.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBad.Name = "lblBad";
            this.lblBad.StylePriority.UseFont = false;
            this.lblBad.Text = "Bad";
            this.lblBad.Weight = 0.41776315789473678;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblRetail,
            this.lblRetailG,
            this.lblRetailB});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Weight = 1;
            // 
            // lblRetail
            // 
            this.lblRetail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblRetail.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRetail.Name = "lblRetail";
            this.lblRetail.StylePriority.UseBackColor = false;
            this.lblRetail.StylePriority.UseFont = false;
            this.lblRetail.Text = "Total no. Of Active Retail Accounts";
            this.lblRetail.Weight = 1.6414473684210527;
            // 
            // lblRetailG
            // 
            this.lblRetailG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfRetailAccountsGood", "")});
            this.lblRetailG.Name = "lblRetailG";
            this.lblRetailG.StylePriority.UseForeColor = false;
            this.lblRetailG.Weight = 0.41282894736842096;
            // 
            // lblRetailB
            // 
            this.lblRetailB.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfRetailAccountsBad", "")});
            this.lblRetailB.Name = "lblRetailB";
            this.lblRetailB.Weight = 0.41776315789473678;
            // 
            // xrTableRow47
            // 
            this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCc,
            this.lblCcG,
            this.lblCcB});
            this.xrTableRow47.Name = "xrTableRow47";
            this.xrTableRow47.Weight = 1;
            // 
            // lblCc
            // 
            this.lblCc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblCc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCc.Name = "lblCc";
            this.lblCc.StylePriority.UseBackColor = false;
            this.lblCc.StylePriority.UseFont = false;
            this.lblCc.Text = "Total no. Of Active Credit card Accounts";
            this.lblCc.Weight = 1.6414473684210527;
            // 
            // lblCcG
            // 
            this.lblCcG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfCreditCardAccountsGood", "")});
            this.lblCcG.Name = "lblCcG";
            this.lblCcG.Weight = 0.41282894736842096;
            // 
            // lblCcB
            // 
            this.lblCcB.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfCreditCardAccountsBad", "")});
            this.lblCcB.Name = "lblCcB";
            this.lblCcB.Weight = 0.41776315789473678;
            // 
            // xrTableRow46
            // 
            this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblFurnAcc,
            this.lblFurnAccG,
            this.lblFurnAccB});
            this.xrTableRow46.Name = "xrTableRow46";
            this.xrTableRow46.Weight = 1;
            // 
            // lblFurnAcc
            // 
            this.lblFurnAcc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblFurnAcc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFurnAcc.Name = "lblFurnAcc";
            this.lblFurnAcc.StylePriority.UseBackColor = false;
            this.lblFurnAcc.StylePriority.UseFont = false;
            this.lblFurnAcc.Text = "Total no. Of Active Furniture Store Accounts";
            this.lblFurnAcc.Weight = 1.6414473684210527;
            // 
            // lblFurnAccG
            // 
            this.lblFurnAccG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfFurnitureAccountsGood", "")});
            this.lblFurnAccG.Name = "lblFurnAccG";
            this.lblFurnAccG.Weight = 0.41282894736842096;
            // 
            // lblFurnAccB
            // 
            this.lblFurnAccB.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfFurnitureAccountsBad", "")});
            this.lblFurnAccB.Name = "lblFurnAccB";
            this.lblFurnAccB.Weight = 0.41776315789473678;
            // 
            // xrTableRow45
            // 
            this.xrTableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblInsurance,
            this.lblInsuranceG,
            this.lblInsuranceB});
            this.xrTableRow45.Name = "xrTableRow45";
            this.xrTableRow45.Weight = 1;
            // 
            // lblInsurance
            // 
            this.lblInsurance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblInsurance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInsurance.Name = "lblInsurance";
            this.lblInsurance.StylePriority.UseBackColor = false;
            this.lblInsurance.StylePriority.UseFont = false;
            this.lblInsurance.Text = "Total no. Of Active Insurance Accounts";
            this.lblInsurance.Weight = 1.6414473684210527;
            // 
            // lblInsuranceG
            // 
            this.lblInsuranceG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfInsuranceAccountsGood", "")});
            this.lblInsuranceG.Name = "lblInsuranceG";
            this.lblInsuranceG.Weight = 0.41282894736842096;
            // 
            // lblInsuranceB
            // 
            this.lblInsuranceB.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfInsuranceAccountsBad", "")});
            this.lblInsuranceB.Name = "lblInsuranceB";
            this.lblInsuranceB.Weight = 0.41776315789473678;
            // 
            // xrTableRow44
            // 
            this.xrTableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblFinance,
            this.lblFinanceG,
            this.lblFinanceB});
            this.xrTableRow44.Name = "xrTableRow44";
            this.xrTableRow44.Weight = 1;
            // 
            // lblFinance
            // 
            this.lblFinance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblFinance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFinance.Name = "lblFinance";
            this.lblFinance.StylePriority.UseBackColor = false;
            this.lblFinance.StylePriority.UseFont = false;
            this.lblFinance.Text = "Total no. Of Active Personal Finance Accounts";
            this.lblFinance.Weight = 1.6414473684210527;
            // 
            // lblFinanceG
            // 
            this.lblFinanceG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfPersonalFinAccountsGood", "")});
            this.lblFinanceG.Name = "lblFinanceG";
            this.lblFinanceG.Weight = 0.41282894736842096;
            // 
            // lblFinanceB
            // 
            this.lblFinanceB.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfPersonalFinAccountsBad", "")});
            this.lblFinanceB.Name = "lblFinanceB";
            this.lblFinanceB.Weight = 0.41776315789473678;
            // 
            // xrTableRow43
            // 
            this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblBankAcc,
            this.lblBankAccG,
            this.lblBankAccB});
            this.xrTableRow43.Name = "xrTableRow43";
            this.xrTableRow43.Weight = 1;
            // 
            // lblBankAcc
            // 
            this.lblBankAcc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblBankAcc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBankAcc.Name = "lblBankAcc";
            this.lblBankAcc.StylePriority.UseBackColor = false;
            this.lblBankAcc.StylePriority.UseFont = false;
            this.lblBankAcc.Text = "Total no. Of Active Banl Accounts";
            this.lblBankAcc.Weight = 1.6414473684210527;
            // 
            // lblBankAccG
            // 
            this.lblBankAccG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfBankAccountsGood", "")});
            this.lblBankAccG.Name = "lblBankAccG";
            this.lblBankAccG.Weight = 0.41282894736842096;
            // 
            // lblBankAccB
            // 
            this.lblBankAccB.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfBankAccountsBad", "")});
            this.lblBankAccB.Name = "lblBankAccB";
            this.lblBankAccB.Weight = 0.41776315789473678;
            // 
            // xrTableRow42
            // 
            this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblTelecomms,
            this.lblTelecommsG,
            this.lblTelecommsB});
            this.xrTableRow42.Name = "xrTableRow42";
            this.xrTableRow42.Weight = 1;
            // 
            // lblTelecomms
            // 
            this.lblTelecomms.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblTelecomms.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelecomms.Name = "lblTelecomms";
            this.lblTelecomms.StylePriority.UseBackColor = false;
            this.lblTelecomms.StylePriority.UseFont = false;
            this.lblTelecomms.Text = "Total no. Of Active Telecomms Accounts";
            this.lblTelecomms.Weight = 1.6414473684210527;
            // 
            // lblTelecommsG
            // 
            this.lblTelecommsG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfTelecomAccountsGood", "")});
            this.lblTelecommsG.Name = "lblTelecommsG";
            this.lblTelecommsG.Weight = 0.41282894736842096;
            // 
            // lblTelecommsB
            // 
            this.lblTelecommsB.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfTelecomAccountsBad", "")});
            this.lblTelecommsB.Name = "lblTelecommsB";
            this.lblTelecommsB.Weight = 0.41776315789473678;
            // 
            // xrTableRow41
            // 
            this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblHomeLoan,
            this.lblHomeLoanG,
            this.lblHomeLoanB});
            this.xrTableRow41.Name = "xrTableRow41";
            this.xrTableRow41.Weight = 1;
            // 
            // lblHomeLoan
            // 
            this.lblHomeLoan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblHomeLoan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHomeLoan.Name = "lblHomeLoan";
            this.lblHomeLoan.StylePriority.UseBackColor = false;
            this.lblHomeLoan.StylePriority.UseFont = false;
            this.lblHomeLoan.Text = "Total no. Of Active Home Loan Accounts";
            this.lblHomeLoan.Weight = 1.6414473684210527;
            // 
            // lblHomeLoanG
            // 
            this.lblHomeLoanG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfHomeLoanAccountsGood", "")});
            this.lblHomeLoanG.Name = "lblHomeLoanG";
            this.lblHomeLoanG.Weight = 0.41282894736842096;
            // 
            // lblHomeLoanB
            // 
            this.lblHomeLoanB.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfHomeLoanAccountsBad", "")});
            this.lblHomeLoanB.Name = "lblHomeLoanB";
            this.lblHomeLoanB.Weight = 0.41776315789473678;
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblVehicleFin,
            this.lblVehicleFinG,
            this.lblVehicleFinB});
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.Weight = 1;
            // 
            // lblVehicleFin
            // 
            this.lblVehicleFin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblVehicleFin.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVehicleFin.Name = "lblVehicleFin";
            this.lblVehicleFin.StylePriority.UseBackColor = false;
            this.lblVehicleFin.StylePriority.UseFont = false;
            this.lblVehicleFin.Text = "Total no. Of Active Motor Vehicle Finance Accounts";
            this.lblVehicleFin.Weight = 1.6414473684210527;
            // 
            // lblVehicleFinG
            // 
            this.lblVehicleFinG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfMotorFinanceAccountsGood", "")});
            this.lblVehicleFinG.Name = "lblVehicleFinG";
            this.lblVehicleFinG.Weight = 0.41282894736842096;
            // 
            // lblVehicleFinB
            // 
            this.lblVehicleFinB.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfMotorFinanceAccountsBad", "")});
            this.lblVehicleFinB.Name = "lblVehicleFinB";
            this.lblVehicleFinB.Weight = 0.41776315789473678;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblOtherAcc,
            this.lblOtherAccG,
            this.lblOtherAccB});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Weight = 1;
            // 
            // lblOtherAcc
            // 
            this.lblOtherAcc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblOtherAcc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOtherAcc.Name = "lblOtherAcc";
            this.lblOtherAcc.StylePriority.UseBackColor = false;
            this.lblOtherAcc.StylePriority.UseFont = false;
            this.lblOtherAcc.Text = "Total no. Of Active Other Accounts";
            this.lblOtherAcc.Weight = 1.6414473684210527;
            // 
            // lblOtherAccG
            // 
            this.lblOtherAccG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfOtherAccountsGood", "")});
            this.lblOtherAccG.Name = "lblOtherAccG";
            this.lblOtherAccG.Weight = 0.41282894736842096;
            // 
            // lblOtherAccB
            // 
            this.lblOtherAccB.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfOtherAccountsBad", "")});
            this.lblOtherAccB.Name = "lblOtherAccB";
            this.lblOtherAccB.Weight = 0.41776315789473678;
            // 
            // GroupHeader5
            // 
            this.GroupHeader5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblCrAccStatus});
            this.GroupHeader5.Height = 34;
            this.GroupHeader5.Name = "GroupHeader5";
            // 
            // tblCrAccStatus
            // 
            this.tblCrAccStatus.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblCrAccStatus.Location = new System.Drawing.Point(8, 8);
            this.tblCrAccStatus.Name = "tblCrAccStatus";
            this.tblCrAccStatus.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow25});
            this.tblCrAccStatus.Size = new System.Drawing.Size(333, 25);
            this.tblCrAccStatus.StylePriority.UseFont = false;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 1;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.Text = "Credit Account Status Summary";
            this.xrTableCell4.Weight = 3;
            // 
            // DetailReport5
            // 
            this.DetailReport5.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail6,
            this.GroupHeader6});
            this.DetailReport5.DataMember = "ConsumerDetailConfirmationName";
            this.DetailReport5.Level = 5;
            this.DetailReport5.Name = "DetailReport5";
            // 
            // Detail6
            // 
            this.Detail6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Detail6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblNameHistoryValues});
            this.Detail6.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.Detail6.Height = 25;
            this.Detail6.Name = "Detail6";
            this.Detail6.StylePriority.UseBorders = false;
            this.Detail6.StylePriority.UseForeColor = false;
            // 
            // tblNameHistoryValues
            // 
            this.tblNameHistoryValues.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tblNameHistoryValues.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblNameHistoryValues.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tblNameHistoryValues.Location = new System.Drawing.Point(8, 0);
            this.tblNameHistoryValues.Name = "tblNameHistoryValues";
            this.tblNameHistoryValues.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow52});
            this.tblNameHistoryValues.Size = new System.Drawing.Size(700, 25);
            this.tblNameHistoryValues.StylePriority.UseBorderColor = false;
            this.tblNameHistoryValues.StylePriority.UseFont = false;
            this.tblNameHistoryValues.StylePriority.UseForeColor = false;
            // 
            // xrTableRow52
            // 
            this.xrTableRow52.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblNameValue,
            this.lblNHSurNameValue,
            this.lblNHIDNoValue,
            this.lblNHdatevalue});
            this.xrTableRow52.Name = "xrTableRow52";
            this.xrTableRow52.Weight = 1;
            // 
            // lblNameValue
            // 
            this.lblNameValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationName.Name", "")});
            this.lblNameValue.Name = "lblNameValue";
            this.lblNameValue.Weight = 1;
            // 
            // lblNHSurNameValue
            // 
            this.lblNHSurNameValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationName.Surname", "")});
            this.lblNHSurNameValue.Name = "lblNHSurNameValue";
            this.lblNHSurNameValue.Weight = 1;
            // 
            // lblNHIDNoValue
            // 
            this.lblNHIDNoValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationName.IDno", "")});
            this.lblNHIDNoValue.Name = "lblNHIDNoValue";
            this.lblNHIDNoValue.Weight = 0.5;
            // 
            // lblNHdatevalue
            // 
            this.lblNHdatevalue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationName.DateConfirmed", "")});
            this.lblNHdatevalue.Name = "lblNHdatevalue";
            this.lblNHdatevalue.Weight = 0.5;
            // 
            // GroupHeader6
            // 
            this.GroupHeader6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblNameHistColumns,
            this.tblNameHistoryHeader,
            this.tblpersonalConfirmationHeader,
            this.tblDetailedInfoHeader});
            this.GroupHeader6.Height = 158;
            this.GroupHeader6.Name = "GroupHeader6";
            // 
            // tblNameHistColumns
            // 
            this.tblNameHistColumns.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tblNameHistColumns.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.tblNameHistColumns.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tblNameHistColumns.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblNameHistColumns.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblNameHistColumns.Location = new System.Drawing.Point(8, 133);
            this.tblNameHistColumns.Name = "tblNameHistColumns";
            this.tblNameHistColumns.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow51});
            this.tblNameHistColumns.Size = new System.Drawing.Size(700, 25);
            this.tblNameHistColumns.StylePriority.UseBackColor = false;
            this.tblNameHistColumns.StylePriority.UseBorderColor = false;
            this.tblNameHistColumns.StylePriority.UseBorders = false;
            this.tblNameHistColumns.StylePriority.UseFont = false;
            // 
            // xrTableRow51
            // 
            this.xrTableRow51.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblName,
            this.lblNHSurName,
            this.lblNHIDNo,
            this.lblNHdate});
            this.xrTableRow51.Name = "xrTableRow51";
            this.xrTableRow51.Weight = 1;
            // 
            // lblName
            // 
            this.lblName.CanGrow = false;
            this.lblName.Name = "lblName";
            this.lblName.Text = "Name";
            this.lblName.Weight = 1;
            // 
            // lblNHSurName
            // 
            this.lblNHSurName.CanGrow = false;
            this.lblNHSurName.Name = "lblNHSurName";
            this.lblNHSurName.Text = "SurName";
            this.lblNHSurName.Weight = 1;
            // 
            // lblNHIDNo
            // 
            this.lblNHIDNo.CanGrow = false;
            this.lblNHIDNo.Name = "lblNHIDNo";
            this.lblNHIDNo.Text = "ID No";
            this.lblNHIDNo.Weight = 0.5;
            // 
            // lblNHdate
            // 
            this.lblNHdate.CanGrow = false;
            this.lblNHdate.Name = "lblNHdate";
            this.lblNHdate.Text = "Date Confirmed";
            this.lblNHdate.Weight = 0.5;
            // 
            // tblNameHistoryHeader
            // 
            this.tblNameHistoryHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblNameHistoryHeader.Location = new System.Drawing.Point(8, 92);
            this.tblNameHistoryHeader.Name = "tblNameHistoryHeader";
            this.tblNameHistoryHeader.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow50});
            this.tblNameHistoryHeader.Size = new System.Drawing.Size(375, 25);
            this.tblNameHistoryHeader.StylePriority.UseFont = false;
            // 
            // xrTableRow50
            // 
            this.xrTableRow50.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9});
            this.xrTableRow50.Name = "xrTableRow50";
            this.xrTableRow50.Weight = 1;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.Text = "Name History";
            this.xrTableCell9.Weight = 3;
            // 
            // tblpersonalConfirmationHeader
            // 
            this.tblpersonalConfirmationHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblpersonalConfirmationHeader.Location = new System.Drawing.Point(8, 50);
            this.tblpersonalConfirmationHeader.Name = "tblpersonalConfirmationHeader";
            this.tblpersonalConfirmationHeader.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow49});
            this.tblpersonalConfirmationHeader.Size = new System.Drawing.Size(375, 25);
            this.tblpersonalConfirmationHeader.StylePriority.UseFont = false;
            // 
            // xrTableRow49
            // 
            this.xrTableRow49.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8});
            this.xrTableRow49.Name = "xrTableRow49";
            this.xrTableRow49.Weight = 1;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.Text = "Personal Details Confirmation Table";
            this.xrTableCell8.Weight = 3;
            // 
            // tblDetailedInfoHeader
            // 
            this.tblDetailedInfoHeader.Font = new System.Drawing.Font("Book Antiqua", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblDetailedInfoHeader.Location = new System.Drawing.Point(8, 8);
            this.tblDetailedInfoHeader.Name = "tblDetailedInfoHeader";
            this.tblDetailedInfoHeader.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow48});
            this.tblDetailedInfoHeader.Size = new System.Drawing.Size(500, 25);
            this.tblDetailedInfoHeader.StylePriority.UseFont = false;
            this.tblDetailedInfoHeader.StylePriority.UseTextAlignment = false;
            this.tblDetailedInfoHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow48
            // 
            this.xrTableRow48.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5});
            this.xrTableRow48.Name = "xrTableRow48";
            this.xrTableRow48.Weight = 1;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Underline);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.Text = "DETAILED INFORMATION";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell5.Weight = 3;
            // 
            // DetailReport6
            // 
            this.DetailReport6.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail7,
            this.GroupHeader7});
            this.DetailReport6.DataMember = "ConsumerDetailConfirmationAddress";
            this.DetailReport6.Level = 6;
            this.DetailReport6.Name = "DetailReport6";
            // 
            // Detail7
            // 
            this.Detail7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.Detail7.Height = 25;
            this.Detail7.Name = "Detail7";
            // 
            // xrTable3
            // 
            this.xrTable3.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.Location = new System.Drawing.Point(8, 0);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow55});
            this.xrTable3.Size = new System.Drawing.Size(700, 25);
            this.xrTable3.StylePriority.UseBorderColor = false;
            this.xrTable3.StylePriority.UseBorders = false;
            // 
            // xrTableRow55
            // 
            this.xrTableRow55.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell20,
            this.xrTableCell17,
            this.xrTableCell21,
            this.xrTableCell18,
            this.xrTableCell22,
            this.xrTableCell19});
            this.xrTableRow55.Name = "xrTableRow55";
            this.xrTableRow55.Weight = 1;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationAddress.AddressType", "")});
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Weight = 0.2857142857142857;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationAddress.Address1", "")});
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Weight = 0.96714285714285708;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationAddress.Address2", "")});
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Weight = 0.35000000000000003;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationAddress.Address3", "")});
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Weight = 0.50428571428571423;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationAddress.Address4", "")});
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Weight = 0.46142857142857141;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationAddress.DateConfirmed", "")});
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Weight = 0.43142857142857144;
            // 
            // GroupHeader7
            // 
            this.GroupHeader7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblConfAddHistoryC,
            this.tblAddHistoryHeader});
            this.GroupHeader7.Height = 77;
            this.GroupHeader7.Name = "GroupHeader7";
            // 
            // tblConfAddHistoryC
            // 
            this.tblConfAddHistoryC.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tblConfAddHistoryC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.tblConfAddHistoryC.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tblConfAddHistoryC.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblConfAddHistoryC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblConfAddHistoryC.Location = new System.Drawing.Point(8, 52);
            this.tblConfAddHistoryC.Name = "tblConfAddHistoryC";
            this.tblConfAddHistoryC.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow54});
            this.tblConfAddHistoryC.Size = new System.Drawing.Size(700, 25);
            this.tblConfAddHistoryC.StylePriority.UseBackColor = false;
            this.tblConfAddHistoryC.StylePriority.UseBorderColor = false;
            this.tblConfAddHistoryC.StylePriority.UseBorders = false;
            this.tblConfAddHistoryC.StylePriority.UseFont = false;
            // 
            // xrTableRow54
            // 
            this.xrTableRow54.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCAddType,
            this.lblCAddLine1,
            this.lblCAddLine2,
            this.lblCAddLine3,
            this.lblCAddLine4,
            this.lblCdate});
            this.xrTableRow54.Name = "xrTableRow54";
            this.xrTableRow54.Weight = 1;
            // 
            // lblCAddType
            // 
            this.lblCAddType.CanGrow = false;
            this.lblCAddType.Name = "lblCAddType";
            this.lblCAddType.Text = "Type";
            this.lblCAddType.Weight = 0.2857142857142857;
            // 
            // lblCAddLine1
            // 
            this.lblCAddLine1.CanGrow = false;
            this.lblCAddLine1.Name = "lblCAddLine1";
            this.lblCAddLine1.Text = "Line 1";
            this.lblCAddLine1.Weight = 0.96714285714285708;
            // 
            // lblCAddLine2
            // 
            this.lblCAddLine2.CanGrow = false;
            this.lblCAddLine2.Name = "lblCAddLine2";
            this.lblCAddLine2.Text = "Line 2";
            this.lblCAddLine2.Weight = 0.35000000000000003;
            // 
            // lblCAddLine3
            // 
            this.lblCAddLine3.CanGrow = false;
            this.lblCAddLine3.Name = "lblCAddLine3";
            this.lblCAddLine3.Text = "Line 3";
            this.lblCAddLine3.Weight = 0.50428571428571423;
            // 
            // lblCAddLine4
            // 
            this.lblCAddLine4.CanGrow = false;
            this.lblCAddLine4.Name = "lblCAddLine4";
            this.lblCAddLine4.Text = "Line 4";
            this.lblCAddLine4.Weight = 0.46142857142857147;
            // 
            // lblCdate
            // 
            this.lblCdate.CanGrow = false;
            this.lblCdate.Name = "lblCdate";
            this.lblCdate.Text = "Date Confirmed";
            this.lblCdate.Weight = 0.43142857142857144;
            // 
            // tblAddHistoryHeader
            // 
            this.tblAddHistoryHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblAddHistoryHeader.Location = new System.Drawing.Point(8, 8);
            this.tblAddHistoryHeader.Name = "tblAddHistoryHeader";
            this.tblAddHistoryHeader.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow53});
            this.tblAddHistoryHeader.Size = new System.Drawing.Size(375, 25);
            this.tblAddHistoryHeader.StylePriority.UseFont = false;
            // 
            // xrTableRow53
            // 
            this.xrTableRow53.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10});
            this.xrTableRow53.Name = "xrTableRow53";
            this.xrTableRow53.Weight = 1;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StylePriority.UseFont = false;
            this.xrTableCell10.Text = "Address History";
            this.xrTableCell10.Weight = 3;
            // 
            // DetailReport7
            // 
            this.DetailReport7.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail8,
            this.GroupHeader8});
            this.DetailReport7.DataMember = "ConsumerDetailConfirmationContact";
            this.DetailReport7.Level = 7;
            this.DetailReport7.Name = "DetailReport7";
            // 
            // Detail8
            // 
            this.Detail8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.Detail8.Height = 25;
            this.Detail8.Name = "Detail8";
            // 
            // xrTable6
            // 
            this.xrTable6.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.xrTable6.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrTable6.Location = new System.Drawing.Point(8, 0);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow58});
            this.xrTable6.Size = new System.Drawing.Size(700, 25);
            this.xrTable6.StylePriority.UseBorderColor = false;
            this.xrTable6.StylePriority.UseBorders = false;
            this.xrTable6.StylePriority.UseFont = false;
            this.xrTable6.StylePriority.UseForeColor = false;
            // 
            // xrTableRow58
            // 
            this.xrTableRow58.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell16,
            this.xrTableCell23,
            this.xrTableCell24,
            this.xrTableCell25});
            this.xrTableRow58.Name = "xrTableRow58";
            this.xrTableRow58.Weight = 1;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationContact.LandLineNumber", "")});
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Text = "xrTableCell16";
            this.xrTableCell16.Weight = 1;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationContact.LandLineNumberConfirmDate", "")});
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Weight = 0.75571428571428578;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationContact.CellNumber", "")});
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Text = "xrTableCell24";
            this.xrTableCell24.Weight = 0.74428571428571422;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationContact.CellNoConfirmDate", "")});
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Weight = 0.5;
            // 
            // GroupHeader8
            // 
            this.GroupHeader8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5,
            this.xrTable4});
            this.GroupHeader8.Height = 76;
            this.GroupHeader8.Name = "GroupHeader8";
            // 
            // xrTable5
            // 
            this.xrTable5.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.xrTable5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.xrTable5.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.xrTable5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable5.Location = new System.Drawing.Point(8, 51);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow57});
            this.xrTable5.Size = new System.Drawing.Size(700, 25);
            this.xrTable5.StylePriority.UseBackColor = false;
            this.xrTable5.StylePriority.UseBorderColor = false;
            this.xrTable5.StylePriority.UseBorders = false;
            this.xrTable5.StylePriority.UseFont = false;
            // 
            // xrTableRow57
            // 
            this.xrTableRow57.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell12,
            this.xrTableCell13,
            this.xrTableCell14,
            this.xrTableCell15});
            this.xrTableRow57.Name = "xrTableRow57";
            this.xrTableRow57.Weight = 1;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.CanGrow = false;
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Text = "Land Line Number";
            this.xrTableCell12.Weight = 1;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.CanGrow = false;
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Text = "Date Confirmed";
            this.xrTableCell13.Weight = 0.75571428571428578;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.CanGrow = false;
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Text = "Cell Number";
            this.xrTableCell14.Weight = 0.74428571428571422;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.CanGrow = false;
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Text = "Date Confirmed";
            this.xrTableCell15.Weight = 0.5;
            // 
            // xrTable4
            // 
            this.xrTable4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable4.Location = new System.Drawing.Point(8, 8);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow56});
            this.xrTable4.Size = new System.Drawing.Size(375, 25);
            this.xrTable4.StylePriority.UseFont = false;
            // 
            // xrTableRow56
            // 
            this.xrTableRow56.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell11});
            this.xrTableRow56.Name = "xrTableRow56";
            this.xrTableRow56.Weight = 1;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.StylePriority.UseFont = false;
            this.xrTableCell11.Text = "Contact No.History";
            this.xrTableCell11.Weight = 3;
            // 
            // DetailReport8
            // 
            this.DetailReport8.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail9,
            this.GroupHeader9});
            this.DetailReport8.DataMember = "ConsumerDetailConfirmationEmloyment";
            this.DetailReport8.Level = 8;
            this.DetailReport8.Name = "DetailReport8";
            // 
            // Detail9
            // 
            this.Detail9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable9});
            this.Detail9.Height = 26;
            this.Detail9.Name = "Detail9";
            // 
            // xrTable9
            // 
            this.xrTable9.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.xrTable9.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable9.Location = new System.Drawing.Point(8, 0);
            this.xrTable9.Name = "xrTable9";
            this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow61});
            this.xrTable9.Size = new System.Drawing.Size(700, 25);
            this.xrTable9.StylePriority.UseBorderColor = false;
            this.xrTable9.StylePriority.UseBorders = false;
            this.xrTable9.StylePriority.UseFont = false;
            // 
            // xrTableRow61
            // 
            this.xrTableRow61.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell29,
            this.xrTableCell31,
            this.xrTableCell32});
            this.xrTableRow61.Name = "xrTableRow61";
            this.xrTableRow61.Weight = 1;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationEmloyment.EmployerDetail", "")});
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Weight = 1;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationEmloyment.Designation", "")});
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Text = "xrTableCell31";
            this.xrTableCell31.Weight = 1.33;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationEmloyment.DateConfirmed", "")});
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Weight = 0.67;
            // 
            // GroupHeader9
            // 
            this.GroupHeader9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable8,
            this.xrTable7});
            this.GroupHeader9.Height = 71;
            this.GroupHeader9.Name = "GroupHeader9";
            // 
            // xrTable8
            // 
            this.xrTable8.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.xrTable8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.xrTable8.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.xrTable8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable8.Location = new System.Drawing.Point(8, 46);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow60});
            this.xrTable8.Size = new System.Drawing.Size(700, 25);
            this.xrTable8.StylePriority.UseBackColor = false;
            this.xrTable8.StylePriority.UseBorderColor = false;
            this.xrTable8.StylePriority.UseBorders = false;
            this.xrTable8.StylePriority.UseFont = false;
            // 
            // xrTableRow60
            // 
            this.xrTableRow60.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell27,
            this.xrTableCell28,
            this.xrTableCell30});
            this.xrTableRow60.Name = "xrTableRow60";
            this.xrTableRow60.Weight = 1;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.CanGrow = false;
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Text = "Employer";
            this.xrTableCell27.Weight = 1;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.CanGrow = false;
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Text = "Designation";
            this.xrTableCell28.Weight = 1.33;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.CanGrow = false;
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Text = "Date Confirmed";
            this.xrTableCell30.Weight = 0.67;
            // 
            // xrTable7
            // 
            this.xrTable7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable7.Location = new System.Drawing.Point(8, 8);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow59});
            this.xrTable7.Size = new System.Drawing.Size(375, 25);
            this.xrTable7.StylePriority.UseFont = false;
            // 
            // xrTableRow59
            // 
            this.xrTableRow59.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell26});
            this.xrTableRow59.Name = "xrTableRow59";
            this.xrTableRow59.Weight = 1;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.StylePriority.UseFont = false;
            this.xrTableCell26.Text = "Employment History";
            this.xrTableCell26.Weight = 3;
            // 
            // DetailReport9
            // 
            this.DetailReport9.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail10,
            this.GroupHeader10});
            this.DetailReport9.DataMember = "ConsumerNameHistory";
            this.DetailReport9.Level = 9;
            this.DetailReport9.Name = "DetailReport9";
            // 
            // Detail10
            // 
            this.Detail10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable30});
            this.Detail10.Height = 25;
            this.Detail10.Name = "Detail10";
            // 
            // xrTable30
            // 
            this.xrTable30.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable30.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable30.Location = new System.Drawing.Point(8, 0);
            this.xrTable30.Name = "xrTable30";
            this.xrTable30.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow65});
            this.xrTable30.Size = new System.Drawing.Size(800, 25);
            this.xrTable30.StylePriority.UseBorderColor = false;
            this.xrTable30.StylePriority.UseBorders = false;
            // 
            // xrTableRow65
            // 
            this.xrTableRow65.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell203,
            this.xrTableCell200,
            this.xrTableCell191,
            this.xrTableCell204,
            this.xrTableCell201,
            this.xrTableCell192,
            this.xrTableCell205,
            this.xrTableCell202,
            this.xrTableCell193});
            this.xrTableRow65.Name = "xrTableRow65";
            this.xrTableRow65.Weight = 1;
            // 
            // xrTableCell203
            // 
            this.xrTableCell203.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.LastUpdatedDate", "")});
            this.xrTableCell203.Name = "xrTableCell203";
            this.xrTableCell203.Weight = 0.25;
            // 
            // xrTableCell200
            // 
            this.xrTableCell200.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.Surname", "")});
            this.xrTableCell200.Name = "xrTableCell200";
            this.xrTableCell200.Weight = 0.4375;
            // 
            // xrTableCell191
            // 
            this.xrTableCell191.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.FirstName", "")});
            this.xrTableCell191.Name = "xrTableCell191";
            this.xrTableCell191.Weight = 0.47;
            // 
            // xrTableCell204
            // 
            this.xrTableCell204.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.SecondName", "")});
            this.xrTableCell204.Name = "xrTableCell204";
            this.xrTableCell204.Weight = 0.40375;
            // 
            // xrTableCell201
            // 
            this.xrTableCell201.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.Initials", "")});
            this.xrTableCell201.Name = "xrTableCell201";
            this.xrTableCell201.Weight = 0.18250000000000002;
            // 
            // xrTableCell192
            // 
            this.xrTableCell192.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.TitleDesc", "")});
            this.xrTableCell192.Name = "xrTableCell192";
            this.xrTableCell192.Weight = 0.25624999999999992;
            // 
            // xrTableCell205
            // 
            this.xrTableCell205.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.IDNo", "")});
            this.xrTableCell205.Name = "xrTableCell205";
            this.xrTableCell205.Weight = 0.37375;
            // 
            // xrTableCell202
            // 
            this.xrTableCell202.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.PassportNo", "")});
            this.xrTableCell202.Name = "xrTableCell202";
            this.xrTableCell202.Weight = 0.31375;
            // 
            // xrTableCell193
            // 
            this.xrTableCell193.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.BirthDate", "")});
            this.xrTableCell193.Name = "xrTableCell193";
            this.xrTableCell193.Text = "xrTableCell193";
            this.xrTableCell193.Weight = 0.3125;
            // 
            // GroupHeader10
            // 
            this.GroupHeader10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable31,
            this.xrTable11,
            this.xrTable10});
            this.GroupHeader10.Height = 117;
            this.GroupHeader10.Name = "GroupHeader10";
            // 
            // xrTable31
            // 
            this.xrTable31.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable31.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable31.Location = new System.Drawing.Point(8, 92);
            this.xrTable31.Name = "xrTable31";
            this.xrTable31.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow64});
            this.xrTable31.Size = new System.Drawing.Size(800, 25);
            this.xrTable31.StylePriority.UseBorderColor = false;
            this.xrTable31.StylePriority.UseBorders = false;
            // 
            // xrTableRow64
            // 
            this.xrTableRow64.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell206,
            this.xrTableCell207,
            this.xrTableCell208,
            this.xrTableCell209,
            this.xrTableCell210,
            this.xrTableCell211,
            this.xrTableCell212,
            this.xrTableCell213,
            this.xrTableCell214});
            this.xrTableRow64.Name = "xrTableRow64";
            this.xrTableRow64.Weight = 1;
            // 
            // xrTableCell206
            // 
            this.xrTableCell206.Name = "xrTableCell206";
            this.xrTableCell206.Text = "Bureau Update";
            this.xrTableCell206.Weight = 0.25;
            // 
            // xrTableCell207
            // 
            this.xrTableCell207.Name = "xrTableCell207";
            this.xrTableCell207.Text = "Surname";
            this.xrTableCell207.Weight = 0.4375;
            // 
            // xrTableCell208
            // 
            this.xrTableCell208.Name = "xrTableCell208";
            this.xrTableCell208.Text = "First Name";
            this.xrTableCell208.Weight = 0.47;
            // 
            // xrTableCell209
            // 
            this.xrTableCell209.Name = "xrTableCell209";
            this.xrTableCell209.Text = "Second Name";
            this.xrTableCell209.Weight = 0.40375;
            // 
            // xrTableCell210
            // 
            this.xrTableCell210.Name = "xrTableCell210";
            this.xrTableCell210.Text = "Initial";
            this.xrTableCell210.Weight = 0.18250000000000002;
            // 
            // xrTableCell211
            // 
            this.xrTableCell211.Name = "xrTableCell211";
            this.xrTableCell211.Text = "Title";
            this.xrTableCell211.Weight = 0.25624999999999992;
            // 
            // xrTableCell212
            // 
            this.xrTableCell212.Name = "xrTableCell212";
            this.xrTableCell212.Text = "ID No";
            this.xrTableCell212.Weight = 0.37375;
            // 
            // xrTableCell213
            // 
            this.xrTableCell213.Name = "xrTableCell213";
            this.xrTableCell213.Text = "Other ID No";
            this.xrTableCell213.Weight = 0.31375;
            // 
            // xrTableCell214
            // 
            this.xrTableCell214.Name = "xrTableCell214";
            this.xrTableCell214.Text = "Birth Date";
            this.xrTableCell214.Weight = 0.3125;
            // 
            // xrTable11
            // 
            this.xrTable11.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable11.Location = new System.Drawing.Point(8, 50);
            this.xrTable11.Name = "xrTable11";
            this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow63});
            this.xrTable11.Size = new System.Drawing.Size(375, 25);
            this.xrTable11.StylePriority.UseFont = false;
            // 
            // xrTableRow63
            // 
            this.xrTableRow63.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell34});
            this.xrTableRow63.Name = "xrTableRow63";
            this.xrTableRow63.Weight = 1;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.StylePriority.UseFont = false;
            this.xrTableCell34.Text = "Name History";
            this.xrTableCell34.Weight = 3;
            // 
            // xrTable10
            // 
            this.xrTable10.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable10.Location = new System.Drawing.Point(8, 8);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow62});
            this.xrTable10.Size = new System.Drawing.Size(375, 25);
            this.xrTable10.StylePriority.UseFont = false;
            // 
            // xrTableRow62
            // 
            this.xrTableRow62.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell33});
            this.xrTableRow62.Name = "xrTableRow62";
            this.xrTableRow62.Weight = 1;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.StylePriority.UseFont = false;
            this.xrTableCell33.Text = "Consumer Information";
            this.xrTableCell33.Weight = 3;
            // 
            // DetailReport10
            // 
            this.DetailReport10.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail11,
            this.GroupHeader11});
            this.DetailReport10.DataMember = "ConsumerAddressHistory";
            this.DetailReport10.Level = 10;
            this.DetailReport10.Name = "DetailReport10";
            // 
            // Detail11
            // 
            this.Detail11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable33});
            this.Detail11.Height = 27;
            this.Detail11.Name = "Detail11";
            // 
            // xrTable33
            // 
            this.xrTable33.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable33.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable33.Location = new System.Drawing.Point(8, 0);
            this.xrTable33.Name = "xrTable33";
            this.xrTable33.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow68});
            this.xrTable33.Size = new System.Drawing.Size(792, 25);
            this.xrTable33.StylePriority.UseBorderColor = false;
            this.xrTable33.StylePriority.UseBorders = false;
            // 
            // xrTableRow68
            // 
            this.xrTableRow68.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell217,
            this.xrTableCell195,
            this.xrTableCell218,
            this.xrTableCell196,
            this.xrTableCell219,
            this.xrTableCell220,
            this.xrTableCell197});
            this.xrTableRow68.Name = "xrTableRow68";
            this.xrTableRow68.Weight = 1;
            // 
            // xrTableCell217
            // 
            this.xrTableCell217.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.LastUpdatedDate", "")});
            this.xrTableCell217.Name = "xrTableCell217";
            this.xrTableCell217.Text = "xrTableCell217";
            this.xrTableCell217.Weight = 0.28409090909090906;
            // 
            // xrTableCell195
            // 
            this.xrTableCell195.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.AddressType", "")});
            this.xrTableCell195.Name = "xrTableCell195";
            this.xrTableCell195.Weight = 0.38257575757575762;
            // 
            // xrTableCell218
            // 
            this.xrTableCell218.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.Address1", "")});
            this.xrTableCell218.Name = "xrTableCell218";
            this.xrTableCell218.Weight = 0.83333333333333326;
            // 
            // xrTableCell196
            // 
            this.xrTableCell196.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.Address2", "")});
            this.xrTableCell196.Name = "xrTableCell196";
            this.xrTableCell196.Weight = 0.42803030303030304;
            // 
            // xrTableCell219
            // 
            this.xrTableCell219.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.Address3", "")});
            this.xrTableCell219.Name = "xrTableCell219";
            this.xrTableCell219.Weight = 0.44318181818181823;
            // 
            // xrTableCell220
            // 
            this.xrTableCell220.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.Address4", "")});
            this.xrTableCell220.Name = "xrTableCell220";
            this.xrTableCell220.Weight = 0.37878787878787878;
            // 
            // xrTableCell197
            // 
            this.xrTableCell197.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.PostalCode", "")});
            this.xrTableCell197.Name = "xrTableCell197";
            this.xrTableCell197.Weight = 0.25;
            // 
            // GroupHeader11
            // 
            this.GroupHeader11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable32,
            this.xrTable29});
            this.GroupHeader11.Height = 84;
            this.GroupHeader11.Name = "GroupHeader11";
            // 
            // xrTable32
            // 
            this.xrTable32.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.xrTable32.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable32.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable32.Location = new System.Drawing.Point(8, 50);
            this.xrTable32.Name = "xrTable32";
            this.xrTable32.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow67});
            this.xrTable32.Size = new System.Drawing.Size(792, 34);
            this.xrTable32.StylePriority.UseBorderColor = false;
            this.xrTable32.StylePriority.UseBorders = false;
            // 
            // xrTableRow67
            // 
            this.xrTableRow67.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell198,
            this.xrTableCell189,
            this.xrTableCell199,
            this.xrTableCell190,
            this.xrTableCell215,
            this.xrTableCell216,
            this.xrTableCell194});
            this.xrTableRow67.Name = "xrTableRow67";
            this.xrTableRow67.Weight = 1;
            // 
            // xrTableCell198
            // 
            this.xrTableCell198.CanGrow = false;
            this.xrTableCell198.Name = "xrTableCell198";
            this.xrTableCell198.Text = "Bureau Update";
            this.xrTableCell198.Weight = 0.28409090909090906;
            // 
            // xrTableCell189
            // 
            this.xrTableCell189.CanGrow = false;
            this.xrTableCell189.Name = "xrTableCell189";
            this.xrTableCell189.Text = "Type";
            this.xrTableCell189.Weight = 0.38257575757575757;
            // 
            // xrTableCell199
            // 
            this.xrTableCell199.CanGrow = false;
            this.xrTableCell199.Name = "xrTableCell199";
            this.xrTableCell199.Text = "Line1";
            this.xrTableCell199.Weight = 0.83333333333333326;
            // 
            // xrTableCell190
            // 
            this.xrTableCell190.CanGrow = false;
            this.xrTableCell190.Name = "xrTableCell190";
            this.xrTableCell190.Text = "Line2";
            this.xrTableCell190.Weight = 0.42803030303030304;
            // 
            // xrTableCell215
            // 
            this.xrTableCell215.CanGrow = false;
            this.xrTableCell215.Name = "xrTableCell215";
            this.xrTableCell215.Text = "Line 3";
            this.xrTableCell215.Weight = 0.44318181818181823;
            // 
            // xrTableCell216
            // 
            this.xrTableCell216.CanGrow = false;
            this.xrTableCell216.Name = "xrTableCell216";
            this.xrTableCell216.Text = "Line 4";
            this.xrTableCell216.Weight = 0.37878787878787878;
            // 
            // xrTableCell194
            // 
            this.xrTableCell194.CanGrow = false;
            this.xrTableCell194.Name = "xrTableCell194";
            this.xrTableCell194.Text = "Postal Code";
            this.xrTableCell194.Weight = 0.25;
            // 
            // xrTable29
            // 
            this.xrTable29.ForeColor = System.Drawing.Color.Black;
            this.xrTable29.Location = new System.Drawing.Point(8, 17);
            this.xrTable29.Name = "xrTable29";
            this.xrTable29.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow66});
            this.xrTable29.Size = new System.Drawing.Size(433, 25);
            this.xrTable29.StylePriority.UseForeColor = false;
            // 
            // xrTableRow66
            // 
            this.xrTableRow66.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell188});
            this.xrTableRow66.Name = "xrTableRow66";
            this.xrTableRow66.Weight = 1;
            // 
            // xrTableCell188
            // 
            this.xrTableCell188.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell188.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell188.Name = "xrTableCell188";
            this.xrTableCell188.StylePriority.UseFont = false;
            this.xrTableCell188.StylePriority.UseForeColor = false;
            this.xrTableCell188.Text = "Address History";
            this.xrTableCell188.Weight = 3;
            // 
            // DetailReport11
            // 
            this.DetailReport11.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail12,
            this.GroupHeader12});
            this.DetailReport11.DataMember = "ConsumerTelephoneHistory";
            this.DetailReport11.Level = 11;
            this.DetailReport11.Name = "DetailReport11";
            // 
            // Detail12
            // 
            this.Detail12.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable36});
            this.Detail12.Height = 25;
            this.Detail12.Name = "Detail12";
            // 
            // xrTable36
            // 
            this.xrTable36.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable36.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable36.Location = new System.Drawing.Point(8, 0);
            this.xrTable36.Name = "xrTable36";
            this.xrTable36.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow71});
            this.xrTable36.Size = new System.Drawing.Size(792, 25);
            this.xrTable36.StylePriority.UseBorderColor = false;
            this.xrTable36.StylePriority.UseBorders = false;
            // 
            // xrTableRow71
            // 
            this.xrTableRow71.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell225,
            this.xrTableCell227,
            this.xrTableCell224,
            this.xrTableCell226});
            this.xrTableRow71.Name = "xrTableRow71";
            this.xrTableRow71.Weight = 1;
            // 
            // xrTableCell225
            // 
            this.xrTableCell225.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneHistory.LastUpdatedDate", "")});
            this.xrTableCell225.Name = "xrTableCell225";
            this.xrTableCell225.Weight = 0.34848484848484851;
            // 
            // xrTableCell227
            // 
            this.xrTableCell227.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneHistory.TelephoneType", "")});
            this.xrTableCell227.Name = "xrTableCell227";
            this.xrTableCell227.Text = "xrTableCell227";
            this.xrTableCell227.Weight = 0.21969696969696967;
            // 
            // xrTableCell224
            // 
            this.xrTableCell224.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneHistory.TelephoneNo", "")});
            this.xrTableCell224.Name = "xrTableCell224";
            this.xrTableCell224.Weight = 0.50378787878787867;
            // 
            // xrTableCell226
            // 
            this.xrTableCell226.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneHistory.EmailAddress", "")});
            this.xrTableCell226.Name = "xrTableCell226";
            this.xrTableCell226.Weight = 1.928030303030303;
            // 
            // GroupHeader12
            // 
            this.GroupHeader12.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable35,
            this.xrTable34});
            this.GroupHeader12.Height = 79;
            this.GroupHeader12.Name = "GroupHeader12";
            // 
            // xrTable35
            // 
            this.xrTable35.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.xrTable35.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable35.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable35.Location = new System.Drawing.Point(8, 54);
            this.xrTable35.Name = "xrTable35";
            this.xrTable35.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow70});
            this.xrTable35.Size = new System.Drawing.Size(792, 25);
            this.xrTable35.StylePriority.UseBorderColor = false;
            this.xrTable35.StylePriority.UseBorders = false;
            // 
            // xrTableRow70
            // 
            this.xrTableRow70.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell228,
            this.xrTableCell222,
            this.xrTableCell229,
            this.xrTableCell223});
            this.xrTableRow70.Name = "xrTableRow70";
            this.xrTableRow70.Weight = 1;
            // 
            // xrTableCell228
            // 
            this.xrTableCell228.CanGrow = false;
            this.xrTableCell228.Name = "xrTableCell228";
            this.xrTableCell228.Text = "Bureau Update";
            this.xrTableCell228.Weight = 0.34848484848484851;
            // 
            // xrTableCell222
            // 
            this.xrTableCell222.CanGrow = false;
            this.xrTableCell222.Name = "xrTableCell222";
            this.xrTableCell222.Text = "Type";
            this.xrTableCell222.Weight = 0.21969696969696967;
            // 
            // xrTableCell229
            // 
            this.xrTableCell229.CanGrow = false;
            this.xrTableCell229.Name = "xrTableCell229";
            this.xrTableCell229.Text = "Telephone No";
            this.xrTableCell229.Weight = 0.5037878787878789;
            // 
            // xrTableCell223
            // 
            this.xrTableCell223.CanGrow = false;
            this.xrTableCell223.Name = "xrTableCell223";
            this.xrTableCell223.Text = "Email Address";
            this.xrTableCell223.Weight = 1.928030303030303;
            // 
            // xrTable34
            // 
            this.xrTable34.ForeColor = System.Drawing.Color.Black;
            this.xrTable34.Location = new System.Drawing.Point(8, 17);
            this.xrTable34.Name = "xrTable34";
            this.xrTable34.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow69});
            this.xrTable34.Size = new System.Drawing.Size(433, 25);
            this.xrTable34.StylePriority.UseForeColor = false;
            // 
            // xrTableRow69
            // 
            this.xrTableRow69.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell221});
            this.xrTableRow69.Name = "xrTableRow69";
            this.xrTableRow69.Weight = 1;
            // 
            // xrTableCell221
            // 
            this.xrTableCell221.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell221.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell221.Name = "xrTableCell221";
            this.xrTableCell221.StylePriority.UseFont = false;
            this.xrTableCell221.StylePriority.UseForeColor = false;
            this.xrTableCell221.Text = "Contact No.History";
            this.xrTableCell221.Weight = 3;
            // 
            // DetailReport12
            // 
            this.DetailReport12.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail13,
            this.GroupHeader13});
            this.DetailReport12.DataMember = "ConsumerEmploymentHistory";
            this.DetailReport12.Level = 12;
            this.DetailReport12.Name = "DetailReport12";
            // 
            // Detail13
            // 
            this.Detail13.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable39});
            this.Detail13.Height = 25;
            this.Detail13.Name = "Detail13";
            // 
            // xrTable39
            // 
            this.xrTable39.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable39.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable39.Location = new System.Drawing.Point(8, 0);
            this.xrTable39.Name = "xrTable39";
            this.xrTable39.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow74});
            this.xrTable39.Size = new System.Drawing.Size(792, 25);
            this.xrTable39.StylePriority.UseBorderColor = false;
            this.xrTable39.StylePriority.UseBorders = false;
            // 
            // xrTableRow74
            // 
            this.xrTableRow74.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell234,
            this.xrTableCell235,
            this.xrTableCell236});
            this.xrTableRow74.Name = "xrTableRow74";
            this.xrTableRow74.Weight = 1;
            // 
            // xrTableCell234
            // 
            this.xrTableCell234.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerEmploymentHistory.LastUpdatedDate", "")});
            this.xrTableCell234.Name = "xrTableCell234";
            this.xrTableCell234.Weight = 0.34848484848484851;
            // 
            // xrTableCell235
            // 
            this.xrTableCell235.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerEmploymentHistory.EmployerDetail", "")});
            this.xrTableCell235.Name = "xrTableCell235";
            this.xrTableCell235.Weight = 1.4204545454545454;
            // 
            // xrTableCell236
            // 
            this.xrTableCell236.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerEmploymentHistory.Designation", "")});
            this.xrTableCell236.Name = "xrTableCell236";
            this.xrTableCell236.Weight = 1.231060606060606;
            // 
            // GroupHeader13
            // 
            this.GroupHeader13.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable38,
            this.xrTable37});
            this.GroupHeader13.Height = 79;
            this.GroupHeader13.Name = "GroupHeader13";
            // 
            // xrTable38
            // 
            this.xrTable38.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.xrTable38.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable38.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable38.Location = new System.Drawing.Point(8, 54);
            this.xrTable38.Name = "xrTable38";
            this.xrTable38.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow73});
            this.xrTable38.Size = new System.Drawing.Size(792, 25);
            this.xrTable38.StylePriority.UseBorderColor = false;
            this.xrTable38.StylePriority.UseBorders = false;
            // 
            // xrTableRow73
            // 
            this.xrTableRow73.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell231,
            this.xrTableCell232,
            this.xrTableCell233});
            this.xrTableRow73.Name = "xrTableRow73";
            this.xrTableRow73.Weight = 1;
            // 
            // xrTableCell231
            // 
            this.xrTableCell231.CanGrow = false;
            this.xrTableCell231.Name = "xrTableCell231";
            this.xrTableCell231.Text = "Bureau Update";
            this.xrTableCell231.Weight = 0.34848484848484851;
            // 
            // xrTableCell232
            // 
            this.xrTableCell232.CanGrow = false;
            this.xrTableCell232.Name = "xrTableCell232";
            this.xrTableCell232.Text = "Employer";
            this.xrTableCell232.Weight = 1.4204545454545454;
            // 
            // xrTableCell233
            // 
            this.xrTableCell233.CanGrow = false;
            this.xrTableCell233.Name = "xrTableCell233";
            this.xrTableCell233.Text = "Designation";
            this.xrTableCell233.Weight = 1.231060606060606;
            // 
            // xrTable37
            // 
            this.xrTable37.ForeColor = System.Drawing.Color.Black;
            this.xrTable37.Location = new System.Drawing.Point(8, 17);
            this.xrTable37.Name = "xrTable37";
            this.xrTable37.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow72});
            this.xrTable37.Size = new System.Drawing.Size(433, 25);
            this.xrTable37.StylePriority.UseForeColor = false;
            // 
            // xrTableRow72
            // 
            this.xrTableRow72.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell230});
            this.xrTableRow72.Name = "xrTableRow72";
            this.xrTableRow72.Weight = 1;
            // 
            // xrTableCell230
            // 
            this.xrTableCell230.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell230.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell230.Name = "xrTableCell230";
            this.xrTableCell230.StylePriority.UseFont = false;
            this.xrTableCell230.StylePriority.UseForeColor = false;
            this.xrTableCell230.Text = "Employment History";
            this.xrTableCell230.Weight = 3;
            // 
            // DetailReport13
            // 
            this.DetailReport13.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail14,
            this.GroupHeader14});
            this.DetailReport13.DataMember = "CounsumerAccountStatus";
            this.DetailReport13.Level = 13;
            this.DetailReport13.Name = "DetailReport13";
            // 
            // Detail14
            // 
            this.Detail14.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable14});
            this.Detail14.Height = 25;
            this.Detail14.Name = "Detail14";
            // 
            // xrTable14
            // 
            this.xrTable14.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable14.Location = new System.Drawing.Point(8, 0);
            this.xrTable14.Name = "xrTable14";
            this.xrTable14.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow76});
            this.xrTable14.Size = new System.Drawing.Size(650, 25);
            this.xrTable14.StylePriority.UseBorderColor = false;
            // 
            // xrTableRow76
            // 
            this.xrTableRow76.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell45,
            this.xrTableCell36,
            this.xrTableCell46,
            this.xrTableCell44,
            this.xrTableCell47,
            this.xrTableCell48,
            this.xrTableCell49});
            this.xrTableRow76.Name = "xrTableRow76";
            this.xrTableRow76.StylePriority.UseBorders = false;
            this.xrTableRow76.Weight = 1;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell45.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CounsumerAccountStatus.AccountOpenedDate", "")});
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.StylePriority.UseBorders = false;
            this.xrTableCell45.Text = "xrTableCell45";
            this.xrTableCell45.Weight = 0.38461538461538458;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell36.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CounsumerAccountStatus.SubscriberName", "")});
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.StylePriority.UseBorders = false;
            this.xrTableCell36.Text = "xrTableCell15";
            this.xrTableCell36.Weight = 0.73076923076923084;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell46.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CounsumerAccountStatus.CreditLimitAmt", "")});
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.StylePriority.UseBorders = false;
            this.xrTableCell46.Weight = 0.42615384615384611;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell44.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CounsumerAccountStatus.CurrentBalanceAmt", "")});
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.StylePriority.UseBorders = false;
            this.xrTableCell44.Weight = 0.38461538461538464;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell47.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CounsumerAccountStatus.MonthlyInstalmentAmt", "")});
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.StylePriority.UseBorders = false;
            this.xrTableCell47.Weight = 0.34769230769230763;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell48.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CounsumerAccountStatus.ArrearsAmt", "")});
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.StylePriority.UseBorders = false;
            this.xrTableCell48.Weight = 0.34692307692307689;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell49.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CounsumerAccountStatus.AccountType", "")});
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.StylePriority.UseBorders = false;
            this.xrTableCell49.Weight = 0.37923076923076926;
            // 
            // GroupHeader14
            // 
            this.GroupHeader14.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable13,
            this.xrTable12});
            this.GroupHeader14.Height = 68;
            this.GroupHeader14.Name = "GroupHeader14";
            // 
            // xrTable13
            // 
            this.xrTable13.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable13.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable13.Location = new System.Drawing.Point(8, 42);
            this.xrTable13.Name = "xrTable13";
            this.xrTable13.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow77});
            this.xrTable13.Size = new System.Drawing.Size(650, 25);
            this.xrTable13.StylePriority.UseBorderColor = false;
            this.xrTable13.StylePriority.UseBorders = false;
            // 
            // xrTableRow77
            // 
            this.xrTableRow77.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell37,
            this.xrTableCell38,
            this.xrTableCell41,
            this.xrTableCell39,
            this.xrTableCell42,
            this.xrTableCell43,
            this.xrTableCell40});
            this.xrTableRow77.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableRow77.Name = "xrTableRow77";
            this.xrTableRow77.StylePriority.UseFont = false;
            this.xrTableRow77.Weight = 1;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Text = "Date Account Opened";
            this.xrTableCell37.Weight = 0.38461538461538458;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Text = "Company";
            this.xrTableCell38.Weight = 0.73076923076923084;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Text = "Account Credit Limit";
            this.xrTableCell41.Weight = 0.42615384615384616;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Text = "Current Balance";
            this.xrTableCell39.Weight = 0.38461538461538458;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Text = "Installment Amount";
            this.xrTableCell42.Weight = 0.34769230769230763;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Text = "Arrears Amount";
            this.xrTableCell43.Weight = 0.34692307692307689;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Text = "Type of Account";
            this.xrTableCell40.Weight = 0.37923076923076926;
            // 
            // xrTable12
            // 
            this.xrTable12.ForeColor = System.Drawing.Color.Black;
            this.xrTable12.Location = new System.Drawing.Point(8, 8);
            this.xrTable12.Name = "xrTable12";
            this.xrTable12.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow75});
            this.xrTable12.Size = new System.Drawing.Size(433, 25);
            this.xrTable12.StylePriority.UseForeColor = false;
            // 
            // xrTableRow75
            // 
            this.xrTableRow75.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell35});
            this.xrTableRow75.Name = "xrTableRow75";
            this.xrTableRow75.Weight = 1;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell35.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.StylePriority.UseFont = false;
            this.xrTableCell35.StylePriority.UseForeColor = false;
            this.xrTableCell35.Text = "Credit Account Status";
            this.xrTableCell35.Weight = 3;
            // 
            // DetailReport14
            // 
            this.DetailReport14.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail15,
            this.GroupHeader15});
            this.DetailReport14.DataMember = "Consumer24MonthlyPaymentHeader";
            this.DetailReport14.Level = 14;
            this.DetailReport14.Name = "DetailReport14";
            // 
            // Detail15
            // 
            this.Detail15.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable16});
            this.Detail15.Height = 25;
            this.Detail15.Name = "Detail15";
            // 
            // xrTable16
            // 
            this.xrTable16.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable16.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable16.Location = new System.Drawing.Point(8, 0);
            this.xrTable16.Name = "xrTable16";
            this.xrTable16.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow79});
            this.xrTable16.Size = new System.Drawing.Size(792, 25);
            this.xrTable16.StylePriority.UseBorderColor = false;
            this.xrTable16.StylePriority.UseBorders = false;
            this.xrTable16.StylePriority.UseTextAlignment = false;
            this.xrTable16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow79
            // 
            this.xrTableRow79.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell75,
            this.xrTableCell98,
            this.xrTableCell76,
            this.xrTableCell77,
            this.xrTableCell78,
            this.xrTableCell79,
            this.xrTableCell80,
            this.xrTableCell81,
            this.xrTableCell82,
            this.xrTableCell83,
            this.xrTableCell84,
            this.xrTableCell85,
            this.xrTableCell86,
            this.xrTableCell87,
            this.xrTableCell88,
            this.xrTableCell89,
            this.xrTableCell90,
            this.xrTableCell91,
            this.xrTableCell92,
            this.xrTableCell93,
            this.xrTableCell94,
            this.xrTableCell95,
            this.xrTableCell96,
            this.xrTableCell97,
            this.xrTableCell99});
            this.xrTableRow79.Name = "xrTableRow79";
            this.xrTableRow79.Weight = 1;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.SubscriberName", "")});
            this.xrTableCell75.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.StylePriority.UseFont = false;
            this.xrTableCell75.StylePriority.UseTextAlignment = false;
            this.xrTableCell75.Text = "xrTableCell49";
            this.xrTableCell75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell75.Weight = 0.45036567069356459;
            // 
            // xrTableCell98
            // 
            this.xrTableCell98.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M01", "")});
            this.xrTableCell98.Name = "xrTableCell98";
            this.xrTableCell98.Weight = 0.18908346833704748;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M02", "")});
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.Weight = 0.18935347576530615;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M03", "")});
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.Weight = 0.18546715561224492;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M04", "")});
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Weight = 0.1869021045918369;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M05", "")});
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.Text = "xrTableCell78";
            this.xrTableCell79.Weight = 0.18546715561224489;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M06", "")});
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.Weight = 0.18391262755102042;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M07", "")});
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.Weight = 0.18391262755102053;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M08", "")});
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.Weight = 0.18558673469387751;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M09", "")});
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.Weight = 0.18415178571428584;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M10", "")});
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.Weight = 0.18534757653061221;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M11", "")});
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.Weight = 0.18534757653061221;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M12", "")});
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.Weight = 0.18534757653061229;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M13", "")});
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.Weight = 0.18534757653061232;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M14", "")});
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.Weight = 0.18534757653061224;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M15", "")});
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.Weight = 0.18582589285714291;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M16", "")});
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.Weight = 0.18343431122448989;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M17", "")});
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.Weight = 0.18917410714285726;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M18", "")});
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.Weight = 0.1889349489795919;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M19", "")});
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.Weight = 0.18606505102040827;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M20", "")});
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.Weight = 0.18893494897959184;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M21", "")});
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.Weight = 0.18749999999999992;
            // 
            // xrTableCell96
            // 
            this.xrTableCell96.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M22", "")});
            this.xrTableCell96.Name = "xrTableCell96";
            this.xrTableCell96.Weight = 0.18749999999999994;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M23", "")});
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.Weight = 0.1879783163265307;
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell99.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M24", "")});
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.StylePriority.UseBorders = false;
            this.xrTableCell99.Weight = 0.18845663265306129;
            // 
            // GroupHeader15
            // 
            this.GroupHeader15.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable15,
            this.xrPageInfo3});
            this.GroupHeader15.Height = 52;
            this.GroupHeader15.Name = "GroupHeader15";
            // 
            // xrTable15
            // 
            this.xrTable15.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable15.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable15.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTable15.Location = new System.Drawing.Point(8, 25);
            this.xrTable15.Name = "xrTable15";
            this.xrTable15.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow78});
            this.xrTable15.Size = new System.Drawing.Size(792, 25);
            this.xrTable15.StylePriority.UseBorderColor = false;
            this.xrTable15.StylePriority.UseBorders = false;
            this.xrTable15.StylePriority.UseFont = false;
            this.xrTable15.StylePriority.UseTextAlignment = false;
            this.xrTable15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableRow78
            // 
            this.xrTableRow78.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell51,
            this.xrTableCell72,
            this.xrTableCell67,
            this.xrTableCell71,
            this.xrTableCell65,
            this.xrTableCell70,
            this.xrTableCell64,
            this.xrTableCell52,
            this.xrTableCell68,
            this.xrTableCell63,
            this.xrTableCell69,
            this.xrTableCell54,
            this.xrTableCell73,
            this.xrTableCell62,
            this.xrTableCell74,
            this.xrTableCell50,
            this.xrTableCell66,
            this.xrTableCell57,
            this.xrTableCell61,
            this.xrTableCell56,
            this.xrTableCell59,
            this.xrTableCell58,
            this.xrTableCell60,
            this.xrTableCell55,
            this.xrTableCell53});
            this.xrTableRow78.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableRow78.Name = "xrTableRow78";
            this.xrTableRow78.StylePriority.UseBorders = false;
            this.xrTableRow78.StylePriority.UseFont = false;
            this.xrTableRow78.Weight = 1;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.StylePriority.UseFont = false;
            this.xrTableCell51.Text = "Company";
            this.xrTableCell51.Weight = 0.44387755102040793;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M01", "")});
            this.xrTableCell72.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.StylePriority.UseFont = false;
            this.xrTableCell72.Text = "xrTableCell72";
            this.xrTableCell72.Weight = 0.18601171846934769;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M02", "")});
            this.xrTableCell67.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.StylePriority.UseFont = false;
            this.xrTableCell67.Weight = 0.18601171846934772;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M03", "")});
            this.xrTableCell71.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.StylePriority.UseFont = false;
            this.xrTableCell71.Weight = 0.18601171846934769;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M04", "")});
            this.xrTableCell65.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.StylePriority.UseFont = false;
            this.xrTableCell65.Weight = 0.18594890203530276;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M05", "")});
            this.xrTableCell70.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.StylePriority.UseFont = false;
            this.xrTableCell70.Weight = 0.18692207722491891;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M06", "")});
            this.xrTableCell64.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.StylePriority.UseFont = false;
            this.xrTableCell64.Text = "xrTableCell64";
            this.xrTableCell64.Weight = 0.18480901023986329;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M07", "")});
            this.xrTableCell52.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.StylePriority.UseFont = false;
            this.xrTableCell52.Text = "xrTableCell52";
            this.xrTableCell52.Weight = 0.18282615846050218;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M08", "")});
            this.xrTableCell68.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.StylePriority.UseFont = false;
            this.xrTableCell68.Weight = 0.18692207722491891;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M09", "")});
            this.xrTableCell63.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.StylePriority.UseFont = false;
            this.xrTableCell63.Weight = 0.18692207722491885;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M10", "")});
            this.xrTableCell69.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.StylePriority.UseFont = false;
            this.xrTableCell69.Weight = 0.18692207722491883;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M11", "")});
            this.xrTableCell54.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.StylePriority.UseFont = false;
            this.xrTableCell54.Weight = 0.18692207722491888;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M12", "")});
            this.xrTableCell73.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.StylePriority.UseFont = false;
            this.xrTableCell73.Weight = 0.18692207722491883;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M13", "")});
            this.xrTableCell62.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.StylePriority.UseFont = false;
            this.xrTableCell62.Weight = 0.18692207722491888;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M14", "")});
            this.xrTableCell74.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.StylePriority.UseFont = false;
            this.xrTableCell74.Weight = 0.18692207722491888;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M15", "")});
            this.xrTableCell50.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.StylePriority.UseFont = false;
            this.xrTableCell50.Weight = 0.18692207722491866;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M16", "")});
            this.xrTableCell66.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.StylePriority.UseFont = false;
            this.xrTableCell66.Weight = 0.18861484967550241;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M17", "")});
            this.xrTableCell57.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.StylePriority.UseFont = false;
            this.xrTableCell57.Weight = 0.18586923151767038;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M18", "")});
            this.xrTableCell61.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.StylePriority.UseFont = false;
            this.xrTableCell61.Weight = 0.18888485710376113;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M19", "")});
            this.xrTableCell56.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.StylePriority.UseFont = false;
            this.xrTableCell56.Weight = 0.18793983110485579;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M20", "")});
            this.xrTableCell59.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.StylePriority.UseFont = false;
            this.xrTableCell59.Weight = 0.18807483481898493;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M21", "")});
            this.xrTableCell58.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.StylePriority.UseFont = false;
            this.xrTableCell58.Weight = 0.18888485710376105;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M22", "")});
            this.xrTableCell60.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.StylePriority.UseFont = false;
            this.xrTableCell60.Weight = 0.18681425751657579;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M23", "")});
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.StylePriority.UseFont = false;
            this.xrTableCell55.Weight = 0.18871569070953312;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell53.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M24", "")});
            this.xrTableCell53.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.StylePriority.UseBorders = false;
            this.xrTableCell53.StylePriority.UseFont = false;
            this.xrTableCell53.Weight = 0.16381838531246676;
            // 
            // xrPageInfo3
            // 
            this.xrPageInfo3.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrPageInfo3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrPageInfo3.Format = "Monthly Payment Behaviour as at {0:dd/MM/yyyy}";
            this.xrPageInfo3.Location = new System.Drawing.Point(8, 0);
            this.xrPageInfo3.Name = "xrPageInfo3";
            this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo3.Size = new System.Drawing.Size(792, 25);
            this.xrPageInfo3.StylePriority.UseBorderColor = false;
            this.xrPageInfo3.StylePriority.UseBorders = false;
            this.xrPageInfo3.StylePriority.UseTextAlignment = false;
            this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DetailReport15
            // 
            this.DetailReport15.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail16,
            this.GroupHeader16,
            this.GroupFooter1});
            this.DetailReport15.DataMember = "Definition";
            this.DetailReport15.Level = 15;
            this.DetailReport15.Name = "DetailReport15";
            // 
            // Detail16
            // 
            this.Detail16.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable18});
            this.Detail16.Height = 25;
            this.Detail16.Name = "Detail16";
            // 
            // xrTable18
            // 
            this.xrTable18.Location = new System.Drawing.Point(8, 0);
            this.xrTable18.Name = "xrTable18";
            this.xrTable18.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow81});
            this.xrTable18.Size = new System.Drawing.Size(792, 25);
            this.xrTable18.StylePriority.UseBorders = false;
            // 
            // xrTableRow81
            // 
            this.xrTableRow81.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell102,
            this.xrTableCell103});
            this.xrTableRow81.Name = "xrTableRow81";
            this.xrTableRow81.Weight = 1;
            // 
            // xrTableCell102
            // 
            this.xrTableCell102.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTableCell102.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell102.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Definition.DefinitionDesc", "")});
            this.xrTableCell102.Name = "xrTableCell102";
            this.xrTableCell102.StylePriority.UseBorderColor = false;
            this.xrTableCell102.StylePriority.UseBorders = false;
            this.xrTableCell102.Text = "xrTableCell102";
            this.xrTableCell102.Weight = 2.2113564668769716;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTableCell103.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell103.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Definition.DefinitionCode", "")});
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.StylePriority.UseBorderColor = false;
            this.xrTableCell103.StylePriority.UseBorders = false;
            this.xrTableCell103.Weight = 5.2933753943217665;
            // 
            // GroupHeader16
            // 
            this.GroupHeader16.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable17});
            this.GroupHeader16.Height = 25;
            this.GroupHeader16.Name = "GroupHeader16";
            // 
            // xrTable17
            // 
            this.xrTable17.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.xrTable17.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable17.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable17.Location = new System.Drawing.Point(8, 0);
            this.xrTable17.Name = "xrTable17";
            this.xrTable17.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow80});
            this.xrTable17.Size = new System.Drawing.Size(792, 25);
            this.xrTable17.StylePriority.UseBorderColor = false;
            this.xrTable17.StylePriority.UseBorders = false;
            // 
            // xrTableRow80
            // 
            this.xrTableRow80.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell100,
            this.xrTableCell101});
            this.xrTableRow80.Name = "xrTableRow80";
            this.xrTableRow80.Weight = 1;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell100.CanGrow = false;
            this.xrTableCell100.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Definition.DisplayText", "")});
            this.xrTableCell100.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Underline);
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.StylePriority.UseBorders = false;
            this.xrTableCell100.StylePriority.UseFont = false;
            this.xrTableCell100.Text = "xrTableCell99";
            this.xrTableCell100.Weight = 2.2113564668769716;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell101.CanGrow = false;
            this.xrTableCell101.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Underline);
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.StylePriority.UseBorders = false;
            this.xrTableCell101.StylePriority.UseFont = false;
            this.xrTableCell101.Text = "Indicators";
            this.xrTableCell101.Weight = 5.2933753943217665;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine2});
            this.GroupFooter1.Height = 4;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrLine2
            // 
            this.xrLine2.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.xrLine2.Location = new System.Drawing.Point(8, 0);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.Size = new System.Drawing.Size(792, 2);
            this.xrLine2.StylePriority.UseForeColor = false;
            // 
            // DetailReport16
            // 
            this.DetailReport16.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail17,
            this.GroupHeader17});
            this.DetailReport16.DataMember = "ConsumerAdverseInfo";
            this.DetailReport16.Level = 16;
            this.DetailReport16.Name = "DetailReport16";
            // 
            // Detail17
            // 
            this.Detail17.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable22});
            this.Detail17.Height = 25;
            this.Detail17.Name = "Detail17";
            // 
            // xrTable22
            // 
            this.xrTable22.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable22.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable22.Location = new System.Drawing.Point(8, 0);
            this.xrTable22.Name = "xrTable22";
            this.xrTable22.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow85});
            this.xrTable22.Size = new System.Drawing.Size(800, 25);
            this.xrTable22.StylePriority.UseBorderColor = false;
            this.xrTable22.StylePriority.UseBorders = false;
            // 
            // xrTableRow85
            // 
            this.xrTableRow85.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell112,
            this.xrTableCell115,
            this.xrTableCell109,
            this.xrTableCell113,
            this.xrTableCell116,
            this.xrTableCell117});
            this.xrTableRow85.Name = "xrTableRow85";
            this.xrTableRow85.Weight = 1;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdverseInfo.SubscriberName", "")});
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.Text = "xrTableCell112";
            this.xrTableCell112.Weight = 0.725;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdverseInfo.AccountNo", "")});
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.Text = "xrTableCell115";
            this.xrTableCell115.Weight = 0.46374999999999994;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdverseInfo.ActionDate", "")});
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.Text = "xrTableCell109";
            this.xrTableCell109.Weight = 0.31375;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdverseInfo.CurrentBalanceAmt", "")});
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.Text = "xrTableCell113";
            this.xrTableCell113.Weight = 0.3425;
            // 
            // xrTableCell116
            // 
            this.xrTableCell116.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdverseInfo.DataStatus", "")});
            this.xrTableCell116.Name = "xrTableCell116";
            this.xrTableCell116.Text = "xrTableCell116";
            this.xrTableCell116.Weight = 0.41375;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdverseInfo.Comments", "")});
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.Weight = 0.74125;
            // 
            // GroupHeader17
            // 
            this.GroupHeader17.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable21,
            this.xrTable20,
            this.xrTable19});
            this.GroupHeader17.Height = 108;
            this.GroupHeader17.Name = "GroupHeader17";
            // 
            // xrTable21
            // 
            this.xrTable21.ForeColor = System.Drawing.Color.Black;
            this.xrTable21.Location = new System.Drawing.Point(8, 50);
            this.xrTable21.Name = "xrTable21";
            this.xrTable21.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow84});
            this.xrTable21.Size = new System.Drawing.Size(433, 25);
            this.xrTable21.StylePriority.UseForeColor = false;
            // 
            // xrTableRow84
            // 
            this.xrTableRow84.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell108});
            this.xrTableRow84.Name = "xrTableRow84";
            this.xrTableRow84.Weight = 1;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell108.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.StylePriority.UseFont = false;
            this.xrTableCell108.StylePriority.UseForeColor = false;
            this.xrTableCell108.Text = "Adverse Defaults";
            this.xrTableCell108.Weight = 3;
            // 
            // xrTable20
            // 
            this.xrTable20.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.xrTable20.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable20.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable20.Location = new System.Drawing.Point(8, 83);
            this.xrTable20.Name = "xrTable20";
            this.xrTable20.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow83});
            this.xrTable20.Size = new System.Drawing.Size(800, 25);
            this.xrTable20.StylePriority.UseBorderColor = false;
            this.xrTable20.StylePriority.UseBorders = false;
            // 
            // xrTableRow83
            // 
            this.xrTableRow83.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell110,
            this.xrTableCell105,
            this.xrTableCell114,
            this.xrTableCell111,
            this.xrTableCell106,
            this.xrTableCell107});
            this.xrTableRow83.Name = "xrTableRow83";
            this.xrTableRow83.Weight = 1;
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.CanGrow = false;
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.Text = "Subscriber";
            this.xrTableCell110.Weight = 0.725;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.CanGrow = false;
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.Text = "Account No.";
            this.xrTableCell105.Weight = 0.46625;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.CanGrow = false;
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.Text = "Action date";
            this.xrTableCell114.Weight = 0.31375;
            // 
            // xrTableCell111
            // 
            this.xrTableCell111.CanGrow = false;
            this.xrTableCell111.Name = "xrTableCell111";
            this.xrTableCell111.Text = "Amount";
            this.xrTableCell111.Weight = 0.33999999999999997;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.CanGrow = false;
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.Text = "Account Status";
            this.xrTableCell106.Weight = 0.41375;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.CanGrow = false;
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.Text = "Comment";
            this.xrTableCell107.Weight = 0.74125;
            // 
            // xrTable19
            // 
            this.xrTable19.ForeColor = System.Drawing.Color.Black;
            this.xrTable19.Location = new System.Drawing.Point(8, 17);
            this.xrTable19.Name = "xrTable19";
            this.xrTable19.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow82});
            this.xrTable19.Size = new System.Drawing.Size(433, 25);
            this.xrTable19.StylePriority.UseForeColor = false;
            // 
            // xrTableRow82
            // 
            this.xrTableRow82.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell104});
            this.xrTableRow82.Name = "xrTableRow82";
            this.xrTableRow82.Weight = 1;
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell104.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.StylePriority.UseFont = false;
            this.xrTableCell104.StylePriority.UseForeColor = false;
            this.xrTableCell104.Text = "Public Domain Records ";
            this.xrTableCell104.Weight = 3;
            // 
            // DetailReport17
            // 
            this.DetailReport17.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail18,
            this.GroupHeader18});
            this.DetailReport17.DataMember = "ConsumerJudgement";
            this.DetailReport17.Level = 17;
            this.DetailReport17.Name = "DetailReport17";
            // 
            // Detail18
            // 
            this.Detail18.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable25});
            this.Detail18.Height = 33;
            this.Detail18.Name = "Detail18";
            // 
            // xrTable25
            // 
            this.xrTable25.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable25.Location = new System.Drawing.Point(8, 8);
            this.xrTable25.Name = "xrTable25";
            this.xrTable25.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow88});
            this.xrTable25.Size = new System.Drawing.Size(800, 25);
            this.xrTable25.StylePriority.UseBorderColor = false;
            // 
            // xrTableRow88
            // 
            this.xrTableRow88.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell135,
            this.xrTableCell136,
            this.xrTableCell137,
            this.xrTableCell138,
            this.xrTableCell139,
            this.xrTableCell140,
            this.xrTableCell141,
            this.xrTableCell142,
            this.xrTableCell143});
            this.xrTableRow88.Name = "xrTableRow88";
            this.xrTableRow88.Weight = 1;
            // 
            // xrTableCell135
            // 
            this.xrTableCell135.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.CaseNumber", "")});
            this.xrTableCell135.Name = "xrTableCell135";
            this.xrTableCell135.Text = "xrTableCell126";
            this.xrTableCell135.Weight = 0.28;
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.CaseFilingDate", "")});
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.Text = "xrTableCell127";
            this.xrTableCell136.Weight = 0.18625;
            // 
            // xrTableCell137
            // 
            this.xrTableCell137.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.CaseType", "")});
            this.xrTableCell137.Name = "xrTableCell137";
            this.xrTableCell137.Text = "xrTableCell128";
            this.xrTableCell137.Weight = 0.37375;
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.DisputeAmt", "")});
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.Text = "xrTableCell129";
            this.xrTableCell138.Weight = 0.18625;
            // 
            // xrTableCell139
            // 
            this.xrTableCell139.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.PlaintiffName", "")});
            this.xrTableCell139.Name = "xrTableCell139";
            this.xrTableCell139.Text = "xrTableCell130";
            this.xrTableCell139.Weight = 0.34375;
            // 
            // xrTableCell140
            // 
            this.xrTableCell140.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.CourtName", "")});
            this.xrTableCell140.Name = "xrTableCell140";
            this.xrTableCell140.Text = "xrTableCell131";
            this.xrTableCell140.Weight = 0.34375;
            // 
            // xrTableCell141
            // 
            this.xrTableCell141.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.AttorneyName", "")});
            this.xrTableCell141.Name = "xrTableCell141";
            this.xrTableCell141.Text = "xrTableCell132";
            this.xrTableCell141.Weight = 0.40625000000000006;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.TelephoneNo", "")});
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.Text = "xrTableCell133";
            this.xrTableCell142.Weight = 0.3425;
            // 
            // xrTableCell143
            // 
            this.xrTableCell143.Name = "xrTableCell143";
            this.xrTableCell143.Weight = 0.5375;
            // 
            // GroupHeader18
            // 
            this.GroupHeader18.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable24,
            this.xrTable23});
            this.GroupHeader18.Height = 75;
            this.GroupHeader18.Name = "GroupHeader18";
            // 
            // xrTable24
            // 
            this.xrTable24.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable24.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable24.Location = new System.Drawing.Point(8, 50);
            this.xrTable24.Name = "xrTable24";
            this.xrTable24.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow87});
            this.xrTable24.Size = new System.Drawing.Size(800, 25);
            this.xrTable24.StylePriority.UseBorderColor = false;
            this.xrTable24.StylePriority.UseBorders = false;
            // 
            // xrTableRow87
            // 
            this.xrTableRow87.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell123,
            this.xrTableCell120,
            this.xrTableCell124,
            this.xrTableCell119,
            this.xrTableCell125,
            this.xrTableCell121,
            this.xrTableCell122,
            this.xrTableCell126,
            this.xrTableCell127});
            this.xrTableRow87.Name = "xrTableRow87";
            this.xrTableRow87.Weight = 1;
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.Text = "Case No.";
            this.xrTableCell123.Weight = 0.28;
            // 
            // xrTableCell120
            // 
            this.xrTableCell120.Name = "xrTableCell120";
            this.xrTableCell120.Text = "Issue date";
            this.xrTableCell120.Weight = 0.18625;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.Text = "Judgment Type";
            this.xrTableCell124.Weight = 0.37375;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.Text = "Amount";
            this.xrTableCell119.Weight = 0.18625;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.Text = "Plaintiff";
            this.xrTableCell125.Weight = 0.34374999999999994;
            // 
            // xrTableCell121
            // 
            this.xrTableCell121.Name = "xrTableCell121";
            this.xrTableCell121.Text = "Court";
            this.xrTableCell121.Weight = 0.34375;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.Text = "Attorney";
            this.xrTableCell122.Weight = 0.40625000000000006;
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.Text = "Phone No";
            this.xrTableCell126.Weight = 0.34250000000000008;
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.Text = "Comment";
            this.xrTableCell127.Weight = 0.5375;
            // 
            // xrTable23
            // 
            this.xrTable23.ForeColor = System.Drawing.Color.Black;
            this.xrTable23.Location = new System.Drawing.Point(8, 17);
            this.xrTable23.Name = "xrTable23";
            this.xrTable23.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow86});
            this.xrTable23.Size = new System.Drawing.Size(433, 25);
            this.xrTable23.StylePriority.UseForeColor = false;
            // 
            // xrTableRow86
            // 
            this.xrTableRow86.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell118});
            this.xrTableRow86.Name = "xrTableRow86";
            this.xrTableRow86.Weight = 1;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell118.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.StylePriority.UseFont = false;
            this.xrTableCell118.StylePriority.UseForeColor = false;
            this.xrTableCell118.Text = "Judgements";
            this.xrTableCell118.Weight = 3;
            // 
            // DetailReport18
            // 
            this.DetailReport18.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail19,
            this.GroupHeader19});
            this.DetailReport18.DataMember = "ConsumerAdminOrder";
            this.DetailReport18.Level = 18;
            this.DetailReport18.Name = "DetailReport18";
            // 
            // Detail19
            // 
            this.Detail19.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable28});
            this.Detail19.Height = 25;
            this.Detail19.Name = "Detail19";
            // 
            // xrTable28
            // 
            this.xrTable28.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable28.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable28.Location = new System.Drawing.Point(8, 0);
            this.xrTable28.Name = "xrTable28";
            this.xrTable28.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow91});
            this.xrTable28.Size = new System.Drawing.Size(800, 25);
            this.xrTable28.StylePriority.UseBorderColor = false;
            this.xrTable28.StylePriority.UseBorders = false;
            // 
            // xrTableRow91
            // 
            this.xrTableRow91.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell151,
            this.xrTableCell148,
            this.xrTableCell132,
            this.xrTableCell152,
            this.xrTableCell149,
            this.xrTableCell150,
            this.xrTableCell153,
            this.xrTableCell154,
            this.xrTableCell155});
            this.xrTableRow91.Name = "xrTableRow91";
            this.xrTableRow91.Weight = 1;
            // 
            // xrTableCell151
            // 
            this.xrTableCell151.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.CaseNumber", "")});
            this.xrTableCell151.Name = "xrTableCell151";
            this.xrTableCell151.Weight = 0.31375;
            // 
            // xrTableCell148
            // 
            this.xrTableCell148.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.CaseFilingDate", "")});
            this.xrTableCell148.Name = "xrTableCell148";
            this.xrTableCell148.Weight = 0.25;
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.LastUpdatedDate", "")});
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.Weight = 0.27875000000000005;
            // 
            // xrTableCell152
            // 
            this.xrTableCell152.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.CaseType", "")});
            this.xrTableCell152.Name = "xrTableCell152";
            this.xrTableCell152.Text = "xrTableCell152";
            this.xrTableCell152.Weight = 0.34375;
            // 
            // xrTableCell149
            // 
            this.xrTableCell149.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.DisputeAmt", "")});
            this.xrTableCell149.Name = "xrTableCell149";
            this.xrTableCell149.Weight = 0.21625;
            // 
            // xrTableCell150
            // 
            this.xrTableCell150.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.PlaintiffName", "")});
            this.xrTableCell150.Name = "xrTableCell150";
            this.xrTableCell150.Weight = 0.5;
            // 
            // xrTableCell153
            // 
            this.xrTableCell153.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.CourtName", "")});
            this.xrTableCell153.Name = "xrTableCell153";
            this.xrTableCell153.Weight = 0.3475;
            // 
            // xrTableCell154
            // 
            this.xrTableCell154.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.AttorneyName", "")});
            this.xrTableCell154.Name = "xrTableCell154";
            this.xrTableCell154.Weight = 0.4;
            // 
            // xrTableCell155
            // 
            this.xrTableCell155.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.TelephoneNo", "")});
            this.xrTableCell155.Name = "xrTableCell155";
            this.xrTableCell155.Weight = 0.35;
            // 
            // GroupHeader19
            // 
            this.GroupHeader19.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable27,
            this.xrTable26});
            this.GroupHeader19.Height = 70;
            this.GroupHeader19.Name = "GroupHeader19";
            // 
            // xrTable27
            // 
            this.xrTable27.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.xrTable27.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable27.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable27.Location = new System.Drawing.Point(8, 45);
            this.xrTable27.Name = "xrTable27";
            this.xrTable27.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow90});
            this.xrTable27.Size = new System.Drawing.Size(800, 25);
            this.xrTable27.StylePriority.UseBorderColor = false;
            this.xrTable27.StylePriority.UseBorders = false;
            // 
            // xrTableRow90
            // 
            this.xrTableRow90.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell134,
            this.xrTableCell133,
            this.xrTableCell129,
            this.xrTableCell146,
            this.xrTableCell144,
            this.xrTableCell130,
            this.xrTableCell147,
            this.xrTableCell145,
            this.xrTableCell131});
            this.xrTableRow90.Name = "xrTableRow90";
            this.xrTableRow90.Weight = 1;
            // 
            // xrTableCell134
            // 
            this.xrTableCell134.CanGrow = false;
            this.xrTableCell134.Name = "xrTableCell134";
            this.xrTableCell134.Text = "Case No.";
            this.xrTableCell134.Weight = 0.31375;
            // 
            // xrTableCell133
            // 
            this.xrTableCell133.CanGrow = false;
            this.xrTableCell133.Name = "xrTableCell133";
            this.xrTableCell133.Text = "Issue Date";
            this.xrTableCell133.Weight = 0.25;
            // 
            // xrTableCell129
            // 
            this.xrTableCell129.CanGrow = false;
            this.xrTableCell129.Name = "xrTableCell129";
            this.xrTableCell129.Text = "Date loaded";
            this.xrTableCell129.Weight = 0.27875000000000005;
            // 
            // xrTableCell146
            // 
            this.xrTableCell146.CanGrow = false;
            this.xrTableCell146.Name = "xrTableCell146";
            this.xrTableCell146.Text = "Judgment Type";
            this.xrTableCell146.Weight = 0.34375000000000006;
            // 
            // xrTableCell144
            // 
            this.xrTableCell144.CanGrow = false;
            this.xrTableCell144.Name = "xrTableCell144";
            this.xrTableCell144.Text = "Amount";
            this.xrTableCell144.Weight = 0.21625;
            // 
            // xrTableCell130
            // 
            this.xrTableCell130.CanGrow = false;
            this.xrTableCell130.Name = "xrTableCell130";
            this.xrTableCell130.Text = "Plaintiff";
            this.xrTableCell130.Weight = 0.5;
            // 
            // xrTableCell147
            // 
            this.xrTableCell147.CanGrow = false;
            this.xrTableCell147.Name = "xrTableCell147";
            this.xrTableCell147.Text = "Court";
            this.xrTableCell147.Weight = 0.3475;
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.CanGrow = false;
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.Text = "Attorney";
            this.xrTableCell145.Weight = 0.4;
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.CanGrow = false;
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.Text = "Phone No";
            this.xrTableCell131.Weight = 0.35;
            // 
            // xrTable26
            // 
            this.xrTable26.ForeColor = System.Drawing.Color.Black;
            this.xrTable26.Location = new System.Drawing.Point(8, 17);
            this.xrTable26.Name = "xrTable26";
            this.xrTable26.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow89});
            this.xrTable26.Size = new System.Drawing.Size(433, 25);
            this.xrTable26.StylePriority.UseForeColor = false;
            // 
            // xrTableRow89
            // 
            this.xrTableRow89.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell128});
            this.xrTableRow89.Name = "xrTableRow89";
            this.xrTableRow89.Weight = 1;
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell128.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.StylePriority.UseFont = false;
            this.xrTableCell128.StylePriority.UseForeColor = false;
            this.xrTableCell128.Text = "Admin Orders";
            this.xrTableCell128.Weight = 3;
            // 
            // DetailReport19
            // 
            this.DetailReport19.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail20,
            this.GroupHeader20});
            this.DetailReport19.DataMember = "ConsumerSequestration";
            this.DetailReport19.Level = 19;
            this.DetailReport19.Name = "DetailReport19";
            // 
            // Detail20
            // 
            this.Detail20.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable42});
            this.Detail20.Height = 25;
            this.Detail20.Name = "Detail20";
            // 
            // xrTable42
            // 
            this.xrTable42.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable42.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable42.Location = new System.Drawing.Point(8, 0);
            this.xrTable42.Name = "xrTable42";
            this.xrTable42.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow94});
            this.xrTable42.Size = new System.Drawing.Size(800, 25);
            this.xrTable42.StylePriority.UseBorderColor = false;
            this.xrTable42.StylePriority.UseBorders = false;
            // 
            // xrTableRow94
            // 
            this.xrTableRow94.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell166,
            this.xrTableCell167,
            this.xrTableCell168,
            this.xrTableCell169,
            this.xrTableCell170,
            this.xrTableCell171,
            this.xrTableCell172,
            this.xrTableCell173,
            this.xrTableCell174});
            this.xrTableRow94.Name = "xrTableRow94";
            this.xrTableRow94.Weight = 1;
            // 
            // xrTableCell166
            // 
            this.xrTableCell166.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.CaseNumber", "")});
            this.xrTableCell166.Name = "xrTableCell166";
            this.xrTableCell166.Text = "xrTableCell164";
            this.xrTableCell166.Weight = 0.31375;
            // 
            // xrTableCell167
            // 
            this.xrTableCell167.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.CaseFilingDate", "")});
            this.xrTableCell167.Name = "xrTableCell167";
            this.xrTableCell167.Text = "xrTableCell165";
            this.xrTableCell167.Weight = 0.25;
            // 
            // xrTableCell168
            // 
            this.xrTableCell168.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.LastUpdatedDate", "")});
            this.xrTableCell168.Name = "xrTableCell168";
            this.xrTableCell168.Text = "xrTableCell166";
            this.xrTableCell168.Weight = 0.27875000000000005;
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.CaseType", "")});
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.Text = "xrTableCell167";
            this.xrTableCell169.Weight = 0.34375;
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.DisputeAmt", "")});
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.Text = "xrTableCell168";
            this.xrTableCell170.Weight = 0.21625;
            // 
            // xrTableCell171
            // 
            this.xrTableCell171.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.PlaintiffName", "")});
            this.xrTableCell171.Name = "xrTableCell171";
            this.xrTableCell171.Text = "xrTableCell169";
            this.xrTableCell171.Weight = 0.5;
            // 
            // xrTableCell172
            // 
            this.xrTableCell172.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.CourtName", "")});
            this.xrTableCell172.Name = "xrTableCell172";
            this.xrTableCell172.Text = "xrTableCell170";
            this.xrTableCell172.Weight = 0.3475;
            // 
            // xrTableCell173
            // 
            this.xrTableCell173.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.AttorneyName", "")});
            this.xrTableCell173.Name = "xrTableCell173";
            this.xrTableCell173.Text = "xrTableCell171";
            this.xrTableCell173.Weight = 0.4;
            // 
            // xrTableCell174
            // 
            this.xrTableCell174.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.TelephoneNo", "")});
            this.xrTableCell174.Name = "xrTableCell174";
            this.xrTableCell174.Text = "xrTableCell172";
            this.xrTableCell174.Weight = 0.35;
            // 
            // GroupHeader20
            // 
            this.GroupHeader20.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable41,
            this.xrTable40});
            this.GroupHeader20.Height = 72;
            this.GroupHeader20.Name = "GroupHeader20";
            // 
            // xrTable41
            // 
            this.xrTable41.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.xrTable41.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable41.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable41.Location = new System.Drawing.Point(8, 46);
            this.xrTable41.Name = "xrTable41";
            this.xrTable41.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow93});
            this.xrTable41.Size = new System.Drawing.Size(800, 25);
            this.xrTable41.StylePriority.UseBorderColor = false;
            this.xrTable41.StylePriority.UseBorders = false;
            // 
            // xrTableRow93
            // 
            this.xrTableRow93.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell157,
            this.xrTableCell158,
            this.xrTableCell159,
            this.xrTableCell160,
            this.xrTableCell161,
            this.xrTableCell162,
            this.xrTableCell163,
            this.xrTableCell164,
            this.xrTableCell165});
            this.xrTableRow93.Name = "xrTableRow93";
            this.xrTableRow93.Weight = 1;
            // 
            // xrTableCell157
            // 
            this.xrTableCell157.CanGrow = false;
            this.xrTableCell157.Name = "xrTableCell157";
            this.xrTableCell157.Text = "Case No.";
            this.xrTableCell157.Weight = 0.31375;
            // 
            // xrTableCell158
            // 
            this.xrTableCell158.CanGrow = false;
            this.xrTableCell158.Name = "xrTableCell158";
            this.xrTableCell158.Text = "Issue Date";
            this.xrTableCell158.Weight = 0.25;
            // 
            // xrTableCell159
            // 
            this.xrTableCell159.CanGrow = false;
            this.xrTableCell159.Name = "xrTableCell159";
            this.xrTableCell159.Text = "Date loaded";
            this.xrTableCell159.Weight = 0.27875000000000005;
            // 
            // xrTableCell160
            // 
            this.xrTableCell160.CanGrow = false;
            this.xrTableCell160.Name = "xrTableCell160";
            this.xrTableCell160.Text = "Judgment Type";
            this.xrTableCell160.Weight = 0.34375000000000006;
            // 
            // xrTableCell161
            // 
            this.xrTableCell161.CanGrow = false;
            this.xrTableCell161.Name = "xrTableCell161";
            this.xrTableCell161.Text = "Amount";
            this.xrTableCell161.Weight = 0.21625;
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.CanGrow = false;
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.Text = "Plaintiff";
            this.xrTableCell162.Weight = 0.5;
            // 
            // xrTableCell163
            // 
            this.xrTableCell163.CanGrow = false;
            this.xrTableCell163.Name = "xrTableCell163";
            this.xrTableCell163.Text = "Court";
            this.xrTableCell163.Weight = 0.3475;
            // 
            // xrTableCell164
            // 
            this.xrTableCell164.CanGrow = false;
            this.xrTableCell164.Name = "xrTableCell164";
            this.xrTableCell164.Text = "Attorney";
            this.xrTableCell164.Weight = 0.4;
            // 
            // xrTableCell165
            // 
            this.xrTableCell165.CanGrow = false;
            this.xrTableCell165.Name = "xrTableCell165";
            this.xrTableCell165.Text = "Phone No";
            this.xrTableCell165.Weight = 0.35;
            // 
            // xrTable40
            // 
            this.xrTable40.ForeColor = System.Drawing.Color.Black;
            this.xrTable40.Location = new System.Drawing.Point(8, 17);
            this.xrTable40.Name = "xrTable40";
            this.xrTable40.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow92});
            this.xrTable40.Size = new System.Drawing.Size(433, 25);
            this.xrTable40.StylePriority.UseForeColor = false;
            // 
            // xrTableRow92
            // 
            this.xrTableRow92.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell156});
            this.xrTableRow92.Name = "xrTableRow92";
            this.xrTableRow92.Weight = 1;
            // 
            // xrTableCell156
            // 
            this.xrTableCell156.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell156.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell156.Name = "xrTableCell156";
            this.xrTableCell156.StylePriority.UseFont = false;
            this.xrTableCell156.StylePriority.UseForeColor = false;
            this.xrTableCell156.Text = "Sequestrations";
            this.xrTableCell156.Weight = 3;
            // 
            // DetailReport20
            // 
            this.DetailReport20.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail21,
            this.GroupHeader21});
            this.DetailReport20.DataMember = "ConsumerDebtReviewStatus";
            this.DetailReport20.Level = 20;
            this.DetailReport20.Name = "DetailReport20";
            // 
            // Detail21
            // 
            this.Detail21.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable44});
            this.Detail21.Name = "Detail21";
            // 
            // xrTable44
            // 
            this.xrTable44.Location = new System.Drawing.Point(8, 8);
            this.xrTable44.Name = "xrTable44";
            this.xrTable44.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow96,
            this.xrTableRow98,
            this.xrTableRow97});
            this.xrTable44.Size = new System.Drawing.Size(792, 75);
            // 
            // xrTableRow96
            // 
            this.xrTableRow96.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell176,
            this.xrTableCell177});
            this.xrTableRow96.Name = "xrTableRow96";
            this.xrTableRow96.Weight = 1;
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.Text = "Debt Review date";
            this.xrTableCell176.Weight = 0.66287878787878785;
            // 
            // xrTableCell177
            // 
            this.xrTableCell177.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtReviewStatus.DebtReviewStatusDate", "")});
            this.xrTableCell177.Name = "xrTableCell177";
            this.xrTableCell177.Text = "xrTableCell177";
            this.xrTableCell177.Weight = 2.3371212121212119;
            // 
            // xrTableRow98
            // 
            this.xrTableRow98.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell180,
            this.xrTableCell181});
            this.xrTableRow98.Name = "xrTableRow98";
            this.xrTableRow98.Weight = 1;
            // 
            // xrTableCell180
            // 
            this.xrTableCell180.Name = "xrTableCell180";
            this.xrTableCell180.Text = "Debt Counsellor Name";
            this.xrTableCell180.Weight = 0.66287878787878785;
            // 
            // xrTableCell181
            // 
            this.xrTableCell181.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtReviewStatus.DebtCounsellorName", "")});
            this.xrTableCell181.Name = "xrTableCell181";
            this.xrTableCell181.Text = "xrTableCell181";
            this.xrTableCell181.Weight = 2.3371212121212119;
            // 
            // xrTableRow97
            // 
            this.xrTableRow97.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell178,
            this.xrTableCell179});
            this.xrTableRow97.Name = "xrTableRow97";
            this.xrTableRow97.Weight = 1;
            // 
            // xrTableCell178
            // 
            this.xrTableCell178.Name = "xrTableCell178";
            this.xrTableCell178.Text = "Debt Counsellor Contact No.";
            this.xrTableCell178.Weight = 0.66287878787878785;
            // 
            // xrTableCell179
            // 
            this.xrTableCell179.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtReviewStatus.DebtCounsellorTelephoneNo", "")});
            this.xrTableCell179.Name = "xrTableCell179";
            this.xrTableCell179.Text = "xrTableCell179";
            this.xrTableCell179.Weight = 2.3371212121212119;
            // 
            // GroupHeader21
            // 
            this.GroupHeader21.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable43});
            this.GroupHeader21.Height = 34;
            this.GroupHeader21.Name = "GroupHeader21";
            // 
            // xrTable43
            // 
            this.xrTable43.ForeColor = System.Drawing.Color.Black;
            this.xrTable43.Location = new System.Drawing.Point(8, 8);
            this.xrTable43.Name = "xrTable43";
            this.xrTable43.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow95});
            this.xrTable43.Size = new System.Drawing.Size(433, 25);
            this.xrTable43.StylePriority.UseForeColor = false;
            // 
            // xrTableRow95
            // 
            this.xrTableRow95.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell175});
            this.xrTableRow95.Name = "xrTableRow95";
            this.xrTableRow95.Weight = 1;
            // 
            // xrTableCell175
            // 
            this.xrTableCell175.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell175.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell175.Name = "xrTableCell175";
            this.xrTableCell175.StylePriority.UseFont = false;
            this.xrTableCell175.StylePriority.UseForeColor = false;
            this.xrTableCell175.Text = "Debt Review Status";
            this.xrTableCell175.Weight = 3;
            // 
            // DetailReport21
            // 
            this.DetailReport21.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail22,
            this.GroupHeader22});
            this.DetailReport21.DataMember = "ConsumerEnquiryHistory";
            this.DetailReport21.Level = 21;
            this.DetailReport21.Name = "DetailReport21";
            // 
            // Detail22
            // 
            this.Detail22.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable47});
            this.Detail22.Height = 25;
            this.Detail22.Name = "Detail22";
            // 
            // xrTable47
            // 
            this.xrTable47.Location = new System.Drawing.Point(8, 0);
            this.xrTable47.Name = "xrTable47";
            this.xrTable47.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow101});
            this.xrTable47.Size = new System.Drawing.Size(800, 25);
            // 
            // xrTableRow101
            // 
            this.xrTableRow101.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell186,
            this.xrTableCell187,
            this.xrTableCell237});
            this.xrTableRow101.Name = "xrTableRow101";
            this.xrTableRow101.Weight = 1;
            // 
            // xrTableCell186
            // 
            this.xrTableCell186.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerEnquiryHistory.EnquiryDate", "")});
            this.xrTableCell186.Name = "xrTableCell186";
            this.xrTableCell186.Text = "xrTableCell186";
            this.xrTableCell186.Weight = 0.34375;
            // 
            // xrTableCell187
            // 
            this.xrTableCell187.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerEnquiryHistory.SubscriberName", "")});
            this.xrTableCell187.Name = "xrTableCell187";
            this.xrTableCell187.Text = "xrTableCell187";
            this.xrTableCell187.Weight = 1.28125;
            // 
            // xrTableCell237
            // 
            this.xrTableCell237.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerEnquiryHistory.SubscriberBusinessTypeDesc", "")});
            this.xrTableCell237.Name = "xrTableCell237";
            this.xrTableCell237.Text = "xrTableCell237";
            this.xrTableCell237.Weight = 1.375;
            // 
            // GroupHeader22
            // 
            this.GroupHeader22.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable46,
            this.xrTable45});
            this.GroupHeader22.Height = 75;
            this.GroupHeader22.Name = "GroupHeader22";
            // 
            // xrTable46
            // 
            this.xrTable46.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.xrTable46.Location = new System.Drawing.Point(8, 42);
            this.xrTable46.Name = "xrTable46";
            this.xrTable46.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow100});
            this.xrTable46.Size = new System.Drawing.Size(800, 33);
            // 
            // xrTableRow100
            // 
            this.xrTableRow100.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell183,
            this.xrTableCell184,
            this.xrTableCell185});
            this.xrTableRow100.Name = "xrTableRow100";
            this.xrTableRow100.Weight = 1;
            // 
            // xrTableCell183
            // 
            this.xrTableCell183.CanGrow = false;
            this.xrTableCell183.Name = "xrTableCell183";
            this.xrTableCell183.Text = "Enquiry Date";
            this.xrTableCell183.Weight = 0.34375;
            // 
            // xrTableCell184
            // 
            this.xrTableCell184.CanGrow = false;
            this.xrTableCell184.Name = "xrTableCell184";
            this.xrTableCell184.Text = "Name Of Credit Grantor";
            this.xrTableCell184.Weight = 1.28125;
            // 
            // xrTableCell185
            // 
            this.xrTableCell185.CanGrow = false;
            this.xrTableCell185.Name = "xrTableCell185";
            this.xrTableCell185.Text = "Type/Category of Credit Grantor";
            this.xrTableCell185.Weight = 1.375;
            // 
            // xrTable45
            // 
            this.xrTable45.ForeColor = System.Drawing.Color.Black;
            this.xrTable45.Location = new System.Drawing.Point(8, 8);
            this.xrTable45.Name = "xrTable45";
            this.xrTable45.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow99});
            this.xrTable45.Size = new System.Drawing.Size(433, 25);
            this.xrTable45.StylePriority.UseForeColor = false;
            // 
            // xrTableRow99
            // 
            this.xrTableRow99.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell182});
            this.xrTableRow99.Name = "xrTableRow99";
            this.xrTableRow99.Weight = 1;
            // 
            // xrTableCell182
            // 
            this.xrTableCell182.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell182.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell182.Name = "xrTableCell182";
            this.xrTableCell182.StylePriority.UseFont = false;
            this.xrTableCell182.StylePriority.UseForeColor = false;
            this.xrTableCell182.Text = "Trace Enquiry History";
            this.xrTableCell182.Weight = 3;
            // 
            // DetailReport22
            // 
            this.DetailReport22.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail23,
            this.GroupHeader23});
            this.DetailReport22.DataMember = "ConsumerPropertyInformation";
            this.DetailReport22.Level = 22;
            this.DetailReport22.Name = "DetailReport22";
            // 
            // Detail23
            // 
            this.Detail23.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable63,
            this.xrTable62});
            this.Detail23.Height = 204;
            this.Detail23.Name = "Detail23";
            // 
            // xrTable63
            // 
            this.xrTable63.Location = new System.Drawing.Point(8, 42);
            this.xrTable63.Name = "xrTable63";
            this.xrTable63.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow120,
            this.xrTableRow125,
            this.xrTableRow124,
            this.xrTableRow123,
            this.xrTableRow122,
            this.xrTableRow121});
            this.xrTable63.Size = new System.Drawing.Size(800, 145);
            // 
            // xrTableRow120
            // 
            this.xrTableRow120.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell290,
            this.xrTableCell291,
            this.xrTableCell293,
            this.xrTableCell292});
            this.xrTableRow120.Name = "xrTableRow120";
            this.xrTableRow120.Weight = 1;
            // 
            // xrTableCell290
            // 
            this.xrTableCell290.Name = "xrTableCell290";
            this.xrTableCell290.Text = "Title Deed number";
            this.xrTableCell290.Weight = 0.625;
            // 
            // xrTableCell291
            // 
            this.xrTableCell291.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.TitleDeedNo", "")});
            this.xrTableCell291.Name = "xrTableCell291";
            this.xrTableCell291.Text = "xrTableCell291";
            this.xrTableCell291.Weight = 0.90625;
            // 
            // xrTableCell293
            // 
            this.xrTableCell293.Name = "xrTableCell293";
            this.xrTableCell293.Text = "Erf/Site No.";
            this.xrTableCell293.Weight = 0.59375;
            // 
            // xrTableCell292
            // 
            this.xrTableCell292.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.ErfNo", "")});
            this.xrTableCell292.Name = "xrTableCell292";
            this.xrTableCell292.Text = "xrTableCell292";
            this.xrTableCell292.Weight = 0.875;
            // 
            // xrTableRow125
            // 
            this.xrTableRow125.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell310,
            this.xrTableCell311,
            this.xrTableCell312,
            this.xrTableCell313});
            this.xrTableRow125.Name = "xrTableRow125";
            this.xrTableRow125.Weight = 1;
            // 
            // xrTableCell310
            // 
            this.xrTableCell310.Name = "xrTableCell310";
            this.xrTableCell310.Text = "Deeds Office";
            this.xrTableCell310.Weight = 0.625;
            // 
            // xrTableCell311
            // 
            this.xrTableCell311.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.DeedsOffice", "")});
            this.xrTableCell311.Name = "xrTableCell311";
            this.xrTableCell311.Text = "xrTableCell311";
            this.xrTableCell311.Weight = 0.90625;
            // 
            // xrTableCell312
            // 
            this.xrTableCell312.Name = "xrTableCell312";
            this.xrTableCell312.Text = "Physical Address";
            this.xrTableCell312.Weight = 0.59375;
            // 
            // xrTableCell313
            // 
            this.xrTableCell313.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.PhysicalAddress", "")});
            this.xrTableCell313.Name = "xrTableCell313";
            this.xrTableCell313.Text = "xrTableCell313";
            this.xrTableCell313.Weight = 0.875;
            // 
            // xrTableRow124
            // 
            this.xrTableRow124.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell306,
            this.xrTableCell307,
            this.xrTableCell308,
            this.xrTableCell309});
            this.xrTableRow124.Name = "xrTableRow124";
            this.xrTableRow124.Weight = 1;
            // 
            // xrTableCell306
            // 
            this.xrTableCell306.Name = "xrTableCell306";
            this.xrTableCell306.Text = "Property Type";
            this.xrTableCell306.Weight = 0.625;
            // 
            // xrTableCell307
            // 
            this.xrTableCell307.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.PropertyTypeDesc", "")});
            this.xrTableCell307.Name = "xrTableCell307";
            this.xrTableCell307.Text = "xrTableCell307";
            this.xrTableCell307.Weight = 0.90625;
            // 
            // xrTableCell308
            // 
            this.xrTableCell308.Name = "xrTableCell308";
            this.xrTableCell308.Text = "Extent / Size";
            this.xrTableCell308.Weight = 0.59375;
            // 
            // xrTableCell309
            // 
            this.xrTableCell309.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.ErfSize", "")});
            this.xrTableCell309.Name = "xrTableCell309";
            this.xrTableCell309.Text = "xrTableCell309";
            this.xrTableCell309.Weight = 0.875;
            // 
            // xrTableRow123
            // 
            this.xrTableRow123.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell302,
            this.xrTableCell303,
            this.xrTableCell304,
            this.xrTableCell305});
            this.xrTableRow123.Name = "xrTableRow123";
            this.xrTableRow123.Weight = 1;
            // 
            // xrTableCell302
            // 
            this.xrTableCell302.Name = "xrTableCell302";
            this.xrTableCell302.Text = "Purchase Date";
            this.xrTableCell302.Weight = 0.625;
            // 
            // xrTableCell303
            // 
            this.xrTableCell303.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.PurchaseDate", "")});
            this.xrTableCell303.Name = "xrTableCell303";
            this.xrTableCell303.Text = "xrTableCell303";
            this.xrTableCell303.Weight = 0.90625;
            // 
            // xrTableCell304
            // 
            this.xrTableCell304.Name = "xrTableCell304";
            this.xrTableCell304.Text = "Purchase price";
            this.xrTableCell304.Weight = 0.59375;
            // 
            // xrTableCell305
            // 
            this.xrTableCell305.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.PurchasePriceAmt", "")});
            this.xrTableCell305.Name = "xrTableCell305";
            this.xrTableCell305.Text = "xrTableCell305";
            this.xrTableCell305.Weight = 0.875;
            // 
            // xrTableRow122
            // 
            this.xrTableRow122.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell298,
            this.xrTableCell299,
            this.xrTableCell300,
            this.xrTableCell301});
            this.xrTableRow122.Name = "xrTableRow122";
            this.xrTableRow122.Weight = 1;
            // 
            // xrTableCell298
            // 
            this.xrTableCell298.Name = "xrTableCell298";
            this.xrTableCell298.Text = "% Ownership";
            this.xrTableCell298.Weight = 0.625;
            // 
            // xrTableCell299
            // 
            this.xrTableCell299.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.BuyerSharePerc", "")});
            this.xrTableCell299.Name = "xrTableCell299";
            this.xrTableCell299.Text = "xrTableCell299";
            this.xrTableCell299.Weight = 0.90625;
            // 
            // xrTableCell300
            // 
            this.xrTableCell300.Name = "xrTableCell300";
            this.xrTableCell300.Text = "Bond Holder";
            this.xrTableCell300.Weight = 0.59375;
            // 
            // xrTableCell301
            // 
            this.xrTableCell301.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.BondHolderName", "")});
            this.xrTableCell301.Name = "xrTableCell301";
            this.xrTableCell301.Text = "xrTableCell301";
            this.xrTableCell301.Weight = 0.875;
            // 
            // xrTableRow121
            // 
            this.xrTableRow121.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell294,
            this.xrTableCell295,
            this.xrTableCell296,
            this.xrTableCell297});
            this.xrTableRow121.Name = "xrTableRow121";
            this.xrTableRow121.Weight = 1;
            // 
            // xrTableCell294
            // 
            this.xrTableCell294.Name = "xrTableCell294";
            this.xrTableCell294.Text = "Bond Number";
            this.xrTableCell294.Weight = 0.625;
            // 
            // xrTableCell295
            // 
            this.xrTableCell295.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.BondAccountNo", "")});
            this.xrTableCell295.Name = "xrTableCell295";
            this.xrTableCell295.Text = "xrTableCell295";
            this.xrTableCell295.Weight = 0.90625;
            // 
            // xrTableCell296
            // 
            this.xrTableCell296.Name = "xrTableCell296";
            this.xrTableCell296.Text = "Bond Amount";
            this.xrTableCell296.Weight = 0.59375;
            // 
            // xrTableCell297
            // 
            this.xrTableCell297.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.BondAmt", "")});
            this.xrTableCell297.Name = "xrTableCell297";
            this.xrTableCell297.Text = "xrTableCell297";
            this.xrTableCell297.Weight = 0.875;
            // 
            // xrTable62
            // 
            this.xrTable62.ForeColor = System.Drawing.Color.Black;
            this.xrTable62.Location = new System.Drawing.Point(8, 8);
            this.xrTable62.Name = "xrTable62";
            this.xrTable62.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow119});
            this.xrTable62.Size = new System.Drawing.Size(433, 25);
            this.xrTable62.StylePriority.UseForeColor = false;
            // 
            // xrTableRow119
            // 
            this.xrTableRow119.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell281});
            this.xrTableRow119.Name = "xrTableRow119";
            this.xrTableRow119.Weight = 1;
            // 
            // xrTableCell281
            // 
            this.xrTableCell281.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell281.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell281.Name = "xrTableCell281";
            this.xrTableCell281.StylePriority.UseFont = false;
            this.xrTableCell281.StylePriority.UseForeColor = false;
            this.xrTableCell281.Text = "Property Interest";
            this.xrTableCell281.Weight = 3;
            // 
            // GroupHeader23
            // 
            this.GroupHeader23.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable61});
            this.GroupHeader23.Height = 46;
            this.GroupHeader23.Name = "GroupHeader23";
            // 
            // xrTable61
            // 
            this.xrTable61.ForeColor = System.Drawing.Color.Black;
            this.xrTable61.Location = new System.Drawing.Point(8, 17);
            this.xrTable61.Name = "xrTable61";
            this.xrTable61.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow118});
            this.xrTable61.Size = new System.Drawing.Size(433, 25);
            this.xrTable61.StylePriority.UseForeColor = false;
            // 
            // xrTableRow118
            // 
            this.xrTableRow118.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell280});
            this.xrTableRow118.Name = "xrTableRow118";
            this.xrTableRow118.Weight = 1;
            // 
            // xrTableCell280
            // 
            this.xrTableCell280.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell280.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell280.Name = "xrTableCell280";
            this.xrTableCell280.StylePriority.UseFont = false;
            this.xrTableCell280.StylePriority.UseForeColor = false;
            this.xrTableCell280.Text = "Property Interests";
            this.xrTableCell280.Weight = 3;
            // 
            // DetailReport23
            // 
            this.DetailReport23.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail24,
            this.GroupHeader27});
            this.DetailReport23.DataMember = "ConsumerDirectorShipLink";
            this.DetailReport23.Level = 23;
            this.DetailReport23.Name = "DetailReport23";
            // 
            // Detail24
            // 
            this.Detail24.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable59,
            this.xrTable60});
            this.Detail24.Height = 151;
            this.Detail24.Name = "Detail24";
            // 
            // xrTable59
            // 
            this.xrTable59.Location = new System.Drawing.Point(8, 42);
            this.xrTable59.Name = "xrTable59";
            this.xrTable59.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow113,
            this.xrTableRow116,
            this.xrTableRow117,
            this.xrTableRow115});
            this.xrTable59.Size = new System.Drawing.Size(792, 97);
            // 
            // xrTableRow113
            // 
            this.xrTableRow113.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell273,
            this.xrTableCell275,
            this.xrTableCell277,
            this.xrTableCell276});
            this.xrTableRow113.Name = "xrTableRow113";
            this.xrTableRow113.Weight = 1;
            // 
            // xrTableCell273
            // 
            this.xrTableCell273.Name = "xrTableCell273";
            this.xrTableCell273.Text = "Current Post";
            this.xrTableCell273.Weight = 1;
            // 
            // xrTableCell275
            // 
            this.xrTableCell275.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.DirectorDesignationDesc", "")});
            this.xrTableCell275.Name = "xrTableCell275";
            this.xrTableCell275.Text = "xrTableCell275";
            this.xrTableCell275.Weight = 0.67045454545454541;
            // 
            // xrTableCell277
            // 
            this.xrTableCell277.Name = "xrTableCell277";
            this.xrTableCell277.Text = "Date of Inception";
            this.xrTableCell277.Weight = 0.63636363636363646;
            // 
            // xrTableCell276
            // 
            this.xrTableCell276.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.AppointmentDate", "")});
            this.xrTableCell276.Name = "xrTableCell276";
            this.xrTableCell276.Text = "xrTableCell276";
            this.xrTableCell276.Weight = 0.69318181818181812;
            // 
            // xrTableRow116
            // 
            this.xrTableRow116.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell282,
            this.xrTableCell283,
            this.xrTableCell284,
            this.xrTableCell285});
            this.xrTableRow116.Name = "xrTableRow116";
            this.xrTableRow116.Weight = 1;
            // 
            // xrTableCell282
            // 
            this.xrTableCell282.Name = "xrTableCell282";
            this.xrTableCell282.Text = "Company Name";
            this.xrTableCell282.Weight = 1;
            // 
            // xrTableCell283
            // 
            this.xrTableCell283.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.CommercialName", "")});
            this.xrTableCell283.Name = "xrTableCell283";
            this.xrTableCell283.Text = "xrTableCell283";
            this.xrTableCell283.Weight = 0.67045454545454541;
            // 
            // xrTableCell284
            // 
            this.xrTableCell284.Name = "xrTableCell284";
            this.xrTableCell284.Text = "Company Reg No.";
            this.xrTableCell284.Weight = 0.63636363636363646;
            // 
            // xrTableCell285
            // 
            this.xrTableCell285.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.RegistrationNo", "")});
            this.xrTableCell285.Name = "xrTableCell285";
            this.xrTableCell285.Text = "xrTableCell285";
            this.xrTableCell285.Weight = 0.69318181818181812;
            // 
            // xrTableRow117
            // 
            this.xrTableRow117.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell286,
            this.xrTableCell287,
            this.xrTableCell288,
            this.xrTableCell289});
            this.xrTableRow117.Name = "xrTableRow117";
            this.xrTableRow117.Weight = 1;
            // 
            // xrTableCell286
            // 
            this.xrTableCell286.Name = "xrTableCell286";
            this.xrTableCell286.Text = "Company Address";
            this.xrTableCell286.Weight = 1;
            // 
            // xrTableCell287
            // 
            this.xrTableCell287.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.PhysicalAddress", "")});
            this.xrTableCell287.Name = "xrTableCell287";
            this.xrTableCell287.Text = "xrTableCell287";
            this.xrTableCell287.Weight = 0.67045454545454541;
            // 
            // xrTableCell288
            // 
            this.xrTableCell288.Name = "xrTableCell288";
            this.xrTableCell288.Text = "Company Phone No.";
            this.xrTableCell288.Weight = 0.63636363636363646;
            // 
            // xrTableCell289
            // 
            this.xrTableCell289.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.TelephoneNo", "")});
            this.xrTableCell289.Name = "xrTableCell289";
            this.xrTableCell289.Text = "xrTableCell289";
            this.xrTableCell289.Weight = 0.69318181818181812;
            // 
            // xrTableRow115
            // 
            this.xrTableRow115.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell278,
            this.xrTableCell279});
            this.xrTableRow115.Name = "xrTableRow115";
            this.xrTableRow115.Weight = 1;
            // 
            // xrTableCell278
            // 
            this.xrTableCell278.Name = "xrTableCell278";
            this.xrTableCell278.Text = "Industry category";
            this.xrTableCell278.Weight = 1;
            // 
            // xrTableCell279
            // 
            this.xrTableCell279.Name = "xrTableCell279";
            this.xrTableCell279.Text = "xrTableCell279";
            this.xrTableCell279.Weight = 2;
            // 
            // xrTable60
            // 
            this.xrTable60.ForeColor = System.Drawing.Color.Black;
            this.xrTable60.Location = new System.Drawing.Point(8, 8);
            this.xrTable60.Name = "xrTable60";
            this.xrTable60.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow114});
            this.xrTable60.Size = new System.Drawing.Size(625, 25);
            this.xrTable60.StylePriority.UseForeColor = false;
            // 
            // xrTableRow114
            // 
            this.xrTableRow114.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell274});
            this.xrTableRow114.Name = "xrTableRow114";
            this.xrTableRow114.Weight = 1;
            // 
            // xrTableCell274
            // 
            this.xrTableCell274.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell274.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell274.Name = "xrTableCell274";
            this.xrTableCell274.StylePriority.UseFont = false;
            this.xrTableCell274.StylePriority.UseForeColor = false;
            this.xrTableCell274.Text = "Directorship Link";
            this.xrTableCell274.Weight = 3;
            // 
            // GroupHeader27
            // 
            this.GroupHeader27.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable58});
            this.GroupHeader27.Height = 33;
            this.GroupHeader27.Name = "GroupHeader27";
            // 
            // xrTable58
            // 
            this.xrTable58.ForeColor = System.Drawing.Color.Black;
            this.xrTable58.Location = new System.Drawing.Point(8, 8);
            this.xrTable58.Name = "xrTable58";
            this.xrTable58.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow112});
            this.xrTable58.Size = new System.Drawing.Size(433, 25);
            this.xrTable58.StylePriority.UseForeColor = false;
            // 
            // xrTableRow112
            // 
            this.xrTableRow112.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell272});
            this.xrTableRow112.Name = "xrTableRow112";
            this.xrTableRow112.Weight = 1;
            // 
            // xrTableCell272
            // 
            this.xrTableCell272.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell272.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell272.Name = "xrTableCell272";
            this.xrTableCell272.StylePriority.UseFont = false;
            this.xrTableCell272.StylePriority.UseForeColor = false;
            this.xrTableCell272.Text = "Directorship Links";
            this.xrTableCell272.Weight = 3;
            // 
            // DetailReport24
            // 
            this.DetailReport24.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail25,
            this.GroupHeader25});
            this.DetailReport24.DataMember = "TelephoneLinkageHome";
            this.DetailReport24.Level = 24;
            this.DetailReport24.Name = "DetailReport24";
            // 
            // Detail25
            // 
            this.Detail25.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable51});
            this.Detail25.Height = 25;
            this.Detail25.Name = "Detail25";
            // 
            // xrTable51
            // 
            this.xrTable51.Location = new System.Drawing.Point(8, 0);
            this.xrTable51.Name = "xrTable51";
            this.xrTable51.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow105});
            this.xrTable51.Size = new System.Drawing.Size(800, 25);
            // 
            // xrTableRow105
            // 
            this.xrTableRow105.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell243,
            this.xrTableCell244,
            this.xrTableCell249,
            this.xrTableCell248,
            this.xrTableCell245});
            this.xrTableRow105.Name = "xrTableRow105";
            this.xrTableRow105.Weight = 1;
            // 
            // xrTableCell243
            // 
            this.xrTableCell243.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TelephoneLinkageHome.ConsumerName", "")});
            this.xrTableCell243.Name = "xrTableCell243";
            this.xrTableCell243.Text = "xrTableCell243";
            this.xrTableCell243.Weight = 0.625;
            // 
            // xrTableCell244
            // 
            this.xrTableCell244.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TelephoneLinkageHome.Surname", "")});
            this.xrTableCell244.Name = "xrTableCell244";
            this.xrTableCell244.Text = "xrTableCell244";
            this.xrTableCell244.Weight = 0.625;
            // 
            // xrTableCell249
            // 
            this.xrTableCell249.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TelephoneLinkageHome.HomeTelephone", "")});
            this.xrTableCell249.Name = "xrTableCell249";
            this.xrTableCell249.Text = "xrTableCell249";
            this.xrTableCell249.Weight = 0.56125;
            // 
            // xrTableCell248
            // 
            this.xrTableCell248.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TelephoneLinkageHome.WorkTelephone", "")});
            this.xrTableCell248.Name = "xrTableCell248";
            this.xrTableCell248.Text = "xrTableCell248";
            this.xrTableCell248.Weight = 0.595;
            // 
            // xrTableCell245
            // 
            this.xrTableCell245.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TelephoneLinkageHome.CellularNo", "")});
            this.xrTableCell245.Name = "xrTableCell245";
            this.xrTableCell245.Text = "xrTableCell245";
            this.xrTableCell245.Weight = 0.59375;
            // 
            // GroupHeader25
            // 
            this.GroupHeader25.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable50,
            this.xrTable49,
            this.xrTable48});
            this.GroupHeader25.Height = 117;
            this.GroupHeader25.Name = "GroupHeader25";
            // 
            // xrTable50
            // 
            this.xrTable50.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.xrTable50.Location = new System.Drawing.Point(8, 92);
            this.xrTable50.Name = "xrTable50";
            this.xrTable50.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow104});
            this.xrTable50.Size = new System.Drawing.Size(800, 25);
            // 
            // xrTableRow104
            // 
            this.xrTableRow104.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell246,
            this.xrTableCell240,
            this.xrTableCell247,
            this.xrTableCell241,
            this.xrTableCell242});
            this.xrTableRow104.Name = "xrTableRow104";
            this.xrTableRow104.Weight = 1;
            // 
            // xrTableCell246
            // 
            this.xrTableCell246.CanGrow = false;
            this.xrTableCell246.Name = "xrTableCell246";
            this.xrTableCell246.Text = "Name";
            this.xrTableCell246.Weight = 0.6275;
            // 
            // xrTableCell240
            // 
            this.xrTableCell240.CanGrow = false;
            this.xrTableCell240.Name = "xrTableCell240";
            this.xrTableCell240.Text = "Surname";
            this.xrTableCell240.Weight = 0.6275;
            // 
            // xrTableCell247
            // 
            this.xrTableCell247.CanGrow = false;
            this.xrTableCell247.Name = "xrTableCell247";
            this.xrTableCell247.Text = "Home Number";
            this.xrTableCell247.Weight = 0.56375;
            // 
            // xrTableCell241
            // 
            this.xrTableCell241.CanGrow = false;
            this.xrTableCell241.Name = "xrTableCell241";
            this.xrTableCell241.Text = "Work Number";
            this.xrTableCell241.Weight = 0.5975;
            // 
            // xrTableCell242
            // 
            this.xrTableCell242.CanGrow = false;
            this.xrTableCell242.Name = "xrTableCell242";
            this.xrTableCell242.Text = "Cell Number";
            this.xrTableCell242.Weight = 0.58375;
            // 
            // xrTable49
            // 
            this.xrTable49.ForeColor = System.Drawing.Color.Black;
            this.xrTable49.Location = new System.Drawing.Point(8, 42);
            this.xrTable49.Name = "xrTable49";
            this.xrTable49.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow103});
            this.xrTable49.Size = new System.Drawing.Size(625, 25);
            this.xrTable49.StylePriority.UseForeColor = false;
            // 
            // xrTableRow103
            // 
            this.xrTableRow103.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell239});
            this.xrTableRow103.Name = "xrTableRow103";
            this.xrTableRow103.Weight = 1;
            // 
            // xrTableCell239
            // 
            this.xrTableCell239.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.HomeTelephoneNo", "Latest Home Telephone Link to {0}")});
            this.xrTableCell239.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell239.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell239.Name = "xrTableCell239";
            this.xrTableCell239.StylePriority.UseFont = false;
            this.xrTableCell239.StylePriority.UseForeColor = false;
            this.xrTableCell239.Text = "xrTableCell239";
            this.xrTableCell239.Weight = 3;
            // 
            // xrTable48
            // 
            this.xrTable48.ForeColor = System.Drawing.Color.Black;
            this.xrTable48.Location = new System.Drawing.Point(8, 0);
            this.xrTable48.Name = "xrTable48";
            this.xrTable48.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow102});
            this.xrTable48.Size = new System.Drawing.Size(433, 25);
            this.xrTable48.StylePriority.UseForeColor = false;
            // 
            // xrTableRow102
            // 
            this.xrTableRow102.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell238});
            this.xrTableRow102.Name = "xrTableRow102";
            this.xrTableRow102.Weight = 1;
            // 
            // xrTableCell238
            // 
            this.xrTableCell238.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell238.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell238.Name = "xrTableCell238";
            this.xrTableCell238.StylePriority.UseFont = false;
            this.xrTableCell238.StylePriority.UseForeColor = false;
            this.xrTableCell238.Text = "LINKAGES";
            this.xrTableCell238.Weight = 3;
            // 
            // DetailReport25
            // 
            this.DetailReport25.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail26,
            this.GroupHeader24});
            this.DetailReport25.DataMember = "TelephoneLinkageWork";
            this.DetailReport25.Level = 25;
            this.DetailReport25.Name = "DetailReport25";
            // 
            // Detail26
            // 
            this.Detail26.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable54});
            this.Detail26.Height = 25;
            this.Detail26.Name = "Detail26";
            // 
            // xrTable54
            // 
            this.xrTable54.Location = new System.Drawing.Point(8, 0);
            this.xrTable54.Name = "xrTable54";
            this.xrTable54.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow108});
            this.xrTable54.Size = new System.Drawing.Size(800, 25);
            // 
            // xrTableRow108
            // 
            this.xrTableRow108.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell256,
            this.xrTableCell257,
            this.xrTableCell258,
            this.xrTableCell259,
            this.xrTableCell260});
            this.xrTableRow108.Name = "xrTableRow108";
            this.xrTableRow108.Weight = 1;
            // 
            // xrTableCell256
            // 
            this.xrTableCell256.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TelephoneLinkageWork.ConsumerName", "")});
            this.xrTableCell256.Name = "xrTableCell256";
            this.xrTableCell256.Text = "xrTableCell256";
            this.xrTableCell256.Weight = 0.625;
            // 
            // xrTableCell257
            // 
            this.xrTableCell257.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TelephoneLinkageWork.Surname", "")});
            this.xrTableCell257.Name = "xrTableCell257";
            this.xrTableCell257.Text = "xrTableCell257";
            this.xrTableCell257.Weight = 0.625;
            // 
            // xrTableCell258
            // 
            this.xrTableCell258.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TelephoneLinkageWork.HomeTelephone", "")});
            this.xrTableCell258.Name = "xrTableCell258";
            this.xrTableCell258.Text = "xrTableCell258";
            this.xrTableCell258.Weight = 0.56125;
            // 
            // xrTableCell259
            // 
            this.xrTableCell259.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TelephoneLinkageWork.WorkTelephone", "")});
            this.xrTableCell259.Name = "xrTableCell259";
            this.xrTableCell259.Text = "xrTableCell259";
            this.xrTableCell259.Weight = 0.595;
            // 
            // xrTableCell260
            // 
            this.xrTableCell260.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TelephoneLinkageWork.CellularNo", "")});
            this.xrTableCell260.Name = "xrTableCell260";
            this.xrTableCell260.Text = "xrTableCell260";
            this.xrTableCell260.Weight = 0.59375;
            // 
            // GroupHeader24
            // 
            this.GroupHeader24.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable53,
            this.xrTable52});
            this.GroupHeader24.Height = 75;
            this.GroupHeader24.Name = "GroupHeader24";
            // 
            // xrTable53
            // 
            this.xrTable53.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.xrTable53.Location = new System.Drawing.Point(8, 50);
            this.xrTable53.Name = "xrTable53";
            this.xrTable53.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow107});
            this.xrTable53.Size = new System.Drawing.Size(800, 25);
            // 
            // xrTableRow107
            // 
            this.xrTableRow107.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell251,
            this.xrTableCell252,
            this.xrTableCell253,
            this.xrTableCell254,
            this.xrTableCell255});
            this.xrTableRow107.Name = "xrTableRow107";
            this.xrTableRow107.Weight = 1;
            // 
            // xrTableCell251
            // 
            this.xrTableCell251.CanGrow = false;
            this.xrTableCell251.Name = "xrTableCell251";
            this.xrTableCell251.Text = "Name";
            this.xrTableCell251.Weight = 0.6275;
            // 
            // xrTableCell252
            // 
            this.xrTableCell252.CanGrow = false;
            this.xrTableCell252.Name = "xrTableCell252";
            this.xrTableCell252.Text = "Surname";
            this.xrTableCell252.Weight = 0.6275;
            // 
            // xrTableCell253
            // 
            this.xrTableCell253.CanGrow = false;
            this.xrTableCell253.Name = "xrTableCell253";
            this.xrTableCell253.Text = "Home Number";
            this.xrTableCell253.Weight = 0.56375;
            // 
            // xrTableCell254
            // 
            this.xrTableCell254.CanGrow = false;
            this.xrTableCell254.Name = "xrTableCell254";
            this.xrTableCell254.Text = "Work Number";
            this.xrTableCell254.Weight = 0.5975;
            // 
            // xrTableCell255
            // 
            this.xrTableCell255.CanGrow = false;
            this.xrTableCell255.Name = "xrTableCell255";
            this.xrTableCell255.Text = "Cell Number";
            this.xrTableCell255.Weight = 0.58375;
            // 
            // xrTable52
            // 
            this.xrTable52.ForeColor = System.Drawing.Color.Black;
            this.xrTable52.Location = new System.Drawing.Point(8, 0);
            this.xrTable52.Name = "xrTable52";
            this.xrTable52.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow106});
            this.xrTable52.Size = new System.Drawing.Size(625, 25);
            this.xrTable52.StylePriority.UseForeColor = false;
            // 
            // xrTableRow106
            // 
            this.xrTableRow106.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell250});
            this.xrTableRow106.Name = "xrTableRow106";
            this.xrTableRow106.Weight = 1;
            // 
            // xrTableCell250
            // 
            this.xrTableCell250.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.CellularNo", "Latest Work Telephone Link to {0}")});
            this.xrTableCell250.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell250.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell250.Name = "xrTableCell250";
            this.xrTableCell250.StylePriority.UseFont = false;
            this.xrTableCell250.StylePriority.UseForeColor = false;
            this.xrTableCell250.Text = "xrTableCell250";
            this.xrTableCell250.Weight = 3;
            // 
            // DetailReport26
            // 
            this.DetailReport26.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail27,
            this.GroupHeader26});
            this.DetailReport26.DataMember = "TelephoneLinkageCellular";
            this.DetailReport26.Level = 26;
            this.DetailReport26.Name = "DetailReport26";
            // 
            // Detail27
            // 
            this.Detail27.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable57});
            this.Detail27.Height = 25;
            this.Detail27.Name = "Detail27";
            // 
            // xrTable57
            // 
            this.xrTable57.Location = new System.Drawing.Point(8, 0);
            this.xrTable57.Name = "xrTable57";
            this.xrTable57.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow111});
            this.xrTable57.Size = new System.Drawing.Size(800, 25);
            // 
            // xrTableRow111
            // 
            this.xrTableRow111.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell267,
            this.xrTableCell268,
            this.xrTableCell269,
            this.xrTableCell270,
            this.xrTableCell271});
            this.xrTableRow111.Name = "xrTableRow111";
            this.xrTableRow111.Weight = 1;
            // 
            // xrTableCell267
            // 
            this.xrTableCell267.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TelephoneLinkageCellular.ConsumerName", "")});
            this.xrTableCell267.Name = "xrTableCell267";
            this.xrTableCell267.Text = "xrTableCell256";
            this.xrTableCell267.Weight = 0.625;
            // 
            // xrTableCell268
            // 
            this.xrTableCell268.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TelephoneLinkageCellular.Surname", "")});
            this.xrTableCell268.Name = "xrTableCell268";
            this.xrTableCell268.Text = "xrTableCell257";
            this.xrTableCell268.Weight = 0.625;
            // 
            // xrTableCell269
            // 
            this.xrTableCell269.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TelephoneLinkageCellular.HomeTelephone", "")});
            this.xrTableCell269.Name = "xrTableCell269";
            this.xrTableCell269.Text = "xrTableCell258";
            this.xrTableCell269.Weight = 0.56125;
            // 
            // xrTableCell270
            // 
            this.xrTableCell270.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TelephoneLinkageCellular.WorkTelephone", "")});
            this.xrTableCell270.Name = "xrTableCell270";
            this.xrTableCell270.Text = "xrTableCell259";
            this.xrTableCell270.Weight = 0.595;
            // 
            // xrTableCell271
            // 
            this.xrTableCell271.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TelephoneLinkageCellular.CellularNo", "")});
            this.xrTableCell271.Name = "xrTableCell271";
            this.xrTableCell271.Text = "xrTableCell260";
            this.xrTableCell271.Weight = 0.59375;
            // 
            // GroupHeader26
            // 
            this.GroupHeader26.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable56,
            this.xrTable55});
            this.GroupHeader26.Height = 75;
            this.GroupHeader26.Name = "GroupHeader26";
            // 
            // xrTable56
            // 
            this.xrTable56.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.xrTable56.Location = new System.Drawing.Point(8, 50);
            this.xrTable56.Name = "xrTable56";
            this.xrTable56.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow110});
            this.xrTable56.Size = new System.Drawing.Size(800, 25);
            // 
            // xrTableRow110
            // 
            this.xrTableRow110.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell262,
            this.xrTableCell263,
            this.xrTableCell264,
            this.xrTableCell265,
            this.xrTableCell266});
            this.xrTableRow110.Name = "xrTableRow110";
            this.xrTableRow110.Weight = 1;
            // 
            // xrTableCell262
            // 
            this.xrTableCell262.CanGrow = false;
            this.xrTableCell262.Name = "xrTableCell262";
            this.xrTableCell262.Text = "Name";
            this.xrTableCell262.Weight = 0.6275;
            // 
            // xrTableCell263
            // 
            this.xrTableCell263.CanGrow = false;
            this.xrTableCell263.Name = "xrTableCell263";
            this.xrTableCell263.Text = "Surname";
            this.xrTableCell263.Weight = 0.6275;
            // 
            // xrTableCell264
            // 
            this.xrTableCell264.CanGrow = false;
            this.xrTableCell264.Name = "xrTableCell264";
            this.xrTableCell264.Text = "Home Number";
            this.xrTableCell264.Weight = 0.56375;
            // 
            // xrTableCell265
            // 
            this.xrTableCell265.CanGrow = false;
            this.xrTableCell265.Name = "xrTableCell265";
            this.xrTableCell265.Text = "Work Number";
            this.xrTableCell265.Weight = 0.5975;
            // 
            // xrTableCell266
            // 
            this.xrTableCell266.CanGrow = false;
            this.xrTableCell266.Name = "xrTableCell266";
            this.xrTableCell266.Text = "Cell Number";
            this.xrTableCell266.Weight = 0.58375;
            // 
            // xrTable55
            // 
            this.xrTable55.ForeColor = System.Drawing.Color.Black;
            this.xrTable55.Location = new System.Drawing.Point(8, 0);
            this.xrTable55.Name = "xrTable55";
            this.xrTable55.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow109});
            this.xrTable55.Size = new System.Drawing.Size(625, 25);
            this.xrTable55.StylePriority.UseForeColor = false;
            // 
            // xrTableRow109
            // 
            this.xrTableRow109.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell261});
            this.xrTableRow109.Name = "xrTableRow109";
            this.xrTableRow109.Weight = 1;
            // 
            // xrTableCell261
            // 
            this.xrTableCell261.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.CellularNo", "Latest Work Telephone Link to {0}")});
            this.xrTableCell261.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell261.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell261.Name = "xrTableCell261";
            this.xrTableCell261.StylePriority.UseFont = false;
            this.xrTableCell261.StylePriority.UseForeColor = false;
            this.xrTableCell261.Text = "xrTableCell250";
            this.xrTableCell261.Weight = 3;
            // 
            // CreditEnquiryReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.PageFooter,
            this.ReportHeader,
            this.GroupHeader1,
            this.DetailReport,
            this.DetailReport1,
            this.DetailReport2,
            this.DetailReport3,
            this.DetailReport4,
            this.DetailReport5,
            this.DetailReport6,
            this.DetailReport7,
            this.DetailReport8,
            this.DetailReport9,
            this.DetailReport10,
            this.DetailReport11,
            this.DetailReport12,
            this.DetailReport13,
            this.DetailReport14,
            this.DetailReport15,
            this.DetailReport16,
            this.DetailReport17,
            this.DetailReport18,
            this.DetailReport19,
            this.DetailReport20,
            this.DetailReport21,
            this.DetailReport22,
            this.DetailReport23,
            this.DetailReport24,
            this.DetailReport25,
            this.DetailReport26});
            this.Margins = new System.Drawing.Printing.Margins(20, 20, 100, 100);
            this.Name = "CreditEnquiryReport";
            this.PageHeight = 1100;
            this.PageWidth = 850;
            this.RequestParameters = false;
            this.Version = "9.1";
            this.XmlDataPath = "C:\\ConsumerTraceReport.xml";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblPersonalDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonalDetailsSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblFraudIndSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblFraudSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblscrorevalues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblscoreColumns)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblScoreHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDebtSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDebtHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAccStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblCrAccStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblNameHistoryValues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblNameHistColumns)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblNameHistoryHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblpersonalConfirmationHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDetailedInfoHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblConfAddHistoryC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAddHistoryHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo2;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell lblHeaderText;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTable tblPersonalDetail;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.XRTable PersonalDetailsSummary;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell lblReferenceNo;
        private DevExpress.XtraReports.UI.XRTableCell lblRefnoValue;
        private DevExpress.XtraReports.UI.XRTableCell lblExtRefNo;
        private DevExpress.XtraReports.UI.XRTableCell lblExternalRefNoValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell lblIDNo;
        private DevExpress.XtraReports.UI.XRTableCell lblIDnoValue;
        private DevExpress.XtraReports.UI.XRTableCell lblPPNo;
        private DevExpress.XtraReports.UI.XRTableCell lblPassportnoValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell lblSurname;
        private DevExpress.XtraReports.UI.XRTableCell lblSurnameValue;
        private DevExpress.XtraReports.UI.XRTableCell lblResAdd;
        private DevExpress.XtraReports.UI.XRTableCell lblResAddValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell lblFirstname;
        private DevExpress.XtraReports.UI.XRTableCell lblFirstnameValue;
        private DevExpress.XtraReports.UI.XRTableCell lbl;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell lblSecondName;
        private DevExpress.XtraReports.UI.XRTableCell lblSecondnameValue;
        private DevExpress.XtraReports.UI.XRTableCell lblHomeTelNo;
        private DevExpress.XtraReports.UI.XRTableCell lblHomeTelNoValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell lblTitle;
        private DevExpress.XtraReports.UI.XRTableCell lbltitlevalue;
        private DevExpress.XtraReports.UI.XRTableCell lblPostalAdd;
        private DevExpress.XtraReports.UI.XRTableCell lblPostalAddvalue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell lblGender;
        private DevExpress.XtraReports.UI.XRTableCell lblGenderValue;
        private DevExpress.XtraReports.UI.XRTableCell lbl2;
        private DevExpress.XtraReports.UI.XRTableCell lbl2value;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell lblDOB;
        private DevExpress.XtraReports.UI.XRTableCell lblDOBValue;
        private DevExpress.XtraReports.UI.XRTableCell lblWorkTelNo;
        private DevExpress.XtraReports.UI.XRTableCell lblWorkTelNoValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell lblCurrentEmployer;
        private DevExpress.XtraReports.UI.XRTableCell lblCurrentEmployerValue;
        private DevExpress.XtraReports.UI.XRTableCell lblMobileNo;
        private DevExpress.XtraReports.UI.XRTableCell lblMobileNoValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell lblEmailAddress;
        private DevExpress.XtraReports.UI.XRTableCell lblEmailAddressValue;
        private DevExpress.XtraReports.UI.XRTableCell lblMaritalStatus;
        private DevExpress.XtraReports.UI.XRTableCell lblMaritalStatusValue;
        private DevExpress.XtraReports.UI.XRTableCell lblValue;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport1;
        private DevExpress.XtraReports.UI.DetailBand Detail2;
        private DevExpress.XtraReports.UI.XRTable tblFraudIndSummary;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell lblVerified;
        private DevExpress.XtraReports.UI.XRTableCell lblVerifiedVal;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell lblDeceased;
        private DevExpress.XtraReports.UI.XRTableCell lblDeceasedValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell lblFoundDB;
        private DevExpress.XtraReports.UI.XRTableCell lblFoundDBValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell lblEmployerFraudDB;
        private DevExpress.XtraReports.UI.XRTableCell lblEmployerFraudDBValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell lblProrectiveReg;
        private DevExpress.XtraReports.UI.XRTableCell lblProrectiveRegValue;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader2;
        private DevExpress.XtraReports.UI.XRTable tblFraudSummary;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport2;
        private DevExpress.XtraReports.UI.DetailBand Detail3;
        private DevExpress.XtraReports.UI.XRTable tblscrorevalues;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell lblUniqueIdentifierValue;
        private DevExpress.XtraReports.UI.XRTableCell lblDateValue;
        private DevExpress.XtraReports.UI.XRTableCell lblScroreValue;
        private DevExpress.XtraReports.UI.XRTableCell lblExceptionValue;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader3;
        private DevExpress.XtraReports.UI.XRTable tblscoreColumns;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell lblUniqueIdentifier;
        private DevExpress.XtraReports.UI.XRTableCell lblDate;
        private DevExpress.XtraReports.UI.XRTableCell lblScrore;
        private DevExpress.XtraReports.UI.XRTableCell lblException;
        private DevExpress.XtraReports.UI.XRTable tblScoreHeader;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport3;
        private DevExpress.XtraReports.UI.DetailBand Detail4;
        private DevExpress.XtraReports.UI.XRTable tblDebtSummary;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell lblActiveAcc;
        private DevExpress.XtraReports.UI.XRTableCell lblActiveAccValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell lblDelinquentAcc;
        private DevExpress.XtraReports.UI.XRTableCell lblDelinquentAccValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell lblMonthlyinst;
        private DevExpress.XtraReports.UI.XRTableCell lblMonthlyinstValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell lblOutstandingDebt;
        private DevExpress.XtraReports.UI.XRTableCell lblOutstandingDebtValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell lblArrearamt;
        private DevExpress.XtraReports.UI.XRTableCell lblArrearamtvalue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell lblJdgDate;
        private DevExpress.XtraReports.UI.XRTableCell lblJdgDateValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell lblJudAmt;
        private DevExpress.XtraReports.UI.XRTableCell lblJudAmtValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell lblDelinquentrating;
        private DevExpress.XtraReports.UI.XRTableCell lblDelinquentratingValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell lblDbtReview;
        private DevExpress.XtraReports.UI.XRTableCell lblDbtReviewValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell lblAO;
        private DevExpress.XtraReports.UI.XRTableCell lblAOValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell lblAdverseAmt;
        private DevExpress.XtraReports.UI.XRTableCell lblAdverseAmtValue;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader4;
        private DevExpress.XtraReports.UI.XRTable tblDebtHeader;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell lblAccGood;
        private DevExpress.XtraReports.UI.XRTableCell lblAccGoodvalue;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport4;
        private DevExpress.XtraReports.UI.DetailBand Detail5;
        private DevExpress.XtraReports.UI.XRTable tblAccStatus;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell lblRetail;
        private DevExpress.XtraReports.UI.XRTableCell lblRetailG;
        private DevExpress.XtraReports.UI.XRTableCell lblRetailB;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader5;
        private DevExpress.XtraReports.UI.XRTable tblCrAccStatus;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell lblAccdesc;
        private DevExpress.XtraReports.UI.XRTableCell lblGood;
        private DevExpress.XtraReports.UI.XRTableCell lblBad;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow47;
        private DevExpress.XtraReports.UI.XRTableCell lblCc;
        private DevExpress.XtraReports.UI.XRTableCell lblCcG;
        private DevExpress.XtraReports.UI.XRTableCell lblCcB;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow46;
        private DevExpress.XtraReports.UI.XRTableCell lblFurnAcc;
        private DevExpress.XtraReports.UI.XRTableCell lblFurnAccG;
        private DevExpress.XtraReports.UI.XRTableCell lblFurnAccB;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow45;
        private DevExpress.XtraReports.UI.XRTableCell lblInsurance;
        private DevExpress.XtraReports.UI.XRTableCell lblInsuranceG;
        private DevExpress.XtraReports.UI.XRTableCell lblInsuranceB;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow44;
        private DevExpress.XtraReports.UI.XRTableCell lblFinance;
        private DevExpress.XtraReports.UI.XRTableCell lblFinanceG;
        private DevExpress.XtraReports.UI.XRTableCell lblFinanceB;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow43;
        private DevExpress.XtraReports.UI.XRTableCell lblBankAcc;
        private DevExpress.XtraReports.UI.XRTableCell lblBankAccG;
        private DevExpress.XtraReports.UI.XRTableCell lblBankAccB;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow42;
        private DevExpress.XtraReports.UI.XRTableCell lblTelecomms;
        private DevExpress.XtraReports.UI.XRTableCell lblTelecommsG;
        private DevExpress.XtraReports.UI.XRTableCell lblTelecommsB;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow41;
        private DevExpress.XtraReports.UI.XRTableCell lblHomeLoan;
        private DevExpress.XtraReports.UI.XRTableCell lblHomeLoanG;
        private DevExpress.XtraReports.UI.XRTableCell lblHomeLoanB;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
        private DevExpress.XtraReports.UI.XRTableCell lblVehicleFin;
        private DevExpress.XtraReports.UI.XRTableCell lblVehicleFinG;
        private DevExpress.XtraReports.UI.XRTableCell lblVehicleFinB;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell lblOtherAcc;
        private DevExpress.XtraReports.UI.XRTableCell lblOtherAccG;
        private DevExpress.XtraReports.UI.XRTableCell lblOtherAccB;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport5;
        private DevExpress.XtraReports.UI.DetailBand Detail6;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader6;
        private DevExpress.XtraReports.UI.XRTable tblDetailedInfoHeader;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTable tblNameHistoryValues;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow52;
        private DevExpress.XtraReports.UI.XRTableCell lblNameValue;
        private DevExpress.XtraReports.UI.XRTableCell lblNHSurNameValue;
        private DevExpress.XtraReports.UI.XRTableCell lblNHdatevalue;
        private DevExpress.XtraReports.UI.XRTable tblNameHistColumns;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow51;
        private DevExpress.XtraReports.UI.XRTableCell lblName;
        private DevExpress.XtraReports.UI.XRTableCell lblNHSurName;
        private DevExpress.XtraReports.UI.XRTableCell lblNHIDNo;
        private DevExpress.XtraReports.UI.XRTableCell lblNHdate;
        private DevExpress.XtraReports.UI.XRTable tblNameHistoryHeader;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTable tblpersonalConfirmationHeader;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell lblNHIDNoValue;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport6;
        private DevExpress.XtraReports.UI.DetailBand Detail7;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader7;
        private DevExpress.XtraReports.UI.XRTable tblConfAddHistoryC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow54;
        private DevExpress.XtraReports.UI.XRTableCell lblCAddLine1;
        private DevExpress.XtraReports.UI.XRTableCell lblCAddLine3;
        private DevExpress.XtraReports.UI.XRTableCell lblCAddLine4;
        private DevExpress.XtraReports.UI.XRTableCell lblCdate;
        private DevExpress.XtraReports.UI.XRTable tblAddHistoryHeader;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell lblCAddType;
        private DevExpress.XtraReports.UI.XRTableCell lblCAddLine2;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport7;
        private DevExpress.XtraReports.UI.DetailBand Detail8;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader8;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport8;
        private DevExpress.XtraReports.UI.DetailBand Detail9;
        private DevExpress.XtraReports.UI.XRTable xrTable9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader9;
        private DevExpress.XtraReports.UI.XRTable xrTable8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport9;
        private DevExpress.XtraReports.UI.DetailBand Detail10;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader10;
        private DevExpress.XtraReports.UI.XRTable xrTable11;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTable xrTable10;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTable xrTable31;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell206;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell207;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell208;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell209;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell210;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell211;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell212;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell213;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell214;
        private DevExpress.XtraReports.UI.XRTable xrTable30;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow65;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell203;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell200;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell191;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell204;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell201;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell192;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell205;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell202;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell193;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport10;
        private DevExpress.XtraReports.UI.DetailBand Detail11;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader11;
        private DevExpress.XtraReports.UI.XRTable xrTable32;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell198;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell189;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell199;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell190;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell215;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell216;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell194;
        private DevExpress.XtraReports.UI.XRTable xrTable29;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell188;
        private DevExpress.XtraReports.UI.XRTable xrTable33;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell217;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell195;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell218;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell196;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell219;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell220;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell197;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport11;
        private DevExpress.XtraReports.UI.DetailBand Detail12;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader12;
        private DevExpress.XtraReports.UI.XRTable xrTable35;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell228;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell222;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell229;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell223;
        private DevExpress.XtraReports.UI.XRTable xrTable34;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell221;
        private DevExpress.XtraReports.UI.XRTable xrTable36;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell225;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell227;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell224;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell226;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport12;
        private DevExpress.XtraReports.UI.DetailBand Detail13;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader13;
        private DevExpress.XtraReports.UI.XRTable xrTable38;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow73;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell231;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell232;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell233;
        private DevExpress.XtraReports.UI.XRTable xrTable37;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow72;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell230;
        private DevExpress.XtraReports.UI.XRTable xrTable39;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell234;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell235;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell236;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport13;
        private DevExpress.XtraReports.UI.DetailBand Detail14;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader14;
        private DevExpress.XtraReports.UI.XRTable xrTable12;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTable xrTable13;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow77;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTable xrTable14;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow76;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport14;
        private DevExpress.XtraReports.UI.DetailBand Detail15;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader15;
        private DevExpress.XtraReports.UI.XRTable xrTable15;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow78;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo3;
        private DevExpress.XtraReports.UI.XRTable xrTable16;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow79;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell98;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell96;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell99;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport15;
        private DevExpress.XtraReports.UI.DetailBand Detail16;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader16;
        private DevExpress.XtraReports.UI.XRTable xrTable17;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow80;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.XRTable xrTable18;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow81;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell102;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell103;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport16;
        private DevExpress.XtraReports.UI.DetailBand Detail17;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader17;
        private DevExpress.XtraReports.UI.XRTable xrTable21;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow84;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        private DevExpress.XtraReports.UI.XRTable xrTable20;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow83;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell111;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTable xrTable19;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow82;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
        private DevExpress.XtraReports.UI.XRTable xrTable22;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow85;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell116;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell117;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport17;
        private DevExpress.XtraReports.UI.DetailBand Detail18;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader18;
        private DevExpress.XtraReports.UI.XRTable xrTable23;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow86;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRTable xrTable24;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow87;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell123;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell120;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell124;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell121;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell126;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell127;
        private DevExpress.XtraReports.UI.XRTable xrTable25;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow88;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell135;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell136;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell137;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell139;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell140;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell141;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell143;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport18;
        private DevExpress.XtraReports.UI.DetailBand Detail19;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader19;
        private DevExpress.XtraReports.UI.XRTable xrTable27;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow90;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell134;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell133;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell129;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell146;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell144;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell130;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell147;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell145;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
        private DevExpress.XtraReports.UI.XRTable xrTable26;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow89;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell128;
        private DevExpress.XtraReports.UI.XRTable xrTable28;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow91;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell151;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell148;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell132;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell152;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell149;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell150;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell153;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell154;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell155;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport19;
        private DevExpress.XtraReports.UI.DetailBand Detail20;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader20;
        private DevExpress.XtraReports.UI.XRTable xrTable41;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow93;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell157;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell158;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell159;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell160;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell161;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell162;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell163;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell164;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell165;
        private DevExpress.XtraReports.UI.XRTable xrTable40;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow92;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell156;
        private DevExpress.XtraReports.UI.XRTable xrTable42;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow94;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell166;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell167;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell168;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell169;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell170;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell171;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell172;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell173;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell174;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport20;
        private DevExpress.XtraReports.UI.DetailBand Detail21;
        private DevExpress.XtraReports.UI.XRTable xrTable44;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow96;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell176;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell177;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow98;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell180;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell181;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow97;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell178;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell179;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader21;
        private DevExpress.XtraReports.UI.XRTable xrTable43;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow95;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell175;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport21;
        private DevExpress.XtraReports.UI.DetailBand Detail22;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader22;
        private DevExpress.XtraReports.UI.XRTable xrTable47;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow101;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell186;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell187;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell237;
        private DevExpress.XtraReports.UI.XRTable xrTable46;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow100;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell183;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell184;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell185;
        private DevExpress.XtraReports.UI.XRTable xrTable45;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow99;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell182;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport22;
        private DevExpress.XtraReports.UI.DetailBand Detail23;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader23;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport23;
        private DevExpress.XtraReports.UI.DetailBand Detail24;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport24;
        private DevExpress.XtraReports.UI.DetailBand Detail25;
        private DevExpress.XtraReports.UI.XRTable xrTable51;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow105;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell243;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell244;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell249;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell248;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell245;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader25;
        private DevExpress.XtraReports.UI.XRTable xrTable50;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow104;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell246;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell240;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell247;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell241;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell242;
        private DevExpress.XtraReports.UI.XRTable xrTable49;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow103;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell239;
        private DevExpress.XtraReports.UI.XRTable xrTable48;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow102;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell238;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport25;
        private DevExpress.XtraReports.UI.DetailBand Detail26;
        private DevExpress.XtraReports.UI.XRTable xrTable54;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow108;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell256;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell257;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell258;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell259;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell260;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader24;
        private DevExpress.XtraReports.UI.XRTable xrTable53;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow107;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell251;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell252;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell253;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell254;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell255;
        private DevExpress.XtraReports.UI.XRTable xrTable52;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow106;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell250;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport26;
        private DevExpress.XtraReports.UI.DetailBand Detail27;
        private DevExpress.XtraReports.UI.XRTable xrTable57;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow111;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell267;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell268;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell269;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell270;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell271;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader26;
        private DevExpress.XtraReports.UI.XRTable xrTable56;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow110;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell262;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell263;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell264;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell265;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell266;
        private DevExpress.XtraReports.UI.XRTable xrTable55;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow109;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell261;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader27;
        private DevExpress.XtraReports.UI.XRTable xrTable58;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow112;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell272;
        private DevExpress.XtraReports.UI.XRTable xrTable60;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow114;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell274;
        private DevExpress.XtraReports.UI.XRTable xrTable59;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow113;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell273;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell275;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell277;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell276;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow116;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell282;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell283;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell284;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell285;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow117;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell286;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell287;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell288;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell289;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow115;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell278;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell279;
        private DevExpress.XtraReports.UI.XRTable xrTable63;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow120;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell290;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell291;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell293;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell292;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow125;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell310;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell311;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell312;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell313;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow124;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell306;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell307;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell308;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell309;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow123;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell302;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell303;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell304;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell305;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow122;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell298;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell299;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell300;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell301;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow121;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell294;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell295;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell296;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell297;
        private DevExpress.XtraReports.UI.XRTable xrTable62;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow119;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell281;
        private DevExpress.XtraReports.UI.XRTable xrTable61;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow118;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell280;
    }
}
