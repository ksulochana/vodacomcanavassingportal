namespace XdsPortalReports
{
    partial class ABSATraceReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ABSATraceReport));
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLabel95 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel73 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow51 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPictureBox2 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblHeaderText = new DevExpress.XtraReports.UI.XRTableCell();
            this.ConsumerDetail = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailConsumerDetail = new DevExpress.XtraReports.UI.DetailBand();
            this.PersonalDetailsSummary = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblReferenceNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblRefnoValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblExtRefNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblExternalRefNoValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblIDNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblIDnoValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPPNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPassportnoValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSurname = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSurnameValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblResAdd = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblResAddValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblFirstname = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblFirstnameValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPostalAdd = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPostalAddValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSecondName = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSecondnameValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHomeTelNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHomeTelNoValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblTitle = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbltitlevalue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblWorkTelno = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblWorkTelnoValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblGender = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGenderValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMobileNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMobileNoValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDOB = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDOBValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblEmailAddress = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblEmailAddressValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblmaritalStatus = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblmaritalStatusValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCurrentEmployer = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCurrentEmployerValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader6 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblPersonalDetail = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblPersonalDetail = new DevExpress.XtraReports.UI.XRTableCell();
            this.Scoring = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailScoring = new DevExpress.XtraReports.UI.DetailBand();
            this.tblscrorevalues = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblClassification = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDescription = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader3 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblScore = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblScore = new DevExpress.XtraReports.UI.XRTableCell();
            this.DebtSummary = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailDebtSummary = new DevExpress.XtraReports.UI.DetailBand();
            this.tblDebtSummary = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblActiveAcc = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblActiveAccValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblAccGood = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAccGoodvalue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDelinquentAcc = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDelinquentAccValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblMonthlyinst = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMonthlyinstValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblOutstandingDebt = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblOutstandingDebtValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblArrearamt = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblArrearamtvalue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblJdgDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJdgDateValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblJudAmt = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJudAmtValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDelinquentrating = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDelinquentratingValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDbtReview = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDbtReviewValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblAO = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAOValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblAdverseAmt = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAdverseAmtValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader4 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblDebtHeader = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDebtHeader = new DevExpress.XtraReports.UI.XRTableCell();
            this.AccountGoodBadSummary = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailAccountGoodBadSummary = new DevExpress.XtraReports.UI.DetailBand();
            this.tblAccStatus = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblAccdesc = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbDefCode = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGood = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblBad = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblRetail = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblRetailG = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblRetailB = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblCc = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCreditCradG = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCreditCardB = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblFurnAcc = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblFurnAccG = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblFurnAccB = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblInsurance = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblInsuranceG = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblInsuranceB = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblFinance = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblFinanceG = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblFinanceB = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblBankAcc = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblBankAccG = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblBankAccB = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblTelecomms = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTelecommsG = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTelecommsB = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblHomeLoan = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHomeLoanG = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHomeLoanB = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblVehicleFin = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblVehicleFinG = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblVehicleFinB = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblOtherAcc = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblOtherAccG = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblOtherAccB = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader5 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblCrAccStatus = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblCrAccStatus = new DevExpress.XtraReports.UI.XRTableCell();
            this.NameHistory = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailNameHistory = new DevExpress.XtraReports.UI.DetailBand();
            this.tblNamehistD = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow65 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDUpdatedDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHDSurname = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHDFirstname = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHDSecodnName = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHDInitial = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHDTitle = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHDIDno = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHDPPNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHDBirthDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader10 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblNameHisC = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow64 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblUpdatedDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHSurname = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHFirstname = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHSecodnName = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHInitial = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHTitle = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHIDno = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHPPNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHBirthDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblNameHistH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow63 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblNameHistH = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblConsInfoH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow62 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.AddressHistory = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailAddressHistory = new DevExpress.XtraReports.UI.DetailBand();
            this.tblAddHD = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow68 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDAddUpdatedate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAddtype = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAddline1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAddLine2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAddLine3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAddLine4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDPostalcode = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader11 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblAddHC = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow67 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblAddUpdatedate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAddtype = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAddline1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAddLine2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAddLine3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAddLine4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPostalcode = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblAddHistoryH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow66 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblAddHistoryH = new DevExpress.XtraReports.UI.XRTableCell();
            this.TelephoneHistory = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailTelephoneHistory = new DevExpress.XtraReports.UI.DetailBand();
            this.tblContactNoD = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow71 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDNoUpdatedate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDTelnoType = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDTelNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDEmail = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader12 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblContactNoC = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow70 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblNoUpdatedate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTelnoType = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTelNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblEmail = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblContactNoH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow69 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblContactNoH = new DevExpress.XtraReports.UI.XRTableCell();
            this.EmploymentHistory = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailEmploymentHistory = new DevExpress.XtraReports.UI.DetailBand();
            this.tblEmploymentD = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow74 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDEmpUpdatedate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDEmployer = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDDesignation = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader13 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblEmploymentC = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow73 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblEmpUpdatedate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblEmployer = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDesignation = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblEmploymentH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow72 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblEmploymentH = new DevExpress.XtraReports.UI.XRTableCell();
            this.AccountStatus = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailAccountStatus = new DevExpress.XtraReports.UI.DetailBand();
            this.tblAccStatusD = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow76 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblAccOpenDateD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCompanyD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCreditLimitD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCurrBalanceD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblInstallementD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblArrearsAmtD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAccTypeD = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader14 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblAccStatusC = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow77 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblAccOpenDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCompany = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCreditLimit = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCurrBalance = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblInstallement = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblArrearsAmt = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAccType = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblAccStatusH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow75 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblAccStatusH = new DevExpress.XtraReports.UI.XRTableCell();
            this.MonthlyPaymentHeader = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailMonthlyPaymentHeader = new DevExpress.XtraReports.UI.DetailBand();
            this.GroupHeader15 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblMonthlyPaymentH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow78 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblcompanyMP = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM01 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM02 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM03 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM04 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM05 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM06 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDelinquentStatusC = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageinfoMonthlyPaymentH = new DevExpress.XtraReports.UI.XRPageInfo();
            this.ConsumerDefinition = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailConsumerDefinition = new DevExpress.XtraReports.UI.DetailBand();
            this.tblConsumerDefinitionD = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow81 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDefDescD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbDefCodeD = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader16 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.tblConsumerDefinitionC = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow80 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDefDesc = new DevExpress.XtraReports.UI.XRTableCell();
            this.AdverseInfo = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailAdverseInfo = new DevExpress.XtraReports.UI.DetailBand();
            this.tblAdverseData = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow85 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDSubscriber = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAccNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDActionDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAmount = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAccStatus = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAdvComment = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader17 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblAdverseColumns = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow83 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSubscriber = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAccountNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblActionDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAmount = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAccStatus = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAdverseComment = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblAdverseH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow84 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblAdverseH = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblDomainH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow82 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Judgements = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailJudgements = new DevExpress.XtraReports.UI.DetailBand();
            this.tblDJudg = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow88 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblJDCaseno = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJDIssueDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJDType = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJDAmount = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJDPlaintiff = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJDCourt = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJDAttorney = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJDPhoneNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJDComment = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader18 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblCJud = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow87 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblJCaseNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJIssueDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJType = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJAmount = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJPlaintiff = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJCourt = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJAttorney = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJPhoneNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJComment = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblJudgmentsH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow86 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblJudgmentsH = new DevExpress.XtraReports.UI.XRTableCell();
            this.AdminOrders = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailAdminOrders = new DevExpress.XtraReports.UI.DetailBand();
            this.tblDAO = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow91 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblAODCaseno = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAODIssuedate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAODLoadeddate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAODType = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAODAmount = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAODPlaintiff = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAODCOurt = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAODAttorney = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAODPhoenNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAODComment = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader19 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblCAO = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow90 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblAOCaseno = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAOIssuedate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAODateLoaded = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAOType = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAOAmount = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAOPlaintiff = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAOCourt = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAOAttorney = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAOPhoneNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAOComment = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblAOH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow89 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblAOH = new DevExpress.XtraReports.UI.XRTableCell();
            this.Sequestration = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailSequestration = new DevExpress.XtraReports.UI.DetailBand();
            this.tblSEQD = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow94 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSEQDCaseno = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSEQDIssuedate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSEQDLoadedDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSEQDType = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSEQDAmount = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSEQDPlaintiff = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSEQDCourt = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSEQDAttorney = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSEQDPhoneNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader20 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblSEQC = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow93 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSEQCaseno = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSEQIssueDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSEQLoadeddate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSEQType = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSEQAmount = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSEQplaintiff = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSEQCourt = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSEQAttorney = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSEQPhoneNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblSEQH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow92 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSEQH = new DevExpress.XtraReports.UI.XRTableCell();
            this.DebtReviewStatus = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailDebtReviewStatus = new DevExpress.XtraReports.UI.DetailBand();
            this.tblDbtReviewD = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow48 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow96 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDbtReviewDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDbtReviewDateD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow98 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblCounsellorName = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCounsellorNameD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow97 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblCounsellorTelephoneNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCounsellorTelephoneNoD = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader21 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tbldbtReviewH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow95 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lbldbtReviewH = new DevExpress.XtraReports.UI.XRTableCell();
            this.propertyInfo = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailpropertyInfo = new DevExpress.XtraReports.UI.DetailBand();
            this.tblPropInterests = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow49 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow120 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDeedNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDDeedNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblsiteno = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDsiteno = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow125 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDeedsoffice = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDDeedsoffice = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhyadd = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDPhyadd = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow124 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblPropType = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDPropType = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSize = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDSize = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow123 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblPurchasedate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDPurchasedate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPurchaseprice = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDPurchaseprice = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow122 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblshareperc = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDshareperc = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHolder = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDHolder = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow121 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblBondNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDBondNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblBondAmount = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDBondAmount = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader23 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblPropInterestsH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow118 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblPropInterestsH = new DevExpress.XtraReports.UI.XRTableCell();
            this.DirectorshipLink = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailDirectorshipLink = new DevExpress.XtraReports.UI.DetailBand();
            this.tblDirInfo = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow50 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow113 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblCurrentPost = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDCurrentPost = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblInceptionDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAppDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow116 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblCompanyName = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDCompanyName = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblRegNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDRegNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow117 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblCompanyAdd = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDPhysicalAdd = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhoneNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDPhoneNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow115 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblIndustryCatg = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDIndCateg = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader27 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblDirectorlinksH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow112 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDirectorlinksH = new DevExpress.XtraReports.UI.XRTableCell();
            this.MonthlyPayment = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailMonthlyPayment = new DevExpress.XtraReports.UI.DetailBand();
            this.tblMonthlyPaymentD = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow79 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblcompanyMPD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM01D = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM02D = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM03D = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM04D = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM05D = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM06D = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDelinquentStatusD = new DevExpress.XtraReports.UI.XRTableCell();
            this.DefaultAlert = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailDefaultAlert = new DevExpress.XtraReports.UI.DetailBand();
            this.tblDefaultAlertD = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDACompanyD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAAccnoD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDADLoadeddateD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAAmountD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAStatusD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDACommentsD = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader2 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblDefaultAlertC = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDACompany = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAAccno = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDADLoadeddate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAAmount = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAStatus = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAComments = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblDefaultAlertH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDefaultAlertH = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail7 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblEnquiryDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblEnquiryDateValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.EnquiryHistory = new DevExpress.XtraReports.UI.DetailReportBand();
            this.DetailEnquiryHistory = new DevExpress.XtraReports.UI.DetailBand();
            this.tblEnquiryD = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow101 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblEnqDateD = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblsubscribernameD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblbusTypeD = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblEnquiryC = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow100 = new DevExpress.XtraReports.UI.XRTableRow();
            this.tblEnqDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblsubscribername = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblbusType = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblEnquiryH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow99 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblEnquiryH = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonalDetailsSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblPersonalDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblscrorevalues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblScore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDebtSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDebtHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAccStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblCrAccStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblNamehistD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblNameHisC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblNameHistH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblConsInfoH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAddHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAddHC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAddHistoryH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblContactNoD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblContactNoC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblContactNoH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEmploymentD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEmploymentC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEmploymentH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAccStatusD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAccStatusC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAccStatusH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblMonthlyPaymentH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblConsumerDefinitionD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblConsumerDefinitionC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAdverseData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAdverseColumns)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAdverseH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDomainH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDJudg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblCJud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblJudgmentsH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDAO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblCAO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAOH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblSEQD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblSEQC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblSEQH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDbtReviewD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbldbtReviewH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblPropInterests)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblPropInterestsH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDirInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDirectorlinksH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblMonthlyPaymentD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDefaultAlertD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDefaultAlertC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDefaultAlertH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEnquiryD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEnquiryC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEnquiryH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.HeightF = 2F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageHeader
            // 
            this.PageHeader.HeightF = 0F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox1,
            this.xrLabel95,
            this.xrLabel73,
            this.xrPageInfo2,
            this.xrPageInfo1});
            this.PageFooter.HeightF = 45.00002F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(81.92F, 35.0001F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // xrLabel95
            // 
            this.xrLabel95.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrLabel95.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrLabel95.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel95.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel95.ForeColor = System.Drawing.Color.DimGray;
            this.xrLabel95.LocationFloat = new DevExpress.Utils.PointFloat(733F, 9.999973F);
            this.xrLabel95.Name = "xrLabel95";
            this.xrLabel95.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel95.SizeF = new System.Drawing.SizeF(42F, 25F);
            this.xrLabel95.StylePriority.UseBackColor = false;
            this.xrLabel95.StylePriority.UseBorderColor = false;
            this.xrLabel95.StylePriority.UseBorders = false;
            this.xrLabel95.StylePriority.UseFont = false;
            this.xrLabel95.StylePriority.UseForeColor = false;
            this.xrLabel95.StylePriority.UseTextAlignment = false;
            this.xrLabel95.Text = "Page:";
            this.xrLabel95.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel73
            // 
            this.xrLabel73.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrLabel73.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrLabel73.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel73.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel73.ForeColor = System.Drawing.Color.DimGray;
            this.xrLabel73.LocationFloat = new DevExpress.Utils.PointFloat(90.83001F, 9.999974F);
            this.xrLabel73.Name = "xrLabel73";
            this.xrLabel73.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel73.SizeF = new System.Drawing.SizeF(300F, 25F);
            this.xrLabel73.StylePriority.UseBackColor = false;
            this.xrLabel73.StylePriority.UseBorderColor = false;
            this.xrLabel73.StylePriority.UseBorders = false;
            this.xrLabel73.StylePriority.UseFont = false;
            this.xrLabel73.StylePriority.UseForeColor = false;
            this.xrLabel73.StylePriority.UseTextAlignment = false;
            this.xrLabel73.Text = "A Partership Initiative with Xpert Decision Systems. ";
            this.xrLabel73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrPageInfo2.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrPageInfo2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPageInfo2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrPageInfo2.ForeColor = System.Drawing.Color.DimGray;
            this.xrPageInfo2.Format = "Printed Date: {0:dddd, dd MMMM yyyy H:mm}";
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(391F, 10.0001F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(342F, 25F);
            this.xrPageInfo2.StylePriority.UseBackColor = false;
            this.xrPageInfo2.StylePriority.UseBorderColor = false;
            this.xrPageInfo2.StylePriority.UseBorders = false;
            this.xrPageInfo2.StylePriority.UseFont = false;
            this.xrPageInfo2.StylePriority.UseForeColor = false;
            this.xrPageInfo2.StylePriority.UseTextAlignment = false;
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrPageInfo1.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrPageInfo1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPageInfo1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrPageInfo1.ForeColor = System.Drawing.Color.DimGray;
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(775F, 9.999973F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(25F, 25F);
            this.xrPageInfo1.StylePriority.UseBackColor = false;
            this.xrPageInfo1.StylePriority.UseBorderColor = false;
            this.xrPageInfo1.StylePriority.UseBorders = false;
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.StylePriority.UseForeColor = false;
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5,
            this.xrPictureBox2,
            this.xrLabel1,
            this.xrTable1});
            this.ReportHeader.HeightF = 115F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrTable5
            // 
            this.xrTable5.BorderColor = System.Drawing.Color.White;
            this.xrTable5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable5.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(8F, 8F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow51});
            this.xrTable5.SizeF = new System.Drawing.SizeF(50F, 100F);
            this.xrTable5.StylePriority.UseBorderColor = false;
            this.xrTable5.StylePriority.UseBorders = false;
            this.xrTable5.StylePriority.UseFont = false;
            // 
            // xrTableRow51
            // 
            this.xrTableRow51.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8});
            this.xrTableRow51.Name = "xrTableRow51";
            this.xrTableRow51.Weight = 1.5334063526834612;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.xrTableCell8.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.xrTableCell8.Font = new System.Drawing.Font("Arial", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell8.ForeColor = System.Drawing.Color.White;
            this.xrTableCell8.Multiline = true;
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseBackColor = false;
            this.xrTableCell8.StylePriority.UseBorderColor = false;
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.StylePriority.UseForeColor = false;
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell8.Weight = 1.9059967585089144;
            // 
            // xrPictureBox2
            // 
            this.xrPictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox2.Image")));
            this.xrPictureBox2.LocationFloat = new DevExpress.Utils.PointFloat(58F, 8F);
            this.xrPictureBox2.Name = "xrPictureBox2";
            this.xrPictureBox2.SizeF = new System.Drawing.SizeF(208F, 100F);
            // 
            // xrLabel1
            // 
            this.xrLabel1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.xrLabel1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel1.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(155)))), ((int)(((byte)(213)))));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(267F, 83F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(533F, 25F);
            this.xrLabel1.StylePriority.UseBorderColor = false;
            this.xrLabel1.StylePriority.UseBorders = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseForeColor = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Retail Bank Collections";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable1
            // 
            this.xrTable1.BorderColor = System.Drawing.Color.White;
            this.xrTable1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable1.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(267F, 8F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable1.SizeF = new System.Drawing.SizeF(533F, 75F);
            this.xrTable1.StylePriority.UseBorderColor = false;
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblHeaderText});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1.2727272727272727;
            // 
            // lblHeaderText
            // 
            this.lblHeaderText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.lblHeaderText.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.lblHeaderText.Font = new System.Drawing.Font("Arial", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeaderText.ForeColor = System.Drawing.Color.White;
            this.lblHeaderText.Multiline = true;
            this.lblHeaderText.Name = "lblHeaderText";
            this.lblHeaderText.StylePriority.UseBackColor = false;
            this.lblHeaderText.StylePriority.UseBorderColor = false;
            this.lblHeaderText.StylePriority.UseFont = false;
            this.lblHeaderText.StylePriority.UseForeColor = false;
            this.lblHeaderText.StylePriority.UseTextAlignment = false;
            this.lblHeaderText.Text = "Collection Analysis";
            this.lblHeaderText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblHeaderText.Weight = 1.9059967585089144;
            // 
            // ConsumerDetail
            // 
            this.ConsumerDetail.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailConsumerDetail,
            this.GroupHeader6});
            this.ConsumerDetail.DataMember = "ConsumerDetail";
            this.ConsumerDetail.Level = 1;
            this.ConsumerDetail.Name = "ConsumerDetail";
            // 
            // DetailConsumerDetail
            // 
            this.DetailConsumerDetail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.PersonalDetailsSummary});
            this.DetailConsumerDetail.HeightF = 180F;
            this.DetailConsumerDetail.Name = "DetailConsumerDetail";
            this.DetailConsumerDetail.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 100F);
            this.DetailConsumerDetail.StylePriority.UsePadding = false;
            // 
            // PersonalDetailsSummary
            // 
            this.PersonalDetailsSummary.BorderColor = System.Drawing.Color.White;
            this.PersonalDetailsSummary.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.PersonalDetailsSummary.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PersonalDetailsSummary.LocationFloat = new DevExpress.Utils.PointFloat(8F, 0F);
            this.PersonalDetailsSummary.Name = "PersonalDetailsSummary";
            this.PersonalDetailsSummary.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.PersonalDetailsSummary.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow4,
            this.xrTableRow3,
            this.xrTableRow13,
            this.xrTableRow10,
            this.xrTableRow9,
            this.xrTableRow8,
            this.xrTableRow7,
            this.xrTableRow6});
            this.PersonalDetailsSummary.SizeF = new System.Drawing.SizeF(792F, 180F);
            this.PersonalDetailsSummary.StylePriority.UseBorderColor = false;
            this.PersonalDetailsSummary.StylePriority.UseBorders = false;
            this.PersonalDetailsSummary.StylePriority.UseFont = false;
            this.PersonalDetailsSummary.StylePriority.UsePadding = false;
            this.PersonalDetailsSummary.StylePriority.UseTextAlignment = false;
            this.PersonalDetailsSummary.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblReferenceNo,
            this.lblRefnoValue,
            this.lblExtRefNo,
            this.lblExternalRefNoValue});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100F);
            this.xrTableRow1.StylePriority.UsePadding = false;
            this.xrTableRow1.Weight = 0.8;
            // 
            // lblReferenceNo
            // 
            this.lblReferenceNo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblReferenceNo.BorderColor = System.Drawing.Color.White;
            this.lblReferenceNo.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblReferenceNo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReferenceNo.ForeColor = System.Drawing.Color.Gray;
            this.lblReferenceNo.Name = "lblReferenceNo";
            this.lblReferenceNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblReferenceNo.StylePriority.UseBackColor = false;
            this.lblReferenceNo.StylePriority.UseBorderColor = false;
            this.lblReferenceNo.StylePriority.UseBorders = false;
            this.lblReferenceNo.StylePriority.UseFont = false;
            this.lblReferenceNo.StylePriority.UseForeColor = false;
            this.lblReferenceNo.StylePriority.UsePadding = false;
            this.lblReferenceNo.Text = "Reference No.";
            this.lblReferenceNo.Weight = 0.59755134281200639;
            // 
            // lblRefnoValue
            // 
            this.lblRefnoValue.BackColor = System.Drawing.Color.White;
            this.lblRefnoValue.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblRefnoValue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblRefnoValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.ReferenceNo")});
            this.lblRefnoValue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblRefnoValue.ForeColor = System.Drawing.Color.DimGray;
            this.lblRefnoValue.Name = "lblRefnoValue";
            this.lblRefnoValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblRefnoValue.StylePriority.UseBackColor = false;
            this.lblRefnoValue.StylePriority.UseBorderColor = false;
            this.lblRefnoValue.StylePriority.UseBorders = false;
            this.lblRefnoValue.StylePriority.UseFont = false;
            this.lblRefnoValue.StylePriority.UseForeColor = false;
            this.lblRefnoValue.StylePriority.UsePadding = false;
            this.lblRefnoValue.Weight = 0.89727268876814947;
            // 
            // lblExtRefNo
            // 
            this.lblExtRefNo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblExtRefNo.BorderColor = System.Drawing.Color.White;
            this.lblExtRefNo.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblExtRefNo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExtRefNo.ForeColor = System.Drawing.Color.Gray;
            this.lblExtRefNo.Name = "lblExtRefNo";
            this.lblExtRefNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblExtRefNo.StylePriority.UseBackColor = false;
            this.lblExtRefNo.StylePriority.UseBorderColor = false;
            this.lblExtRefNo.StylePriority.UseBorders = false;
            this.lblExtRefNo.StylePriority.UseFont = false;
            this.lblExtRefNo.StylePriority.UseForeColor = false;
            this.lblExtRefNo.StylePriority.UsePadding = false;
            this.lblExtRefNo.Text = "External Reference No.";
            this.lblExtRefNo.Weight = 0.59803108681621631;
            // 
            // lblExternalRefNoValue
            // 
            this.lblExternalRefNoValue.BackColor = System.Drawing.Color.White;
            this.lblExternalRefNoValue.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblExternalRefNoValue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblExternalRefNoValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.ExternalReference")});
            this.lblExternalRefNoValue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblExternalRefNoValue.ForeColor = System.Drawing.Color.DimGray;
            this.lblExternalRefNoValue.Name = "lblExternalRefNoValue";
            this.lblExternalRefNoValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblExternalRefNoValue.StylePriority.UseBackColor = false;
            this.lblExternalRefNoValue.StylePriority.UseBorderColor = false;
            this.lblExternalRefNoValue.StylePriority.UseBorders = false;
            this.lblExternalRefNoValue.StylePriority.UseFont = false;
            this.lblExternalRefNoValue.StylePriority.UseForeColor = false;
            this.lblExternalRefNoValue.StylePriority.UsePadding = false;
            this.lblExternalRefNoValue.Weight = 0.89578124523999147;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblIDNo,
            this.lblIDnoValue,
            this.lblPPNo,
            this.lblPassportnoValue});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100F);
            this.xrTableRow4.StylePriority.UsePadding = false;
            this.xrTableRow4.Weight = 0.79999999999999993;
            // 
            // lblIDNo
            // 
            this.lblIDNo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblIDNo.BorderColor = System.Drawing.Color.White;
            this.lblIDNo.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblIDNo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIDNo.ForeColor = System.Drawing.Color.Gray;
            this.lblIDNo.Name = "lblIDNo";
            this.lblIDNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblIDNo.StylePriority.UseBackColor = false;
            this.lblIDNo.StylePriority.UseBorderColor = false;
            this.lblIDNo.StylePriority.UseBorders = false;
            this.lblIDNo.StylePriority.UseFont = false;
            this.lblIDNo.StylePriority.UseForeColor = false;
            this.lblIDNo.StylePriority.UsePadding = false;
            this.lblIDNo.Text = "ID No.";
            this.lblIDNo.Weight = 0.59755134281200639;
            // 
            // lblIDnoValue
            // 
            this.lblIDnoValue.BackColor = System.Drawing.Color.White;
            this.lblIDnoValue.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblIDnoValue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblIDnoValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.IDNo")});
            this.lblIDnoValue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblIDnoValue.ForeColor = System.Drawing.Color.DimGray;
            this.lblIDnoValue.Name = "lblIDnoValue";
            this.lblIDnoValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblIDnoValue.StylePriority.UseBackColor = false;
            this.lblIDnoValue.StylePriority.UseBorderColor = false;
            this.lblIDnoValue.StylePriority.UseBorders = false;
            this.lblIDnoValue.StylePriority.UseFont = false;
            this.lblIDnoValue.StylePriority.UseForeColor = false;
            this.lblIDnoValue.StylePriority.UsePadding = false;
            this.lblIDnoValue.Weight = 0.89727268876814947;
            // 
            // lblPPNo
            // 
            this.lblPPNo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblPPNo.BorderColor = System.Drawing.Color.White;
            this.lblPPNo.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblPPNo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPPNo.ForeColor = System.Drawing.Color.Gray;
            this.lblPPNo.Name = "lblPPNo";
            this.lblPPNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblPPNo.StylePriority.UseBackColor = false;
            this.lblPPNo.StylePriority.UseBorderColor = false;
            this.lblPPNo.StylePriority.UseBorders = false;
            this.lblPPNo.StylePriority.UseFont = false;
            this.lblPPNo.StylePriority.UseForeColor = false;
            this.lblPPNo.StylePriority.UsePadding = false;
            this.lblPPNo.Text = "Passport or 2nd ID No.";
            this.lblPPNo.Weight = 0.59803108681621631;
            // 
            // lblPassportnoValue
            // 
            this.lblPassportnoValue.BackColor = System.Drawing.Color.White;
            this.lblPassportnoValue.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblPassportnoValue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblPassportnoValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.PassportNo")});
            this.lblPassportnoValue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblPassportnoValue.ForeColor = System.Drawing.Color.DimGray;
            this.lblPassportnoValue.Name = "lblPassportnoValue";
            this.lblPassportnoValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblPassportnoValue.StylePriority.UseBackColor = false;
            this.lblPassportnoValue.StylePriority.UseBorderColor = false;
            this.lblPassportnoValue.StylePriority.UseBorders = false;
            this.lblPassportnoValue.StylePriority.UseFont = false;
            this.lblPassportnoValue.StylePriority.UseForeColor = false;
            this.lblPassportnoValue.StylePriority.UsePadding = false;
            this.lblPassportnoValue.Weight = 0.89578124523999147;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSurname,
            this.lblSurnameValue,
            this.lblResAdd,
            this.lblResAddValue});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100F);
            this.xrTableRow3.StylePriority.UsePadding = false;
            this.xrTableRow3.Weight = 0.79999999999999993;
            // 
            // lblSurname
            // 
            this.lblSurname.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblSurname.BorderColor = System.Drawing.Color.White;
            this.lblSurname.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSurname.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSurname.ForeColor = System.Drawing.Color.Gray;
            this.lblSurname.Name = "lblSurname";
            this.lblSurname.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblSurname.StylePriority.UseBackColor = false;
            this.lblSurname.StylePriority.UseBorderColor = false;
            this.lblSurname.StylePriority.UseBorders = false;
            this.lblSurname.StylePriority.UseFont = false;
            this.lblSurname.StylePriority.UseForeColor = false;
            this.lblSurname.StylePriority.UsePadding = false;
            this.lblSurname.Text = "Surname";
            this.lblSurname.Weight = 0.59755134281200639;
            // 
            // lblSurnameValue
            // 
            this.lblSurnameValue.BackColor = System.Drawing.Color.White;
            this.lblSurnameValue.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblSurnameValue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblSurnameValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.Surname")});
            this.lblSurnameValue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblSurnameValue.ForeColor = System.Drawing.Color.DimGray;
            this.lblSurnameValue.Name = "lblSurnameValue";
            this.lblSurnameValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblSurnameValue.StylePriority.UseBackColor = false;
            this.lblSurnameValue.StylePriority.UseBorderColor = false;
            this.lblSurnameValue.StylePriority.UseBorders = false;
            this.lblSurnameValue.StylePriority.UseFont = false;
            this.lblSurnameValue.StylePriority.UseForeColor = false;
            this.lblSurnameValue.StylePriority.UsePadding = false;
            this.lblSurnameValue.Weight = 0.89727268876814947;
            // 
            // lblResAdd
            // 
            this.lblResAdd.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblResAdd.BorderColor = System.Drawing.Color.White;
            this.lblResAdd.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblResAdd.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResAdd.ForeColor = System.Drawing.Color.Gray;
            this.lblResAdd.Name = "lblResAdd";
            this.lblResAdd.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblResAdd.StylePriority.UseBackColor = false;
            this.lblResAdd.StylePriority.UseBorderColor = false;
            this.lblResAdd.StylePriority.UseBorders = false;
            this.lblResAdd.StylePriority.UseFont = false;
            this.lblResAdd.StylePriority.UseForeColor = false;
            this.lblResAdd.StylePriority.UsePadding = false;
            this.lblResAdd.Text = "Residential Address";
            this.lblResAdd.Weight = 0.59803108681621631;
            // 
            // lblResAddValue
            // 
            this.lblResAddValue.BackColor = System.Drawing.Color.White;
            this.lblResAddValue.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblResAddValue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblResAddValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.ResidentialAddress")});
            this.lblResAddValue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblResAddValue.ForeColor = System.Drawing.Color.DimGray;
            this.lblResAddValue.Name = "lblResAddValue";
            this.lblResAddValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblResAddValue.StylePriority.UseBackColor = false;
            this.lblResAddValue.StylePriority.UseBorderColor = false;
            this.lblResAddValue.StylePriority.UseBorders = false;
            this.lblResAddValue.StylePriority.UseFont = false;
            this.lblResAddValue.StylePriority.UseForeColor = false;
            this.lblResAddValue.StylePriority.UsePadding = false;
            this.lblResAddValue.Text = "lblResAddValue";
            this.lblResAddValue.Weight = 0.89578124523999147;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblFirstname,
            this.lblFirstnameValue,
            this.lblPostalAdd,
            this.lblPostalAddValue});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100F);
            this.xrTableRow13.StylePriority.UsePadding = false;
            this.xrTableRow13.Weight = 0.8;
            // 
            // lblFirstname
            // 
            this.lblFirstname.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblFirstname.BorderColor = System.Drawing.Color.White;
            this.lblFirstname.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblFirstname.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFirstname.ForeColor = System.Drawing.Color.Gray;
            this.lblFirstname.Name = "lblFirstname";
            this.lblFirstname.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblFirstname.StylePriority.UseBackColor = false;
            this.lblFirstname.StylePriority.UseBorderColor = false;
            this.lblFirstname.StylePriority.UseBorders = false;
            this.lblFirstname.StylePriority.UseFont = false;
            this.lblFirstname.StylePriority.UseForeColor = false;
            this.lblFirstname.StylePriority.UsePadding = false;
            this.lblFirstname.Text = "First Name";
            this.lblFirstname.Weight = 0.59755134281200639;
            // 
            // lblFirstnameValue
            // 
            this.lblFirstnameValue.BackColor = System.Drawing.Color.White;
            this.lblFirstnameValue.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblFirstnameValue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblFirstnameValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.FirstName")});
            this.lblFirstnameValue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblFirstnameValue.ForeColor = System.Drawing.Color.DimGray;
            this.lblFirstnameValue.Name = "lblFirstnameValue";
            this.lblFirstnameValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblFirstnameValue.StylePriority.UseBackColor = false;
            this.lblFirstnameValue.StylePriority.UseBorderColor = false;
            this.lblFirstnameValue.StylePriority.UseBorders = false;
            this.lblFirstnameValue.StylePriority.UseFont = false;
            this.lblFirstnameValue.StylePriority.UseForeColor = false;
            this.lblFirstnameValue.StylePriority.UsePadding = false;
            this.lblFirstnameValue.Weight = 0.89727268876814947;
            // 
            // lblPostalAdd
            // 
            this.lblPostalAdd.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblPostalAdd.BorderColor = System.Drawing.Color.White;
            this.lblPostalAdd.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblPostalAdd.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPostalAdd.ForeColor = System.Drawing.Color.Gray;
            this.lblPostalAdd.Name = "lblPostalAdd";
            this.lblPostalAdd.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblPostalAdd.StylePriority.UseBackColor = false;
            this.lblPostalAdd.StylePriority.UseBorderColor = false;
            this.lblPostalAdd.StylePriority.UseBorders = false;
            this.lblPostalAdd.StylePriority.UseFont = false;
            this.lblPostalAdd.StylePriority.UseForeColor = false;
            this.lblPostalAdd.StylePriority.UsePadding = false;
            this.lblPostalAdd.Text = "Postal Address";
            this.lblPostalAdd.Weight = 0.59803108681621631;
            // 
            // lblPostalAddValue
            // 
            this.lblPostalAddValue.BackColor = System.Drawing.Color.White;
            this.lblPostalAddValue.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblPostalAddValue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblPostalAddValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.PostalAddress")});
            this.lblPostalAddValue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblPostalAddValue.ForeColor = System.Drawing.Color.DimGray;
            this.lblPostalAddValue.Name = "lblPostalAddValue";
            this.lblPostalAddValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblPostalAddValue.StylePriority.UseBackColor = false;
            this.lblPostalAddValue.StylePriority.UseBorderColor = false;
            this.lblPostalAddValue.StylePriority.UseBorders = false;
            this.lblPostalAddValue.StylePriority.UseFont = false;
            this.lblPostalAddValue.StylePriority.UseForeColor = false;
            this.lblPostalAddValue.StylePriority.UsePadding = false;
            this.lblPostalAddValue.Text = "lblPostalAddValue";
            this.lblPostalAddValue.Weight = 0.89578124523999147;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSecondName,
            this.lblSecondnameValue,
            this.lblHomeTelNo,
            this.lblHomeTelNoValue});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100F);
            this.xrTableRow10.StylePriority.UsePadding = false;
            this.xrTableRow10.Weight = 0.8;
            // 
            // lblSecondName
            // 
            this.lblSecondName.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblSecondName.BorderColor = System.Drawing.Color.White;
            this.lblSecondName.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSecondName.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSecondName.ForeColor = System.Drawing.Color.Gray;
            this.lblSecondName.Name = "lblSecondName";
            this.lblSecondName.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblSecondName.StylePriority.UseBackColor = false;
            this.lblSecondName.StylePriority.UseBorderColor = false;
            this.lblSecondName.StylePriority.UseBorders = false;
            this.lblSecondName.StylePriority.UseFont = false;
            this.lblSecondName.StylePriority.UseForeColor = false;
            this.lblSecondName.StylePriority.UsePadding = false;
            this.lblSecondName.Text = "Second Name";
            this.lblSecondName.Weight = 0.59755134281200639;
            // 
            // lblSecondnameValue
            // 
            this.lblSecondnameValue.BackColor = System.Drawing.Color.White;
            this.lblSecondnameValue.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblSecondnameValue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblSecondnameValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.SecondName")});
            this.lblSecondnameValue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblSecondnameValue.ForeColor = System.Drawing.Color.DimGray;
            this.lblSecondnameValue.Name = "lblSecondnameValue";
            this.lblSecondnameValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblSecondnameValue.StylePriority.UseBackColor = false;
            this.lblSecondnameValue.StylePriority.UseBorderColor = false;
            this.lblSecondnameValue.StylePriority.UseBorders = false;
            this.lblSecondnameValue.StylePriority.UseFont = false;
            this.lblSecondnameValue.StylePriority.UseForeColor = false;
            this.lblSecondnameValue.StylePriority.UsePadding = false;
            this.lblSecondnameValue.Weight = 0.89727268876814947;
            // 
            // lblHomeTelNo
            // 
            this.lblHomeTelNo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblHomeTelNo.BorderColor = System.Drawing.Color.White;
            this.lblHomeTelNo.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblHomeTelNo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHomeTelNo.ForeColor = System.Drawing.Color.Gray;
            this.lblHomeTelNo.Name = "lblHomeTelNo";
            this.lblHomeTelNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblHomeTelNo.StylePriority.UseBackColor = false;
            this.lblHomeTelNo.StylePriority.UseBorderColor = false;
            this.lblHomeTelNo.StylePriority.UseBorders = false;
            this.lblHomeTelNo.StylePriority.UseFont = false;
            this.lblHomeTelNo.StylePriority.UseForeColor = false;
            this.lblHomeTelNo.StylePriority.UsePadding = false;
            this.lblHomeTelNo.Text = "Telephone No. (H)";
            this.lblHomeTelNo.Weight = 0.59803108681621631;
            // 
            // lblHomeTelNoValue
            // 
            this.lblHomeTelNoValue.BackColor = System.Drawing.Color.White;
            this.lblHomeTelNoValue.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblHomeTelNoValue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblHomeTelNoValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.HomeTelephoneNo")});
            this.lblHomeTelNoValue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblHomeTelNoValue.ForeColor = System.Drawing.Color.DimGray;
            this.lblHomeTelNoValue.Name = "lblHomeTelNoValue";
            this.lblHomeTelNoValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblHomeTelNoValue.StylePriority.UseBackColor = false;
            this.lblHomeTelNoValue.StylePriority.UseBorderColor = false;
            this.lblHomeTelNoValue.StylePriority.UseBorders = false;
            this.lblHomeTelNoValue.StylePriority.UseFont = false;
            this.lblHomeTelNoValue.StylePriority.UseForeColor = false;
            this.lblHomeTelNoValue.StylePriority.UsePadding = false;
            this.lblHomeTelNoValue.Text = "lblHomeTelNoValue";
            this.lblHomeTelNoValue.Weight = 0.89578124523999147;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblTitle,
            this.lbltitlevalue,
            this.lblWorkTelno,
            this.lblWorkTelnoValue});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100F);
            this.xrTableRow9.StylePriority.UsePadding = false;
            this.xrTableRow9.Weight = 0.8;
            // 
            // lblTitle
            // 
            this.lblTitle.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblTitle.BorderColor = System.Drawing.Color.White;
            this.lblTitle.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTitle.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.Gray;
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblTitle.StylePriority.UseBackColor = false;
            this.lblTitle.StylePriority.UseBorderColor = false;
            this.lblTitle.StylePriority.UseBorders = false;
            this.lblTitle.StylePriority.UseFont = false;
            this.lblTitle.StylePriority.UseForeColor = false;
            this.lblTitle.StylePriority.UsePadding = false;
            this.lblTitle.Text = "Title";
            this.lblTitle.Weight = 0.59755134281200639;
            // 
            // lbltitlevalue
            // 
            this.lbltitlevalue.BackColor = System.Drawing.Color.White;
            this.lbltitlevalue.BorderColor = System.Drawing.Color.Gainsboro;
            this.lbltitlevalue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbltitlevalue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.TitleDesc")});
            this.lbltitlevalue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbltitlevalue.ForeColor = System.Drawing.Color.DimGray;
            this.lbltitlevalue.Name = "lbltitlevalue";
            this.lbltitlevalue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lbltitlevalue.StylePriority.UseBackColor = false;
            this.lbltitlevalue.StylePriority.UseBorderColor = false;
            this.lbltitlevalue.StylePriority.UseBorders = false;
            this.lbltitlevalue.StylePriority.UseFont = false;
            this.lbltitlevalue.StylePriority.UseForeColor = false;
            this.lbltitlevalue.StylePriority.UsePadding = false;
            this.lbltitlevalue.Weight = 0.89727268876814947;
            // 
            // lblWorkTelno
            // 
            this.lblWorkTelno.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblWorkTelno.BorderColor = System.Drawing.Color.White;
            this.lblWorkTelno.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblWorkTelno.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWorkTelno.ForeColor = System.Drawing.Color.Gray;
            this.lblWorkTelno.Name = "lblWorkTelno";
            this.lblWorkTelno.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblWorkTelno.StylePriority.UseBackColor = false;
            this.lblWorkTelno.StylePriority.UseBorderColor = false;
            this.lblWorkTelno.StylePriority.UseBorders = false;
            this.lblWorkTelno.StylePriority.UseFont = false;
            this.lblWorkTelno.StylePriority.UseForeColor = false;
            this.lblWorkTelno.StylePriority.UsePadding = false;
            this.lblWorkTelno.Text = "Telephone No. (W)";
            this.lblWorkTelno.Weight = 0.59803108681621631;
            // 
            // lblWorkTelnoValue
            // 
            this.lblWorkTelnoValue.BackColor = System.Drawing.Color.White;
            this.lblWorkTelnoValue.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblWorkTelnoValue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblWorkTelnoValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.WorkTelephoneNo")});
            this.lblWorkTelnoValue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblWorkTelnoValue.ForeColor = System.Drawing.Color.DimGray;
            this.lblWorkTelnoValue.Name = "lblWorkTelnoValue";
            this.lblWorkTelnoValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblWorkTelnoValue.StylePriority.UseBackColor = false;
            this.lblWorkTelnoValue.StylePriority.UseBorderColor = false;
            this.lblWorkTelnoValue.StylePriority.UseBorders = false;
            this.lblWorkTelnoValue.StylePriority.UseFont = false;
            this.lblWorkTelnoValue.StylePriority.UseForeColor = false;
            this.lblWorkTelnoValue.StylePriority.UsePadding = false;
            this.lblWorkTelnoValue.Text = "lblWorkTelnoValue";
            this.lblWorkTelnoValue.Weight = 0.89578124523999147;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblGender,
            this.lblGenderValue,
            this.lblMobileNo,
            this.lblMobileNoValue});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100F);
            this.xrTableRow8.StylePriority.UsePadding = false;
            this.xrTableRow8.Weight = 0.80000000000000027;
            // 
            // lblGender
            // 
            this.lblGender.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblGender.BorderColor = System.Drawing.Color.White;
            this.lblGender.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblGender.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGender.ForeColor = System.Drawing.Color.Gray;
            this.lblGender.Name = "lblGender";
            this.lblGender.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblGender.StylePriority.UseBackColor = false;
            this.lblGender.StylePriority.UseBorderColor = false;
            this.lblGender.StylePriority.UseBorders = false;
            this.lblGender.StylePriority.UseFont = false;
            this.lblGender.StylePriority.UseForeColor = false;
            this.lblGender.StylePriority.UsePadding = false;
            this.lblGender.Text = "Gender";
            this.lblGender.Weight = 0.59755134281200639;
            // 
            // lblGenderValue
            // 
            this.lblGenderValue.BackColor = System.Drawing.Color.White;
            this.lblGenderValue.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblGenderValue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblGenderValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.Gender")});
            this.lblGenderValue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblGenderValue.ForeColor = System.Drawing.Color.DimGray;
            this.lblGenderValue.Name = "lblGenderValue";
            this.lblGenderValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblGenderValue.StylePriority.UseBackColor = false;
            this.lblGenderValue.StylePriority.UseBorderColor = false;
            this.lblGenderValue.StylePriority.UseBorders = false;
            this.lblGenderValue.StylePriority.UseFont = false;
            this.lblGenderValue.StylePriority.UseForeColor = false;
            this.lblGenderValue.StylePriority.UsePadding = false;
            this.lblGenderValue.Weight = 0.89727268876814947;
            // 
            // lblMobileNo
            // 
            this.lblMobileNo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblMobileNo.BorderColor = System.Drawing.Color.White;
            this.lblMobileNo.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMobileNo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMobileNo.ForeColor = System.Drawing.Color.Gray;
            this.lblMobileNo.Name = "lblMobileNo";
            this.lblMobileNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblMobileNo.StylePriority.UseBackColor = false;
            this.lblMobileNo.StylePriority.UseBorderColor = false;
            this.lblMobileNo.StylePriority.UseBorders = false;
            this.lblMobileNo.StylePriority.UseFont = false;
            this.lblMobileNo.StylePriority.UseForeColor = false;
            this.lblMobileNo.StylePriority.UsePadding = false;
            this.lblMobileNo.Text = "Cellular/Mobile";
            this.lblMobileNo.Weight = 0.59803108681621631;
            // 
            // lblMobileNoValue
            // 
            this.lblMobileNoValue.BackColor = System.Drawing.Color.White;
            this.lblMobileNoValue.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblMobileNoValue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblMobileNoValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.CellularNo")});
            this.lblMobileNoValue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblMobileNoValue.ForeColor = System.Drawing.Color.DimGray;
            this.lblMobileNoValue.Name = "lblMobileNoValue";
            this.lblMobileNoValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblMobileNoValue.StylePriority.UseBackColor = false;
            this.lblMobileNoValue.StylePriority.UseBorderColor = false;
            this.lblMobileNoValue.StylePriority.UseBorders = false;
            this.lblMobileNoValue.StylePriority.UseFont = false;
            this.lblMobileNoValue.StylePriority.UseForeColor = false;
            this.lblMobileNoValue.StylePriority.UsePadding = false;
            this.lblMobileNoValue.Text = "lblMobileNoValue";
            this.lblMobileNoValue.Weight = 0.89578124523999147;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDOB,
            this.lblDOBValue,
            this.lblEmailAddress,
            this.lblEmailAddressValue});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100F);
            this.xrTableRow7.StylePriority.UsePadding = false;
            this.xrTableRow7.Weight = 0.79999999999999982;
            // 
            // lblDOB
            // 
            this.lblDOB.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblDOB.BorderColor = System.Drawing.Color.White;
            this.lblDOB.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDOB.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDOB.ForeColor = System.Drawing.Color.Gray;
            this.lblDOB.Name = "lblDOB";
            this.lblDOB.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDOB.StylePriority.UseBackColor = false;
            this.lblDOB.StylePriority.UseBorderColor = false;
            this.lblDOB.StylePriority.UseBorders = false;
            this.lblDOB.StylePriority.UseFont = false;
            this.lblDOB.StylePriority.UseForeColor = false;
            this.lblDOB.StylePriority.UsePadding = false;
            this.lblDOB.Text = "Date of Birth";
            this.lblDOB.Weight = 0.59755134281200639;
            // 
            // lblDOBValue
            // 
            this.lblDOBValue.BackColor = System.Drawing.Color.White;
            this.lblDOBValue.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblDOBValue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDOBValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.BirthDate")});
            this.lblDOBValue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDOBValue.ForeColor = System.Drawing.Color.DimGray;
            this.lblDOBValue.Name = "lblDOBValue";
            this.lblDOBValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDOBValue.StylePriority.UseBackColor = false;
            this.lblDOBValue.StylePriority.UseBorderColor = false;
            this.lblDOBValue.StylePriority.UseBorders = false;
            this.lblDOBValue.StylePriority.UseFont = false;
            this.lblDOBValue.StylePriority.UseForeColor = false;
            this.lblDOBValue.StylePriority.UsePadding = false;
            this.lblDOBValue.Weight = 0.89727268876814947;
            // 
            // lblEmailAddress
            // 
            this.lblEmailAddress.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblEmailAddress.BorderColor = System.Drawing.Color.White;
            this.lblEmailAddress.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblEmailAddress.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmailAddress.ForeColor = System.Drawing.Color.Gray;
            this.lblEmailAddress.Name = "lblEmailAddress";
            this.lblEmailAddress.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblEmailAddress.StylePriority.UseBackColor = false;
            this.lblEmailAddress.StylePriority.UseBorderColor = false;
            this.lblEmailAddress.StylePriority.UseBorders = false;
            this.lblEmailAddress.StylePriority.UseFont = false;
            this.lblEmailAddress.StylePriority.UseForeColor = false;
            this.lblEmailAddress.StylePriority.UsePadding = false;
            this.lblEmailAddress.Text = "E-mail Address";
            this.lblEmailAddress.Weight = 0.59803108681621631;
            // 
            // lblEmailAddressValue
            // 
            this.lblEmailAddressValue.BackColor = System.Drawing.Color.White;
            this.lblEmailAddressValue.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblEmailAddressValue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblEmailAddressValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.EmailAddress")});
            this.lblEmailAddressValue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblEmailAddressValue.ForeColor = System.Drawing.Color.DimGray;
            this.lblEmailAddressValue.Name = "lblEmailAddressValue";
            this.lblEmailAddressValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblEmailAddressValue.StylePriority.UseBackColor = false;
            this.lblEmailAddressValue.StylePriority.UseBorderColor = false;
            this.lblEmailAddressValue.StylePriority.UseBorders = false;
            this.lblEmailAddressValue.StylePriority.UseFont = false;
            this.lblEmailAddressValue.StylePriority.UseForeColor = false;
            this.lblEmailAddressValue.StylePriority.UsePadding = false;
            this.lblEmailAddressValue.Weight = 0.89578124523999147;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblmaritalStatus,
            this.lblmaritalStatusValue,
            this.lblCurrentEmployer,
            this.lblCurrentEmployerValue});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100F);
            this.xrTableRow6.StylePriority.UsePadding = false;
            this.xrTableRow6.Weight = 0.8;
            // 
            // lblmaritalStatus
            // 
            this.lblmaritalStatus.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblmaritalStatus.BorderColor = System.Drawing.Color.White;
            this.lblmaritalStatus.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblmaritalStatus.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmaritalStatus.ForeColor = System.Drawing.Color.Gray;
            this.lblmaritalStatus.Name = "lblmaritalStatus";
            this.lblmaritalStatus.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblmaritalStatus.StylePriority.UseBackColor = false;
            this.lblmaritalStatus.StylePriority.UseBorderColor = false;
            this.lblmaritalStatus.StylePriority.UseBorders = false;
            this.lblmaritalStatus.StylePriority.UseFont = false;
            this.lblmaritalStatus.StylePriority.UseForeColor = false;
            this.lblmaritalStatus.StylePriority.UsePadding = false;
            this.lblmaritalStatus.Text = "Marital Status";
            this.lblmaritalStatus.Weight = 0.59755134281200639;
            // 
            // lblmaritalStatusValue
            // 
            this.lblmaritalStatusValue.BackColor = System.Drawing.Color.White;
            this.lblmaritalStatusValue.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblmaritalStatusValue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblmaritalStatusValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.MaritalStatusDesc")});
            this.lblmaritalStatusValue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblmaritalStatusValue.ForeColor = System.Drawing.Color.DimGray;
            this.lblmaritalStatusValue.Name = "lblmaritalStatusValue";
            this.lblmaritalStatusValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblmaritalStatusValue.StylePriority.UseBackColor = false;
            this.lblmaritalStatusValue.StylePriority.UseBorderColor = false;
            this.lblmaritalStatusValue.StylePriority.UseBorders = false;
            this.lblmaritalStatusValue.StylePriority.UseFont = false;
            this.lblmaritalStatusValue.StylePriority.UseForeColor = false;
            this.lblmaritalStatusValue.StylePriority.UsePadding = false;
            this.lblmaritalStatusValue.Text = "lblmaritalStatusValue";
            this.lblmaritalStatusValue.Weight = 0.89727268876814947;
            // 
            // lblCurrentEmployer
            // 
            this.lblCurrentEmployer.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblCurrentEmployer.BorderColor = System.Drawing.Color.White;
            this.lblCurrentEmployer.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblCurrentEmployer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentEmployer.ForeColor = System.Drawing.Color.Gray;
            this.lblCurrentEmployer.Name = "lblCurrentEmployer";
            this.lblCurrentEmployer.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblCurrentEmployer.StylePriority.UseBackColor = false;
            this.lblCurrentEmployer.StylePriority.UseBorderColor = false;
            this.lblCurrentEmployer.StylePriority.UseBorders = false;
            this.lblCurrentEmployer.StylePriority.UseFont = false;
            this.lblCurrentEmployer.StylePriority.UseForeColor = false;
            this.lblCurrentEmployer.StylePriority.UsePadding = false;
            this.lblCurrentEmployer.Text = "Current Employer";
            this.lblCurrentEmployer.Weight = 0.59803108681621631;
            // 
            // lblCurrentEmployerValue
            // 
            this.lblCurrentEmployerValue.BackColor = System.Drawing.Color.White;
            this.lblCurrentEmployerValue.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblCurrentEmployerValue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblCurrentEmployerValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.EmployerDetail")});
            this.lblCurrentEmployerValue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblCurrentEmployerValue.ForeColor = System.Drawing.Color.DimGray;
            this.lblCurrentEmployerValue.Name = "lblCurrentEmployerValue";
            this.lblCurrentEmployerValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblCurrentEmployerValue.StylePriority.UseBackColor = false;
            this.lblCurrentEmployerValue.StylePriority.UseBorderColor = false;
            this.lblCurrentEmployerValue.StylePriority.UseBorders = false;
            this.lblCurrentEmployerValue.StylePriority.UseFont = false;
            this.lblCurrentEmployerValue.StylePriority.UseForeColor = false;
            this.lblCurrentEmployerValue.StylePriority.UsePadding = false;
            this.lblCurrentEmployerValue.Text = "lblCurrentEmployerValue";
            this.lblCurrentEmployerValue.Weight = 0.89578124523999147;
            // 
            // GroupHeader6
            // 
            this.GroupHeader6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblPersonalDetail});
            this.GroupHeader6.HeightF = 45F;
            this.GroupHeader6.Name = "GroupHeader6";
            // 
            // tblPersonalDetail
            // 
            this.tblPersonalDetail.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.tblPersonalDetail.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblPersonalDetail.LocationFloat = new DevExpress.Utils.PointFloat(8F, 8F);
            this.tblPersonalDetail.Name = "tblPersonalDetail";
            this.tblPersonalDetail.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow12});
            this.tblPersonalDetail.SizeF = new System.Drawing.SizeF(792F, 33F);
            this.tblPersonalDetail.StylePriority.UseBorders = false;
            this.tblPersonalDetail.StylePriority.UseFont = false;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblPersonalDetail});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 1;
            // 
            // lblPersonalDetail
            // 
            this.lblPersonalDetail.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.lblPersonalDetail.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblPersonalDetail.BorderWidth = 2;
            this.lblPersonalDetail.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblPersonalDetail.ForeColor = System.Drawing.Color.Gray;
            this.lblPersonalDetail.Name = "lblPersonalDetail";
            this.lblPersonalDetail.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblPersonalDetail.StylePriority.UseBorderColor = false;
            this.lblPersonalDetail.StylePriority.UseBorders = false;
            this.lblPersonalDetail.StylePriority.UseBorderWidth = false;
            this.lblPersonalDetail.StylePriority.UseFont = false;
            this.lblPersonalDetail.StylePriority.UseForeColor = false;
            this.lblPersonalDetail.StylePriority.UsePadding = false;
            this.lblPersonalDetail.StylePriority.UseTextAlignment = false;
            this.lblPersonalDetail.Text = "Personal Details Summary";
            this.lblPersonalDetail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblPersonalDetail.Weight = 3;
            // 
            // Scoring
            // 
            this.Scoring.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailScoring,
            this.GroupHeader3});
            this.Scoring.DataMember = "ConsumerScoring";
            this.Scoring.Level = 2;
            this.Scoring.Name = "Scoring";
            // 
            // DetailScoring
            // 
            this.DetailScoring.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblscrorevalues});
            this.DetailScoring.HeightF = 25F;
            this.DetailScoring.KeepTogether = true;
            this.DetailScoring.Name = "DetailScoring";
            // 
            // tblscrorevalues
            // 
            this.tblscrorevalues.BorderColor = System.Drawing.Color.White;
            this.tblscrorevalues.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblscrorevalues.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblscrorevalues.LocationFloat = new DevExpress.Utils.PointFloat(8F, 0F);
            this.tblscrorevalues.Name = "tblscrorevalues";
            this.tblscrorevalues.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblscrorevalues.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow22});
            this.tblscrorevalues.SizeF = new System.Drawing.SizeF(792F, 25F);
            this.tblscrorevalues.StylePriority.UseBorderColor = false;
            this.tblscrorevalues.StylePriority.UseBorders = false;
            this.tblscrorevalues.StylePriority.UseFont = false;
            this.tblscrorevalues.StylePriority.UsePadding = false;
            this.tblscrorevalues.StylePriority.UseTextAlignment = false;
            this.tblscrorevalues.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.tblscrorevalues.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.tblscrorevalues_BeforePrint);
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblClassification,
            this.lblDescription});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 1;
            // 
            // lblClassification
            // 
            this.lblClassification.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblClassification.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblClassification.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerScoring.Classification")});
            this.lblClassification.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClassification.ForeColor = System.Drawing.Color.Gray;
            this.lblClassification.Name = "lblClassification";
            this.lblClassification.StylePriority.UseBackColor = false;
            this.lblClassification.StylePriority.UseBorders = false;
            this.lblClassification.StylePriority.UseFont = false;
            this.lblClassification.StylePriority.UseForeColor = false;
            this.lblClassification.Text = "lblClassification";
            this.lblClassification.Weight = 1.5012337662337665;
            // 
            // lblDescription
            // 
            this.lblDescription.BackColor = System.Drawing.Color.White;
            this.lblDescription.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblDescription.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDescription.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerScoring.Description")});
            this.lblDescription.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescription.ForeColor = System.Drawing.Color.DimGray;
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.StylePriority.UseBackColor = false;
            this.lblDescription.StylePriority.UseBorderColor = false;
            this.lblDescription.StylePriority.UseBorders = false;
            this.lblDescription.StylePriority.UseFont = false;
            this.lblDescription.StylePriority.UseForeColor = false;
            this.lblDescription.Text = "lblDescription";
            this.lblDescription.Weight = 1.498766233766234;
            // 
            // GroupHeader3
            // 
            this.GroupHeader3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblScore});
            this.GroupHeader3.HeightF = 45F;
            this.GroupHeader3.Name = "GroupHeader3";
            // 
            // tblScore
            // 
            this.tblScore.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblScore.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblScore.LocationFloat = new DevExpress.Utils.PointFloat(8F, 8F);
            this.tblScore.Name = "tblScore";
            this.tblScore.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.tblScore.SizeF = new System.Drawing.SizeF(792F, 33F);
            this.tblScore.StylePriority.UseBorders = false;
            this.tblScore.StylePriority.UseFont = false;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblScore});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1;
            // 
            // lblScore
            // 
            this.lblScore.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.lblScore.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblScore.BorderWidth = 2;
            this.lblScore.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblScore.ForeColor = System.Drawing.Color.Gray;
            this.lblScore.Name = "lblScore";
            this.lblScore.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblScore.StylePriority.UseBorderColor = false;
            this.lblScore.StylePriority.UseBorders = false;
            this.lblScore.StylePriority.UseBorderWidth = false;
            this.lblScore.StylePriority.UseFont = false;
            this.lblScore.StylePriority.UseForeColor = false;
            this.lblScore.StylePriority.UsePadding = false;
            this.lblScore.StylePriority.UseTextAlignment = false;
            this.lblScore.Text = "Score Summary Indicator";
            this.lblScore.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblScore.Weight = 3;
            // 
            // DebtSummary
            // 
            this.DebtSummary.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailDebtSummary,
            this.GroupHeader4});
            this.DebtSummary.DataMember = "ConsumerDebtSummary";
            this.DebtSummary.Level = 3;
            this.DebtSummary.Name = "DebtSummary";
            // 
            // DetailDebtSummary
            // 
            this.DetailDebtSummary.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblDebtSummary});
            this.DetailDebtSummary.HeightF = 240F;
            this.DetailDebtSummary.KeepTogether = true;
            this.DetailDebtSummary.KeepTogetherWithDetailReports = true;
            this.DetailDebtSummary.Name = "DetailDebtSummary";
            // 
            // tblDebtSummary
            // 
            this.tblDebtSummary.BorderColor = System.Drawing.Color.White;
            this.tblDebtSummary.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblDebtSummary.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblDebtSummary.LocationFloat = new DevExpress.Utils.PointFloat(8F, 0F);
            this.tblDebtSummary.Name = "tblDebtSummary";
            this.tblDebtSummary.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblDebtSummary.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow24,
            this.xrTableRow36,
            this.xrTableRow35,
            this.xrTableRow34,
            this.xrTableRow33,
            this.xrTableRow32,
            this.xrTableRow31,
            this.xrTableRow30,
            this.xrTableRow29,
            this.xrTableRow28,
            this.xrTableRow27,
            this.xrTableRow26});
            this.tblDebtSummary.SizeF = new System.Drawing.SizeF(792F, 240F);
            this.tblDebtSummary.StylePriority.UseBorderColor = false;
            this.tblDebtSummary.StylePriority.UseBorders = false;
            this.tblDebtSummary.StylePriority.UseFont = false;
            this.tblDebtSummary.StylePriority.UsePadding = false;
            this.tblDebtSummary.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.tblDebtSummary_BeforePrint);
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblActiveAcc,
            this.lblActiveAccValue});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100F);
            this.xrTableRow24.StylePriority.UsePadding = false;
            this.xrTableRow24.Weight = 0.8;
            // 
            // lblActiveAcc
            // 
            this.lblActiveAcc.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblActiveAcc.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblActiveAcc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblActiveAcc.ForeColor = System.Drawing.Color.Gray;
            this.lblActiveAcc.Name = "lblActiveAcc";
            this.lblActiveAcc.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblActiveAcc.StylePriority.UseBackColor = false;
            this.lblActiveAcc.StylePriority.UseBorders = false;
            this.lblActiveAcc.StylePriority.UseFont = false;
            this.lblActiveAcc.StylePriority.UseForeColor = false;
            this.lblActiveAcc.StylePriority.UsePadding = false;
            this.lblActiveAcc.StylePriority.UseTextAlignment = false;
            this.lblActiveAcc.Text = "Total No. Of Active Accounts";
            this.lblActiveAcc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblActiveAcc.Weight = 1.5010822510822512;
            // 
            // lblActiveAccValue
            // 
            this.lblActiveAccValue.BackColor = System.Drawing.Color.White;
            this.lblActiveAccValue.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblActiveAccValue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblActiveAccValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.NoOFActiveAccounts")});
            this.lblActiveAccValue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblActiveAccValue.ForeColor = System.Drawing.Color.DimGray;
            this.lblActiveAccValue.Name = "lblActiveAccValue";
            this.lblActiveAccValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblActiveAccValue.StylePriority.UseBackColor = false;
            this.lblActiveAccValue.StylePriority.UseBorderColor = false;
            this.lblActiveAccValue.StylePriority.UseBorders = false;
            this.lblActiveAccValue.StylePriority.UseFont = false;
            this.lblActiveAccValue.StylePriority.UseForeColor = false;
            this.lblActiveAccValue.StylePriority.UsePadding = false;
            this.lblActiveAccValue.StylePriority.UseTextAlignment = false;
            this.lblActiveAccValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblActiveAccValue.Weight = 1.4989177489177488;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblAccGood,
            this.lblAccGoodvalue});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100F);
            this.xrTableRow36.StylePriority.UsePadding = false;
            this.xrTableRow36.Weight = 0.79999999999999993;
            // 
            // lblAccGood
            // 
            this.lblAccGood.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblAccGood.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblAccGood.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAccGood.ForeColor = System.Drawing.Color.Gray;
            this.lblAccGood.Name = "lblAccGood";
            this.lblAccGood.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblAccGood.StylePriority.UseBackColor = false;
            this.lblAccGood.StylePriority.UseBorders = false;
            this.lblAccGood.StylePriority.UseFont = false;
            this.lblAccGood.StylePriority.UseForeColor = false;
            this.lblAccGood.StylePriority.UsePadding = false;
            this.lblAccGood.StylePriority.UseTextAlignment = false;
            this.lblAccGood.Text = "Total No. Of Accounts In Good Standing";
            this.lblAccGood.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAccGood.Weight = 1.5010822510822512;
            // 
            // lblAccGoodvalue
            // 
            this.lblAccGoodvalue.BackColor = System.Drawing.Color.White;
            this.lblAccGoodvalue.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblAccGoodvalue.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblAccGoodvalue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.NoOfAccountInGoodStanding")});
            this.lblAccGoodvalue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblAccGoodvalue.ForeColor = System.Drawing.Color.DimGray;
            this.lblAccGoodvalue.Name = "lblAccGoodvalue";
            this.lblAccGoodvalue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblAccGoodvalue.StylePriority.UseBackColor = false;
            this.lblAccGoodvalue.StylePriority.UseBorderColor = false;
            this.lblAccGoodvalue.StylePriority.UseBorders = false;
            this.lblAccGoodvalue.StylePriority.UseFont = false;
            this.lblAccGoodvalue.StylePriority.UseForeColor = false;
            this.lblAccGoodvalue.StylePriority.UsePadding = false;
            this.lblAccGoodvalue.StylePriority.UseTextAlignment = false;
            this.lblAccGoodvalue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAccGoodvalue.Weight = 1.4989177489177488;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDelinquentAcc,
            this.lblDelinquentAccValue});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100F);
            this.xrTableRow35.StylePriority.UsePadding = false;
            this.xrTableRow35.Weight = 0.79999999999999993;
            // 
            // lblDelinquentAcc
            // 
            this.lblDelinquentAcc.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblDelinquentAcc.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDelinquentAcc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDelinquentAcc.ForeColor = System.Drawing.Color.Gray;
            this.lblDelinquentAcc.Name = "lblDelinquentAcc";
            this.lblDelinquentAcc.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDelinquentAcc.StylePriority.UseBackColor = false;
            this.lblDelinquentAcc.StylePriority.UseBorders = false;
            this.lblDelinquentAcc.StylePriority.UseFont = false;
            this.lblDelinquentAcc.StylePriority.UseForeColor = false;
            this.lblDelinquentAcc.StylePriority.UsePadding = false;
            this.lblDelinquentAcc.StylePriority.UseTextAlignment = false;
            this.lblDelinquentAcc.Text = "Total No. Of Delinquent Accounts";
            this.lblDelinquentAcc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDelinquentAcc.Weight = 1.5010822510822512;
            // 
            // lblDelinquentAccValue
            // 
            this.lblDelinquentAccValue.BackColor = System.Drawing.Color.White;
            this.lblDelinquentAccValue.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblDelinquentAccValue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDelinquentAccValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.NoOfAccountInBadStanding")});
            this.lblDelinquentAccValue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDelinquentAccValue.ForeColor = System.Drawing.Color.DimGray;
            this.lblDelinquentAccValue.Name = "lblDelinquentAccValue";
            this.lblDelinquentAccValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDelinquentAccValue.StylePriority.UseBackColor = false;
            this.lblDelinquentAccValue.StylePriority.UseBorderColor = false;
            this.lblDelinquentAccValue.StylePriority.UseBorders = false;
            this.lblDelinquentAccValue.StylePriority.UseFont = false;
            this.lblDelinquentAccValue.StylePriority.UseForeColor = false;
            this.lblDelinquentAccValue.StylePriority.UsePadding = false;
            this.lblDelinquentAccValue.StylePriority.UseTextAlignment = false;
            this.lblDelinquentAccValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDelinquentAccValue.Weight = 1.4989177489177488;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblMonthlyinst,
            this.lblMonthlyinstValue});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100F);
            this.xrTableRow34.StylePriority.UsePadding = false;
            this.xrTableRow34.Weight = 0.8;
            // 
            // lblMonthlyinst
            // 
            this.lblMonthlyinst.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblMonthlyinst.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMonthlyinst.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMonthlyinst.ForeColor = System.Drawing.Color.Gray;
            this.lblMonthlyinst.Name = "lblMonthlyinst";
            this.lblMonthlyinst.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblMonthlyinst.StylePriority.UseBackColor = false;
            this.lblMonthlyinst.StylePriority.UseBorders = false;
            this.lblMonthlyinst.StylePriority.UseFont = false;
            this.lblMonthlyinst.StylePriority.UseForeColor = false;
            this.lblMonthlyinst.StylePriority.UsePadding = false;
            this.lblMonthlyinst.StylePriority.UseTextAlignment = false;
            this.lblMonthlyinst.Text = "Total Monthly Installments";
            this.lblMonthlyinst.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblMonthlyinst.Weight = 1.5010822510822512;
            // 
            // lblMonthlyinstValue
            // 
            this.lblMonthlyinstValue.BackColor = System.Drawing.Color.White;
            this.lblMonthlyinstValue.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblMonthlyinstValue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblMonthlyinstValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.TotalMonthlyInstallment")});
            this.lblMonthlyinstValue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblMonthlyinstValue.ForeColor = System.Drawing.Color.DimGray;
            this.lblMonthlyinstValue.Name = "lblMonthlyinstValue";
            this.lblMonthlyinstValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblMonthlyinstValue.StylePriority.UseBackColor = false;
            this.lblMonthlyinstValue.StylePriority.UseBorderColor = false;
            this.lblMonthlyinstValue.StylePriority.UseBorders = false;
            this.lblMonthlyinstValue.StylePriority.UseFont = false;
            this.lblMonthlyinstValue.StylePriority.UseForeColor = false;
            this.lblMonthlyinstValue.StylePriority.UsePadding = false;
            this.lblMonthlyinstValue.StylePriority.UseTextAlignment = false;
            this.lblMonthlyinstValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblMonthlyinstValue.Weight = 1.4989177489177488;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblOutstandingDebt,
            this.lblOutstandingDebtValue});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100F);
            this.xrTableRow33.StylePriority.UsePadding = false;
            this.xrTableRow33.Weight = 0.8;
            // 
            // lblOutstandingDebt
            // 
            this.lblOutstandingDebt.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblOutstandingDebt.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblOutstandingDebt.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOutstandingDebt.ForeColor = System.Drawing.Color.Gray;
            this.lblOutstandingDebt.Name = "lblOutstandingDebt";
            this.lblOutstandingDebt.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblOutstandingDebt.StylePriority.UseBackColor = false;
            this.lblOutstandingDebt.StylePriority.UseBorders = false;
            this.lblOutstandingDebt.StylePriority.UseFont = false;
            this.lblOutstandingDebt.StylePriority.UseForeColor = false;
            this.lblOutstandingDebt.StylePriority.UsePadding = false;
            this.lblOutstandingDebt.StylePriority.UseTextAlignment = false;
            this.lblOutstandingDebt.Text = "Total Outstanding Debt";
            this.lblOutstandingDebt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblOutstandingDebt.Weight = 1.5010822510822512;
            // 
            // lblOutstandingDebtValue
            // 
            this.lblOutstandingDebtValue.BackColor = System.Drawing.Color.White;
            this.lblOutstandingDebtValue.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblOutstandingDebtValue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblOutstandingDebtValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.TotalOutStandingDebt")});
            this.lblOutstandingDebtValue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblOutstandingDebtValue.ForeColor = System.Drawing.Color.DimGray;
            this.lblOutstandingDebtValue.Name = "lblOutstandingDebtValue";
            this.lblOutstandingDebtValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblOutstandingDebtValue.StylePriority.UseBackColor = false;
            this.lblOutstandingDebtValue.StylePriority.UseBorderColor = false;
            this.lblOutstandingDebtValue.StylePriority.UseBorders = false;
            this.lblOutstandingDebtValue.StylePriority.UseFont = false;
            this.lblOutstandingDebtValue.StylePriority.UseForeColor = false;
            this.lblOutstandingDebtValue.StylePriority.UsePadding = false;
            this.lblOutstandingDebtValue.StylePriority.UseTextAlignment = false;
            this.lblOutstandingDebtValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblOutstandingDebtValue.Weight = 1.4989177489177488;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblArrearamt,
            this.lblArrearamtvalue});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100F);
            this.xrTableRow32.StylePriority.UsePadding = false;
            this.xrTableRow32.Weight = 0.8;
            // 
            // lblArrearamt
            // 
            this.lblArrearamt.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblArrearamt.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblArrearamt.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblArrearamt.ForeColor = System.Drawing.Color.Gray;
            this.lblArrearamt.Name = "lblArrearamt";
            this.lblArrearamt.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblArrearamt.StylePriority.UseBackColor = false;
            this.lblArrearamt.StylePriority.UseBorders = false;
            this.lblArrearamt.StylePriority.UseFont = false;
            this.lblArrearamt.StylePriority.UseForeColor = false;
            this.lblArrearamt.StylePriority.UsePadding = false;
            this.lblArrearamt.StylePriority.UseTextAlignment = false;
            this.lblArrearamt.Text = "Total Arrear Amount";
            this.lblArrearamt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblArrearamt.Weight = 1.5010822510822512;
            // 
            // lblArrearamtvalue
            // 
            this.lblArrearamtvalue.BackColor = System.Drawing.Color.White;
            this.lblArrearamtvalue.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblArrearamtvalue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblArrearamtvalue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.TotalArrearAmount")});
            this.lblArrearamtvalue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblArrearamtvalue.ForeColor = System.Drawing.Color.DimGray;
            this.lblArrearamtvalue.Name = "lblArrearamtvalue";
            this.lblArrearamtvalue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblArrearamtvalue.StylePriority.UseBackColor = false;
            this.lblArrearamtvalue.StylePriority.UseBorderColor = false;
            this.lblArrearamtvalue.StylePriority.UseBorders = false;
            this.lblArrearamtvalue.StylePriority.UseFont = false;
            this.lblArrearamtvalue.StylePriority.UseForeColor = false;
            this.lblArrearamtvalue.StylePriority.UsePadding = false;
            this.lblArrearamtvalue.StylePriority.UseTextAlignment = false;
            this.lblArrearamtvalue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblArrearamtvalue.Weight = 1.4989177489177488;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblJdgDate,
            this.lblJdgDateValue});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100F);
            this.xrTableRow31.StylePriority.UsePadding = false;
            this.xrTableRow31.Weight = 0.80000000000000027;
            // 
            // lblJdgDate
            // 
            this.lblJdgDate.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblJdgDate.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblJdgDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJdgDate.ForeColor = System.Drawing.Color.Gray;
            this.lblJdgDate.Name = "lblJdgDate";
            this.lblJdgDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblJdgDate.StylePriority.UseBackColor = false;
            this.lblJdgDate.StylePriority.UseBorders = false;
            this.lblJdgDate.StylePriority.UseFont = false;
            this.lblJdgDate.StylePriority.UseForeColor = false;
            this.lblJdgDate.StylePriority.UsePadding = false;
            this.lblJdgDate.StylePriority.UseTextAlignment = false;
            this.lblJdgDate.Text = "Most Recent Judgement Date";
            this.lblJdgDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblJdgDate.Weight = 1.5010822510822512;
            // 
            // lblJdgDateValue
            // 
            this.lblJdgDateValue.BackColor = System.Drawing.Color.White;
            this.lblJdgDateValue.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblJdgDateValue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblJdgDateValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.MostRecentJudgmentDate")});
            this.lblJdgDateValue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblJdgDateValue.ForeColor = System.Drawing.Color.DimGray;
            this.lblJdgDateValue.Name = "lblJdgDateValue";
            this.lblJdgDateValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblJdgDateValue.StylePriority.UseBackColor = false;
            this.lblJdgDateValue.StylePriority.UseBorderColor = false;
            this.lblJdgDateValue.StylePriority.UseBorders = false;
            this.lblJdgDateValue.StylePriority.UseFont = false;
            this.lblJdgDateValue.StylePriority.UseForeColor = false;
            this.lblJdgDateValue.StylePriority.UsePadding = false;
            this.lblJdgDateValue.StylePriority.UseTextAlignment = false;
            this.lblJdgDateValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblJdgDateValue.Weight = 1.4989177489177488;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblJudAmt,
            this.lblJudAmtValue});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100F);
            this.xrTableRow30.StylePriority.UsePadding = false;
            this.xrTableRow30.Weight = 0.79999999999999982;
            // 
            // lblJudAmt
            // 
            this.lblJudAmt.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblJudAmt.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblJudAmt.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJudAmt.ForeColor = System.Drawing.Color.Gray;
            this.lblJudAmt.Name = "lblJudAmt";
            this.lblJudAmt.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblJudAmt.StylePriority.UseBackColor = false;
            this.lblJudAmt.StylePriority.UseBorders = false;
            this.lblJudAmt.StylePriority.UseFont = false;
            this.lblJudAmt.StylePriority.UseForeColor = false;
            this.lblJudAmt.StylePriority.UsePadding = false;
            this.lblJudAmt.StylePriority.UseTextAlignment = false;
            this.lblJudAmt.Text = "Highest Judgement Amount";
            this.lblJudAmt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblJudAmt.Weight = 1.5010822510822512;
            // 
            // lblJudAmtValue
            // 
            this.lblJudAmtValue.BackColor = System.Drawing.Color.White;
            this.lblJudAmtValue.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblJudAmtValue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblJudAmtValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.HighestJudgmentAmount")});
            this.lblJudAmtValue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblJudAmtValue.ForeColor = System.Drawing.Color.DimGray;
            this.lblJudAmtValue.Name = "lblJudAmtValue";
            this.lblJudAmtValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblJudAmtValue.StylePriority.UseBackColor = false;
            this.lblJudAmtValue.StylePriority.UseBorderColor = false;
            this.lblJudAmtValue.StylePriority.UseBorders = false;
            this.lblJudAmtValue.StylePriority.UseFont = false;
            this.lblJudAmtValue.StylePriority.UseForeColor = false;
            this.lblJudAmtValue.StylePriority.UsePadding = false;
            this.lblJudAmtValue.StylePriority.UseTextAlignment = false;
            this.lblJudAmtValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblJudAmtValue.Weight = 1.4989177489177488;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDelinquentrating,
            this.lblDelinquentratingValue});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100F);
            this.xrTableRow29.StylePriority.UsePadding = false;
            this.xrTableRow29.Weight = 0.8;
            // 
            // lblDelinquentrating
            // 
            this.lblDelinquentrating.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblDelinquentrating.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDelinquentrating.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDelinquentrating.ForeColor = System.Drawing.Color.Gray;
            this.lblDelinquentrating.Name = "lblDelinquentrating";
            this.lblDelinquentrating.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDelinquentrating.StylePriority.UseBackColor = false;
            this.lblDelinquentrating.StylePriority.UseBorders = false;
            this.lblDelinquentrating.StylePriority.UseFont = false;
            this.lblDelinquentrating.StylePriority.UseForeColor = false;
            this.lblDelinquentrating.StylePriority.UsePadding = false;
            this.lblDelinquentrating.StylePriority.UseTextAlignment = false;
            this.lblDelinquentrating.Text = "Highest Delinquent Rating (Last 24 Months)";
            this.lblDelinquentrating.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDelinquentrating.Weight = 1.5010822510822512;
            // 
            // lblDelinquentratingValue
            // 
            this.lblDelinquentratingValue.BackColor = System.Drawing.Color.White;
            this.lblDelinquentratingValue.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblDelinquentratingValue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDelinquentratingValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.HighestDelinquentRating")});
            this.lblDelinquentratingValue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDelinquentratingValue.ForeColor = System.Drawing.Color.DimGray;
            this.lblDelinquentratingValue.Name = "lblDelinquentratingValue";
            this.lblDelinquentratingValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDelinquentratingValue.StylePriority.UseBackColor = false;
            this.lblDelinquentratingValue.StylePriority.UseBorderColor = false;
            this.lblDelinquentratingValue.StylePriority.UseBorders = false;
            this.lblDelinquentratingValue.StylePriority.UseFont = false;
            this.lblDelinquentratingValue.StylePriority.UseForeColor = false;
            this.lblDelinquentratingValue.StylePriority.UsePadding = false;
            this.lblDelinquentratingValue.StylePriority.UseTextAlignment = false;
            this.lblDelinquentratingValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDelinquentratingValue.Weight = 1.4989177489177488;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDbtReview,
            this.lblDbtReviewValue});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100F);
            this.xrTableRow28.StylePriority.UsePadding = false;
            this.xrTableRow28.Weight = 0.79999999999999982;
            // 
            // lblDbtReview
            // 
            this.lblDbtReview.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblDbtReview.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDbtReview.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDbtReview.ForeColor = System.Drawing.Color.Gray;
            this.lblDbtReview.Name = "lblDbtReview";
            this.lblDbtReview.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDbtReview.StylePriority.UseBackColor = false;
            this.lblDbtReview.StylePriority.UseBorders = false;
            this.lblDbtReview.StylePriority.UseFont = false;
            this.lblDbtReview.StylePriority.UseForeColor = false;
            this.lblDbtReview.StylePriority.UsePadding = false;
            this.lblDbtReview.StylePriority.UseTextAlignment = false;
            this.lblDbtReview.Text = "Debt Review Status";
            this.lblDbtReview.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDbtReview.Weight = 1.5010822510822512;
            // 
            // lblDbtReviewValue
            // 
            this.lblDbtReviewValue.BackColor = System.Drawing.Color.White;
            this.lblDbtReviewValue.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblDbtReviewValue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDbtReviewValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.DebtReviewStatus")});
            this.lblDbtReviewValue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDbtReviewValue.ForeColor = System.Drawing.Color.DimGray;
            this.lblDbtReviewValue.Name = "lblDbtReviewValue";
            this.lblDbtReviewValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDbtReviewValue.StylePriority.UseBackColor = false;
            this.lblDbtReviewValue.StylePriority.UseBorderColor = false;
            this.lblDbtReviewValue.StylePriority.UseBorders = false;
            this.lblDbtReviewValue.StylePriority.UseFont = false;
            this.lblDbtReviewValue.StylePriority.UseForeColor = false;
            this.lblDbtReviewValue.StylePriority.UsePadding = false;
            this.lblDbtReviewValue.StylePriority.UseTextAlignment = false;
            this.lblDbtReviewValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDbtReviewValue.Weight = 1.4989177489177488;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblAO,
            this.lblAOValue});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100F);
            this.xrTableRow27.StylePriority.UsePadding = false;
            this.xrTableRow27.Weight = 0.79999999999999982;
            // 
            // lblAO
            // 
            this.lblAO.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblAO.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblAO.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAO.ForeColor = System.Drawing.Color.Gray;
            this.lblAO.Name = "lblAO";
            this.lblAO.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblAO.StylePriority.UseBackColor = false;
            this.lblAO.StylePriority.UseBorders = false;
            this.lblAO.StylePriority.UseFont = false;
            this.lblAO.StylePriority.UseForeColor = false;
            this.lblAO.StylePriority.UsePadding = false;
            this.lblAO.StylePriority.UseTextAlignment = false;
            this.lblAO.Text = "Admin Order Status";
            this.lblAO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAO.Weight = 1.5010822510822512;
            // 
            // lblAOValue
            // 
            this.lblAOValue.BackColor = System.Drawing.Color.White;
            this.lblAOValue.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblAOValue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblAOValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.AdminOrderStatus")});
            this.lblAOValue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblAOValue.ForeColor = System.Drawing.Color.DimGray;
            this.lblAOValue.Name = "lblAOValue";
            this.lblAOValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblAOValue.StylePriority.UseBackColor = false;
            this.lblAOValue.StylePriority.UseBorderColor = false;
            this.lblAOValue.StylePriority.UseBorders = false;
            this.lblAOValue.StylePriority.UseFont = false;
            this.lblAOValue.StylePriority.UseForeColor = false;
            this.lblAOValue.StylePriority.UsePadding = false;
            this.lblAOValue.StylePriority.UseTextAlignment = false;
            this.lblAOValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAOValue.Weight = 1.4989177489177488;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblAdverseAmt,
            this.lblAdverseAmtValue});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100F);
            this.xrTableRow26.StylePriority.UsePadding = false;
            this.xrTableRow26.Weight = 0.80000000000000016;
            // 
            // lblAdverseAmt
            // 
            this.lblAdverseAmt.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblAdverseAmt.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblAdverseAmt.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAdverseAmt.ForeColor = System.Drawing.Color.Gray;
            this.lblAdverseAmt.Name = "lblAdverseAmt";
            this.lblAdverseAmt.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblAdverseAmt.StylePriority.UseBackColor = false;
            this.lblAdverseAmt.StylePriority.UseBorders = false;
            this.lblAdverseAmt.StylePriority.UseFont = false;
            this.lblAdverseAmt.StylePriority.UseForeColor = false;
            this.lblAdverseAmt.StylePriority.UsePadding = false;
            this.lblAdverseAmt.StylePriority.UseTextAlignment = false;
            this.lblAdverseAmt.Text = "Total Adverse Amount";
            this.lblAdverseAmt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAdverseAmt.Weight = 1.5010822510822512;
            // 
            // lblAdverseAmtValue
            // 
            this.lblAdverseAmtValue.BackColor = System.Drawing.Color.White;
            this.lblAdverseAmtValue.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblAdverseAmtValue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblAdverseAmtValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.TotalAdverseAmount")});
            this.lblAdverseAmtValue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblAdverseAmtValue.ForeColor = System.Drawing.Color.DimGray;
            this.lblAdverseAmtValue.Name = "lblAdverseAmtValue";
            this.lblAdverseAmtValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblAdverseAmtValue.StylePriority.UseBackColor = false;
            this.lblAdverseAmtValue.StylePriority.UseBorderColor = false;
            this.lblAdverseAmtValue.StylePriority.UseBorders = false;
            this.lblAdverseAmtValue.StylePriority.UseFont = false;
            this.lblAdverseAmtValue.StylePriority.UseForeColor = false;
            this.lblAdverseAmtValue.StylePriority.UsePadding = false;
            this.lblAdverseAmtValue.StylePriority.UseTextAlignment = false;
            this.lblAdverseAmtValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAdverseAmtValue.Weight = 1.4989177489177488;
            // 
            // GroupHeader4
            // 
            this.GroupHeader4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblDebtHeader});
            this.GroupHeader4.HeightF = 46F;
            this.GroupHeader4.KeepTogether = true;
            this.GroupHeader4.Name = "GroupHeader4";
            // 
            // tblDebtHeader
            // 
            this.tblDebtHeader.BackColor = System.Drawing.Color.Transparent;
            this.tblDebtHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblDebtHeader.LocationFloat = new DevExpress.Utils.PointFloat(8F, 8F);
            this.tblDebtHeader.Name = "tblDebtHeader";
            this.tblDebtHeader.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow23});
            this.tblDebtHeader.SizeF = new System.Drawing.SizeF(792F, 33F);
            this.tblDebtHeader.StylePriority.UseBackColor = false;
            this.tblDebtHeader.StylePriority.UseFont = false;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDebtHeader});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 1;
            // 
            // lblDebtHeader
            // 
            this.lblDebtHeader.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.lblDebtHeader.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDebtHeader.BorderWidth = 2;
            this.lblDebtHeader.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblDebtHeader.ForeColor = System.Drawing.Color.Gray;
            this.lblDebtHeader.Name = "lblDebtHeader";
            this.lblDebtHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDebtHeader.StylePriority.UseBorderColor = false;
            this.lblDebtHeader.StylePriority.UseBorders = false;
            this.lblDebtHeader.StylePriority.UseBorderWidth = false;
            this.lblDebtHeader.StylePriority.UseFont = false;
            this.lblDebtHeader.StylePriority.UseForeColor = false;
            this.lblDebtHeader.StylePriority.UsePadding = false;
            this.lblDebtHeader.StylePriority.UseTextAlignment = false;
            this.lblDebtHeader.Text = "Debt Summary";
            this.lblDebtHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDebtHeader.Weight = 3;
            // 
            // AccountGoodBadSummary
            // 
            this.AccountGoodBadSummary.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailAccountGoodBadSummary,
            this.GroupHeader5});
            this.AccountGoodBadSummary.DataMember = "ConsumerAccountGoodBadSummary";
            this.AccountGoodBadSummary.Level = 4;
            this.AccountGoodBadSummary.Name = "AccountGoodBadSummary";
            // 
            // DetailAccountGoodBadSummary
            // 
            this.DetailAccountGoodBadSummary.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblAccStatus});
            this.DetailAccountGoodBadSummary.HeightF = 225F;
            this.DetailAccountGoodBadSummary.Name = "DetailAccountGoodBadSummary";
            // 
            // tblAccStatus
            // 
            this.tblAccStatus.BorderColor = System.Drawing.Color.White;
            this.tblAccStatus.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblAccStatus.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblAccStatus.LocationFloat = new DevExpress.Utils.PointFloat(8F, 0F);
            this.tblAccStatus.Name = "tblAccStatus";
            this.tblAccStatus.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblAccStatus.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow39,
            this.xrTableRow38,
            this.xrTableRow47,
            this.xrTableRow46,
            this.xrTableRow45,
            this.xrTableRow44,
            this.xrTableRow43,
            this.xrTableRow42,
            this.xrTableRow41,
            this.xrTableRow40,
            this.xrTableRow37});
            this.tblAccStatus.SizeF = new System.Drawing.SizeF(792F, 225F);
            this.tblAccStatus.StylePriority.UseBorderColor = false;
            this.tblAccStatus.StylePriority.UseBorders = false;
            this.tblAccStatus.StylePriority.UseFont = false;
            this.tblAccStatus.StylePriority.UsePadding = false;
            this.tblAccStatus.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.tblAccStatus_BeforePrint);
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(194)))), ((int)(((byte)(194)))));
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblAccdesc,
            this.lblGood,
            this.lblBad});
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.StylePriority.UseBackColor = false;
            this.xrTableRow39.StylePriority.UseBorderColor = false;
            this.xrTableRow39.StylePriority.UseForeColor = false;
            this.xrTableRow39.Weight = 1;
            // 
            // lblAccdesc
            // 
            this.lblAccdesc.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblAccdesc.BookmarkParent = this.lbDefCode;
            this.lblAccdesc.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.lblAccdesc.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblAccdesc.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.lblAccdesc.ForeColor = System.Drawing.Color.Gray;
            this.lblAccdesc.Name = "lblAccdesc";
            this.lblAccdesc.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblAccdesc.StylePriority.UseBackColor = false;
            this.lblAccdesc.StylePriority.UseBorderColor = false;
            this.lblAccdesc.StylePriority.UseBorders = false;
            this.lblAccdesc.StylePriority.UseFont = false;
            this.lblAccdesc.StylePriority.UseForeColor = false;
            this.lblAccdesc.StylePriority.UsePadding = false;
            this.lblAccdesc.StylePriority.UseTextAlignment = false;
            this.lblAccdesc.Text = "Account Description";
            this.lblAccdesc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAccdesc.Weight = 1.6414473684210527;
            // 
            // lbDefCode
            // 
            this.lbDefCode.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lbDefCode.BorderColor = System.Drawing.Color.White;
            this.lbDefCode.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbDefCode.CanGrow = false;
            this.lbDefCode.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbDefCode.ForeColor = System.Drawing.Color.Gray;
            this.lbDefCode.Name = "lbDefCode";
            this.lbDefCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lbDefCode.StylePriority.UseBackColor = false;
            this.lbDefCode.StylePriority.UseBorderColor = false;
            this.lbDefCode.StylePriority.UseBorders = false;
            this.lbDefCode.StylePriority.UseFont = false;
            this.lbDefCode.StylePriority.UseForeColor = false;
            this.lbDefCode.StylePriority.UsePadding = false;
            this.lbDefCode.StylePriority.UseTextAlignment = false;
            this.lbDefCode.Text = "Indicators";
            this.lbDefCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbDefCode.Weight = 3.7488409329891978;
            // 
            // lblGood
            // 
            this.lblGood.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblGood.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.lblGood.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblGood.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.lblGood.ForeColor = System.Drawing.Color.Gray;
            this.lblGood.Name = "lblGood";
            this.lblGood.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblGood.StylePriority.UseBackColor = false;
            this.lblGood.StylePriority.UseBorderColor = false;
            this.lblGood.StylePriority.UseBorders = false;
            this.lblGood.StylePriority.UseFont = false;
            this.lblGood.StylePriority.UseForeColor = false;
            this.lblGood.StylePriority.UsePadding = false;
            this.lblGood.StylePriority.UseTextAlignment = false;
            this.lblGood.Text = "Good";
            this.lblGood.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblGood.Weight = 0.41282894736842096;
            // 
            // lblBad
            // 
            this.lblBad.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblBad.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.lblBad.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblBad.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.lblBad.ForeColor = System.Drawing.Color.Gray;
            this.lblBad.Name = "lblBad";
            this.lblBad.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblBad.StylePriority.UseBackColor = false;
            this.lblBad.StylePriority.UseBorderColor = false;
            this.lblBad.StylePriority.UseBorders = false;
            this.lblBad.StylePriority.UseFont = false;
            this.lblBad.StylePriority.UseForeColor = false;
            this.lblBad.StylePriority.UsePadding = false;
            this.lblBad.StylePriority.UseTextAlignment = false;
            this.lblBad.Text = "Bad";
            this.lblBad.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblBad.Weight = 0.41776315789473678;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblRetail,
            this.lblRetailG,
            this.lblRetailB});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Weight = 0.8;
            // 
            // lblRetail
            // 
            this.lblRetail.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblRetail.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblRetail.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRetail.ForeColor = System.Drawing.Color.Gray;
            this.lblRetail.Name = "lblRetail";
            this.lblRetail.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblRetail.StylePriority.UseBackColor = false;
            this.lblRetail.StylePriority.UseBorders = false;
            this.lblRetail.StylePriority.UseFont = false;
            this.lblRetail.StylePriority.UseForeColor = false;
            this.lblRetail.StylePriority.UsePadding = false;
            this.lblRetail.StylePriority.UseTextAlignment = false;
            this.lblRetail.Text = "Total no. Of Active Retail Accounts";
            this.lblRetail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblRetail.Weight = 1.6414473684210527;
            // 
            // lblRetailG
            // 
            this.lblRetailG.BackColor = System.Drawing.Color.White;
            this.lblRetailG.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblRetailG.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblRetailG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfRetailAccountsGood")});
            this.lblRetailG.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblRetailG.ForeColor = System.Drawing.Color.DimGray;
            this.lblRetailG.Name = "lblRetailG";
            this.lblRetailG.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblRetailG.StylePriority.UseBackColor = false;
            this.lblRetailG.StylePriority.UseBorderColor = false;
            this.lblRetailG.StylePriority.UseBorders = false;
            this.lblRetailG.StylePriority.UseFont = false;
            this.lblRetailG.StylePriority.UseForeColor = false;
            this.lblRetailG.StylePriority.UsePadding = false;
            this.lblRetailG.StylePriority.UseTextAlignment = false;
            this.lblRetailG.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblRetailG.Weight = 0.41282894736842096;
            // 
            // lblRetailB
            // 
            this.lblRetailB.BackColor = System.Drawing.Color.White;
            this.lblRetailB.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblRetailB.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblRetailB.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfRetailAccountsBad")});
            this.lblRetailB.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblRetailB.ForeColor = System.Drawing.Color.DimGray;
            this.lblRetailB.Name = "lblRetailB";
            this.lblRetailB.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblRetailB.StylePriority.UseBackColor = false;
            this.lblRetailB.StylePriority.UseBorderColor = false;
            this.lblRetailB.StylePriority.UseBorders = false;
            this.lblRetailB.StylePriority.UseFont = false;
            this.lblRetailB.StylePriority.UseForeColor = false;
            this.lblRetailB.StylePriority.UsePadding = false;
            this.lblRetailB.StylePriority.UseTextAlignment = false;
            this.lblRetailB.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblRetailB.Weight = 0.41776315789473678;
            // 
            // xrTableRow47
            // 
            this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCc,
            this.lblCreditCradG,
            this.lblCreditCardB});
            this.xrTableRow47.Name = "xrTableRow47";
            this.xrTableRow47.Weight = 0.79999999999999993;
            // 
            // lblCc
            // 
            this.lblCc.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblCc.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblCc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCc.ForeColor = System.Drawing.Color.Gray;
            this.lblCc.Name = "lblCc";
            this.lblCc.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblCc.StylePriority.UseBackColor = false;
            this.lblCc.StylePriority.UseBorders = false;
            this.lblCc.StylePriority.UseFont = false;
            this.lblCc.StylePriority.UseForeColor = false;
            this.lblCc.StylePriority.UsePadding = false;
            this.lblCc.StylePriority.UseTextAlignment = false;
            this.lblCc.Text = "Total no. Of Active Credit card Accounts";
            this.lblCc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblCc.Weight = 1.6414473684210527;
            // 
            // lblCreditCradG
            // 
            this.lblCreditCradG.BackColor = System.Drawing.Color.White;
            this.lblCreditCradG.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblCreditCradG.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblCreditCradG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfCreditCardAccountsGood")});
            this.lblCreditCradG.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblCreditCradG.ForeColor = System.Drawing.Color.DimGray;
            this.lblCreditCradG.Name = "lblCreditCradG";
            this.lblCreditCradG.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblCreditCradG.StylePriority.UseBackColor = false;
            this.lblCreditCradG.StylePriority.UseBorderColor = false;
            this.lblCreditCradG.StylePriority.UseBorders = false;
            this.lblCreditCradG.StylePriority.UseFont = false;
            this.lblCreditCradG.StylePriority.UseForeColor = false;
            this.lblCreditCradG.StylePriority.UsePadding = false;
            this.lblCreditCradG.StylePriority.UseTextAlignment = false;
            this.lblCreditCradG.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblCreditCradG.Weight = 0.41282894736842096;
            // 
            // lblCreditCardB
            // 
            this.lblCreditCardB.BackColor = System.Drawing.Color.White;
            this.lblCreditCardB.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblCreditCardB.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblCreditCardB.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfCreditCardAccountsBad")});
            this.lblCreditCardB.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblCreditCardB.ForeColor = System.Drawing.Color.DimGray;
            this.lblCreditCardB.Name = "lblCreditCardB";
            this.lblCreditCardB.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblCreditCardB.StylePriority.UseBackColor = false;
            this.lblCreditCardB.StylePriority.UseBorderColor = false;
            this.lblCreditCardB.StylePriority.UseBorders = false;
            this.lblCreditCardB.StylePriority.UseFont = false;
            this.lblCreditCardB.StylePriority.UseForeColor = false;
            this.lblCreditCardB.StylePriority.UsePadding = false;
            this.lblCreditCardB.StylePriority.UseTextAlignment = false;
            this.lblCreditCardB.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblCreditCardB.Weight = 0.41776315789473678;
            // 
            // xrTableRow46
            // 
            this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblFurnAcc,
            this.lblFurnAccG,
            this.lblFurnAccB});
            this.xrTableRow46.Name = "xrTableRow46";
            this.xrTableRow46.Weight = 0.79999999999999993;
            // 
            // lblFurnAcc
            // 
            this.lblFurnAcc.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblFurnAcc.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblFurnAcc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFurnAcc.ForeColor = System.Drawing.Color.Gray;
            this.lblFurnAcc.Name = "lblFurnAcc";
            this.lblFurnAcc.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblFurnAcc.StylePriority.UseBackColor = false;
            this.lblFurnAcc.StylePriority.UseBorders = false;
            this.lblFurnAcc.StylePriority.UseFont = false;
            this.lblFurnAcc.StylePriority.UseForeColor = false;
            this.lblFurnAcc.StylePriority.UsePadding = false;
            this.lblFurnAcc.StylePriority.UseTextAlignment = false;
            this.lblFurnAcc.Text = "Total no. Of Active Furniture Store Accounts";
            this.lblFurnAcc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblFurnAcc.Weight = 1.6414473684210527;
            // 
            // lblFurnAccG
            // 
            this.lblFurnAccG.BackColor = System.Drawing.Color.White;
            this.lblFurnAccG.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblFurnAccG.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblFurnAccG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfFurnitureAccountsGood")});
            this.lblFurnAccG.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblFurnAccG.ForeColor = System.Drawing.Color.DimGray;
            this.lblFurnAccG.Name = "lblFurnAccG";
            this.lblFurnAccG.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblFurnAccG.StylePriority.UseBackColor = false;
            this.lblFurnAccG.StylePriority.UseBorderColor = false;
            this.lblFurnAccG.StylePriority.UseBorders = false;
            this.lblFurnAccG.StylePriority.UseFont = false;
            this.lblFurnAccG.StylePriority.UseForeColor = false;
            this.lblFurnAccG.StylePriority.UsePadding = false;
            this.lblFurnAccG.StylePriority.UseTextAlignment = false;
            this.lblFurnAccG.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblFurnAccG.Weight = 0.41282894736842096;
            // 
            // lblFurnAccB
            // 
            this.lblFurnAccB.BackColor = System.Drawing.Color.White;
            this.lblFurnAccB.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblFurnAccB.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblFurnAccB.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfFurnitureAccountsBad")});
            this.lblFurnAccB.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblFurnAccB.ForeColor = System.Drawing.Color.DimGray;
            this.lblFurnAccB.Name = "lblFurnAccB";
            this.lblFurnAccB.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblFurnAccB.StylePriority.UseBackColor = false;
            this.lblFurnAccB.StylePriority.UseBorderColor = false;
            this.lblFurnAccB.StylePriority.UseBorders = false;
            this.lblFurnAccB.StylePriority.UseFont = false;
            this.lblFurnAccB.StylePriority.UseForeColor = false;
            this.lblFurnAccB.StylePriority.UsePadding = false;
            this.lblFurnAccB.StylePriority.UseTextAlignment = false;
            this.lblFurnAccB.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblFurnAccB.Weight = 0.41776315789473678;
            // 
            // xrTableRow45
            // 
            this.xrTableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblInsurance,
            this.lblInsuranceG,
            this.lblInsuranceB});
            this.xrTableRow45.Name = "xrTableRow45";
            this.xrTableRow45.Weight = 0.8;
            // 
            // lblInsurance
            // 
            this.lblInsurance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblInsurance.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblInsurance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInsurance.ForeColor = System.Drawing.Color.Gray;
            this.lblInsurance.Name = "lblInsurance";
            this.lblInsurance.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblInsurance.StylePriority.UseBackColor = false;
            this.lblInsurance.StylePriority.UseBorders = false;
            this.lblInsurance.StylePriority.UseFont = false;
            this.lblInsurance.StylePriority.UseForeColor = false;
            this.lblInsurance.StylePriority.UsePadding = false;
            this.lblInsurance.StylePriority.UseTextAlignment = false;
            this.lblInsurance.Text = "Total no. Of Active Insurance Accounts";
            this.lblInsurance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblInsurance.Weight = 1.6414473684210527;
            // 
            // lblInsuranceG
            // 
            this.lblInsuranceG.BackColor = System.Drawing.Color.White;
            this.lblInsuranceG.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblInsuranceG.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblInsuranceG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfInsuranceAccountsGood")});
            this.lblInsuranceG.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblInsuranceG.ForeColor = System.Drawing.Color.DimGray;
            this.lblInsuranceG.Name = "lblInsuranceG";
            this.lblInsuranceG.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblInsuranceG.StylePriority.UseBackColor = false;
            this.lblInsuranceG.StylePriority.UseBorderColor = false;
            this.lblInsuranceG.StylePriority.UseBorders = false;
            this.lblInsuranceG.StylePriority.UseFont = false;
            this.lblInsuranceG.StylePriority.UseForeColor = false;
            this.lblInsuranceG.StylePriority.UsePadding = false;
            this.lblInsuranceG.StylePriority.UseTextAlignment = false;
            this.lblInsuranceG.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblInsuranceG.Weight = 0.41282894736842096;
            // 
            // lblInsuranceB
            // 
            this.lblInsuranceB.BackColor = System.Drawing.Color.White;
            this.lblInsuranceB.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblInsuranceB.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblInsuranceB.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfInsuranceAccountsBad")});
            this.lblInsuranceB.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblInsuranceB.ForeColor = System.Drawing.Color.DimGray;
            this.lblInsuranceB.Name = "lblInsuranceB";
            this.lblInsuranceB.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblInsuranceB.StylePriority.UseBackColor = false;
            this.lblInsuranceB.StylePriority.UseBorderColor = false;
            this.lblInsuranceB.StylePriority.UseBorders = false;
            this.lblInsuranceB.StylePriority.UseFont = false;
            this.lblInsuranceB.StylePriority.UseForeColor = false;
            this.lblInsuranceB.StylePriority.UsePadding = false;
            this.lblInsuranceB.StylePriority.UseTextAlignment = false;
            this.lblInsuranceB.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblInsuranceB.Weight = 0.41776315789473678;
            // 
            // xrTableRow44
            // 
            this.xrTableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblFinance,
            this.lblFinanceG,
            this.lblFinanceB});
            this.xrTableRow44.Name = "xrTableRow44";
            this.xrTableRow44.Weight = 0.8;
            // 
            // lblFinance
            // 
            this.lblFinance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblFinance.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblFinance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFinance.ForeColor = System.Drawing.Color.Gray;
            this.lblFinance.Name = "lblFinance";
            this.lblFinance.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblFinance.StylePriority.UseBackColor = false;
            this.lblFinance.StylePriority.UseBorders = false;
            this.lblFinance.StylePriority.UseFont = false;
            this.lblFinance.StylePriority.UseForeColor = false;
            this.lblFinance.StylePriority.UsePadding = false;
            this.lblFinance.StylePriority.UseTextAlignment = false;
            this.lblFinance.Text = "Total no. Of Active Personal Finance Accounts";
            this.lblFinance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblFinance.Weight = 1.6414473684210527;
            // 
            // lblFinanceG
            // 
            this.lblFinanceG.BackColor = System.Drawing.Color.White;
            this.lblFinanceG.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblFinanceG.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblFinanceG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfPersonalFinAccountsGood")});
            this.lblFinanceG.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblFinanceG.ForeColor = System.Drawing.Color.DimGray;
            this.lblFinanceG.Name = "lblFinanceG";
            this.lblFinanceG.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblFinanceG.StylePriority.UseBackColor = false;
            this.lblFinanceG.StylePriority.UseBorderColor = false;
            this.lblFinanceG.StylePriority.UseBorders = false;
            this.lblFinanceG.StylePriority.UseFont = false;
            this.lblFinanceG.StylePriority.UseForeColor = false;
            this.lblFinanceG.StylePriority.UsePadding = false;
            this.lblFinanceG.StylePriority.UseTextAlignment = false;
            this.lblFinanceG.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblFinanceG.Weight = 0.41282894736842096;
            // 
            // lblFinanceB
            // 
            this.lblFinanceB.BackColor = System.Drawing.Color.White;
            this.lblFinanceB.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblFinanceB.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblFinanceB.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfPersonalFinAccountsBad")});
            this.lblFinanceB.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblFinanceB.ForeColor = System.Drawing.Color.DimGray;
            this.lblFinanceB.Name = "lblFinanceB";
            this.lblFinanceB.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblFinanceB.StylePriority.UseBackColor = false;
            this.lblFinanceB.StylePriority.UseBorderColor = false;
            this.lblFinanceB.StylePriority.UseBorders = false;
            this.lblFinanceB.StylePriority.UseFont = false;
            this.lblFinanceB.StylePriority.UseForeColor = false;
            this.lblFinanceB.StylePriority.UsePadding = false;
            this.lblFinanceB.StylePriority.UseTextAlignment = false;
            this.lblFinanceB.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblFinanceB.Weight = 0.41776315789473678;
            // 
            // xrTableRow43
            // 
            this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblBankAcc,
            this.lblBankAccG,
            this.lblBankAccB});
            this.xrTableRow43.Name = "xrTableRow43";
            this.xrTableRow43.Weight = 0.8;
            // 
            // lblBankAcc
            // 
            this.lblBankAcc.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblBankAcc.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblBankAcc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBankAcc.ForeColor = System.Drawing.Color.Gray;
            this.lblBankAcc.Name = "lblBankAcc";
            this.lblBankAcc.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblBankAcc.StylePriority.UseBackColor = false;
            this.lblBankAcc.StylePriority.UseBorders = false;
            this.lblBankAcc.StylePriority.UseFont = false;
            this.lblBankAcc.StylePriority.UseForeColor = false;
            this.lblBankAcc.StylePriority.UsePadding = false;
            this.lblBankAcc.StylePriority.UseTextAlignment = false;
            this.lblBankAcc.Text = "Total no. Of Active Bank Accounts";
            this.lblBankAcc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblBankAcc.Weight = 1.6414473684210527;
            // 
            // lblBankAccG
            // 
            this.lblBankAccG.BackColor = System.Drawing.Color.White;
            this.lblBankAccG.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblBankAccG.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblBankAccG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfBankAccountsGood")});
            this.lblBankAccG.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblBankAccG.ForeColor = System.Drawing.Color.DimGray;
            this.lblBankAccG.Name = "lblBankAccG";
            this.lblBankAccG.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblBankAccG.StylePriority.UseBackColor = false;
            this.lblBankAccG.StylePriority.UseBorderColor = false;
            this.lblBankAccG.StylePriority.UseBorders = false;
            this.lblBankAccG.StylePriority.UseFont = false;
            this.lblBankAccG.StylePriority.UseForeColor = false;
            this.lblBankAccG.StylePriority.UsePadding = false;
            this.lblBankAccG.StylePriority.UseTextAlignment = false;
            this.lblBankAccG.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblBankAccG.Weight = 0.41282894736842096;
            // 
            // lblBankAccB
            // 
            this.lblBankAccB.BackColor = System.Drawing.Color.White;
            this.lblBankAccB.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblBankAccB.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblBankAccB.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfBankAccountsBad")});
            this.lblBankAccB.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblBankAccB.ForeColor = System.Drawing.Color.DimGray;
            this.lblBankAccB.Name = "lblBankAccB";
            this.lblBankAccB.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblBankAccB.StylePriority.UseBackColor = false;
            this.lblBankAccB.StylePriority.UseBorderColor = false;
            this.lblBankAccB.StylePriority.UseBorders = false;
            this.lblBankAccB.StylePriority.UseFont = false;
            this.lblBankAccB.StylePriority.UseForeColor = false;
            this.lblBankAccB.StylePriority.UsePadding = false;
            this.lblBankAccB.StylePriority.UseTextAlignment = false;
            this.lblBankAccB.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblBankAccB.Weight = 0.41776315789473678;
            // 
            // xrTableRow42
            // 
            this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblTelecomms,
            this.lblTelecommsG,
            this.lblTelecommsB});
            this.xrTableRow42.Name = "xrTableRow42";
            this.xrTableRow42.Weight = 0.80000000000000027;
            // 
            // lblTelecomms
            // 
            this.lblTelecomms.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblTelecomms.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTelecomms.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelecomms.ForeColor = System.Drawing.Color.Gray;
            this.lblTelecomms.Name = "lblTelecomms";
            this.lblTelecomms.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblTelecomms.StylePriority.UseBackColor = false;
            this.lblTelecomms.StylePriority.UseBorders = false;
            this.lblTelecomms.StylePriority.UseFont = false;
            this.lblTelecomms.StylePriority.UseForeColor = false;
            this.lblTelecomms.StylePriority.UsePadding = false;
            this.lblTelecomms.StylePriority.UseTextAlignment = false;
            this.lblTelecomms.Text = "Total no. Of Active Telecomms Accounts";
            this.lblTelecomms.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblTelecomms.Weight = 1.6414473684210527;
            // 
            // lblTelecommsG
            // 
            this.lblTelecommsG.BackColor = System.Drawing.Color.White;
            this.lblTelecommsG.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblTelecommsG.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblTelecommsG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfTelecomAccountsGood")});
            this.lblTelecommsG.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblTelecommsG.ForeColor = System.Drawing.Color.DimGray;
            this.lblTelecommsG.Name = "lblTelecommsG";
            this.lblTelecommsG.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblTelecommsG.StylePriority.UseBackColor = false;
            this.lblTelecommsG.StylePriority.UseBorderColor = false;
            this.lblTelecommsG.StylePriority.UseBorders = false;
            this.lblTelecommsG.StylePriority.UseFont = false;
            this.lblTelecommsG.StylePriority.UseForeColor = false;
            this.lblTelecommsG.StylePriority.UsePadding = false;
            this.lblTelecommsG.StylePriority.UseTextAlignment = false;
            this.lblTelecommsG.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblTelecommsG.Weight = 0.41282894736842096;
            // 
            // lblTelecommsB
            // 
            this.lblTelecommsB.BackColor = System.Drawing.Color.White;
            this.lblTelecommsB.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblTelecommsB.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblTelecommsB.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfTelecomAccountsBad")});
            this.lblTelecommsB.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblTelecommsB.ForeColor = System.Drawing.Color.DimGray;
            this.lblTelecommsB.Name = "lblTelecommsB";
            this.lblTelecommsB.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblTelecommsB.StylePriority.UseBackColor = false;
            this.lblTelecommsB.StylePriority.UseBorderColor = false;
            this.lblTelecommsB.StylePriority.UseBorders = false;
            this.lblTelecommsB.StylePriority.UseFont = false;
            this.lblTelecommsB.StylePriority.UseForeColor = false;
            this.lblTelecommsB.StylePriority.UsePadding = false;
            this.lblTelecommsB.StylePriority.UseTextAlignment = false;
            this.lblTelecommsB.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblTelecommsB.Weight = 0.41776315789473678;
            // 
            // xrTableRow41
            // 
            this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblHomeLoan,
            this.lblHomeLoanG,
            this.lblHomeLoanB});
            this.xrTableRow41.Name = "xrTableRow41";
            this.xrTableRow41.Weight = 0.79999999999999982;
            // 
            // lblHomeLoan
            // 
            this.lblHomeLoan.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblHomeLoan.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblHomeLoan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHomeLoan.ForeColor = System.Drawing.Color.Gray;
            this.lblHomeLoan.Name = "lblHomeLoan";
            this.lblHomeLoan.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblHomeLoan.StylePriority.UseBackColor = false;
            this.lblHomeLoan.StylePriority.UseBorders = false;
            this.lblHomeLoan.StylePriority.UseFont = false;
            this.lblHomeLoan.StylePriority.UseForeColor = false;
            this.lblHomeLoan.StylePriority.UsePadding = false;
            this.lblHomeLoan.StylePriority.UseTextAlignment = false;
            this.lblHomeLoan.Text = "Total no. Of Active Home Loan Accounts";
            this.lblHomeLoan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblHomeLoan.Weight = 1.6414473684210527;
            // 
            // lblHomeLoanG
            // 
            this.lblHomeLoanG.BackColor = System.Drawing.Color.White;
            this.lblHomeLoanG.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblHomeLoanG.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblHomeLoanG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfHomeLoanAccountsGood")});
            this.lblHomeLoanG.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblHomeLoanG.ForeColor = System.Drawing.Color.DimGray;
            this.lblHomeLoanG.Name = "lblHomeLoanG";
            this.lblHomeLoanG.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblHomeLoanG.StylePriority.UseBackColor = false;
            this.lblHomeLoanG.StylePriority.UseBorderColor = false;
            this.lblHomeLoanG.StylePriority.UseBorders = false;
            this.lblHomeLoanG.StylePriority.UseFont = false;
            this.lblHomeLoanG.StylePriority.UseForeColor = false;
            this.lblHomeLoanG.StylePriority.UsePadding = false;
            this.lblHomeLoanG.StylePriority.UseTextAlignment = false;
            this.lblHomeLoanG.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblHomeLoanG.Weight = 0.41282894736842096;
            // 
            // lblHomeLoanB
            // 
            this.lblHomeLoanB.BackColor = System.Drawing.Color.White;
            this.lblHomeLoanB.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblHomeLoanB.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblHomeLoanB.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfHomeLoanAccountsBad")});
            this.lblHomeLoanB.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblHomeLoanB.ForeColor = System.Drawing.Color.DimGray;
            this.lblHomeLoanB.Name = "lblHomeLoanB";
            this.lblHomeLoanB.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblHomeLoanB.StylePriority.UseBackColor = false;
            this.lblHomeLoanB.StylePriority.UseBorderColor = false;
            this.lblHomeLoanB.StylePriority.UseBorders = false;
            this.lblHomeLoanB.StylePriority.UseFont = false;
            this.lblHomeLoanB.StylePriority.UseForeColor = false;
            this.lblHomeLoanB.StylePriority.UsePadding = false;
            this.lblHomeLoanB.StylePriority.UseTextAlignment = false;
            this.lblHomeLoanB.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblHomeLoanB.Weight = 0.41776315789473678;
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblVehicleFin,
            this.lblVehicleFinG,
            this.lblVehicleFinB});
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.Weight = 0.8;
            // 
            // lblVehicleFin
            // 
            this.lblVehicleFin.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblVehicleFin.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblVehicleFin.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVehicleFin.ForeColor = System.Drawing.Color.Gray;
            this.lblVehicleFin.Name = "lblVehicleFin";
            this.lblVehicleFin.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblVehicleFin.StylePriority.UseBackColor = false;
            this.lblVehicleFin.StylePriority.UseBorders = false;
            this.lblVehicleFin.StylePriority.UseFont = false;
            this.lblVehicleFin.StylePriority.UseForeColor = false;
            this.lblVehicleFin.StylePriority.UsePadding = false;
            this.lblVehicleFin.StylePriority.UseTextAlignment = false;
            this.lblVehicleFin.Text = "Total no. Of Active Motor Vehicle Finance Accounts";
            this.lblVehicleFin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblVehicleFin.Weight = 1.6414473684210527;
            // 
            // lblVehicleFinG
            // 
            this.lblVehicleFinG.BackColor = System.Drawing.Color.White;
            this.lblVehicleFinG.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblVehicleFinG.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblVehicleFinG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfMotorFinanceAccountsGood")});
            this.lblVehicleFinG.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblVehicleFinG.ForeColor = System.Drawing.Color.DimGray;
            this.lblVehicleFinG.Name = "lblVehicleFinG";
            this.lblVehicleFinG.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblVehicleFinG.StylePriority.UseBackColor = false;
            this.lblVehicleFinG.StylePriority.UseBorderColor = false;
            this.lblVehicleFinG.StylePriority.UseBorders = false;
            this.lblVehicleFinG.StylePriority.UseFont = false;
            this.lblVehicleFinG.StylePriority.UseForeColor = false;
            this.lblVehicleFinG.StylePriority.UsePadding = false;
            this.lblVehicleFinG.StylePriority.UseTextAlignment = false;
            this.lblVehicleFinG.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblVehicleFinG.Weight = 0.41282894736842096;
            // 
            // lblVehicleFinB
            // 
            this.lblVehicleFinB.BackColor = System.Drawing.Color.White;
            this.lblVehicleFinB.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblVehicleFinB.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblVehicleFinB.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfMotorFinanceAccountsBad")});
            this.lblVehicleFinB.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblVehicleFinB.ForeColor = System.Drawing.Color.DimGray;
            this.lblVehicleFinB.Name = "lblVehicleFinB";
            this.lblVehicleFinB.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblVehicleFinB.StylePriority.UseBackColor = false;
            this.lblVehicleFinB.StylePriority.UseBorderColor = false;
            this.lblVehicleFinB.StylePriority.UseBorders = false;
            this.lblVehicleFinB.StylePriority.UseFont = false;
            this.lblVehicleFinB.StylePriority.UseForeColor = false;
            this.lblVehicleFinB.StylePriority.UsePadding = false;
            this.lblVehicleFinB.StylePriority.UseTextAlignment = false;
            this.lblVehicleFinB.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblVehicleFinB.Weight = 0.41776315789473678;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblOtherAcc,
            this.lblOtherAccG,
            this.lblOtherAccB});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Weight = 0.79999999999999982;
            // 
            // lblOtherAcc
            // 
            this.lblOtherAcc.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblOtherAcc.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblOtherAcc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOtherAcc.ForeColor = System.Drawing.Color.Gray;
            this.lblOtherAcc.Name = "lblOtherAcc";
            this.lblOtherAcc.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblOtherAcc.StylePriority.UseBackColor = false;
            this.lblOtherAcc.StylePriority.UseBorders = false;
            this.lblOtherAcc.StylePriority.UseFont = false;
            this.lblOtherAcc.StylePriority.UseForeColor = false;
            this.lblOtherAcc.StylePriority.UsePadding = false;
            this.lblOtherAcc.StylePriority.UseTextAlignment = false;
            this.lblOtherAcc.Text = "Total no. Of Active Other Accounts";
            this.lblOtherAcc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblOtherAcc.Weight = 1.6414473684210527;
            // 
            // lblOtherAccG
            // 
            this.lblOtherAccG.BackColor = System.Drawing.Color.White;
            this.lblOtherAccG.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblOtherAccG.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblOtherAccG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfOtherAccountsGood")});
            this.lblOtherAccG.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblOtherAccG.ForeColor = System.Drawing.Color.DimGray;
            this.lblOtherAccG.Name = "lblOtherAccG";
            this.lblOtherAccG.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblOtherAccG.StylePriority.UseBackColor = false;
            this.lblOtherAccG.StylePriority.UseBorderColor = false;
            this.lblOtherAccG.StylePriority.UseBorders = false;
            this.lblOtherAccG.StylePriority.UseFont = false;
            this.lblOtherAccG.StylePriority.UseForeColor = false;
            this.lblOtherAccG.StylePriority.UsePadding = false;
            this.lblOtherAccG.StylePriority.UseTextAlignment = false;
            this.lblOtherAccG.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblOtherAccG.Weight = 0.41282894736842096;
            // 
            // lblOtherAccB
            // 
            this.lblOtherAccB.BackColor = System.Drawing.Color.White;
            this.lblOtherAccB.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblOtherAccB.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblOtherAccB.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfOtherAccountsBad")});
            this.lblOtherAccB.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblOtherAccB.ForeColor = System.Drawing.Color.DimGray;
            this.lblOtherAccB.Name = "lblOtherAccB";
            this.lblOtherAccB.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblOtherAccB.StylePriority.UseBackColor = false;
            this.lblOtherAccB.StylePriority.UseBorderColor = false;
            this.lblOtherAccB.StylePriority.UseBorders = false;
            this.lblOtherAccB.StylePriority.UseFont = false;
            this.lblOtherAccB.StylePriority.UseForeColor = false;
            this.lblOtherAccB.StylePriority.UsePadding = false;
            this.lblOtherAccB.StylePriority.UseTextAlignment = false;
            this.lblOtherAccB.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblOtherAccB.Weight = 0.41776315789473678;
            // 
            // GroupHeader5
            // 
            this.GroupHeader5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblCrAccStatus});
            this.GroupHeader5.HeightF = 45F;
            this.GroupHeader5.Name = "GroupHeader5";
            // 
            // tblCrAccStatus
            // 
            this.tblCrAccStatus.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblCrAccStatus.LocationFloat = new DevExpress.Utils.PointFloat(8F, 8F);
            this.tblCrAccStatus.Name = "tblCrAccStatus";
            this.tblCrAccStatus.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow25});
            this.tblCrAccStatus.SizeF = new System.Drawing.SizeF(792F, 33F);
            this.tblCrAccStatus.StylePriority.UseFont = false;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCrAccStatus});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 1;
            // 
            // lblCrAccStatus
            // 
            this.lblCrAccStatus.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.lblCrAccStatus.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblCrAccStatus.BorderWidth = 2;
            this.lblCrAccStatus.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblCrAccStatus.ForeColor = System.Drawing.Color.Gray;
            this.lblCrAccStatus.Name = "lblCrAccStatus";
            this.lblCrAccStatus.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100F);
            this.lblCrAccStatus.StylePriority.UseBorderColor = false;
            this.lblCrAccStatus.StylePriority.UseBorders = false;
            this.lblCrAccStatus.StylePriority.UseBorderWidth = false;
            this.lblCrAccStatus.StylePriority.UseFont = false;
            this.lblCrAccStatus.StylePriority.UseForeColor = false;
            this.lblCrAccStatus.StylePriority.UsePadding = false;
            this.lblCrAccStatus.StylePriority.UseTextAlignment = false;
            this.lblCrAccStatus.Text = "Credit Account Status Summary";
            this.lblCrAccStatus.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblCrAccStatus.Weight = 3;
            // 
            // NameHistory
            // 
            this.NameHistory.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailNameHistory,
            this.GroupHeader10});
            this.NameHistory.DataMember = "ConsumerNameHistory";
            this.NameHistory.Level = 18;
            this.NameHistory.Name = "NameHistory";
            // 
            // DetailNameHistory
            // 
            this.DetailNameHistory.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblNamehistD});
            this.DetailNameHistory.HeightF = 20F;
            this.DetailNameHistory.Name = "DetailNameHistory";
            // 
            // tblNamehistD
            // 
            this.tblNamehistD.BackColor = System.Drawing.Color.White;
            this.tblNamehistD.BorderColor = System.Drawing.Color.Gainsboro;
            this.tblNamehistD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.tblNamehistD.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.tblNamehistD.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tblNamehistD.LocationFloat = new DevExpress.Utils.PointFloat(8F, 0F);
            this.tblNamehistD.Name = "tblNamehistD";
            this.tblNamehistD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblNamehistD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow65});
            this.tblNamehistD.SizeF = new System.Drawing.SizeF(792F, 20F);
            this.tblNamehistD.StylePriority.UseBackColor = false;
            this.tblNamehistD.StylePriority.UseBorderColor = false;
            this.tblNamehistD.StylePriority.UseBorders = false;
            this.tblNamehistD.StylePriority.UseFont = false;
            this.tblNamehistD.StylePriority.UseForeColor = false;
            this.tblNamehistD.StylePriority.UsePadding = false;
            this.tblNamehistD.StylePriority.UseTextAlignment = false;
            this.tblNamehistD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow65
            // 
            this.xrTableRow65.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDUpdatedDate,
            this.lblHDSurname,
            this.lblHDFirstname,
            this.lblHDSecodnName,
            this.lblHDInitial,
            this.lblHDTitle,
            this.lblHDIDno,
            this.lblHDPPNo,
            this.lblHDBirthDate});
            this.xrTableRow65.Name = "xrTableRow65";
            this.xrTableRow65.Weight = 1;
            // 
            // lblDUpdatedDate
            // 
            this.lblDUpdatedDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.LastUpdatedDate")});
            this.lblDUpdatedDate.ForeColor = System.Drawing.Color.DimGray;
            this.lblDUpdatedDate.Name = "lblDUpdatedDate";
            this.lblDUpdatedDate.StylePriority.UseForeColor = false;
            this.lblDUpdatedDate.Weight = 0.34848484848484851;
            // 
            // lblHDSurname
            // 
            this.lblHDSurname.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.Surname")});
            this.lblHDSurname.ForeColor = System.Drawing.Color.DimGray;
            this.lblHDSurname.Name = "lblHDSurname";
            this.lblHDSurname.StylePriority.UseForeColor = false;
            this.lblHDSurname.Weight = 0.33901515151515149;
            // 
            // lblHDFirstname
            // 
            this.lblHDFirstname.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.FirstName")});
            this.lblHDFirstname.ForeColor = System.Drawing.Color.DimGray;
            this.lblHDFirstname.Name = "lblHDFirstname";
            this.lblHDFirstname.StylePriority.UseForeColor = false;
            this.lblHDFirstname.Weight = 0.47;
            // 
            // lblHDSecodnName
            // 
            this.lblHDSecodnName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.SecondName")});
            this.lblHDSecodnName.ForeColor = System.Drawing.Color.DimGray;
            this.lblHDSecodnName.Name = "lblHDSecodnName";
            this.lblHDSecodnName.StylePriority.UseForeColor = false;
            this.lblHDSecodnName.Weight = 0.40375;
            // 
            // lblHDInitial
            // 
            this.lblHDInitial.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.Initials")});
            this.lblHDInitial.ForeColor = System.Drawing.Color.DimGray;
            this.lblHDInitial.Name = "lblHDInitial";
            this.lblHDInitial.StylePriority.UseForeColor = false;
            this.lblHDInitial.Weight = 0.18250000000000002;
            // 
            // lblHDTitle
            // 
            this.lblHDTitle.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.TitleDesc")});
            this.lblHDTitle.ForeColor = System.Drawing.Color.DimGray;
            this.lblHDTitle.Name = "lblHDTitle";
            this.lblHDTitle.StylePriority.UseForeColor = false;
            this.lblHDTitle.Weight = 0.21458333333333327;
            // 
            // lblHDIDno
            // 
            this.lblHDIDno.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.IDNo")});
            this.lblHDIDno.ForeColor = System.Drawing.Color.DimGray;
            this.lblHDIDno.Name = "lblHDIDno";
            this.lblHDIDno.StylePriority.UseForeColor = false;
            this.lblHDIDno.Weight = 0.41541666666666671;
            // 
            // lblHDPPNo
            // 
            this.lblHDPPNo.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.PassportNo")});
            this.lblHDPPNo.ForeColor = System.Drawing.Color.DimGray;
            this.lblHDPPNo.Name = "lblHDPPNo";
            this.lblHDPPNo.StylePriority.UseForeColor = false;
            this.lblHDPPNo.Weight = 0.31375;
            // 
            // lblHDBirthDate
            // 
            this.lblHDBirthDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.BirthDate")});
            this.lblHDBirthDate.ForeColor = System.Drawing.Color.DimGray;
            this.lblHDBirthDate.Name = "lblHDBirthDate";
            this.lblHDBirthDate.StylePriority.UseForeColor = false;
            this.lblHDBirthDate.Text = "lblHDBirthDate";
            this.lblHDBirthDate.Weight = 0.3125;
            // 
            // GroupHeader10
            // 
            this.GroupHeader10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblNameHisC,
            this.tblNameHistH,
            this.tblConsInfoH});
            this.GroupHeader10.HeightF = 95F;
            this.GroupHeader10.Name = "GroupHeader10";
            // 
            // tblNameHisC
            // 
            this.tblNameHisC.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tblNameHisC.BorderColor = System.Drawing.Color.White;
            this.tblNameHisC.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblNameHisC.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.tblNameHisC.ForeColor = System.Drawing.Color.Gray;
            this.tblNameHisC.LocationFloat = new DevExpress.Utils.PointFloat(8F, 75F);
            this.tblNameHisC.Name = "tblNameHisC";
            this.tblNameHisC.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblNameHisC.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow64});
            this.tblNameHisC.SizeF = new System.Drawing.SizeF(792F, 20F);
            this.tblNameHisC.StylePriority.UseBackColor = false;
            this.tblNameHisC.StylePriority.UseBorderColor = false;
            this.tblNameHisC.StylePriority.UseBorders = false;
            this.tblNameHisC.StylePriority.UseFont = false;
            this.tblNameHisC.StylePriority.UseForeColor = false;
            this.tblNameHisC.StylePriority.UsePadding = false;
            this.tblNameHisC.StylePriority.UseTextAlignment = false;
            this.tblNameHisC.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow64
            // 
            this.xrTableRow64.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblUpdatedDate,
            this.lblHSurname,
            this.lblHFirstname,
            this.lblHSecodnName,
            this.lblHInitial,
            this.lblHTitle,
            this.lblHIDno,
            this.lblHPPNo,
            this.lblHBirthDate});
            this.xrTableRow64.Name = "xrTableRow64";
            this.xrTableRow64.Weight = 1;
            // 
            // lblUpdatedDate
            // 
            this.lblUpdatedDate.Name = "lblUpdatedDate";
            this.lblUpdatedDate.Text = "Bureau Update";
            this.lblUpdatedDate.Weight = 0.34848484848484851;
            // 
            // lblHSurname
            // 
            this.lblHSurname.Name = "lblHSurname";
            this.lblHSurname.Text = "Surname";
            this.lblHSurname.Weight = 0.33901515151515149;
            // 
            // lblHFirstname
            // 
            this.lblHFirstname.Name = "lblHFirstname";
            this.lblHFirstname.Text = "First Name";
            this.lblHFirstname.Weight = 0.47;
            // 
            // lblHSecodnName
            // 
            this.lblHSecodnName.Name = "lblHSecodnName";
            this.lblHSecodnName.Text = "Second Name";
            this.lblHSecodnName.Weight = 0.40375;
            // 
            // lblHInitial
            // 
            this.lblHInitial.Name = "lblHInitial";
            this.lblHInitial.Text = "Initial";
            this.lblHInitial.Weight = 0.18250000000000002;
            // 
            // lblHTitle
            // 
            this.lblHTitle.Name = "lblHTitle";
            this.lblHTitle.Text = "Title";
            this.lblHTitle.Weight = 0.21458333333333327;
            // 
            // lblHIDno
            // 
            this.lblHIDno.Name = "lblHIDno";
            this.lblHIDno.Text = "ID No";
            this.lblHIDno.Weight = 0.41541666666666671;
            // 
            // lblHPPNo
            // 
            this.lblHPPNo.Name = "lblHPPNo";
            this.lblHPPNo.Text = "Other ID No";
            this.lblHPPNo.Weight = 0.31375;
            // 
            // lblHBirthDate
            // 
            this.lblHBirthDate.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblHBirthDate.Name = "lblHBirthDate";
            this.lblHBirthDate.StylePriority.UseBorders = false;
            this.lblHBirthDate.Text = "Birth Date";
            this.lblHBirthDate.Weight = 0.3125;
            // 
            // tblNameHistH
            // 
            this.tblNameHistH.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblNameHistH.LocationFloat = new DevExpress.Utils.PointFloat(8F, 50F);
            this.tblNameHistH.Name = "tblNameHistH";
            this.tblNameHistH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblNameHistH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow63});
            this.tblNameHistH.SizeF = new System.Drawing.SizeF(792F, 25F);
            this.tblNameHistH.StylePriority.UseFont = false;
            this.tblNameHistH.StylePriority.UsePadding = false;
            // 
            // xrTableRow63
            // 
            this.xrTableRow63.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblNameHistH});
            this.xrTableRow63.ForeColor = System.Drawing.Color.Gray;
            this.xrTableRow63.Name = "xrTableRow63";
            this.xrTableRow63.StylePriority.UseForeColor = false;
            this.xrTableRow63.Weight = 1;
            // 
            // lblNameHistH
            // 
            this.lblNameHistH.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblNameHistH.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.lblNameHistH.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblNameHistH.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.lblNameHistH.Name = "lblNameHistH";
            this.lblNameHistH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblNameHistH.StylePriority.UseBackColor = false;
            this.lblNameHistH.StylePriority.UseBorderColor = false;
            this.lblNameHistH.StylePriority.UseBorders = false;
            this.lblNameHistH.StylePriority.UseFont = false;
            this.lblNameHistH.StylePriority.UsePadding = false;
            this.lblNameHistH.StylePriority.UseTextAlignment = false;
            this.lblNameHistH.Text = "Name History";
            this.lblNameHistH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblNameHistH.Weight = 3;
            // 
            // tblConsInfoH
            // 
            this.tblConsInfoH.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblConsInfoH.LocationFloat = new DevExpress.Utils.PointFloat(8F, 8F);
            this.tblConsInfoH.Name = "tblConsInfoH";
            this.tblConsInfoH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100F);
            this.tblConsInfoH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow62});
            this.tblConsInfoH.SizeF = new System.Drawing.SizeF(792F, 33F);
            this.tblConsInfoH.StylePriority.UseFont = false;
            this.tblConsInfoH.StylePriority.UsePadding = false;
            // 
            // xrTableRow62
            // 
            this.xrTableRow62.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell33});
            this.xrTableRow62.Name = "xrTableRow62";
            this.xrTableRow62.Weight = 1;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.xrTableCell33.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell33.BorderWidth = 2;
            this.xrTableCell33.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell33.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell33.StylePriority.UseBorderColor = false;
            this.xrTableCell33.StylePriority.UseBorders = false;
            this.xrTableCell33.StylePriority.UseBorderWidth = false;
            this.xrTableCell33.StylePriority.UseFont = false;
            this.xrTableCell33.StylePriority.UseForeColor = false;
            this.xrTableCell33.StylePriority.UsePadding = false;
            this.xrTableCell33.StylePriority.UseTextAlignment = false;
            this.xrTableCell33.Text = "Consumer Information";
            this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell33.Weight = 3;
            // 
            // AddressHistory
            // 
            this.AddressHistory.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailAddressHistory,
            this.GroupHeader11});
            this.AddressHistory.DataMember = "ConsumerAddressHistory";
            this.AddressHistory.Level = 19;
            this.AddressHistory.Name = "AddressHistory";
            // 
            // DetailAddressHistory
            // 
            this.DetailAddressHistory.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblAddHD});
            this.DetailAddressHistory.HeightF = 20F;
            this.DetailAddressHistory.Name = "DetailAddressHistory";
            // 
            // tblAddHD
            // 
            this.tblAddHD.BackColor = System.Drawing.Color.White;
            this.tblAddHD.BorderColor = System.Drawing.Color.Gainsboro;
            this.tblAddHD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.tblAddHD.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.tblAddHD.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tblAddHD.LocationFloat = new DevExpress.Utils.PointFloat(8F, 0F);
            this.tblAddHD.Name = "tblAddHD";
            this.tblAddHD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblAddHD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow68});
            this.tblAddHD.SizeF = new System.Drawing.SizeF(792F, 20F);
            this.tblAddHD.StylePriority.UseBackColor = false;
            this.tblAddHD.StylePriority.UseBorderColor = false;
            this.tblAddHD.StylePriority.UseBorders = false;
            this.tblAddHD.StylePriority.UseFont = false;
            this.tblAddHD.StylePriority.UseForeColor = false;
            this.tblAddHD.StylePriority.UsePadding = false;
            this.tblAddHD.StylePriority.UseTextAlignment = false;
            this.tblAddHD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow68
            // 
            this.xrTableRow68.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDAddUpdatedate,
            this.lblDAddtype,
            this.lblDAddline1,
            this.lblDAddLine2,
            this.lblDAddLine3,
            this.lblDAddLine4,
            this.lblDPostalcode});
            this.xrTableRow68.Name = "xrTableRow68";
            this.xrTableRow68.Weight = 1;
            // 
            // lblDAddUpdatedate
            // 
            this.lblDAddUpdatedate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.LastUpdatedDate")});
            this.lblDAddUpdatedate.ForeColor = System.Drawing.Color.DimGray;
            this.lblDAddUpdatedate.Name = "lblDAddUpdatedate";
            this.lblDAddUpdatedate.StylePriority.UseForeColor = false;
            this.lblDAddUpdatedate.Text = "lblDAddUpdatedate";
            this.lblDAddUpdatedate.Weight = 0.31439393939393934;
            // 
            // lblDAddtype
            // 
            this.lblDAddtype.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.AddressType")});
            this.lblDAddtype.ForeColor = System.Drawing.Color.DimGray;
            this.lblDAddtype.Name = "lblDAddtype";
            this.lblDAddtype.StylePriority.UseForeColor = false;
            this.lblDAddtype.Weight = 0.35227272727272729;
            // 
            // lblDAddline1
            // 
            this.lblDAddline1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.Address1")});
            this.lblDAddline1.ForeColor = System.Drawing.Color.DimGray;
            this.lblDAddline1.Name = "lblDAddline1";
            this.lblDAddline1.StylePriority.UseForeColor = false;
            this.lblDAddline1.Weight = 0.78409090909090906;
            // 
            // lblDAddLine2
            // 
            this.lblDAddLine2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.Address2")});
            this.lblDAddLine2.ForeColor = System.Drawing.Color.DimGray;
            this.lblDAddLine2.Name = "lblDAddLine2";
            this.lblDAddLine2.StylePriority.UseForeColor = false;
            this.lblDAddLine2.Weight = 0.47727272727272729;
            // 
            // lblDAddLine3
            // 
            this.lblDAddLine3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.Address3")});
            this.lblDAddLine3.ForeColor = System.Drawing.Color.DimGray;
            this.lblDAddLine3.Name = "lblDAddLine3";
            this.lblDAddLine3.StylePriority.UseForeColor = false;
            this.lblDAddLine3.Weight = 0.44318181818181823;
            // 
            // lblDAddLine4
            // 
            this.lblDAddLine4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.Address4")});
            this.lblDAddLine4.ForeColor = System.Drawing.Color.DimGray;
            this.lblDAddLine4.Name = "lblDAddLine4";
            this.lblDAddLine4.StylePriority.UseForeColor = false;
            this.lblDAddLine4.Weight = 0.37878787878787878;
            // 
            // lblDPostalcode
            // 
            this.lblDPostalcode.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.PostalCode")});
            this.lblDPostalcode.ForeColor = System.Drawing.Color.DimGray;
            this.lblDPostalcode.Name = "lblDPostalcode";
            this.lblDPostalcode.StylePriority.UseForeColor = false;
            this.lblDPostalcode.Weight = 0.25;
            // 
            // GroupHeader11
            // 
            this.GroupHeader11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblAddHC,
            this.tblAddHistoryH});
            this.GroupHeader11.HeightF = 67F;
            this.GroupHeader11.Name = "GroupHeader11";
            // 
            // tblAddHC
            // 
            this.tblAddHC.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tblAddHC.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblAddHC.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblAddHC.ForeColor = System.Drawing.Color.Gray;
            this.tblAddHC.LocationFloat = new DevExpress.Utils.PointFloat(8F, 34F);
            this.tblAddHC.Name = "tblAddHC";
            this.tblAddHC.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblAddHC.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow67});
            this.tblAddHC.SizeF = new System.Drawing.SizeF(792F, 33F);
            this.tblAddHC.StylePriority.UseBorderColor = false;
            this.tblAddHC.StylePriority.UseBorders = false;
            this.tblAddHC.StylePriority.UseForeColor = false;
            this.tblAddHC.StylePriority.UsePadding = false;
            this.tblAddHC.StylePriority.UseTextAlignment = false;
            this.tblAddHC.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow67
            // 
            this.xrTableRow67.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableRow67.BorderColor = System.Drawing.Color.White;
            this.xrTableRow67.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblAddUpdatedate,
            this.lblAddtype,
            this.lblAddline1,
            this.lblAddLine2,
            this.lblAddLine3,
            this.lblAddLine4,
            this.lblPostalcode});
            this.xrTableRow67.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableRow67.Name = "xrTableRow67";
            this.xrTableRow67.StylePriority.UseBackColor = false;
            this.xrTableRow67.StylePriority.UseBorderColor = false;
            this.xrTableRow67.StylePriority.UseFont = false;
            this.xrTableRow67.StylePriority.UseTextAlignment = false;
            this.xrTableRow67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow67.Weight = 1;
            // 
            // lblAddUpdatedate
            // 
            this.lblAddUpdatedate.CanGrow = false;
            this.lblAddUpdatedate.Multiline = true;
            this.lblAddUpdatedate.Name = "lblAddUpdatedate";
            this.lblAddUpdatedate.Text = "Bureau Update";
            this.lblAddUpdatedate.Weight = 0.31439393939393934;
            // 
            // lblAddtype
            // 
            this.lblAddtype.CanGrow = false;
            this.lblAddtype.Multiline = true;
            this.lblAddtype.Name = "lblAddtype";
            this.lblAddtype.Text = "Type";
            this.lblAddtype.Weight = 0.35227272727272729;
            // 
            // lblAddline1
            // 
            this.lblAddline1.CanGrow = false;
            this.lblAddline1.Multiline = true;
            this.lblAddline1.Name = "lblAddline1";
            this.lblAddline1.Text = "Line1";
            this.lblAddline1.Weight = 0.78409090909090906;
            // 
            // lblAddLine2
            // 
            this.lblAddLine2.CanGrow = false;
            this.lblAddLine2.Multiline = true;
            this.lblAddLine2.Name = "lblAddLine2";
            this.lblAddLine2.Text = "Line2";
            this.lblAddLine2.Weight = 0.47727272727272729;
            // 
            // lblAddLine3
            // 
            this.lblAddLine3.CanGrow = false;
            this.lblAddLine3.Multiline = true;
            this.lblAddLine3.Name = "lblAddLine3";
            this.lblAddLine3.Text = "Line 3";
            this.lblAddLine3.Weight = 0.44318181818181823;
            // 
            // lblAddLine4
            // 
            this.lblAddLine4.CanGrow = false;
            this.lblAddLine4.Multiline = true;
            this.lblAddLine4.Name = "lblAddLine4";
            this.lblAddLine4.Text = "Line 4";
            this.lblAddLine4.Weight = 0.37878787878787878;
            // 
            // lblPostalcode
            // 
            this.lblPostalcode.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblPostalcode.CanGrow = false;
            this.lblPostalcode.Multiline = true;
            this.lblPostalcode.Name = "lblPostalcode";
            this.lblPostalcode.StylePriority.UseBorders = false;
            this.lblPostalcode.Text = "Postal Code";
            this.lblPostalcode.Weight = 0.25;
            // 
            // tblAddHistoryH
            // 
            this.tblAddHistoryH.ForeColor = System.Drawing.Color.Gray;
            this.tblAddHistoryH.LocationFloat = new DevExpress.Utils.PointFloat(8F, 8F);
            this.tblAddHistoryH.Name = "tblAddHistoryH";
            this.tblAddHistoryH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow66});
            this.tblAddHistoryH.SizeF = new System.Drawing.SizeF(792F, 25F);
            this.tblAddHistoryH.StylePriority.UseForeColor = false;
            // 
            // xrTableRow66
            // 
            this.xrTableRow66.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblAddHistoryH});
            this.xrTableRow66.Name = "xrTableRow66";
            this.xrTableRow66.Weight = 1;
            // 
            // lblAddHistoryH
            // 
            this.lblAddHistoryH.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblAddHistoryH.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.lblAddHistoryH.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblAddHistoryH.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.lblAddHistoryH.ForeColor = System.Drawing.Color.Gray;
            this.lblAddHistoryH.Name = "lblAddHistoryH";
            this.lblAddHistoryH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblAddHistoryH.StylePriority.UseBackColor = false;
            this.lblAddHistoryH.StylePriority.UseBorderColor = false;
            this.lblAddHistoryH.StylePriority.UseBorders = false;
            this.lblAddHistoryH.StylePriority.UseFont = false;
            this.lblAddHistoryH.StylePriority.UseForeColor = false;
            this.lblAddHistoryH.StylePriority.UsePadding = false;
            this.lblAddHistoryH.StylePriority.UseTextAlignment = false;
            this.lblAddHistoryH.Text = "Address History";
            this.lblAddHistoryH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblAddHistoryH.Weight = 3;
            // 
            // TelephoneHistory
            // 
            this.TelephoneHistory.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailTelephoneHistory,
            this.GroupHeader12});
            this.TelephoneHistory.DataMember = "ConsumerTelephoneHistory";
            this.TelephoneHistory.Level = 20;
            this.TelephoneHistory.Name = "TelephoneHistory";
            // 
            // DetailTelephoneHistory
            // 
            this.DetailTelephoneHistory.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblContactNoD});
            this.DetailTelephoneHistory.HeightF = 20F;
            this.DetailTelephoneHistory.Name = "DetailTelephoneHistory";
            // 
            // tblContactNoD
            // 
            this.tblContactNoD.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblContactNoD.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblContactNoD.LocationFloat = new DevExpress.Utils.PointFloat(8F, 0F);
            this.tblContactNoD.Name = "tblContactNoD";
            this.tblContactNoD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblContactNoD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow71});
            this.tblContactNoD.SizeF = new System.Drawing.SizeF(792F, 20F);
            this.tblContactNoD.StylePriority.UseBorderColor = false;
            this.tblContactNoD.StylePriority.UseBorders = false;
            this.tblContactNoD.StylePriority.UsePadding = false;
            // 
            // xrTableRow71
            // 
            this.xrTableRow71.BackColor = System.Drawing.Color.White;
            this.xrTableRow71.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableRow71.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow71.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDNoUpdatedate,
            this.lblDTelnoType,
            this.lblDTelNo,
            this.lblDEmail});
            this.xrTableRow71.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableRow71.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.xrTableRow71.Name = "xrTableRow71";
            this.xrTableRow71.StylePriority.UseBackColor = false;
            this.xrTableRow71.StylePriority.UseBorderColor = false;
            this.xrTableRow71.StylePriority.UseBorders = false;
            this.xrTableRow71.StylePriority.UseFont = false;
            this.xrTableRow71.StylePriority.UseForeColor = false;
            this.xrTableRow71.StylePriority.UseTextAlignment = false;
            this.xrTableRow71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow71.Weight = 1;
            // 
            // lblDNoUpdatedate
            // 
            this.lblDNoUpdatedate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneHistory.LastUpdatedDate")});
            this.lblDNoUpdatedate.ForeColor = System.Drawing.Color.DimGray;
            this.lblDNoUpdatedate.Name = "lblDNoUpdatedate";
            this.lblDNoUpdatedate.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDNoUpdatedate.StylePriority.UseForeColor = false;
            this.lblDNoUpdatedate.StylePriority.UsePadding = false;
            this.lblDNoUpdatedate.Weight = 0.37878787878787878;
            // 
            // lblDTelnoType
            // 
            this.lblDTelnoType.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneHistory.TelephoneType")});
            this.lblDTelnoType.ForeColor = System.Drawing.Color.DimGray;
            this.lblDTelnoType.Name = "lblDTelnoType";
            this.lblDTelnoType.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDTelnoType.StylePriority.UseForeColor = false;
            this.lblDTelnoType.StylePriority.UsePadding = false;
            this.lblDTelnoType.Text = "lblDTelnoType";
            this.lblDTelnoType.Weight = 0.56818181818181823;
            // 
            // lblDTelNo
            // 
            this.lblDTelNo.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneHistory.TelephoneNo")});
            this.lblDTelNo.ForeColor = System.Drawing.Color.DimGray;
            this.lblDTelNo.Name = "lblDTelNo";
            this.lblDTelNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDTelNo.StylePriority.UseForeColor = false;
            this.lblDTelNo.StylePriority.UsePadding = false;
            this.lblDTelNo.Weight = 0.78787878787878762;
            // 
            // lblDEmail
            // 
            this.lblDEmail.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneHistory.EmailAddress")});
            this.lblDEmail.ForeColor = System.Drawing.Color.DimGray;
            this.lblDEmail.Name = "lblDEmail";
            this.lblDEmail.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDEmail.StylePriority.UseForeColor = false;
            this.lblDEmail.StylePriority.UsePadding = false;
            this.lblDEmail.Weight = 1.2651515151515151;
            // 
            // GroupHeader12
            // 
            this.GroupHeader12.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblContactNoC,
            this.tblContactNoH});
            this.GroupHeader12.HeightF = 54F;
            this.GroupHeader12.Name = "GroupHeader12";
            // 
            // tblContactNoC
            // 
            this.tblContactNoC.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tblContactNoC.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tblContactNoC.BorderColor = System.Drawing.Color.White;
            this.tblContactNoC.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblContactNoC.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.tblContactNoC.ForeColor = System.Drawing.Color.Gray;
            this.tblContactNoC.LocationFloat = new DevExpress.Utils.PointFloat(8F, 34F);
            this.tblContactNoC.Name = "tblContactNoC";
            this.tblContactNoC.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblContactNoC.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow70});
            this.tblContactNoC.SizeF = new System.Drawing.SizeF(792F, 20F);
            this.tblContactNoC.StylePriority.UseBackColor = false;
            this.tblContactNoC.StylePriority.UseBorderColor = false;
            this.tblContactNoC.StylePriority.UseBorders = false;
            this.tblContactNoC.StylePriority.UseFont = false;
            this.tblContactNoC.StylePriority.UseForeColor = false;
            this.tblContactNoC.StylePriority.UsePadding = false;
            // 
            // xrTableRow70
            // 
            this.xrTableRow70.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblNoUpdatedate,
            this.lblTelnoType,
            this.lblTelNo,
            this.lblEmail});
            this.xrTableRow70.Name = "xrTableRow70";
            this.xrTableRow70.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100F);
            this.xrTableRow70.StylePriority.UsePadding = false;
            this.xrTableRow70.StylePriority.UseTextAlignment = false;
            this.xrTableRow70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow70.Weight = 1;
            // 
            // lblNoUpdatedate
            // 
            this.lblNoUpdatedate.CanGrow = false;
            this.lblNoUpdatedate.Name = "lblNoUpdatedate";
            this.lblNoUpdatedate.Text = "Bureau Update";
            this.lblNoUpdatedate.Weight = 0.37878787878787878;
            // 
            // lblTelnoType
            // 
            this.lblTelnoType.CanGrow = false;
            this.lblTelnoType.Name = "lblTelnoType";
            this.lblTelnoType.Text = "Type";
            this.lblTelnoType.Weight = 0.56818181818181823;
            // 
            // lblTelNo
            // 
            this.lblTelNo.CanGrow = false;
            this.lblTelNo.Name = "lblTelNo";
            this.lblTelNo.Text = "Telephone No";
            this.lblTelNo.Weight = 0.78787878787878785;
            // 
            // lblEmail
            // 
            this.lblEmail.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblEmail.CanGrow = false;
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.StylePriority.UseBorders = false;
            this.lblEmail.Text = "Email Address";
            this.lblEmail.Weight = 1.2651515151515151;
            // 
            // tblContactNoH
            // 
            this.tblContactNoH.ForeColor = System.Drawing.Color.Black;
            this.tblContactNoH.LocationFloat = new DevExpress.Utils.PointFloat(8F, 8F);
            this.tblContactNoH.Name = "tblContactNoH";
            this.tblContactNoH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow69});
            this.tblContactNoH.SizeF = new System.Drawing.SizeF(792F, 25F);
            this.tblContactNoH.StylePriority.UseForeColor = false;
            // 
            // xrTableRow69
            // 
            this.xrTableRow69.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblContactNoH});
            this.xrTableRow69.Name = "xrTableRow69";
            this.xrTableRow69.Weight = 1;
            // 
            // lblContactNoH
            // 
            this.lblContactNoH.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblContactNoH.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.lblContactNoH.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblContactNoH.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.lblContactNoH.ForeColor = System.Drawing.Color.Gray;
            this.lblContactNoH.Name = "lblContactNoH";
            this.lblContactNoH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblContactNoH.StylePriority.UseBackColor = false;
            this.lblContactNoH.StylePriority.UseBorderColor = false;
            this.lblContactNoH.StylePriority.UseBorders = false;
            this.lblContactNoH.StylePriority.UseFont = false;
            this.lblContactNoH.StylePriority.UseForeColor = false;
            this.lblContactNoH.StylePriority.UsePadding = false;
            this.lblContactNoH.StylePriority.UseTextAlignment = false;
            this.lblContactNoH.Text = "Contact No.History";
            this.lblContactNoH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblContactNoH.Weight = 3;
            // 
            // EmploymentHistory
            // 
            this.EmploymentHistory.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailEmploymentHistory,
            this.GroupHeader13});
            this.EmploymentHistory.DataMember = "ConsumerEmploymentHistory";
            this.EmploymentHistory.Level = 21;
            this.EmploymentHistory.Name = "EmploymentHistory";
            // 
            // DetailEmploymentHistory
            // 
            this.DetailEmploymentHistory.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblEmploymentD});
            this.DetailEmploymentHistory.HeightF = 20F;
            this.DetailEmploymentHistory.Name = "DetailEmploymentHistory";
            // 
            // tblEmploymentD
            // 
            this.tblEmploymentD.BackColor = System.Drawing.Color.White;
            this.tblEmploymentD.BorderColor = System.Drawing.Color.Gainsboro;
            this.tblEmploymentD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.tblEmploymentD.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.tblEmploymentD.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tblEmploymentD.LocationFloat = new DevExpress.Utils.PointFloat(8F, 0F);
            this.tblEmploymentD.Name = "tblEmploymentD";
            this.tblEmploymentD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblEmploymentD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow74});
            this.tblEmploymentD.SizeF = new System.Drawing.SizeF(792F, 20F);
            this.tblEmploymentD.StylePriority.UseBackColor = false;
            this.tblEmploymentD.StylePriority.UseBorderColor = false;
            this.tblEmploymentD.StylePriority.UseBorders = false;
            this.tblEmploymentD.StylePriority.UseFont = false;
            this.tblEmploymentD.StylePriority.UseForeColor = false;
            this.tblEmploymentD.StylePriority.UsePadding = false;
            // 
            // xrTableRow74
            // 
            this.xrTableRow74.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableRow74.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableRow74.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow74.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDEmpUpdatedate,
            this.lblDEmployer,
            this.lblDDesignation});
            this.xrTableRow74.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.xrTableRow74.Name = "xrTableRow74";
            this.xrTableRow74.StylePriority.UseBackColor = false;
            this.xrTableRow74.StylePriority.UseBorderColor = false;
            this.xrTableRow74.StylePriority.UseBorders = false;
            this.xrTableRow74.StylePriority.UseForeColor = false;
            this.xrTableRow74.StylePriority.UseTextAlignment = false;
            this.xrTableRow74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow74.Weight = 0.8;
            // 
            // lblDEmpUpdatedate
            // 
            this.lblDEmpUpdatedate.BackColor = System.Drawing.Color.White;
            this.lblDEmpUpdatedate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerEmploymentHistory.LastUpdatedDate")});
            this.lblDEmpUpdatedate.ForeColor = System.Drawing.Color.DimGray;
            this.lblDEmpUpdatedate.Name = "lblDEmpUpdatedate";
            this.lblDEmpUpdatedate.StylePriority.UseBackColor = false;
            this.lblDEmpUpdatedate.StylePriority.UseForeColor = false;
            this.lblDEmpUpdatedate.Weight = 0.47348484848484851;
            // 
            // lblDEmployer
            // 
            this.lblDEmployer.BackColor = System.Drawing.Color.White;
            this.lblDEmployer.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerEmploymentHistory.EmployerDetail")});
            this.lblDEmployer.ForeColor = System.Drawing.Color.DimGray;
            this.lblDEmployer.Name = "lblDEmployer";
            this.lblDEmployer.StylePriority.UseBackColor = false;
            this.lblDEmployer.StylePriority.UseForeColor = false;
            this.lblDEmployer.Weight = 1.4507575757575757;
            // 
            // lblDDesignation
            // 
            this.lblDDesignation.BackColor = System.Drawing.Color.White;
            this.lblDDesignation.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerEmploymentHistory.Designation")});
            this.lblDDesignation.ForeColor = System.Drawing.Color.DimGray;
            this.lblDDesignation.Name = "lblDDesignation";
            this.lblDDesignation.StylePriority.UseBackColor = false;
            this.lblDDesignation.StylePriority.UseForeColor = false;
            this.lblDDesignation.Weight = 1.0757575757575757;
            // 
            // GroupHeader13
            // 
            this.GroupHeader13.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblEmploymentC,
            this.tblEmploymentH});
            this.GroupHeader13.HeightF = 56F;
            this.GroupHeader13.Name = "GroupHeader13";
            // 
            // tblEmploymentC
            // 
            this.tblEmploymentC.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tblEmploymentC.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tblEmploymentC.BorderColor = System.Drawing.Color.White;
            this.tblEmploymentC.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblEmploymentC.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.tblEmploymentC.ForeColor = System.Drawing.Color.Gray;
            this.tblEmploymentC.LocationFloat = new DevExpress.Utils.PointFloat(8F, 36F);
            this.tblEmploymentC.Name = "tblEmploymentC";
            this.tblEmploymentC.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblEmploymentC.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow73});
            this.tblEmploymentC.SizeF = new System.Drawing.SizeF(792F, 20F);
            this.tblEmploymentC.StylePriority.UseBackColor = false;
            this.tblEmploymentC.StylePriority.UseBorderColor = false;
            this.tblEmploymentC.StylePriority.UseBorders = false;
            this.tblEmploymentC.StylePriority.UseFont = false;
            this.tblEmploymentC.StylePriority.UseForeColor = false;
            this.tblEmploymentC.StylePriority.UsePadding = false;
            this.tblEmploymentC.StylePriority.UseTextAlignment = false;
            this.tblEmploymentC.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow73
            // 
            this.xrTableRow73.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblEmpUpdatedate,
            this.lblEmployer,
            this.lblDesignation});
            this.xrTableRow73.Name = "xrTableRow73";
            this.xrTableRow73.Weight = 1;
            // 
            // lblEmpUpdatedate
            // 
            this.lblEmpUpdatedate.CanGrow = false;
            this.lblEmpUpdatedate.Name = "lblEmpUpdatedate";
            this.lblEmpUpdatedate.Text = "Bureau Update";
            this.lblEmpUpdatedate.Weight = 0.47348484848484851;
            // 
            // lblEmployer
            // 
            this.lblEmployer.CanGrow = false;
            this.lblEmployer.Name = "lblEmployer";
            this.lblEmployer.Text = "Employer";
            this.lblEmployer.Weight = 1.4507575757575757;
            // 
            // lblDesignation
            // 
            this.lblDesignation.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDesignation.CanGrow = false;
            this.lblDesignation.Name = "lblDesignation";
            this.lblDesignation.StylePriority.UseBorders = false;
            this.lblDesignation.Text = "Designation";
            this.lblDesignation.Weight = 1.0757575757575757;
            // 
            // tblEmploymentH
            // 
            this.tblEmploymentH.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.tblEmploymentH.ForeColor = System.Drawing.Color.Black;
            this.tblEmploymentH.LocationFloat = new DevExpress.Utils.PointFloat(8F, 8F);
            this.tblEmploymentH.Name = "tblEmploymentH";
            this.tblEmploymentH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow72});
            this.tblEmploymentH.SizeF = new System.Drawing.SizeF(792F, 25F);
            this.tblEmploymentH.StylePriority.UseFont = false;
            this.tblEmploymentH.StylePriority.UseForeColor = false;
            this.tblEmploymentH.StylePriority.UseTextAlignment = false;
            this.tblEmploymentH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow72
            // 
            this.xrTableRow72.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblEmploymentH});
            this.xrTableRow72.Name = "xrTableRow72";
            this.xrTableRow72.Weight = 1;
            // 
            // lblEmploymentH
            // 
            this.lblEmploymentH.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblEmploymentH.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.lblEmploymentH.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblEmploymentH.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.lblEmploymentH.ForeColor = System.Drawing.Color.Gray;
            this.lblEmploymentH.Name = "lblEmploymentH";
            this.lblEmploymentH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblEmploymentH.StylePriority.UseBackColor = false;
            this.lblEmploymentH.StylePriority.UseBorderColor = false;
            this.lblEmploymentH.StylePriority.UseBorders = false;
            this.lblEmploymentH.StylePriority.UseFont = false;
            this.lblEmploymentH.StylePriority.UseForeColor = false;
            this.lblEmploymentH.StylePriority.UsePadding = false;
            this.lblEmploymentH.Text = "Employment History";
            this.lblEmploymentH.Weight = 3;
            // 
            // AccountStatus
            // 
            this.AccountStatus.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailAccountStatus,
            this.GroupHeader14});
            this.AccountStatus.DataMember = "ConsumerAccountStatus";
            this.AccountStatus.Level = 5;
            this.AccountStatus.Name = "AccountStatus";
            // 
            // DetailAccountStatus
            // 
            this.DetailAccountStatus.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblAccStatusD});
            this.DetailAccountStatus.HeightF = 20F;
            this.DetailAccountStatus.Name = "DetailAccountStatus";
            // 
            // tblAccStatusD
            // 
            this.tblAccStatusD.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblAccStatusD.Font = new System.Drawing.Font("Arial", 8.25F);
            this.tblAccStatusD.LocationFloat = new DevExpress.Utils.PointFloat(8F, 0F);
            this.tblAccStatusD.Name = "tblAccStatusD";
            this.tblAccStatusD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblAccStatusD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow76});
            this.tblAccStatusD.SizeF = new System.Drawing.SizeF(792F, 20F);
            this.tblAccStatusD.StylePriority.UseBorderColor = false;
            this.tblAccStatusD.StylePriority.UseFont = false;
            this.tblAccStatusD.StylePriority.UsePadding = false;
            // 
            // xrTableRow76
            // 
            this.xrTableRow76.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableRow76.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow76.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblAccOpenDateD,
            this.lblCompanyD,
            this.lblCreditLimitD,
            this.lblCurrBalanceD,
            this.lblInstallementD,
            this.lblArrearsAmtD,
            this.lblAccTypeD});
            this.xrTableRow76.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableRow76.Name = "xrTableRow76";
            this.xrTableRow76.StylePriority.UseBorderColor = false;
            this.xrTableRow76.StylePriority.UseBorders = false;
            this.xrTableRow76.StylePriority.UseForeColor = false;
            this.xrTableRow76.Weight = 0.8;
            // 
            // lblAccOpenDateD
            // 
            this.lblAccOpenDateD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblAccOpenDateD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountStatus.AccountOpenedDate")});
            this.lblAccOpenDateD.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblAccOpenDateD.ForeColor = System.Drawing.Color.DimGray;
            this.lblAccOpenDateD.Name = "lblAccOpenDateD";
            this.lblAccOpenDateD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblAccOpenDateD.StylePriority.UseBorders = false;
            this.lblAccOpenDateD.StylePriority.UseFont = false;
            this.lblAccOpenDateD.StylePriority.UseForeColor = false;
            this.lblAccOpenDateD.StylePriority.UsePadding = false;
            this.lblAccOpenDateD.StylePriority.UseTextAlignment = false;
            this.lblAccOpenDateD.Text = "lblAccOpenDateD";
            this.lblAccOpenDateD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAccOpenDateD.Weight = 0.38461538461538458;
            // 
            // lblCompanyD
            // 
            this.lblCompanyD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblCompanyD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountStatus.SubscriberName")});
            this.lblCompanyD.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblCompanyD.ForeColor = System.Drawing.Color.DimGray;
            this.lblCompanyD.Name = "lblCompanyD";
            this.lblCompanyD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblCompanyD.StylePriority.UseBorders = false;
            this.lblCompanyD.StylePriority.UseFont = false;
            this.lblCompanyD.StylePriority.UseForeColor = false;
            this.lblCompanyD.StylePriority.UsePadding = false;
            this.lblCompanyD.StylePriority.UseTextAlignment = false;
            this.lblCompanyD.Text = "xrTableCell15";
            this.lblCompanyD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblCompanyD.Weight = 0.73076923076923084;
            // 
            // lblCreditLimitD
            // 
            this.lblCreditLimitD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblCreditLimitD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountStatus.CreditLimitAmt")});
            this.lblCreditLimitD.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblCreditLimitD.ForeColor = System.Drawing.Color.DimGray;
            this.lblCreditLimitD.Name = "lblCreditLimitD";
            this.lblCreditLimitD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblCreditLimitD.StylePriority.UseBorders = false;
            this.lblCreditLimitD.StylePriority.UseFont = false;
            this.lblCreditLimitD.StylePriority.UseForeColor = false;
            this.lblCreditLimitD.StylePriority.UsePadding = false;
            this.lblCreditLimitD.StylePriority.UseTextAlignment = false;
            this.lblCreditLimitD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblCreditLimitD.Weight = 0.42615384615384611;
            // 
            // lblCurrBalanceD
            // 
            this.lblCurrBalanceD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblCurrBalanceD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountStatus.CurrentBalanceAmt")});
            this.lblCurrBalanceD.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblCurrBalanceD.ForeColor = System.Drawing.Color.DimGray;
            this.lblCurrBalanceD.Name = "lblCurrBalanceD";
            this.lblCurrBalanceD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblCurrBalanceD.StylePriority.UseBorders = false;
            this.lblCurrBalanceD.StylePriority.UseFont = false;
            this.lblCurrBalanceD.StylePriority.UseForeColor = false;
            this.lblCurrBalanceD.StylePriority.UsePadding = false;
            this.lblCurrBalanceD.StylePriority.UseTextAlignment = false;
            this.lblCurrBalanceD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblCurrBalanceD.Weight = 0.38461538461538464;
            // 
            // lblInstallementD
            // 
            this.lblInstallementD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblInstallementD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountStatus.MonthlyInstalmentAmt")});
            this.lblInstallementD.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblInstallementD.ForeColor = System.Drawing.Color.DimGray;
            this.lblInstallementD.Name = "lblInstallementD";
            this.lblInstallementD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblInstallementD.StylePriority.UseBorders = false;
            this.lblInstallementD.StylePriority.UseFont = false;
            this.lblInstallementD.StylePriority.UseForeColor = false;
            this.lblInstallementD.StylePriority.UsePadding = false;
            this.lblInstallementD.StylePriority.UseTextAlignment = false;
            this.lblInstallementD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblInstallementD.Weight = 0.34769230769230763;
            // 
            // lblArrearsAmtD
            // 
            this.lblArrearsAmtD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblArrearsAmtD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountStatus.ArrearsAmt")});
            this.lblArrearsAmtD.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblArrearsAmtD.ForeColor = System.Drawing.Color.DimGray;
            this.lblArrearsAmtD.Name = "lblArrearsAmtD";
            this.lblArrearsAmtD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblArrearsAmtD.StylePriority.UseBorders = false;
            this.lblArrearsAmtD.StylePriority.UseFont = false;
            this.lblArrearsAmtD.StylePriority.UseForeColor = false;
            this.lblArrearsAmtD.StylePriority.UsePadding = false;
            this.lblArrearsAmtD.StylePriority.UseTextAlignment = false;
            this.lblArrearsAmtD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblArrearsAmtD.Weight = 0.34692307692307689;
            // 
            // lblAccTypeD
            // 
            this.lblAccTypeD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblAccTypeD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountStatus.AccountType")});
            this.lblAccTypeD.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblAccTypeD.ForeColor = System.Drawing.Color.DimGray;
            this.lblAccTypeD.Name = "lblAccTypeD";
            this.lblAccTypeD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblAccTypeD.StylePriority.UseBorders = false;
            this.lblAccTypeD.StylePriority.UseFont = false;
            this.lblAccTypeD.StylePriority.UseForeColor = false;
            this.lblAccTypeD.StylePriority.UsePadding = false;
            this.lblAccTypeD.StylePriority.UseTextAlignment = false;
            this.lblAccTypeD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAccTypeD.Weight = 0.37923076923076926;
            // 
            // GroupHeader14
            // 
            this.GroupHeader14.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblAccStatusC,
            this.tblAccStatusH});
            this.GroupHeader14.HeightF = 83F;
            this.GroupHeader14.Name = "GroupHeader14";
            // 
            // tblAccStatusC
            // 
            this.tblAccStatusC.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tblAccStatusC.BorderColor = System.Drawing.Color.White;
            this.tblAccStatusC.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblAccStatusC.Font = new System.Drawing.Font("Arial", 8.25F);
            this.tblAccStatusC.ForeColor = System.Drawing.Color.Gray;
            this.tblAccStatusC.LocationFloat = new DevExpress.Utils.PointFloat(8F, 50F);
            this.tblAccStatusC.Name = "tblAccStatusC";
            this.tblAccStatusC.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblAccStatusC.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow77});
            this.tblAccStatusC.SizeF = new System.Drawing.SizeF(792F, 33F);
            this.tblAccStatusC.StylePriority.UseBackColor = false;
            this.tblAccStatusC.StylePriority.UseBorderColor = false;
            this.tblAccStatusC.StylePriority.UseBorders = false;
            this.tblAccStatusC.StylePriority.UseFont = false;
            this.tblAccStatusC.StylePriority.UseForeColor = false;
            this.tblAccStatusC.StylePriority.UsePadding = false;
            // 
            // xrTableRow77
            // 
            this.xrTableRow77.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblAccOpenDate,
            this.lblCompany,
            this.lblCreditLimit,
            this.lblCurrBalance,
            this.lblInstallement,
            this.lblArrearsAmt,
            this.lblAccType});
            this.xrTableRow77.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableRow77.Name = "xrTableRow77";
            this.xrTableRow77.StylePriority.UseFont = false;
            this.xrTableRow77.Weight = 1.36;
            // 
            // lblAccOpenDate
            // 
            this.lblAccOpenDate.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblAccOpenDate.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblAccOpenDate.Name = "lblAccOpenDate";
            this.lblAccOpenDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblAccOpenDate.StylePriority.UseBorders = false;
            this.lblAccOpenDate.StylePriority.UseFont = false;
            this.lblAccOpenDate.StylePriority.UsePadding = false;
            this.lblAccOpenDate.StylePriority.UseTextAlignment = false;
            this.lblAccOpenDate.Text = "Date Account Opened";
            this.lblAccOpenDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAccOpenDate.Weight = 0.38461538461538458;
            // 
            // lblCompany
            // 
            this.lblCompany.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblCompany.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblCompany.StylePriority.UseBorders = false;
            this.lblCompany.StylePriority.UseFont = false;
            this.lblCompany.StylePriority.UsePadding = false;
            this.lblCompany.StylePriority.UseTextAlignment = false;
            this.lblCompany.Text = "Company";
            this.lblCompany.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblCompany.Weight = 0.73076923076923084;
            // 
            // lblCreditLimit
            // 
            this.lblCreditLimit.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblCreditLimit.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblCreditLimit.Name = "lblCreditLimit";
            this.lblCreditLimit.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblCreditLimit.StylePriority.UseBorders = false;
            this.lblCreditLimit.StylePriority.UseFont = false;
            this.lblCreditLimit.StylePriority.UsePadding = false;
            this.lblCreditLimit.StylePriority.UseTextAlignment = false;
            this.lblCreditLimit.Text = "Account Credit Limit";
            this.lblCreditLimit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblCreditLimit.Weight = 0.42615384615384616;
            // 
            // lblCurrBalance
            // 
            this.lblCurrBalance.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblCurrBalance.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblCurrBalance.Name = "lblCurrBalance";
            this.lblCurrBalance.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblCurrBalance.StylePriority.UseBorders = false;
            this.lblCurrBalance.StylePriority.UseFont = false;
            this.lblCurrBalance.StylePriority.UsePadding = false;
            this.lblCurrBalance.StylePriority.UseTextAlignment = false;
            this.lblCurrBalance.Text = "Current Balance";
            this.lblCurrBalance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblCurrBalance.Weight = 0.38461538461538458;
            // 
            // lblInstallement
            // 
            this.lblInstallement.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblInstallement.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblInstallement.Name = "lblInstallement";
            this.lblInstallement.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblInstallement.StylePriority.UseBorders = false;
            this.lblInstallement.StylePriority.UseFont = false;
            this.lblInstallement.StylePriority.UsePadding = false;
            this.lblInstallement.StylePriority.UseTextAlignment = false;
            this.lblInstallement.Text = "Installment Amount";
            this.lblInstallement.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblInstallement.Weight = 0.34769230769230763;
            // 
            // lblArrearsAmt
            // 
            this.lblArrearsAmt.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblArrearsAmt.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblArrearsAmt.Name = "lblArrearsAmt";
            this.lblArrearsAmt.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblArrearsAmt.StylePriority.UseBorders = false;
            this.lblArrearsAmt.StylePriority.UseFont = false;
            this.lblArrearsAmt.StylePriority.UsePadding = false;
            this.lblArrearsAmt.StylePriority.UseTextAlignment = false;
            this.lblArrearsAmt.Text = "Arrears Amount";
            this.lblArrearsAmt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblArrearsAmt.Weight = 0.34692307692307689;
            // 
            // lblAccType
            // 
            this.lblAccType.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblAccType.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblAccType.Name = "lblAccType";
            this.lblAccType.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblAccType.StylePriority.UseBorders = false;
            this.lblAccType.StylePriority.UseFont = false;
            this.lblAccType.StylePriority.UsePadding = false;
            this.lblAccType.StylePriority.UseTextAlignment = false;
            this.lblAccType.Text = "Type of Account";
            this.lblAccType.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAccType.Weight = 0.37923076923076926;
            // 
            // tblAccStatusH
            // 
            this.tblAccStatusH.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.tblAccStatusH.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblAccStatusH.BorderWidth = 2;
            this.tblAccStatusH.ForeColor = System.Drawing.Color.Black;
            this.tblAccStatusH.LocationFloat = new DevExpress.Utils.PointFloat(8F, 8F);
            this.tblAccStatusH.Name = "tblAccStatusH";
            this.tblAccStatusH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow75});
            this.tblAccStatusH.SizeF = new System.Drawing.SizeF(792F, 33F);
            this.tblAccStatusH.StylePriority.UseBorderColor = false;
            this.tblAccStatusH.StylePriority.UseBorders = false;
            this.tblAccStatusH.StylePriority.UseBorderWidth = false;
            this.tblAccStatusH.StylePriority.UseForeColor = false;
            this.tblAccStatusH.StylePriority.UseTextAlignment = false;
            this.tblAccStatusH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow75
            // 
            this.xrTableRow75.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblAccStatusH});
            this.xrTableRow75.Name = "xrTableRow75";
            this.xrTableRow75.Weight = 1;
            // 
            // lblAccStatusH
            // 
            this.lblAccStatusH.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblAccStatusH.ForeColor = System.Drawing.Color.Gray;
            this.lblAccStatusH.Name = "lblAccStatusH";
            this.lblAccStatusH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblAccStatusH.StylePriority.UseFont = false;
            this.lblAccStatusH.StylePriority.UseForeColor = false;
            this.lblAccStatusH.StylePriority.UsePadding = false;
            this.lblAccStatusH.Text = "Credit Account Status";
            this.lblAccStatusH.Weight = 3;
            // 
            // MonthlyPaymentHeader
            // 
            this.MonthlyPaymentHeader.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailMonthlyPaymentHeader,
            this.GroupHeader15});
            this.MonthlyPaymentHeader.DataMember = "Consumer6MonthlyPaymentWithDelinquentHeader";
            this.MonthlyPaymentHeader.Level = 6;
            this.MonthlyPaymentHeader.Name = "MonthlyPaymentHeader";
            // 
            // DetailMonthlyPaymentHeader
            // 
            this.DetailMonthlyPaymentHeader.HeightF = 0F;
            this.DetailMonthlyPaymentHeader.Name = "DetailMonthlyPaymentHeader";
            // 
            // GroupHeader15
            // 
            this.GroupHeader15.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblMonthlyPaymentH,
            this.PageinfoMonthlyPaymentH});
            this.GroupHeader15.HeightF = 40F;
            this.GroupHeader15.Name = "GroupHeader15";
            // 
            // tblMonthlyPaymentH
            // 
            this.tblMonthlyPaymentH.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tblMonthlyPaymentH.BorderColor = System.Drawing.Color.White;
            this.tblMonthlyPaymentH.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblMonthlyPaymentH.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.tblMonthlyPaymentH.LocationFloat = new DevExpress.Utils.PointFloat(8F, 20F);
            this.tblMonthlyPaymentH.Name = "tblMonthlyPaymentH";
            this.tblMonthlyPaymentH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblMonthlyPaymentH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow78});
            this.tblMonthlyPaymentH.SizeF = new System.Drawing.SizeF(792F, 20F);
            this.tblMonthlyPaymentH.StylePriority.UseBackColor = false;
            this.tblMonthlyPaymentH.StylePriority.UseBorderColor = false;
            this.tblMonthlyPaymentH.StylePriority.UseBorders = false;
            this.tblMonthlyPaymentH.StylePriority.UseFont = false;
            this.tblMonthlyPaymentH.StylePriority.UsePadding = false;
            this.tblMonthlyPaymentH.StylePriority.UseTextAlignment = false;
            this.tblMonthlyPaymentH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow78
            // 
            this.xrTableRow78.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblcompanyMP,
            this.lblM01,
            this.lblM02,
            this.lblM03,
            this.lblM04,
            this.lblM05,
            this.lblM06,
            this.lblDelinquentStatusC});
            this.xrTableRow78.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableRow78.Name = "xrTableRow78";
            this.xrTableRow78.StylePriority.UseBorders = false;
            this.xrTableRow78.StylePriority.UseFont = false;
            this.xrTableRow78.Weight = 1;
            // 
            // lblcompanyMP
            // 
            this.lblcompanyMP.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblcompanyMP.ForeColor = System.Drawing.Color.Gray;
            this.lblcompanyMP.Name = "lblcompanyMP";
            this.lblcompanyMP.StylePriority.UseBorders = false;
            this.lblcompanyMP.StylePriority.UseFont = false;
            this.lblcompanyMP.StylePriority.UseForeColor = false;
            this.lblcompanyMP.StylePriority.UseTextAlignment = false;
            this.lblcompanyMP.Text = "Company";
            this.lblcompanyMP.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblcompanyMP.Weight = 1.3915140117008114;
            // 
            // lblM01
            // 
            this.lblM01.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer6MonthlyPaymentWithDelinquentHeader.M24")});
            this.lblM01.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblM01.ForeColor = System.Drawing.Color.Gray;
            this.lblM01.Name = "lblM01";
            this.lblM01.StylePriority.UseFont = false;
            this.lblM01.StylePriority.UseForeColor = false;
            this.lblM01.Text = "lblM01";
            this.lblM01.Weight = 0.309885765617113;
            // 
            // lblM02
            // 
            this.lblM02.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer6MonthlyPaymentWithDelinquentHeader.M23")});
            this.lblM02.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblM02.ForeColor = System.Drawing.Color.Gray;
            this.lblM02.Name = "lblM02";
            this.lblM02.StylePriority.UseFont = false;
            this.lblM02.StylePriority.UseForeColor = false;
            this.lblM02.Text = "lblM02";
            this.lblM02.Weight = 0.30988576561711284;
            // 
            // lblM03
            // 
            this.lblM03.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer6MonthlyPaymentWithDelinquentHeader.M22")});
            this.lblM03.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblM03.ForeColor = System.Drawing.Color.Gray;
            this.lblM03.Name = "lblM03";
            this.lblM03.StylePriority.UseFont = false;
            this.lblM03.StylePriority.UseForeColor = false;
            this.lblM03.Text = "lblM03";
            this.lblM03.Weight = 0.30988576561711284;
            // 
            // lblM04
            // 
            this.lblM04.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer6MonthlyPaymentWithDelinquentHeader.M21")});
            this.lblM04.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblM04.ForeColor = System.Drawing.Color.Gray;
            this.lblM04.Name = "lblM04";
            this.lblM04.StylePriority.UseFont = false;
            this.lblM04.StylePriority.UseForeColor = false;
            this.lblM04.Text = "lblM04";
            this.lblM04.Weight = 0.30982294918306796;
            // 
            // lblM05
            // 
            this.lblM05.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer6MonthlyPaymentWithDelinquentHeader.M20")});
            this.lblM05.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblM05.ForeColor = System.Drawing.Color.Gray;
            this.lblM05.Name = "lblM05";
            this.lblM05.StylePriority.UseFont = false;
            this.lblM05.StylePriority.UseForeColor = false;
            this.lblM05.Text = "lblM05";
            this.lblM05.Weight = 0.310796124372684;
            // 
            // lblM06
            // 
            this.lblM06.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer6MonthlyPaymentWithDelinquentHeader.M19")});
            this.lblM06.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblM06.ForeColor = System.Drawing.Color.Gray;
            this.lblM06.Name = "lblM06";
            this.lblM06.StylePriority.UseFont = false;
            this.lblM06.StylePriority.UseForeColor = false;
            this.lblM06.Text = "lblM06";
            this.lblM06.Weight = 0.30868305738762852;
            // 
            // lblDelinquentStatusC
            // 
            this.lblDelinquentStatusC.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDelinquentStatusC.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer6MonthlyPaymentWithDelinquentHeader.LastDeliquentStatus")});
            this.lblDelinquentStatusC.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDelinquentStatusC.ForeColor = System.Drawing.Color.Gray;
            this.lblDelinquentStatusC.Name = "lblDelinquentStatusC";
            this.lblDelinquentStatusC.StylePriority.UseBorders = false;
            this.lblDelinquentStatusC.StylePriority.UseFont = false;
            this.lblDelinquentStatusC.StylePriority.UseForeColor = false;
            this.lblDelinquentStatusC.Text = "lblDelinquentStatusC";
            this.lblDelinquentStatusC.Weight = 1.65493882755597;
            // 
            // PageinfoMonthlyPaymentH
            // 
            this.PageinfoMonthlyPaymentH.BackColor = System.Drawing.Color.WhiteSmoke;
            this.PageinfoMonthlyPaymentH.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.PageinfoMonthlyPaymentH.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.PageinfoMonthlyPaymentH.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.PageinfoMonthlyPaymentH.ForeColor = System.Drawing.Color.Gray;
            this.PageinfoMonthlyPaymentH.Format = "Monthly Payment Behaviour as at {0:dd/MM/yyyy}";
            this.PageinfoMonthlyPaymentH.LocationFloat = new DevExpress.Utils.PointFloat(8F, 0F);
            this.PageinfoMonthlyPaymentH.Name = "PageinfoMonthlyPaymentH";
            this.PageinfoMonthlyPaymentH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.PageinfoMonthlyPaymentH.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.PageinfoMonthlyPaymentH.SizeF = new System.Drawing.SizeF(792F, 20F);
            this.PageinfoMonthlyPaymentH.StylePriority.UseBackColor = false;
            this.PageinfoMonthlyPaymentH.StylePriority.UseBorderColor = false;
            this.PageinfoMonthlyPaymentH.StylePriority.UseBorders = false;
            this.PageinfoMonthlyPaymentH.StylePriority.UseFont = false;
            this.PageinfoMonthlyPaymentH.StylePriority.UseForeColor = false;
            this.PageinfoMonthlyPaymentH.StylePriority.UsePadding = false;
            this.PageinfoMonthlyPaymentH.StylePriority.UseTextAlignment = false;
            this.PageinfoMonthlyPaymentH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // ConsumerDefinition
            // 
            this.ConsumerDefinition.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailConsumerDefinition,
            this.GroupHeader16});
            this.ConsumerDefinition.DataMember = "ConsumerDefinition";
            this.ConsumerDefinition.Level = 8;
            this.ConsumerDefinition.Name = "ConsumerDefinition";
            // 
            // DetailConsumerDefinition
            // 
            this.DetailConsumerDefinition.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblConsumerDefinitionD});
            this.DetailConsumerDefinition.HeightF = 20F;
            this.DetailConsumerDefinition.Name = "DetailConsumerDefinition";
            // 
            // tblConsumerDefinitionD
            // 
            this.tblConsumerDefinitionD.LocationFloat = new DevExpress.Utils.PointFloat(8F, 0F);
            this.tblConsumerDefinitionD.Name = "tblConsumerDefinitionD";
            this.tblConsumerDefinitionD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblConsumerDefinitionD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow81});
            this.tblConsumerDefinitionD.SizeF = new System.Drawing.SizeF(792F, 20F);
            this.tblConsumerDefinitionD.StylePriority.UseBorders = false;
            this.tblConsumerDefinitionD.StylePriority.UsePadding = false;
            // 
            // xrTableRow81
            // 
            this.xrTableRow81.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDefDescD,
            this.lbDefCodeD});
            this.xrTableRow81.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.xrTableRow81.Name = "xrTableRow81";
            this.xrTableRow81.StylePriority.UseForeColor = false;
            this.xrTableRow81.Weight = 0.8;
            // 
            // lblDefDescD
            // 
            this.lblDefDescD.BackColor = System.Drawing.Color.White;
            this.lblDefDescD.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblDefDescD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDefDescD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDefinition.DefinitionDesc")});
            this.lblDefDescD.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDefDescD.ForeColor = System.Drawing.Color.DimGray;
            this.lblDefDescD.Name = "lblDefDescD";
            this.lblDefDescD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDefDescD.StylePriority.UseBackColor = false;
            this.lblDefDescD.StylePriority.UseBorderColor = false;
            this.lblDefDescD.StylePriority.UseBorders = false;
            this.lblDefDescD.StylePriority.UseFont = false;
            this.lblDefDescD.StylePriority.UseForeColor = false;
            this.lblDefDescD.StylePriority.UsePadding = false;
            this.lblDefDescD.StylePriority.UseTextAlignment = false;
            this.lblDefDescD.Text = "lblDefDescD";
            this.lblDefDescD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDefDescD.Weight = 3.7558909282095403;
            // 
            // lbDefCodeD
            // 
            this.lbDefCodeD.BackColor = System.Drawing.Color.White;
            this.lbDefCodeD.BorderColor = System.Drawing.Color.Gainsboro;
            this.lbDefCodeD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbDefCodeD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDefinition.DefinitionCode")});
            this.lbDefCodeD.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbDefCodeD.ForeColor = System.Drawing.Color.DimGray;
            this.lbDefCodeD.Name = "lbDefCodeD";
            this.lbDefCodeD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lbDefCodeD.StylePriority.UseBackColor = false;
            this.lbDefCodeD.StylePriority.UseBorderColor = false;
            this.lbDefCodeD.StylePriority.UseBorders = false;
            this.lbDefCodeD.StylePriority.UseFont = false;
            this.lbDefCodeD.StylePriority.UseForeColor = false;
            this.lbDefCodeD.StylePriority.UsePadding = false;
            this.lbDefCodeD.StylePriority.UseTextAlignment = false;
            this.lbDefCodeD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbDefCodeD.Weight = 3.7488409329891978;
            // 
            // GroupHeader16
            // 
            this.GroupHeader16.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel2,
            this.tblConsumerDefinitionC});
            this.GroupHeader16.HeightF = 43F;
            this.GroupHeader16.Name = "GroupHeader16";
            // 
            // xrLabel2
            // 
            this.xrLabel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrLabel2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.xrLabel2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel2.ForeColor = System.Drawing.Color.Gray;
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(8F, 0F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(792F, 20F);
            this.xrLabel2.StylePriority.UseBackColor = false;
            this.xrLabel2.StylePriority.UseBorderColor = false;
            this.xrLabel2.StylePriority.UseBorders = false;
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseForeColor = false;
            this.xrLabel2.StylePriority.UsePadding = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Monthly Payment ConsumerDefinitions";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // tblConsumerDefinitionC
            // 
            this.tblConsumerDefinitionC.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tblConsumerDefinitionC.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblConsumerDefinitionC.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblConsumerDefinitionC.LocationFloat = new DevExpress.Utils.PointFloat(8F, 23F);
            this.tblConsumerDefinitionC.Name = "tblConsumerDefinitionC";
            this.tblConsumerDefinitionC.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblConsumerDefinitionC.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow80});
            this.tblConsumerDefinitionC.SizeF = new System.Drawing.SizeF(792F, 20F);
            this.tblConsumerDefinitionC.StylePriority.UseBorderColor = false;
            this.tblConsumerDefinitionC.StylePriority.UseBorders = false;
            this.tblConsumerDefinitionC.StylePriority.UsePadding = false;
            // 
            // xrTableRow80
            // 
            this.xrTableRow80.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDefDesc,
            this.lbDefCode});
            this.xrTableRow80.Name = "xrTableRow80";
            this.xrTableRow80.Weight = 1;
            // 
            // lblDefDesc
            // 
            this.lblDefDesc.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblDefDesc.BorderColor = System.Drawing.Color.White;
            this.lblDefDesc.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDefDesc.CanGrow = false;
            this.lblDefDesc.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDefDesc.ForeColor = System.Drawing.Color.Gray;
            this.lblDefDesc.Name = "lblDefDesc";
            this.lblDefDesc.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDefDesc.StylePriority.UseBackColor = false;
            this.lblDefDesc.StylePriority.UseBorderColor = false;
            this.lblDefDesc.StylePriority.UseBorders = false;
            this.lblDefDesc.StylePriority.UseFont = false;
            this.lblDefDesc.StylePriority.UseForeColor = false;
            this.lblDefDesc.StylePriority.UsePadding = false;
            this.lblDefDesc.StylePriority.UseTextAlignment = false;
            this.lblDefDesc.Text = "ConsumerDefinitions";
            this.lblDefDesc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDefDesc.Weight = 3.7558909282095403;
            // 
            // AdverseInfo
            // 
            this.AdverseInfo.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailAdverseInfo,
            this.GroupHeader17});
            this.AdverseInfo.DataMember = "ConsumerAdverseInfo";
            this.AdverseInfo.Level = 9;
            this.AdverseInfo.Name = "AdverseInfo";
            // 
            // DetailAdverseInfo
            // 
            this.DetailAdverseInfo.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblAdverseData});
            this.DetailAdverseInfo.HeightF = 20F;
            this.DetailAdverseInfo.Name = "DetailAdverseInfo";
            // 
            // tblAdverseData
            // 
            this.tblAdverseData.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblAdverseData.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblAdverseData.LocationFloat = new DevExpress.Utils.PointFloat(8F, 0F);
            this.tblAdverseData.Name = "tblAdverseData";
            this.tblAdverseData.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblAdverseData.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow85});
            this.tblAdverseData.SizeF = new System.Drawing.SizeF(792F, 20F);
            this.tblAdverseData.StylePriority.UseBorderColor = false;
            this.tblAdverseData.StylePriority.UseBorders = false;
            this.tblAdverseData.StylePriority.UsePadding = false;
            // 
            // xrTableRow85
            // 
            this.xrTableRow85.BackColor = System.Drawing.Color.White;
            this.xrTableRow85.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableRow85.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow85.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDSubscriber,
            this.lblDAccNo,
            this.lblDActionDate,
            this.lblDAmount,
            this.lblDAccStatus,
            this.lblDAdvComment});
            this.xrTableRow85.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.xrTableRow85.Name = "xrTableRow85";
            this.xrTableRow85.StylePriority.UseBackColor = false;
            this.xrTableRow85.StylePriority.UseBorderColor = false;
            this.xrTableRow85.StylePriority.UseBorders = false;
            this.xrTableRow85.StylePriority.UseForeColor = false;
            this.xrTableRow85.Weight = 1;
            // 
            // lblDSubscriber
            // 
            this.lblDSubscriber.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdverseInfo.SubscriberName")});
            this.lblDSubscriber.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDSubscriber.ForeColor = System.Drawing.Color.DimGray;
            this.lblDSubscriber.Name = "lblDSubscriber";
            this.lblDSubscriber.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDSubscriber.StylePriority.UseFont = false;
            this.lblDSubscriber.StylePriority.UseForeColor = false;
            this.lblDSubscriber.StylePriority.UsePadding = false;
            this.lblDSubscriber.StylePriority.UseTextAlignment = false;
            this.lblDSubscriber.Text = "lblDSubscriber";
            this.lblDSubscriber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDSubscriber.Weight = 0.725;
            // 
            // lblDAccNo
            // 
            this.lblDAccNo.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdverseInfo.AccountNo")});
            this.lblDAccNo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDAccNo.ForeColor = System.Drawing.Color.DimGray;
            this.lblDAccNo.Name = "lblDAccNo";
            this.lblDAccNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDAccNo.StylePriority.UseFont = false;
            this.lblDAccNo.StylePriority.UseForeColor = false;
            this.lblDAccNo.StylePriority.UsePadding = false;
            this.lblDAccNo.StylePriority.UseTextAlignment = false;
            this.lblDAccNo.Text = "lblDAccNo";
            this.lblDAccNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDAccNo.Weight = 0.46374999999999994;
            // 
            // lblDActionDate
            // 
            this.lblDActionDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdverseInfo.ActionDate")});
            this.lblDActionDate.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDActionDate.ForeColor = System.Drawing.Color.DimGray;
            this.lblDActionDate.Name = "lblDActionDate";
            this.lblDActionDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDActionDate.StylePriority.UseFont = false;
            this.lblDActionDate.StylePriority.UseForeColor = false;
            this.lblDActionDate.StylePriority.UsePadding = false;
            this.lblDActionDate.StylePriority.UseTextAlignment = false;
            this.lblDActionDate.Text = "lblDActionDate";
            this.lblDActionDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDActionDate.Weight = 0.31375;
            // 
            // lblDAmount
            // 
            this.lblDAmount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdverseInfo.CurrentBalanceAmt")});
            this.lblDAmount.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDAmount.ForeColor = System.Drawing.Color.DimGray;
            this.lblDAmount.Name = "lblDAmount";
            this.lblDAmount.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDAmount.StylePriority.UseFont = false;
            this.lblDAmount.StylePriority.UseForeColor = false;
            this.lblDAmount.StylePriority.UsePadding = false;
            this.lblDAmount.StylePriority.UseTextAlignment = false;
            this.lblDAmount.Text = "lblDAmount";
            this.lblDAmount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDAmount.Weight = 0.3425;
            // 
            // lblDAccStatus
            // 
            this.lblDAccStatus.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdverseInfo.DataStatus")});
            this.lblDAccStatus.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDAccStatus.ForeColor = System.Drawing.Color.DimGray;
            this.lblDAccStatus.Name = "lblDAccStatus";
            this.lblDAccStatus.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDAccStatus.StylePriority.UseFont = false;
            this.lblDAccStatus.StylePriority.UseForeColor = false;
            this.lblDAccStatus.StylePriority.UsePadding = false;
            this.lblDAccStatus.StylePriority.UseTextAlignment = false;
            this.lblDAccStatus.Text = "lblDAccStatus";
            this.lblDAccStatus.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDAccStatus.Weight = 0.41375;
            // 
            // lblDAdvComment
            // 
            this.lblDAdvComment.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdverseInfo.Comments")});
            this.lblDAdvComment.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDAdvComment.ForeColor = System.Drawing.Color.DimGray;
            this.lblDAdvComment.Name = "lblDAdvComment";
            this.lblDAdvComment.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDAdvComment.StylePriority.UseFont = false;
            this.lblDAdvComment.StylePriority.UseForeColor = false;
            this.lblDAdvComment.StylePriority.UsePadding = false;
            this.lblDAdvComment.StylePriority.UseTextAlignment = false;
            this.lblDAdvComment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDAdvComment.Weight = 0.74125;
            // 
            // GroupHeader17
            // 
            this.GroupHeader17.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblAdverseColumns,
            this.tblAdverseH,
            this.tblDomainH});
            this.GroupHeader17.HeightF = 96F;
            this.GroupHeader17.Name = "GroupHeader17";
            // 
            // tblAdverseColumns
            // 
            this.tblAdverseColumns.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tblAdverseColumns.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tblAdverseColumns.BorderColor = System.Drawing.Color.White;
            this.tblAdverseColumns.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblAdverseColumns.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.tblAdverseColumns.ForeColor = System.Drawing.Color.Gray;
            this.tblAdverseColumns.LocationFloat = new DevExpress.Utils.PointFloat(8F, 76F);
            this.tblAdverseColumns.Name = "tblAdverseColumns";
            this.tblAdverseColumns.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblAdverseColumns.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow83});
            this.tblAdverseColumns.SizeF = new System.Drawing.SizeF(792F, 20F);
            this.tblAdverseColumns.StylePriority.UseBackColor = false;
            this.tblAdverseColumns.StylePriority.UseBorderColor = false;
            this.tblAdverseColumns.StylePriority.UseBorders = false;
            this.tblAdverseColumns.StylePriority.UseFont = false;
            this.tblAdverseColumns.StylePriority.UseForeColor = false;
            this.tblAdverseColumns.StylePriority.UsePadding = false;
            this.tblAdverseColumns.StylePriority.UseTextAlignment = false;
            this.tblAdverseColumns.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow83
            // 
            this.xrTableRow83.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSubscriber,
            this.lblAccountNo,
            this.lblActionDate,
            this.lblAmount,
            this.lblAccStatus,
            this.lblAdverseComment});
            this.xrTableRow83.Name = "xrTableRow83";
            this.xrTableRow83.Weight = 1.1764705882352942;
            // 
            // lblSubscriber
            // 
            this.lblSubscriber.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSubscriber.CanGrow = false;
            this.lblSubscriber.Name = "lblSubscriber";
            this.lblSubscriber.StylePriority.UseBorders = false;
            this.lblSubscriber.Text = "Subscriber";
            this.lblSubscriber.Weight = 0.725;
            // 
            // lblAccountNo
            // 
            this.lblAccountNo.CanGrow = false;
            this.lblAccountNo.Name = "lblAccountNo";
            this.lblAccountNo.Text = "Account No.";
            this.lblAccountNo.Weight = 0.46625;
            // 
            // lblActionDate
            // 
            this.lblActionDate.CanGrow = false;
            this.lblActionDate.Name = "lblActionDate";
            this.lblActionDate.Text = "Action date";
            this.lblActionDate.Weight = 0.31375;
            // 
            // lblAmount
            // 
            this.lblAmount.CanGrow = false;
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Text = "Amount";
            this.lblAmount.Weight = 0.33999999999999997;
            // 
            // lblAccStatus
            // 
            this.lblAccStatus.CanGrow = false;
            this.lblAccStatus.Name = "lblAccStatus";
            this.lblAccStatus.Text = "Account Status";
            this.lblAccStatus.Weight = 0.41375;
            // 
            // lblAdverseComment
            // 
            this.lblAdverseComment.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblAdverseComment.CanGrow = false;
            this.lblAdverseComment.Name = "lblAdverseComment";
            this.lblAdverseComment.StylePriority.UseBorders = false;
            this.lblAdverseComment.Text = "Comment";
            this.lblAdverseComment.Weight = 0.74125;
            // 
            // tblAdverseH
            // 
            this.tblAdverseH.ForeColor = System.Drawing.Color.Black;
            this.tblAdverseH.LocationFloat = new DevExpress.Utils.PointFloat(8F, 50F);
            this.tblAdverseH.Name = "tblAdverseH";
            this.tblAdverseH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblAdverseH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow84});
            this.tblAdverseH.SizeF = new System.Drawing.SizeF(792F, 25F);
            this.tblAdverseH.StylePriority.UseForeColor = false;
            this.tblAdverseH.StylePriority.UsePadding = false;
            // 
            // xrTableRow84
            // 
            this.xrTableRow84.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblAdverseH});
            this.xrTableRow84.Name = "xrTableRow84";
            this.xrTableRow84.Weight = 1;
            // 
            // lblAdverseH
            // 
            this.lblAdverseH.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblAdverseH.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.lblAdverseH.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblAdverseH.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.lblAdverseH.ForeColor = System.Drawing.Color.Gray;
            this.lblAdverseH.Name = "lblAdverseH";
            this.lblAdverseH.StylePriority.UseBackColor = false;
            this.lblAdverseH.StylePriority.UseBorderColor = false;
            this.lblAdverseH.StylePriority.UseBorders = false;
            this.lblAdverseH.StylePriority.UseFont = false;
            this.lblAdverseH.StylePriority.UseForeColor = false;
            this.lblAdverseH.StylePriority.UseTextAlignment = false;
            this.lblAdverseH.Text = "Adverse/Defaults";
            this.lblAdverseH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblAdverseH.Weight = 3;
            // 
            // tblDomainH
            // 
            this.tblDomainH.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.tblDomainH.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblDomainH.BorderWidth = 2;
            this.tblDomainH.ForeColor = System.Drawing.Color.Black;
            this.tblDomainH.LocationFloat = new DevExpress.Utils.PointFloat(8F, 8F);
            this.tblDomainH.Name = "tblDomainH";
            this.tblDomainH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow82});
            this.tblDomainH.SizeF = new System.Drawing.SizeF(792F, 33F);
            this.tblDomainH.StylePriority.UseBorderColor = false;
            this.tblDomainH.StylePriority.UseBorders = false;
            this.tblDomainH.StylePriority.UseBorderWidth = false;
            this.tblDomainH.StylePriority.UseForeColor = false;
            // 
            // xrTableRow82
            // 
            this.xrTableRow82.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell104});
            this.xrTableRow82.Name = "xrTableRow82";
            this.xrTableRow82.Weight = 1;
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell104.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell104.StylePriority.UseFont = false;
            this.xrTableCell104.StylePriority.UseForeColor = false;
            this.xrTableCell104.StylePriority.UsePadding = false;
            this.xrTableCell104.StylePriority.UseTextAlignment = false;
            this.xrTableCell104.Text = "Public Domain Records ";
            this.xrTableCell104.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell104.Weight = 3;
            // 
            // Judgements
            // 
            this.Judgements.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailJudgements,
            this.GroupHeader18});
            this.Judgements.DataMember = "ConsumerJudgement";
            this.Judgements.Level = 10;
            this.Judgements.Name = "Judgements";
            // 
            // DetailJudgements
            // 
            this.DetailJudgements.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblDJudg});
            this.DetailJudgements.HeightF = 20F;
            this.DetailJudgements.Name = "DetailJudgements";
            // 
            // tblDJudg
            // 
            this.tblDJudg.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblDJudg.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblDJudg.LocationFloat = new DevExpress.Utils.PointFloat(8F, 0F);
            this.tblDJudg.Name = "tblDJudg";
            this.tblDJudg.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblDJudg.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow88});
            this.tblDJudg.SizeF = new System.Drawing.SizeF(792F, 20F);
            this.tblDJudg.StylePriority.UseBorderColor = false;
            this.tblDJudg.StylePriority.UseBorders = false;
            this.tblDJudg.StylePriority.UsePadding = false;
            // 
            // xrTableRow88
            // 
            this.xrTableRow88.BackColor = System.Drawing.Color.White;
            this.xrTableRow88.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableRow88.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow88.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblJDCaseno,
            this.lblJDIssueDate,
            this.lblJDType,
            this.lblJDAmount,
            this.lblJDPlaintiff,
            this.lblJDCourt,
            this.lblJDAttorney,
            this.lblJDPhoneNo,
            this.lblJDComment});
            this.xrTableRow88.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableRow88.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.xrTableRow88.Name = "xrTableRow88";
            this.xrTableRow88.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100F);
            this.xrTableRow88.StylePriority.UseBackColor = false;
            this.xrTableRow88.StylePriority.UseBorderColor = false;
            this.xrTableRow88.StylePriority.UseBorders = false;
            this.xrTableRow88.StylePriority.UseFont = false;
            this.xrTableRow88.StylePriority.UseForeColor = false;
            this.xrTableRow88.StylePriority.UsePadding = false;
            this.xrTableRow88.StylePriority.UseTextAlignment = false;
            this.xrTableRow88.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow88.Weight = 1;
            // 
            // lblJDCaseno
            // 
            this.lblJDCaseno.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.CaseNumber")});
            this.lblJDCaseno.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblJDCaseno.ForeColor = System.Drawing.Color.DimGray;
            this.lblJDCaseno.Name = "lblJDCaseno";
            this.lblJDCaseno.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblJDCaseno.StylePriority.UseFont = false;
            this.lblJDCaseno.StylePriority.UseForeColor = false;
            this.lblJDCaseno.StylePriority.UsePadding = false;
            this.lblJDCaseno.Text = "xrTableCell126";
            this.lblJDCaseno.Weight = 0.28;
            // 
            // lblJDIssueDate
            // 
            this.lblJDIssueDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.CaseFilingDate")});
            this.lblJDIssueDate.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblJDIssueDate.ForeColor = System.Drawing.Color.DimGray;
            this.lblJDIssueDate.Name = "lblJDIssueDate";
            this.lblJDIssueDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblJDIssueDate.StylePriority.UseFont = false;
            this.lblJDIssueDate.StylePriority.UseForeColor = false;
            this.lblJDIssueDate.StylePriority.UsePadding = false;
            this.lblJDIssueDate.Text = "xrTableCell127";
            this.lblJDIssueDate.Weight = 0.18625;
            // 
            // lblJDType
            // 
            this.lblJDType.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.CaseType")});
            this.lblJDType.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblJDType.ForeColor = System.Drawing.Color.DimGray;
            this.lblJDType.Name = "lblJDType";
            this.lblJDType.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblJDType.StylePriority.UseFont = false;
            this.lblJDType.StylePriority.UseForeColor = false;
            this.lblJDType.StylePriority.UsePadding = false;
            this.lblJDType.Text = "xrTableCell128";
            this.lblJDType.Weight = 0.37375;
            // 
            // lblJDAmount
            // 
            this.lblJDAmount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.DisputeAmt")});
            this.lblJDAmount.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblJDAmount.ForeColor = System.Drawing.Color.DimGray;
            this.lblJDAmount.Name = "lblJDAmount";
            this.lblJDAmount.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblJDAmount.StylePriority.UseFont = false;
            this.lblJDAmount.StylePriority.UseForeColor = false;
            this.lblJDAmount.StylePriority.UsePadding = false;
            this.lblJDAmount.Text = "xrTableCell129";
            this.lblJDAmount.Weight = 0.23170454545454547;
            // 
            // lblJDPlaintiff
            // 
            this.lblJDPlaintiff.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.PlaintiffName")});
            this.lblJDPlaintiff.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblJDPlaintiff.ForeColor = System.Drawing.Color.DimGray;
            this.lblJDPlaintiff.Name = "lblJDPlaintiff";
            this.lblJDPlaintiff.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblJDPlaintiff.StylePriority.UseFont = false;
            this.lblJDPlaintiff.StylePriority.UseForeColor = false;
            this.lblJDPlaintiff.StylePriority.UsePadding = false;
            this.lblJDPlaintiff.Text = "xrTableCell130";
            this.lblJDPlaintiff.Weight = 0.34753787878787878;
            // 
            // lblJDCourt
            // 
            this.lblJDCourt.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.CourtName")});
            this.lblJDCourt.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblJDCourt.ForeColor = System.Drawing.Color.DimGray;
            this.lblJDCourt.Name = "lblJDCourt";
            this.lblJDCourt.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblJDCourt.StylePriority.UseFont = false;
            this.lblJDCourt.StylePriority.UseForeColor = false;
            this.lblJDCourt.StylePriority.UsePadding = false;
            this.lblJDCourt.Text = "xrTableCell131";
            this.lblJDCourt.Weight = 0.34753787878787878;
            // 
            // lblJDAttorney
            // 
            this.lblJDAttorney.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.AttorneyName")});
            this.lblJDAttorney.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblJDAttorney.ForeColor = System.Drawing.Color.DimGray;
            this.lblJDAttorney.Name = "lblJDAttorney";
            this.lblJDAttorney.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblJDAttorney.StylePriority.UseFont = false;
            this.lblJDAttorney.StylePriority.UseForeColor = false;
            this.lblJDAttorney.StylePriority.UsePadding = false;
            this.lblJDAttorney.Text = "xrTableCell132";
            this.lblJDAttorney.Weight = 0.41003787878787884;
            // 
            // lblJDPhoneNo
            // 
            this.lblJDPhoneNo.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.TelephoneNo")});
            this.lblJDPhoneNo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblJDPhoneNo.ForeColor = System.Drawing.Color.DimGray;
            this.lblJDPhoneNo.Name = "lblJDPhoneNo";
            this.lblJDPhoneNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblJDPhoneNo.StylePriority.UseFont = false;
            this.lblJDPhoneNo.StylePriority.UseForeColor = false;
            this.lblJDPhoneNo.StylePriority.UsePadding = false;
            this.lblJDPhoneNo.Text = "xrTableCell133";
            this.lblJDPhoneNo.Weight = 0.31598484848484854;
            // 
            // lblJDComment
            // 
            this.lblJDComment.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.Comments")});
            this.lblJDComment.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblJDComment.ForeColor = System.Drawing.Color.DimGray;
            this.lblJDComment.Name = "lblJDComment";
            this.lblJDComment.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblJDComment.StylePriority.UseFont = false;
            this.lblJDComment.StylePriority.UseForeColor = false;
            this.lblJDComment.StylePriority.UsePadding = false;
            this.lblJDComment.Weight = 0.5071969696969697;
            // 
            // GroupHeader18
            // 
            this.GroupHeader18.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblCJud,
            this.tblJudgmentsH});
            this.GroupHeader18.HeightF = 54F;
            this.GroupHeader18.Name = "GroupHeader18";
            // 
            // tblCJud
            // 
            this.tblCJud.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tblCJud.BorderColor = System.Drawing.Color.White;
            this.tblCJud.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblCJud.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.tblCJud.ForeColor = System.Drawing.Color.Gray;
            this.tblCJud.LocationFloat = new DevExpress.Utils.PointFloat(8F, 34F);
            this.tblCJud.Name = "tblCJud";
            this.tblCJud.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblCJud.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow87});
            this.tblCJud.SizeF = new System.Drawing.SizeF(792F, 20F);
            this.tblCJud.StylePriority.UseBackColor = false;
            this.tblCJud.StylePriority.UseBorderColor = false;
            this.tblCJud.StylePriority.UseBorders = false;
            this.tblCJud.StylePriority.UseFont = false;
            this.tblCJud.StylePriority.UseForeColor = false;
            this.tblCJud.StylePriority.UsePadding = false;
            this.tblCJud.StylePriority.UseTextAlignment = false;
            this.tblCJud.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow87
            // 
            this.xrTableRow87.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblJCaseNo,
            this.lblJIssueDate,
            this.lblJType,
            this.lblJAmount,
            this.lblJPlaintiff,
            this.lblJCourt,
            this.lblJAttorney,
            this.lblJPhoneNo,
            this.lblJComment});
            this.xrTableRow87.Name = "xrTableRow87";
            this.xrTableRow87.Weight = 1.1764705882352942;
            // 
            // lblJCaseNo
            // 
            this.lblJCaseNo.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblJCaseNo.Name = "lblJCaseNo";
            this.lblJCaseNo.StylePriority.UseBorders = false;
            this.lblJCaseNo.Text = "Case No.";
            this.lblJCaseNo.Weight = 0.28;
            // 
            // lblJIssueDate
            // 
            this.lblJIssueDate.Name = "lblJIssueDate";
            this.lblJIssueDate.Text = "Issue date";
            this.lblJIssueDate.Weight = 0.18625;
            // 
            // lblJType
            // 
            this.lblJType.Name = "lblJType";
            this.lblJType.Text = "Judgment Type";
            this.lblJType.Weight = 0.37375;
            // 
            // lblJAmount
            // 
            this.lblJAmount.Name = "lblJAmount";
            this.lblJAmount.Text = "Amount";
            this.lblJAmount.Weight = 0.23170454545454547;
            // 
            // lblJPlaintiff
            // 
            this.lblJPlaintiff.Name = "lblJPlaintiff";
            this.lblJPlaintiff.Text = "Plaintiff";
            this.lblJPlaintiff.Weight = 0.35132575757575751;
            // 
            // lblJCourt
            // 
            this.lblJCourt.Name = "lblJCourt";
            this.lblJCourt.Text = "Court";
            this.lblJCourt.Weight = 0.34375;
            // 
            // lblJAttorney
            // 
            this.lblJAttorney.Name = "lblJAttorney";
            this.lblJAttorney.Text = "Attorney";
            this.lblJAttorney.Weight = 0.41003787878787884;
            // 
            // lblJPhoneNo
            // 
            this.lblJPhoneNo.Name = "lblJPhoneNo";
            this.lblJPhoneNo.Text = "Phone No";
            this.lblJPhoneNo.Weight = 0.31598484848484854;
            // 
            // lblJComment
            // 
            this.lblJComment.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblJComment.Name = "lblJComment";
            this.lblJComment.StylePriority.UseBorders = false;
            this.lblJComment.Text = "Comment";
            this.lblJComment.Weight = 0.5071969696969697;
            // 
            // tblJudgmentsH
            // 
            this.tblJudgmentsH.ForeColor = System.Drawing.Color.Black;
            this.tblJudgmentsH.LocationFloat = new DevExpress.Utils.PointFloat(8F, 8F);
            this.tblJudgmentsH.Name = "tblJudgmentsH";
            this.tblJudgmentsH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow86});
            this.tblJudgmentsH.SizeF = new System.Drawing.SizeF(792F, 25F);
            this.tblJudgmentsH.StylePriority.UseForeColor = false;
            // 
            // xrTableRow86
            // 
            this.xrTableRow86.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblJudgmentsH});
            this.xrTableRow86.Name = "xrTableRow86";
            this.xrTableRow86.Weight = 1;
            // 
            // lblJudgmentsH
            // 
            this.lblJudgmentsH.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblJudgmentsH.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.lblJudgmentsH.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblJudgmentsH.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.lblJudgmentsH.ForeColor = System.Drawing.Color.Gray;
            this.lblJudgmentsH.Name = "lblJudgmentsH";
            this.lblJudgmentsH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblJudgmentsH.StylePriority.UseBackColor = false;
            this.lblJudgmentsH.StylePriority.UseBorderColor = false;
            this.lblJudgmentsH.StylePriority.UseBorders = false;
            this.lblJudgmentsH.StylePriority.UseFont = false;
            this.lblJudgmentsH.StylePriority.UseForeColor = false;
            this.lblJudgmentsH.StylePriority.UsePadding = false;
            this.lblJudgmentsH.StylePriority.UseTextAlignment = false;
            this.lblJudgmentsH.Text = "Judgements";
            this.lblJudgmentsH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblJudgmentsH.Weight = 3;
            // 
            // AdminOrders
            // 
            this.AdminOrders.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailAdminOrders,
            this.GroupHeader19});
            this.AdminOrders.DataMember = "ConsumerAdminOrder";
            this.AdminOrders.Level = 11;
            this.AdminOrders.Name = "AdminOrders";
            // 
            // DetailAdminOrders
            // 
            this.DetailAdminOrders.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblDAO});
            this.DetailAdminOrders.HeightF = 20F;
            this.DetailAdminOrders.Name = "DetailAdminOrders";
            // 
            // tblDAO
            // 
            this.tblDAO.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblDAO.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblDAO.LocationFloat = new DevExpress.Utils.PointFloat(8F, 0F);
            this.tblDAO.Name = "tblDAO";
            this.tblDAO.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblDAO.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow91});
            this.tblDAO.SizeF = new System.Drawing.SizeF(792F, 20F);
            this.tblDAO.StylePriority.UseBorderColor = false;
            this.tblDAO.StylePriority.UseBorders = false;
            this.tblDAO.StylePriority.UsePadding = false;
            // 
            // xrTableRow91
            // 
            this.xrTableRow91.BackColor = System.Drawing.Color.White;
            this.xrTableRow91.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableRow91.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow91.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblAODCaseno,
            this.lblAODIssuedate,
            this.lblAODLoadeddate,
            this.lblAODType,
            this.lblAODAmount,
            this.lblAODPlaintiff,
            this.lblAODCOurt,
            this.lblAODAttorney,
            this.lblAODPhoenNo,
            this.lblAODComment});
            this.xrTableRow91.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableRow91.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.xrTableRow91.Name = "xrTableRow91";
            this.xrTableRow91.StylePriority.UseBackColor = false;
            this.xrTableRow91.StylePriority.UseBorderColor = false;
            this.xrTableRow91.StylePriority.UseBorders = false;
            this.xrTableRow91.StylePriority.UseFont = false;
            this.xrTableRow91.StylePriority.UseForeColor = false;
            this.xrTableRow91.Weight = 0.8;
            // 
            // lblAODCaseno
            // 
            this.lblAODCaseno.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.CaseNumber")});
            this.lblAODCaseno.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblAODCaseno.ForeColor = System.Drawing.Color.DimGray;
            this.lblAODCaseno.Name = "lblAODCaseno";
            this.lblAODCaseno.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblAODCaseno.StylePriority.UseFont = false;
            this.lblAODCaseno.StylePriority.UseForeColor = false;
            this.lblAODCaseno.StylePriority.UsePadding = false;
            this.lblAODCaseno.StylePriority.UseTextAlignment = false;
            this.lblAODCaseno.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAODCaseno.Weight = 0.31375;
            // 
            // lblAODIssuedate
            // 
            this.lblAODIssuedate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.CaseFilingDate")});
            this.lblAODIssuedate.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblAODIssuedate.ForeColor = System.Drawing.Color.DimGray;
            this.lblAODIssuedate.Name = "lblAODIssuedate";
            this.lblAODIssuedate.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblAODIssuedate.StylePriority.UseFont = false;
            this.lblAODIssuedate.StylePriority.UseForeColor = false;
            this.lblAODIssuedate.StylePriority.UsePadding = false;
            this.lblAODIssuedate.StylePriority.UseTextAlignment = false;
            this.lblAODIssuedate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAODIssuedate.Weight = 0.25;
            // 
            // lblAODLoadeddate
            // 
            this.lblAODLoadeddate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.LastUpdatedDate")});
            this.lblAODLoadeddate.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblAODLoadeddate.ForeColor = System.Drawing.Color.DimGray;
            this.lblAODLoadeddate.Name = "lblAODLoadeddate";
            this.lblAODLoadeddate.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblAODLoadeddate.StylePriority.UseFont = false;
            this.lblAODLoadeddate.StylePriority.UseForeColor = false;
            this.lblAODLoadeddate.StylePriority.UsePadding = false;
            this.lblAODLoadeddate.StylePriority.UseTextAlignment = false;
            this.lblAODLoadeddate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAODLoadeddate.Weight = 0.27875000000000005;
            // 
            // lblAODType
            // 
            this.lblAODType.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.CaseType")});
            this.lblAODType.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblAODType.ForeColor = System.Drawing.Color.DimGray;
            this.lblAODType.Name = "lblAODType";
            this.lblAODType.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblAODType.StylePriority.UseFont = false;
            this.lblAODType.StylePriority.UseForeColor = false;
            this.lblAODType.StylePriority.UsePadding = false;
            this.lblAODType.StylePriority.UseTextAlignment = false;
            this.lblAODType.Text = "lblAODType";
            this.lblAODType.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAODType.Weight = 0.34375;
            // 
            // lblAODAmount
            // 
            this.lblAODAmount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.DisputeAmt")});
            this.lblAODAmount.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblAODAmount.ForeColor = System.Drawing.Color.DimGray;
            this.lblAODAmount.Name = "lblAODAmount";
            this.lblAODAmount.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblAODAmount.StylePriority.UseFont = false;
            this.lblAODAmount.StylePriority.UseForeColor = false;
            this.lblAODAmount.StylePriority.UsePadding = false;
            this.lblAODAmount.StylePriority.UseTextAlignment = false;
            this.lblAODAmount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAODAmount.Weight = 0.21625;
            // 
            // lblAODPlaintiff
            // 
            this.lblAODPlaintiff.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.PlaintiffName")});
            this.lblAODPlaintiff.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblAODPlaintiff.ForeColor = System.Drawing.Color.DimGray;
            this.lblAODPlaintiff.Name = "lblAODPlaintiff";
            this.lblAODPlaintiff.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblAODPlaintiff.StylePriority.UseFont = false;
            this.lblAODPlaintiff.StylePriority.UseForeColor = false;
            this.lblAODPlaintiff.StylePriority.UsePadding = false;
            this.lblAODPlaintiff.StylePriority.UseTextAlignment = false;
            this.lblAODPlaintiff.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAODPlaintiff.Weight = 0.30303030303030309;
            // 
            // lblAODCOurt
            // 
            this.lblAODCOurt.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.CourtName")});
            this.lblAODCOurt.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblAODCOurt.ForeColor = System.Drawing.Color.DimGray;
            this.lblAODCOurt.Name = "lblAODCOurt";
            this.lblAODCOurt.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblAODCOurt.StylePriority.UseFont = false;
            this.lblAODCOurt.StylePriority.UseForeColor = false;
            this.lblAODCOurt.StylePriority.UsePadding = false;
            this.lblAODCOurt.StylePriority.UseTextAlignment = false;
            this.lblAODCOurt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAODCOurt.Weight = 0.30303028222286343;
            // 
            // lblAODAttorney
            // 
            this.lblAODAttorney.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.AttorneyName")});
            this.lblAODAttorney.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblAODAttorney.ForeColor = System.Drawing.Color.DimGray;
            this.lblAODAttorney.Name = "lblAODAttorney";
            this.lblAODAttorney.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblAODAttorney.StylePriority.UseFont = false;
            this.lblAODAttorney.StylePriority.UseForeColor = false;
            this.lblAODAttorney.StylePriority.UsePadding = false;
            this.lblAODAttorney.StylePriority.UseTextAlignment = false;
            this.lblAODAttorney.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAODAttorney.Weight = 0.30303027991092568;
            // 
            // lblAODPhoenNo
            // 
            this.lblAODPhoenNo.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.TelephoneNo")});
            this.lblAODPhoenNo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblAODPhoenNo.ForeColor = System.Drawing.Color.DimGray;
            this.lblAODPhoenNo.Name = "lblAODPhoenNo";
            this.lblAODPhoenNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblAODPhoenNo.StylePriority.UseFont = false;
            this.lblAODPhoenNo.StylePriority.UseForeColor = false;
            this.lblAODPhoenNo.StylePriority.UsePadding = false;
            this.lblAODPhoenNo.StylePriority.UseTextAlignment = false;
            this.lblAODPhoenNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAODPhoenNo.Weight = 0.26515152671120368;
            // 
            // lblAODComment
            // 
            this.lblAODComment.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.Comments")});
            this.lblAODComment.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAODComment.ForeColor = System.Drawing.Color.DimGray;
            this.lblAODComment.Name = "lblAODComment";
            this.lblAODComment.StylePriority.UseFont = false;
            this.lblAODComment.StylePriority.UseForeColor = false;
            this.lblAODComment.Text = "lblAODComment";
            this.lblAODComment.Weight = 0.42325760812470403;
            // 
            // GroupHeader19
            // 
            this.GroupHeader19.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblCAO,
            this.tblAOH});
            this.GroupHeader19.HeightF = 54F;
            this.GroupHeader19.Name = "GroupHeader19";
            // 
            // tblCAO
            // 
            this.tblCAO.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tblCAO.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tblCAO.BorderColor = System.Drawing.Color.White;
            this.tblCAO.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblCAO.LocationFloat = new DevExpress.Utils.PointFloat(8F, 34F);
            this.tblCAO.Name = "tblCAO";
            this.tblCAO.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblCAO.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow90});
            this.tblCAO.SizeF = new System.Drawing.SizeF(792F, 20F);
            this.tblCAO.StylePriority.UseBackColor = false;
            this.tblCAO.StylePriority.UseBorderColor = false;
            this.tblCAO.StylePriority.UseBorders = false;
            this.tblCAO.StylePriority.UsePadding = false;
            this.tblCAO.StylePriority.UseTextAlignment = false;
            this.tblCAO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow90
            // 
            this.xrTableRow90.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow90.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblAOCaseno,
            this.lblAOIssuedate,
            this.lblAODateLoaded,
            this.lblAOType,
            this.lblAOAmount,
            this.lblAOPlaintiff,
            this.lblAOCourt,
            this.lblAOAttorney,
            this.lblAOPhoneNo,
            this.lblAOComment});
            this.xrTableRow90.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableRow90.ForeColor = System.Drawing.Color.Gray;
            this.xrTableRow90.Name = "xrTableRow90";
            this.xrTableRow90.StylePriority.UseBorders = false;
            this.xrTableRow90.StylePriority.UseFont = false;
            this.xrTableRow90.StylePriority.UseForeColor = false;
            this.xrTableRow90.Weight = 1;
            // 
            // lblAOCaseno
            // 
            this.lblAOCaseno.CanGrow = false;
            this.lblAOCaseno.Name = "lblAOCaseno";
            this.lblAOCaseno.Text = "Case No.";
            this.lblAOCaseno.Weight = 0.31375;
            // 
            // lblAOIssuedate
            // 
            this.lblAOIssuedate.CanGrow = false;
            this.lblAOIssuedate.Name = "lblAOIssuedate";
            this.lblAOIssuedate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.lblAOIssuedate.StylePriority.UsePadding = false;
            this.lblAOIssuedate.Text = "Issue Date";
            this.lblAOIssuedate.Weight = 0.25;
            // 
            // lblAODateLoaded
            // 
            this.lblAODateLoaded.CanGrow = false;
            this.lblAODateLoaded.Name = "lblAODateLoaded";
            this.lblAODateLoaded.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.lblAODateLoaded.StylePriority.UsePadding = false;
            this.lblAODateLoaded.Text = "Date loaded";
            this.lblAODateLoaded.Weight = 0.27875000000000005;
            // 
            // lblAOType
            // 
            this.lblAOType.CanGrow = false;
            this.lblAOType.Name = "lblAOType";
            this.lblAOType.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.lblAOType.StylePriority.UsePadding = false;
            this.lblAOType.Text = "Judgment Type";
            this.lblAOType.Weight = 0.34375000000000006;
            // 
            // lblAOAmount
            // 
            this.lblAOAmount.CanGrow = false;
            this.lblAOAmount.Name = "lblAOAmount";
            this.lblAOAmount.Text = "Amount";
            this.lblAOAmount.Weight = 0.21625;
            // 
            // lblAOPlaintiff
            // 
            this.lblAOPlaintiff.CanGrow = false;
            this.lblAOPlaintiff.Name = "lblAOPlaintiff";
            this.lblAOPlaintiff.Text = "Plaintiff";
            this.lblAOPlaintiff.Weight = 0.30303030303030304;
            // 
            // lblAOCourt
            // 
            this.lblAOCourt.CanGrow = false;
            this.lblAOCourt.Name = "lblAOCourt";
            this.lblAOCourt.Text = "Court";
            this.lblAOCourt.Weight = 0.30303031112208512;
            // 
            // lblAOAttorney
            // 
            this.lblAOAttorney.CanGrow = false;
            this.lblAOAttorney.Name = "lblAOAttorney";
            this.lblAOAttorney.Text = "Attorney";
            this.lblAOAttorney.Weight = 0.30303030881014736;
            // 
            // lblAOPhoneNo
            // 
            this.lblAOPhoneNo.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblAOPhoneNo.CanGrow = false;
            this.lblAOPhoneNo.Name = "lblAOPhoneNo";
            this.lblAOPhoneNo.StylePriority.UseBorders = false;
            this.lblAOPhoneNo.Text = "Phone No";
            this.lblAOPhoneNo.Weight = 0.26515152671120379;
            // 
            // lblAOComment
            // 
            this.lblAOComment.CanGrow = false;
            this.lblAOComment.Name = "lblAOComment";
            this.lblAOComment.Text = "Comment";
            this.lblAOComment.Weight = 0.42325755032626056;
            // 
            // tblAOH
            // 
            this.tblAOH.ForeColor = System.Drawing.Color.Black;
            this.tblAOH.LocationFloat = new DevExpress.Utils.PointFloat(8F, 8F);
            this.tblAOH.Name = "tblAOH";
            this.tblAOH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow89});
            this.tblAOH.SizeF = new System.Drawing.SizeF(792F, 25F);
            this.tblAOH.StylePriority.UseForeColor = false;
            // 
            // xrTableRow89
            // 
            this.xrTableRow89.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblAOH});
            this.xrTableRow89.Name = "xrTableRow89";
            this.xrTableRow89.Weight = 1;
            // 
            // lblAOH
            // 
            this.lblAOH.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblAOH.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.lblAOH.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblAOH.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.lblAOH.ForeColor = System.Drawing.Color.Gray;
            this.lblAOH.Name = "lblAOH";
            this.lblAOH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblAOH.StylePriority.UseBackColor = false;
            this.lblAOH.StylePriority.UseBorderColor = false;
            this.lblAOH.StylePriority.UseBorders = false;
            this.lblAOH.StylePriority.UseFont = false;
            this.lblAOH.StylePriority.UseForeColor = false;
            this.lblAOH.StylePriority.UsePadding = false;
            this.lblAOH.StylePriority.UseTextAlignment = false;
            this.lblAOH.Text = "Admin Orders";
            this.lblAOH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblAOH.Weight = 3;
            // 
            // Sequestration
            // 
            this.Sequestration.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailSequestration,
            this.GroupHeader20});
            this.Sequestration.DataMember = "ConsumerSequestration";
            this.Sequestration.Level = 12;
            this.Sequestration.Name = "Sequestration";
            // 
            // DetailSequestration
            // 
            this.DetailSequestration.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblSEQD});
            this.DetailSequestration.HeightF = 20F;
            this.DetailSequestration.Name = "DetailSequestration";
            // 
            // tblSEQD
            // 
            this.tblSEQD.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblSEQD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.tblSEQD.LocationFloat = new DevExpress.Utils.PointFloat(8F, 0F);
            this.tblSEQD.Name = "tblSEQD";
            this.tblSEQD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblSEQD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow94});
            this.tblSEQD.SizeF = new System.Drawing.SizeF(792F, 20F);
            this.tblSEQD.StylePriority.UseBorderColor = false;
            this.tblSEQD.StylePriority.UseBorders = false;
            this.tblSEQD.StylePriority.UsePadding = false;
            // 
            // xrTableRow94
            // 
            this.xrTableRow94.BackColor = System.Drawing.Color.White;
            this.xrTableRow94.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableRow94.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow94.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSEQDCaseno,
            this.lblSEQDIssuedate,
            this.lblSEQDLoadedDate,
            this.lblSEQDType,
            this.lblSEQDAmount,
            this.lblSEQDPlaintiff,
            this.lblSEQDCourt,
            this.lblSEQDAttorney,
            this.lblSEQDPhoneNo});
            this.xrTableRow94.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.xrTableRow94.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.xrTableRow94.Name = "xrTableRow94";
            this.xrTableRow94.StylePriority.UseBackColor = false;
            this.xrTableRow94.StylePriority.UseBorderColor = false;
            this.xrTableRow94.StylePriority.UseBorders = false;
            this.xrTableRow94.StylePriority.UseFont = false;
            this.xrTableRow94.StylePriority.UseForeColor = false;
            this.xrTableRow94.StylePriority.UseTextAlignment = false;
            this.xrTableRow94.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow94.Weight = 0.8;
            // 
            // lblSEQDCaseno
            // 
            this.lblSEQDCaseno.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.CaseNumber")});
            this.lblSEQDCaseno.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblSEQDCaseno.ForeColor = System.Drawing.Color.DimGray;
            this.lblSEQDCaseno.Name = "lblSEQDCaseno";
            this.lblSEQDCaseno.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblSEQDCaseno.StylePriority.UseFont = false;
            this.lblSEQDCaseno.StylePriority.UseForeColor = false;
            this.lblSEQDCaseno.StylePriority.UsePadding = false;
            this.lblSEQDCaseno.Text = "xrTableCell164";
            this.lblSEQDCaseno.Weight = 0.31375;
            // 
            // lblSEQDIssuedate
            // 
            this.lblSEQDIssuedate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.CaseFilingDate")});
            this.lblSEQDIssuedate.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblSEQDIssuedate.ForeColor = System.Drawing.Color.DimGray;
            this.lblSEQDIssuedate.Name = "lblSEQDIssuedate";
            this.lblSEQDIssuedate.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblSEQDIssuedate.StylePriority.UseFont = false;
            this.lblSEQDIssuedate.StylePriority.UseForeColor = false;
            this.lblSEQDIssuedate.StylePriority.UsePadding = false;
            this.lblSEQDIssuedate.Text = "xrTableCell165";
            this.lblSEQDIssuedate.Weight = 0.25;
            // 
            // lblSEQDLoadedDate
            // 
            this.lblSEQDLoadedDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.LastUpdatedDate")});
            this.lblSEQDLoadedDate.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblSEQDLoadedDate.ForeColor = System.Drawing.Color.DimGray;
            this.lblSEQDLoadedDate.Name = "lblSEQDLoadedDate";
            this.lblSEQDLoadedDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblSEQDLoadedDate.StylePriority.UseFont = false;
            this.lblSEQDLoadedDate.StylePriority.UseForeColor = false;
            this.lblSEQDLoadedDate.StylePriority.UsePadding = false;
            this.lblSEQDLoadedDate.Text = "xrTableCell166";
            this.lblSEQDLoadedDate.Weight = 0.27875000000000005;
            // 
            // lblSEQDType
            // 
            this.lblSEQDType.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.CaseType")});
            this.lblSEQDType.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblSEQDType.ForeColor = System.Drawing.Color.DimGray;
            this.lblSEQDType.Name = "lblSEQDType";
            this.lblSEQDType.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblSEQDType.StylePriority.UseFont = false;
            this.lblSEQDType.StylePriority.UseForeColor = false;
            this.lblSEQDType.StylePriority.UsePadding = false;
            this.lblSEQDType.Text = "xrTableCell167";
            this.lblSEQDType.Weight = 0.34375;
            // 
            // lblSEQDAmount
            // 
            this.lblSEQDAmount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.DisputeAmt")});
            this.lblSEQDAmount.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblSEQDAmount.ForeColor = System.Drawing.Color.DimGray;
            this.lblSEQDAmount.Name = "lblSEQDAmount";
            this.lblSEQDAmount.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblSEQDAmount.StylePriority.UseFont = false;
            this.lblSEQDAmount.StylePriority.UseForeColor = false;
            this.lblSEQDAmount.StylePriority.UsePadding = false;
            this.lblSEQDAmount.Text = "xrTableCell168";
            this.lblSEQDAmount.Weight = 0.21625;
            // 
            // lblSEQDPlaintiff
            // 
            this.lblSEQDPlaintiff.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.PlaintiffName")});
            this.lblSEQDPlaintiff.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblSEQDPlaintiff.ForeColor = System.Drawing.Color.DimGray;
            this.lblSEQDPlaintiff.Name = "lblSEQDPlaintiff";
            this.lblSEQDPlaintiff.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblSEQDPlaintiff.StylePriority.UseFont = false;
            this.lblSEQDPlaintiff.StylePriority.UseForeColor = false;
            this.lblSEQDPlaintiff.StylePriority.UsePadding = false;
            this.lblSEQDPlaintiff.Text = "xrTableCell169";
            this.lblSEQDPlaintiff.Weight = 0.5;
            // 
            // lblSEQDCourt
            // 
            this.lblSEQDCourt.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.CourtName")});
            this.lblSEQDCourt.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblSEQDCourt.ForeColor = System.Drawing.Color.DimGray;
            this.lblSEQDCourt.Name = "lblSEQDCourt";
            this.lblSEQDCourt.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblSEQDCourt.StylePriority.UseFont = false;
            this.lblSEQDCourt.StylePriority.UseForeColor = false;
            this.lblSEQDCourt.StylePriority.UsePadding = false;
            this.lblSEQDCourt.Text = "xrTableCell170";
            this.lblSEQDCourt.Weight = 0.3475;
            // 
            // lblSEQDAttorney
            // 
            this.lblSEQDAttorney.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.AttorneyName")});
            this.lblSEQDAttorney.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblSEQDAttorney.ForeColor = System.Drawing.Color.DimGray;
            this.lblSEQDAttorney.Name = "lblSEQDAttorney";
            this.lblSEQDAttorney.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblSEQDAttorney.StylePriority.UseFont = false;
            this.lblSEQDAttorney.StylePriority.UseForeColor = false;
            this.lblSEQDAttorney.StylePriority.UsePadding = false;
            this.lblSEQDAttorney.Text = "xrTableCell171";
            this.lblSEQDAttorney.Weight = 0.4;
            // 
            // lblSEQDPhoneNo
            // 
            this.lblSEQDPhoneNo.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.TelephoneNo")});
            this.lblSEQDPhoneNo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblSEQDPhoneNo.ForeColor = System.Drawing.Color.DimGray;
            this.lblSEQDPhoneNo.Name = "lblSEQDPhoneNo";
            this.lblSEQDPhoneNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblSEQDPhoneNo.StylePriority.UseFont = false;
            this.lblSEQDPhoneNo.StylePriority.UseForeColor = false;
            this.lblSEQDPhoneNo.StylePriority.UsePadding = false;
            this.lblSEQDPhoneNo.Text = "xrTableCell172";
            this.lblSEQDPhoneNo.Weight = 0.35;
            // 
            // GroupHeader20
            // 
            this.GroupHeader20.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblSEQC,
            this.tblSEQH});
            this.GroupHeader20.HeightF = 54F;
            this.GroupHeader20.Name = "GroupHeader20";
            // 
            // tblSEQC
            // 
            this.tblSEQC.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tblSEQC.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblSEQC.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblSEQC.LocationFloat = new DevExpress.Utils.PointFloat(8F, 34F);
            this.tblSEQC.Name = "tblSEQC";
            this.tblSEQC.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblSEQC.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow93});
            this.tblSEQC.SizeF = new System.Drawing.SizeF(792F, 20F);
            this.tblSEQC.StylePriority.UseBorderColor = false;
            this.tblSEQC.StylePriority.UseBorders = false;
            this.tblSEQC.StylePriority.UsePadding = false;
            // 
            // xrTableRow93
            // 
            this.xrTableRow93.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableRow93.BorderColor = System.Drawing.Color.White;
            this.xrTableRow93.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSEQCaseno,
            this.lblSEQIssueDate,
            this.lblSEQLoadeddate,
            this.lblSEQType,
            this.lblSEQAmount,
            this.lblSEQplaintiff,
            this.lblSEQCourt,
            this.lblSEQAttorney,
            this.lblSEQPhoneNo});
            this.xrTableRow93.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableRow93.ForeColor = System.Drawing.Color.Gray;
            this.xrTableRow93.Name = "xrTableRow93";
            this.xrTableRow93.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableRow93.StylePriority.UseBackColor = false;
            this.xrTableRow93.StylePriority.UseBorderColor = false;
            this.xrTableRow93.StylePriority.UseFont = false;
            this.xrTableRow93.StylePriority.UseForeColor = false;
            this.xrTableRow93.StylePriority.UsePadding = false;
            this.xrTableRow93.StylePriority.UseTextAlignment = false;
            this.xrTableRow93.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow93.Weight = 1;
            // 
            // lblSEQCaseno
            // 
            this.lblSEQCaseno.CanGrow = false;
            this.lblSEQCaseno.Name = "lblSEQCaseno";
            this.lblSEQCaseno.Text = "Case No.";
            this.lblSEQCaseno.Weight = 0.31375;
            // 
            // lblSEQIssueDate
            // 
            this.lblSEQIssueDate.CanGrow = false;
            this.lblSEQIssueDate.Name = "lblSEQIssueDate";
            this.lblSEQIssueDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.lblSEQIssueDate.StylePriority.UsePadding = false;
            this.lblSEQIssueDate.Text = "Issue Date";
            this.lblSEQIssueDate.Weight = 0.25;
            // 
            // lblSEQLoadeddate
            // 
            this.lblSEQLoadeddate.CanGrow = false;
            this.lblSEQLoadeddate.Name = "lblSEQLoadeddate";
            this.lblSEQLoadeddate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.lblSEQLoadeddate.StylePriority.UsePadding = false;
            this.lblSEQLoadeddate.Text = "Date loaded";
            this.lblSEQLoadeddate.Weight = 0.27875000000000005;
            // 
            // lblSEQType
            // 
            this.lblSEQType.CanGrow = false;
            this.lblSEQType.Name = "lblSEQType";
            this.lblSEQType.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.lblSEQType.StylePriority.UsePadding = false;
            this.lblSEQType.Text = "Judgment Type";
            this.lblSEQType.Weight = 0.34375000000000006;
            // 
            // lblSEQAmount
            // 
            this.lblSEQAmount.CanGrow = false;
            this.lblSEQAmount.Name = "lblSEQAmount";
            this.lblSEQAmount.Text = "Amount";
            this.lblSEQAmount.Weight = 0.21625;
            // 
            // lblSEQplaintiff
            // 
            this.lblSEQplaintiff.CanGrow = false;
            this.lblSEQplaintiff.Name = "lblSEQplaintiff";
            this.lblSEQplaintiff.Text = "Plaintiff";
            this.lblSEQplaintiff.Weight = 0.5;
            // 
            // lblSEQCourt
            // 
            this.lblSEQCourt.CanGrow = false;
            this.lblSEQCourt.Name = "lblSEQCourt";
            this.lblSEQCourt.Text = "Court";
            this.lblSEQCourt.Weight = 0.3475;
            // 
            // lblSEQAttorney
            // 
            this.lblSEQAttorney.CanGrow = false;
            this.lblSEQAttorney.Name = "lblSEQAttorney";
            this.lblSEQAttorney.Text = "Attorney";
            this.lblSEQAttorney.Weight = 0.4;
            // 
            // lblSEQPhoneNo
            // 
            this.lblSEQPhoneNo.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSEQPhoneNo.CanGrow = false;
            this.lblSEQPhoneNo.Name = "lblSEQPhoneNo";
            this.lblSEQPhoneNo.StylePriority.UseBorders = false;
            this.lblSEQPhoneNo.Text = "Phone No";
            this.lblSEQPhoneNo.Weight = 0.35;
            // 
            // tblSEQH
            // 
            this.tblSEQH.ForeColor = System.Drawing.Color.Black;
            this.tblSEQH.LocationFloat = new DevExpress.Utils.PointFloat(8F, 8F);
            this.tblSEQH.Name = "tblSEQH";
            this.tblSEQH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow92});
            this.tblSEQH.SizeF = new System.Drawing.SizeF(792F, 25F);
            this.tblSEQH.StylePriority.UseForeColor = false;
            // 
            // xrTableRow92
            // 
            this.xrTableRow92.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSEQH});
            this.xrTableRow92.Name = "xrTableRow92";
            this.xrTableRow92.Weight = 1;
            // 
            // lblSEQH
            // 
            this.lblSEQH.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblSEQH.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.lblSEQH.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblSEQH.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.lblSEQH.ForeColor = System.Drawing.Color.Gray;
            this.lblSEQH.Name = "lblSEQH";
            this.lblSEQH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblSEQH.StylePriority.UseBackColor = false;
            this.lblSEQH.StylePriority.UseBorderColor = false;
            this.lblSEQH.StylePriority.UseBorders = false;
            this.lblSEQH.StylePriority.UseFont = false;
            this.lblSEQH.StylePriority.UseForeColor = false;
            this.lblSEQH.StylePriority.UsePadding = false;
            this.lblSEQH.StylePriority.UseTextAlignment = false;
            this.lblSEQH.Text = "Sequestrations";
            this.lblSEQH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSEQH.Weight = 3;
            // 
            // DebtReviewStatus
            // 
            this.DebtReviewStatus.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailDebtReviewStatus,
            this.GroupHeader21});
            this.DebtReviewStatus.DataMember = "ConsumerDebtReviewStatus";
            this.DebtReviewStatus.Level = 14;
            this.DebtReviewStatus.Name = "DebtReviewStatus";
            // 
            // DetailDebtReviewStatus
            // 
            this.DetailDebtReviewStatus.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblDbtReviewD});
            this.DetailDebtReviewStatus.HeightF = 80F;
            this.DetailDebtReviewStatus.Name = "DetailDebtReviewStatus";
            // 
            // tblDbtReviewD
            // 
            this.tblDbtReviewD.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblDbtReviewD.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblDbtReviewD.LocationFloat = new DevExpress.Utils.PointFloat(8F, 0F);
            this.tblDbtReviewD.Name = "tblDbtReviewD";
            this.tblDbtReviewD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblDbtReviewD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow48,
            this.xrTableRow96,
            this.xrTableRow98,
            this.xrTableRow97});
            this.tblDbtReviewD.SizeF = new System.Drawing.SizeF(792F, 80F);
            this.tblDbtReviewD.StylePriority.UseBorderColor = false;
            this.tblDbtReviewD.StylePriority.UseBorders = false;
            this.tblDbtReviewD.StylePriority.UsePadding = false;
            // 
            // xrTableRow48
            // 
            this.xrTableRow48.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9});
            this.xrTableRow48.Name = "xrTableRow48";
            this.xrTableRow48.Weight = 0.79999999999999993;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell9.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.xrTableCell9.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtReviewStatus.DebtCounsellorName")});
            this.xrTableCell9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell9.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.StylePriority.UseBackColor = false;
            this.xrTableCell9.StylePriority.UseBorderColor = false;
            this.xrTableCell9.StylePriority.UseBorders = false;
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.StylePriority.UseForeColor = false;
            xrSummary1.FormatString = "Debt Review No: {0}";
            xrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.RecordNumber;
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell9.Summary = xrSummary1;
            this.xrTableCell9.Text = "xrTableCell9";
            this.xrTableCell9.Weight = 3;
            // 
            // xrTableRow96
            // 
            this.xrTableRow96.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDbtReviewDate,
            this.lblDbtReviewDateD});
            this.xrTableRow96.Name = "xrTableRow96";
            this.xrTableRow96.Weight = 0.79999999999999993;
            // 
            // lblDbtReviewDate
            // 
            this.lblDbtReviewDate.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblDbtReviewDate.BorderColor = System.Drawing.Color.White;
            this.lblDbtReviewDate.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDbtReviewDate.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDbtReviewDate.ForeColor = System.Drawing.Color.Gray;
            this.lblDbtReviewDate.Name = "lblDbtReviewDate";
            this.lblDbtReviewDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDbtReviewDate.StylePriority.UseBackColor = false;
            this.lblDbtReviewDate.StylePriority.UseBorderColor = false;
            this.lblDbtReviewDate.StylePriority.UseBorders = false;
            this.lblDbtReviewDate.StylePriority.UseFont = false;
            this.lblDbtReviewDate.StylePriority.UseForeColor = false;
            this.lblDbtReviewDate.StylePriority.UsePadding = false;
            this.lblDbtReviewDate.StylePriority.UseTextAlignment = false;
            this.lblDbtReviewDate.Text = "Debt Review Date";
            this.lblDbtReviewDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDbtReviewDate.Weight = 0.75757575757575757;
            // 
            // lblDbtReviewDateD
            // 
            this.lblDbtReviewDateD.BackColor = System.Drawing.Color.White;
            this.lblDbtReviewDateD.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblDbtReviewDateD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDbtReviewDateD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtReviewStatus.DebtReviewStatusDate")});
            this.lblDbtReviewDateD.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDbtReviewDateD.ForeColor = System.Drawing.Color.DimGray;
            this.lblDbtReviewDateD.Name = "lblDbtReviewDateD";
            this.lblDbtReviewDateD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDbtReviewDateD.StylePriority.UseBackColor = false;
            this.lblDbtReviewDateD.StylePriority.UseBorderColor = false;
            this.lblDbtReviewDateD.StylePriority.UseBorders = false;
            this.lblDbtReviewDateD.StylePriority.UseFont = false;
            this.lblDbtReviewDateD.StylePriority.UseForeColor = false;
            this.lblDbtReviewDateD.StylePriority.UsePadding = false;
            this.lblDbtReviewDateD.StylePriority.UseTextAlignment = false;
            this.lblDbtReviewDateD.Text = "lblDbtReviewDateD";
            this.lblDbtReviewDateD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDbtReviewDateD.Weight = 2.2424242424242422;
            // 
            // xrTableRow98
            // 
            this.xrTableRow98.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCounsellorName,
            this.lblCounsellorNameD});
            this.xrTableRow98.Name = "xrTableRow98";
            this.xrTableRow98.Weight = 0.79999999999999993;
            // 
            // lblCounsellorName
            // 
            this.lblCounsellorName.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblCounsellorName.BorderColor = System.Drawing.Color.White;
            this.lblCounsellorName.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblCounsellorName.ForeColor = System.Drawing.Color.Gray;
            this.lblCounsellorName.Name = "lblCounsellorName";
            this.lblCounsellorName.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblCounsellorName.StylePriority.UseBackColor = false;
            this.lblCounsellorName.StylePriority.UseBorderColor = false;
            this.lblCounsellorName.StylePriority.UseFont = false;
            this.lblCounsellorName.StylePriority.UseForeColor = false;
            this.lblCounsellorName.StylePriority.UsePadding = false;
            this.lblCounsellorName.StylePriority.UseTextAlignment = false;
            this.lblCounsellorName.Text = "Debt Counsellor Name";
            this.lblCounsellorName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblCounsellorName.Weight = 0.75757575757575757;
            // 
            // lblCounsellorNameD
            // 
            this.lblCounsellorNameD.BackColor = System.Drawing.Color.White;
            this.lblCounsellorNameD.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblCounsellorNameD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblCounsellorNameD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtReviewStatus.DebtCounsellorName")});
            this.lblCounsellorNameD.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblCounsellorNameD.ForeColor = System.Drawing.Color.DimGray;
            this.lblCounsellorNameD.Name = "lblCounsellorNameD";
            this.lblCounsellorNameD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblCounsellorNameD.StylePriority.UseBackColor = false;
            this.lblCounsellorNameD.StylePriority.UseBorderColor = false;
            this.lblCounsellorNameD.StylePriority.UseBorders = false;
            this.lblCounsellorNameD.StylePriority.UseFont = false;
            this.lblCounsellorNameD.StylePriority.UseForeColor = false;
            this.lblCounsellorNameD.StylePriority.UsePadding = false;
            this.lblCounsellorNameD.StylePriority.UseTextAlignment = false;
            this.lblCounsellorNameD.Text = "lblCounsellorNameD";
            this.lblCounsellorNameD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblCounsellorNameD.Weight = 2.2424242424242422;
            // 
            // xrTableRow97
            // 
            this.xrTableRow97.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCounsellorTelephoneNo,
            this.lblCounsellorTelephoneNoD});
            this.xrTableRow97.Name = "xrTableRow97";
            this.xrTableRow97.Weight = 0.8;
            // 
            // lblCounsellorTelephoneNo
            // 
            this.lblCounsellorTelephoneNo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblCounsellorTelephoneNo.BorderColor = System.Drawing.Color.White;
            this.lblCounsellorTelephoneNo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblCounsellorTelephoneNo.ForeColor = System.Drawing.Color.Gray;
            this.lblCounsellorTelephoneNo.Name = "lblCounsellorTelephoneNo";
            this.lblCounsellorTelephoneNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblCounsellorTelephoneNo.StylePriority.UseBackColor = false;
            this.lblCounsellorTelephoneNo.StylePriority.UseBorderColor = false;
            this.lblCounsellorTelephoneNo.StylePriority.UseFont = false;
            this.lblCounsellorTelephoneNo.StylePriority.UseForeColor = false;
            this.lblCounsellorTelephoneNo.StylePriority.UsePadding = false;
            this.lblCounsellorTelephoneNo.StylePriority.UseTextAlignment = false;
            this.lblCounsellorTelephoneNo.Text = "Debt Counsellor Contact No.";
            this.lblCounsellorTelephoneNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblCounsellorTelephoneNo.Weight = 0.75757575757575757;
            // 
            // lblCounsellorTelephoneNoD
            // 
            this.lblCounsellorTelephoneNoD.BackColor = System.Drawing.Color.White;
            this.lblCounsellorTelephoneNoD.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblCounsellorTelephoneNoD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblCounsellorTelephoneNoD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtReviewStatus.DebtCounsellorTelephoneNo")});
            this.lblCounsellorTelephoneNoD.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblCounsellorTelephoneNoD.ForeColor = System.Drawing.Color.DimGray;
            this.lblCounsellorTelephoneNoD.Name = "lblCounsellorTelephoneNoD";
            this.lblCounsellorTelephoneNoD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblCounsellorTelephoneNoD.StylePriority.UseBackColor = false;
            this.lblCounsellorTelephoneNoD.StylePriority.UseBorderColor = false;
            this.lblCounsellorTelephoneNoD.StylePriority.UseBorders = false;
            this.lblCounsellorTelephoneNoD.StylePriority.UseFont = false;
            this.lblCounsellorTelephoneNoD.StylePriority.UseForeColor = false;
            this.lblCounsellorTelephoneNoD.StylePriority.UsePadding = false;
            this.lblCounsellorTelephoneNoD.StylePriority.UseTextAlignment = false;
            this.lblCounsellorTelephoneNoD.Text = "lblCounsellorTelephoneNoD";
            this.lblCounsellorTelephoneNoD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblCounsellorTelephoneNoD.Weight = 2.2424242424242422;
            // 
            // GroupHeader21
            // 
            this.GroupHeader21.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tbldbtReviewH});
            this.GroupHeader21.HeightF = 33F;
            this.GroupHeader21.Name = "GroupHeader21";
            // 
            // tbldbtReviewH
            // 
            this.tbldbtReviewH.ForeColor = System.Drawing.Color.Black;
            this.tbldbtReviewH.LocationFloat = new DevExpress.Utils.PointFloat(8F, 8F);
            this.tbldbtReviewH.Name = "tbldbtReviewH";
            this.tbldbtReviewH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow95});
            this.tbldbtReviewH.SizeF = new System.Drawing.SizeF(792F, 25F);
            this.tbldbtReviewH.StylePriority.UseForeColor = false;
            // 
            // xrTableRow95
            // 
            this.xrTableRow95.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.xrTableRow95.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow95.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lbldbtReviewH});
            this.xrTableRow95.Name = "xrTableRow95";
            this.xrTableRow95.StylePriority.UseBorderColor = false;
            this.xrTableRow95.StylePriority.UseBorders = false;
            this.xrTableRow95.Weight = 1;
            // 
            // lbldbtReviewH
            // 
            this.lbldbtReviewH.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lbldbtReviewH.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.lbldbtReviewH.ForeColor = System.Drawing.Color.Gray;
            this.lbldbtReviewH.Name = "lbldbtReviewH";
            this.lbldbtReviewH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lbldbtReviewH.StylePriority.UseBackColor = false;
            this.lbldbtReviewH.StylePriority.UseFont = false;
            this.lbldbtReviewH.StylePriority.UseForeColor = false;
            this.lbldbtReviewH.StylePriority.UsePadding = false;
            this.lbldbtReviewH.StylePriority.UseTextAlignment = false;
            this.lbldbtReviewH.Text = "Debt Review Status";
            this.lbldbtReviewH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lbldbtReviewH.Weight = 3;
            // 
            // propertyInfo
            // 
            this.propertyInfo.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailpropertyInfo,
            this.GroupHeader23});
            this.propertyInfo.DataMember = "ConsumerPropertyInformation";
            this.propertyInfo.Level = 16;
            this.propertyInfo.Name = "propertyInfo";
            // 
            // DetailpropertyInfo
            // 
            this.DetailpropertyInfo.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblPropInterests});
            this.DetailpropertyInfo.HeightF = 141F;
            this.DetailpropertyInfo.Name = "DetailpropertyInfo";
            // 
            // tblPropInterests
            // 
            this.tblPropInterests.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblPropInterests.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblPropInterests.LocationFloat = new DevExpress.Utils.PointFloat(8F, 0F);
            this.tblPropInterests.Name = "tblPropInterests";
            this.tblPropInterests.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblPropInterests.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow49,
            this.xrTableRow120,
            this.xrTableRow125,
            this.xrTableRow124,
            this.xrTableRow123,
            this.xrTableRow122,
            this.xrTableRow121});
            this.tblPropInterests.SizeF = new System.Drawing.SizeF(792F, 141F);
            this.tblPropInterests.StylePriority.UseBorderColor = false;
            this.tblPropInterests.StylePriority.UseBorders = false;
            this.tblPropInterests.StylePriority.UsePadding = false;
            // 
            // xrTableRow49
            // 
            this.xrTableRow49.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell16});
            this.xrTableRow49.Name = "xrTableRow49";
            this.xrTableRow49.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableRow49.StylePriority.UsePadding = false;
            this.xrTableRow49.StylePriority.UseTextAlignment = false;
            this.xrTableRow49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow49.Weight = 0.834319526627219;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell16.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.xrTableCell16.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell16.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.TitleDeedNo")});
            this.xrTableCell16.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell16.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.StylePriority.UseBackColor = false;
            this.xrTableCell16.StylePriority.UseBorderColor = false;
            this.xrTableCell16.StylePriority.UseBorders = false;
            this.xrTableCell16.StylePriority.UseFont = false;
            this.xrTableCell16.StylePriority.UseForeColor = false;
            xrSummary2.FormatString = "Property Interests No: {0}";
            xrSummary2.Func = DevExpress.XtraReports.UI.SummaryFunc.RecordNumber;
            xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell16.Summary = xrSummary2;
            this.xrTableCell16.Text = "xrTableCell16";
            this.xrTableCell16.Weight = 3;
            // 
            // xrTableRow120
            // 
            this.xrTableRow120.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDeedNo,
            this.lblDDeedNo,
            this.lblsiteno,
            this.lblDsiteno});
            this.xrTableRow120.Name = "xrTableRow120";
            this.xrTableRow120.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableRow120.StylePriority.UsePadding = false;
            this.xrTableRow120.StylePriority.UseTextAlignment = false;
            this.xrTableRow120.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow120.Weight = 0.83431952662721887;
            // 
            // lblDeedNo
            // 
            this.lblDeedNo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblDeedNo.BorderColor = System.Drawing.Color.White;
            this.lblDeedNo.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDeedNo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDeedNo.ForeColor = System.Drawing.Color.Gray;
            this.lblDeedNo.Name = "lblDeedNo";
            this.lblDeedNo.StylePriority.UseBackColor = false;
            this.lblDeedNo.StylePriority.UseBorderColor = false;
            this.lblDeedNo.StylePriority.UseBorders = false;
            this.lblDeedNo.StylePriority.UseFont = false;
            this.lblDeedNo.StylePriority.UseForeColor = false;
            this.lblDeedNo.Text = "Title Deed number";
            this.lblDeedNo.Weight = 0.625;
            // 
            // lblDDeedNo
            // 
            this.lblDDeedNo.BackColor = System.Drawing.Color.White;
            this.lblDDeedNo.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblDDeedNo.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDDeedNo.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.TitleDeedNo")});
            this.lblDDeedNo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDDeedNo.ForeColor = System.Drawing.Color.DimGray;
            this.lblDDeedNo.Name = "lblDDeedNo";
            this.lblDDeedNo.StylePriority.UseBackColor = false;
            this.lblDDeedNo.StylePriority.UseBorderColor = false;
            this.lblDDeedNo.StylePriority.UseBorders = false;
            this.lblDDeedNo.StylePriority.UseFont = false;
            this.lblDDeedNo.StylePriority.UseForeColor = false;
            this.lblDDeedNo.Text = "lblDDeedNo";
            this.lblDDeedNo.Weight = 0.90625;
            // 
            // lblsiteno
            // 
            this.lblsiteno.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblsiteno.BorderColor = System.Drawing.Color.White;
            this.lblsiteno.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblsiteno.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblsiteno.ForeColor = System.Drawing.Color.Gray;
            this.lblsiteno.Name = "lblsiteno";
            this.lblsiteno.StylePriority.UseBackColor = false;
            this.lblsiteno.StylePriority.UseBorderColor = false;
            this.lblsiteno.StylePriority.UseBorders = false;
            this.lblsiteno.StylePriority.UseFont = false;
            this.lblsiteno.StylePriority.UseForeColor = false;
            this.lblsiteno.Text = "Erf/Site No.";
            this.lblsiteno.Weight = 0.59375;
            // 
            // lblDsiteno
            // 
            this.lblDsiteno.BackColor = System.Drawing.Color.White;
            this.lblDsiteno.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblDsiteno.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDsiteno.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.ErfNo")});
            this.lblDsiteno.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDsiteno.ForeColor = System.Drawing.Color.DimGray;
            this.lblDsiteno.Name = "lblDsiteno";
            this.lblDsiteno.StylePriority.UseBackColor = false;
            this.lblDsiteno.StylePriority.UseBorderColor = false;
            this.lblDsiteno.StylePriority.UseBorders = false;
            this.lblDsiteno.StylePriority.UseFont = false;
            this.lblDsiteno.StylePriority.UseForeColor = false;
            this.lblDsiteno.Text = "lblDsiteno";
            this.lblDsiteno.Weight = 0.875;
            // 
            // xrTableRow125
            // 
            this.xrTableRow125.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDeedsoffice,
            this.lblDDeedsoffice,
            this.lblPhyadd,
            this.lblDPhyadd});
            this.xrTableRow125.Name = "xrTableRow125";
            this.xrTableRow125.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableRow125.StylePriority.UsePadding = false;
            this.xrTableRow125.StylePriority.UseTextAlignment = false;
            this.xrTableRow125.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow125.Weight = 0.83431952662721887;
            // 
            // lblDeedsoffice
            // 
            this.lblDeedsoffice.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblDeedsoffice.BorderColor = System.Drawing.Color.White;
            this.lblDeedsoffice.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDeedsoffice.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDeedsoffice.ForeColor = System.Drawing.Color.Gray;
            this.lblDeedsoffice.Name = "lblDeedsoffice";
            this.lblDeedsoffice.StylePriority.UseBackColor = false;
            this.lblDeedsoffice.StylePriority.UseBorderColor = false;
            this.lblDeedsoffice.StylePriority.UseBorders = false;
            this.lblDeedsoffice.StylePriority.UseFont = false;
            this.lblDeedsoffice.StylePriority.UseForeColor = false;
            this.lblDeedsoffice.Text = "Deeds Office";
            this.lblDeedsoffice.Weight = 0.625;
            // 
            // lblDDeedsoffice
            // 
            this.lblDDeedsoffice.BackColor = System.Drawing.Color.White;
            this.lblDDeedsoffice.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblDDeedsoffice.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDDeedsoffice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.DeedsOffice")});
            this.lblDDeedsoffice.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDDeedsoffice.ForeColor = System.Drawing.Color.DimGray;
            this.lblDDeedsoffice.Name = "lblDDeedsoffice";
            this.lblDDeedsoffice.StylePriority.UseBackColor = false;
            this.lblDDeedsoffice.StylePriority.UseBorderColor = false;
            this.lblDDeedsoffice.StylePriority.UseBorders = false;
            this.lblDDeedsoffice.StylePriority.UseFont = false;
            this.lblDDeedsoffice.StylePriority.UseForeColor = false;
            this.lblDDeedsoffice.Text = "lblDDeedsoffice";
            this.lblDDeedsoffice.Weight = 0.90625;
            // 
            // lblPhyadd
            // 
            this.lblPhyadd.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblPhyadd.BorderColor = System.Drawing.Color.White;
            this.lblPhyadd.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblPhyadd.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblPhyadd.ForeColor = System.Drawing.Color.Gray;
            this.lblPhyadd.Name = "lblPhyadd";
            this.lblPhyadd.StylePriority.UseBackColor = false;
            this.lblPhyadd.StylePriority.UseBorderColor = false;
            this.lblPhyadd.StylePriority.UseBorders = false;
            this.lblPhyadd.StylePriority.UseFont = false;
            this.lblPhyadd.StylePriority.UseForeColor = false;
            this.lblPhyadd.Text = "Physical Address";
            this.lblPhyadd.Weight = 0.59375;
            // 
            // lblDPhyadd
            // 
            this.lblDPhyadd.BackColor = System.Drawing.Color.White;
            this.lblDPhyadd.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblDPhyadd.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDPhyadd.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.PhysicalAddress")});
            this.lblDPhyadd.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDPhyadd.ForeColor = System.Drawing.Color.DimGray;
            this.lblDPhyadd.Name = "lblDPhyadd";
            this.lblDPhyadd.StylePriority.UseBackColor = false;
            this.lblDPhyadd.StylePriority.UseBorderColor = false;
            this.lblDPhyadd.StylePriority.UseBorders = false;
            this.lblDPhyadd.StylePriority.UseFont = false;
            this.lblDPhyadd.StylePriority.UseForeColor = false;
            this.lblDPhyadd.Text = "lblDPhyadd";
            this.lblDPhyadd.Weight = 0.875;
            // 
            // xrTableRow124
            // 
            this.xrTableRow124.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblPropType,
            this.lblDPropType,
            this.lblSize,
            this.lblDSize});
            this.xrTableRow124.Name = "xrTableRow124";
            this.xrTableRow124.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableRow124.StylePriority.UsePadding = false;
            this.xrTableRow124.StylePriority.UseTextAlignment = false;
            this.xrTableRow124.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow124.Weight = 0.83431952662721875;
            // 
            // lblPropType
            // 
            this.lblPropType.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblPropType.BorderColor = System.Drawing.Color.White;
            this.lblPropType.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblPropType.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblPropType.ForeColor = System.Drawing.Color.Gray;
            this.lblPropType.Name = "lblPropType";
            this.lblPropType.StylePriority.UseBackColor = false;
            this.lblPropType.StylePriority.UseBorderColor = false;
            this.lblPropType.StylePriority.UseBorders = false;
            this.lblPropType.StylePriority.UseFont = false;
            this.lblPropType.StylePriority.UseForeColor = false;
            this.lblPropType.Text = "Property Type";
            this.lblPropType.Weight = 0.625;
            // 
            // lblDPropType
            // 
            this.lblDPropType.BackColor = System.Drawing.Color.White;
            this.lblDPropType.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblDPropType.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDPropType.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.PropertyTypeDesc")});
            this.lblDPropType.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDPropType.ForeColor = System.Drawing.Color.DimGray;
            this.lblDPropType.Name = "lblDPropType";
            this.lblDPropType.StylePriority.UseBackColor = false;
            this.lblDPropType.StylePriority.UseBorderColor = false;
            this.lblDPropType.StylePriority.UseBorders = false;
            this.lblDPropType.StylePriority.UseFont = false;
            this.lblDPropType.StylePriority.UseForeColor = false;
            this.lblDPropType.Text = "lblDPropType";
            this.lblDPropType.Weight = 0.90625;
            // 
            // lblSize
            // 
            this.lblSize.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblSize.BorderColor = System.Drawing.Color.White;
            this.lblSize.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSize.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblSize.ForeColor = System.Drawing.Color.Gray;
            this.lblSize.Name = "lblSize";
            this.lblSize.StylePriority.UseBackColor = false;
            this.lblSize.StylePriority.UseBorderColor = false;
            this.lblSize.StylePriority.UseBorders = false;
            this.lblSize.StylePriority.UseFont = false;
            this.lblSize.StylePriority.UseForeColor = false;
            this.lblSize.Text = "Extent / Size";
            this.lblSize.Weight = 0.59375;
            // 
            // lblDSize
            // 
            this.lblDSize.BackColor = System.Drawing.Color.White;
            this.lblDSize.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblDSize.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDSize.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.ErfSize")});
            this.lblDSize.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDSize.ForeColor = System.Drawing.Color.DimGray;
            this.lblDSize.Name = "lblDSize";
            this.lblDSize.StylePriority.UseBackColor = false;
            this.lblDSize.StylePriority.UseBorderColor = false;
            this.lblDSize.StylePriority.UseBorders = false;
            this.lblDSize.StylePriority.UseFont = false;
            this.lblDSize.StylePriority.UseForeColor = false;
            this.lblDSize.Text = "lblDSize";
            this.lblDSize.Weight = 0.875;
            // 
            // xrTableRow123
            // 
            this.xrTableRow123.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblPurchasedate,
            this.lblDPurchasedate,
            this.lblPurchaseprice,
            this.lblDPurchaseprice});
            this.xrTableRow123.Name = "xrTableRow123";
            this.xrTableRow123.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableRow123.StylePriority.UsePadding = false;
            this.xrTableRow123.StylePriority.UseTextAlignment = false;
            this.xrTableRow123.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow123.Weight = 0.834319526627219;
            // 
            // lblPurchasedate
            // 
            this.lblPurchasedate.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblPurchasedate.BorderColor = System.Drawing.Color.White;
            this.lblPurchasedate.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblPurchasedate.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblPurchasedate.ForeColor = System.Drawing.Color.Gray;
            this.lblPurchasedate.Name = "lblPurchasedate";
            this.lblPurchasedate.StylePriority.UseBackColor = false;
            this.lblPurchasedate.StylePriority.UseBorderColor = false;
            this.lblPurchasedate.StylePriority.UseBorders = false;
            this.lblPurchasedate.StylePriority.UseFont = false;
            this.lblPurchasedate.StylePriority.UseForeColor = false;
            this.lblPurchasedate.Text = "Purchase Date";
            this.lblPurchasedate.Weight = 0.625;
            // 
            // lblDPurchasedate
            // 
            this.lblDPurchasedate.BackColor = System.Drawing.Color.White;
            this.lblDPurchasedate.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblDPurchasedate.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDPurchasedate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.PurchaseDate")});
            this.lblDPurchasedate.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDPurchasedate.ForeColor = System.Drawing.Color.DimGray;
            this.lblDPurchasedate.Name = "lblDPurchasedate";
            this.lblDPurchasedate.StylePriority.UseBackColor = false;
            this.lblDPurchasedate.StylePriority.UseBorderColor = false;
            this.lblDPurchasedate.StylePriority.UseBorders = false;
            this.lblDPurchasedate.StylePriority.UseFont = false;
            this.lblDPurchasedate.StylePriority.UseForeColor = false;
            this.lblDPurchasedate.Text = "lblDPurchasedate";
            this.lblDPurchasedate.Weight = 0.90625;
            // 
            // lblPurchaseprice
            // 
            this.lblPurchaseprice.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblPurchaseprice.BorderColor = System.Drawing.Color.White;
            this.lblPurchaseprice.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblPurchaseprice.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblPurchaseprice.ForeColor = System.Drawing.Color.Gray;
            this.lblPurchaseprice.Name = "lblPurchaseprice";
            this.lblPurchaseprice.StylePriority.UseBackColor = false;
            this.lblPurchaseprice.StylePriority.UseBorderColor = false;
            this.lblPurchaseprice.StylePriority.UseBorders = false;
            this.lblPurchaseprice.StylePriority.UseFont = false;
            this.lblPurchaseprice.StylePriority.UseForeColor = false;
            this.lblPurchaseprice.Text = "Purchase price";
            this.lblPurchaseprice.Weight = 0.59375;
            // 
            // lblDPurchaseprice
            // 
            this.lblDPurchaseprice.BackColor = System.Drawing.Color.White;
            this.lblDPurchaseprice.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblDPurchaseprice.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDPurchaseprice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.PurchasePriceAmt")});
            this.lblDPurchaseprice.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDPurchaseprice.ForeColor = System.Drawing.Color.DimGray;
            this.lblDPurchaseprice.Name = "lblDPurchaseprice";
            this.lblDPurchaseprice.StylePriority.UseBackColor = false;
            this.lblDPurchaseprice.StylePriority.UseBorderColor = false;
            this.lblDPurchaseprice.StylePriority.UseBorders = false;
            this.lblDPurchaseprice.StylePriority.UseFont = false;
            this.lblDPurchaseprice.StylePriority.UseForeColor = false;
            this.lblDPurchaseprice.Text = "lblDPurchaseprice";
            this.lblDPurchaseprice.Weight = 0.875;
            // 
            // xrTableRow122
            // 
            this.xrTableRow122.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblshareperc,
            this.lblDshareperc,
            this.lblHolder,
            this.lblDHolder});
            this.xrTableRow122.Name = "xrTableRow122";
            this.xrTableRow122.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableRow122.StylePriority.UsePadding = false;
            this.xrTableRow122.StylePriority.UseTextAlignment = false;
            this.xrTableRow122.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow122.Weight = 0.834319526627219;
            // 
            // lblshareperc
            // 
            this.lblshareperc.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblshareperc.BorderColor = System.Drawing.Color.White;
            this.lblshareperc.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblshareperc.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblshareperc.ForeColor = System.Drawing.Color.Gray;
            this.lblshareperc.Name = "lblshareperc";
            this.lblshareperc.StylePriority.UseBackColor = false;
            this.lblshareperc.StylePriority.UseBorderColor = false;
            this.lblshareperc.StylePriority.UseBorders = false;
            this.lblshareperc.StylePriority.UseFont = false;
            this.lblshareperc.StylePriority.UseForeColor = false;
            this.lblshareperc.Text = "% Ownership";
            this.lblshareperc.Weight = 0.625;
            // 
            // lblDshareperc
            // 
            this.lblDshareperc.BackColor = System.Drawing.Color.White;
            this.lblDshareperc.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblDshareperc.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDshareperc.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.BuyerSharePerc")});
            this.lblDshareperc.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDshareperc.ForeColor = System.Drawing.Color.DimGray;
            this.lblDshareperc.Name = "lblDshareperc";
            this.lblDshareperc.StylePriority.UseBackColor = false;
            this.lblDshareperc.StylePriority.UseBorderColor = false;
            this.lblDshareperc.StylePriority.UseBorders = false;
            this.lblDshareperc.StylePriority.UseFont = false;
            this.lblDshareperc.StylePriority.UseForeColor = false;
            this.lblDshareperc.Text = "lblDshareperc";
            this.lblDshareperc.Weight = 0.90625;
            // 
            // lblHolder
            // 
            this.lblHolder.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblHolder.BorderColor = System.Drawing.Color.White;
            this.lblHolder.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblHolder.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblHolder.ForeColor = System.Drawing.Color.Gray;
            this.lblHolder.Name = "lblHolder";
            this.lblHolder.StylePriority.UseBackColor = false;
            this.lblHolder.StylePriority.UseBorderColor = false;
            this.lblHolder.StylePriority.UseBorders = false;
            this.lblHolder.StylePriority.UseFont = false;
            this.lblHolder.StylePriority.UseForeColor = false;
            this.lblHolder.Text = "Bond Holder";
            this.lblHolder.Weight = 0.59375;
            // 
            // lblDHolder
            // 
            this.lblDHolder.BackColor = System.Drawing.Color.White;
            this.lblDHolder.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblDHolder.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDHolder.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.BondHolderName")});
            this.lblDHolder.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDHolder.ForeColor = System.Drawing.Color.DimGray;
            this.lblDHolder.Name = "lblDHolder";
            this.lblDHolder.StylePriority.UseBackColor = false;
            this.lblDHolder.StylePriority.UseBorderColor = false;
            this.lblDHolder.StylePriority.UseBorders = false;
            this.lblDHolder.StylePriority.UseFont = false;
            this.lblDHolder.StylePriority.UseForeColor = false;
            this.lblDHolder.Text = "lblDHolder";
            this.lblDHolder.Weight = 0.875;
            // 
            // xrTableRow121
            // 
            this.xrTableRow121.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblBondNo,
            this.lblDBondNo,
            this.lblBondAmount,
            this.lblDBondAmount});
            this.xrTableRow121.Name = "xrTableRow121";
            this.xrTableRow121.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableRow121.StylePriority.UsePadding = false;
            this.xrTableRow121.StylePriority.UseTextAlignment = false;
            this.xrTableRow121.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow121.Weight = 0.83431952662721887;
            // 
            // lblBondNo
            // 
            this.lblBondNo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblBondNo.BorderColor = System.Drawing.Color.White;
            this.lblBondNo.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblBondNo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblBondNo.ForeColor = System.Drawing.Color.Gray;
            this.lblBondNo.Name = "lblBondNo";
            this.lblBondNo.StylePriority.UseBackColor = false;
            this.lblBondNo.StylePriority.UseBorderColor = false;
            this.lblBondNo.StylePriority.UseBorders = false;
            this.lblBondNo.StylePriority.UseFont = false;
            this.lblBondNo.StylePriority.UseForeColor = false;
            this.lblBondNo.Text = "Bond Number";
            this.lblBondNo.Weight = 0.625;
            // 
            // lblDBondNo
            // 
            this.lblDBondNo.BackColor = System.Drawing.Color.White;
            this.lblDBondNo.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblDBondNo.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDBondNo.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.BondAccountNo")});
            this.lblDBondNo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDBondNo.ForeColor = System.Drawing.Color.DimGray;
            this.lblDBondNo.Name = "lblDBondNo";
            this.lblDBondNo.StylePriority.UseBackColor = false;
            this.lblDBondNo.StylePriority.UseBorderColor = false;
            this.lblDBondNo.StylePriority.UseBorders = false;
            this.lblDBondNo.StylePriority.UseFont = false;
            this.lblDBondNo.StylePriority.UseForeColor = false;
            this.lblDBondNo.Text = "lblDBondNo";
            this.lblDBondNo.Weight = 0.90625;
            // 
            // lblBondAmount
            // 
            this.lblBondAmount.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblBondAmount.BorderColor = System.Drawing.Color.White;
            this.lblBondAmount.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblBondAmount.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblBondAmount.ForeColor = System.Drawing.Color.Gray;
            this.lblBondAmount.Name = "lblBondAmount";
            this.lblBondAmount.StylePriority.UseBackColor = false;
            this.lblBondAmount.StylePriority.UseBorderColor = false;
            this.lblBondAmount.StylePriority.UseBorders = false;
            this.lblBondAmount.StylePriority.UseFont = false;
            this.lblBondAmount.StylePriority.UseForeColor = false;
            this.lblBondAmount.Text = "Bond Amount";
            this.lblBondAmount.Weight = 0.59375;
            // 
            // lblDBondAmount
            // 
            this.lblDBondAmount.BackColor = System.Drawing.Color.White;
            this.lblDBondAmount.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblDBondAmount.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDBondAmount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.BondAmt")});
            this.lblDBondAmount.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDBondAmount.ForeColor = System.Drawing.Color.DimGray;
            this.lblDBondAmount.Name = "lblDBondAmount";
            this.lblDBondAmount.StylePriority.UseBackColor = false;
            this.lblDBondAmount.StylePriority.UseBorderColor = false;
            this.lblDBondAmount.StylePriority.UseBorders = false;
            this.lblDBondAmount.StylePriority.UseFont = false;
            this.lblDBondAmount.StylePriority.UseForeColor = false;
            this.lblDBondAmount.Text = "lblDBondAmount";
            this.lblDBondAmount.Weight = 0.875;
            // 
            // GroupHeader23
            // 
            this.GroupHeader23.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblPropInterestsH});
            this.GroupHeader23.HeightF = 46F;
            this.GroupHeader23.Name = "GroupHeader23";
            // 
            // tblPropInterestsH
            // 
            this.tblPropInterestsH.ForeColor = System.Drawing.Color.Black;
            this.tblPropInterestsH.LocationFloat = new DevExpress.Utils.PointFloat(8F, 8F);
            this.tblPropInterestsH.Name = "tblPropInterestsH";
            this.tblPropInterestsH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow118});
            this.tblPropInterestsH.SizeF = new System.Drawing.SizeF(792F, 33F);
            this.tblPropInterestsH.StylePriority.UseForeColor = false;
            // 
            // xrTableRow118
            // 
            this.xrTableRow118.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblPropInterestsH});
            this.xrTableRow118.Name = "xrTableRow118";
            this.xrTableRow118.Weight = 1;
            // 
            // lblPropInterestsH
            // 
            this.lblPropInterestsH.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.lblPropInterestsH.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblPropInterestsH.BorderWidth = 2;
            this.lblPropInterestsH.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblPropInterestsH.ForeColor = System.Drawing.Color.Gray;
            this.lblPropInterestsH.Name = "lblPropInterestsH";
            this.lblPropInterestsH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblPropInterestsH.StylePriority.UseBorderColor = false;
            this.lblPropInterestsH.StylePriority.UseBorders = false;
            this.lblPropInterestsH.StylePriority.UseBorderWidth = false;
            this.lblPropInterestsH.StylePriority.UseFont = false;
            this.lblPropInterestsH.StylePriority.UseForeColor = false;
            this.lblPropInterestsH.StylePriority.UsePadding = false;
            this.lblPropInterestsH.StylePriority.UseTextAlignment = false;
            this.lblPropInterestsH.Text = "Property Interests";
            this.lblPropInterestsH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblPropInterestsH.Weight = 3;
            // 
            // DirectorshipLink
            // 
            this.DirectorshipLink.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailDirectorshipLink,
            this.GroupHeader27});
            this.DirectorshipLink.DataMember = "ConsumerDirectorShipLink";
            this.DirectorshipLink.Level = 17;
            this.DirectorshipLink.Name = "DirectorshipLink";
            // 
            // DetailDirectorshipLink
            // 
            this.DetailDirectorshipLink.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblDirInfo});
            this.DetailDirectorshipLink.HeightF = 100F;
            this.DetailDirectorshipLink.Name = "DetailDirectorshipLink";
            // 
            // tblDirInfo
            // 
            this.tblDirInfo.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblDirInfo.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblDirInfo.LocationFloat = new DevExpress.Utils.PointFloat(8F, 0F);
            this.tblDirInfo.Name = "tblDirInfo";
            this.tblDirInfo.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblDirInfo.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow50,
            this.xrTableRow113,
            this.xrTableRow116,
            this.xrTableRow117,
            this.xrTableRow115});
            this.tblDirInfo.SizeF = new System.Drawing.SizeF(792F, 100F);
            this.tblDirInfo.StylePriority.UseBorderColor = false;
            this.tblDirInfo.StylePriority.UseBorders = false;
            this.tblDirInfo.StylePriority.UsePadding = false;
            this.tblDirInfo.StylePriority.UseTextAlignment = false;
            this.tblDirInfo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow50
            // 
            this.xrTableRow50.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell17});
            this.xrTableRow50.Name = "xrTableRow50";
            this.xrTableRow50.Weight = 0.83471074380165289;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell17.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.xrTableCell17.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell17.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.CommercialName")});
            this.xrTableCell17.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell17.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.StylePriority.UseBackColor = false;
            this.xrTableCell17.StylePriority.UseBorderColor = false;
            this.xrTableCell17.StylePriority.UseBorders = false;
            this.xrTableCell17.StylePriority.UseFont = false;
            this.xrTableCell17.StylePriority.UseForeColor = false;
            this.xrTableCell17.StylePriority.UseTextAlignment = false;
            xrSummary3.FormatString = "Director Link No: {0}";
            xrSummary3.Func = DevExpress.XtraReports.UI.SummaryFunc.RecordNumber;
            xrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell17.Summary = xrSummary3;
            this.xrTableCell17.Text = "xrTableCell17";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell17.Weight = 3;
            // 
            // xrTableRow113
            // 
            this.xrTableRow113.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCurrentPost,
            this.lblDCurrentPost,
            this.lblInceptionDate,
            this.lblDAppDate});
            this.xrTableRow113.Name = "xrTableRow113";
            this.xrTableRow113.Weight = 0.83471074380165278;
            // 
            // lblCurrentPost
            // 
            this.lblCurrentPost.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblCurrentPost.BorderColor = System.Drawing.Color.White;
            this.lblCurrentPost.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblCurrentPost.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblCurrentPost.ForeColor = System.Drawing.Color.Gray;
            this.lblCurrentPost.Name = "lblCurrentPost";
            this.lblCurrentPost.StylePriority.UseBackColor = false;
            this.lblCurrentPost.StylePriority.UseBorderColor = false;
            this.lblCurrentPost.StylePriority.UseBorders = false;
            this.lblCurrentPost.StylePriority.UseFont = false;
            this.lblCurrentPost.StylePriority.UseForeColor = false;
            this.lblCurrentPost.StylePriority.UseTextAlignment = false;
            this.lblCurrentPost.Text = "Current Post";
            this.lblCurrentPost.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblCurrentPost.Weight = 1;
            // 
            // lblDCurrentPost
            // 
            this.lblDCurrentPost.BackColor = System.Drawing.Color.White;
            this.lblDCurrentPost.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblDCurrentPost.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDCurrentPost.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.DirectorDesignationDesc")});
            this.lblDCurrentPost.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDCurrentPost.ForeColor = System.Drawing.Color.DimGray;
            this.lblDCurrentPost.Name = "lblDCurrentPost";
            this.lblDCurrentPost.StylePriority.UseBackColor = false;
            this.lblDCurrentPost.StylePriority.UseBorderColor = false;
            this.lblDCurrentPost.StylePriority.UseBorders = false;
            this.lblDCurrentPost.StylePriority.UseFont = false;
            this.lblDCurrentPost.StylePriority.UseForeColor = false;
            this.lblDCurrentPost.StylePriority.UseTextAlignment = false;
            this.lblDCurrentPost.Text = "lblDCurrentPost";
            this.lblDCurrentPost.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDCurrentPost.Weight = 0.67045454545454541;
            // 
            // lblInceptionDate
            // 
            this.lblInceptionDate.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblInceptionDate.BorderColor = System.Drawing.Color.White;
            this.lblInceptionDate.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblInceptionDate.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblInceptionDate.ForeColor = System.Drawing.Color.Gray;
            this.lblInceptionDate.Name = "lblInceptionDate";
            this.lblInceptionDate.StylePriority.UseBackColor = false;
            this.lblInceptionDate.StylePriority.UseBorderColor = false;
            this.lblInceptionDate.StylePriority.UseBorders = false;
            this.lblInceptionDate.StylePriority.UseFont = false;
            this.lblInceptionDate.StylePriority.UseForeColor = false;
            this.lblInceptionDate.StylePriority.UseTextAlignment = false;
            this.lblInceptionDate.Text = "Date of Inception";
            this.lblInceptionDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblInceptionDate.Weight = 0.63636363636363646;
            // 
            // lblDAppDate
            // 
            this.lblDAppDate.BackColor = System.Drawing.Color.White;
            this.lblDAppDate.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblDAppDate.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDAppDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.AppointmentDate")});
            this.lblDAppDate.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDAppDate.ForeColor = System.Drawing.Color.DimGray;
            this.lblDAppDate.Name = "lblDAppDate";
            this.lblDAppDate.StylePriority.UseBackColor = false;
            this.lblDAppDate.StylePriority.UseBorderColor = false;
            this.lblDAppDate.StylePriority.UseBorders = false;
            this.lblDAppDate.StylePriority.UseFont = false;
            this.lblDAppDate.StylePriority.UseForeColor = false;
            this.lblDAppDate.StylePriority.UseTextAlignment = false;
            this.lblDAppDate.Text = "lblDAppDate";
            this.lblDAppDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDAppDate.Weight = 0.69318181818181812;
            // 
            // xrTableRow116
            // 
            this.xrTableRow116.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCompanyName,
            this.lblDCompanyName,
            this.lblRegNo,
            this.lblDRegNo});
            this.xrTableRow116.Name = "xrTableRow116";
            this.xrTableRow116.Weight = 0.83471074380165278;
            // 
            // lblCompanyName
            // 
            this.lblCompanyName.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblCompanyName.BorderColor = System.Drawing.Color.White;
            this.lblCompanyName.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblCompanyName.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblCompanyName.ForeColor = System.Drawing.Color.Gray;
            this.lblCompanyName.Name = "lblCompanyName";
            this.lblCompanyName.StylePriority.UseBackColor = false;
            this.lblCompanyName.StylePriority.UseBorderColor = false;
            this.lblCompanyName.StylePriority.UseBorders = false;
            this.lblCompanyName.StylePriority.UseFont = false;
            this.lblCompanyName.StylePriority.UseForeColor = false;
            this.lblCompanyName.StylePriority.UseTextAlignment = false;
            this.lblCompanyName.Text = "Company Name";
            this.lblCompanyName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblCompanyName.Weight = 1;
            // 
            // lblDCompanyName
            // 
            this.lblDCompanyName.BackColor = System.Drawing.Color.White;
            this.lblDCompanyName.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblDCompanyName.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDCompanyName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.CommercialName")});
            this.lblDCompanyName.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDCompanyName.ForeColor = System.Drawing.Color.DimGray;
            this.lblDCompanyName.Name = "lblDCompanyName";
            this.lblDCompanyName.StylePriority.UseBackColor = false;
            this.lblDCompanyName.StylePriority.UseBorderColor = false;
            this.lblDCompanyName.StylePriority.UseBorders = false;
            this.lblDCompanyName.StylePriority.UseFont = false;
            this.lblDCompanyName.StylePriority.UseForeColor = false;
            this.lblDCompanyName.StylePriority.UseTextAlignment = false;
            this.lblDCompanyName.Text = "lblDCompanyName";
            this.lblDCompanyName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDCompanyName.Weight = 0.67045454545454541;
            // 
            // lblRegNo
            // 
            this.lblRegNo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblRegNo.BorderColor = System.Drawing.Color.White;
            this.lblRegNo.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblRegNo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblRegNo.ForeColor = System.Drawing.Color.Gray;
            this.lblRegNo.Name = "lblRegNo";
            this.lblRegNo.StylePriority.UseBackColor = false;
            this.lblRegNo.StylePriority.UseBorderColor = false;
            this.lblRegNo.StylePriority.UseBorders = false;
            this.lblRegNo.StylePriority.UseFont = false;
            this.lblRegNo.StylePriority.UseForeColor = false;
            this.lblRegNo.StylePriority.UseTextAlignment = false;
            this.lblRegNo.Text = "Company Reg No.";
            this.lblRegNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblRegNo.Weight = 0.63636363636363646;
            // 
            // lblDRegNo
            // 
            this.lblDRegNo.BackColor = System.Drawing.Color.White;
            this.lblDRegNo.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblDRegNo.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDRegNo.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.RegistrationNo")});
            this.lblDRegNo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDRegNo.ForeColor = System.Drawing.Color.DimGray;
            this.lblDRegNo.Name = "lblDRegNo";
            this.lblDRegNo.StylePriority.UseBackColor = false;
            this.lblDRegNo.StylePriority.UseBorderColor = false;
            this.lblDRegNo.StylePriority.UseBorders = false;
            this.lblDRegNo.StylePriority.UseFont = false;
            this.lblDRegNo.StylePriority.UseForeColor = false;
            this.lblDRegNo.StylePriority.UseTextAlignment = false;
            this.lblDRegNo.Text = "lblDRegNo";
            this.lblDRegNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDRegNo.Weight = 0.69318181818181812;
            // 
            // xrTableRow117
            // 
            this.xrTableRow117.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCompanyAdd,
            this.lblDPhysicalAdd,
            this.lblPhoneNo,
            this.lblDPhoneNo});
            this.xrTableRow117.Name = "xrTableRow117";
            this.xrTableRow117.Weight = 0.83471074380165278;
            // 
            // lblCompanyAdd
            // 
            this.lblCompanyAdd.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblCompanyAdd.BorderColor = System.Drawing.Color.White;
            this.lblCompanyAdd.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblCompanyAdd.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblCompanyAdd.ForeColor = System.Drawing.Color.Gray;
            this.lblCompanyAdd.Name = "lblCompanyAdd";
            this.lblCompanyAdd.StylePriority.UseBackColor = false;
            this.lblCompanyAdd.StylePriority.UseBorderColor = false;
            this.lblCompanyAdd.StylePriority.UseBorders = false;
            this.lblCompanyAdd.StylePriority.UseFont = false;
            this.lblCompanyAdd.StylePriority.UseForeColor = false;
            this.lblCompanyAdd.StylePriority.UseTextAlignment = false;
            this.lblCompanyAdd.Text = "Company Address";
            this.lblCompanyAdd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblCompanyAdd.Weight = 1;
            // 
            // lblDPhysicalAdd
            // 
            this.lblDPhysicalAdd.BackColor = System.Drawing.Color.White;
            this.lblDPhysicalAdd.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblDPhysicalAdd.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDPhysicalAdd.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.PhysicalAddress")});
            this.lblDPhysicalAdd.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDPhysicalAdd.ForeColor = System.Drawing.Color.DimGray;
            this.lblDPhysicalAdd.Name = "lblDPhysicalAdd";
            this.lblDPhysicalAdd.StylePriority.UseBackColor = false;
            this.lblDPhysicalAdd.StylePriority.UseBorderColor = false;
            this.lblDPhysicalAdd.StylePriority.UseBorders = false;
            this.lblDPhysicalAdd.StylePriority.UseFont = false;
            this.lblDPhysicalAdd.StylePriority.UseForeColor = false;
            this.lblDPhysicalAdd.StylePriority.UseTextAlignment = false;
            this.lblDPhysicalAdd.Text = "lblDPhysicalAdd";
            this.lblDPhysicalAdd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDPhysicalAdd.Weight = 0.67045454545454541;
            // 
            // lblPhoneNo
            // 
            this.lblPhoneNo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblPhoneNo.BorderColor = System.Drawing.Color.White;
            this.lblPhoneNo.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblPhoneNo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblPhoneNo.ForeColor = System.Drawing.Color.Gray;
            this.lblPhoneNo.Name = "lblPhoneNo";
            this.lblPhoneNo.StylePriority.UseBackColor = false;
            this.lblPhoneNo.StylePriority.UseBorderColor = false;
            this.lblPhoneNo.StylePriority.UseBorders = false;
            this.lblPhoneNo.StylePriority.UseFont = false;
            this.lblPhoneNo.StylePriority.UseForeColor = false;
            this.lblPhoneNo.StylePriority.UseTextAlignment = false;
            this.lblPhoneNo.Text = "Company Phone No.";
            this.lblPhoneNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblPhoneNo.Weight = 0.63636363636363646;
            // 
            // lblDPhoneNo
            // 
            this.lblDPhoneNo.BackColor = System.Drawing.Color.White;
            this.lblDPhoneNo.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblDPhoneNo.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDPhoneNo.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.TelephoneNo")});
            this.lblDPhoneNo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDPhoneNo.ForeColor = System.Drawing.Color.DimGray;
            this.lblDPhoneNo.Name = "lblDPhoneNo";
            this.lblDPhoneNo.StylePriority.UseBackColor = false;
            this.lblDPhoneNo.StylePriority.UseBorderColor = false;
            this.lblDPhoneNo.StylePriority.UseBorders = false;
            this.lblDPhoneNo.StylePriority.UseFont = false;
            this.lblDPhoneNo.StylePriority.UseForeColor = false;
            this.lblDPhoneNo.StylePriority.UseTextAlignment = false;
            this.lblDPhoneNo.Text = "lblDPhoneNo";
            this.lblDPhoneNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDPhoneNo.Weight = 0.69318181818181812;
            // 
            // xrTableRow115
            // 
            this.xrTableRow115.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblIndustryCatg,
            this.lblDIndCateg});
            this.xrTableRow115.Name = "xrTableRow115";
            this.xrTableRow115.Weight = 0.79338842975206625;
            // 
            // lblIndustryCatg
            // 
            this.lblIndustryCatg.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblIndustryCatg.BorderColor = System.Drawing.Color.White;
            this.lblIndustryCatg.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblIndustryCatg.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblIndustryCatg.ForeColor = System.Drawing.Color.Gray;
            this.lblIndustryCatg.Name = "lblIndustryCatg";
            this.lblIndustryCatg.StylePriority.UseBackColor = false;
            this.lblIndustryCatg.StylePriority.UseBorderColor = false;
            this.lblIndustryCatg.StylePriority.UseBorders = false;
            this.lblIndustryCatg.StylePriority.UseFont = false;
            this.lblIndustryCatg.StylePriority.UseForeColor = false;
            this.lblIndustryCatg.StylePriority.UseTextAlignment = false;
            this.lblIndustryCatg.Text = "Industry category";
            this.lblIndustryCatg.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblIndustryCatg.Weight = 1;
            // 
            // lblDIndCateg
            // 
            this.lblDIndCateg.BackColor = System.Drawing.Color.White;
            this.lblDIndCateg.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblDIndCateg.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.SICDesc")});
            this.lblDIndCateg.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDIndCateg.ForeColor = System.Drawing.Color.DimGray;
            this.lblDIndCateg.Name = "lblDIndCateg";
            this.lblDIndCateg.StylePriority.UseBackColor = false;
            this.lblDIndCateg.StylePriority.UseBorderColor = false;
            this.lblDIndCateg.StylePriority.UseFont = false;
            this.lblDIndCateg.StylePriority.UseForeColor = false;
            this.lblDIndCateg.StylePriority.UseTextAlignment = false;
            this.lblDIndCateg.Text = "lblDIndCateg";
            this.lblDIndCateg.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDIndCateg.Weight = 2;
            // 
            // GroupHeader27
            // 
            this.GroupHeader27.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblDirectorlinksH});
            this.GroupHeader27.HeightF = 47F;
            this.GroupHeader27.Name = "GroupHeader27";
            // 
            // tblDirectorlinksH
            // 
            this.tblDirectorlinksH.ForeColor = System.Drawing.Color.Black;
            this.tblDirectorlinksH.LocationFloat = new DevExpress.Utils.PointFloat(8F, 8F);
            this.tblDirectorlinksH.Name = "tblDirectorlinksH";
            this.tblDirectorlinksH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow112});
            this.tblDirectorlinksH.SizeF = new System.Drawing.SizeF(792F, 33F);
            this.tblDirectorlinksH.StylePriority.UseForeColor = false;
            // 
            // xrTableRow112
            // 
            this.xrTableRow112.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDirectorlinksH});
            this.xrTableRow112.Name = "xrTableRow112";
            this.xrTableRow112.Weight = 1;
            // 
            // lblDirectorlinksH
            // 
            this.lblDirectorlinksH.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.lblDirectorlinksH.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDirectorlinksH.BorderWidth = 2;
            this.lblDirectorlinksH.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblDirectorlinksH.ForeColor = System.Drawing.Color.Gray;
            this.lblDirectorlinksH.Name = "lblDirectorlinksH";
            this.lblDirectorlinksH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDirectorlinksH.StylePriority.UseBorderColor = false;
            this.lblDirectorlinksH.StylePriority.UseBorders = false;
            this.lblDirectorlinksH.StylePriority.UseBorderWidth = false;
            this.lblDirectorlinksH.StylePriority.UseFont = false;
            this.lblDirectorlinksH.StylePriority.UseForeColor = false;
            this.lblDirectorlinksH.StylePriority.UsePadding = false;
            this.lblDirectorlinksH.StylePriority.UseTextAlignment = false;
            this.lblDirectorlinksH.Text = "Directorship Links";
            this.lblDirectorlinksH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDirectorlinksH.Weight = 3;
            // 
            // MonthlyPayment
            // 
            this.MonthlyPayment.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailMonthlyPayment});
            this.MonthlyPayment.DataMember = "Consumer6MonthlyPaymentWithDelinquent";
            this.MonthlyPayment.Level = 7;
            this.MonthlyPayment.Name = "MonthlyPayment";
            // 
            // DetailMonthlyPayment
            // 
            this.DetailMonthlyPayment.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblMonthlyPaymentD});
            this.DetailMonthlyPayment.HeightF = 20F;
            this.DetailMonthlyPayment.Name = "DetailMonthlyPayment";
            // 
            // tblMonthlyPaymentD
            // 
            this.tblMonthlyPaymentD.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblMonthlyPaymentD.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblMonthlyPaymentD.LocationFloat = new DevExpress.Utils.PointFloat(8F, 0F);
            this.tblMonthlyPaymentD.Name = "tblMonthlyPaymentD";
            this.tblMonthlyPaymentD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblMonthlyPaymentD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow79});
            this.tblMonthlyPaymentD.SizeF = new System.Drawing.SizeF(792F, 20F);
            this.tblMonthlyPaymentD.StylePriority.UseBorderColor = false;
            this.tblMonthlyPaymentD.StylePriority.UseBorders = false;
            this.tblMonthlyPaymentD.StylePriority.UsePadding = false;
            this.tblMonthlyPaymentD.StylePriority.UseTextAlignment = false;
            this.tblMonthlyPaymentD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.tblMonthlyPaymentD.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.tblMonthlyPaymentD_BeforePrint);
            // 
            // xrTableRow79
            // 
            this.xrTableRow79.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblcompanyMPD,
            this.lblM01D,
            this.lblM02D,
            this.lblM03D,
            this.lblM04D,
            this.lblM05D,
            this.lblM06D,
            this.lblDelinquentStatusD});
            this.xrTableRow79.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.xrTableRow79.Name = "xrTableRow79";
            this.xrTableRow79.StylePriority.UseForeColor = false;
            this.xrTableRow79.Weight = 1;
            // 
            // lblcompanyMPD
            // 
            this.lblcompanyMPD.BackColor = System.Drawing.Color.White;
            this.lblcompanyMPD.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblcompanyMPD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblcompanyMPD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer6MonthlyPaymentWithDelinquent.SubscriberName")});
            this.lblcompanyMPD.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblcompanyMPD.ForeColor = System.Drawing.Color.DimGray;
            this.lblcompanyMPD.Name = "lblcompanyMPD";
            this.lblcompanyMPD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblcompanyMPD.StylePriority.UseBackColor = false;
            this.lblcompanyMPD.StylePriority.UseBorderColor = false;
            this.lblcompanyMPD.StylePriority.UseBorders = false;
            this.lblcompanyMPD.StylePriority.UseFont = false;
            this.lblcompanyMPD.StylePriority.UseForeColor = false;
            this.lblcompanyMPD.StylePriority.UsePadding = false;
            this.lblcompanyMPD.StylePriority.UseTextAlignment = false;
            this.lblcompanyMPD.Text = "lblcompanyMPD";
            this.lblcompanyMPD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblcompanyMPD.Weight = 1.4017368441629525;
            // 
            // lblM01D
            // 
            this.lblM01D.BackColor = System.Drawing.Color.White;
            this.lblM01D.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblM01D.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblM01D.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer6MonthlyPaymentWithDelinquent.M24")});
            this.lblM01D.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblM01D.ForeColor = System.Drawing.Color.DimGray;
            this.lblM01D.Name = "lblM01D";
            this.lblM01D.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblM01D.StylePriority.UseBackColor = false;
            this.lblM01D.StylePriority.UseBorderColor = false;
            this.lblM01D.StylePriority.UseBorders = false;
            this.lblM01D.StylePriority.UseFont = false;
            this.lblM01D.StylePriority.UseForeColor = false;
            this.lblM01D.StylePriority.UsePadding = false;
            this.lblM01D.StylePriority.UseTextAlignment = false;
            this.lblM01D.Text = "lblM01D";
            this.lblM01D.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblM01D.Weight = 0.3134457132350067;
            // 
            // lblM02D
            // 
            this.lblM02D.BackColor = System.Drawing.Color.White;
            this.lblM02D.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblM02D.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblM02D.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer6MonthlyPaymentWithDelinquent.M23")});
            this.lblM02D.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblM02D.ForeColor = System.Drawing.Color.DimGray;
            this.lblM02D.Name = "lblM02D";
            this.lblM02D.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblM02D.StylePriority.UseBackColor = false;
            this.lblM02D.StylePriority.UseBorderColor = false;
            this.lblM02D.StylePriority.UseBorders = false;
            this.lblM02D.StylePriority.UseFont = false;
            this.lblM02D.StylePriority.UseForeColor = false;
            this.lblM02D.StylePriority.UsePadding = false;
            this.lblM02D.StylePriority.UseTextAlignment = false;
            this.lblM02D.Text = "lblM02D";
            this.lblM02D.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblM02D.Weight = 0.31371572066326536;
            // 
            // lblM03D
            // 
            this.lblM03D.BackColor = System.Drawing.Color.White;
            this.lblM03D.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblM03D.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblM03D.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer6MonthlyPaymentWithDelinquent.M22")});
            this.lblM03D.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblM03D.ForeColor = System.Drawing.Color.DimGray;
            this.lblM03D.Name = "lblM03D";
            this.lblM03D.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblM03D.StylePriority.UseBackColor = false;
            this.lblM03D.StylePriority.UseBorderColor = false;
            this.lblM03D.StylePriority.UseBorders = false;
            this.lblM03D.StylePriority.UseFont = false;
            this.lblM03D.StylePriority.UseForeColor = false;
            this.lblM03D.StylePriority.UsePadding = false;
            this.lblM03D.StylePriority.UseTextAlignment = false;
            this.lblM03D.Text = "lblM03D";
            this.lblM03D.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblM03D.Weight = 0.30982940051020413;
            // 
            // lblM04D
            // 
            this.lblM04D.BackColor = System.Drawing.Color.White;
            this.lblM04D.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblM04D.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblM04D.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer6MonthlyPaymentWithDelinquent.M21")});
            this.lblM04D.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblM04D.ForeColor = System.Drawing.Color.DimGray;
            this.lblM04D.Name = "lblM04D";
            this.lblM04D.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblM04D.StylePriority.UseBackColor = false;
            this.lblM04D.StylePriority.UseBorderColor = false;
            this.lblM04D.StylePriority.UseBorders = false;
            this.lblM04D.StylePriority.UseFont = false;
            this.lblM04D.StylePriority.UseForeColor = false;
            this.lblM04D.StylePriority.UsePadding = false;
            this.lblM04D.StylePriority.UseTextAlignment = false;
            this.lblM04D.Text = "lblM04D";
            this.lblM04D.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblM04D.Weight = 0.31126434948979609;
            // 
            // lblM05D
            // 
            this.lblM05D.BackColor = System.Drawing.Color.White;
            this.lblM05D.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblM05D.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblM05D.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer6MonthlyPaymentWithDelinquent.M20")});
            this.lblM05D.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblM05D.ForeColor = System.Drawing.Color.DimGray;
            this.lblM05D.Name = "lblM05D";
            this.lblM05D.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblM05D.StylePriority.UseBackColor = false;
            this.lblM05D.StylePriority.UseBorderColor = false;
            this.lblM05D.StylePriority.UseBorders = false;
            this.lblM05D.StylePriority.UseFont = false;
            this.lblM05D.StylePriority.UseForeColor = false;
            this.lblM05D.StylePriority.UsePadding = false;
            this.lblM05D.StylePriority.UseTextAlignment = false;
            this.lblM05D.Text = "lblM05D";
            this.lblM05D.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblM05D.Weight = 0.30982940051020413;
            // 
            // lblM06D
            // 
            this.lblM06D.BackColor = System.Drawing.Color.White;
            this.lblM06D.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblM06D.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblM06D.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer6MonthlyPaymentWithDelinquent.M19")});
            this.lblM06D.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblM06D.ForeColor = System.Drawing.Color.DimGray;
            this.lblM06D.Name = "lblM06D";
            this.lblM06D.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblM06D.StylePriority.UseBackColor = false;
            this.lblM06D.StylePriority.UseBorderColor = false;
            this.lblM06D.StylePriority.UseBorders = false;
            this.lblM06D.StylePriority.UseFont = false;
            this.lblM06D.StylePriority.UseForeColor = false;
            this.lblM06D.StylePriority.UsePadding = false;
            this.lblM06D.StylePriority.UseTextAlignment = false;
            this.lblM06D.Text = "lblM06D";
            this.lblM06D.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblM06D.Weight = 0.30827487244897966;
            // 
            // lblDelinquentStatusD
            // 
            this.lblDelinquentStatusD.BackColor = System.Drawing.Color.White;
            this.lblDelinquentStatusD.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblDelinquentStatusD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDelinquentStatusD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer6MonthlyPaymentWithDelinquent.LastDeliquentStatus")});
            this.lblDelinquentStatusD.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDelinquentStatusD.ForeColor = System.Drawing.Color.DimGray;
            this.lblDelinquentStatusD.Name = "lblDelinquentStatusD";
            this.lblDelinquentStatusD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDelinquentStatusD.StylePriority.UseBackColor = false;
            this.lblDelinquentStatusD.StylePriority.UseBorderColor = false;
            this.lblDelinquentStatusD.StylePriority.UseBorders = false;
            this.lblDelinquentStatusD.StylePriority.UseFont = false;
            this.lblDelinquentStatusD.StylePriority.UseForeColor = false;
            this.lblDelinquentStatusD.StylePriority.UsePadding = false;
            this.lblDelinquentStatusD.StylePriority.UseTextAlignment = false;
            this.lblDelinquentStatusD.Text = "lblDelinquentStatusD";
            this.lblDelinquentStatusD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblDelinquentStatusD.Weight = 1.6566485969387763;
            // 
            // DefaultAlert
            // 
            this.DefaultAlert.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailDefaultAlert,
            this.GroupHeader2});
            this.DefaultAlert.DataMember = "ConsumerDefaultAlert";
            this.DefaultAlert.Level = 13;
            this.DefaultAlert.Name = "DefaultAlert";
            // 
            // DetailDefaultAlert
            // 
            this.DetailDefaultAlert.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblDefaultAlertD});
            this.DetailDefaultAlert.HeightF = 20F;
            this.DetailDefaultAlert.Name = "DetailDefaultAlert";
            // 
            // tblDefaultAlertD
            // 
            this.tblDefaultAlertD.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblDefaultAlertD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.tblDefaultAlertD.LocationFloat = new DevExpress.Utils.PointFloat(8F, 0F);
            this.tblDefaultAlertD.Name = "tblDefaultAlertD";
            this.tblDefaultAlertD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblDefaultAlertD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow16});
            this.tblDefaultAlertD.SizeF = new System.Drawing.SizeF(792F, 20F);
            this.tblDefaultAlertD.StylePriority.UseBorderColor = false;
            this.tblDefaultAlertD.StylePriority.UseBorders = false;
            this.tblDefaultAlertD.StylePriority.UsePadding = false;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.BackColor = System.Drawing.Color.White;
            this.xrTableRow16.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableRow16.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDACompanyD,
            this.lblDAAccnoD,
            this.lblDADLoadeddateD,
            this.lblDAAmountD,
            this.lblDAStatusD,
            this.lblDACommentsD});
            this.xrTableRow16.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableRow16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.StylePriority.UseBackColor = false;
            this.xrTableRow16.StylePriority.UseBorderColor = false;
            this.xrTableRow16.StylePriority.UseBorders = false;
            this.xrTableRow16.StylePriority.UseFont = false;
            this.xrTableRow16.StylePriority.UseForeColor = false;
            this.xrTableRow16.StylePriority.UseTextAlignment = false;
            this.xrTableRow16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow16.Weight = 0.8;
            // 
            // lblDACompanyD
            // 
            this.lblDACompanyD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDefaultAlert.Company")});
            this.lblDACompanyD.ForeColor = System.Drawing.Color.DimGray;
            this.lblDACompanyD.Name = "lblDACompanyD";
            this.lblDACompanyD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDACompanyD.StylePriority.UseForeColor = false;
            this.lblDACompanyD.StylePriority.UsePadding = false;
            this.lblDACompanyD.Text = "lblDSubscriber";
            this.lblDACompanyD.Weight = 0.725;
            // 
            // lblDAAccnoD
            // 
            this.lblDAAccnoD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDefaultAlert.AccountNo")});
            this.lblDAAccnoD.ForeColor = System.Drawing.Color.DimGray;
            this.lblDAAccnoD.Name = "lblDAAccnoD";
            this.lblDAAccnoD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDAAccnoD.StylePriority.UseForeColor = false;
            this.lblDAAccnoD.StylePriority.UsePadding = false;
            this.lblDAAccnoD.Text = "lblDAccNo";
            this.lblDAAccnoD.Weight = 0.46374999999999994;
            // 
            // lblDADLoadeddateD
            // 
            this.lblDADLoadeddateD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDefaultAlert.DateLoaded")});
            this.lblDADLoadeddateD.ForeColor = System.Drawing.Color.DimGray;
            this.lblDADLoadeddateD.Name = "lblDADLoadeddateD";
            this.lblDADLoadeddateD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDADLoadeddateD.StylePriority.UseForeColor = false;
            this.lblDADLoadeddateD.StylePriority.UsePadding = false;
            this.lblDADLoadeddateD.Text = "lblDActionDate";
            this.lblDADLoadeddateD.Weight = 0.31375;
            // 
            // lblDAAmountD
            // 
            this.lblDAAmountD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDefaultAlert.Amount")});
            this.lblDAAmountD.ForeColor = System.Drawing.Color.DimGray;
            this.lblDAAmountD.Name = "lblDAAmountD";
            this.lblDAAmountD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDAAmountD.StylePriority.UseForeColor = false;
            this.lblDAAmountD.StylePriority.UsePadding = false;
            this.lblDAAmountD.Text = "lblDAmount";
            this.lblDAAmountD.Weight = 0.3425;
            // 
            // lblDAStatusD
            // 
            this.lblDAStatusD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDefaultAlert.Statuscode")});
            this.lblDAStatusD.ForeColor = System.Drawing.Color.DimGray;
            this.lblDAStatusD.Name = "lblDAStatusD";
            this.lblDAStatusD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDAStatusD.StylePriority.UseForeColor = false;
            this.lblDAStatusD.StylePriority.UsePadding = false;
            this.lblDAStatusD.Text = "lblDAccStatus";
            this.lblDAStatusD.Weight = 0.41375;
            // 
            // lblDACommentsD
            // 
            this.lblDACommentsD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDefaultAlert.Comments")});
            this.lblDACommentsD.ForeColor = System.Drawing.Color.DimGray;
            this.lblDACommentsD.Name = "lblDACommentsD";
            this.lblDACommentsD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDACommentsD.StylePriority.UseForeColor = false;
            this.lblDACommentsD.StylePriority.UsePadding = false;
            this.lblDACommentsD.Weight = 0.74125;
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblDefaultAlertC,
            this.tblDefaultAlertH});
            this.GroupHeader2.HeightF = 54F;
            this.GroupHeader2.Name = "GroupHeader2";
            // 
            // tblDefaultAlertC
            // 
            this.tblDefaultAlertC.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tblDefaultAlertC.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblDefaultAlertC.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblDefaultAlertC.LocationFloat = new DevExpress.Utils.PointFloat(8F, 34F);
            this.tblDefaultAlertC.Name = "tblDefaultAlertC";
            this.tblDefaultAlertC.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblDefaultAlertC.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow15});
            this.tblDefaultAlertC.SizeF = new System.Drawing.SizeF(792F, 20F);
            this.tblDefaultAlertC.StylePriority.UseBorderColor = false;
            this.tblDefaultAlertC.StylePriority.UseBorders = false;
            this.tblDefaultAlertC.StylePriority.UsePadding = false;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableRow15.BorderColor = System.Drawing.Color.White;
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDACompany,
            this.lblDAAccno,
            this.lblDADLoadeddate,
            this.lblDAAmount,
            this.lblDAStatus,
            this.lblDAComments});
            this.xrTableRow15.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableRow15.ForeColor = System.Drawing.Color.Gray;
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableRow15.StylePriority.UseBackColor = false;
            this.xrTableRow15.StylePriority.UseBorderColor = false;
            this.xrTableRow15.StylePriority.UseFont = false;
            this.xrTableRow15.StylePriority.UseForeColor = false;
            this.xrTableRow15.StylePriority.UsePadding = false;
            this.xrTableRow15.StylePriority.UseTextAlignment = false;
            this.xrTableRow15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow15.Weight = 1;
            // 
            // lblDACompany
            // 
            this.lblDACompany.CanGrow = false;
            this.lblDACompany.Name = "lblDACompany";
            this.lblDACompany.Text = "Company";
            this.lblDACompany.Weight = 0.725;
            // 
            // lblDAAccno
            // 
            this.lblDAAccno.CanGrow = false;
            this.lblDAAccno.Name = "lblDAAccno";
            this.lblDAAccno.Text = "Account No.";
            this.lblDAAccno.Weight = 0.46625;
            // 
            // lblDADLoadeddate
            // 
            this.lblDADLoadeddate.CanGrow = false;
            this.lblDADLoadeddate.Name = "lblDADLoadeddate";
            this.lblDADLoadeddate.Text = "Date loaded";
            this.lblDADLoadeddate.Weight = 0.31375;
            // 
            // lblDAAmount
            // 
            this.lblDAAmount.CanGrow = false;
            this.lblDAAmount.Name = "lblDAAmount";
            this.lblDAAmount.Text = "Amount";
            this.lblDAAmount.Weight = 0.33999999999999997;
            // 
            // lblDAStatus
            // 
            this.lblDAStatus.CanGrow = false;
            this.lblDAStatus.Name = "lblDAStatus";
            this.lblDAStatus.Text = "Status";
            this.lblDAStatus.Weight = 0.41375;
            // 
            // lblDAComments
            // 
            this.lblDAComments.CanGrow = false;
            this.lblDAComments.Name = "lblDAComments";
            this.lblDAComments.Text = "Comment";
            this.lblDAComments.Weight = 0.74125;
            // 
            // tblDefaultAlertH
            // 
            this.tblDefaultAlertH.ForeColor = System.Drawing.Color.Black;
            this.tblDefaultAlertH.LocationFloat = new DevExpress.Utils.PointFloat(8F, 8F);
            this.tblDefaultAlertH.Name = "tblDefaultAlertH";
            this.tblDefaultAlertH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow14});
            this.tblDefaultAlertH.SizeF = new System.Drawing.SizeF(792F, 25F);
            this.tblDefaultAlertH.StylePriority.UseForeColor = false;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDefaultAlertH});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 1;
            // 
            // lblDefaultAlertH
            // 
            this.lblDefaultAlertH.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblDefaultAlertH.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.lblDefaultAlertH.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDefaultAlertH.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.lblDefaultAlertH.ForeColor = System.Drawing.Color.Gray;
            this.lblDefaultAlertH.Name = "lblDefaultAlertH";
            this.lblDefaultAlertH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblDefaultAlertH.StylePriority.UseBackColor = false;
            this.lblDefaultAlertH.StylePriority.UseBorderColor = false;
            this.lblDefaultAlertH.StylePriority.UseBorders = false;
            this.lblDefaultAlertH.StylePriority.UseFont = false;
            this.lblDefaultAlertH.StylePriority.UseForeColor = false;
            this.lblDefaultAlertH.StylePriority.UsePadding = false;
            this.lblDefaultAlertH.StylePriority.UseTextAlignment = false;
            this.lblDefaultAlertH.Text = "Default Alerts";
            this.lblDefaultAlertH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblDefaultAlertH.Weight = 3;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail7});
            this.DetailReport.DataMember = "SubscriberInputDetails";
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail7
            // 
            this.Detail7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4,
            this.xrTable3});
            this.Detail7.HeightF = 142F;
            this.Detail7.Name = "Detail7";
            // 
            // xrTable4
            // 
            this.xrTable4.BorderColor = System.Drawing.Color.White;
            this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(8F, 42F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow17,
            this.xrTableRow18,
            this.xrTableRow19,
            this.xrTableRow20,
            this.xrTableRow21});
            this.xrTable4.SizeF = new System.Drawing.SizeF(792F, 100F);
            this.xrTable4.StylePriority.UseBorderColor = false;
            this.xrTable4.StylePriority.UseBorders = false;
            this.xrTable4.StylePriority.UseFont = false;
            this.xrTable4.StylePriority.UsePadding = false;
            this.xrTable4.StylePriority.UseTextAlignment = false;
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblEnquiryDate,
            this.lblEnquiryDateValue});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 0.8;
            // 
            // lblEnquiryDate
            // 
            this.lblEnquiryDate.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblEnquiryDate.BorderColor = System.Drawing.Color.White;
            this.lblEnquiryDate.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblEnquiryDate.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnquiryDate.ForeColor = System.Drawing.Color.Gray;
            this.lblEnquiryDate.Name = "lblEnquiryDate";
            this.lblEnquiryDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblEnquiryDate.StylePriority.UseBackColor = false;
            this.lblEnquiryDate.StylePriority.UseBorderColor = false;
            this.lblEnquiryDate.StylePriority.UseBorders = false;
            this.lblEnquiryDate.StylePriority.UseFont = false;
            this.lblEnquiryDate.StylePriority.UseForeColor = false;
            this.lblEnquiryDate.StylePriority.UsePadding = false;
            this.lblEnquiryDate.Text = "Enquiry Date";
            this.lblEnquiryDate.Weight = 0.59755134281200639;
            // 
            // lblEnquiryDateValue
            // 
            this.lblEnquiryDateValue.BackColor = System.Drawing.Color.White;
            this.lblEnquiryDateValue.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblEnquiryDateValue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblEnquiryDateValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.EnquiryDate", "{0:ddd, dd MMM yyyy HH\':\'mm\':\'ss \'GMT\'}")});
            this.lblEnquiryDateValue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblEnquiryDateValue.ForeColor = System.Drawing.Color.DimGray;
            this.lblEnquiryDateValue.Name = "lblEnquiryDateValue";
            this.lblEnquiryDateValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblEnquiryDateValue.StylePriority.UseBackColor = false;
            this.lblEnquiryDateValue.StylePriority.UseBorderColor = false;
            this.lblEnquiryDateValue.StylePriority.UseBorders = false;
            this.lblEnquiryDateValue.StylePriority.UseFont = false;
            this.lblEnquiryDateValue.StylePriority.UseForeColor = false;
            this.lblEnquiryDateValue.StylePriority.UsePadding = false;
            this.lblEnquiryDateValue.Text = "lblEnquiryDateValue";
            this.lblEnquiryDateValue.Weight = 2.3910850208243573;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell11,
            this.xrTableCell14});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 0.79999999999999993;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell11.BorderColor = System.Drawing.Color.White;
            this.xrTableCell11.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell11.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell11.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell11.StylePriority.UseBackColor = false;
            this.xrTableCell11.StylePriority.UseBorderColor = false;
            this.xrTableCell11.StylePriority.UseBorders = false;
            this.xrTableCell11.StylePriority.UseFont = false;
            this.xrTableCell11.StylePriority.UseForeColor = false;
            this.xrTableCell11.StylePriority.UsePadding = false;
            this.xrTableCell11.Text = "Enquiry Type";
            this.xrTableCell11.Weight = 0.59755134281200639;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.BackColor = System.Drawing.Color.White;
            this.xrTableCell14.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell14.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell14.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.EnquiryType")});
            this.xrTableCell14.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell14.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell14.StylePriority.UseBackColor = false;
            this.xrTableCell14.StylePriority.UseBorderColor = false;
            this.xrTableCell14.StylePriority.UseBorders = false;
            this.xrTableCell14.StylePriority.UseFont = false;
            this.xrTableCell14.StylePriority.UseForeColor = false;
            this.xrTableCell14.StylePriority.UsePadding = false;
            this.xrTableCell14.Text = "xrTableCell14";
            this.xrTableCell14.Weight = 2.3910850208243573;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell15,
            this.xrTableCell18});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 0.79999999999999993;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell15.BorderColor = System.Drawing.Color.White;
            this.xrTableCell15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell15.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell15.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell15.StylePriority.UseBackColor = false;
            this.xrTableCell15.StylePriority.UseBorderColor = false;
            this.xrTableCell15.StylePriority.UseBorders = false;
            this.xrTableCell15.StylePriority.UseFont = false;
            this.xrTableCell15.StylePriority.UseForeColor = false;
            this.xrTableCell15.StylePriority.UsePadding = false;
            this.xrTableCell15.Text = "Subscriber Name";
            this.xrTableCell15.Weight = 0.59755134281200639;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.BackColor = System.Drawing.Color.White;
            this.xrTableCell18.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell18.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell18.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.SubscriberName")});
            this.xrTableCell18.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell18.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell18.StylePriority.UseBackColor = false;
            this.xrTableCell18.StylePriority.UseBorderColor = false;
            this.xrTableCell18.StylePriority.UseBorders = false;
            this.xrTableCell18.StylePriority.UseFont = false;
            this.xrTableCell18.StylePriority.UseForeColor = false;
            this.xrTableCell18.StylePriority.UsePadding = false;
            this.xrTableCell18.Text = "xrTableCell18";
            this.xrTableCell18.Weight = 2.3910850208243573;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell19,
            this.xrTableCell22});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 0.8;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell19.BorderColor = System.Drawing.Color.White;
            this.xrTableCell19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell19.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell19.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell19.StylePriority.UseBackColor = false;
            this.xrTableCell19.StylePriority.UseBorderColor = false;
            this.xrTableCell19.StylePriority.UseBorders = false;
            this.xrTableCell19.StylePriority.UseFont = false;
            this.xrTableCell19.StylePriority.UseForeColor = false;
            this.xrTableCell19.StylePriority.UsePadding = false;
            this.xrTableCell19.Text = "Subscriber User Name";
            this.xrTableCell19.Weight = 0.59755134281200639;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.BackColor = System.Drawing.Color.White;
            this.xrTableCell22.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell22.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell22.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.SubscriberUserName")});
            this.xrTableCell22.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell22.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell22.StylePriority.UseBackColor = false;
            this.xrTableCell22.StylePriority.UseBorderColor = false;
            this.xrTableCell22.StylePriority.UseBorders = false;
            this.xrTableCell22.StylePriority.UseFont = false;
            this.xrTableCell22.StylePriority.UseForeColor = false;
            this.xrTableCell22.StylePriority.UsePadding = false;
            this.xrTableCell22.Text = "xrTableCell22";
            this.xrTableCell22.Weight = 2.3910850208243573;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell23,
            this.xrTableCell26});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 0.8;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell23.BorderColor = System.Drawing.Color.White;
            this.xrTableCell23.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell23.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell23.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell23.StylePriority.UseBackColor = false;
            this.xrTableCell23.StylePriority.UseBorderColor = false;
            this.xrTableCell23.StylePriority.UseBorders = false;
            this.xrTableCell23.StylePriority.UseFont = false;
            this.xrTableCell23.StylePriority.UseForeColor = false;
            this.xrTableCell23.StylePriority.UsePadding = false;
            this.xrTableCell23.Text = "Enquiry Input";
            this.xrTableCell23.Weight = 0.59755134281200639;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.BackColor = System.Drawing.Color.White;
            this.xrTableCell26.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell26.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell26.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.EnquiryInput")});
            this.xrTableCell26.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell26.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell26.StylePriority.UseBackColor = false;
            this.xrTableCell26.StylePriority.UseBorderColor = false;
            this.xrTableCell26.StylePriority.UseBorders = false;
            this.xrTableCell26.StylePriority.UseFont = false;
            this.xrTableCell26.StylePriority.UseForeColor = false;
            this.xrTableCell26.StylePriority.UsePadding = false;
            this.xrTableCell26.Text = "xrTableCell26";
            this.xrTableCell26.Weight = 2.3910850208243573;
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(8F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow11});
            this.xrTable3.SizeF = new System.Drawing.SizeF(792F, 33F);
            this.xrTable3.StylePriority.UseBorders = false;
            this.xrTable3.StylePriority.UseFont = false;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 1;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.xrTableCell2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell2.BorderWidth = 2;
            this.xrTableCell2.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell2.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell2.StylePriority.UseBorderColor = false;
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.StylePriority.UseBorderWidth = false;
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.StylePriority.UseForeColor = false;
            this.xrTableCell2.StylePriority.UsePadding = false;
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.Text = "Enquiry Input Details";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell2.Weight = 3;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.HeightF = 20F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.HeightF = 22F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // EnquiryHistory
            // 
            this.EnquiryHistory.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailEnquiryHistory,
            this.GroupHeader1});
            this.EnquiryHistory.DataMember = "ConsumerEnquiryHistory";
            this.EnquiryHistory.Level = 15;
            this.EnquiryHistory.Name = "EnquiryHistory";
            // 
            // DetailEnquiryHistory
            // 
            this.DetailEnquiryHistory.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblEnquiryD});
            this.DetailEnquiryHistory.HeightF = 22F;
            this.DetailEnquiryHistory.Name = "DetailEnquiryHistory";
            // 
            // tblEnquiryD
            // 
            this.tblEnquiryD.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tblEnquiryD.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.tblEnquiryD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblEnquiryD.LocationFloat = new DevExpress.Utils.PointFloat(7F, 0F);
            this.tblEnquiryD.Name = "tblEnquiryD";
            this.tblEnquiryD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblEnquiryD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow101});
            this.tblEnquiryD.SizeF = new System.Drawing.SizeF(792F, 20F);
            this.tblEnquiryD.StylePriority.UseBackColor = false;
            this.tblEnquiryD.StylePriority.UseBorderColor = false;
            this.tblEnquiryD.StylePriority.UseFont = false;
            this.tblEnquiryD.StylePriority.UsePadding = false;
            // 
            // xrTableRow101
            // 
            this.xrTableRow101.BackColor = System.Drawing.Color.White;
            this.xrTableRow101.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableRow101.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow101.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblEnqDateD,
            this.tblsubscribernameD,
            this.lblbusTypeD});
            this.xrTableRow101.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableRow101.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableRow101.Name = "xrTableRow101";
            this.xrTableRow101.StylePriority.UseBackColor = false;
            this.xrTableRow101.StylePriority.UseBorderColor = false;
            this.xrTableRow101.StylePriority.UseBorders = false;
            this.xrTableRow101.StylePriority.UseFont = false;
            this.xrTableRow101.StylePriority.UseForeColor = false;
            this.xrTableRow101.StylePriority.UseTextAlignment = false;
            this.xrTableRow101.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow101.Weight = 0.8;
            // 
            // lblEnqDateD
            // 
            this.lblEnqDateD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerEnquiryHistory.EnquiryDate")});
            this.lblEnqDateD.Name = "lblEnqDateD";
            this.lblEnqDateD.Text = "lblEnqDateD";
            this.lblEnqDateD.Weight = 0.40814393939393939;
            // 
            // tblsubscribernameD
            // 
            this.tblsubscribernameD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerEnquiryHistory.SubscriberName")});
            this.tblsubscribernameD.Name = "tblsubscribernameD";
            this.tblsubscribernameD.Text = "tblsubscribernameD";
            this.tblsubscribernameD.Weight = 1.1714015151515151;
            // 
            // lblbusTypeD
            // 
            this.lblbusTypeD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerEnquiryHistory.SubscriberBusinessTypeDesc")});
            this.lblbusTypeD.Name = "lblbusTypeD";
            this.lblbusTypeD.Text = "lblbusTypeD";
            this.lblbusTypeD.Weight = 1.4204545454545454;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblEnquiryC,
            this.tblEnquiryH});
            this.GroupHeader1.HeightF = 65F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // tblEnquiryC
            // 
            this.tblEnquiryC.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tblEnquiryC.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tblEnquiryC.BookmarkParent = this.lbDefCode;
            this.tblEnquiryC.BorderColor = System.Drawing.Color.White;
            this.tblEnquiryC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblEnquiryC.LocationFloat = new DevExpress.Utils.PointFloat(7F, 45F);
            this.tblEnquiryC.Name = "tblEnquiryC";
            this.tblEnquiryC.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow100});
            this.tblEnquiryC.SizeF = new System.Drawing.SizeF(792F, 20F);
            this.tblEnquiryC.StylePriority.UseBackColor = false;
            this.tblEnquiryC.StylePriority.UseBorderColor = false;
            this.tblEnquiryC.StylePriority.UseFont = false;
            // 
            // xrTableRow100
            // 
            this.xrTableRow100.BorderColor = System.Drawing.Color.White;
            this.xrTableRow100.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow100.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tblEnqDate,
            this.tblsubscribername,
            this.lblbusType});
            this.xrTableRow100.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableRow100.ForeColor = System.Drawing.Color.Gray;
            this.xrTableRow100.Name = "xrTableRow100";
            this.xrTableRow100.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableRow100.StylePriority.UseBackColor = false;
            this.xrTableRow100.StylePriority.UseBorderColor = false;
            this.xrTableRow100.StylePriority.UseBorders = false;
            this.xrTableRow100.StylePriority.UseFont = false;
            this.xrTableRow100.StylePriority.UseForeColor = false;
            this.xrTableRow100.StylePriority.UsePadding = false;
            this.xrTableRow100.StylePriority.UseTextAlignment = false;
            this.xrTableRow100.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow100.Weight = 0.75757575757575757;
            // 
            // tblEnqDate
            // 
            this.tblEnqDate.CanGrow = false;
            this.tblEnqDate.Name = "tblEnqDate";
            this.tblEnqDate.Text = "Enquiry Date";
            this.tblEnqDate.Weight = 0.40814393939393939;
            // 
            // tblsubscribername
            // 
            this.tblsubscribername.CanGrow = false;
            this.tblsubscribername.Name = "tblsubscribername";
            this.tblsubscribername.Text = "Name Of Credit Grantor";
            this.tblsubscribername.Weight = 1.1714015151515151;
            // 
            // lblbusType
            // 
            this.lblbusType.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblbusType.CanGrow = false;
            this.lblbusType.Name = "lblbusType";
            this.lblbusType.StylePriority.UseBorders = false;
            this.lblbusType.Text = "Type/Category of Credit Grantor";
            this.lblbusType.Weight = 1.4204545454545454;
            // 
            // tblEnquiryH
            // 
            this.tblEnquiryH.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tblEnquiryH.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblEnquiryH.BorderWidth = 2;
            this.tblEnquiryH.ForeColor = System.Drawing.Color.Gray;
            this.tblEnquiryH.LocationFloat = new DevExpress.Utils.PointFloat(7F, 7F);
            this.tblEnquiryH.Name = "tblEnquiryH";
            this.tblEnquiryH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow99});
            this.tblEnquiryH.SizeF = new System.Drawing.SizeF(792F, 33F);
            this.tblEnquiryH.StylePriority.UseBackColor = false;
            this.tblEnquiryH.StylePriority.UseBorders = false;
            this.tblEnquiryH.StylePriority.UseBorderWidth = false;
            this.tblEnquiryH.StylePriority.UseForeColor = false;
            // 
            // xrTableRow99
            // 
            this.xrTableRow99.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblEnquiryH});
            this.xrTableRow99.Name = "xrTableRow99";
            this.xrTableRow99.Weight = 1;
            // 
            // lblEnquiryH
            // 
            this.lblEnquiryH.BackColor = System.Drawing.Color.White;
            this.lblEnquiryH.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(60)))), ((int)(((byte)(76)))));
            this.lblEnquiryH.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblEnquiryH.ForeColor = System.Drawing.Color.Gray;
            this.lblEnquiryH.Name = "lblEnquiryH";
            this.lblEnquiryH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblEnquiryH.StylePriority.UseBackColor = false;
            this.lblEnquiryH.StylePriority.UseBorderColor = false;
            this.lblEnquiryH.StylePriority.UseFont = false;
            this.lblEnquiryH.StylePriority.UseForeColor = false;
            this.lblEnquiryH.StylePriority.UsePadding = false;
            this.lblEnquiryH.StylePriority.UseTextAlignment = false;
            this.lblEnquiryH.Text = "Trace Enquiry History";
            this.lblEnquiryH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblEnquiryH.Weight = 3;
            // 
            // ABSATraceReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.PageFooter,
            this.ReportHeader,
            this.ConsumerDetail,
            this.Scoring,
            this.DebtSummary,
            this.AccountGoodBadSummary,
            this.NameHistory,
            this.AddressHistory,
            this.TelephoneHistory,
            this.EmploymentHistory,
            this.AccountStatus,
            this.MonthlyPaymentHeader,
            this.ConsumerDefinition,
            this.AdverseInfo,
            this.Judgements,
            this.AdminOrders,
            this.Sequestration,
            this.DebtReviewStatus,
            this.propertyInfo,
            this.DirectorshipLink,
            this.MonthlyPayment,
            this.DefaultAlert,
            this.DetailReport,
            this.topMarginBand1,
            this.bottomMarginBand1,
            this.EnquiryHistory});
            this.Margins = new System.Drawing.Printing.Margins(20, 20, 20, 22);
            this.RequestParameters = false;
            this.Version = "9.3";
            this.XmlDataPath = "C:\\Documents and Settings\\skothamasu\\Desktop\\XMl1.XML";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonalDetailsSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblPersonalDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblscrorevalues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblScore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDebtSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDebtHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAccStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblCrAccStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblNamehistD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblNameHisC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblNameHistH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblConsInfoH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAddHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAddHC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAddHistoryH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblContactNoD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblContactNoC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblContactNoH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEmploymentD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEmploymentC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEmploymentH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAccStatusD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAccStatusC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAccStatusH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblMonthlyPaymentH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblConsumerDefinitionD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblConsumerDefinitionC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAdverseData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAdverseColumns)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAdverseH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDomainH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDJudg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblCJud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblJudgmentsH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDAO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblCAO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAOH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblSEQD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblSEQC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblSEQH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDbtReviewD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbldbtReviewH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblPropInterests)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblPropInterestsH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDirInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDirectorlinksH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblMonthlyPaymentD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDefaultAlertD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDefaultAlertC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDefaultAlertH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEnquiryD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEnquiryC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEnquiryH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell lblHeaderText;
        private DevExpress.XtraReports.UI.DetailReportBand ConsumerDetail;
        private DevExpress.XtraReports.UI.DetailBand DetailConsumerDetail;
        private DevExpress.XtraReports.UI.DetailReportBand Scoring;
        private DevExpress.XtraReports.UI.DetailBand DetailScoring;
        private DevExpress.XtraReports.UI.XRTable tblscrorevalues;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell lblClassification;
        private DevExpress.XtraReports.UI.XRTableCell lblDescription;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader3;
        private DevExpress.XtraReports.UI.DetailReportBand DebtSummary;
        private DevExpress.XtraReports.UI.DetailBand DetailDebtSummary;
        private DevExpress.XtraReports.UI.XRTable tblDebtSummary;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell lblActiveAcc;
        private DevExpress.XtraReports.UI.XRTableCell lblActiveAccValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell lblDelinquentAcc;
        private DevExpress.XtraReports.UI.XRTableCell lblDelinquentAccValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell lblMonthlyinst;
        private DevExpress.XtraReports.UI.XRTableCell lblMonthlyinstValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell lblOutstandingDebt;
        private DevExpress.XtraReports.UI.XRTableCell lblOutstandingDebtValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell lblArrearamt;
        private DevExpress.XtraReports.UI.XRTableCell lblArrearamtvalue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell lblJdgDate;
        private DevExpress.XtraReports.UI.XRTableCell lblJdgDateValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell lblJudAmt;
        private DevExpress.XtraReports.UI.XRTableCell lblJudAmtValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell lblDelinquentrating;
        private DevExpress.XtraReports.UI.XRTableCell lblDelinquentratingValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell lblDbtReview;
        private DevExpress.XtraReports.UI.XRTableCell lblDbtReviewValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell lblAO;
        private DevExpress.XtraReports.UI.XRTableCell lblAOValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell lblAdverseAmt;
        private DevExpress.XtraReports.UI.XRTableCell lblAdverseAmtValue;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader4;
        private DevExpress.XtraReports.UI.XRTable tblDebtHeader;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell lblDebtHeader;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell lblAccGood;
        private DevExpress.XtraReports.UI.XRTableCell lblAccGoodvalue;
        private DevExpress.XtraReports.UI.DetailReportBand AccountGoodBadSummary;
        private DevExpress.XtraReports.UI.DetailBand DetailAccountGoodBadSummary;
        private DevExpress.XtraReports.UI.XRTable tblAccStatus;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell lblRetail;
        private DevExpress.XtraReports.UI.XRTableCell lblRetailG;
        private DevExpress.XtraReports.UI.XRTableCell lblRetailB;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader5;
        private DevExpress.XtraReports.UI.XRTable tblCrAccStatus;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell lblCrAccStatus;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell lblAccdesc;
        private DevExpress.XtraReports.UI.XRTableCell lblGood;
        private DevExpress.XtraReports.UI.XRTableCell lblBad;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow47;
        private DevExpress.XtraReports.UI.XRTableCell lblCc;
        private DevExpress.XtraReports.UI.XRTableCell lblCreditCradG;
        private DevExpress.XtraReports.UI.XRTableCell lblCreditCardB;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow46;
        private DevExpress.XtraReports.UI.XRTableCell lblFurnAcc;
        private DevExpress.XtraReports.UI.XRTableCell lblFurnAccG;
        private DevExpress.XtraReports.UI.XRTableCell lblFurnAccB;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow45;
        private DevExpress.XtraReports.UI.XRTableCell lblInsurance;
        private DevExpress.XtraReports.UI.XRTableCell lblInsuranceG;
        private DevExpress.XtraReports.UI.XRTableCell lblInsuranceB;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow44;
        private DevExpress.XtraReports.UI.XRTableCell lblFinance;
        private DevExpress.XtraReports.UI.XRTableCell lblFinanceG;
        private DevExpress.XtraReports.UI.XRTableCell lblFinanceB;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow43;
        private DevExpress.XtraReports.UI.XRTableCell lblBankAcc;
        private DevExpress.XtraReports.UI.XRTableCell lblBankAccG;
        private DevExpress.XtraReports.UI.XRTableCell lblBankAccB;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow42;
        private DevExpress.XtraReports.UI.XRTableCell lblTelecomms;
        private DevExpress.XtraReports.UI.XRTableCell lblTelecommsG;
        private DevExpress.XtraReports.UI.XRTableCell lblTelecommsB;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow41;
        private DevExpress.XtraReports.UI.XRTableCell lblHomeLoan;
        private DevExpress.XtraReports.UI.XRTableCell lblHomeLoanG;
        private DevExpress.XtraReports.UI.XRTableCell lblHomeLoanB;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
        private DevExpress.XtraReports.UI.XRTableCell lblVehicleFin;
        private DevExpress.XtraReports.UI.XRTableCell lblVehicleFinG;
        private DevExpress.XtraReports.UI.XRTableCell lblVehicleFinB;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell lblOtherAcc;
        private DevExpress.XtraReports.UI.XRTableCell lblOtherAccG;
        private DevExpress.XtraReports.UI.XRTableCell lblOtherAccB;
        private DevExpress.XtraReports.UI.DetailReportBand NameHistory;
        private DevExpress.XtraReports.UI.DetailBand DetailNameHistory;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader10;
        private DevExpress.XtraReports.UI.DetailReportBand AddressHistory;
        private DevExpress.XtraReports.UI.DetailBand DetailAddressHistory;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader11;
        private DevExpress.XtraReports.UI.DetailReportBand TelephoneHistory;
        private DevExpress.XtraReports.UI.DetailBand DetailTelephoneHistory;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader12;
        private DevExpress.XtraReports.UI.DetailReportBand EmploymentHistory;
        private DevExpress.XtraReports.UI.DetailBand DetailEmploymentHistory;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader13;
        private DevExpress.XtraReports.UI.DetailReportBand AccountStatus;
        private DevExpress.XtraReports.UI.DetailBand DetailAccountStatus;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader14;
        private DevExpress.XtraReports.UI.XRTable tblAccStatusH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow75;
        private DevExpress.XtraReports.UI.XRTableCell lblAccStatusH;
        private DevExpress.XtraReports.UI.XRTable tblAccStatusC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow77;
        private DevExpress.XtraReports.UI.XRTableCell lblAccOpenDate;
        private DevExpress.XtraReports.UI.XRTableCell lblCompany;
        private DevExpress.XtraReports.UI.XRTableCell lblCreditLimit;
        private DevExpress.XtraReports.UI.XRTableCell lblCurrBalance;
        private DevExpress.XtraReports.UI.XRTableCell lblInstallement;
        private DevExpress.XtraReports.UI.XRTableCell lblArrearsAmt;
        private DevExpress.XtraReports.UI.XRTableCell lblAccType;
        private DevExpress.XtraReports.UI.XRTable tblAccStatusD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow76;
        private DevExpress.XtraReports.UI.XRTableCell lblAccOpenDateD;
        private DevExpress.XtraReports.UI.XRTableCell lblCompanyD;
        private DevExpress.XtraReports.UI.XRTableCell lblCreditLimitD;
        private DevExpress.XtraReports.UI.XRTableCell lblCurrBalanceD;
        private DevExpress.XtraReports.UI.XRTableCell lblInstallementD;
        private DevExpress.XtraReports.UI.XRTableCell lblArrearsAmtD;
        private DevExpress.XtraReports.UI.XRTableCell lblAccTypeD;
        private DevExpress.XtraReports.UI.DetailReportBand MonthlyPaymentHeader;
        private DevExpress.XtraReports.UI.DetailBand DetailMonthlyPaymentHeader;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader15;
        private DevExpress.XtraReports.UI.XRTable tblMonthlyPaymentH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow78;
        private DevExpress.XtraReports.UI.XRTableCell lblcompanyMP;
        private DevExpress.XtraReports.UI.XRTableCell lblM01;
        private DevExpress.XtraReports.UI.XRTableCell lblM02;
        private DevExpress.XtraReports.UI.XRTableCell lblM03;
        private DevExpress.XtraReports.UI.XRTableCell lblM04;
        private DevExpress.XtraReports.UI.XRTableCell lblM05;
        private DevExpress.XtraReports.UI.XRTableCell lblM06;
        private DevExpress.XtraReports.UI.XRTableCell lblDelinquentStatusC;
        private DevExpress.XtraReports.UI.XRPageInfo PageinfoMonthlyPaymentH;
        private DevExpress.XtraReports.UI.DetailReportBand ConsumerDefinition;
        private DevExpress.XtraReports.UI.DetailBand DetailConsumerDefinition;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader16;
        private DevExpress.XtraReports.UI.XRTable tblConsumerDefinitionC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow80;
        private DevExpress.XtraReports.UI.XRTableCell lblDefDesc;
        private DevExpress.XtraReports.UI.XRTableCell lbDefCode;
        private DevExpress.XtraReports.UI.XRTable tblConsumerDefinitionD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow81;
        private DevExpress.XtraReports.UI.XRTableCell lblDefDescD;
        private DevExpress.XtraReports.UI.XRTableCell lbDefCodeD;
        private DevExpress.XtraReports.UI.DetailReportBand AdverseInfo;
        private DevExpress.XtraReports.UI.DetailBand DetailAdverseInfo;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader17;
        private DevExpress.XtraReports.UI.DetailReportBand Judgements;
        private DevExpress.XtraReports.UI.DetailBand DetailJudgements;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader18;
        private DevExpress.XtraReports.UI.DetailReportBand AdminOrders;
        private DevExpress.XtraReports.UI.DetailBand DetailAdminOrders;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader19;
        private DevExpress.XtraReports.UI.DetailReportBand Sequestration;
        private DevExpress.XtraReports.UI.DetailBand DetailSequestration;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader20;
        private DevExpress.XtraReports.UI.DetailReportBand DebtReviewStatus;
        private DevExpress.XtraReports.UI.DetailBand DetailDebtReviewStatus;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader21;
        private DevExpress.XtraReports.UI.DetailReportBand propertyInfo;
        private DevExpress.XtraReports.UI.DetailBand DetailpropertyInfo;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader23;
        private DevExpress.XtraReports.UI.DetailReportBand DirectorshipLink;
        private DevExpress.XtraReports.UI.DetailBand DetailDirectorshipLink;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader27;
        private DevExpress.XtraReports.UI.XRTable tblNamehistD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow65;
        private DevExpress.XtraReports.UI.XRTableCell lblDUpdatedDate;
        private DevExpress.XtraReports.UI.XRTableCell lblHDSurname;
        private DevExpress.XtraReports.UI.XRTableCell lblHDFirstname;
        private DevExpress.XtraReports.UI.XRTableCell lblHDSecodnName;
        private DevExpress.XtraReports.UI.XRTableCell lblHDInitial;
        private DevExpress.XtraReports.UI.XRTableCell lblHDTitle;
        private DevExpress.XtraReports.UI.XRTableCell lblHDIDno;
        private DevExpress.XtraReports.UI.XRTableCell lblHDPPNo;
        private DevExpress.XtraReports.UI.XRTableCell lblHDBirthDate;
        private DevExpress.XtraReports.UI.XRTable tblNameHisC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow64;
        private DevExpress.XtraReports.UI.XRTableCell lblUpdatedDate;
        private DevExpress.XtraReports.UI.XRTableCell lblHSurname;
        private DevExpress.XtraReports.UI.XRTableCell lblHFirstname;
        private DevExpress.XtraReports.UI.XRTableCell lblHSecodnName;
        private DevExpress.XtraReports.UI.XRTableCell lblHInitial;
        private DevExpress.XtraReports.UI.XRTableCell lblHTitle;
        private DevExpress.XtraReports.UI.XRTableCell lblHIDno;
        private DevExpress.XtraReports.UI.XRTableCell lblHPPNo;
        private DevExpress.XtraReports.UI.XRTableCell lblHBirthDate;
        private DevExpress.XtraReports.UI.XRTable tblNameHistH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow63;
        private DevExpress.XtraReports.UI.XRTableCell lblNameHistH;
        private DevExpress.XtraReports.UI.XRTable tblConsInfoH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTable tblAddHC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow67;
        private DevExpress.XtraReports.UI.XRTableCell lblAddUpdatedate;
        private DevExpress.XtraReports.UI.XRTableCell lblAddtype;
        private DevExpress.XtraReports.UI.XRTableCell lblAddline1;
        private DevExpress.XtraReports.UI.XRTableCell lblAddLine2;
        private DevExpress.XtraReports.UI.XRTableCell lblAddLine3;
        private DevExpress.XtraReports.UI.XRTableCell lblAddLine4;
        private DevExpress.XtraReports.UI.XRTableCell lblPostalcode;
        private DevExpress.XtraReports.UI.XRTable tblAddHistoryH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow66;
        private DevExpress.XtraReports.UI.XRTableCell lblAddHistoryH;
        private DevExpress.XtraReports.UI.XRTable tblAddHD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow68;
        private DevExpress.XtraReports.UI.XRTableCell lblDAddUpdatedate;
        private DevExpress.XtraReports.UI.XRTableCell lblDAddtype;
        private DevExpress.XtraReports.UI.XRTableCell lblDAddline1;
        private DevExpress.XtraReports.UI.XRTableCell lblDAddLine2;
        private DevExpress.XtraReports.UI.XRTableCell lblDAddLine3;
        private DevExpress.XtraReports.UI.XRTableCell lblDAddLine4;
        private DevExpress.XtraReports.UI.XRTableCell lblDPostalcode;
        private DevExpress.XtraReports.UI.XRTable tblContactNoC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow70;
        private DevExpress.XtraReports.UI.XRTableCell lblNoUpdatedate;
        private DevExpress.XtraReports.UI.XRTableCell lblTelnoType;
        private DevExpress.XtraReports.UI.XRTableCell lblTelNo;
        private DevExpress.XtraReports.UI.XRTableCell lblEmail;
        private DevExpress.XtraReports.UI.XRTable tblContactNoH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow69;
        private DevExpress.XtraReports.UI.XRTableCell lblContactNoH;
        private DevExpress.XtraReports.UI.XRTable tblContactNoD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow71;
        private DevExpress.XtraReports.UI.XRTableCell lblDNoUpdatedate;
        private DevExpress.XtraReports.UI.XRTableCell lblDTelnoType;
        private DevExpress.XtraReports.UI.XRTableCell lblDTelNo;
        private DevExpress.XtraReports.UI.XRTableCell lblDEmail;
        private DevExpress.XtraReports.UI.XRTable tblEmploymentC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow73;
        private DevExpress.XtraReports.UI.XRTableCell lblEmpUpdatedate;
        private DevExpress.XtraReports.UI.XRTableCell lblEmployer;
        private DevExpress.XtraReports.UI.XRTableCell lblDesignation;
        private DevExpress.XtraReports.UI.XRTable tblEmploymentH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow72;
        private DevExpress.XtraReports.UI.XRTableCell lblEmploymentH;
        private DevExpress.XtraReports.UI.XRTable tblEmploymentD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow74;
        private DevExpress.XtraReports.UI.XRTableCell lblDEmpUpdatedate;
        private DevExpress.XtraReports.UI.XRTableCell lblDEmployer;
        private DevExpress.XtraReports.UI.XRTableCell lblDDesignation;
        private DevExpress.XtraReports.UI.XRTable tblAdverseColumns;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow83;
        private DevExpress.XtraReports.UI.XRTableCell lblSubscriber;
        private DevExpress.XtraReports.UI.XRTableCell lblAccountNo;
        private DevExpress.XtraReports.UI.XRTableCell lblActionDate;
        private DevExpress.XtraReports.UI.XRTableCell lblAmount;
        private DevExpress.XtraReports.UI.XRTableCell lblAccStatus;
        private DevExpress.XtraReports.UI.XRTableCell lblAdverseComment;
        private DevExpress.XtraReports.UI.XRTable tblAdverseH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow84;
        private DevExpress.XtraReports.UI.XRTableCell lblAdverseH;
        private DevExpress.XtraReports.UI.XRTable tblDomainH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow82;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
        private DevExpress.XtraReports.UI.XRTable tblAdverseData;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow85;
        private DevExpress.XtraReports.UI.XRTableCell lblDSubscriber;
        private DevExpress.XtraReports.UI.XRTableCell lblDAccNo;
        private DevExpress.XtraReports.UI.XRTableCell lblDActionDate;
        private DevExpress.XtraReports.UI.XRTableCell lblDAmount;
        private DevExpress.XtraReports.UI.XRTableCell lblDAccStatus;
        private DevExpress.XtraReports.UI.XRTableCell lblDAdvComment;
        private DevExpress.XtraReports.UI.XRTable tblCJud;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow87;
        private DevExpress.XtraReports.UI.XRTableCell lblJCaseNo;
        private DevExpress.XtraReports.UI.XRTableCell lblJIssueDate;
        private DevExpress.XtraReports.UI.XRTableCell lblJType;
        private DevExpress.XtraReports.UI.XRTableCell lblJAmount;
        private DevExpress.XtraReports.UI.XRTableCell lblJPlaintiff;
        private DevExpress.XtraReports.UI.XRTableCell lblJCourt;
        private DevExpress.XtraReports.UI.XRTableCell lblJAttorney;
        private DevExpress.XtraReports.UI.XRTableCell lblJPhoneNo;
        private DevExpress.XtraReports.UI.XRTableCell lblJComment;
        private DevExpress.XtraReports.UI.XRTable tblJudgmentsH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow86;
        private DevExpress.XtraReports.UI.XRTableCell lblJudgmentsH;
        private DevExpress.XtraReports.UI.XRTable tblDJudg;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow88;
        private DevExpress.XtraReports.UI.XRTableCell lblJDCaseno;
        private DevExpress.XtraReports.UI.XRTableCell lblJDIssueDate;
        private DevExpress.XtraReports.UI.XRTableCell lblJDType;
        private DevExpress.XtraReports.UI.XRTableCell lblJDAmount;
        private DevExpress.XtraReports.UI.XRTableCell lblJDPlaintiff;
        private DevExpress.XtraReports.UI.XRTableCell lblJDCourt;
        private DevExpress.XtraReports.UI.XRTableCell lblJDAttorney;
        private DevExpress.XtraReports.UI.XRTableCell lblJDPhoneNo;
        private DevExpress.XtraReports.UI.XRTableCell lblJDComment;
        private DevExpress.XtraReports.UI.XRTable tblCAO;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow90;
        private DevExpress.XtraReports.UI.XRTableCell lblAOCaseno;
        private DevExpress.XtraReports.UI.XRTableCell lblAOIssuedate;
        private DevExpress.XtraReports.UI.XRTableCell lblAODateLoaded;
        private DevExpress.XtraReports.UI.XRTableCell lblAOType;
        private DevExpress.XtraReports.UI.XRTableCell lblAOAmount;
        private DevExpress.XtraReports.UI.XRTableCell lblAOPlaintiff;
        private DevExpress.XtraReports.UI.XRTableCell lblAOCourt;
        private DevExpress.XtraReports.UI.XRTableCell lblAOAttorney;
        private DevExpress.XtraReports.UI.XRTableCell lblAOPhoneNo;
        private DevExpress.XtraReports.UI.XRTable tblAOH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow89;
        private DevExpress.XtraReports.UI.XRTableCell lblAOH;
        private DevExpress.XtraReports.UI.XRTable tblDAO;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow91;
        private DevExpress.XtraReports.UI.XRTableCell lblAODCaseno;
        private DevExpress.XtraReports.UI.XRTableCell lblAODIssuedate;
        private DevExpress.XtraReports.UI.XRTableCell lblAODLoadeddate;
        private DevExpress.XtraReports.UI.XRTableCell lblAODType;
        private DevExpress.XtraReports.UI.XRTableCell lblAODAmount;
        private DevExpress.XtraReports.UI.XRTableCell lblAODPlaintiff;
        private DevExpress.XtraReports.UI.XRTableCell lblAODCOurt;
        private DevExpress.XtraReports.UI.XRTableCell lblAODAttorney;
        private DevExpress.XtraReports.UI.XRTableCell lblAODPhoenNo;
        private DevExpress.XtraReports.UI.XRTable tblSEQC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow93;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQCaseno;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQIssueDate;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQLoadeddate;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQType;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQAmount;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQplaintiff;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQCourt;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQAttorney;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQPhoneNo;
        private DevExpress.XtraReports.UI.XRTable tblSEQH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow92;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQH;
        private DevExpress.XtraReports.UI.XRTable tblSEQD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow94;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQDCaseno;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQDIssuedate;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQDLoadedDate;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQDType;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQDAmount;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQDPlaintiff;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQDCourt;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQDAttorney;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQDPhoneNo;
        private DevExpress.XtraReports.UI.XRTable tblPropInterestsH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow118;
        private DevExpress.XtraReports.UI.XRTableCell lblPropInterestsH;
        private DevExpress.XtraReports.UI.XRTable tblPropInterests;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow120;
        private DevExpress.XtraReports.UI.XRTableCell lblDeedNo;
        private DevExpress.XtraReports.UI.XRTableCell lblDDeedNo;
        private DevExpress.XtraReports.UI.XRTableCell lblsiteno;
        private DevExpress.XtraReports.UI.XRTableCell lblDsiteno;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow125;
        private DevExpress.XtraReports.UI.XRTableCell lblDeedsoffice;
        private DevExpress.XtraReports.UI.XRTableCell lblDDeedsoffice;
        private DevExpress.XtraReports.UI.XRTableCell lblPhyadd;
        private DevExpress.XtraReports.UI.XRTableCell lblDPhyadd;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow124;
        private DevExpress.XtraReports.UI.XRTableCell lblPropType;
        private DevExpress.XtraReports.UI.XRTableCell lblDPropType;
        private DevExpress.XtraReports.UI.XRTableCell lblSize;
        private DevExpress.XtraReports.UI.XRTableCell lblDSize;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow123;
        private DevExpress.XtraReports.UI.XRTableCell lblPurchasedate;
        private DevExpress.XtraReports.UI.XRTableCell lblDPurchasedate;
        private DevExpress.XtraReports.UI.XRTableCell lblPurchaseprice;
        private DevExpress.XtraReports.UI.XRTableCell lblDPurchaseprice;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow122;
        private DevExpress.XtraReports.UI.XRTableCell lblshareperc;
        private DevExpress.XtraReports.UI.XRTableCell lblDshareperc;
        private DevExpress.XtraReports.UI.XRTableCell lblHolder;
        private DevExpress.XtraReports.UI.XRTableCell lblDHolder;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow121;
        private DevExpress.XtraReports.UI.XRTableCell lblBondNo;
        private DevExpress.XtraReports.UI.XRTableCell lblDBondNo;
        private DevExpress.XtraReports.UI.XRTableCell lblBondAmount;
        private DevExpress.XtraReports.UI.XRTableCell lblDBondAmount;
        private DevExpress.XtraReports.UI.XRTable tblDirectorlinksH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow112;
        private DevExpress.XtraReports.UI.XRTableCell lblDirectorlinksH;
        private DevExpress.XtraReports.UI.XRTable tblDirInfo;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow113;
        private DevExpress.XtraReports.UI.XRTableCell lblCurrentPost;
        private DevExpress.XtraReports.UI.XRTableCell lblDCurrentPost;
        private DevExpress.XtraReports.UI.XRTableCell lblInceptionDate;
        private DevExpress.XtraReports.UI.XRTableCell lblDAppDate;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow116;
        private DevExpress.XtraReports.UI.XRTableCell lblCompanyName;
        private DevExpress.XtraReports.UI.XRTableCell lblDCompanyName;
        private DevExpress.XtraReports.UI.XRTableCell lblRegNo;
        private DevExpress.XtraReports.UI.XRTableCell lblDRegNo;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow117;
        private DevExpress.XtraReports.UI.XRTableCell lblCompanyAdd;
        private DevExpress.XtraReports.UI.XRTableCell lblDPhysicalAdd;
        private DevExpress.XtraReports.UI.XRTableCell lblPhoneNo;
        private DevExpress.XtraReports.UI.XRTableCell lblDPhoneNo;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow115;
        private DevExpress.XtraReports.UI.XRTableCell lblIndustryCatg;
        private DevExpress.XtraReports.UI.XRTableCell lblDIndCateg;
        private DevExpress.XtraReports.UI.DetailReportBand MonthlyPayment;
        private DevExpress.XtraReports.UI.DetailBand DetailMonthlyPayment;
        private DevExpress.XtraReports.UI.XRTable tblMonthlyPaymentD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow79;
        private DevExpress.XtraReports.UI.XRTableCell lblcompanyMPD;
        private DevExpress.XtraReports.UI.XRTableCell lblM01D;
        private DevExpress.XtraReports.UI.XRTableCell lblM02D;
        private DevExpress.XtraReports.UI.XRTableCell lblM03D;
        private DevExpress.XtraReports.UI.XRTableCell lblM04D;
        private DevExpress.XtraReports.UI.XRTableCell lblM05D;
        private DevExpress.XtraReports.UI.XRTableCell lblM06D;
        private DevExpress.XtraReports.UI.XRTableCell lblDelinquentStatusD;
        private DevExpress.XtraReports.UI.DetailReportBand DefaultAlert;
        private DevExpress.XtraReports.UI.DetailBand DetailDefaultAlert;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader2;
        private DevExpress.XtraReports.UI.XRTable tblDefaultAlertD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell lblDACompanyD;
        private DevExpress.XtraReports.UI.XRTableCell lblDAAccnoD;
        private DevExpress.XtraReports.UI.XRTableCell lblDADLoadeddateD;
        private DevExpress.XtraReports.UI.XRTableCell lblDAAmountD;
        private DevExpress.XtraReports.UI.XRTableCell lblDAStatusD;
        private DevExpress.XtraReports.UI.XRTableCell lblDACommentsD;
        private DevExpress.XtraReports.UI.XRTable tblDefaultAlertC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell lblDACompany;
        private DevExpress.XtraReports.UI.XRTableCell lblDAAccno;
        private DevExpress.XtraReports.UI.XRTableCell lblDADLoadeddate;
        private DevExpress.XtraReports.UI.XRTableCell lblDAAmount;
        private DevExpress.XtraReports.UI.XRTableCell lblDAStatus;
        private DevExpress.XtraReports.UI.XRTableCell lblDAComments;
        private DevExpress.XtraReports.UI.XRTable tblDefaultAlertH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell lblDefaultAlertH;
        private DevExpress.XtraReports.UI.XRTable tbldbtReviewH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow95;
        private DevExpress.XtraReports.UI.XRTableCell lbldbtReviewH;
        private DevExpress.XtraReports.UI.XRTable tblDbtReviewD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow96;
        private DevExpress.XtraReports.UI.XRTableCell lblDbtReviewDate;
        private DevExpress.XtraReports.UI.XRTableCell lblDbtReviewDateD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow98;
        private DevExpress.XtraReports.UI.XRTableCell lblCounsellorName;
        private DevExpress.XtraReports.UI.XRTableCell lblCounsellorNameD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow97;
        private DevExpress.XtraReports.UI.XRTableCell lblCounsellorTelephoneNo;
        private DevExpress.XtraReports.UI.XRTableCell lblCounsellorTelephoneNoD;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox2;
        private DevExpress.XtraReports.UI.XRTable PersonalDetailsSummary;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell lblReferenceNo;
        private DevExpress.XtraReports.UI.XRTableCell lblRefnoValue;
        private DevExpress.XtraReports.UI.XRTableCell lblExtRefNo;
        private DevExpress.XtraReports.UI.XRTableCell lblExternalRefNoValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell lblIDNo;
        private DevExpress.XtraReports.UI.XRTableCell lblIDnoValue;
        private DevExpress.XtraReports.UI.XRTableCell lblPPNo;
        private DevExpress.XtraReports.UI.XRTableCell lblPassportnoValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell lblSurname;
        private DevExpress.XtraReports.UI.XRTableCell lblSurnameValue;
        private DevExpress.XtraReports.UI.XRTableCell lblResAdd;
        private DevExpress.XtraReports.UI.XRTableCell lblResAddValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell lblFirstname;
        private DevExpress.XtraReports.UI.XRTableCell lblFirstnameValue;
        private DevExpress.XtraReports.UI.XRTableCell lblPostalAdd;
        private DevExpress.XtraReports.UI.XRTableCell lblPostalAddValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell lblSecondName;
        private DevExpress.XtraReports.UI.XRTableCell lblSecondnameValue;
        private DevExpress.XtraReports.UI.XRTableCell lblHomeTelNo;
        private DevExpress.XtraReports.UI.XRTableCell lblHomeTelNoValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell lblTitle;
        private DevExpress.XtraReports.UI.XRTableCell lbltitlevalue;
        private DevExpress.XtraReports.UI.XRTableCell lblWorkTelno;
        private DevExpress.XtraReports.UI.XRTableCell lblWorkTelnoValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell lblGender;
        private DevExpress.XtraReports.UI.XRTableCell lblGenderValue;
        private DevExpress.XtraReports.UI.XRTableCell lblMobileNo;
        private DevExpress.XtraReports.UI.XRTableCell lblMobileNoValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell lblDOB;
        private DevExpress.XtraReports.UI.XRTableCell lblDOBValue;
        private DevExpress.XtraReports.UI.XRTableCell lblEmailAddress;
        private DevExpress.XtraReports.UI.XRTableCell lblEmailAddressValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell lblmaritalStatus;
        private DevExpress.XtraReports.UI.XRTableCell lblmaritalStatusValue;
        private DevExpress.XtraReports.UI.XRTableCell lblCurrentEmployer;
        private DevExpress.XtraReports.UI.XRTableCell lblCurrentEmployerValue;
        private DevExpress.XtraReports.UI.XRTable tblScore;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell lblScore;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail7;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader6;
        private DevExpress.XtraReports.UI.XRTable tblPersonalDetail;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell lblPersonalDetail;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell lblEnquiryDate;
        private DevExpress.XtraReports.UI.XRTableCell lblEnquiryDateValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel95;
        private DevExpress.XtraReports.UI.XRLabel xrLabel73;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo2;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.TopMarginBand topMarginBand1;
        private DevExpress.XtraReports.UI.BottomMarginBand bottomMarginBand1;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRTableCell lblAOComment;
        private DevExpress.XtraReports.UI.XRTableCell lblAODComment;
        private DevExpress.XtraReports.UI.DetailReportBand EnquiryHistory;
        private DevExpress.XtraReports.UI.DetailBand DetailEnquiryHistory;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.XRTable tblEnquiryC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow100;
        private DevExpress.XtraReports.UI.XRTableCell tblEnqDate;
        private DevExpress.XtraReports.UI.XRTableCell tblsubscribername;
        private DevExpress.XtraReports.UI.XRTableCell lblbusType;
        private DevExpress.XtraReports.UI.XRTable tblEnquiryH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow99;
        private DevExpress.XtraReports.UI.XRTableCell lblEnquiryH;
        private DevExpress.XtraReports.UI.XRTable tblEnquiryD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow101;
        private DevExpress.XtraReports.UI.XRTableCell lblEnqDateD;
        private DevExpress.XtraReports.UI.XRTableCell tblsubscribernameD;
        private DevExpress.XtraReports.UI.XRTableCell lblbusTypeD;
    }
}
