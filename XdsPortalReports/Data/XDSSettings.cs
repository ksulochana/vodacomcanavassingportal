﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace XDSPortalEnquiry.Data
{
    public class XDSSettings
    {
        public string GetConnection(SqlConnection AdminCon, int productID)
        {
            string Connectionstring = string.Empty;

            if (AdminCon.State == ConnectionState.Closed)
                AdminCon.Open();

            string strSQL = "SELECT Connectionstring from XDS_Settings_ConnectionString nolock Where productID = " + productID.ToString() + " and isnull(Active,0) = 1";
            SqlCommand Imycommand = new SqlCommand(strSQL, AdminCon);
            SqlDataReader Idr = Imycommand.ExecuteReader();
            Idr.Read();
            Connectionstring = Idr.GetValue(0).ToString();
            Idr.Close();


            if (AdminCon.State == ConnectionState.Open)
                AdminCon.Close();

            return Connectionstring;
        }
    }
}
