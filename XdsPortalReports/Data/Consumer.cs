﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalEnquiry.Data
{
    public class Consumer
    {
        public Consumer()
        {
        }
        public int GetConsumerID(SqlConnection ObjConstring,int DirectorID, int HomeAffairsID)
        {
            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            SqlCommand sqlcmd = new SqlCommand("spReport_GetConsumerID", ObjConstring);
            sqlcmd.CommandType = CommandType.StoredProcedure;

            sqlcmd.Parameters.AddWithValue("@DirectorID", DirectorID);
            sqlcmd.Parameters.AddWithValue("@HomeAffairsID",HomeAffairsID);

            int consumerID = 0;

            SqlParameter pconsumerID = new SqlParameter("ConsumerID",SqlDbType.Int);
            pconsumerID.Direction = ParameterDirection.ReturnValue;

            sqlcmd.Parameters.Add(pconsumerID);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();

            consumerID = int.Parse(pconsumerID.Value.ToString());

            return consumerID;
        }

        public void CheckBlockedStatus(SqlConnection ObjConstring, int SubscriberID, int COnsumerID, int productID, string SubscriberAssociationCode,int ReportID)
        {
            try
            {
                if (ObjConstring.State == ConnectionState.Closed)
                    ObjConstring.Open();

                SqlCommand sqlcmd = new SqlCommand("spCheckblockingStatus_V2", ObjConstring);
                sqlcmd.CommandType = CommandType.StoredProcedure;

                sqlcmd.Parameters.AddWithValue("@SubscriberID", SubscriberID);
                sqlcmd.Parameters.AddWithValue("@ConsumerID", COnsumerID);
                sqlcmd.Parameters.AddWithValue("@productID", productID);
                sqlcmd.Parameters.AddWithValue("@SubscriberAssociationCode", SubscriberAssociationCode);
                sqlcmd.Parameters.AddWithValue("@ReportID", ReportID);

                sqlcmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public void CheckBlockedStatus(string Constring, int SubscriberID, int COnsumerID, int productID, string SubscriberAssociationCode, int ReportID)
        {
            try
            {
                SqlConnection ObjConstring = new SqlConnection(Constring);

                if (ObjConstring.State == ConnectionState.Closed)
                    ObjConstring.Open();

                SqlCommand sqlcmd = new SqlCommand("spCheckblockingStatus_V2", ObjConstring);
                sqlcmd.CommandType = CommandType.StoredProcedure;

                sqlcmd.Parameters.AddWithValue("@SubscriberID", SubscriberID);
                sqlcmd.Parameters.AddWithValue("@ConsumerID", COnsumerID);
                sqlcmd.Parameters.AddWithValue("@productID", productID);
                sqlcmd.Parameters.AddWithValue("@SubscriberAssociationCode", SubscriberAssociationCode);
                sqlcmd.Parameters.AddWithValue("@ReportID", ReportID);

                sqlcmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

    }
}
