
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalEnquiry.Data
{
    public class SubscriberEnquiryResultBonus
    {
        public SubscriberEnquiryResultBonus() { }


        public int InsertSubscriberEnquiryResultBonus(string DBConnection, Entity.SubscriberEnquiryResultBonus S)
        {
         
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = DBConnection;
            cn.Open();

            string insertSQL = "EXEC sp_i_SubscriberEnquiryResultBonus " + S.SubscriberEnquiryResultID + " , " + S.DataSegmentID + " , '" + S.DataSegmentName + "' , '" + S.DataSegmentDisplayText + "' , " + S.BonusViewed + " , " + S.BillingPrice + " , " + S.Billable + " , '" + S.CreatedByUser + "' , '" + S.CreatedOnDate + "' , '" + S.ChangedByUser + "' , '" + S.ChangedOnDate + "' ";
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();

            string strSQL = "SELECT @@IDENTITY";
            SqlCommand Imycommand = new SqlCommand(strSQL, cn);
            SqlDataReader Idr = Imycommand.ExecuteReader();
            Idr.Read();
            int intSubscriberEnquiryID = Convert.ToInt32(Idr.GetValue(0));
            Idr.Close();
            cn.Close();

            sqlcmd.Dispose();
            Imycommand.Dispose();
            Idr.Dispose();

            return intSubscriberEnquiryID;
        }

        public int InsertSubscriberEnquiryResultBonus(SqlConnection cn, Entity.SubscriberEnquiryResultBonus S)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();


            string insertSQL = "EXEC sp_i_SubscriberEnquiryResultBonus " + S.SubscriberEnquiryResultID + " , " + S.DataSegmentID + " , '" + S.DataSegmentName + "' , '" + S.DataSegmentDisplayText + "' , " + S.BonusViewed + " , " + S.BillingPrice + " , " + S.Billable + " , '" + S.CreatedByUser + "' , '" + S.CreatedOnDate + "' , '" + S.ChangedByUser + "' , '" + S.ChangedOnDate + "' ";
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();

            string strSQL = "SELECT @@IDENTITY";
            SqlCommand Imycommand = new SqlCommand(strSQL, cn);
            SqlDataReader Idr = Imycommand.ExecuteReader();
            Idr.Read();
            int intSubscriberEnquiryID = Convert.ToInt32(Idr.GetValue(0));
            Idr.Close();

            sqlcmd.Dispose();
            Imycommand.Dispose();
            Idr.Dispose();
            cn.Close();

            return intSubscriberEnquiryID;
        }

        public int UpdateSubscriberEnquiryResultBonus(string DBConnection, Entity.SubscriberEnquiryResultBonus S)
        {
            
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = DBConnection;
            cn.Open();

            string insertSQL = "EXEC sp_u_SubscriberEnquiryResultBonus " + S.SubscriberEnquiryResultBonusID + " , " + S.SubscriberEnquiryResultID + " , " + S.DataSegmentID + " , '" + S.DataSegmentName + "' , '" + S.DataSegmentDisplayText + "' , " + S.BonusViewed + " , " + S.BillingPrice + " , '" + S.CreatedByUser + "' , '" + S.CreatedOnDate + "' , '" + S.ChangedByUser + "' , '" + S.ChangedOnDate + "' ";
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();
            cn.Close();

            sqlcmd.Dispose();
            return added;
        }

        public int UpdateSubscriberEnquiryResultBonus(SqlConnection cn, Entity.SubscriberEnquiryResultBonus S)
        {

            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string insertSQL = "EXEC sp_u_SubscriberEnquiryResultBonus " + S.SubscriberEnquiryResultBonusID + " , " + S.SubscriberEnquiryResultID + " , " + S.DataSegmentID + " , '" + S.DataSegmentName + "' , '" + S.DataSegmentDisplayText + "' , " + S.BonusViewed + "," + S.BillingPrice + " , "  + S.Billable + " , '" + S.CreatedByUser + "' , '" + S.CreatedOnDate + "' , '" + S.ChangedByUser + "' , '" + S.ChangedOnDate + "' ";
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();

            sqlcmd.Dispose();
            cn.Close();

            return added;
        }


        public int UpdateResultBonusAsViewed(SqlConnection cn, int SubscriberEnquiryResultID, int DataSegmentID)
        {

            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string insertSQL = string.Format("UPDATE SubscriberEnquiryResultBonus SET BonusViewed = 1 WHERE SubscriberEnquiryResultID = {0} AND DataSegmentID = {1}", SubscriberEnquiryResultID, DataSegmentID);
            
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();

            sqlcmd.Dispose();
            cn.Close();

            return added;
        }


        public DataSet GetSubscriberEnquiryResultBonusDataSet(string DBConnection, int intSubscriberEnquiryResultID)
        {

            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = DBConnection;
            cn.Open();

            string strSQL = "Select * from SubscriberEnquiryResultBonus nolock Where SubscriberEnquiryResultID = " + intSubscriberEnquiryResultID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            sqlcmd.Dispose();
            sqlda.Dispose();
            cn.Close();

            return ds;
        }

        public DataSet GetSubscriberEnquiryResultBonusDataSet(SqlConnection cn, int intSubscriberEnquiryResultID)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string strSQL = "Select * from SubscriberEnquiryResultBonus nolock Where SubscriberEnquiryResultID = " + intSubscriberEnquiryResultID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            sqlda.Dispose();
            sqlcmd.Dispose();
            cn.Close();

            return ds;
        }

        public DataSet GetSelectedSubscriberEnquiryResultBonus(string DBConnection, int intSubscriberEnquiryResultID)
        {

            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = DBConnection;
            cn.Open();

            string strSQL = "Select * from SubscriberEnquiryResultBonus nolock Where SubscriberEnquiryResultID = " + intSubscriberEnquiryResultID.ToString() + " and BonusViewed = 1";
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            sqlcmd.Dispose();
            sqlda.Dispose();
            cn.Close();

            return ds;
        }

        public DataSet GetSelectedSubscriberEnquiryResultBonus(SqlConnection cn, int intSubscriberEnquiryResultID)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string strSQL = "Select * from SubscriberEnquiryResultBonus nolock Where SubscriberEnquiryResultID = " + intSubscriberEnquiryResultID.ToString() + " and BonusViewed = 1";
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            sqlda.Dispose();
            sqlcmd.Dispose();
            cn.Close();

            return ds;
        }

        public List<Entity.SubscriberEnquiryResultBonus> GetSubscriberEnquiryResultBonusListObject(string DBConnection, int intSubscriberEnquiryResultID)
        {
            List<Entity.SubscriberEnquiryResultBonus> Lsec = new List<Entity.SubscriberEnquiryResultBonus>();
            Entity.SubscriberEnquiryResultBonus sec = new Entity.SubscriberEnquiryResultBonus();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = DBConnection;
            cn.Open();

            string strSQL = "Select * from SubscriberEnquiryResultBonus  nolock Where SubscriberEnquiryResultID = " + intSubscriberEnquiryResultID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                sec.SubscriberEnquiryResultID = int.Parse(r["SubscriberEnquiryResultID"].ToString());
                sec.SubscriberEnquiryResultBonusID = int.Parse(r["SubscriberEnquiryResultBonusID"].ToString());
                sec.Billable = bool.Parse(r["Billable"].ToString());
                sec.BillingPrice = double.Parse(r["BillingPrice"].ToString());
                sec.BonusViewed = bool.Parse(r["BonusViewed"].ToString());
                sec.ChangedByUser = r["ChangedByUser"].ToString();
                sec.ChangedOnDate = DateTime.Parse(r["ChangedOnDate"].ToString());
                sec.CreatedByUser = r["CreatedByUser"].ToString();
                sec.CreatedOnDate = DateTime.Parse(r["CreatedOnDate"].ToString());
                sec.DataSegmentDisplayText = r["DataSegmentDisplayText"].ToString();
                sec.DataSegmentID = int.Parse(r["DataSegmentID"].ToString());
                sec.DataSegmentName = r["DataSegmentName"].ToString();
                Lsec.Add(sec);
            }

            sqlcmd.Dispose();
            sqlda.Dispose();
            ds.Dispose();
            cn.Close();

            return Lsec;


        }

        public List<Entity.SubscriberEnquiryResultBonus> GetSubscriberEnquiryResultBonusListObject(SqlConnection cn, int intSubscriberEnquiryResultID)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            List<Entity.SubscriberEnquiryResultBonus> Lsec = new List<Entity.SubscriberEnquiryResultBonus>();
            Entity.SubscriberEnquiryResultBonus sec = new Entity.SubscriberEnquiryResultBonus();
           

            string strSQL = "Select * from SubscriberEnquiryResultBonus nolock Where SubscriberEnquiryResultID = " + intSubscriberEnquiryResultID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                sec.SubscriberEnquiryResultID = int.Parse(r["SubscriberEnquiryResultID"].ToString());
                sec.SubscriberEnquiryResultBonusID = int.Parse(r["SubscriberEnquiryResultBonusID"].ToString());
                sec.Billable = bool.Parse(r["Billable"].ToString());
                sec.BillingPrice = double.Parse(r["BillingPrice"].ToString());
                sec.BonusViewed = bool.Parse(r["BonusViewed"].ToString());
                sec.ChangedByUser = r["ChangedByUser"].ToString();
                sec.ChangedOnDate = DateTime.Parse(r["ChangedOnDate"].ToString());
                sec.CreatedByUser = r["CreatedByUser"].ToString();
                sec.CreatedOnDate = DateTime.Parse(r["CreatedOnDate"].ToString());
                sec.DataSegmentDisplayText = r["DataSegmentDisplayText"].ToString();
                sec.DataSegmentID = int.Parse(r["DataSegmentID"].ToString());
                sec.DataSegmentName = r["DataSegmentName"].ToString();
                Lsec.Add(sec);
            }

            sqlcmd.Dispose();
            sqlda.Dispose();
            ds.Dispose();
            cn.Close();

            return Lsec;
        }

        public Entity.SubscriberEnquiryResultBonus GetSubscriberEnquiryResultBonusObject(string DBConnection, int intSubscriberEnquiryResultID)
        {
            Entity.SubscriberEnquiryResultBonus sec = new Entity.SubscriberEnquiryResultBonus();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = DBConnection;
            cn.Open();

            string strSQL = "Select * from SubscriberEnquiryResult nolock Where SubscriberEnquiryResultID = " + intSubscriberEnquiryResultID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                sec.SubscriberEnquiryResultID = int.Parse(r["SubscriberEnquiryResultID"].ToString());
                sec.SubscriberEnquiryResultBonusID = int.Parse(r["SubscriberEnquiryResultBonusID"].ToString());
                sec.Billable = bool.Parse(r["Billable"].ToString());
                sec.BillingPrice = double.Parse(r["BillingPrice"].ToString());
                sec.BonusViewed = bool.Parse(r["BonusViewed"].ToString());
                sec.ChangedByUser = r["ChangedByUser"].ToString();
                sec.ChangedOnDate = DateTime.Parse(r["ChangedOnDate"].ToString());
                sec.CreatedByUser = r["CreatedByUser"].ToString();
                sec.CreatedOnDate = DateTime.Parse(r["CreatedOnDate"].ToString());
                sec.DataSegmentDisplayText = r["DataSegmentDisplayText"].ToString();
                sec.DataSegmentID = int.Parse(r["DataSegmentID"].ToString());
                sec.DataSegmentName = r["DataSegmentName"].ToString();
            }
            sqlcmd.Dispose();
            sqlda.Dispose();
            ds.Dispose();
            cn.Close();

            return sec;


        }

        public Entity.SubscriberEnquiryResultBonus GetSubscriberEnquiryResultBonusObject(SqlConnection cn, int intSubscriberEnquiryResultID)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            Entity.SubscriberEnquiryResultBonus sec = new Entity.SubscriberEnquiryResultBonus();
           

            string strSQL = "Select * from SubscriberEnquiryResult nolock Where SubscriberEnquiryResultID = " + intSubscriberEnquiryResultID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                sec.SubscriberEnquiryResultID = int.Parse(r["SubscriberEnquiryResultID"].ToString());
                sec.SubscriberEnquiryResultBonusID = int.Parse(r["SubscriberEnquiryResultBonusID"].ToString());
                sec.Billable = bool.Parse(r["Billable"].ToString());
                sec.BillingPrice = double.Parse(r["BillingPrice"].ToString());
                sec.BonusViewed = bool.Parse(r["BonusViewed"].ToString());
                sec.ChangedByUser = r["ChangedByUser"].ToString();
                sec.ChangedOnDate = DateTime.Parse(r["ChangedOnDate"].ToString());
                sec.CreatedByUser = r["CreatedByUser"].ToString();
                sec.CreatedOnDate = DateTime.Parse(r["CreatedOnDate"].ToString());
                sec.DataSegmentDisplayText = r["DataSegmentDisplayText"].ToString();
                sec.DataSegmentID = int.Parse(r["DataSegmentID"].ToString());
                sec.DataSegmentName = r["DataSegmentName"].ToString();
            }

            sqlcmd.Dispose();
            sqlda.Dispose();
            ds.Dispose();
            cn.Close();

            return sec;


        }

        public DataSet GetSubscriberEnquiryResultBonusDS(SqlConnection cn, int intSubscriberEnquiryResultID)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string strSQL = "Select DataSegmentID, DataSegmentName, DataSegmentDisplayText, BonusViewed, Convert(Decimal(10,4),BillingPrice) as BillingPrice from SubscriberEnquiryResultBonus nolock Where SubscriberEnquiryResultID = " + intSubscriberEnquiryResultID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet("BonusSegments");
            sqlda.Fill(ds);

            foreach (DataTable dt in ds.Tables)
            {
                dt.TableName = "Segments";
            }

            sqlda.Dispose();
            sqlcmd.Dispose();
            cn.Close();
            
            return ds;
        }

    }
}
