﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace XDSPortalEnquiry.Data
{
    public class SubscriberFootPrint
    {
        public SubscriberFootPrint()
        {
        }

        public DataSet GetSubscriberFootPrint(string conString, int intConsumerID)
        {
            Entity.Subscriber objSubscriber = new Entity.Subscriber();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "select * from SubscriberFootPrint (NOLOCK) WHERE KEYID = " + intConsumerID;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            objsqladp.Dispose();
            Objsqlcom.Dispose();
            ObjConstring.Close();

            return ds;
        }

        public DataSet GetSubscriberFootPrint(SqlConnection ObjConstring, int intConsumerID)
        {
            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            Entity.Subscriber objSubscriber = new Entity.Subscriber();

            string sqlSelect = "select * from SubscriberFootPrint (NOLOCK) WHERE KEYID = " + intConsumerID;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            objsqladp.Dispose();
            Objsqlcom.Dispose();
            ObjConstring.Close();

            return ds;
        }


        public int insertSubscriberFootPrint(string conString, XDSPortalEnquiry.Entity.SubscriberFootPrint oSubscriberFootPrint)
        {
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = conString;
            cn.Open();

            string insertSQL = "INSERT INTO [SubscriberFootPrint]([SubscriberEnquiryResultID],[SubscriberEnquiryID],[SubscriberEnquiryDate],[KeyType],[KeyID],[EnquiryReason],[SubscriberID],[SubscriberName],[SubscriberContact],[SubscriberUser],[SubscriberBusinessType],[AssociationTypeCode],[ProductID],[CreatedByUser],[CreatedOnDate],[ChangedByUser],[ChangedOnDate]) VALUES ( " + oSubscriberFootPrint.SubscriberEnquiryResultID + " , " + oSubscriberFootPrint.SubscriberEnquiryID + " , '" + oSubscriberFootPrint.SubscriberEnquiryDate + "' , '" + oSubscriberFootPrint.KeyType + "' , " + oSubscriberFootPrint.KeyID + " , '" + oSubscriberFootPrint.EnquiryReason + "' , " + oSubscriberFootPrint.SubscriberID + " , '" + oSubscriberFootPrint.SubscriberName + "' , '" + oSubscriberFootPrint.SubscriberContact + "' , '" + oSubscriberFootPrint.SubscriberUser + "' , '" + oSubscriberFootPrint.SubscriberBusinessType + "' , '"+oSubscriberFootPrint.AssociationTypeCode+"' , "+oSubscriberFootPrint.ProductID+" , '" + oSubscriberFootPrint.CreatedByUser + "' , '" + oSubscriberFootPrint.CreatedOnDate + "' , '" + oSubscriberFootPrint.ChangedByUser + "' , '" + oSubscriberFootPrint.ChangedOnDate + "')";
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();

            string strSQL = "SELECT @@IDENTITY";
            SqlCommand Imycommand = new SqlCommand(strSQL, cn);
            SqlDataReader Idr = Imycommand.ExecuteReader();
            Idr.Read();
            int intSubscriberFootPrintID = Convert.ToInt32(Idr.GetValue(0));
            Idr.Close();
            cn.Close();

            sqlcmd.Dispose();
            Imycommand.Dispose();
            Idr.Dispose();

            return intSubscriberFootPrintID;
        }

        public int insertSubscriberFootPrint(SqlConnection cn, XDSPortalEnquiry.Entity.SubscriberFootPrint oSubscriberFootPrint)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            if (!string.IsNullOrEmpty(oSubscriberFootPrint.SubscriberName))
                oSubscriberFootPrint.SubscriberName = oSubscriberFootPrint.SubscriberName.Replace("'", "''");
            if (!string.IsNullOrEmpty(oSubscriberFootPrint.SubscriberUser))
                oSubscriberFootPrint.SubscriberUser = oSubscriberFootPrint.SubscriberUser.Replace("'", "''");

            string insertSQL = "sp_I_SubscriberFootPrint " + oSubscriberFootPrint.SubscriberEnquiryResultID + " , " + oSubscriberFootPrint.SubscriberEnquiryID + " , '" + oSubscriberFootPrint.SubscriberEnquiryDate + "' , '" + oSubscriberFootPrint.KeyType + "' , " + oSubscriberFootPrint.KeyID + " , '" + oSubscriberFootPrint.EnquiryReason + "' , " + oSubscriberFootPrint.SubscriberID + " , '" + oSubscriberFootPrint.SubscriberName + "' , '" + oSubscriberFootPrint.SubscriberContact + "' , '" + oSubscriberFootPrint.SubscriberUser + "' , '" + oSubscriberFootPrint.SubscriberBusinessType + "' , '" + oSubscriberFootPrint.AssociationTypeCode + "' , " + oSubscriberFootPrint.ProductID + " , '" + oSubscriberFootPrint.CreatedByUser + "' , '" + oSubscriberFootPrint.CreatedOnDate + "'";
            //, '" + oSubscriberFootPrint.ChangedByUser + "' , '" + oSubscriberFootPrint.ChangedOnDate + "'";
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();

            //string strSQL = "SELECT @@IDENTITY";
            //SqlCommand Imycommand = new SqlCommand(strSQL, cn);
            //SqlDataReader Idr = Imycommand.ExecuteReader();
            //Idr.Read();
            int intSubscriberFootPrintID = added;
            //Idr.Close();

            sqlcmd.Dispose();
            //Imycommand.Dispose();
            //Idr.Dispose();
            cn.Close();
            return intSubscriberFootPrintID;
        }

    }
}

