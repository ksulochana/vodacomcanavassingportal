﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace XDSPortalEnquiry.Data
{
   public  class IdecoreturnCodes
    {
       public DataSet GetIdecoReturnCodesDataSet(SqlConnection cn)
       {
           if (cn.State == ConnectionState.Closed)
               cn.Open();

           string strSQL = "Select * from IdecoReturnCodes nolock ";
           SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
           SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
           DataSet ds = new DataSet();
           sqlda.Fill(ds);

           sqlda.Dispose();
           sqlcmd.Dispose();
           cn.Close();

           return ds;
       }
    }
}
