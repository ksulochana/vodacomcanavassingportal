﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalEnquiry.Data
{
    public class Reports
    {
        public Reports()
        {
        }
        public Entity.Reports GetProductReportsRecord(SqlConnection ObjConstring, int ReportID)
        {
            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            Entity.Reports objReport = new Entity.Reports();

            string sqlSelect = "select * from Reports where ReportID = " + ReportID;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objReport.ReportID = Convert.ToInt32(Dr["ReportID"]);
                objReport.ReportName = Dr["ReportName"].ToString();
                objReport.ReportPath = Dr["ReportPath"].ToString();

            }
            ds.Dispose();
            Objsqlcom.Dispose();
            objsqladp.Dispose();
            ObjConstring.Close();

            return objReport;
        }
       
    }
}
