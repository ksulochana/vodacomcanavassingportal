﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace XDSPortalEnquiry.Data
{
    public class CreditEnquiryReasons
    {
        public CreditEnquiryReasons()
        {
        }

        public DataSet GetCreditEnquiryReasons(SqlConnection ObjConstring)
        {
            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            Entity.Subscriber objSubscriber = new Entity.Subscriber();


            string sqlSelect = "select * from CreditGrantorEnquiryReason (NOLOCK) ";

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            ObjConstring.Close();

            return ds;
        }

       
    }
}

