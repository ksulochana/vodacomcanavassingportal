
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalEnquiry.Data
{
    public class SubscriberEnquiryResult
    {

        public SubscriberEnquiryResult() {}

        public int InsertSubscriberEnquiryResult(string DBConnection, Entity.SubscriberEnquiryResult SC)
        {

            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = DBConnection;
            cn.Open();

            string insertSQL = "sp_i_SubscriberEnquiryResult " + SC.SubscriberEnquiryID + " , '" + SC.KeyType + "' , " + SC.KeyID + " , '" + SC.EnquiryResult + "' , '" + SC.SearchOutput + "' , '" + SC.IDNo + "' , '" + SC.PassportNo + "' , '" + SC.FirstName + "' , '" + SC.SecondName + "' , '" + SC.Surname + "' , '" + SC.BirthDate + "' , '" + SC.Gender + "' , '" + SC.FirstInitial + "' , '" + SC.SecondInitial + "' , '" + SC.BusRegistrationNo + "' , '" + SC.BusBusinessName + "' , '" + SC.Ha_IDNo + "' , '" + SC.Ha_FirstName + "' , '" + SC.Ha_SecondName + "' , '" + SC.Ha_Surname + "' , '" + SC.Ha_DeceasedStatus + "' , '" + SC.Ha_DeceasedDate + "' , '" + SC.Ha_CauseOfDeath + "' , " + SC.BillingTypeID + " , " + SC.BillingPrice + " , '" + SC.VoucherCode + "' , " + SC.DetailsViewedYN + " , '" + SC.DetailsViewedDate + "' , " + SC.Billable + " , '" + SC.XMLData + "' , '" + SC.XMLBonus + "' , " + SC.BonusIncluded + " , '" + SC.ExtraVarOutput1 + "' , '" + SC.ExtraVarOutput2 + "' , '" + SC.ExtraVarOutput3 + "' , " + SC.ExtraIntOutput1 + " , " + SC.ExtraIntOutput2 + " , '" + SC.CreatedByUser + "' , '" + SC.CreatedOnDate + "' , '" + SC.ChangedByUser + "' , '" + SC.ChangedOnDate + "' ";
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();

            string strSQL = "SELECT @@IDENTITY";
            SqlCommand Imycommand = new SqlCommand(strSQL, cn);
            SqlDataReader Idr = Imycommand.ExecuteReader();
            Idr.Read();
            int intSubscriberEnquiryResultID = Convert.ToInt32(Idr.GetValue(0));
            Idr.Close();
            cn.Close();

            sqlcmd.Dispose();
            Imycommand.Dispose();
            Idr.Dispose();
            return intSubscriberEnquiryResultID;
        }

        public int InsertSubscriberEnquiryResult(SqlConnection cn, Entity.SubscriberEnquiryResult SC)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            if (!string.IsNullOrEmpty(SC.Surname))
                SC.Surname = SC.Surname.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.FirstName))
                SC.FirstName = SC.FirstName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.SecondName))
                SC.SecondName = SC.SecondName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.Ha_FirstName))
                SC.Ha_FirstName = SC.Ha_FirstName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.Ha_SecondName))
                SC.Ha_SecondName = SC.Ha_SecondName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.Ha_Surname))
                SC.Ha_Surname = SC.Ha_Surname.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.BusBusinessName))
                SC.BusBusinessName = SC.BusBusinessName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.Ha_CauseOfDeath))
                SC.Ha_CauseOfDeath = SC.Ha_CauseOfDeath.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.XMLBonus))
                SC.XMLBonus = SC.XMLBonus.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.XMLData))
                SC.XMLData = SC.XMLData.Replace("'", "''");

   
            string insertSQL = "sp_i_SubscriberEnquiryResult " + SC.SubscriberEnquiryID + " , '" + SC.KeyType + "' , " + SC.KeyID + " , '" + SC.EnquiryResult + "' , '" + SC.SearchOutput + "' , '" + SC.IDNo + "' , '" + SC.PassportNo + "' , '" + SC.FirstName + "' , '" + SC.SecondName + "' , '" + SC.Surname + "' , '" + SC.BirthDate + "' , '" + SC.Gender + "' , '" + SC.FirstInitial + "' , '" + SC.SecondInitial + "' , '" + SC.BusRegistrationNo + "' , '" + SC.BusBusinessName + "' , '" + SC.Ha_IDNo + "' , '" + SC.Ha_FirstName + "' , '" + SC.Ha_SecondName + "' , '" + SC.Ha_Surname + "' , '" + SC.Ha_DeceasedStatus + "' , '" + SC.Ha_DeceasedDate + "' , '" + SC.Ha_CauseOfDeath + "' , " + SC.BillingTypeID + " , " + SC.BillingPrice + " , '" + SC.VoucherCode + "' , " +  SC.DetailsViewedYN + " , '" + SC.DetailsViewedDate + "' , " + SC.Billable + " , '" + SC.XMLData + "' , '" + SC.XMLBonus + "' , " + SC.BonusIncluded + " , '" + SC.ExtraVarOutput1 + "' , '" + SC.ExtraVarOutput2 + "' , '" + SC.ExtraVarOutput3 + "' , " + SC.ExtraIntOutput1 + " , " + SC.ExtraIntOutput2 + " , '" + SC.CreatedByUser + "' , '" + SC.CreatedOnDate + "' , '" + SC.ChangedByUser + "' , '" + SC.ChangedOnDate + "',"+SC.ProductID;
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();

            string strSQL = "SELECT @@IDENTITY";
            SqlCommand Imycommand = new SqlCommand(strSQL, cn);
            SqlDataReader Idr = Imycommand.ExecuteReader();
            Idr.Read();
            int intSubscriberEnquiryResultID = Convert.ToInt32(Idr.GetValue(0));
            Idr.Close();

            sqlcmd.Dispose();
            Idr.Dispose();
            Imycommand.Dispose();
            cn.Close();
            return intSubscriberEnquiryResultID;
        }

        public int UpdateSubscriberEnquiryResult(string DBConnection, Entity.SubscriberEnquiryResult SC)
        {

            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = DBConnection;
            cn.Open();

            if (!string.IsNullOrEmpty(SC.Surname))
                SC.Surname = SC.Surname.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.FirstName))
                SC.FirstName = SC.FirstName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.SecondName))
                SC.SecondName = SC.SecondName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.Ha_FirstName))
                SC.Ha_FirstName = SC.Ha_FirstName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.Ha_SecondName))
                SC.Ha_SecondName = SC.Ha_SecondName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.Ha_Surname))
                SC.Ha_Surname = SC.Ha_Surname.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.BusBusinessName))
                SC.BusBusinessName = SC.BusBusinessName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.Ha_CauseOfDeath))
                SC.Ha_CauseOfDeath = SC.Ha_CauseOfDeath.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.XMLBonus))
                SC.XMLBonus = SC.XMLBonus.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.XMLData))
                SC.XMLData = SC.XMLData.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.SearchOutput))
                SC.SearchOutput = SC.SearchOutput.Replace("'", "''");

            string insertSQL = "EXEC sp_u_SubscriberEnquiryResult " + SC.SubscriberEnquiryResultID + " , " + SC.SubscriberEnquiryID + " , '" + SC.KeyType + "' , " + SC.KeyID + " , '" + SC.EnquiryResult + "' , '" + SC.SearchOutput + "' , '" + SC.IDNo + "' , '" + SC.PassportNo + "' , '" + SC.FirstName + "' , '" + SC.SecondName + "' , '" + SC.Surname + "' , '" + SC.BirthDate + "' , '" + SC.Gender + "' , '" + SC.FirstInitial + "' , '" + SC.SecondInitial + "' , '" + SC.BusRegistrationNo + "' , '" + SC.BusBusinessName + "' , '" + SC.Ha_IDNo + "' , '" + SC.Ha_FirstName + "' , '" + SC.Ha_SecondName + "' , '" + SC.Ha_Surname + "' , '" + SC.Ha_DeceasedStatus + "' , '" + SC.Ha_DeceasedDate + "' , '" + SC.Ha_CauseOfDeath + "' , " + SC.BillingTypeID + " , " + SC.BillingPrice + " , '" + SC.VoucherCode + "' , " + SC.DetailsViewedYN + " , '" + SC.DetailsViewedDate + "' , " + SC.Billable + " , '" + SC.XMLData + "' , '" + SC.XMLBonus + "' , " + SC.BonusIncluded + " , '" + SC.ExtraVarOutput1 + "' , '" + SC.ExtraVarOutput2 + "' , '" + SC.ExtraVarOutput3 + "' , " + SC.ExtraIntOutput1 + " , " + SC.ExtraIntOutput2 + " , '" + SC.CreatedByUser + "' , '" + SC.CreatedOnDate + "' , '" + SC.ChangedByUser + "' , '" + SC.ChangedOnDate + "'," + SC.ProductID + "," + SC.NLRDataIncluded;
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();
            cn.Close();

            sqlcmd.Dispose();

            return added;
        }

        public int UpdateSubscriberEnquiryResult(SqlConnection cn, Entity.SubscriberEnquiryResult SC)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            if (!string.IsNullOrEmpty(SC.Surname))
                SC.Surname = SC.Surname.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.FirstName))
                SC.FirstName = SC.FirstName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.SecondName))
                SC.SecondName = SC.SecondName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.Ha_FirstName))
                SC.Ha_FirstName = SC.Ha_FirstName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.Ha_SecondName))
                SC.Ha_SecondName = SC.Ha_SecondName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.Ha_Surname))
                SC.Ha_Surname = SC.Ha_Surname.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.BusBusinessName))
                SC.BusBusinessName = SC.BusBusinessName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.Ha_CauseOfDeath))
                SC.Ha_CauseOfDeath = SC.Ha_CauseOfDeath.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.XMLBonus))
                SC.XMLBonus = SC.XMLBonus.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.XMLData))
                SC.XMLData = SC.XMLData.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.SearchOutput))
                SC.SearchOutput = SC.SearchOutput.Replace("'", "''");

            string insertSQL = "EXEC sp_u_SubscriberEnquiryResult " + SC.SubscriberEnquiryResultID + " , " + SC.SubscriberEnquiryID + " , '" + SC.KeyType + "' , " + SC.KeyID + " , '" + SC.EnquiryResult + "' , '" + SC.SearchOutput + "' , '" + SC.IDNo + "' , '" + SC.PassportNo + "' , '" + SC.FirstName + "' , '" + SC.SecondName + "' , '" + SC.Surname + "' , '" + SC.BirthDate + "' , '" + SC.Gender + "' , '" + SC.FirstInitial + "' , '" + SC.SecondInitial + "' , '" + SC.BusRegistrationNo + "' , '" + SC.BusBusinessName + "' , '" + SC.Ha_IDNo + "' , '" + SC.Ha_FirstName + "' , '" + SC.Ha_SecondName + "' , '" + SC.Ha_Surname + "' , '" + SC.Ha_DeceasedStatus + "' , '" + SC.Ha_DeceasedDate + "' , '" + SC.Ha_CauseOfDeath + "' , " + SC.BillingTypeID + " , " + SC.BillingPrice + " , '" + SC.VoucherCode + "' , " + SC.DetailsViewedYN + " , '" + SC.DetailsViewedDate + "' , " + SC.Billable + " , '" + SC.XMLData + "' , '" + SC.XMLBonus + "' , " + SC.BonusIncluded + " , '" + SC.ExtraVarOutput1 + "' , '" + SC.ExtraVarOutput2 + "' , '" + SC.ExtraVarOutput3 + "' , " + SC.ExtraIntOutput1 + " , " + SC.ExtraIntOutput2 + " , '" + SC.CreatedByUser + "' , '" + SC.CreatedOnDate + "' , '" + SC.ChangedByUser + "' , '" + SC.ChangedOnDate + "'," + SC.ProductID + "," + SC.NLRDataIncluded;
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();

            sqlcmd.Dispose();
            cn.Close();

            return added;
        }

        public int UpdateMulipleSubscriberEnquiryResult(string DBConnection, Entity.SubscriberEnquiryResult SC)
        {


            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = DBConnection;
            cn.Open();
            string insertSQL = "EXEC sp_u_SubscriberEnquiryResult " + SC.SubscriberEnquiryResultID + " , " + SC.SubscriberEnquiryID + " , '" + SC.KeyType + "' , " + SC.KeyID + " , '" + SC.EnquiryResult + "' , '" + SC.SearchOutput + "' , '" + SC.IDNo + "' , '" + SC.PassportNo + "' , '" + SC.FirstName + "' , '" + SC.SecondName + "' , '" + SC.Surname + "' , '" + SC.BirthDate + "' , '" + SC.Gender + "' , '" + SC.FirstInitial + "' , '" + SC.SecondInitial + "' , '" + SC.BusRegistrationNo + "' , '" + SC.BusBusinessName + "' , '" + SC.Ha_IDNo + "' , '" + SC.Ha_FirstName + "' , '" + SC.Ha_SecondName + "' , '" + SC.Ha_Surname + "' , '" + SC.Ha_DeceasedStatus + "' , '" + SC.Ha_DeceasedDate + "' , '" + SC.Ha_CauseOfDeath + "' , " + SC.BillingTypeID + " , " + SC.BillingPrice + " , '" + SC.VoucherCode + "' , " + SC.DetailsViewedYN + " , '" + SC.DetailsViewedDate + "' , " + SC.Billable + " , '" + SC.XMLData + "' , '" + SC.XMLBonus + "' , " + SC.BonusIncluded + " , '" + SC.ExtraVarOutput1 + "' , '" + SC.ExtraVarOutput2 + "' , '" + SC.ExtraVarOutput3 + "' , " + SC.ExtraIntOutput1 + " , " + SC.ExtraIntOutput2 + " , '" + SC.CreatedByUser + "' , '" + SC.CreatedOnDate + "' , '" + SC.ChangedByUser + "' , '" + SC.ChangedOnDate + "', " + SC.ProductID + "," + SC.NLRDataIncluded;
            //string insertSQL = "EXEC sp_u_SubscriberEnquiryResult " + SC.SubscriberEnquiryResultID + " , " + SC.SubscriberEnquiryID + " , '" + SC.KeyType + "' , " + SC.KeyID + " , '" + SC.EnquiryResult + "' , '" + SC.SearchOutput + "' , '" + SC.IDNo + "' , '" + SC.PassportNo + "' , '" + SC.FirstName + "' , '" + SC.Surname + "' , '" + SC.BirthDate + "' , '" + SC.Gender + "' , '" + SC.FirstInitial + "' , '" + SC.SecondInitial + "' , '" + SC.BusRegistrationNo + "' , '" + SC.BusBusinessName + "' , " + SC.BillingTypeID + " , " + SC.BillingPrice + " , '" + SC.VoucherCode + "' , " + SC.DetailsViewedYN + " , '" + SC.DetailsViewedDate + "' , " + SC.Billable + " , '" + SC.XMLData + "' , '" + SC.XMLBonus + "' , " + SC.BonusIncluded + " , '" + SC.ExtraVarOutput1 + "' , '" + SC.ExtraVarOutput2 + "' , '" + SC.ExtraVarOutput3 + "' , " + SC.ExtraIntOutput1 + " , " + SC.ExtraIntOutput2 + " , '" + SC.CreatedByUser + "' , '" + SC.CreatedOnDate + "' , '" + SC.ChangedByUser + "' , '" + SC.ChangedOnDate + "' ";
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();
            cn.Close();

            sqlcmd.Dispose();
            return added;
        }

        public int UpdateMulipleSubscriberEnquiryResult(SqlConnection cn, Entity.SubscriberEnquiryResult SC)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            if (!string.IsNullOrEmpty(SC.Surname))
                SC.Surname = SC.Surname.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.FirstName))
                SC.FirstName = SC.FirstName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.SecondName))
                SC.SecondName = SC.SecondName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.Ha_FirstName))
                SC.Ha_FirstName = SC.Ha_FirstName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.Ha_SecondName))
                SC.Ha_SecondName = SC.Ha_SecondName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.Ha_Surname))
                SC.Ha_Surname = SC.Ha_Surname.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.BusBusinessName))
                SC.BusBusinessName = SC.BusBusinessName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.Ha_CauseOfDeath))
                SC.Ha_CauseOfDeath = SC.Ha_CauseOfDeath.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.XMLBonus))
                SC.XMLBonus = SC.XMLBonus.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.XMLData))
                SC.XMLData = SC.XMLData.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.SearchOutput))
                SC.SearchOutput = SC.SearchOutput.Replace("'", "''");
            
            string insertSQL = "EXEC sp_u_SubscriberEnquiryResult " + SC.SubscriberEnquiryResultID + " , " + SC.SubscriberEnquiryID + " , '" + SC.KeyType + "' , " + SC.KeyID + " , '" + SC.EnquiryResult + "' , '" + SC.SearchOutput + "' , '" + SC.IDNo + "' , '" + SC.PassportNo + "' , '" + SC.FirstName + "' , '" + SC.SecondName + "' , '" + SC.Surname + "' , '" + SC.BirthDate + "' , '" + SC.Gender + "' , '" + SC.FirstInitial + "' , '" + SC.SecondInitial + "' , '" + SC.BusRegistrationNo + "' , '" + SC.BusBusinessName + "' , '" + SC.Ha_IDNo + "' , '" + SC.Ha_FirstName + "' , '" + SC.Ha_SecondName + "' , '" + SC.Ha_Surname + "' , '" + SC.Ha_DeceasedStatus + "' , '" + SC.Ha_DeceasedDate + "' , '" + SC.Ha_CauseOfDeath + "' , " + SC.BillingTypeID + " , " + SC.BillingPrice + " , '" + SC.VoucherCode + "' , " + SC.DetailsViewedYN + " , '" + SC.DetailsViewedDate + "' , " + SC.Billable + " , '" + SC.XMLData + "' , '" + SC.XMLBonus + "' , " + SC.BonusIncluded + " , '" + SC.ExtraVarOutput1 + "' , '" + SC.ExtraVarOutput2 + "' , '" + SC.ExtraVarOutput3 + "' , " + SC.ExtraIntOutput1 + " , " + SC.ExtraIntOutput2 + " , '" + SC.CreatedByUser + "' , '" + SC.CreatedOnDate + "' , '" + SC.ChangedByUser + "' , '" + SC.ChangedOnDate + "', " + SC.ProductID + "," + SC.NLRDataIncluded;
            //string insertSQL = "EXEC sp_u_SubscriberEnquiryResult " + SC.SubscriberEnquiryResultID + " , " + SC.SubscriberEnquiryID + " , '" + SC.KeyType + "' , " + SC.KeyID + " , '" + SC.EnquiryResult + "' , '" + SC.SearchOutput + "' , '" + SC.IDNo + "' , '" + SC.PassportNo + "' , '" + SC.FirstName + "' , '" + SC.Surname + "' , '" + SC.BirthDate + "' , '" + SC.Gender + "' , '" + SC.FirstInitial + "' , '" + SC.SecondInitial + "' , '" + SC.BusRegistrationNo + "' , '" + SC.BusBusinessName + "' , " + SC.BillingTypeID + " , " + SC.BillingPrice + " , '" + SC.VoucherCode + "' , " + SC.DetailsViewedYN + " , '" + SC.DetailsViewedDate + "' , " + SC.Billable + " , '" + SC.XMLData + "' , '" + SC.XMLBonus + "' , " + SC.BonusIncluded + " , '" + SC.ExtraVarOutput1 + "' , '" + SC.ExtraVarOutput2 + "' , '" + SC.ExtraVarOutput3 + "' , " + SC.ExtraIntOutput1 + " , " + SC.ExtraIntOutput2 + " , '" + SC.CreatedByUser + "' , '" + SC.CreatedOnDate + "' , '" + SC.ChangedByUser + "' , '" + SC.ChangedOnDate + "' ";
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();

            sqlcmd.Dispose();
            cn.Close();

            return added;
        }

        public int UpdateBonusSubscriberEnquiryResult(string DBConnection, Entity.SubscriberEnquiryResult SC)
        {

            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = DBConnection;
            cn.Open();

            string insertSQL = "UPDATE SubscriberEnquiryResult SET DetailsViewedYN = 1,  DetailsViewedDate = GETDATE(), XMLData = '" + SC.XMLData + "' WHERE DetailsViewedYN = 0 AND SubscriberEnquiryResultID = " + SC.SubscriberEnquiryResultID.ToString();
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();
            cn.Close();

            sqlcmd.Dispose();
            cn.Close();

            return added;
        }

        public int UpdateBonusSubscriberEnquiryResult(SqlConnection cn, Entity.SubscriberEnquiryResult SC)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();
            
            string insertSQL = "UPDATE SubscriberEnquiryResult SET DetailsViewedYN = 1,  DetailsViewedDate = GETDATE(), XMLData = '" + SC.XMLData + "' WHERE DetailsViewedYN = 0 AND SubscriberEnquiryResultID = " + SC.SubscriberEnquiryResultID.ToString();
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();

            sqlcmd.Dispose();
            cn.Close();

            return added;
        }

        public DataSet GetSubscriberEnquiryResultDataSet(string DBConnection, int intSubscriberEnquiryID)
        {
            
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = DBConnection;
            cn.Open();

            string strSQL = "Select * from SubscriberEnquiryResult nolock Where SubscriberEnquiryID = " + intSubscriberEnquiryID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            sqlcmd.Dispose();
            sqlda.Dispose();
            cn.Close();

            return ds;
        }

        public DataSet GetSubscriberEnquiryResultDataSet(SqlConnection cn, int intSubscriberEnquiryID)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string strSQL = "Select * from SubscriberEnquiryResult nolock Where SubscriberEnquiryID = " + intSubscriberEnquiryID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            sqlda.Dispose();
            sqlcmd.Dispose();
            cn.Close();

            return ds;
        }

        public DataSet GetSubscriberEnquiryResultDataSetProduct(SqlConnection cn, int SubscriberID, int ProductID)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string strSQL = "Select top 50 * from SubscriberEnquirySearchResults where subscriberid = " + SubscriberID.ToString() + " and ProductID = " + ProductID.ToString() + " order by subscriberenquiryresultid desc";
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            sqlda.Dispose();
            sqlcmd.Dispose();

            cn.Close();

            return ds;
        }

        public DataSet GetSubscriberEnquiryResultDataSetSubscriber(SqlConnection cn, int SubscriberID)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string strSQL = "Select top 50 * from SubscriberEnquirySearchResults where subscriberid = " + SubscriberID.ToString() + " order by subscriberenquiryresultid desc"; ;
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            sqlda.Dispose();
            sqlcmd.Dispose();
            cn.Close();
            return ds;
        }

        public DataSet GetSubscriberEnquiryResultDataSetUser(SqlConnection cn, int SystemUserID, int ProductID)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string strSQL = "";

            if (ProductID == 0)
            {
                strSQL = "Select top 50 * from SubscriberEnquirySearchResults where systemuserid = " + SystemUserID.ToString() + " and ProductID = " + ProductID.ToString() + " order by subscriberenquiryresultid desc";
            }
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            sqlda.Dispose();
            sqlcmd.Dispose();
            cn.Close();

            return ds;
        }


        public List<Entity.SubscriberEnquiryResult> GetSubscriberEnquiryResultListObject(string DBConnection, int intSubscriberEnquiryID)
        {
            List<Entity.SubscriberEnquiryResult> Lsec = new List<Entity.SubscriberEnquiryResult>();
            Entity.SubscriberEnquiryResult sec = new Entity.SubscriberEnquiryResult();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = DBConnection;
            cn.Open();

            string strSQL = "Select * from SubscriberEnquiryResult nolock Where SubscriberEnquiryID = " + intSubscriberEnquiryID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                sec.SubscriberEnquiryResultID = int.Parse(r["SubscriberEnquiryResultID"].ToString());
                sec.SubscriberEnquiryID = int.Parse(r["SubscriberEnquiryID"].ToString());
                sec.Billable = bool.Parse(r["Billable"].ToString());
                sec.BillingPrice = double.Parse(r["BillingPrice"].ToString());
                if (!string.IsNullOrEmpty(r["BillingTypeID"].ToString())) { sec.BillingTypeID = int.Parse(r["BillingTypeID"].ToString()); }
                if (!string.IsNullOrEmpty(r["BirthDate"].ToString())) { sec.BirthDate = DateTime.Parse(r["BirthDate"].ToString()); }
                sec.BonusIncluded = bool.Parse(r["BonusIncluded"].ToString());
                if (!string.IsNullOrEmpty(r["BusBusinessName"].ToString())) { sec.BusBusinessName = r["BusBusinessName"].ToString(); }
                if (!string.IsNullOrEmpty(r["BusRegistrationNo"].ToString())) { sec.BusRegistrationNo = r["BusRegistrationNo"].ToString(); }
                sec.ChangedByUser = r["ChangedByUser"].ToString();
                if (!string.IsNullOrEmpty(r["ChangedOnDate"].ToString())) { sec.ChangedOnDate = DateTime.Parse(r["ChangedOnDate"].ToString()); }
                sec.CreatedByUser = r["CreatedByUser"].ToString();
                if (!string.IsNullOrEmpty(r["CreatedOnDate"].ToString())) { sec.CreatedOnDate = DateTime.Parse(r["CreatedOnDate"].ToString()); }
                if (!string.IsNullOrEmpty(r["DetailsViewedDate"].ToString())) {sec.DetailsViewedDate = DateTime.Parse(r["DetailsViewedDate"].ToString());}
                sec.DetailsViewedYN = bool.Parse(r["CreatedByUser"].ToString());
                sec.EnquiryResult = r["EnquiryResult"].ToString();
                sec.ExtraIntOutput1 = int.Parse(r["ExtraIntOutput1"].ToString());
                sec.ExtraIntOutput2 = int.Parse(r["ExtraIntOutput2"].ToString());
                if (!string.IsNullOrEmpty(r["ExtraVarOutput1"].ToString())) { sec.ExtraVarOutput1 = r["ExtraVarOutput1"].ToString(); }
                if (!string.IsNullOrEmpty(r["ExtraVarOutput2"].ToString())) {sec.ExtraVarOutput2 = r["ExtraVarOutput2"].ToString();}
                if (!string.IsNullOrEmpty(r["ExtraVarOutput3"].ToString())) {sec.ExtraVarOutput3 = r["ExtraVarOutput3"].ToString();}
                if (!string.IsNullOrEmpty(r["FirstInitial"].ToString())) {sec.FirstInitial = r["FirstInitial"].ToString();}
                if (!string.IsNullOrEmpty(r["FirstName"].ToString())) {sec.FirstName = r["FirstName"].ToString();}
                if (!string.IsNullOrEmpty(r["GenderInd"].ToString())) {sec.Gender = r["GenderInd"].ToString();}
                if (!string.IsNullOrEmpty(r["IDNo"].ToString())) { sec.IDNo = r["IDNo"].ToString(); }
                sec.KeyID = int.Parse(r["KeyID"].ToString());
                if (!string.IsNullOrEmpty(r["KeyType"].ToString())) {sec.KeyType = r["KeyType"].ToString();}
                if (!string.IsNullOrEmpty(r["PassportNo"].ToString())) {sec.PassportNo = r["PassportNo"].ToString();}
                if (!string.IsNullOrEmpty(r["Reference"].ToString())) {sec.Reference = r["Reference"].ToString();}
                if (!string.IsNullOrEmpty(r["SearchOutput"].ToString())) {sec.SearchOutput = r["SearchOutput"].ToString();}
                if (!string.IsNullOrEmpty(r["SecondInitial"].ToString())) {sec.SecondInitial = r["SecondInitial"].ToString();}
                if (!string.IsNullOrEmpty(r["SecondName"].ToString())) { sec.SecondName = r["SecondName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_FirstName"].ToString())) { sec.Ha_FirstName = r["Ha_FirstName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_IDNo"].ToString())) { sec.Ha_IDNo = r["Ha_IDNo"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_SecondName"].ToString())) { sec.Ha_SecondName = r["Ha_SecondName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_Surname"].ToString())) { sec.Ha_Surname = r["Ha_Surname"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_DeceasedStatus"].ToString())) { sec.Ha_DeceasedStatus = r["Ha_DeceasedStatus"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_DeceasedDate"].ToString())) { sec.Ha_DeceasedDate = r["Ha_DeceasedDate"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_CauseOfDeath"].ToString())) { sec.Ha_CauseOfDeath = r["Ha_CauseOfDeath"].ToString(); }
                if (!string.IsNullOrEmpty(r["Surname"].ToString())) {sec.Surname = r["Surname"].ToString();}
                if (!string.IsNullOrEmpty(r["VoucherCode"].ToString())) {sec.VoucherCode = r["VoucherCode"].ToString();}
                if (!string.IsNullOrEmpty(r["XMLBonus"].ToString())) {sec.XMLBonus = r["XMLBonus"].ToString();}
                if (!string.IsNullOrEmpty(r["XMLData"].ToString())) { sec.XMLData = r["XMLData"].ToString(); }
                if (!string.IsNullOrEmpty(r["ProductID"].ToString())) { sec.ProductID = Convert.ToInt16(r["ProductID"].ToString()); }
                Lsec.Add(sec);
            }

            sqlcmd.Dispose();
            sqlda.Dispose();
            ds.Dispose();
            cn.Close();

            return Lsec;

           
        }

        public List<Entity.SubscriberEnquiryResult> GetSubscriberEnquiryResultListObject(SqlConnection cn, int intSubscriberEnquiryID)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            List<Entity.SubscriberEnquiryResult> Lsec = new List<Entity.SubscriberEnquiryResult>();
            Entity.SubscriberEnquiryResult sec = new Entity.SubscriberEnquiryResult();
            

            string strSQL = "Select * from SubscriberEnquiryResult nolock Where SubscriberEnquiryID = " + intSubscriberEnquiryID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                sec.SubscriberEnquiryResultID = int.Parse(r["SubscriberEnquiryResultID"].ToString());
                sec.SubscriberEnquiryID = int.Parse(r["SubscriberEnquiryID"].ToString());
                sec.Billable = bool.Parse(r["Billable"].ToString());
                sec.BillingPrice = double.Parse(r["BillingPrice"].ToString());
                if (!string.IsNullOrEmpty(r["BillingTypeID"].ToString())) { sec.BillingTypeID = int.Parse(r["BillingTypeID"].ToString()); }
                if (!string.IsNullOrEmpty(r["BirthDate"].ToString())) { sec.BirthDate = DateTime.Parse(r["BirthDate"].ToString()); }
                sec.BonusIncluded = bool.Parse(r["BonusIncluded"].ToString());
                if (!string.IsNullOrEmpty(r["BusBusinessName"].ToString())) { sec.BusBusinessName = r["BusBusinessName"].ToString(); }
                if (!string.IsNullOrEmpty(r["BusRegistrationNo"].ToString())) { sec.BusRegistrationNo = r["BusRegistrationNo"].ToString(); }
                sec.ChangedByUser = r["ChangedByUser"].ToString();
                if (!string.IsNullOrEmpty(r["ChangedOnDate"].ToString())) { sec.ChangedOnDate = DateTime.Parse(r["ChangedOnDate"].ToString()); }
                sec.CreatedByUser = r["CreatedByUser"].ToString();
                if (!string.IsNullOrEmpty(r["CreatedOnDate"].ToString())) { sec.CreatedOnDate = DateTime.Parse(r["CreatedOnDate"].ToString()); }
                if (!string.IsNullOrEmpty(r["DetailsViewedDate"].ToString())) { sec.DetailsViewedDate = DateTime.Parse(r["DetailsViewedDate"].ToString()); }
                sec.DetailsViewedYN = bool.Parse(r["CreatedByUser"].ToString());
                sec.EnquiryResult = r["EnquiryResult"].ToString();
                sec.ExtraIntOutput1 = int.Parse(r["ExtraIntOutput1"].ToString());
                sec.ExtraIntOutput2 = int.Parse(r["ExtraIntOutput2"].ToString());
                if (!string.IsNullOrEmpty(r["ExtraVarOutput1"].ToString())) { sec.ExtraVarOutput1 = r["ExtraVarOutput1"].ToString(); }
                if (!string.IsNullOrEmpty(r["ExtraVarOutput2"].ToString())) { sec.ExtraVarOutput2 = r["ExtraVarOutput2"].ToString(); }
                if (!string.IsNullOrEmpty(r["ExtraVarOutput3"].ToString())) { sec.ExtraVarOutput3 = r["ExtraVarOutput3"].ToString(); }
                if (!string.IsNullOrEmpty(r["FirstInitial"].ToString())) { sec.FirstInitial = r["FirstInitial"].ToString(); }
                if (!string.IsNullOrEmpty(r["FirstName"].ToString())) { sec.FirstName = r["FirstName"].ToString(); }
                if (!string.IsNullOrEmpty(r["GenderInd"].ToString())) { sec.Gender = r["GenderInd"].ToString(); }
                if (!string.IsNullOrEmpty(r["IDNo"].ToString())) { sec.IDNo = r["IDNo"].ToString(); }
                sec.KeyID = int.Parse(r["KeyID"].ToString());
                if (!string.IsNullOrEmpty(r["KeyType"].ToString())) { sec.KeyType = r["KeyType"].ToString(); }
                if (!string.IsNullOrEmpty(r["PassportNo"].ToString())) { sec.PassportNo = r["PassportNo"].ToString(); }
                if (!string.IsNullOrEmpty(r["Reference"].ToString())) { sec.Reference = r["Reference"].ToString(); }
                if (!string.IsNullOrEmpty(r["SearchOutput"].ToString())) { sec.SearchOutput = r["SearchOutput"].ToString(); }
                if (!string.IsNullOrEmpty(r["SecondInitial"].ToString())) { sec.SecondInitial = r["SecondInitial"].ToString(); }
                if (!string.IsNullOrEmpty(r["SecondName"].ToString())) { sec.SecondName = r["SecondName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_FirstName"].ToString())) { sec.Ha_FirstName = r["Ha_FirstName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_IDNo"].ToString())) { sec.Ha_IDNo = r["Ha_IDNo"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_SecondName"].ToString())) { sec.Ha_SecondName = r["Ha_SecondName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_Surname"].ToString())) { sec.Ha_Surname = r["Ha_Surname"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_DeceasedStatus"].ToString())) { sec.Ha_DeceasedStatus = r["Ha_DeceasedStatus"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_DeceasedDate"].ToString())) { sec.Ha_DeceasedDate = r["Ha_DeceasedDate"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_CauseOfDeath"].ToString())) { sec.Ha_CauseOfDeath = r["Ha_CauseOfDeath"].ToString(); }
                if (!string.IsNullOrEmpty(r["Surname"].ToString())) { sec.Surname = r["Surname"].ToString(); }
                if (!string.IsNullOrEmpty(r["VoucherCode"].ToString())) { sec.VoucherCode = r["VoucherCode"].ToString(); }
                if (!string.IsNullOrEmpty(r["XMLBonus"].ToString())) { sec.XMLBonus = r["XMLBonus"].ToString(); }
                if (!string.IsNullOrEmpty(r["XMLData"].ToString())) { sec.XMLData = r["XMLData"].ToString(); }
                if (!string.IsNullOrEmpty(r["ProductID"].ToString())) { sec.ProductID = Convert.ToInt16(r["ProductID"].ToString()); }
                Lsec.Add(sec);
            }

            sqlcmd.Dispose();
            sqlda.Dispose();
            ds.Dispose();
            cn.Close();

            return Lsec;


        }

        public Entity.SubscriberEnquiryResult GetSubscriberEnquiryResultObject(string DBConnection, int intSubscriberEnquiryResultID)
        {
            Entity.SubscriberEnquiryResult sec = new Entity.SubscriberEnquiryResult();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = DBConnection;
            cn.Open();

            string strSQL = "Select * from SubscriberEnquiryResult nolock Where SubscriberEnquiryResultID = " + intSubscriberEnquiryResultID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                sec.SubscriberEnquiryResultID = int.Parse(r["SubscriberEnquiryResultID"].ToString());
                sec.SubscriberEnquiryID = int.Parse(r["SubscriberEnquiryID"].ToString());
                sec.Billable = bool.Parse(r["Billable"].ToString());
                sec.BillingPrice = double.Parse(r["BillingPrice"].ToString());
                if (!string.IsNullOrEmpty(r["BillingTypeID"].ToString())) { sec.BillingTypeID = int.Parse(r["BillingTypeID"].ToString()); }
                if (!string.IsNullOrEmpty(r["BirthDate"].ToString())) { sec.BirthDate = DateTime.Parse(r["BirthDate"].ToString()); }
                sec.BonusIncluded = bool.Parse(r["BonusIncluded"].ToString());
                if (!string.IsNullOrEmpty(r["BusBusinessName"].ToString())) { sec.BusBusinessName = r["BusBusinessName"].ToString(); }
                if (!string.IsNullOrEmpty(r["BusRegistrationNo"].ToString())) { sec.BusRegistrationNo = r["BusRegistrationNo"].ToString(); }
                sec.ChangedByUser = r["ChangedByUser"].ToString();
                if (!string.IsNullOrEmpty(r["ChangedOnDate"].ToString())) { sec.ChangedOnDate = DateTime.Parse(r["ChangedOnDate"].ToString()); }
                sec.CreatedByUser = r["CreatedByUser"].ToString();
                if (!string.IsNullOrEmpty(r["CreatedOnDate"].ToString())) { sec.CreatedOnDate = DateTime.Parse(r["CreatedOnDate"].ToString()); }
                if (!string.IsNullOrEmpty(r["DetailsViewedDate"].ToString())) { sec.DetailsViewedDate = DateTime.Parse(r["DetailsViewedDate"].ToString()); }
                sec.DetailsViewedYN = bool.Parse(r["DetailsViewedYN"].ToString());
                sec.EnquiryResult = r["EnquiryResultInd"].ToString();
                sec.ExtraIntOutput1 = int.Parse(r["ExtraIntOutput1"].ToString());
                sec.ExtraIntOutput2 = int.Parse(r["ExtraIntOutput2"].ToString());
                if (!string.IsNullOrEmpty(r["ExtraVarOutput1"].ToString())) { sec.ExtraVarOutput1 = r["ExtraVarOutput1"].ToString(); }
                if (!string.IsNullOrEmpty(r["ExtraVarOutput2"].ToString())) { sec.ExtraVarOutput2 = r["ExtraVarOutput2"].ToString(); }
                if (!string.IsNullOrEmpty(r["ExtraVarOutput3"].ToString())) { sec.ExtraVarOutput3 = r["ExtraVarOutput3"].ToString(); }
                if (!string.IsNullOrEmpty(r["FirstInitial"].ToString())) { sec.FirstInitial = r["FirstInitial"].ToString(); }
                if (!string.IsNullOrEmpty(r["FirstName"].ToString())) { sec.FirstName = r["FirstName"].ToString(); }
                if (!string.IsNullOrEmpty(r["GenderInd"].ToString())) { sec.Gender = r["GenderInd"].ToString(); }
                if (!string.IsNullOrEmpty(r["IDNo"].ToString())) { sec.IDNo = r["IDNo"].ToString(); }
                sec.KeyID = int.Parse(r["KeyID"].ToString());
                if (!string.IsNullOrEmpty(r["KeyType"].ToString())) { sec.KeyType = r["KeyType"].ToString(); }
                if (!string.IsNullOrEmpty(r["PassportNo"].ToString())) { sec.PassportNo = r["PassportNo"].ToString(); }
                if (!string.IsNullOrEmpty(r["Reference"].ToString())) { sec.Reference = r["Reference"].ToString(); }
                if (!string.IsNullOrEmpty(r["SearchOutput"].ToString())) { sec.SearchOutput = r["SearchOutput"].ToString(); }
                if (!string.IsNullOrEmpty(r["SecondInitial"].ToString())) { sec.SecondInitial = r["SecondInitial"].ToString(); }
                if (!string.IsNullOrEmpty(r["SecondName"].ToString())) { sec.SecondName = r["SecondName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_FirstName"].ToString())) { sec.Ha_FirstName = r["Ha_FirstName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_IDNo"].ToString())) { sec.Ha_IDNo = r["Ha_IDNo"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_SecondName"].ToString())) { sec.Ha_SecondName = r["Ha_SecondName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_Surname"].ToString())) { sec.Ha_Surname = r["Ha_Surname"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_DeceasedStatus"].ToString())) { sec.Ha_DeceasedStatus = r["Ha_DeceasedStatus"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_DeceasedDate"].ToString())) { sec.Ha_DeceasedDate = r["Ha_DeceasedDate"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_CauseOfDeath"].ToString())) { sec.Ha_CauseOfDeath = r["Ha_CauseOfDeath"].ToString(); }
                if (!string.IsNullOrEmpty(r["Surname"].ToString())) { sec.Surname = r["Surname"].ToString(); }
                if (!string.IsNullOrEmpty(r["VoucherCode"].ToString())) { sec.VoucherCode = r["VoucherCode"].ToString(); }
                if (!string.IsNullOrEmpty(r["XMLBonus"].ToString())) { sec.XMLBonus = r["XMLBonus"].ToString(); }
                if (!string.IsNullOrEmpty(r["XMLData"].ToString())) { sec.XMLData = r["XMLData"].ToString(); }
                if (!string.IsNullOrEmpty(r["ProductID"].ToString())) { sec.ProductID = Convert.ToInt16(r["ProductID"].ToString()); }
            }

            sqlcmd.Dispose();
            sqlda.Dispose();
            ds.Dispose();
            cn.Close();

            return sec;


        }

        public Entity.SubscriberEnquiryResult GetSubscriberEnquiryResultObject(SqlConnection cn, int intSubscriberEnquiryResultID)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            Entity.SubscriberEnquiryResult sec = new Entity.SubscriberEnquiryResult();

            string strSQL = "Select * from SubscriberEnquiryResult nolock Where SubscriberEnquiryResultID = " + intSubscriberEnquiryResultID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                sec.SubscriberEnquiryResultID = int.Parse(r["SubscriberEnquiryResultID"].ToString());
                sec.SubscriberEnquiryID = int.Parse(r["SubscriberEnquiryID"].ToString());
                sec.Billable = bool.Parse(r["Billable"].ToString());
                sec.BillingPrice = double.Parse(r["BillingPrice"].ToString());
                if (!string.IsNullOrEmpty(r["BillingTypeID"].ToString())) { sec.BillingTypeID = int.Parse(r["BillingTypeID"].ToString()); }
                if (!string.IsNullOrEmpty(r["BirthDate"].ToString())) { sec.BirthDate = DateTime.Parse(r["BirthDate"].ToString()); }
                sec.BonusIncluded = bool.Parse(r["BonusIncluded"].ToString());
                if (!string.IsNullOrEmpty(r["BusBusinessName"].ToString())) { sec.BusBusinessName = r["BusBusinessName"].ToString(); }
                if (!string.IsNullOrEmpty(r["BusRegistrationNo"].ToString())) { sec.BusRegistrationNo = r["BusRegistrationNo"].ToString(); }
                sec.ChangedByUser = r["ChangedByUser"].ToString();
                if (!string.IsNullOrEmpty(r["ChangedOnDate"].ToString())) { sec.ChangedOnDate = DateTime.Parse(r["ChangedOnDate"].ToString()); }
                sec.CreatedByUser = r["CreatedByUser"].ToString();
                if (!string.IsNullOrEmpty(r["CreatedOnDate"].ToString())) { sec.CreatedOnDate = DateTime.Parse(r["CreatedOnDate"].ToString()); }
                if (!string.IsNullOrEmpty(r["DetailsViewedDate"].ToString())) { sec.DetailsViewedDate = DateTime.Parse(r["DetailsViewedDate"].ToString()); }
                sec.DetailsViewedYN = bool.Parse(r["DetailsViewedYN"].ToString());
                sec.EnquiryResult = r["EnquiryResultInd"].ToString();
                sec.ExtraIntOutput1 = int.Parse(r["ExtraIntOutput1"].ToString());
                sec.ExtraIntOutput2 = int.Parse(r["ExtraIntOutput2"].ToString());
                if (!string.IsNullOrEmpty(r["ExtraVarOutput1"].ToString())) { sec.ExtraVarOutput1 = r["ExtraVarOutput1"].ToString(); }
                if (!string.IsNullOrEmpty(r["ExtraVarOutput2"].ToString())) { sec.ExtraVarOutput2 = r["ExtraVarOutput2"].ToString(); }
                if (!string.IsNullOrEmpty(r["ExtraVarOutput3"].ToString())) { sec.ExtraVarOutput3 = r["ExtraVarOutput3"].ToString(); }
                if (!string.IsNullOrEmpty(r["FirstInitial"].ToString())) { sec.FirstInitial = r["FirstInitial"].ToString(); }
                if (!string.IsNullOrEmpty(r["FirstName"].ToString())) { sec.FirstName = r["FirstName"].ToString(); }
                if (!string.IsNullOrEmpty(r["GenderInd"].ToString())) { sec.Gender = r["GenderInd"].ToString(); }
                if (!string.IsNullOrEmpty(r["IDNo"].ToString())) { sec.IDNo = r["IDNo"].ToString(); }
                sec.KeyID = int.Parse(r["KeyID"].ToString());
                if (!string.IsNullOrEmpty(r["KeyType"].ToString())) { sec.KeyType = r["KeyType"].ToString(); }
                if (!string.IsNullOrEmpty(r["PassportNo"].ToString())) { sec.PassportNo = r["PassportNo"].ToString(); }
                if (!string.IsNullOrEmpty(r["Reference"].ToString())) { sec.Reference = r["Reference"].ToString(); }
                if (!string.IsNullOrEmpty(r["SearchOutput"].ToString())) { sec.SearchOutput = r["SearchOutput"].ToString(); }
                if (!string.IsNullOrEmpty(r["SecondInitial"].ToString())) { sec.SecondInitial = r["SecondInitial"].ToString(); }
                if (!string.IsNullOrEmpty(r["SecondName"].ToString())) { sec.SecondName = r["SecondName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_FirstName"].ToString())) { sec.Ha_FirstName = r["Ha_FirstName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_IDNo"].ToString())) { sec.Ha_IDNo = r["Ha_IDNo"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_SecondName"].ToString())) { sec.Ha_SecondName = r["Ha_SecondName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_Surname"].ToString())) { sec.Ha_Surname = r["Ha_Surname"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_DeceasedStatus"].ToString())) { sec.Ha_DeceasedStatus = r["Ha_DeceasedStatus"].ToString(); }
                //if (!string.IsNullOrEmpty(r["Ha_DeceasedDate"].ToString())) { sec.Ha_DeceasedDate = r["Ha_DeceasedDate"].ToString(); }
                if (!String.IsNullOrEmpty(r["Ha_DeceasedDate"].ToString()))
                {
                    try
                    {
                        sec.Ha_DeceasedDate = Convert.ToDateTime(r["Ha_DeceasedDate"].ToString()).ToString("yyyy/MM/dd");
                    }
                    catch
                    {
                        sec.Ha_DeceasedDate = r["Ha_DeceasedDate"].ToString();
                    }
                }
                if (!string.IsNullOrEmpty(r["Ha_CauseOfDeath"].ToString())) { sec.Ha_CauseOfDeath = r["Ha_CauseOfDeath"].ToString(); }
                if (!string.IsNullOrEmpty(r["Surname"].ToString())) { sec.Surname = r["Surname"].ToString(); }
                if (!string.IsNullOrEmpty(r["VoucherCode"].ToString())) { sec.VoucherCode = r["VoucherCode"].ToString(); }
                if (!string.IsNullOrEmpty(r["XMLBonus"].ToString())) { sec.XMLBonus = r["XMLBonus"].ToString(); }
                if (!string.IsNullOrEmpty(r["XMLData"].ToString())) { sec.XMLData = r["XMLData"].ToString(); }
                if (!string.IsNullOrEmpty(r["ProductID"].ToString())) { sec.ProductID = Convert.ToInt16(r["ProductID"].ToString()); }
            }

            sqlcmd.Dispose();
            sqlda.Dispose();
            ds.Dispose();
            cn.Close();

            return sec;

        }

        public Entity.SubscriberEnquiryResult GetSubscriberAuthenticationEnquiryResultObject(SqlConnection cn, int intSubscriberAuthenticationID)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            Entity.SubscriberEnquiryResult sec = new Entity.SubscriberEnquiryResult();

            string strSQL = "Select * from SubscriberEnquiryResult nolock Where ExtraIntOutput1 = " + intSubscriberAuthenticationID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                sec.SubscriberEnquiryResultID = int.Parse(r["SubscriberEnquiryResultID"].ToString());
                sec.SubscriberEnquiryID = int.Parse(r["SubscriberEnquiryID"].ToString());
                sec.Billable = bool.Parse(r["Billable"].ToString());
                sec.BillingPrice = double.Parse(r["BillingPrice"].ToString());
                if (!string.IsNullOrEmpty(r["BillingTypeID"].ToString())) { sec.BillingTypeID = int.Parse(r["BillingTypeID"].ToString()); }
                if (!string.IsNullOrEmpty(r["BirthDate"].ToString())) { sec.BirthDate = DateTime.Parse(r["BirthDate"].ToString()); }
                sec.BonusIncluded = bool.Parse(r["BonusIncluded"].ToString());
                if (!string.IsNullOrEmpty(r["BusBusinessName"].ToString())) { sec.BusBusinessName = r["BusBusinessName"].ToString(); }
                if (!string.IsNullOrEmpty(r["BusRegistrationNo"].ToString())) { sec.BusRegistrationNo = r["BusRegistrationNo"].ToString(); }
                sec.ChangedByUser = r["ChangedByUser"].ToString();
                if (!string.IsNullOrEmpty(r["ChangedOnDate"].ToString())) { sec.ChangedOnDate = DateTime.Parse(r["ChangedOnDate"].ToString()); }
                sec.CreatedByUser = r["CreatedByUser"].ToString();
                if (!string.IsNullOrEmpty(r["CreatedOnDate"].ToString())) { sec.CreatedOnDate = DateTime.Parse(r["CreatedOnDate"].ToString()); }
                if (!string.IsNullOrEmpty(r["DetailsViewedDate"].ToString())) { sec.DetailsViewedDate = DateTime.Parse(r["DetailsViewedDate"].ToString()); }
                sec.DetailsViewedYN = bool.Parse(r["DetailsViewedYN"].ToString());
                sec.EnquiryResult = r["EnquiryResultInd"].ToString();
                sec.ExtraIntOutput1 = int.Parse(r["ExtraIntOutput1"].ToString());
                sec.ExtraIntOutput2 = int.Parse(r["ExtraIntOutput2"].ToString());
                if (!string.IsNullOrEmpty(r["ExtraVarOutput1"].ToString())) { sec.ExtraVarOutput1 = r["ExtraVarOutput1"].ToString(); }
                if (!string.IsNullOrEmpty(r["ExtraVarOutput2"].ToString())) { sec.ExtraVarOutput2 = r["ExtraVarOutput2"].ToString(); }
                if (!string.IsNullOrEmpty(r["ExtraVarOutput3"].ToString())) { sec.ExtraVarOutput3 = r["ExtraVarOutput3"].ToString(); }
                if (!string.IsNullOrEmpty(r["FirstInitial"].ToString())) { sec.FirstInitial = r["FirstInitial"].ToString(); }
                if (!string.IsNullOrEmpty(r["FirstName"].ToString())) { sec.FirstName = r["FirstName"].ToString(); }
                if (!string.IsNullOrEmpty(r["GenderInd"].ToString())) { sec.Gender = r["GenderInd"].ToString(); }
                if (!string.IsNullOrEmpty(r["IDNo"].ToString())) { sec.IDNo = r["IDNo"].ToString(); }
                sec.KeyID = int.Parse(r["KeyID"].ToString());
                if (!string.IsNullOrEmpty(r["KeyType"].ToString())) { sec.KeyType = r["KeyType"].ToString(); }
                if (!string.IsNullOrEmpty(r["PassportNo"].ToString())) { sec.PassportNo = r["PassportNo"].ToString(); }
                if (!string.IsNullOrEmpty(r["Reference"].ToString())) { sec.Reference = r["Reference"].ToString(); }
                if (!string.IsNullOrEmpty(r["SearchOutput"].ToString())) { sec.SearchOutput = r["SearchOutput"].ToString(); }
                if (!string.IsNullOrEmpty(r["SecondInitial"].ToString())) { sec.SecondInitial = r["SecondInitial"].ToString(); }
                if (!string.IsNullOrEmpty(r["SecondName"].ToString())) { sec.SecondName = r["SecondName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_FirstName"].ToString())) { sec.Ha_FirstName = r["Ha_FirstName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_IDNo"].ToString())) { sec.Ha_IDNo = r["Ha_IDNo"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_SecondName"].ToString())) { sec.Ha_SecondName = r["Ha_SecondName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_Surname"].ToString())) { sec.Ha_Surname = r["Ha_Surname"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_DeceasedStatus"].ToString())) { sec.Ha_DeceasedStatus = r["Ha_DeceasedStatus"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_DeceasedDate"].ToString())) { sec.Ha_DeceasedDate = r["Ha_DeceasedDate"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_CauseOfDeath"].ToString())) { sec.Ha_CauseOfDeath = r["Ha_CauseOfDeath"].ToString(); }
                if (!string.IsNullOrEmpty(r["Surname"].ToString())) { sec.Surname = r["Surname"].ToString(); }
                if (!string.IsNullOrEmpty(r["VoucherCode"].ToString())) { sec.VoucherCode = r["VoucherCode"].ToString(); }
                if (!string.IsNullOrEmpty(r["XMLBonus"].ToString())) { sec.XMLBonus = r["XMLBonus"].ToString(); }
                if (!string.IsNullOrEmpty(r["XMLData"].ToString())) { sec.XMLData = r["XMLData"].ToString(); }
                if (!string.IsNullOrEmpty(r["ProductID"].ToString())) { sec.ProductID = Convert.ToInt16(r["ProductID"].ToString()); }
            }

            sqlcmd.Dispose();
            sqlda.Dispose();
            ds.Dispose();
            cn.Close();

            return sec;

        }


    }
}
