﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalEnquiry.Data
{
   public class IdecoBankNames
    {
       public DataSet GetIdecoBankNamesDataSet(SqlConnection cn)
       {
           if (cn.State == ConnectionState.Closed)
               cn.Open();

           string strSQL = "Select BankName,Code from IdecoBankNames nolock ";
           SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
           SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
           DataSet ds = new DataSet();
           sqlda.Fill(ds);

           sqlda.Dispose();
           sqlcmd.Dispose();
           cn.Close();
           return ds;
       }
    }
}
