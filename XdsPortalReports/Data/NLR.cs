﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalEnquiry.Data
{
    public class NLR
    {
        public void ExecValidations(SqlConnection cn, XDSPortalLibrary.Entity_Layer.NLRDailyRegistrations NReg)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            SqlCommand objcmd = new SqlCommand("spValidate", cn);
            objcmd.CommandType = CommandType.StoredProcedure;

            objcmd.Parameters.AddWithValue("@TransactionID", "MFRC");
            objcmd.Parameters.AddWithValue("@TransactionType", 4);
            objcmd.Parameters.AddWithValue("@EnquiryReason1", NReg.EnquiryReason1);
            objcmd.Parameters.AddWithValue("@SubscriberCode", NReg.SubscriberCode);
            objcmd.Parameters.AddWithValue("@SubscriberPassword", NReg.SubscriberPassword);
            objcmd.Parameters.AddWithValue("@SubscriberGroupID", NReg.SubscriberGroupID);
            objcmd.Parameters.AddWithValue("@SubscriberOperatorID", NReg.SubscriberOperatorID);
            objcmd.Parameters.AddWithValue("@SubscriberOperatorPassword", NReg.SubscriberOperatorPassword);
            objcmd.Parameters.AddWithValue("@TransactionStatus", NReg.TransactionStatus);
            objcmd.Parameters.AddWithValue("@ErrorCode", NReg.ErrorCode);
            objcmd.Parameters.AddWithValue("@MatchedRecordsFound", NReg.MatchedRecordsFound);
            objcmd.Parameters.AddWithValue("@ConsIdentificationNo", NReg.ConsIdentificationNo);
            objcmd.Parameters.AddWithValue("@DateEnquirySent", NReg.DateEnquirySent);
            objcmd.Parameters.AddWithValue("@TimeEnquirySent", NReg.TimeEnquirySent);
            objcmd.Parameters.AddWithValue("@EchoIdentityNo", NReg.EchoIdentityNo);
            objcmd.Parameters.AddWithValue("@EchoUniqueClientNo", NReg.EchoUniqueClientNo);
            objcmd.Parameters.AddWithValue("@MaxNoOfInquiries", NReg.MaxNoOfInquiries);
            objcmd.Parameters.AddWithValue("@MaxNoOfNLRAccounts", NReg.MaxNoOfNLRAccounts);
            objcmd.Parameters.AddWithValue("@MaxNoOfAssAddresses", NReg.MaxNoOfAssAddresses);
            objcmd.Parameters.AddWithValue("@MaxNoOfOtherEmployers", NReg.MaxNoOfOtherEmployers);
            objcmd.Parameters.AddWithValue("@MaxNoOfAssHomeTelNos", NReg.MaxNoOfAssHomeTelNos);
            objcmd.Parameters.AddWithValue("@MaxNoOfAssBusTelNos", NReg.MaxNoOfAssBusTelNos);
            objcmd.Parameters.AddWithValue("@MaxNoOfAssCellNos", NReg.MaxNoOfAssCellNos);
            objcmd.Parameters.AddWithValue("@MaxNoOfAssOtherTelNos", NReg.MaxNoOfAssOtherTelNos);
            objcmd.Parameters.AddWithValue("@LoanRegNo", NReg.LoanRegNo);
            objcmd.Parameters.AddWithValue("@SupplierRefNo", NReg.SupplierRefNo);
            objcmd.Parameters.AddWithValue("@EnquiryRefNo", NReg.EnquiryRefNo);
            objcmd.Parameters.AddWithValue("@ConsSurname", NReg.ConsSurname);
            objcmd.Parameters.AddWithValue("@Forename1", NReg.Forename1);
            objcmd.Parameters.AddWithValue("@Forename2", NReg.Forename2);
            objcmd.Parameters.AddWithValue("@Forename3", NReg.Forename3);
            objcmd.Parameters.AddWithValue("@SpouseCurrSurname", NReg.SpouseCurrSurname);
            objcmd.Parameters.AddWithValue("@SpouseForename1", NReg.SpouseForename1);
            objcmd.Parameters.AddWithValue("@SpouseForename2", NReg.SpouseForename2);
            objcmd.Parameters.AddWithValue("@ConsBirthDate", NReg.ConsBirthDate);
            objcmd.Parameters.AddWithValue("@ConsIdentityNo", NReg.ConsIdentityNo);
            objcmd.Parameters.AddWithValue("@CurrAddrLn1", NReg.CurrAddrLn1);
            objcmd.Parameters.AddWithValue("@CurrAddrLn2", NReg.CurrAddrLn2);
            objcmd.Parameters.AddWithValue("@CurrAddrLn3", NReg.CurrAddrLn3);
            objcmd.Parameters.AddWithValue("@CurrAddrLn4", NReg.CurrAddrLn4);
            objcmd.Parameters.AddWithValue("@CurrAddressPostalCode", NReg.CurrAddressPostalCode);
            objcmd.Parameters.AddWithValue("@PeriodAtCurrAddress", NReg.PeriodAtCurrAddress);
            objcmd.Parameters.AddWithValue("@OtherAddrLn1", NReg.OtherAddrLn1);
            objcmd.Parameters.AddWithValue("@OtherAddrLn2", NReg.OtherAddrLn2);
            objcmd.Parameters.AddWithValue("@OtherAddrLn3", NReg.OtherAddrLn3);
            objcmd.Parameters.AddWithValue("@OtherAddrLn4", NReg.OtherAddrLn4);
            objcmd.Parameters.AddWithValue("@OtherAddressPostalCode", NReg.OtherAddressPostalCode);
            objcmd.Parameters.AddWithValue("@OwnerTenantAtCurrAddress", NReg.OwnerTenantAtCurrAddress);
            objcmd.Parameters.AddWithValue("@ConsOccupation", NReg.ConsOccupation);
            objcmd.Parameters.AddWithValue("@ConsEmployer", NReg.ConsEmployer);
            objcmd.Parameters.AddWithValue("@ConsMaidenName", NReg.ConsMaidenName);
            objcmd.Parameters.AddWithValue("@ConsAliasName", NReg.ConsAliasName);
            objcmd.Parameters.AddWithValue("@Gender", NReg.Gender);
            objcmd.Parameters.AddWithValue("@ConsMaritalStatus", NReg.ConsMaritalStatus);
            objcmd.Parameters.AddWithValue("@ConsHomeTelDiallingCode", NReg.ConsHomeTelDiallingCode);
            objcmd.Parameters.AddWithValue("@ConsHomeTelNo", NReg.ConsHomeTelNo);
            objcmd.Parameters.AddWithValue("@ConsWorkTelDiallingCode", NReg.ConsWorkTelDiallingCode);
            objcmd.Parameters.AddWithValue("@ConsWorkTelNo", NReg.ConsWorkTelNo);
            objcmd.Parameters.AddWithValue("@ConsCellularTelNo", NReg.ConsCellularTelNo);
            objcmd.Parameters.AddWithValue("@LoanAmt1", NReg.LoanAmt1);
            objcmd.Parameters.AddWithValue("@InstalmentAmt", NReg.InstalmentAmt);
            objcmd.Parameters.AddWithValue("@MonthlySalary", NReg.MonthlySalary);
            objcmd.Parameters.AddWithValue("@SalaryFrequency", NReg.SalaryFrequency);
            objcmd.Parameters.AddWithValue("@RepaymentPeriod1", NReg.RepaymentPeriod1);
            objcmd.Parameters.AddWithValue("@EnquiryReason2", NReg.EnquiryReason2);
            objcmd.Parameters.AddWithValue("@BranchCode", NReg.BranchCode);
            objcmd.Parameters.AddWithValue("@AccountNo", NReg.AccountNo);
            objcmd.Parameters.AddWithValue("@SubAccountNo", NReg.SubAccountNo);
            objcmd.Parameters.AddWithValue("@LoanType", NReg.LoanType);
            objcmd.Parameters.AddWithValue("@DateLoanDisbursed", NReg.DateLoanDisbursed);
            objcmd.Parameters.AddWithValue("@LoanAmt2", NReg.LoanAmt2);
            objcmd.Parameters.AddWithValue("@LoanAmtBalanceIndicator", NReg.LoanAmtBalanceIndicator);
            objcmd.Parameters.AddWithValue("@CurrBalance", NReg.CurrBalance);
            objcmd.Parameters.AddWithValue("@CurrBalanceIndicator", NReg.CurrBalanceIndicator);
            objcmd.Parameters.AddWithValue("@MonthlyInstalment", NReg.MonthlyInstalment);
            objcmd.Parameters.AddWithValue("@LoadIndicator", NReg.LoadIndicator);
            objcmd.Parameters.AddWithValue("@RepaymentPeriod2", NReg.RepaymentPeriod2);
            objcmd.Parameters.AddWithValue("@LoanPurpose", NReg.LoanPurpose);
            objcmd.Parameters.AddWithValue("@TotalAmtRepayable", NReg.TotalAmtRepayable);
            objcmd.Parameters.AddWithValue("@InterestType", NReg.InterestType);
            objcmd.Parameters.AddWithValue("@AnnualRateForTotalChargeOfCredit", NReg.AnnualRateForTotalChargeOfCredit);
            objcmd.Parameters.AddWithValue("@RandValueOfInterestCharges", NReg.RandValueOfInterestCharges);
            objcmd.Parameters.AddWithValue("@RandValueOfTotalChargeOfCredit", NReg.RandValueOfTotalChargeOfCredit);
            objcmd.Parameters.AddWithValue("@SettlementAmt", NReg.SettlementAmt);
            objcmd.Parameters.AddWithValue("@ReAdvanceIndicator", NReg.ReAdvanceIndicator);
            objcmd.Parameters.AddWithValue("@PassportNo", NReg.PassportNo);
            objcmd.Parameters.AddWithValue("@StatusCode", string.Empty);
            objcmd.Parameters.AddWithValue("@DateOFClosure", string.Empty);

            objcmd.ExecuteNonQuery();

            if (cn.State == ConnectionState.Open)
                cn.Close();

        }

        public int InsertLog(SqlConnection cn, XDSPortalLibrary.Entity_Layer.NLRDailyRegistrations NReg)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            SqlCommand objcmd = new SqlCommand("sp_I_NLRDailyRegistrationsLog", cn);
            objcmd.CommandType = CommandType.StoredProcedure;

            objcmd.Parameters.AddWithValue("@EnquirySubscriberID",NReg.EnquirySubscriberID);
            objcmd.Parameters.AddWithValue("@Username", NReg.Username);
            objcmd.Parameters.AddWithValue("@Subscribername", NReg.SubscriberName);
            objcmd.Parameters.AddWithValue("@SystemUserID", NReg.SystemUserID);
            objcmd.Parameters.AddWithValue("@StatusInd", NReg.StatusInd);            
            objcmd.Parameters.AddWithValue("@TransactionID", "MFRC");
            objcmd.Parameters.AddWithValue("@TransactionType", 4);
            objcmd.Parameters.AddWithValue("@EnquiryReason1", NReg.EnquiryReason1);
            objcmd.Parameters.AddWithValue("@SubscriberCode", NReg.SubscriberCode);
            objcmd.Parameters.AddWithValue("@SubscriberPassword", NReg.SubscriberPassword);
            objcmd.Parameters.AddWithValue("@SubscriberGroupID", NReg.SubscriberGroupID);
            objcmd.Parameters.AddWithValue("@SubscriberOperatorID", NReg.SubscriberOperatorID);
            objcmd.Parameters.AddWithValue("@SubscriberOperatorPassword", NReg.SubscriberOperatorPassword);
            objcmd.Parameters.AddWithValue("@TransactionStatus", NReg.TransactionStatus);
            objcmd.Parameters.AddWithValue("@ErrorCode", NReg.ErrorCode);
            objcmd.Parameters.AddWithValue("@MatchedRecordsFound", NReg.MatchedRecordsFound);
            objcmd.Parameters.AddWithValue("@ConsIdentificationNo", NReg.ConsIdentificationNo);
            objcmd.Parameters.AddWithValue("@DateEnquirySent", NReg.DateEnquirySent);
            objcmd.Parameters.AddWithValue("@TimeEnquirySent", NReg.TimeEnquirySent);
            objcmd.Parameters.AddWithValue("@EchoIdentityNo", NReg.EchoIdentityNo);
            objcmd.Parameters.AddWithValue("@EchoUniqueClientNo", NReg.EchoUniqueClientNo);
            objcmd.Parameters.AddWithValue("@MaxNoOfInquiries", NReg.MaxNoOfInquiries);
            objcmd.Parameters.AddWithValue("@MaxNoOfNLRAccounts", NReg.MaxNoOfNLRAccounts);
            objcmd.Parameters.AddWithValue("@MaxNoOfAssAddresses", NReg.MaxNoOfAssAddresses);
            objcmd.Parameters.AddWithValue("@MaxNoOfOtherEmployers", NReg.MaxNoOfOtherEmployers);
            objcmd.Parameters.AddWithValue("@MaxNoOfAssHomeTelNos", NReg.MaxNoOfAssHomeTelNos);
            objcmd.Parameters.AddWithValue("@MaxNoOfAssBusTelNos", NReg.MaxNoOfAssBusTelNos);
            objcmd.Parameters.AddWithValue("@MaxNoOfAssCellNos", NReg.MaxNoOfAssCellNos);
            objcmd.Parameters.AddWithValue("@MaxNoOfAssOtherTelNos", NReg.MaxNoOfAssOtherTelNos);
            objcmd.Parameters.AddWithValue("@LoanRegNo", NReg.LoanRegNo);
            objcmd.Parameters.AddWithValue("@SupplierRefNo", NReg.SupplierRefNo);
            objcmd.Parameters.AddWithValue("@EnquiryRefNo", NReg.EnquiryRefNo);
            objcmd.Parameters.AddWithValue("@ConsSurname", NReg.ConsSurname);
            objcmd.Parameters.AddWithValue("@Forename1", NReg.Forename1);
            objcmd.Parameters.AddWithValue("@Forename2", NReg.Forename2);
            objcmd.Parameters.AddWithValue("@Forename3", NReg.Forename3);
            objcmd.Parameters.AddWithValue("@SpouseCurrSurname", NReg.SpouseCurrSurname);
            objcmd.Parameters.AddWithValue("@SpouseForename1", NReg.SpouseForename1);
            objcmd.Parameters.AddWithValue("@SpouseForename2", NReg.SpouseForename2);
            objcmd.Parameters.AddWithValue("@ConsBirthDate", NReg.ConsBirthDate);
            objcmd.Parameters.AddWithValue("@ConsIdentityNo", NReg.ConsIdentityNo);
            objcmd.Parameters.AddWithValue("@CurrAddrLn1", NReg.CurrAddrLn1);
            objcmd.Parameters.AddWithValue("@CurrAddrLn2", NReg.CurrAddrLn2);
            objcmd.Parameters.AddWithValue("@CurrAddrLn3", NReg.CurrAddrLn3);
            objcmd.Parameters.AddWithValue("@CurrAddrLn4", NReg.CurrAddrLn4);
            objcmd.Parameters.AddWithValue("@CurrAddressPostalCode", NReg.CurrAddressPostalCode);
            objcmd.Parameters.AddWithValue("@PeriodAtCurrAddress", NReg.PeriodAtCurrAddress);
            objcmd.Parameters.AddWithValue("@OtherAddrLn1", NReg.OtherAddrLn1);
            objcmd.Parameters.AddWithValue("@OtherAddrLn2", NReg.OtherAddrLn2);
            objcmd.Parameters.AddWithValue("@OtherAddrLn3", NReg.OtherAddrLn3);
            objcmd.Parameters.AddWithValue("@OtherAddrLn4", NReg.OtherAddrLn4);
            objcmd.Parameters.AddWithValue("@OtherAddressPostalCode", NReg.OtherAddressPostalCode);
            objcmd.Parameters.AddWithValue("@OwnerTenantAtCurrAddress", NReg.OwnerTenantAtCurrAddress);
            objcmd.Parameters.AddWithValue("@ConsOccupation", NReg.ConsOccupation);
            objcmd.Parameters.AddWithValue("@ConsEmployer", NReg.ConsEmployer);
            objcmd.Parameters.AddWithValue("@ConsMaidenName", NReg.ConsMaidenName);
            objcmd.Parameters.AddWithValue("@ConsAliasName", NReg.ConsAliasName);
            objcmd.Parameters.AddWithValue("@Gender", NReg.Gender);
            objcmd.Parameters.AddWithValue("@ConsMaritalStatus", NReg.ConsMaritalStatus);
            objcmd.Parameters.AddWithValue("@ConsHomeTelDiallingCode", NReg.ConsHomeTelDiallingCode);
            objcmd.Parameters.AddWithValue("@ConsHomeTelNo", NReg.ConsHomeTelNo);
            objcmd.Parameters.AddWithValue("@ConsWorkTelDiallingCode", NReg.ConsWorkTelDiallingCode);
            objcmd.Parameters.AddWithValue("@ConsWorkTelNo", NReg.ConsWorkTelNo);
            objcmd.Parameters.AddWithValue("@ConsCellularTelNo", NReg.ConsCellularTelNo);
            objcmd.Parameters.AddWithValue("@LoanAmt1", NReg.LoanAmt1);
            objcmd.Parameters.AddWithValue("@InstalmentAmt", NReg.InstalmentAmt);
            objcmd.Parameters.AddWithValue("@MonthlySalary", NReg.MonthlySalary);
            objcmd.Parameters.AddWithValue("@SalaryFrequency", NReg.SalaryFrequency);
            objcmd.Parameters.AddWithValue("@RepaymentPeriod1", NReg.RepaymentPeriod1);
            objcmd.Parameters.AddWithValue("@EnquiryReason2", NReg.EnquiryReason2);
            objcmd.Parameters.AddWithValue("@BranchCode", NReg.BranchCode);
            objcmd.Parameters.AddWithValue("@AccountNo", NReg.AccountNo);
            objcmd.Parameters.AddWithValue("@SubAccountNo", NReg.SubAccountNo);
            objcmd.Parameters.AddWithValue("@LoanType", NReg.LoanType);
            objcmd.Parameters.AddWithValue("@DateLoanDisbursed", NReg.DateLoanDisbursed);
            objcmd.Parameters.AddWithValue("@LoanAmt2", NReg.LoanAmt2);
            objcmd.Parameters.AddWithValue("@LoanAmtBalanceIndicator", NReg.LoanAmtBalanceIndicator);
            objcmd.Parameters.AddWithValue("@CurrBalance", NReg.CurrBalance);
            objcmd.Parameters.AddWithValue("@CurrBalanceIndicator", NReg.CurrBalanceIndicator);
            objcmd.Parameters.AddWithValue("@MonthlyInstalment", NReg.MonthlyInstalment);
            objcmd.Parameters.AddWithValue("@LoadIndicator", NReg.LoadIndicator);
            objcmd.Parameters.AddWithValue("@RepaymentPeriod2", NReg.RepaymentPeriod2);
            objcmd.Parameters.AddWithValue("@LoanPurpose", NReg.LoanPurpose);
            objcmd.Parameters.AddWithValue("@TotalAmtRepayable", NReg.TotalAmtRepayable);
            objcmd.Parameters.AddWithValue("@InterestType", NReg.InterestType);
            objcmd.Parameters.AddWithValue("@AnnualRateForTotalChargeOfCredit", NReg.AnnualRateForTotalChargeOfCredit);
            objcmd.Parameters.AddWithValue("@RandValueOfInterestCharges", NReg.RandValueOfInterestCharges);
            objcmd.Parameters.AddWithValue("@RandValueOfTotalChargeOfCredit", NReg.RandValueOfTotalChargeOfCredit);
            objcmd.Parameters.AddWithValue("@SettlementAmt", NReg.SettlementAmt);
            objcmd.Parameters.AddWithValue("@ReAdvanceIndicator", NReg.ReAdvanceIndicator);
            objcmd.Parameters.AddWithValue("@PassportNo", NReg.PassportNo);
            objcmd.Parameters.AddWithValue("@ConsumerID", NReg.ConsumerID);
            objcmd.Parameters.AddWithValue("@SubscriberID", NReg.SubscriberID);
            objcmd.Parameters.AddWithValue("@CreatedByUser", NReg.CreatedByUser);
            objcmd.Parameters.AddWithValue("@SubscriberReference", NReg.SubscriberReference);
            objcmd.Parameters.AddWithValue("@SubscriberEnquiryDate", NReg.subscriberEnquiryDate);
            objcmd.Parameters.AddWithValue("@VoucherCode", NReg.VoucherCode);
            objcmd.Parameters.AddWithValue("@PayAsYouGo", NReg.Payasyougo);
            objcmd.Parameters.AddWithValue("@ProductID", NReg.productID);
            objcmd.Parameters.AddWithValue("@BillingTypeId", NReg.BillingtypeId);
            objcmd.Parameters.AddWithValue("@Billable", NReg.Billable);
            objcmd.Parameters.AddWithValue("@ExtraVarInput1", NReg.ExtraVarInput1);
            objcmd.Parameters.AddWithValue("@ExtraVarInput2", NReg.ExtraVarInput2);
            objcmd.Parameters.AddWithValue("@ExtraVarInput3", NReg.ExtraVarInput3);
            objcmd.Parameters.AddWithValue("@ExtraIntInput1", NReg.ExtraIntInput1);
            objcmd.Parameters.AddWithValue("@ExtraIntInput2", NReg.ExtraIntInput2);
            objcmd.Parameters.AddWithValue("@StatusCode", string.Empty);
            objcmd.Parameters.AddWithValue("@DateOFClosure", string.Empty);

            objcmd.ExecuteNonQuery();

            string strSQL = "SELECT @@IDENTITY";
            SqlCommand Imycommand = new SqlCommand(strSQL, cn);
            SqlDataReader Idr = Imycommand.ExecuteReader();
            Idr.Read();
            int intLogID = Convert.ToInt32(Idr.GetValue(0));
            Idr.Close();

            objcmd.Dispose();
            Imycommand.Dispose();
            Idr.Dispose();
            

            if (cn.State == ConnectionState.Open)
                cn.Close();
            return intLogID;

        }

        public int UpdateError(SqlConnection cn, XDSPortalLibrary.Entity_Layer.NLRDailyRegistrations NReg)
        {
            if (cn.State == ConnectionState.Closed)
            cn.Open();

            string insertSQL = "UPDATE NLRDailyRegistrationsLog SET StatusInd = 'F', ErrorDescription = '" + NReg.ErrorDescription.Replace("'","") + "', ChangedByUser ='"+NReg.ChangedByUser+"', Changedondate = getdate()  WHERE NLRDailyRegistrationsLogID = " + NReg.NLRDailyRegistrationsLogID;
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();
            cn.Close();

            sqlcmd.Dispose();
            return added;
        }

        public int UpdateResult(SqlConnection cn, XDSPortalLibrary.Entity_Layer.NLRDailyRegistrations NReg)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            SqlCommand objcmd = new SqlCommand("sp_u_NLRDailyRegistrationsLog", cn);
            objcmd.CommandType = CommandType.StoredProcedure;

            objcmd.Parameters.AddWithValue("@NLRDailyRegistrationsID", NReg.NLRDailyRegistrationsID);
            objcmd.Parameters.AddWithValue("@NLRDailyRegistrationsLogID", NReg.NLRDailyRegistrationsLogID);
            objcmd.Parameters.AddWithValue("@EnquirySubscriberID", NReg.EnquirySubscriberID);
            objcmd.Parameters.AddWithValue("@Username", NReg.Username);
            objcmd.Parameters.AddWithValue("@Subscribername", NReg.SubscriberName);
            objcmd.Parameters.AddWithValue("@SystemUserID", NReg.SystemUserID);
            objcmd.Parameters.AddWithValue("@StatusInd", NReg.StatusInd);
            objcmd.Parameters.AddWithValue("@TransactionID", NReg.TransactionID);
            objcmd.Parameters.AddWithValue("@TransactionType", NReg.TransactionType);
            objcmd.Parameters.AddWithValue("@EnquiryReason1", NReg.EnquiryReason1);
            objcmd.Parameters.AddWithValue("@SubscriberCode", NReg.SubscriberCode);
            objcmd.Parameters.AddWithValue("@SubscriberPassword", NReg.SubscriberPassword);
            objcmd.Parameters.AddWithValue("@SubscriberGroupID", NReg.SubscriberGroupID);
            objcmd.Parameters.AddWithValue("@SubscriberOperatorID", NReg.SubscriberOperatorID);
            objcmd.Parameters.AddWithValue("@SubscriberOperatorPassword", NReg.SubscriberOperatorPassword);
            objcmd.Parameters.AddWithValue("@TransactionStatus", NReg.TransactionStatus);
            objcmd.Parameters.AddWithValue("@ErrorCode", NReg.ErrorCode);
            objcmd.Parameters.AddWithValue("@MatchedRecordsFound", NReg.MatchedRecordsFound);
            objcmd.Parameters.AddWithValue("@ConsIdentificationNo", NReg.ConsIdentificationNo);
            objcmd.Parameters.AddWithValue("@DateEnquirySent", NReg.DateEnquirySent);
            objcmd.Parameters.AddWithValue("@TimeEnquirySent", NReg.TimeEnquirySent);
            objcmd.Parameters.AddWithValue("@EchoIdentityNo", NReg.EchoIdentityNo);
            objcmd.Parameters.AddWithValue("@EchoUniqueClientNo", NReg.EchoUniqueClientNo);
            objcmd.Parameters.AddWithValue("@MaxNoOfInquiries", NReg.MaxNoOfInquiries);
            objcmd.Parameters.AddWithValue("@MaxNoOfNLRAccounts", NReg.MaxNoOfNLRAccounts);
            objcmd.Parameters.AddWithValue("@MaxNoOfAssAddresses", NReg.MaxNoOfAssAddresses);
            objcmd.Parameters.AddWithValue("@MaxNoOfOtherEmployers", NReg.MaxNoOfOtherEmployers);
            objcmd.Parameters.AddWithValue("@MaxNoOfAssHomeTelNos", NReg.MaxNoOfAssHomeTelNos);
            objcmd.Parameters.AddWithValue("@MaxNoOfAssBusTelNos", NReg.MaxNoOfAssBusTelNos);
            objcmd.Parameters.AddWithValue("@MaxNoOfAssCellNos", NReg.MaxNoOfAssCellNos);
            objcmd.Parameters.AddWithValue("@MaxNoOfAssOtherTelNos", NReg.MaxNoOfAssOtherTelNos);
            objcmd.Parameters.AddWithValue("@LoanRegNo", NReg.LoanRegNo);
            objcmd.Parameters.AddWithValue("@SupplierRefNo", NReg.SupplierRefNo);
            objcmd.Parameters.AddWithValue("@EnquiryRefNo", NReg.EnquiryRefNo);
            objcmd.Parameters.AddWithValue("@ConsSurname", NReg.ConsSurname);
            objcmd.Parameters.AddWithValue("@Forename1", NReg.Forename1);
            objcmd.Parameters.AddWithValue("@Forename2", NReg.Forename2);
            objcmd.Parameters.AddWithValue("@Forename3", NReg.Forename3);
            objcmd.Parameters.AddWithValue("@SpouseCurrSurname", NReg.SpouseCurrSurname);
            objcmd.Parameters.AddWithValue("@SpouseForename1", NReg.SpouseForename1);
            objcmd.Parameters.AddWithValue("@SpouseForename2", NReg.SpouseForename2);
            objcmd.Parameters.AddWithValue("@ConsBirthDate", NReg.ConsBirthDate);
            objcmd.Parameters.AddWithValue("@ConsIdentityNo", NReg.ConsIdentityNo);
            objcmd.Parameters.AddWithValue("@CurrAddrLn1", NReg.CurrAddrLn1);
            objcmd.Parameters.AddWithValue("@CurrAddrLn2", NReg.CurrAddrLn2);
            objcmd.Parameters.AddWithValue("@CurrAddrLn3", NReg.CurrAddrLn3);
            objcmd.Parameters.AddWithValue("@CurrAddrLn4", NReg.CurrAddrLn4);
            objcmd.Parameters.AddWithValue("@CurrAddressPostalCode", NReg.CurrAddressPostalCode);
            objcmd.Parameters.AddWithValue("@PeriodAtCurrAddress", NReg.PeriodAtCurrAddress);
            objcmd.Parameters.AddWithValue("@OtherAddrLn1", NReg.OtherAddrLn1);
            objcmd.Parameters.AddWithValue("@OtherAddrLn2", NReg.OtherAddrLn2);
            objcmd.Parameters.AddWithValue("@OtherAddrLn3", NReg.OtherAddrLn3);
            objcmd.Parameters.AddWithValue("@OtherAddrLn4", NReg.OtherAddrLn4);
            objcmd.Parameters.AddWithValue("@OtherAddressPostalCode", NReg.OtherAddressPostalCode);
            objcmd.Parameters.AddWithValue("@OwnerTenantAtCurrAddress", NReg.OwnerTenantAtCurrAddress);
            objcmd.Parameters.AddWithValue("@ConsOccupation", NReg.ConsOccupation);
            objcmd.Parameters.AddWithValue("@ConsEmployer", NReg.ConsEmployer);
            objcmd.Parameters.AddWithValue("@ConsMaidenName", NReg.ConsMaidenName);
            objcmd.Parameters.AddWithValue("@ConsAliasName", NReg.ConsAliasName);
            objcmd.Parameters.AddWithValue("@Gender", NReg.Gender);
            objcmd.Parameters.AddWithValue("@ConsMaritalStatus", NReg.ConsMaritalStatus);
            objcmd.Parameters.AddWithValue("@ConsHomeTelDiallingCode", NReg.ConsHomeTelDiallingCode);
            objcmd.Parameters.AddWithValue("@ConsHomeTelNo", NReg.ConsHomeTelNo);
            objcmd.Parameters.AddWithValue("@ConsWorkTelDiallingCode", NReg.ConsWorkTelDiallingCode);
            objcmd.Parameters.AddWithValue("@ConsWorkTelNo", NReg.ConsWorkTelNo);
            objcmd.Parameters.AddWithValue("@ConsCellularTelNo", NReg.ConsCellularTelNo);
            objcmd.Parameters.AddWithValue("@LoanAmt1", NReg.LoanAmt1);
            objcmd.Parameters.AddWithValue("@InstalmentAmt", NReg.InstalmentAmt);
            objcmd.Parameters.AddWithValue("@MonthlySalary", NReg.MonthlySalary);
            objcmd.Parameters.AddWithValue("@SalaryFrequency", NReg.SalaryFrequency);
            objcmd.Parameters.AddWithValue("@RepaymentPeriod1", NReg.RepaymentPeriod1);
            objcmd.Parameters.AddWithValue("@EnquiryReason2", NReg.EnquiryReason2);
            objcmd.Parameters.AddWithValue("@BranchCode", NReg.BranchCode);
            objcmd.Parameters.AddWithValue("@AccountNo", NReg.AccountNo);
            objcmd.Parameters.AddWithValue("@SubAccountNo", NReg.SubAccountNo);
            objcmd.Parameters.AddWithValue("@LoanType", NReg.LoanType);
            objcmd.Parameters.AddWithValue("@DateLoanDisbursed", NReg.DateLoanDisbursed);
            objcmd.Parameters.AddWithValue("@LoanAmt2", NReg.LoanAmt2);
            objcmd.Parameters.AddWithValue("@LoanAmtBalanceIndicator", NReg.LoanAmtBalanceIndicator);
            objcmd.Parameters.AddWithValue("@CurrBalance", NReg.CurrBalance);
            objcmd.Parameters.AddWithValue("@CurrBalanceIndicator", NReg.CurrBalanceIndicator);
            objcmd.Parameters.AddWithValue("@MonthlyInstalment", NReg.MonthlyInstalment);
            objcmd.Parameters.AddWithValue("@LoadIndicator", NReg.LoadIndicator);
            objcmd.Parameters.AddWithValue("@RepaymentPeriod2", NReg.RepaymentPeriod2);
            objcmd.Parameters.AddWithValue("@LoanPurpose", NReg.LoanPurpose);
            objcmd.Parameters.AddWithValue("@TotalAmtRepayable", NReg.TotalAmtRepayable);
            objcmd.Parameters.AddWithValue("@InterestType", NReg.InterestType);
            objcmd.Parameters.AddWithValue("@AnnualRateForTotalChargeOfCredit", NReg.AnnualRateForTotalChargeOfCredit);
            objcmd.Parameters.AddWithValue("@RandValueOfInterestCharges", NReg.RandValueOfInterestCharges);
            objcmd.Parameters.AddWithValue("@RandValueOfTotalChargeOfCredit", NReg.RandValueOfTotalChargeOfCredit);
            objcmd.Parameters.AddWithValue("@SettlementAmt", NReg.SettlementAmt);
            objcmd.Parameters.AddWithValue("@ReAdvanceIndicator", NReg.ReAdvanceIndicator);
            objcmd.Parameters.AddWithValue("@PassportNo", NReg.PassportNo);
            objcmd.Parameters.AddWithValue("@ConsumerID", NReg.ConsumerID);
            objcmd.Parameters.AddWithValue("@SubscriberID", NReg.SubscriberID);
            objcmd.Parameters.AddWithValue("@ChangedByUser", NReg.ChangedByUser);
            objcmd.Parameters.AddWithValue("@SubscriberReference", NReg.SubscriberReference);
            objcmd.Parameters.AddWithValue("@SubscriberEnquiryDate", NReg.subscriberEnquiryDate);
            objcmd.Parameters.AddWithValue("@VoucherCode", NReg.VoucherCode);
            objcmd.Parameters.AddWithValue("@PayAsYouGo", NReg.Payasyougo);
            objcmd.Parameters.AddWithValue("@ProductID", NReg.productID);
            objcmd.Parameters.AddWithValue("@BillingTypeId", NReg.BillingtypeId);
            objcmd.Parameters.AddWithValue("@Billable", NReg.Billable);
            objcmd.Parameters.AddWithValue("@ExtraVarInput1", NReg.ExtraVarInput1);
            objcmd.Parameters.AddWithValue("@ExtraVarInput2", NReg.ExtraVarInput2);
            objcmd.Parameters.AddWithValue("@ExtraVarInput3", NReg.ExtraVarInput3);
            objcmd.Parameters.AddWithValue("@ExtraIntInput1", NReg.ExtraIntInput1);
            objcmd.Parameters.AddWithValue("@ExtraIntInput2", NReg.ExtraIntInput2);
            objcmd.Parameters.AddWithValue("@DetailsViewedYN", NReg.DetailsViewedYN);
            objcmd.Parameters.AddWithValue("@DetailsVieweddate", NReg.Detailsvieweddate);
            objcmd.Parameters.AddWithValue("@XMLData", NReg.XMLData);
            objcmd.Parameters.AddWithValue("@StatusCode", string.Empty);
            objcmd.Parameters.AddWithValue("@DateOFClosure", string.Empty);

            int RowsAffected = objcmd.ExecuteNonQuery();

            if (cn.State == ConnectionState.Open)
                cn.Close();
            return RowsAffected;
        }


        public void ExecValidations(SqlConnection cn, XDSPortalLibrary.Entity_Layer.NLRDailyClosures NClose)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            SqlCommand objcmd = new SqlCommand("spValidate", cn);
            objcmd.CommandType = CommandType.StoredProcedure;

            objcmd.Parameters.AddWithValue("@TransactionID", "MFRC");
            objcmd.Parameters.AddWithValue("@TransactionType", 5);
            objcmd.Parameters.AddWithValue("@EnquiryReason1", string.Empty);
            objcmd.Parameters.AddWithValue("@SubscriberCode", string.Empty);
            objcmd.Parameters.AddWithValue("@SubscriberPassword", string.Empty);
            objcmd.Parameters.AddWithValue("@SubscriberGroupID", string.Empty);
            objcmd.Parameters.AddWithValue("@SubscriberOperatorID", string.Empty);
            objcmd.Parameters.AddWithValue("@SubscriberOperatorPassword", string.Empty);
            objcmd.Parameters.AddWithValue("@TransactionStatus", string.Empty);
            objcmd.Parameters.AddWithValue("@ErrorCode", string.Empty);
            objcmd.Parameters.AddWithValue("@MatchedRecordsFound", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsIdentificationNo", string.Empty);
            objcmd.Parameters.AddWithValue("@DateEnquirySent", string.Empty);
            objcmd.Parameters.AddWithValue("@TimeEnquirySent", string.Empty);
            objcmd.Parameters.AddWithValue("@EchoIdentityNo", string.Empty);
            objcmd.Parameters.AddWithValue("@EchoUniqueClientNo", string.Empty);
            objcmd.Parameters.AddWithValue("@MaxNoOfInquiries", string.Empty);
            objcmd.Parameters.AddWithValue("@MaxNoOfNLRAccounts", string.Empty);
            objcmd.Parameters.AddWithValue("@MaxNoOfAssAddresses", string.Empty);
            objcmd.Parameters.AddWithValue("@MaxNoOfOtherEmployers", string.Empty);
            objcmd.Parameters.AddWithValue("@MaxNoOfAssHomeTelNos", string.Empty);
            objcmd.Parameters.AddWithValue("@MaxNoOfAssBusTelNos", string.Empty);
            objcmd.Parameters.AddWithValue("@MaxNoOfAssCellNos", string.Empty);
            objcmd.Parameters.AddWithValue("@MaxNoOfAssOtherTelNos", string.Empty);
            objcmd.Parameters.AddWithValue("@LoanRegNo", NClose.NLRLoanRegistrationNumber);
            objcmd.Parameters.AddWithValue("@SupplierRefNo", NClose.SupplierRefNo);
            objcmd.Parameters.AddWithValue("@EnquiryRefNo", NClose.NLREnquiryReferenceNumber);
            objcmd.Parameters.AddWithValue("@ConsSurname", NClose.Surname);
            objcmd.Parameters.AddWithValue("@Forename1", NClose.Forename1);
            objcmd.Parameters.AddWithValue("@Forename2", NClose.Forename2);
            objcmd.Parameters.AddWithValue("@Forename3", NClose.Forename3);
            objcmd.Parameters.AddWithValue("@SpouseCurrSurname", string.Empty);
            objcmd.Parameters.AddWithValue("@SpouseForename1", string.Empty);
            objcmd.Parameters.AddWithValue("@SpouseForename2", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsBirthDate", NClose.DateOfBirth);
            objcmd.Parameters.AddWithValue("@ConsIdentityNo", NClose.SAIDNumber);
            objcmd.Parameters.AddWithValue("@CurrAddrLn1", string.Empty);
            objcmd.Parameters.AddWithValue("@CurrAddrLn2", string.Empty);
            objcmd.Parameters.AddWithValue("@CurrAddrLn3", string.Empty);
            objcmd.Parameters.AddWithValue("@CurrAddrLn4", string.Empty);
            objcmd.Parameters.AddWithValue("@CurrAddressPostalCode", string.Empty);
            objcmd.Parameters.AddWithValue("@PeriodAtCurrAddress", string.Empty);
            objcmd.Parameters.AddWithValue("@OtherAddrLn1", string.Empty);
            objcmd.Parameters.AddWithValue("@OtherAddrLn2", string.Empty);
            objcmd.Parameters.AddWithValue("@OtherAddrLn3", string.Empty);
            objcmd.Parameters.AddWithValue("@OtherAddrLn4", string.Empty);
            objcmd.Parameters.AddWithValue("@OtherAddressPostalCode", string.Empty);
            objcmd.Parameters.AddWithValue("@OwnerTenantAtCurrAddress", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsOccupation", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsEmployer", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsMaidenName", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsAliasName",string.Empty);
            objcmd.Parameters.AddWithValue("@Gender", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsMaritalStatus",string.Empty);
            objcmd.Parameters.AddWithValue("@ConsHomeTelDiallingCode", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsHomeTelNo", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsWorkTelDiallingCode", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsWorkTelNo", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsCellularTelNo", string.Empty);
            objcmd.Parameters.AddWithValue("@LoanAmt1", string.Empty);
            objcmd.Parameters.AddWithValue("@InstalmentAmt", string.Empty);
            objcmd.Parameters.AddWithValue("@MonthlySalary", string.Empty);
            objcmd.Parameters.AddWithValue("@SalaryFrequency", string.Empty);
            objcmd.Parameters.AddWithValue("@RepaymentPeriod1", string.Empty);
            objcmd.Parameters.AddWithValue("@EnquiryReason2", string.Empty);
            objcmd.Parameters.AddWithValue("@BranchCode", NClose.BranchCode);
            objcmd.Parameters.AddWithValue("@AccountNo", NClose.AccountNumber);
            objcmd.Parameters.AddWithValue("@SubAccountNo",NClose.SubAccountNumber);
            objcmd.Parameters.AddWithValue("@LoanType", string.Empty);
            objcmd.Parameters.AddWithValue("@DateLoanDisbursed", string.Empty);
            objcmd.Parameters.AddWithValue("@LoanAmt2", string.Empty);
            objcmd.Parameters.AddWithValue("@LoanAmtBalanceIndicator", string.Empty);
            objcmd.Parameters.AddWithValue("@CurrBalance", string.Empty);
            objcmd.Parameters.AddWithValue("@CurrBalanceIndicator",string.Empty);
            objcmd.Parameters.AddWithValue("@MonthlyInstalment",string.Empty);
            objcmd.Parameters.AddWithValue("@LoadIndicator",string.Empty);
            objcmd.Parameters.AddWithValue("@RepaymentPeriod2", string.Empty);
            objcmd.Parameters.AddWithValue("@LoanPurpose", string.Empty);
            objcmd.Parameters.AddWithValue("@TotalAmtRepayable", string.Empty);
            objcmd.Parameters.AddWithValue("@InterestType", string.Empty);
            objcmd.Parameters.AddWithValue("@AnnualRateForTotalChargeOfCredit", string.Empty);
            objcmd.Parameters.AddWithValue("@RandValueOfInterestCharges", string.Empty);
            objcmd.Parameters.AddWithValue("@RandValueOfTotalChargeOfCredit", string.Empty);
            objcmd.Parameters.AddWithValue("@SettlementAmt", string.Empty);
            objcmd.Parameters.AddWithValue("@ReAdvanceIndicator",string.Empty);
            objcmd.Parameters.AddWithValue("@PassportNo", NClose.PassportNo);
            objcmd.Parameters.AddWithValue("@StatusCode", NClose.StatusCode);
            objcmd.Parameters.AddWithValue("@DateOFClosure", NClose.DateOfClosureCancellation);

            objcmd.ExecuteNonQuery();

            if (cn.State == ConnectionState.Open)
                cn.Close();

        }

        public int InsertLog(SqlConnection cn, XDSPortalLibrary.Entity_Layer.NLRDailyClosures NClose)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            SqlCommand objcmd = new SqlCommand("sp_I_NLRDailyRegistrationsLog", cn);
            objcmd.CommandType = CommandType.StoredProcedure;

            objcmd.Parameters.AddWithValue("@EnquirySubscriberID", NClose.EnquirySubscriberID);
            objcmd.Parameters.AddWithValue("@Username", NClose.Username);
            objcmd.Parameters.AddWithValue("@Subscribername", NClose.SubscriberName);
            objcmd.Parameters.AddWithValue("@SystemUserID", NClose.SystemUserID);
            objcmd.Parameters.AddWithValue("@StatusInd", NClose.StatusInd);
            objcmd.Parameters.AddWithValue("@TransactionID", "MFRC");
            objcmd.Parameters.AddWithValue("@TransactionType", 5);
            objcmd.Parameters.AddWithValue("@EnquiryReason1", string.Empty);
            objcmd.Parameters.AddWithValue("@SubscriberCode", string.Empty);
            objcmd.Parameters.AddWithValue("@SubscriberPassword", string.Empty);
            objcmd.Parameters.AddWithValue("@SubscriberGroupID", string.Empty);
            objcmd.Parameters.AddWithValue("@SubscriberOperatorID", string.Empty);
            objcmd.Parameters.AddWithValue("@SubscriberOperatorPassword", string.Empty);
            objcmd.Parameters.AddWithValue("@TransactionStatus", string.Empty);
            objcmd.Parameters.AddWithValue("@ErrorCode", string.Empty);
            objcmd.Parameters.AddWithValue("@MatchedRecordsFound", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsIdentificationNo", string.Empty);
            objcmd.Parameters.AddWithValue("@DateEnquirySent", string.Empty);
            objcmd.Parameters.AddWithValue("@TimeEnquirySent", string.Empty);
            objcmd.Parameters.AddWithValue("@EchoIdentityNo", string.Empty);
            objcmd.Parameters.AddWithValue("@EchoUniqueClientNo", string.Empty);
            objcmd.Parameters.AddWithValue("@MaxNoOfInquiries", string.Empty);
            objcmd.Parameters.AddWithValue("@MaxNoOfNLRAccounts", string.Empty);
            objcmd.Parameters.AddWithValue("@MaxNoOfAssAddresses", string.Empty);
            objcmd.Parameters.AddWithValue("@MaxNoOfOtherEmployers", string.Empty);
            objcmd.Parameters.AddWithValue("@MaxNoOfAssHomeTelNos", string.Empty);
            objcmd.Parameters.AddWithValue("@MaxNoOfAssBusTelNos", string.Empty);
            objcmd.Parameters.AddWithValue("@MaxNoOfAssCellNos", string.Empty);
            objcmd.Parameters.AddWithValue("@MaxNoOfAssOtherTelNos", string.Empty);
            objcmd.Parameters.AddWithValue("@LoanRegNo", NClose.NLRLoanRegistrationNumber);
            objcmd.Parameters.AddWithValue("@SupplierRefNo", NClose.SupplierRefNo);
            objcmd.Parameters.AddWithValue("@EnquiryRefNo", NClose.NLREnquiryReferenceNumber);
            objcmd.Parameters.AddWithValue("@ConsSurname", NClose.Surname);
            objcmd.Parameters.AddWithValue("@Forename1", NClose.Forename1);
            objcmd.Parameters.AddWithValue("@Forename2", NClose.Forename2);
            objcmd.Parameters.AddWithValue("@Forename3", NClose.Forename3);
            objcmd.Parameters.AddWithValue("@SpouseCurrSurname", string.Empty);
            objcmd.Parameters.AddWithValue("@SpouseForename1", string.Empty);
            objcmd.Parameters.AddWithValue("@SpouseForename2", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsBirthDate", NClose.DateOfBirth);
            objcmd.Parameters.AddWithValue("@ConsIdentityNo", NClose.SAIDNumber);
            objcmd.Parameters.AddWithValue("@CurrAddrLn1", string.Empty);
            objcmd.Parameters.AddWithValue("@CurrAddrLn2", string.Empty);
            objcmd.Parameters.AddWithValue("@CurrAddrLn3", string.Empty);
            objcmd.Parameters.AddWithValue("@CurrAddrLn4", string.Empty);
            objcmd.Parameters.AddWithValue("@CurrAddressPostalCode", string.Empty);
            objcmd.Parameters.AddWithValue("@PeriodAtCurrAddress", string.Empty);
            objcmd.Parameters.AddWithValue("@OtherAddrLn1", string.Empty);
            objcmd.Parameters.AddWithValue("@OtherAddrLn2", string.Empty);
            objcmd.Parameters.AddWithValue("@OtherAddrLn3", string.Empty);
            objcmd.Parameters.AddWithValue("@OtherAddrLn4", string.Empty);
            objcmd.Parameters.AddWithValue("@OtherAddressPostalCode", string.Empty);
            objcmd.Parameters.AddWithValue("@OwnerTenantAtCurrAddress", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsOccupation", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsEmployer", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsMaidenName", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsAliasName",string.Empty);
            objcmd.Parameters.AddWithValue("@Gender", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsMaritalStatus",string.Empty);
            objcmd.Parameters.AddWithValue("@ConsHomeTelDiallingCode", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsHomeTelNo", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsWorkTelDiallingCode", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsWorkTelNo", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsCellularTelNo", string.Empty);
            objcmd.Parameters.AddWithValue("@LoanAmt1", string.Empty);
            objcmd.Parameters.AddWithValue("@InstalmentAmt", string.Empty);
            objcmd.Parameters.AddWithValue("@MonthlySalary", string.Empty);
            objcmd.Parameters.AddWithValue("@SalaryFrequency", string.Empty);
            objcmd.Parameters.AddWithValue("@RepaymentPeriod1", string.Empty);
            objcmd.Parameters.AddWithValue("@EnquiryReason2", string.Empty);
            objcmd.Parameters.AddWithValue("@BranchCode", string.Empty);
            objcmd.Parameters.AddWithValue("@AccountNo", string.Empty);
            objcmd.Parameters.AddWithValue("@SubAccountNo", string.Empty);
            objcmd.Parameters.AddWithValue("@LoanType", string.Empty);
            objcmd.Parameters.AddWithValue("@DateLoanDisbursed", string.Empty);
            objcmd.Parameters.AddWithValue("@LoanAmt2", string.Empty);
            objcmd.Parameters.AddWithValue("@LoanAmtBalanceIndicator", string.Empty);
            objcmd.Parameters.AddWithValue("@CurrBalance", string.Empty);
            objcmd.Parameters.AddWithValue("@CurrBalanceIndicator",string.Empty);
            objcmd.Parameters.AddWithValue("@MonthlyInstalment",string.Empty);
            objcmd.Parameters.AddWithValue("@LoadIndicator",string.Empty);
            objcmd.Parameters.AddWithValue("@RepaymentPeriod2", string.Empty);
            objcmd.Parameters.AddWithValue("@LoanPurpose", string.Empty);
            objcmd.Parameters.AddWithValue("@TotalAmtRepayable", string.Empty);
            objcmd.Parameters.AddWithValue("@InterestType", string.Empty);
            objcmd.Parameters.AddWithValue("@AnnualRateForTotalChargeOfCredit", string.Empty);
            objcmd.Parameters.AddWithValue("@RandValueOfInterestCharges", string.Empty);
            objcmd.Parameters.AddWithValue("@RandValueOfTotalChargeOfCredit", string.Empty);
            objcmd.Parameters.AddWithValue("@SettlementAmt", string.Empty);
            objcmd.Parameters.AddWithValue("@ReAdvanceIndicator",string.Empty);
            objcmd.Parameters.AddWithValue("@PassportNo", NClose.PassportNo);
            objcmd.Parameters.AddWithValue("@StatusCode", NClose.StatusCode);
            objcmd.Parameters.AddWithValue("@DateOFClosure", NClose.DateOfClosureCancellation);
            objcmd.Parameters.AddWithValue("@ConsumerID", NClose.ConsumerID);
            objcmd.Parameters.AddWithValue("@SubscriberID", NClose.SubscriberID);
            objcmd.Parameters.AddWithValue("@CreatedByUser", NClose.CreatedByUser);
            objcmd.Parameters.AddWithValue("@SubscriberReference", NClose.SubscriberReference);
            objcmd.Parameters.AddWithValue("@SubscriberEnquiryDate", NClose.subscriberEnquiryDate);
            objcmd.Parameters.AddWithValue("@VoucherCode", NClose.VoucherCode);
            objcmd.Parameters.AddWithValue("@PayAsYouGo", NClose.Payasyougo);
            objcmd.Parameters.AddWithValue("@ProductID", NClose.productID);
            objcmd.Parameters.AddWithValue("@BillingTypeId", NClose.BillingtypeId);
            objcmd.Parameters.AddWithValue("@Billable", NClose.Billable);
            objcmd.Parameters.AddWithValue("@ExtraVarInput1", NClose.ExtraVarInput1);
            objcmd.Parameters.AddWithValue("@ExtraVarInput2", NClose.ExtraVarInput2);
            objcmd.Parameters.AddWithValue("@ExtraVarInput3", NClose.ExtraVarInput3);
            objcmd.Parameters.AddWithValue("@ExtraIntInput1", NClose.ExtraIntInput1);
            objcmd.Parameters.AddWithValue("@ExtraIntInput2", NClose.ExtraIntInput2);            

            objcmd.ExecuteNonQuery();

            string strSQL = "SELECT @@IDENTITY";
            SqlCommand Imycommand = new SqlCommand(strSQL, cn);
            SqlDataReader Idr = Imycommand.ExecuteReader();
            Idr.Read();
            int intLogID = Convert.ToInt32(Idr.GetValue(0));
            Idr.Close();

            objcmd.Dispose();
            Imycommand.Dispose();
            Idr.Dispose();


            if (cn.State == ConnectionState.Open)
                cn.Close();
            return intLogID;

        }

        public int UpdateError(SqlConnection cn, XDSPortalLibrary.Entity_Layer.NLRDailyClosures NClose)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string insertSQL = "UPDATE NLRDailyRegistrationsLog SET StatusInd = 'F', ErrorDescription = '" + NClose.ErrorDescription.Replace("'", "") + "', ChangedByUser ='" + NClose.ChangedByUser + "', Changedondate = getdate()  WHERE NLRDailyRegistrationsLogID = " + NClose.NLRDailyRegistrationsLogID;
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();
            cn.Close();

            sqlcmd.Dispose();
            return added;
        }

        public int UpdateResult(SqlConnection cn, XDSPortalLibrary.Entity_Layer.NLRDailyClosures NClose)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            SqlCommand objcmd = new SqlCommand("sp_u_NLRDailyRegistrationsLog", cn);
            objcmd.CommandType = CommandType.StoredProcedure;

            objcmd.Parameters.AddWithValue("@NLRDailyRegistrationsID", NClose.NLRDailyClosuresID);
            objcmd.Parameters.AddWithValue("@NLRDailyRegistrationsLogID", NClose.NLRDailyRegistrationsLogID);
            objcmd.Parameters.AddWithValue("@EnquirySubscriberID", NClose.EnquirySubscriberID);
            objcmd.Parameters.AddWithValue("@Username", NClose.Username);
            objcmd.Parameters.AddWithValue("@Subscribername", NClose.SubscriberName);
            objcmd.Parameters.AddWithValue("@SystemUserID", NClose.SystemUserID);
            objcmd.Parameters.AddWithValue("@StatusInd", NClose.StatusInd);
            objcmd.Parameters.AddWithValue("@TransactionID", "MFRC");
            objcmd.Parameters.AddWithValue("@TransactionType", 5);
            objcmd.Parameters.AddWithValue("@EnquiryReason1", string.Empty);
            objcmd.Parameters.AddWithValue("@SubscriberCode", string.Empty);
            objcmd.Parameters.AddWithValue("@SubscriberPassword", string.Empty);
            objcmd.Parameters.AddWithValue("@SubscriberGroupID", string.Empty);
            objcmd.Parameters.AddWithValue("@SubscriberOperatorID", string.Empty);
            objcmd.Parameters.AddWithValue("@SubscriberOperatorPassword", string.Empty);
            objcmd.Parameters.AddWithValue("@TransactionStatus", string.Empty);
            objcmd.Parameters.AddWithValue("@ErrorCode", string.Empty);
            objcmd.Parameters.AddWithValue("@MatchedRecordsFound", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsIdentificationNo", string.Empty);
            objcmd.Parameters.AddWithValue("@DateEnquirySent", string.Empty);
            objcmd.Parameters.AddWithValue("@TimeEnquirySent", string.Empty);
            objcmd.Parameters.AddWithValue("@EchoIdentityNo", string.Empty);
            objcmd.Parameters.AddWithValue("@EchoUniqueClientNo", string.Empty);
            objcmd.Parameters.AddWithValue("@MaxNoOfInquiries", string.Empty);
            objcmd.Parameters.AddWithValue("@MaxNoOfNLRAccounts", string.Empty);
            objcmd.Parameters.AddWithValue("@MaxNoOfAssAddresses", string.Empty);
            objcmd.Parameters.AddWithValue("@MaxNoOfOtherEmployers", string.Empty);
            objcmd.Parameters.AddWithValue("@MaxNoOfAssHomeTelNos", string.Empty);
            objcmd.Parameters.AddWithValue("@MaxNoOfAssBusTelNos", string.Empty);
            objcmd.Parameters.AddWithValue("@MaxNoOfAssCellNos", string.Empty);
            objcmd.Parameters.AddWithValue("@MaxNoOfAssOtherTelNos", string.Empty);
            objcmd.Parameters.AddWithValue("@LoanRegNo", NClose.NLRLoanRegistrationNumber);
            objcmd.Parameters.AddWithValue("@SupplierRefNo", NClose.SupplierRefNo);
            objcmd.Parameters.AddWithValue("@EnquiryRefNo", NClose.NLREnquiryReferenceNumber);
            objcmd.Parameters.AddWithValue("@ConsSurname", NClose.Surname);
            objcmd.Parameters.AddWithValue("@Forename1", NClose.Forename1);
            objcmd.Parameters.AddWithValue("@Forename2", NClose.Forename2);
            objcmd.Parameters.AddWithValue("@Forename3", NClose.Forename3);
            objcmd.Parameters.AddWithValue("@SpouseCurrSurname", string.Empty);
            objcmd.Parameters.AddWithValue("@SpouseForename1", string.Empty);
            objcmd.Parameters.AddWithValue("@SpouseForename2", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsBirthDate", NClose.DateOfBirth);
            objcmd.Parameters.AddWithValue("@ConsIdentityNo", NClose.SAIDNumber);
            objcmd.Parameters.AddWithValue("@CurrAddrLn1", string.Empty);
            objcmd.Parameters.AddWithValue("@CurrAddrLn2", string.Empty);
            objcmd.Parameters.AddWithValue("@CurrAddrLn3", string.Empty);
            objcmd.Parameters.AddWithValue("@CurrAddrLn4", string.Empty);
            objcmd.Parameters.AddWithValue("@CurrAddressPostalCode", string.Empty);
            objcmd.Parameters.AddWithValue("@PeriodAtCurrAddress", string.Empty);
            objcmd.Parameters.AddWithValue("@OtherAddrLn1", string.Empty);
            objcmd.Parameters.AddWithValue("@OtherAddrLn2", string.Empty);
            objcmd.Parameters.AddWithValue("@OtherAddrLn3", string.Empty);
            objcmd.Parameters.AddWithValue("@OtherAddrLn4", string.Empty);
            objcmd.Parameters.AddWithValue("@OtherAddressPostalCode", string.Empty);
            objcmd.Parameters.AddWithValue("@OwnerTenantAtCurrAddress", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsOccupation", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsEmployer", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsMaidenName", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsAliasName",string.Empty);
            objcmd.Parameters.AddWithValue("@Gender", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsMaritalStatus",string.Empty);
            objcmd.Parameters.AddWithValue("@ConsHomeTelDiallingCode", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsHomeTelNo", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsWorkTelDiallingCode", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsWorkTelNo", string.Empty);
            objcmd.Parameters.AddWithValue("@ConsCellularTelNo", string.Empty);
            objcmd.Parameters.AddWithValue("@LoanAmt1", string.Empty);
            objcmd.Parameters.AddWithValue("@InstalmentAmt", string.Empty);
            objcmd.Parameters.AddWithValue("@MonthlySalary", string.Empty);
            objcmd.Parameters.AddWithValue("@SalaryFrequency", string.Empty);
            objcmd.Parameters.AddWithValue("@RepaymentPeriod1", string.Empty);
            objcmd.Parameters.AddWithValue("@EnquiryReason2", string.Empty);
            objcmd.Parameters.AddWithValue("@BranchCode", NClose.BranchCode);
            objcmd.Parameters.AddWithValue("@AccountNo", NClose.AccountNumber);
            objcmd.Parameters.AddWithValue("@SubAccountNo", NClose.SubAccountNumber);
            objcmd.Parameters.AddWithValue("@LoanType", string.Empty);
            objcmd.Parameters.AddWithValue("@DateLoanDisbursed", string.Empty);
            objcmd.Parameters.AddWithValue("@LoanAmt2", string.Empty);
            objcmd.Parameters.AddWithValue("@LoanAmtBalanceIndicator", string.Empty);
            objcmd.Parameters.AddWithValue("@CurrBalance", string.Empty);
            objcmd.Parameters.AddWithValue("@CurrBalanceIndicator",string.Empty);
            objcmd.Parameters.AddWithValue("@MonthlyInstalment",string.Empty);
            objcmd.Parameters.AddWithValue("@LoadIndicator",string.Empty);
            objcmd.Parameters.AddWithValue("@RepaymentPeriod2", string.Empty);
            objcmd.Parameters.AddWithValue("@LoanPurpose", string.Empty);
            objcmd.Parameters.AddWithValue("@TotalAmtRepayable", string.Empty);
            objcmd.Parameters.AddWithValue("@InterestType", string.Empty);
            objcmd.Parameters.AddWithValue("@AnnualRateForTotalChargeOfCredit", string.Empty);
            objcmd.Parameters.AddWithValue("@RandValueOfInterestCharges", string.Empty);
            objcmd.Parameters.AddWithValue("@RandValueOfTotalChargeOfCredit", string.Empty);
            objcmd.Parameters.AddWithValue("@SettlementAmt", string.Empty);
            objcmd.Parameters.AddWithValue("@ReAdvanceIndicator",string.Empty);
            objcmd.Parameters.AddWithValue("@PassportNo", NClose.PassportNo);
            objcmd.Parameters.AddWithValue("@StatusCode", NClose.StatusCode);
            objcmd.Parameters.AddWithValue("@DateOFClosure", NClose.DateOfClosureCancellation);
            objcmd.Parameters.AddWithValue("@ConsumerID", NClose.ConsumerID);
            objcmd.Parameters.AddWithValue("@SubscriberID", NClose.SubscriberID);
            objcmd.Parameters.AddWithValue("@ChangedByUser", NClose.ChangedByUser);
            objcmd.Parameters.AddWithValue("@SubscriberReference", NClose.SubscriberReference);
            objcmd.Parameters.AddWithValue("@SubscriberEnquiryDate", NClose.subscriberEnquiryDate);
            objcmd.Parameters.AddWithValue("@VoucherCode", NClose.VoucherCode);
            objcmd.Parameters.AddWithValue("@PayAsYouGo", NClose.Payasyougo);
            objcmd.Parameters.AddWithValue("@ProductID", NClose.productID);
            objcmd.Parameters.AddWithValue("@BillingTypeId", NClose.BillingtypeId);
            objcmd.Parameters.AddWithValue("@Billable", NClose.Billable);
            objcmd.Parameters.AddWithValue("@ExtraVarInput1", NClose.ExtraVarInput1);
            objcmd.Parameters.AddWithValue("@ExtraVarInput2", NClose.ExtraVarInput2);
            objcmd.Parameters.AddWithValue("@ExtraVarInput3", NClose.ExtraVarInput3);
            objcmd.Parameters.AddWithValue("@ExtraIntInput1", NClose.ExtraIntInput1);
            objcmd.Parameters.AddWithValue("@ExtraIntInput2", NClose.ExtraIntInput2);
            objcmd.Parameters.AddWithValue("@DetailsViewedYN", NClose.DetailsViewedYN);
            objcmd.Parameters.AddWithValue("@DetailsVieweddate", NClose.Detailsvieweddate);
            objcmd.Parameters.AddWithValue("@XMLData", NClose.XMLData);            

            int RowsAffected = objcmd.ExecuteNonQuery();

            if (cn.State == ConnectionState.Open)
                cn.Close();
            return RowsAffected;
        }

        public string GetNLRDailyRegistrationsLogObject(SqlConnection con, int NLRDailyRegistrationsLogID)
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            XDSPortalLibrary.Entity_Layer.NLRDailyRegistrations NR = new XDSPortalLibrary.Entity_Layer.NLRDailyRegistrations();
           string strsql = "Select * from NLRDailyRegistrationsLog nolock where NLRDailyRegistrationsLogID = "+ NLRDailyRegistrationsLogID+"";
            SqlCommand cmd = new SqlCommand(strsql,con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);

            string ExtraVarInput1 = string.Empty;

            foreach(DataRow r in ds.Tables[0].Rows )
            {
                ExtraVarInput1 = r["ExtraVarInput1"].ToString();
            }
            return ExtraVarInput1;
        }
    }
}
    

