﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.IO;

namespace XDSPortalEnquiry.Data
{
    public class BatchAccountVerificationsFile
    {
        private string fileName;
        private byte[] reportFile;

        public string FileName
        {
            get { return this.fileName; }
            set { this.fileName = value; }
        }

        public byte[] ReportFile
        {
            get { return this.reportFile; }
            set { this.reportFile = value; }
        }
    }
    public class BatchAccountVerifications
    {
        public DataSet GetBatchAccountVerificationDataSetPerSubscriber(SqlConnection cn, int intSubscriberID)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string strSQL = "Select *,case EnquiryStatusInd when 'N' then 'New' when 'C' then 'Completed' when 'F' then 'Failed' when 'P' then 'Pending' end as 'EnquiryStatusDesc' from LoadInformation nolock Where SubscriberID = " + intSubscriberID.ToString() + " order by CreatedOnDate desc";
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            cn.Close();

            return ds;
        }

        public DataSet GetBatchAccountVerificationDataSetPerStatus(SqlConnection cn, int intSubscriberID, string strStatus)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string strSQL = "Select *,case EnquiryStatusInd when 'N' then 'New' when 'C' then 'Completed' when 'F' then 'Failed' when 'P' then 'Pending' end as 'EnquiryStatusDesc'  from LoadInformation nolock Where SubscriberID = " + intSubscriberID.ToString() + " and EnquiryStatusInd = '" + strStatus + "'order by CreatedOnDate desc";
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            cn.Close();

            return ds;
        }

        public DataSet GetBatchAccountVerificationDataSetPerID(SqlConnection cn, int loaderID)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string strSQL = string.Format("EXEC Load_GetCompletedRecordsByID {0} ", loaderID);
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            cn.Close();

            return ds;
        }

        public BatchAccountVerificationsFile GetBatchAccountVerificationReportFile(SqlConnection cn, string loaderID)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            BatchAccountVerificationsFile oBatchAccountVerificationsFile = new BatchAccountVerificationsFile();

            string strSQL = "Select FileName from LoadInformation nolock Where LoaderID =  " + loaderID;
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataReader oReader = sqlcmd.ExecuteReader();

            while (oReader.Read())
            {
                oBatchAccountVerificationsFile.FileName = oReader["FileName"].ToString();
            }

            oReader.Close();
            oReader.Dispose();
            cn.Close();

            return oBatchAccountVerificationsFile;
        }

        public int LogUploadedFiles(SqlConnection cn, int intSubscriberID, string strFileName, string strStagingFileName, string strFileLocation, string strSystemUsername, string templateID,string username,string password)
        {
            try
            {
                if (cn.State == ConnectionState.Closed)
                    cn.Open();

                string strSQL = string.Format("EXEC LogUploadedFiles '{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}'", intSubscriberID, templateID, strFileName, strStagingFileName, strFileLocation, new FileInfo(strFileLocation).Length, "New", "N", strSystemUsername, username,password);
                SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
                int result = sqlcmd.ExecuteNonQuery();
                cn.Close();

                return result;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int CompareColumns(string strFileLocation, string type)
        {
            try
            {
                DataTable dtUploadedFile = new DataTable();
                DataTable dtTemplate = new DataTable();

                dtTemplate.Columns.Add("BankName");
                dtTemplate.Columns.Add("BranchNumber");
                dtTemplate.Columns.Add("AccountNumber");
                dtTemplate.Columns.Add("AccountType");
                dtTemplate.Columns.Add("IdentificationNo");
                dtTemplate.Columns.Add("IdentificationType");
                dtTemplate.Columns.Add("Initials");
                dtTemplate.Columns.Add("Surname");

                switch (type)
                {
                    case "Excel":
                        string ConnectionString = "Provider = Microsoft.ACE.OLEDB.12.0; Data Source=" + strFileLocation + "; Extended Properties=Excel 12.0;";
                        using (OleDbConnection cn = new OleDbConnection(ConnectionString))
                        {
                            cn.Open();

                            DataTable dbSchema = cn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                            if (dbSchema == null || dbSchema.Rows.Count < 1)
                            {
                                return 0;
                            }

                            string WorkSheetName = dbSchema.Rows[0]["TABLE_NAME"].ToString();

                            OleDbDataAdapter da = new OleDbDataAdapter("SELECT * FROM [" + WorkSheetName + "]", cn);
                            dtUploadedFile = new DataTable(WorkSheetName);

                            da.Fill(dtUploadedFile);
                        }

                        if (dtTemplate.Columns.Count != dtUploadedFile.Columns.Count)
                            return 0;

                        foreach (DataColumn column in dtUploadedFile.Columns)
                        {
                            if (!dtTemplate.Columns.Contains(column.ColumnName.Replace(" ", "")))
                                return 0;
                        }

                        break;
                    case "CSV":
                        using (StreamReader reader = new StreamReader(strFileLocation))
                        {
                            string strLine = reader.ReadLine();

                            if (!string.IsNullOrEmpty(strLine))
                            {
                                string[] values = strLine.Split(new char[] { ',' });

                                DataTable dtTemplateHeader = new DataTable("Headers");

                                foreach (string v in values)
                                {
                                    dtTemplateHeader.Columns.Add(v);
                                }

                                if (dtTemplate.Columns.Count != dtTemplateHeader.Columns.Count)
                                    return 0;

                                foreach (DataColumn column in dtTemplateHeader.Columns)
                                {
                                    if (!dtTemplate.Columns.Contains(column.ColumnName.Replace(" ", "")))
                                        return 0;
                                }
                            }
                        }
                        break;
                }

                return 1;

            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}
