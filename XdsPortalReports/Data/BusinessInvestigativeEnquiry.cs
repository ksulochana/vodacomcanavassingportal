﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalEnquiry.Data
{
    public class BusinessInvestigativeEnquiry
    {
        public BusinessInvestigativeEnquiry()
        {
        }

        public DataSet GetBICountryCode(SqlConnection con)
        {
            //if (Enquirycon.State == ConnectionState.Closed)
            //    Enquirycon.Open();

            //string strSQL = "SELECT top 1  ServerConnectionString nolock FROM dbo.EnquiryServers where Active = 1";
            //SqlCommand cmdconstr = new SqlCommand(strSQL, Enquirycon);
            //SqlDataReader dr = cmdconstr.ExecuteReader();
            //dr.Read();
            //string strconstring = dr.GetValue(0).ToString();
            //dr.Close();

            //Enquirycon.Close();

            //SqlConnection con = new SqlConnection(strconstring);

            if (con.State == ConnectionState.Closed)
            con.Open();

            DataSet ds = new DataSet("BICountryCodes");
            string strSQL = "Select CountryID,CountryDescription from BICountryCodes where RecordStatusInd = 'A'";
            SqlCommand cmd = new SqlCommand(strSQL, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);

            foreach (DataTable dt in ds.Tables)
            {
                dt.TableName = "Country";
            }

            con.Close();
            return ds;
        }

        public DataSet GetBIReport(SqlConnection con, int CountryID)
        {
            //if (Enquirycon.State == ConnectionState.Closed)
            //    Enquirycon.Open();

            //string strSQL = "SELECT top 1  ServerConnectionString nolock FROM dbo.EnquiryServers where Active = 1";
            //SqlCommand cmdconstr = new SqlCommand(strSQL, Enquirycon);
            //SqlDataReader dr = cmdconstr.ExecuteReader();
            //dr.Read();
            //string strconstring = dr.GetValue(0).ToString();
            //dr.Close();

            //Enquirycon.Close();

            //SqlConnection con = new SqlConnection(strconstring);

            if (con.State == ConnectionState.Closed)
            con.Open();

            DataSet ds = new DataSet("BIReportTypes");
            string strSQL = "Select a.CountryReportID ReportID, b.ReportName from BICountryReports a (nolock) inner join BIReports b (nolock) on a.ReportID = b.ReportID where a.CountryID = " + CountryID.ToString() + " and a.Active = 1 and b.RecordStatusInd = 'A'";
            SqlCommand cmd = new SqlCommand(strSQL, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            foreach (DataTable dt in ds.Tables)
            {

                dt.TableName = "Reports";
            }
            con.Close();
            return ds;
        }
        public DataSet GetBIReportTimeFrames(SqlConnection con, int ReportID)
        {
            //if (Enquirycon.State == ConnectionState.Closed)
            //    Enquirycon.Open();

            //string strSQL = "SELECT top 1  ServerConnectionString nolock FROM dbo.EnquiryServers where Active = 1";
            //SqlCommand cmdconstr = new SqlCommand(strSQL, Enquirycon);
            //SqlDataReader dr = cmdconstr.ExecuteReader();
            //dr.Read();
            //string strconstring = dr.GetValue(0).ToString();
            //dr.Close();

            //Enquirycon.Close();

            //SqlConnection con = new SqlConnection(strconstring);

            if (con.State == ConnectionState.Closed)
            con.Open();

            DataSet ds = new DataSet("BIReportTimeFrames");
            //string strSQL = "Select a.BIReportTimeFrameID, b.TimeFrameDesc from BIReportTimeFrame a (nolock) inner join BITimeFrames b (nolock) on a.TimeFrameID = b.TimeFrameID where a.ReportID = " + ReportID.ToString() + " and a.Active = 1 and b.RecordStatusInd = 'A'";
            //string strSQL = "Select a.BICountryReportTimeFrameID, b.TimeFrameDesc from BICountryReportTimeFrame a (nolock) 	inner join BITimeFrames b (nolock) on a.TimeFrameID = b.TimeFrameID where 	a.CountryID = " + CountryID.ToString() + " and	a.ReportID = " + ReportID.ToString() + " and a.Active = 1 and b.RecordStatusInd = 'A'";
            string strSQL = "Select a.BICountryReportTimeFrameID, b.TimeFrameDesc from BICountryReportTimeFrame a (nolock) inner join BITimeFrames b (nolock) on a.TimeFrameID = b.TimeFrameID inner join BIReports C (nolock) on a.ReportID = c.ReportID inner join BICountryCodes d (nolock) on a.CountryID = d.CountryID inner join BICountryReports e on a.CountryID = e.CountryID and a.ReportID = e.ReportID where e.CountryreportID =  " + ReportID.ToString() + " and a.Active = 1 and b.RecordStatusInd = 'A'";
            SqlCommand cmd = new SqlCommand(strSQL, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            foreach (DataTable dt in ds.Tables)
            {
                dt.TableName = "TimeFrames";
            }
            con.Close();
            return ds;
        }

    }
}
