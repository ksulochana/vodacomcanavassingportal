﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Reflection;
using System.Net.Mail;
using System.Net;

namespace XDSPortalEnquiry.Data
{
    public class Email
    {
        public int InsertEmailLog(SqlConnection ObjConstring, XDSPortalLibrary.Entity_Layer.Email oEmail)
        {
            int EmaillogID = 0;

            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            SqlCommand Objsqlcom = new SqlCommand("sp_I_EmailLog", ObjConstring);
            Objsqlcom.CommandType = CommandType.StoredProcedure;

            Objsqlcom.Parameters.AddWithValue("@SubscriberEnquiryResultID", oEmail.SubsriberEnquiryResultID);
            Objsqlcom.Parameters.AddWithValue("@SubscriberEmailSettingsID", oEmail.SubscriberEmailSettingsID);
            Objsqlcom.Parameters.AddWithValue("@ToEmailAddress", oEmail.ToEmailAddress);
            Objsqlcom.Parameters.AddWithValue("@FromEmailAddress", oEmail.FromEmailAddress);
            Objsqlcom.Parameters.AddWithValue("@CCEmailAddress", oEmail.CCEmailAddress);
            Objsqlcom.Parameters.AddWithValue("@BCCEmailAddress", oEmail.BCCEmailaddress);
            Objsqlcom.Parameters.AddWithValue("@FromName", oEmail.FromName);
            Objsqlcom.Parameters.AddWithValue("@ToName", oEmail.Toname);
            Objsqlcom.Parameters.AddWithValue("@Subject", oEmail.Subject);
            Objsqlcom.Parameters.AddWithValue("@IncludeAttachment", oEmail.IncludeAttachment);
            Objsqlcom.Parameters.AddWithValue("@NoofAttachments", oEmail.NoOfAttachments);
            Objsqlcom.Parameters.AddWithValue("@MessageBody", oEmail.MessageBody);
            Objsqlcom.Parameters.AddWithValue("@IsBodyhtml", oEmail.IsBodyhtml);
            Objsqlcom.Parameters.AddWithValue("@IsBodyTemplate", oEmail.IsBodyTemplate);
            Objsqlcom.Parameters.AddWithValue("@Bodystream", oEmail.BodyStream);
            Objsqlcom.Parameters.AddWithValue("@EmailStatus", oEmail.EmailStatus);
            Objsqlcom.Parameters.AddWithValue("@ErrorDescription", oEmail.ErrorDescription);
            Objsqlcom.Parameters.AddWithValue("@StatusInd", oEmail.StatusInd);
            Objsqlcom.Parameters.AddWithValue("@CreatedbyUser", oEmail.CreatedbyUser);
            Objsqlcom.Parameters.AddWithValue("@CreatedonDate", oEmail.CreatedOnDate);

            SqlParameter OsqlParam = new SqlParameter("EmailLogID",DbType.Int32);
            OsqlParam.Direction = ParameterDirection.ReturnValue;            
            Objsqlcom.Parameters.Add(OsqlParam);

            Objsqlcom.ExecuteNonQuery();

            EmaillogID = int.Parse(OsqlParam.Value.ToString());

            if (ObjConstring.State == ConnectionState.Open)
                ObjConstring.Close();

            return EmaillogID;
        }

        public DataSet GetEmailSettings(SqlConnection ObjConstring, int SubscriberID, string CategoryInd, int ProductID, int EmailCategoryID)
        {
            DataSet dsSettings = new DataSet();

            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            SqlCommand Objsqlcom = new SqlCommand("sp_Get_EmailSettings", ObjConstring);
            Objsqlcom.CommandType = CommandType.StoredProcedure;

            Objsqlcom.Parameters.AddWithValue("@SubscriberID", SubscriberID);
            Objsqlcom.Parameters.AddWithValue("@CategoryInd", CategoryInd);
            Objsqlcom.Parameters.AddWithValue("@ProductID", ProductID);
            Objsqlcom.Parameters.AddWithValue("@EmailCategoryID",EmailCategoryID);


            SqlDataAdapter oda = new SqlDataAdapter(Objsqlcom);
            oda.Fill(dsSettings);
            

            if (ObjConstring.State == ConnectionState.Open)
                ObjConstring.Close();

            return dsSettings;
        }

        public void UpdateEmailLog(SqlConnection ObjConstring, XDSPortalLibrary.Entity_Layer.Email oEmail)
        {
            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            SqlCommand Objsqlcom = new SqlCommand("sp_U_EmailLog", ObjConstring);
            Objsqlcom.CommandType = CommandType.StoredProcedure;

            Objsqlcom.Parameters.AddWithValue("@SubscriberEnquiryResultID", oEmail.SubsriberEnquiryResultID);
            Objsqlcom.Parameters.AddWithValue("@SubscriberEmailSettingsID", oEmail.SubscriberEmailSettingsID);
            Objsqlcom.Parameters.AddWithValue("@ToEmailAddress", oEmail.ToEmailAddress);
            Objsqlcom.Parameters.AddWithValue("@FromEmailAddress", oEmail.FromEmailAddress);
            Objsqlcom.Parameters.AddWithValue("@CCEmailAddress", oEmail.CCEmailAddress);
            Objsqlcom.Parameters.AddWithValue("@BCCEmailAddress", oEmail.BCCEmailaddress);
            Objsqlcom.Parameters.AddWithValue("@FromName", oEmail.FromName);
            Objsqlcom.Parameters.AddWithValue("@ToName", oEmail.Toname);
            Objsqlcom.Parameters.AddWithValue("@Subject", oEmail.Subject);
            Objsqlcom.Parameters.AddWithValue("@IncludeAttachment", oEmail.IncludeAttachment);
            Objsqlcom.Parameters.AddWithValue("@NoofAttachments", oEmail.NoOfAttachments);
            Objsqlcom.Parameters.AddWithValue("@MessageBody", oEmail.MessageBody);
            Objsqlcom.Parameters.AddWithValue("@IsBodyhtml", oEmail.IsBodyhtml);
            Objsqlcom.Parameters.AddWithValue("@IsBodyTemplate", oEmail.IsBodyTemplate);
            Objsqlcom.Parameters.AddWithValue("@Bodystream", oEmail.BodyStream);
            Objsqlcom.Parameters.AddWithValue("@EmailStatus", oEmail.EmailStatus);
            Objsqlcom.Parameters.AddWithValue("@ErrorDescription", oEmail.ErrorDescription);
            Objsqlcom.Parameters.AddWithValue("@StatusInd", oEmail.StatusInd);
            Objsqlcom.Parameters.AddWithValue("@CreatedbyUser", oEmail.CreatedbyUser);
            Objsqlcom.Parameters.AddWithValue("@CreatedonDate", oEmail.CreatedOnDate);
            Objsqlcom.Parameters.AddWithValue("@EmailLogID", oEmail.EmailLogID);
            Objsqlcom.Parameters.AddWithValue("@ChangedbyUser", oEmail.ChangedByUser);
            Objsqlcom.Parameters.AddWithValue("@ChangedonDate", oEmail.ChangedonDate);          
         

            Objsqlcom.ExecuteNonQuery();

          

            if (ObjConstring.State == ConnectionState.Open)
                ObjConstring.Close();
        }

        public void ProcessEmail(SqlConnection ObjConstring, XDSPortalLibrary.Entity_Layer.Email oEmail)
        {
            try
            {
                SmtpClient oSmtp = new SmtpClient(oEmail.SMTPServerName, oEmail.Port);
                oSmtp.Credentials = new NetworkCredential(oEmail.SMTPUsername, oEmail.SMTPPassword);

                MailMessage mail = new MailMessage();

                string[] ToAddress = oEmail.ToEmailAddress.Split(';');
                string[] CCAddress = oEmail.CCEmailAddress.Split(';');
                string[] BCCAddress = oEmail.BCCEmailaddress.Split(';');
                

                foreach (string ToEmailID in ToAddress) { if (!string.IsNullOrEmpty(ToEmailID)) {mail.To.Add(new MailAddress(ToEmailID.Trim()));} }
                foreach (string CCEmailID in CCAddress) { if (!string.IsNullOrEmpty(CCEmailID)) { mail.CC.Add(new MailAddress(CCEmailID.Trim())); } }
                foreach (string BCCEmailID in BCCAddress) { if (!string.IsNullOrEmpty(BCCEmailID)) { mail.Bcc.Add(new MailAddress(BCCEmailID.Trim())); } }
                
                mail.Subject = oEmail.Subject;
                mail.IsBodyHtml = oEmail.IsBodyhtml;
                mail.Body = oEmail.MessageBody;
                mail.From = new MailAddress(oEmail.FromEmailAddress,oEmail.FromName);

                oSmtp.Send(mail);
                oEmail.EmailStatus = "C";

                UpdateEmailLog(ObjConstring,oEmail);
            }
            catch (Exception ex)
            {
                oEmail.ErrorDescription = ex.Message +" " +ex.InnerException;
                oEmail.EmailStatus = "E";
                UpdateEmailLog(ObjConstring, oEmail);
            }
        }

        public DataSet GetEmailLog(SqlConnection ObjConstring, string EmailStatus)
        {
            DataSet dtEmailLog = new DataSet();

            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            SqlCommand Objsqlcom = new SqlCommand("Select * from EmailLog nolock Where EmailStatus = '" + EmailStatus + "'", ObjConstring);
            Objsqlcom.CommandTimeout = 0;

            SqlDataAdapter oda = new SqlDataAdapter(Objsqlcom);
            oda.Fill(dtEmailLog);

            if (ObjConstring.State == ConnectionState.Open)
                ObjConstring.Close();

            return dtEmailLog;

        }

        public DataSet GetEmailLog(SqlConnection ObjConstring, string EmailStatus, int SubscriberEmailSettingsID)
        {
            DataSet dtEmailLog = new DataSet();

            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            SqlCommand Objsqlcom = new SqlCommand("Select * from EmailLog nolock Where isnull(SubscriberEmailSettingsID,0) = " + SubscriberEmailSettingsID.ToString() + " and EmailStatus = '" + EmailStatus +"'", ObjConstring);
            Objsqlcom.CommandTimeout = 0;

            SqlDataAdapter oda = new SqlDataAdapter(Objsqlcom);
            oda.Fill(dtEmailLog);

            if (ObjConstring.State == ConnectionState.Open)
                ObjConstring.Close();

            return dtEmailLog;

        }
    }
}
