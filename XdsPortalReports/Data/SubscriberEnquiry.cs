
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalEnquiry.Data
{
    public class SubscriberEnquiry
    {
        public SubscriberEnquiry(){}


        public int InsertSubscriberEnquiry(string DBConnection, Entity.SubscriberEnquiry S)
        {

         
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = DBConnection;
            cn.Open();
            if (!string.IsNullOrEmpty(S.SearchInput))
               S.SearchInput = S.SearchInput.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.Surname))
                S.Surname = S.Surname.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.MaidenName))
                S.MaidenName = S.MaidenName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.FirstName))
                S.FirstName = S.FirstName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SecondName))
                S.SecondName = S.SecondName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.BusRegistrationNo))
                S.BusRegistrationNo = S.BusRegistrationNo.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.BusBusinessName))
                S.BusBusinessName = S.BusBusinessName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.ErrorDescription))
                S.ErrorDescription = S.ErrorDescription.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SubscriberName))
                S.SubscriberName = S.SubscriberName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SystemUser))
                S.SystemUser = S.SystemUser.Replace("'", "''");
            
            string insertSQL = "EXEC sp_i_SubscriberEnquiry " + S.SubscriberID + " , '" + S.SubscriberName + "' , " + S.SystemUserID + " , '" + S.SystemUser + "' , '" + S.EnquiryReason + "' , '" + S.SubscriberEnquiryDate + "' , '" + S.SubscriberReference + "' , '" + S.EnquiryStatus + "' , '" + S.EnquiryResult + "' , " + S.ProductID + " , '" + S.SearchInput + "' , '" + S.IDNo + "' , '" + S.PassportNo + "' , '" + S.Surname + "' , '" + S.FirstInitial + "' , '" + S.SecondInitial + "' , '" + S.MaidenName + "' , '" + S.Gender + "' , '" + S.FirstName + "' , '" + S.SecondName + "' , '" + S.BirthDate + "' , '" + S.TelephoneCode + "' , '" + S.TelephoneNo + "' , '" + S.AccountNo + "' , '" + S.SubAccountNo + "' , '" + S.BusRegistrationNo + "' , '" + S.BusBusinessName + "' , '" + S.BusVatNumber + "' , '" + S.AgeSearchTypeInd + "' , " + S.Age + " , " + S.AgeBirthYear + " , " + S.AgeDeviation + " , '" + S.ErrorDescription + "' , '" + S.ExtraVarInput1 + "' , '" + S.ExtraVarInput2 + "' , '" + S.ExtraVarInput3 + "' , " + S.ExtraIntInput1 + " , " + S.ExtraIntInput2 + " , '" + S.BranchCode + "' , " + S.PurposeID + " , '" + S.CreatedByUser + "' , '" + S.CreatedOnDate + "' , '" + S.ChangedByUser + "' , '" + S.ChangedOnDate + "' ";
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();

            string strSQL = "SELECT @@IDENTITY";
            SqlCommand Imycommand = new SqlCommand(strSQL, cn);
            SqlDataReader Idr = Imycommand.ExecuteReader();
            Idr.Read();
            int intSubscriberEnquiryID = Convert.ToInt32(Idr.GetValue(0));
            Idr.Close();
            cn.Close();

            sqlcmd.Dispose();
            Imycommand.Dispose();
            Idr.Dispose();
            return intSubscriberEnquiryID;
        }

        //public int InsertSubscriberEnquiry(SqlConnection cn, Entity.SubscriberEnquiry S)
        //{
        //    if (cn.State == ConnectionState.Closed)
        //        cn.Open();

        //    if (!string.IsNullOrEmpty(S.SearchInput))
        //        S.SearchInput = S.SearchInput.Replace("'", "''");
        //    if (!string.IsNullOrEmpty(S.Surname))
        //        S.Surname = S.Surname.Replace("'", "''");
        //    if (!string.IsNullOrEmpty(S.MaidenName))
        //        S.MaidenName = S.MaidenName.Replace("'", "''");
        //    if (!string.IsNullOrEmpty(S.FirstName))
        //        S.FirstName = S.FirstName.Replace("'", "''");
        //    if (!string.IsNullOrEmpty(S.SecondName))
        //        S.SecondName = S.SecondName.Replace("'", "''");
        //    if (!string.IsNullOrEmpty(S.BusBusinessName))
        //        S.BusBusinessName = S.BusBusinessName.Replace("'", "''");
        //    if (!string.IsNullOrEmpty(S.ErrorDescription))
        //        S.ErrorDescription = S.ErrorDescription.Replace("'", "''");
        //    if (!string.IsNullOrEmpty(S.SubscriberName))
        //        S.SubscriberName = S.SubscriberName.Replace("'", "''");
        //    if (!string.IsNullOrEmpty(S.SystemUser))
        //        S.SystemUser = S.SystemUser.Replace("'", "''");
        //    if (!string.IsNullOrEmpty(S.SubscriberReference))
        //        S.SubscriberReference = S.SubscriberReference.Replace("'", "''");
        //    if (!string.IsNullOrEmpty(S.EnquiryReason))
        //        S.EnquiryReason = S.EnquiryReason.Replace("'", "''");
            
        //    string insertSQL = "EXEC sp_i_SubscriberEnquiry " + S.SubscriberID + " , '" + S.SubscriberName + "' , " + S.SystemUserID + " , '" + S.SystemUser + "' , '" + S.EnquiryReason + "' , '" + S.SubscriberEnquiryDate + "' , '" + S.SubscriberReference + "' , '" + S.EnquiryStatus + "' , '" + S.EnquiryResult + "' , " + S.ProductID + " , '" + S.SearchInput + "' , '" + S.IDNo + "' , '" + S.PassportNo + "' , '" + S.Surname + "' , '" + S.FirstInitial + "' , '" + S.SecondInitial + "' , '" + S.MaidenName + "' , '" + S.Gender + "' , '" + S.FirstName + "' , '" + S.SecondName + "' , '" + S.BirthDate + "' , '" + S.TelephoneCode + "' , '" + S.TelephoneNo + "' , '" + S.AccountNo + "' , '" + S.SubAccountNo + "' , '" + S.BusRegistrationNo + "' , '" + S.BusBusinessName + "' , '" + S.BusVatNumber + "' , '" + S.AgeSearchTypeInd + "' , " + S.Age + " , " + S.AgeBirthYear + " , " + S.AgeDeviation + " , '" + S.ErrorDescription + "' , '" + S.ExtraVarInput1 + "' , '" + S.ExtraVarInput2 + "' , '" + S.ExtraVarInput3 + "' , " + S.ExtraIntInput1 + " , " + S.ExtraIntInput2 + " , '" + S.BranchCode + "' , " + S.PurposeID + " , '" + S.CreatedByUser + "' , '" + S.CreatedOnDate + "' , '" + S.ChangedByUser + "' , '" + S.ChangedOnDate + "' ";
        //    SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

        //    int added = 0;
        //    added = sqlcmd.ExecuteNonQuery();

        //    string strSQL = "SELECT @@IDENTITY";
        //    SqlCommand Imycommand = new SqlCommand(strSQL, cn);
        //    SqlDataReader Idr = Imycommand.ExecuteReader();
        //    Idr.Read();
        //    int intSubscriberEnquiryID = Convert.ToInt32(Idr.GetValue(0));
        //    Idr.Close();

        //    sqlcmd.Dispose();
        //    Imycommand.Dispose();
        //    Idr.Dispose();
        //    cn.Close();

        //    return intSubscriberEnquiryID;
        //}

        public int InsertSubscriberEnquiry(SqlConnection cn, Entity.SubscriberEnquiry S)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            if (!string.IsNullOrEmpty(S.SearchInput))
                S.SearchInput = S.SearchInput.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.Surname))
                S.Surname = S.Surname.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.MaidenName))
                S.MaidenName = S.MaidenName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.FirstName))
                S.FirstName = S.FirstName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SecondName))
                S.SecondName = S.SecondName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.BusRegistrationNo))
                S.BusRegistrationNo = S.BusRegistrationNo.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.BusBusinessName))
                S.BusBusinessName = S.BusBusinessName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.ErrorDescription))
                S.ErrorDescription = S.ErrorDescription.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SubscriberName))
                S.SubscriberName = S.SubscriberName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SystemUser))
                S.SystemUser = S.SystemUser.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SubscriberReference))
                S.SubscriberReference = S.SubscriberReference.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.EnquiryReason))
                S.EnquiryReason = S.EnquiryReason.Replace("'", "''");

            string insertSQL = "EXEC sp_i_SubscriberEnquiry " + S.SubscriberID + " , '" + S.SubscriberName + "' , " + S.SystemUserID + " , '" + S.SystemUser + "' , '" + S.EnquiryReason + "' , '" + S.SubscriberEnquiryDate + "' , '" + S.SubscriberReference + "' , '" + S.EnquiryStatus + "' , '" + S.EnquiryResult + "' , " + S.ProductID + " , '" + S.SearchInput + "' , '" + S.IDNo + "' , '" + S.PassportNo + "' , '" + S.Surname + "' , '" + S.FirstInitial + "' , '" + S.SecondInitial + "' , '" + S.MaidenName + "' , '" + S.Gender + "' , '" + S.FirstName + "' , '" + S.SecondName + "' , '" + S.BirthDate + "' , '" + S.TelephoneCode + "' , '" + S.TelephoneNo + "' , '" + S.AccountNo + "' , '" + S.SubAccountNo + "' , '" + S.BusRegistrationNo + "' , '" + S.BusBusinessName + "' , '" + S.BusVatNumber + "' , '" + S.AgeSearchTypeInd + "' , " + S.Age + " , " + S.AgeBirthYear + " , " + S.AgeDeviation + " , '" + S.ErrorDescription + "' , '" + S.ExtraVarInput1 + "' , '" + S.ExtraVarInput2 + "' , '" + S.ExtraVarInput3 + "' , " + S.ExtraIntInput1 + " , " + S.ExtraIntInput2 + " , '" + S.BranchCode + "' , " + S.PurposeID + " , '" + S.CreatedByUser + "' , '" + S.CreatedOnDate + "' , '" + S.ChangedByUser + "' , '" + S.ChangedOnDate + "','" + S.NetMonthlyIncomeAmt + "'";
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);
            sqlcmd.CommandTimeout = 0;

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();

            string strSQL = "SELECT @@IDENTITY";
            SqlCommand Imycommand = new SqlCommand(strSQL, cn);
            SqlDataReader Idr = Imycommand.ExecuteReader();
            Idr.Read();
            int intSubscriberEnquiryID = Convert.ToInt32(Idr.GetValue(0));
            Idr.Close();

            strSQL = "Exec spGetDataSegments " + S.SubscriberID + "," + S.ProductID + ",'" + S.SubscriberReference +"'";
            Imycommand = new SqlCommand(strSQL, cn);
            Imycommand.CommandTimeout = 0;
            SqlDataAdapter da = new SqlDataAdapter(Imycommand);
            DataSet ds = new DataSet("DataSegments");
            da.Fill(ds);
            ds.Tables[0].TableName = "Segments";
            string xml = string.Empty;
            xml = ds.GetXml();


            insertSQL = "Update SubscriberEnquiry set ExtraVarInput2 = '" + xml + "' where SubscriberEnquiryID = " + intSubscriberEnquiryID;
            sqlcmd = new SqlCommand(insertSQL, cn);

            added = sqlcmd.ExecuteNonQuery();

            sqlcmd.Dispose();
            Imycommand.Dispose();
            Idr.Dispose();
            cn.Close();
            ds.Dispose();

            return intSubscriberEnquiryID;
        }

        public void InsertSubscriberCreditAssesmentEnquiry(SqlConnection cn, Entity.SubscriberEnquiry S, string SubscriberEnquiryID, string strMaritalStatus, string strSpouseFirstName, string strSpouseSurname, string strResAddress1, string strResAddress2, string strResAddress3, string strResAddress4, string strResAddressPC, string strPostAddress1, string strPostAddress2, string strPostAddress3, string strPostAddress4, string strPostAddressPC, string strHomeTelCode, string strHomeTelNo, string strWorkTelCode, string strWorkTelNo, string strCellCode, string strCellNo, string strEmail, decimal strTotalNetMonthlyIncome, string strEmployer, string strJobTitle)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            if (!string.IsNullOrEmpty(S.SearchInput))
                S.SearchInput = S.SearchInput.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.Surname))
                S.Surname = S.Surname.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.MaidenName))
                S.MaidenName = S.MaidenName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.FirstName))
                S.FirstName = S.FirstName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SecondName))
                S.SecondName = S.SecondName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.BusRegistrationNo))
                S.BusRegistrationNo = S.BusRegistrationNo.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.BusBusinessName))
                S.BusBusinessName = S.BusBusinessName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.ErrorDescription))
                S.ErrorDescription = S.ErrorDescription.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SubscriberName))
                S.SubscriberName = S.SubscriberName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SystemUser))
                S.SystemUser = S.SystemUser.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SubscriberReference))
                S.SubscriberReference = S.SubscriberReference.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.EnquiryReason))
                S.EnquiryReason = S.EnquiryReason.Replace("'", "''");

            string insertSQL = "EXEC sp_i_SubscriberCreditAssesmentEnquiry " + SubscriberEnquiryID + ",  " + S.SubscriberID + ", '" + S.SubscriberName + "' , " + S.SystemUserID + " , '" + S.SystemUser + "' , '" + S.EnquiryReason + "' , '" + S.SubscriberEnquiryDate + "' , '" + S.SubscriberReference + "' , " + S.ProductID + " , '" + S.SearchInput + "' , '" + S.IDNo + "' , '" + S.PassportNo + "' , '" + S.Surname + "' , '" + S.FirstInitial + "' , '" + S.SecondInitial + "' , '" + S.MaidenName + "' , '" + S.Gender + "' , '" + S.FirstName + "' , '" + S.SecondName + "' , '" + S.BirthDate + "' , '" + S.TelephoneCode + "' , '" + S.TelephoneNo + "' , '" + strMaritalStatus.Replace("'", "''") + "', '" + strSpouseFirstName.Replace("'", "''") + "', '" + strSpouseSurname.Replace("'", "''") + "', '" + strResAddress1.Replace("'", "''") + "', '" + strResAddress2.Replace("'", "''") + "', '" + strResAddress3.Replace("'", "''") + "', '" + strResAddress4.Replace("'", "''") + "', '" + strResAddressPC.Replace("'", "''") + "', '" + strPostAddress1.Replace("'", "''") + "', '" + strPostAddress2.Replace("'", "''") + "', '" + strPostAddress3.Replace("'", "''") + "', '" + strPostAddress4.Replace("'", "''") + "', '" + strPostAddressPC.Replace("'", "''") + "', '" + strHomeTelCode.Replace("'", "''") + "', '" + strHomeTelNo.Replace("'", "''") + "', '" + strWorkTelCode.Replace("'", "''") + "', '" + strWorkTelNo.Replace("'", "''") + "', '" + strCellCode.Replace("'", "''") + "', '" + strCellNo.Replace("'", "''") + "', '" + strEmail.Replace("'", "''") + "', " + strTotalNetMonthlyIncome + ", '" + strEmployer.Replace("'", "''") + "', '" + strJobTitle.Replace("'", "''") + "', '" + S.CreatedByUser + "' , '" + S.CreatedOnDate + "' , '" + S.ChangedByUser + "' , '" + S.ChangedOnDate + "'";
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);
            sqlcmd.CommandTimeout = 0;
            
            sqlcmd.ExecuteNonQuery();

            sqlcmd.Dispose();
            cn.Close();
        }
        
        public int UpdateSubscriberEnquiry(string DBConnection, Entity.SubscriberEnquiry S)
        {
            if (!string.IsNullOrEmpty(S.SearchInput))
                S.SearchInput = S.SearchInput.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.Surname))
                S.Surname = S.Surname.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.MaidenName))
                S.MaidenName = S.MaidenName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.FirstName))
                S.FirstName = S.FirstName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SecondName))
                S.SecondName = S.SecondName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.BusRegistrationNo))
                S.BusRegistrationNo = S.BusRegistrationNo.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.BusBusinessName))
                S.BusBusinessName = S.BusBusinessName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.ErrorDescription))
                S.ErrorDescription = S.ErrorDescription.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SubscriberName))
                S.SubscriberName = S.SubscriberName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SystemUser))
                S.SystemUser = S.SystemUser.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SubscriberReference))
                S.SubscriberReference = S.SubscriberReference.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.EnquiryReason))
                S.EnquiryReason = S.EnquiryReason.Replace("'", "''");
            
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = DBConnection;
            cn.Open();

            string insertSQL = "EXEC spUpdateSubscriberEnquiry " + S.SubscriberEnquiryID + " , " + S.SubscriberID + " , '" + S.SubscriberName + "' , " + S.SystemUserID + " , '" + S.SubscriberEnquiryDate + "' , '" + S.SubscriberReference + "' , '" + S.EnquiryStatus + "' , '" + S.EnquiryResult + "' , " + S.ProductID + " , '" + S.IDNo + "' , '" + S.PassportNo + "' , '" + S.Surname + "' , '" + S.FirstInitial + "' , '" + S.SecondInitial + "' , '" + S.MaidenName + "' , '" + S.Gender + "' , '" + S.FirstName + "' , '" + S.SecondName + "' , '" + S.BirthDate + "' , '" + S.TelephoneCode + "' , '" + S.TelephoneNo + "' , '" + S.AccountNo + "' , '" + S.SubAccountNo + "'";
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();
            cn.Close();

            sqlcmd.Dispose();
            return added;
        }


        public int UpdateSubscriberEnquiry(SqlConnection cn, Entity.SubscriberEnquiry S)
        {

            if (cn.State == ConnectionState.Closed)
            cn.Open();

            if (!string.IsNullOrEmpty(S.SearchInput))
                S.SearchInput = S.SearchInput.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.Surname))
                S.Surname = S.Surname.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.MaidenName))
                S.MaidenName = S.MaidenName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.FirstName))
                S.FirstName = S.FirstName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SecondName))
                S.SecondName = S.SecondName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.BusRegistrationNo))
                S.BusRegistrationNo = S.BusRegistrationNo.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.BusBusinessName))
                S.BusBusinessName = S.BusBusinessName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.ErrorDescription))
                S.ErrorDescription = S.ErrorDescription.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SubscriberName))
                S.SubscriberName = S.SubscriberName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SystemUser))
                S.SystemUser = S.SystemUser.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SubscriberReference))
                S.SubscriberReference = S.SubscriberReference.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.EnquiryReason))
                S.EnquiryReason = S.EnquiryReason.Replace("'", "''");

            string insertSQL = "EXEC sp_u_SubscriberEnquiry " + S.SubscriberEnquiryID + " , " + S.SubscriberID + " , '" + S.SubscriberName + "' , " + S.SystemUserID + " , '" + S.SystemUser + "' , '" + S.EnquiryReason + "' , '" + S.SubscriberEnquiryDate + "' , '" + S.SubscriberReference + "' , '" + S.EnquiryStatus + "' , '" + S.EnquiryResult + "' , " + S.ProductID + " , '" + S.SearchInput + "' , '" + S.IDNo + "' , '" + S.PassportNo + "' , '" + S.Surname + "' , '" + S.FirstInitial + "' , '" + S.SecondInitial + "' , '" + S.MaidenName + "' , '" + S.Gender + "' , '" + S.FirstName + "' , '" + S.SecondName + "' , '" + S.BirthDate + "' , '" + S.TelephoneCode + "' , '" + S.TelephoneNo + "' , '" + S.AccountNo + "' , '" + S.SubAccountNo + "' , '" + S.BusRegistrationNo + "' , '" + S.BusBusinessName + "' , '" + S.BusVatNumber + "' , '" + S.AgeSearchTypeInd + "' , " + S.Age + " , " + S.AgeBirthYear + " , " + S.AgeDeviation + " , '" + S.ErrorDescription + "' , '" + S.ExtraVarInput1 + "' , '" + S.ExtraVarInput2 + "' , '" + S.ExtraVarInput3 + "' , " + S.ExtraIntInput1 + " , " + S.ExtraIntInput2 + " , '" + S.BranchCode + "' , " + S.PurposeID + " , '" + S.CreatedByUser + "' , '" + S.CreatedOnDate + "' , '" + S.ChangedByUser + "' , '" + S.ChangedOnDate + "'";
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();

            sqlcmd.Dispose();

            cn.Close();
            return added;

        }


        public int UpdateSubscriberEnquiryError(string DBConnection, Entity.SubscriberEnquiry S)
        {

            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = DBConnection;
            cn.Open();

            string insertSQL = "UPDATE SubscriberEnquiry SET EnquiryStatusInd = 'F', EnquiryResultInd = 'E', ErrorDescription = '" + S.ErrorDescription + "' WHERE SubscriberEnquiryID = " + S.SubscriberEnquiryID.ToString();
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();
            cn.Close();

            sqlcmd.Dispose();
            return added;
        }

        
        public int UpdateSubscriberEnquiryError(SqlConnection cn, Entity.SubscriberEnquiry S)
        {

            if (cn.State == ConnectionState.Closed)
                cn.Open(); 
            
            string insertSQL = "UPDATE SubscriberEnquiry SET EnquiryStatusInd = 'F', EnquiryResultInd = 'E', ErrorDescription = '" + S.ErrorDescription.Replace("'","''") + "' WHERE SubscriberEnquiryID = " + S.SubscriberEnquiryID.ToString();
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();

            sqlcmd.Dispose();
            cn.Close();

            return added;
        }

        public DataSet GetSubscriberEnquiryDataSet(string DBConnection, int intSubscriberEnquiryID)
        {
            
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = DBConnection;
            cn.Open();

            string strSQL = "Select * from SubscriberEnquiry nolock Where SubscriberEnquiryID = " + intSubscriberEnquiryID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            sqlcmd.Dispose();
            sqlda.Dispose();
            cn.Close();
            return ds;
        }

        public DataSet GetSubscriberEnquiryDataSet(SqlConnection cn, int intSubscriberEnquiryID)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string strSQL = "Select * from SubscriberEnquiry nolock Where SubscriberEnquiryID = " + intSubscriberEnquiryID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            sqlda.Dispose();
            sqlcmd.Dispose();
            cn.Close();

            return ds;
        }

        public Entity.SubscriberEnquiry GetSubscriberEnquiryObject(string DBConnection, int intSubscriberEnquiryID)
        {
            
            Entity.SubscriberEnquiry se = new Entity.SubscriberEnquiry();

            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = DBConnection;
            cn.Open();

            string strSQL = "Select * from SubscriberEnquiry nolock Where SubscriberEnquiryID = " + intSubscriberEnquiryID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                se.SubscriberEnquiryID = int.Parse(r["SubscriberEnquiryID"].ToString());
                se.EnquiryStatus = r["EnquiryStatusInd"].ToString();
                se.EnquiryResult = r["EnquiryResultInd"].ToString();
                se.ErrorDescription = r["ErrorDescription"].ToString();
                se.ProductID = int.Parse(r["ProductID"].ToString());
                se.SubscriberID = int.Parse(r["SubscriberID"].ToString());
                se.SystemUserID = int.Parse(r["SystemUserID"].ToString());
                se.SystemUser = r["SystemUser"].ToString();
                se.EnquiryReason = r["EnquiryReason"].ToString();
                se.CreatedByUser = r["CreatedByUser"].ToString();
                se.CreatedOnDate = DateTime.Parse(r["CreatedOnDate"].ToString());
                se.SearchInput = r["SearchInput"].ToString();
                se.ExtraIntInput1 = int.Parse(r["ExtraIntInput1"].ToString());
                se.ExtraIntInput2 = int.Parse(r["ExtraIntInput2"].ToString());
                se.ExtraVarInput1 = r["ExtraVarInput1"].ToString();
                se.ExtraVarInput2 = r["ExtraVarInput2"].ToString();
                se.ExtraVarInput3 = r["ExtraVarInput3"].ToString();
                se.BranchCode = r["BranchCode"].ToString();
                se.PurposeID = int.Parse(r["PurposeID"].ToString());
                se.SubscriberReference = r["SubscriberReference"].ToString();
                se.IDNo = r["IDNo"].ToString();
                se.Surname = r["Surname"].ToString();
                se.BusRegistrationNo = r["BusRegistrationNo"].ToString();
                se.BusBusinessName = r["BusBusinessName"].ToString();
                se.MaidenName = r["MaidenName"].ToString();
                se.AccountNo = r["AccountNo"].ToString();
                se.FirstName = r["FirstName"].ToString();
                se.NetMonthlyIncomeAmt = decimal.Parse(r["NetMonthlyIncomeAmt"].ToString());
            }
            sqlcmd.Dispose();
            sqlda.Dispose();
            ds.Dispose();
            cn.Close();
            return se;
        }

        public Entity.SubscriberEnquiry GetSubscriberEnquiryObject(SqlConnection cn, int intSubscriberEnquiryID)
        {

            Entity.SubscriberEnquiry se = new Entity.SubscriberEnquiry();

            if (cn.State == ConnectionState.Closed)
                cn.Open();
            
            string strSQL = "Select * from SubscriberEnquiry nolock Where SubscriberEnquiryID = " + intSubscriberEnquiryID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                se.SubscriberEnquiryID = int.Parse(r["SubscriberEnquiryID"].ToString());
                se.EnquiryStatus = r["EnquiryStatusInd"].ToString();
                se.EnquiryResult = r["EnquiryResultInd"].ToString();
                se.ErrorDescription = r["ErrorDescription"].ToString();
                se.ProductID = int.Parse(r["ProductID"].ToString());
                se.SubscriberID = int.Parse(r["SubscriberID"].ToString());
                se.SystemUserID = int.Parse(r["SystemUserID"].ToString());
                se.SystemUser = r["SystemUser"].ToString();
                se.EnquiryReason = r["EnquiryReason"].ToString();
                se.CreatedByUser = r["CreatedByUser"].ToString();
                se.CreatedOnDate = DateTime.Parse(r["CreatedOnDate"].ToString());
                se.SearchInput = r["SearchInput"].ToString();
                se.ExtraIntInput1 = int.Parse(r["ExtraIntInput1"].ToString());
                se.ExtraIntInput2 = int.Parse(r["ExtraIntInput2"].ToString());
                se.ExtraVarInput1 = r["ExtraVarInput1"].ToString();
                se.ExtraVarInput2 = r["ExtraVarInput2"].ToString();
                se.ExtraVarInput3 = r["ExtraVarInput3"].ToString();
                se.BranchCode = r["BranchCode"].ToString();
                se.PurposeID = int.Parse(r["PurposeID"].ToString());
                se.SubscriberReference = r["SubscriberReference"].ToString();
                se.IDNo = r["IDNo"].ToString();
                se.PassportNo = r["PassportNo"].ToString();
                se.BusRegistrationNo = r["BusRegistrationNo"].ToString(); se.Surname = r["Surname"].ToString();
                se.BusBusinessName = r["BusBusinessName"].ToString();
                se.MaidenName = r["MaidenName"].ToString();
                se.AccountNo = r["AccountNo"].ToString();
                se.FirstName = r["FirstName"].ToString();
                se.NetMonthlyIncomeAmt = decimal.Parse(r["NetMonthlyIncomeAmt"].ToString());
                se.SubscriberEnquiryDate = DateTime.Parse(r["SubscriberEnquiryDate"].ToString());
                se.SubscriberName = r["Subscribername"].ToString();
                se.AccountNo = r["Accountno"].ToString();
                se.BusRegistrationNo = r["BusRegistrationNo"].ToString();
                se.BusBusinessName = r["BusBusinessName"].ToString();
                se.BusVatNumber = r["BusVatNumber"].ToString();
                se.TelephoneNo= r["TelephoneNo"].ToString();
                se.TelephoneCode = r["TelephoneCode"].ToString();
            }
            sqlda.Dispose();
            sqlcmd.Dispose();
            ds.Dispose();

            cn.Close();
            return se;
        }

        public Entity.SubscriberEnquiry ErrorGetSubscriberEnquiryObject(SqlConnection cn, int intSubscriberEnquiryID)
        {

            Entity.SubscriberEnquiry se = new Entity.SubscriberEnquiry();

            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string strSQL = "EXEC [sp_u_ErrorSubscriberEnquiry] " + intSubscriberEnquiryID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                se.SubscriberEnquiryID = int.Parse(r["SubscriberEnquiryID"].ToString());
                se.EnquiryStatus = r["EnquiryStatusInd"].ToString();
                se.EnquiryResult = r["EnquiryResultInd"].ToString();
                se.ErrorDescription = r["ErrorDescription"].ToString();
                se.ProductID = int.Parse(r["ProductID"].ToString());
                se.SubscriberID = int.Parse(r["SubscriberID"].ToString());
                se.SystemUserID = int.Parse(r["SystemUserID"].ToString());
                se.SystemUser = r["SystemUser"].ToString();
                se.EnquiryReason = r["EnquiryReason"].ToString();
                se.CreatedByUser = r["CreatedByUser"].ToString();
                se.CreatedOnDate = DateTime.Parse(r["CreatedOnDate"].ToString());
                se.SearchInput = r["SearchInput"].ToString();
                se.ExtraIntInput1 = int.Parse(r["ExtraIntInput1"].ToString());
                se.ExtraIntInput2 = int.Parse(r["ExtraIntInput2"].ToString());
                se.ExtraVarInput1 = r["ExtraVarInput1"].ToString();
                se.ExtraVarInput2 = r["ExtraVarInput2"].ToString();
                se.ExtraVarInput3 = r["ExtraVarInput3"].ToString();
                se.BranchCode = r["BranchCode"].ToString();
                se.PurposeID = int.Parse(r["PurposeID"].ToString());
                se.SubscriberReference = r["SubscriberReference"].ToString();
                se.IDNo = r["IDNo"].ToString();
                se.Surname = r["Surname"].ToString();
                se.BusRegistrationNo = r["BusRegistrationNo"].ToString(); se.BusRegistrationNo = r["BusRegistrationNo"].ToString();
                se.BusBusinessName = r["BusBusinessName"].ToString();
                se.MaidenName = r["MaidenName"].ToString();
                se.AccountNo = r["AccountNo"].ToString();
                se.FirstName = r["FirstName"].ToString();
                se.NetMonthlyIncomeAmt = decimal.Parse(r["NetMonthlyIncomeAmt"].ToString());
            }
            sqlda.Dispose();
            sqlcmd.Dispose();
            ds.Dispose();

            cn.Close();
            return se;
        }

    }
}
