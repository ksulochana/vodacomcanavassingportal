﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace XDSPortalEnquiry.Data
{
    public class Subscriber
    {
        public Subscriber()
        {
        }
        public Entity.Subscriber GetSubscriberRecord(SqlConnection conString, int SubscriberID)
        {
            Entity.Subscriber objSubscriber = new Entity.Subscriber();

            //SqlConnection ObjConstring = new SqlConnection(conString);
            //ObjConstring.Open();

            if (conString.State == ConnectionState.Closed)
                conString.Open();

            string sqlSelect = "SELECT   Subscriber.* , SubscriberBusinessTypeDesc AS SubscriberBusinessType  FROM Subscriber (NOLOCK) LEFT OUTER JOIN SubscriberBusinessType (NOLOCK) ON Subscriber.SubscriberBusinessTypeCode = SubscriberBusinessType.SubscriberBusinessTypeCode  where SubscriberID = " + SubscriberID;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, conString);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSubscriber.SubscriberID = Convert.ToInt32(Dr["SubscriberID"]);
                objSubscriber.SubscriberName = Dr["SubscriberName"].ToString();
                objSubscriber.SubscriberBusinessType = Dr["SubscriberBusinessType"].ToString();
                objSubscriber.SubscriberTypeInd = Dr["SubscriberTypeInd"].ToString();
                objSubscriber.StatusInd = Dr["StatusInd"].ToString();
                if (Convert.ToBoolean(Dr["PayAsYouGo"].ToString())) { objSubscriber.PayAsYouGo = 1; } else {objSubscriber.PayAsYouGo = 0;}
                objSubscriber.PayAsyouGoEnquiryLimit = Convert.ToDouble(Dr["PayAsYouGoEnquiryLimit"].ToString());
                if (!string.IsNullOrEmpty(Dr["CompanyTelephoneCode"].ToString())) { objSubscriber.CompanyTelephoneCode = Dr["CompanyTelephoneCode"].ToString(); }
                if (!string.IsNullOrEmpty(Dr["CompanyTelephoneNo"].ToString())) { objSubscriber.CompanyTelephoneNo = Dr["CompanyTelephoneNo"].ToString(); }
                if (!string.IsNullOrEmpty(Dr["SubscriberBusinessTypeCode"].ToString())) { objSubscriber.SubscriberBusinessTypeCode = Dr["SubscriberBusinessTypeCode"].ToString(); }
                if (!string.IsNullOrEmpty(Dr["SubscriberAssociationCode"].ToString())) { objSubscriber.SubscriberAssociationCode = Dr["SubscriberAssociationCode"].ToString(); }
                if (!string.IsNullOrEmpty(Dr["AssociationTypeCode"].ToString())) { objSubscriber.AssociationTypeCode = Dr["AssociationTypeCode"].ToString(); }
                if (!string.IsNullOrEmpty(Dr["SubscriberGroupCode"].ToString())) { objSubscriber.SubscriberGroupCode = Dr["SubscriberGroupCode"].ToString(); }
                if (!string.IsNullOrEmpty(Dr["SAFPSEnabled"].ToString())) { bool SAFPSInd = false;
                    if (bool.TryParse(Dr["SAFPSEnabled"].ToString(),out SAFPSInd))
                    {
                        objSubscriber.SAFPSIndicator = SAFPSInd;
                    }}
            }
            //ObjConstring.Close();

            ds.Dispose();
            Objsqlcom.Dispose();
            objsqladp.Dispose();
            conString.Close();

            SqlConnection.ClearPool(conString);

            return objSubscriber;
       
        }

        public Entity.Subscriber GetSubscriberRecord(string conString, int SubscriberID)
        {
            Entity.Subscriber objSubscriber = new Entity.Subscriber();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "SELECT   Subscriber.* , SubscriberBusinessTypeDesc AS SubscriberBusinessType  FROM Subscriber (NOLOCK) LEFT OUTER JOIN SubscriberBusinessType (NOLOCK) ON Subscriber.SubscriberBusinessTypeCode = SubscriberBusinessType.SubscriberBusinessTypeCode  where SubscriberID = " + SubscriberID;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSubscriber.SubscriberID = Convert.ToInt32(Dr["SubscriberID"]);
                objSubscriber.SubscriberName = Dr["SubscriberName"].ToString();
                objSubscriber.SubscriberBusinessType = Dr["SubscriberBusinessType"].ToString();
                objSubscriber.SubscriberTypeInd = Dr["SubscriberTypeInd"].ToString();
                objSubscriber.StatusInd = Dr["StatusInd"].ToString();
                if (Convert.ToBoolean(Dr["PayAsYouGo"].ToString())) { objSubscriber.PayAsYouGo = 1; } else { objSubscriber.PayAsYouGo = 0; }
                objSubscriber.PayAsyouGoEnquiryLimit = Convert.ToDouble(Dr["PayAsYouGoEnquiryLimit"].ToString());
                if (!string.IsNullOrEmpty(Dr["CompanyTelephoneCode"].ToString())) { objSubscriber.CompanyTelephoneCode = Dr["CompanyTelephoneCode"].ToString(); }
                if (!string.IsNullOrEmpty(Dr["CompanyTelephoneNo"].ToString())) { objSubscriber.CompanyTelephoneNo = Dr["CompanyTelephoneNo"].ToString(); }
                if (!string.IsNullOrEmpty(Dr["SubscriberBusinessTypeCode"].ToString())) { objSubscriber.SubscriberBusinessTypeCode = Dr["SubscriberBusinessTypeCode"].ToString(); }
                if (!string.IsNullOrEmpty(Dr["SubscriberAssociationCode"].ToString())) { objSubscriber.SubscriberAssociationCode = Dr["SubscriberAssociationCode"].ToString(); }
            }
            //ObjConstring.Close();
            ds.Dispose();
            Objsqlcom.Dispose();
            objsqladp.Dispose();
            ObjConstring.Close();
            SqlConnection.ClearPool(ObjConstring);
            return objSubscriber;

        }

        public DataSet GetSubscriberProfile(string conString, int SubscriberID)
        {
            Entity.Subscriber objSubscriber = new Entity.Subscriber();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            //string sqlSelect = "SELECT SubscriberID, AuthenticationSearchOnIDNoYN, AuthenticationSearchOnCellPhoneNoYN, AuthenticationSearchOnAccountNoYN from SubscriberProfile (NOLOCK) where SubscriberID = " + SubscriberID;
            string sqlSelect = "spAuthentication_Get_SubscriberProfile";

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            Objsqlcom.CommandType = CommandType.StoredProcedure;
            Objsqlcom.Parameters.AddWithValue("@SubscriberID",SubscriberID);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            
            //ObjConstring.Close();
            ds.Dispose();
            Objsqlcom.Dispose();
            objsqladp.Dispose();
            ObjConstring.Close();

            return ds;

        }

        public DataSet GetBranches(string conString, int SubscriberID)
        {
            XDSSettings oXDSSettings = new XDSSettings();
            string AdminConnectionString = string.Empty;
           
            
            SqlConnection ObjConstring = new SqlConnection(conString);

            AdminConnectionString = oXDSSettings.GetConnection(ObjConstring, 1);

            SqlConnection oAuthCon = new SqlConnection(AdminConnectionString);

            if (oAuthCon.State == ConnectionState.Closed)
                oAuthCon.Open();

//            string sqlSelect = "SELECT SubscriberID, SubscriberBranchCode as BranchCode, SubscriberBranchDesc as BranchName from SubscriberProfileProductAuthenticationBranch (NOLOCK) where SubscriberID = " + SubscriberID;

            string sqlSelect = "SELECT SubscriberID, SubscriberBranchCode as BranchCode, SubscriberBranchDesc as BranchName from Auth_S_Branch (NOLOCK) where SubscriberID = " + SubscriberID + " and Statusind ='A'";

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, oAuthCon);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            if (oAuthCon.State == ConnectionState.Open)
                oAuthCon.Close();

            //ObjConstring.Close();
            ds.Dispose();
            Objsqlcom.Dispose();
            objsqladp.Dispose();
            ObjConstring.Close();

            return ds;

        }

        public DataSet GetPurposes(string conString, int SubscriberID)
        {
            XDSSettings oXDSSettings = new XDSSettings();
            string AdminConnectionString = string.Empty;
            
            SqlConnection ObjConstring = new SqlConnection(conString);
            AdminConnectionString = oXDSSettings.GetConnection(ObjConstring, 1);

            SqlConnection oAuthCon = new SqlConnection(AdminConnectionString);

            if (oAuthCon.State == ConnectionState.Closed)
                oAuthCon.Open();

//            string sqlSelect = "SELECT SubscriberID, AuthenticationPurposeID as PurposeID, AuthenticationPurposeDesc as Purpose from SubscriberProfileProductAuthenticationPurpose (NOLOCK) where SubscriberID = " + SubscriberID;

            string sqlSelect = "SELECT SubscriberID, AuthenticationPurposeID as PurposeID, AuthenticationPurposeDesc as Purpose from Auth_S_PurPose (NOLOCK) where SubscriberID = " + SubscriberID + " and StatusInd = 'A'";

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, oAuthCon);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            if (oAuthCon.State == ConnectionState.Open)
                oAuthCon.Close();

            //ObjConstring.Close();
            ds.Dispose();
            Objsqlcom.Dispose();
            objsqladp.Dispose();
            ObjConstring.Close();

            return ds;

        }


        public List<Entity.Subscriber> GetSubscriberRecord(string conString, string whereclause)
        {
            List<Entity.Subscriber> ObjSubscriberList = new List<Entity.Subscriber>();
            Entity.Subscriber objSubscriber = new Entity.Subscriber();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "select * from Subscriber " + whereclause;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSubscriber.SubscriberID = Convert.ToInt32(Dr["SubscriberID"]);
                objSubscriber.SubscriberName = Dr["SubscriberName"].ToString();
                objSubscriber.SubscriberBusinessType = Dr["SubscriberBusinessType"].ToString();
                objSubscriber.SubscriberTypeInd = Dr["SubscriberTypeInd"].ToString();
                objSubscriber.StatusInd = Dr["StatusInd"].ToString();
                if (!string.IsNullOrEmpty(Dr["CompanyTelephoneCode"].ToString())) { objSubscriber.CompanyTelephoneCode = Dr["CompanyTelephoneCode"].ToString(); }
                if (!string.IsNullOrEmpty(Dr["CompanyTelephoneNo"].ToString())) { objSubscriber.CompanyTelephoneNo = Dr["CompanyTelephoneNo"].ToString(); }
                if (!string.IsNullOrEmpty(Dr["SubscriberBusinessTypeCode"].ToString())) { objSubscriber.SubscriberBusinessTypeCode = Dr["SubscriberBusinessTypeCode"].ToString(); }
                if (!string.IsNullOrEmpty(Dr["SubscriberAssociationCode"].ToString())) { objSubscriber.SubscriberAssociationCode = Dr["SubscriberAssociationCode"].ToString(); }

                ObjSubscriberList.Add(objSubscriber);
            }

            ObjConstring.Close();
            ds.Dispose();
            Objsqlcom.Dispose();
            objsqladp.Dispose();
            return ObjSubscriberList;
        }
        public XDSPortalEnquiry.Entity.Subscriber GetPayAsYouGoEnquiryLimit(string conString, int SubscriberID)
        {
            XDSPortalEnquiry.Entity.Subscriber objSubscriberPayAsYouGo = new XDSPortalEnquiry.Entity.Subscriber();
            SqlConnection objConstring = new SqlConnection(conString);
            string sqlSelect = "select SubscriberID,PayAsYouGo,PayAsYouGoEnquiryLimit from Subscriber nolock where SubscriberID = " + SubscriberID;
            objConstring.Open();

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, objConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSubscriberPayAsYouGo.SubscriberID = Convert.ToInt32(Dr["SubscriberID"]);
                objSubscriberPayAsYouGo.PayAsYouGo = Convert.ToInt32(Dr["PayAsYouGo"]);
                objSubscriberPayAsYouGo.PayAsyouGoEnquiryLimit = Convert.ToDouble(Dr["PayAsYouGoEnquiryLimit"]);

            }
            objConstring.Close();
            ds.Dispose();
            Objsqlcom.Dispose();
            objsqladp.Dispose();
            return objSubscriberPayAsYouGo;
        }

        public int UpdatePayAsYouGoEnquiryLimit(string conString, int SubscriberID, double unitPrice)
        {
            int Status = 0;
            SqlConnection objConstring = new SqlConnection(conString);
            string sqlSelect = "Exec sp_u_Subscriber " + SubscriberID + "," + unitPrice;
            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, objConstring);
            objConstring.Open();
            Status = Objsqlcom.ExecuteNonQuery();
            objConstring.Close();

            Objsqlcom.Dispose();

            return Status;
        }

        public int UpdatePayAsYouGoEnquiryLimit(SqlConnection objConstring, int SubscriberID, double unitPrice)
        {

            if (objConstring.State == ConnectionState.Closed)
                objConstring.Open();
            

            int Status = 0;
           
            string sqlSelect = "Exec sp_u_Subscriber " + SubscriberID + "," + unitPrice;
            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, objConstring);
                        
            Status = Objsqlcom.ExecuteNonQuery();
            Objsqlcom.Dispose();
            objConstring.Close();
            return Status;
        }
       

    }
}

