using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace XdsPortalReports
{
    public partial class AccountVerificationReport_Old : DevExpress.XtraReports.UI.XtraReport
    {
        public AccountVerificationReport_Old(string strXML)
        {
            InitializeComponent();
            System.IO.StringReader Objsr = new System.IO.StringReader(strXML);
            DataSet dsXML = new DataSet();
            dsXML.ReadXml(Objsr);
            string IDNo = string.Empty;

            this.DataSource = dsXML;
            if (dsXML.Tables.Contains("SubscriberInputDetails"))
            {
                dsXML.Tables["SubscriberInputDetails"].Columns.Add("FEnquiryDate", System.Type.GetType("System.DateTime"), "Iif(EnquiryDate = '', '1900-01-01T00:00:00', EnquiryDate)");
                this.lblEnquiryDateValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "SubscriberInputDetails.FEnquiryDate", "{0:dd/MM/yyyy - hh:mm:ss}") });
                IDNo = dsXML.Tables["SubscriberInputDetails"].Rows[0].Field<string>("IDNo").ToString();
                if (IDNo.ToString().IndexOf("/") > 0)
                {
                    lblSurNameC.Text = "Company Name Match";
                    lblIDNoC.Text = "Company Registration Number Match";
                }
                else
                {
                    lblSurNameC.Text = "Surname Match";
                    lblIDNoC.Text = "ID Number Match";

                }
            }


            dsXML.Dispose();
            this.Report.Name = "AccountVerification_RefNo_" + dsXML.Tables["request"].Rows[0].Field<string>("refnumber").ToString()+".pdf";
        }

    }
}
