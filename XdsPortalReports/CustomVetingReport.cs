using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.IO;
using System.Data;
using System.Xml;
using System.Data.SqlClient;

namespace XdsPortalReports
{
    public partial class CustomVetingReport : DevExpress.XtraReports.UI.XtraReport
    {
        public CustomVetingReport(string strXML)
        {
            DataSet dsXML = new DataSet();
            System.IO.StringReader Objsr1;

            if (!string.IsNullOrEmpty(strXML))
            {
                Objsr1 = new System.IO.StringReader(strXML);
                dsXML.ReadXml(Objsr1);
            }

            InitializeComponent();

            if (dsXML.Tables.Contains("CustomVettingResult") && dsXML.Tables["CustomVettingResult"].Columns.Contains("BirthDate"))
            {
                dsXML.Tables["CustomVettingResult"].Columns.Add("FBirthDate", System.Type.GetType("System.DateTime"), "Iif(BirthDate = '', '1900-01-01T00:00:00', BirthDate)");
                this.lblBirthdate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "CustomVettingResult.FBirthDate", "{0:yyyy/MM/dd}") });
            }

            if (dsXML.Tables.Contains("CustomVettingResult") && dsXML.Tables["CustomVettingResult"].Columns.Contains("CreatedOnDate"))
            {
                dsXML.Tables["CustomVettingResult"].Columns.Add("FCreatedOnDate", System.Type.GetType("System.DateTime"), "Iif(CreatedOnDate = '', '1900-01-01T00:00:00', CreatedOnDate)");
                this.lblCreatedOnDate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "CustomVettingResult.FCreatedOnDate", "{0:yyyy/MM/dd hh:mm:ss}") });
            }

            this.DataSource = dsXML;
        }
    }
}