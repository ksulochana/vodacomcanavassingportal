namespace XdsPortalReports
{
    partial class ConsumerPrescreenTraceReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConsumerPrescreenTraceReport));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.TblBusInfo = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport1 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader2 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport2 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail3 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader3 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.DetailReport3 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail4 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader4 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport4 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail5 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable14 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader5 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable13 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable12 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport5 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail6 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable18 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell135 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell139 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader6 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable16 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable15 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport6 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail7 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable20 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell151 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader7 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable19 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell147 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable17 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport8 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail9 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable23 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell165 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell166 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell167 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell168 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader8 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable22 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell156 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell157 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell158 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell161 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell163 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable21 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport9 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail10 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable26 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell183 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell178 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell179 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader9 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable25 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell181 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell175 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell182 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable24 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell173 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport10 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail11 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable30 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell203 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell200 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell191 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell204 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell201 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell192 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell205 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell202 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell193 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader10 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable31 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell206 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell207 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell208 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell209 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell210 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell211 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell212 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell213 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell214 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable28 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable27 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell186 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport11 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail12 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable33 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell217 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell195 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell218 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell196 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell219 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell220 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell197 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader11 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable32 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell198 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell199 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell190 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell215 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell216 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell194 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable29 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell188 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport12 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail13 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable36 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell225 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell227 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell224 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell226 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader12 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable35 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell228 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell222 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell229 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell223 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable34 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell221 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport13 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail14 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable39 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell234 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell235 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell236 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader13 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable38 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell231 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell232 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell233 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable37 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell230 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport7 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail8 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblHomeAffairsH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow53 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell254 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblnote = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow52 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell253 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblHomeAffairsD = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow48 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblXDSDeceasedStatus = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblXDSDeceasedStatusD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblXDSDeceasedDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblXDSDeceasedDateD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow49 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblHAFirstName = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHAFirstNameD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHASurname = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHASurnameD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow50 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblHADeceasedStats = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHADeceasedStatsD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHADeceasedDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHADeceasedDateD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow51 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblHACauseofdeath = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHACauseofdeathD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell251 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell252 = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.TblBusInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblHomeAffairsH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblnote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblHomeAffairsD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Height = 25;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 33;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageFooter.PrintOn = DevExpress.XtraReports.UI.PrintOnPages.NotWithReportHeader;
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.GroupHeader1});
            this.DetailReport.DataMember = "ConsumerDetail";
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.TblBusInfo,
            this.xrTable3});
            this.Detail1.Height = 284;
            this.Detail1.Name = "Detail1";
            // 
            // TblBusInfo
            // 
            this.TblBusInfo.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.TblBusInfo.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TblBusInfo.Location = new System.Drawing.Point(17, 83);
            this.TblBusInfo.Name = "TblBusInfo";
            this.TblBusInfo.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow3,
            this.xrTableRow5,
            this.xrTableRow10,
            this.xrTableRow9,
            this.xrTableRow8,
            this.xrTableRow7});
            this.TblBusInfo.Size = new System.Drawing.Size(656, 175);
            this.TblBusInfo.StylePriority.UseBorderColor = false;
            this.TblBusInfo.StylePriority.UseBorders = false;
            this.TblBusInfo.StylePriority.UseTextAlignment = false;
            this.TblBusInfo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell3,
            this.xrTableCell4});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.Text = "XDS Reference No.";
            this.xrTableCell1.Weight = 0.91130434782608694;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.ReferenceNo", "")});
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseBorderColor = false;
            this.xrTableCell2.Weight = 0.95304347826086955;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.Text = "External Ref No.";
            this.xrTableCell3.Weight = 0.52661028148539724;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.ExternalReference", "")});
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Weight = 0.8547984670278842;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.xrTableCell10,
            this.xrTableCell11,
            this.xrTableCell12});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.Text = "ID No.";
            this.xrTableCell9.Weight = 0.91130434782608694;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.IDNo", "")});
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StylePriority.UseBorderColor = false;
            this.xrTableCell10.Weight = 0.95304347826086955;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.StylePriority.UseFont = false;
            this.xrTableCell11.Text = "Other ID No.";
            this.xrTableCell11.Weight = 0.52661028148539724;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.PassportNo", "")});
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Weight = 0.85479846702788431;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell17,
            this.xrTableCell18,
            this.xrTableCell19,
            this.xrTableCell20});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.StylePriority.UseFont = false;
            this.xrTableCell17.Text = "SurName";
            this.xrTableCell17.Weight = 0.91130434782608694;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.Surname", "")});
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.StylePriority.UseBorderColor = false;
            this.xrTableCell18.Weight = 0.95304347826086955;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.StylePriority.UseFont = false;
            this.xrTableCell19.Text = "Initials";
            this.xrTableCell19.Weight = 0.52661028148539724;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.Initials", "")});
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Weight = 0.8547984670278842;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell37,
            this.xrTableCell38,
            this.xrTableCell39,
            this.xrTableCell40});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.StylePriority.UseFont = false;
            this.xrTableCell37.Text = "First Name";
            this.xrTableCell37.Weight = 0.91130434782608694;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.FirstName", "")});
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.StylePriority.UseBorderColor = false;
            this.xrTableCell38.Weight = 0.95304347826086955;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.StylePriority.UseFont = false;
            this.xrTableCell39.Text = "Second name";
            this.xrTableCell39.Weight = 0.52661028148539724;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.SecondName", "")});
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Weight = 0.8547984670278842;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell33,
            this.xrTableCell34,
            this.xrTableCell35,
            this.xrTableCell36});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.StylePriority.UseFont = false;
            this.xrTableCell33.Text = "Third Name";
            this.xrTableCell33.Weight = 0.91130434782608694;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.ThirdName", "")});
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.StylePriority.UseBorderColor = false;
            this.xrTableCell34.Weight = 0.95304347826086955;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.StylePriority.UseFont = false;
            this.xrTableCell35.Text = "Title";
            this.xrTableCell35.Weight = 0.52661028148539724;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.TitleDesc", "")});
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Weight = 0.8547984670278842;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell29,
            this.xrTableCell30,
            this.xrTableCell31,
            this.xrTableCell32});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.StylePriority.UseFont = false;
            this.xrTableCell29.Text = "Gender";
            this.xrTableCell29.Weight = 0.91130434782608694;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.Gender", "")});
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.StylePriority.UseBorderColor = false;
            this.xrTableCell30.Weight = 0.95304347826086955;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.StylePriority.UseFont = false;
            this.xrTableCell31.Text = "Date of Birth";
            this.xrTableCell31.Weight = 0.52661028148539724;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.BirthDate", "")});
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Weight = 0.8547984670278842;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.xrTableCell26,
            this.xrTableCell27,
            this.xrTableCell28});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.StylePriority.UseFont = false;
            this.xrTableCell25.Text = "Martial Status";
            this.xrTableCell25.Weight = 0.91130434782608694;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.StylePriority.UseBorderColor = false;
            this.xrTableCell26.Weight = 0.95304347826086955;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.StylePriority.UseFont = false;
            this.xrTableCell27.Weight = 0.52661028148539724;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Weight = 0.8547984670278842;
            // 
            // xrTable3
            // 
            this.xrTable3.Location = new System.Drawing.Point(17, 25);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow12});
            this.xrTable3.Size = new System.Drawing.Size(292, 25);
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 1;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.Text = "Consumer Identity Information";
            this.xrTableCell7.Weight = 3;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.GroupHeader1.Height = 65;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // xrTable2
            // 
            this.xrTable2.Location = new System.Drawing.Point(17, 17);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow11});
            this.xrTable2.Size = new System.Drawing.Size(500, 33);
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 1;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "DETAILED CONSUMER IDENTITY INFORMATION";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell6.Weight = 3;
            // 
            // DetailReport1
            // 
            this.DetailReport1.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail2,
            this.GroupHeader2});
            this.DetailReport1.DataMember = "CounsumerAccountStatus";
            this.DetailReport1.Level = 2;
            this.DetailReport1.Name = "DetailReport1";
            // 
            // Detail2
            // 
            this.Detail2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable7});
            this.Detail2.Height = 25;
            this.Detail2.Name = "Detail2";
            // 
            // xrTable7
            // 
            this.xrTable7.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable7.Location = new System.Drawing.Point(17, 0);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow15});
            this.xrTable7.Size = new System.Drawing.Size(650, 25);
            this.xrTable7.StylePriority.UseBorderColor = false;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell45,
            this.xrTableCell15,
            this.xrTableCell46,
            this.xrTableCell16,
            this.xrTableCell47,
            this.xrTableCell48,
            this.xrTableCell44});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.StylePriority.UseBorders = false;
            this.xrTableRow15.Weight = 1;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell45.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CounsumerAccountStatus.AccountOpenedDate", "")});
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.StylePriority.UseBorders = false;
            this.xrTableCell45.Text = "xrTableCell45";
            this.xrTableCell45.Weight = 0.38461538461538458;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell15.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CounsumerAccountStatus.SubscriberName", "")});
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StylePriority.UseBorders = false;
            this.xrTableCell15.Text = "xrTableCell15";
            this.xrTableCell15.Weight = 0.73076923076923084;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell46.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CounsumerAccountStatus.CreditLimitAmt", "")});
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.StylePriority.UseBorders = false;
            this.xrTableCell46.Weight = 0.42615384615384611;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell16.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CounsumerAccountStatus.CurrentBalanceAmt", "")});
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.StylePriority.UseBorders = false;
            this.xrTableCell16.Weight = 0.38461538461538464;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell47.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CounsumerAccountStatus.MonthlyInstalmentAmt", "")});
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.StylePriority.UseBorders = false;
            this.xrTableCell47.Weight = 0.34769230769230763;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell48.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CounsumerAccountStatus.ArrearsAmt", "")});
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.StylePriority.UseBorders = false;
            this.xrTableCell48.Weight = 0.34692307692307689;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell44.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CounsumerAccountStatus.AccountType", "")});
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.StylePriority.UseBorders = false;
            this.xrTableCell44.Weight = 0.37923076923076926;
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6,
            this.xrTable5,
            this.xrTable4});
            this.GroupHeader2.Height = 192;
            this.GroupHeader2.Name = "GroupHeader2";
            // 
            // xrTable6
            // 
            this.xrTable6.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable6.Location = new System.Drawing.Point(17, 142);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow13,
            this.xrTableRow14});
            this.xrTable6.Size = new System.Drawing.Size(650, 50);
            this.xrTable6.StylePriority.UseBorderColor = false;
            this.xrTable6.StylePriority.UseBorders = false;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell14});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CounsumerAccountStatus.DisplayText", "")});
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.StylePriority.UseFont = false;
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.Text = "xrTableCell14";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell14.Weight = 3;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell24,
            this.xrTableCell21,
            this.xrTableCell41,
            this.xrTableCell22,
            this.xrTableCell42,
            this.xrTableCell43,
            this.xrTableCell23});
            this.xrTableRow14.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.StylePriority.UseFont = false;
            this.xrTableRow14.Weight = 1;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Text = "Date Account Opened";
            this.xrTableCell24.Weight = 0.38461538461538458;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Text = "Company";
            this.xrTableCell21.Weight = 0.73076923076923084;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Text = "Account Credit Limit";
            this.xrTableCell41.Weight = 0.42615384615384616;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Text = "Current Balance";
            this.xrTableCell22.Weight = 0.38461538461538458;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Text = "Installment Amount";
            this.xrTableCell42.Weight = 0.34769230769230763;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Text = "Arrears Amount";
            this.xrTableCell43.Weight = 0.34692307692307689;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Text = "Type of Account";
            this.xrTableCell23.Weight = 0.37923076923076926;
            // 
            // xrTable5
            // 
            this.xrTable5.ForeColor = System.Drawing.Color.Black;
            this.xrTable5.Location = new System.Drawing.Point(17, 92);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable5.Size = new System.Drawing.Size(433, 25);
            this.xrTable5.StylePriority.UseForeColor = false;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell13.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.StylePriority.UseFont = false;
            this.xrTableCell13.StylePriority.UseForeColor = false;
            this.xrTableCell13.Text = "CREDIT ACCOUNTS - Monthly Payment Details";
            this.xrTableCell13.Weight = 3;
            // 
            // xrTable4
            // 
            this.xrTable4.Location = new System.Drawing.Point(17, 17);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable4.Size = new System.Drawing.Size(500, 33);
            this.xrTable4.StylePriority.UseTextAlignment = false;
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            this.xrTableCell8.Text = "DETAILED REPORT INFORMATION";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell8.Weight = 3;
            // 
            // DetailReport2
            // 
            this.DetailReport2.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail3,
            this.GroupHeader3});
            this.DetailReport2.DataMember = "Consumer24MonthlyPaymentHeader";
            this.DetailReport2.Level = 3;
            this.DetailReport2.Name = "DetailReport2";
            // 
            // Detail3
            // 
            this.Detail3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable9});
            this.Detail3.Height = 26;
            this.Detail3.Name = "Detail3";
            // 
            // xrTable9
            // 
            this.xrTable9.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable9.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable9.Location = new System.Drawing.Point(8, 0);
            this.xrTable9.Name = "xrTable9";
            this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow16});
            this.xrTable9.Size = new System.Drawing.Size(792, 25);
            this.xrTable9.StylePriority.UseBorderColor = false;
            this.xrTable9.StylePriority.UseBorders = false;
            this.xrTable9.StylePriority.UseTextAlignment = false;
            this.xrTable9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell49,
            this.xrTableCell98,
            this.xrTableCell75,
            this.xrTableCell76,
            this.xrTableCell77,
            this.xrTableCell78,
            this.xrTableCell79,
            this.xrTableCell80,
            this.xrTableCell81,
            this.xrTableCell82,
            this.xrTableCell83,
            this.xrTableCell84,
            this.xrTableCell85,
            this.xrTableCell86,
            this.xrTableCell87,
            this.xrTableCell88,
            this.xrTableCell89,
            this.xrTableCell90,
            this.xrTableCell91,
            this.xrTableCell92,
            this.xrTableCell93,
            this.xrTableCell94,
            this.xrTableCell95,
            this.xrTableCell96,
            this.xrTableCell97});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 1;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.SubscriberName", "")});
            this.xrTableCell49.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.StylePriority.UseFont = false;
            this.xrTableCell49.StylePriority.UseTextAlignment = false;
            this.xrTableCell49.Text = "xrTableCell49";
            this.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell49.Weight = 0.45036567069356459;
            // 
            // xrTableCell98
            // 
            this.xrTableCell98.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M01", "")});
            this.xrTableCell98.Name = "xrTableCell98";
            this.xrTableCell98.Weight = 0.18908346833704748;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M02", "")});
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.Weight = 0.18935347576530615;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M03", "")});
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.Weight = 0.18546715561224492;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M04", "")});
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.Weight = 0.1869021045918369;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M05", "")});
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Text = "xrTableCell78";
            this.xrTableCell78.Weight = 0.18546715561224489;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M06", "")});
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.Weight = 0.18391262755102042;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M07", "")});
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.Weight = 0.18391262755102053;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M08", "")});
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.Weight = 0.18558673469387751;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M09", "")});
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.Weight = 0.18415178571428584;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M10", "")});
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.Weight = 0.18534757653061221;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M11", "")});
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.Weight = 0.18534757653061221;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M12", "")});
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.Weight = 0.18534757653061229;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M13", "")});
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.Weight = 0.18534757653061232;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M14", "")});
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.Weight = 0.18534757653061224;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M15", "")});
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.Weight = 0.18582589285714291;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M16", "")});
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.Weight = 0.18343431122448989;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M17", "")});
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.Weight = 0.18917410714285726;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M18", "")});
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.Weight = 0.1889349489795919;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M19", "")});
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.Weight = 0.18606505102040827;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M20", "")});
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.Weight = 0.18893494897959184;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M21", "")});
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.Weight = 0.18749999999999992;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M22", "")});
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.Weight = 0.18749999999999994;
            // 
            // xrTableCell96
            // 
            this.xrTableCell96.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M23", "")});
            this.xrTableCell96.Name = "xrTableCell96";
            this.xrTableCell96.Weight = 0.1879783163265307;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell97.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M24", "")});
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.StylePriority.UseBorders = false;
            this.xrTableCell97.Weight = 0.18845663265306129;
            // 
            // GroupHeader3
            // 
            this.GroupHeader3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable8,
            this.xrPageInfo3});
            this.GroupHeader3.Height = 58;
            this.GroupHeader3.Name = "GroupHeader3";
            // 
            // xrTable8
            // 
            this.xrTable8.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable8.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable8.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTable8.Location = new System.Drawing.Point(8, 33);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow17});
            this.xrTable8.Size = new System.Drawing.Size(792, 25);
            this.xrTable8.StylePriority.UseBorderColor = false;
            this.xrTable8.StylePriority.UseBorders = false;
            this.xrTable8.StylePriority.UseFont = false;
            this.xrTable8.StylePriority.UseTextAlignment = false;
            this.xrTable8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell51,
            this.xrTableCell72,
            this.xrTableCell67,
            this.xrTableCell71,
            this.xrTableCell65,
            this.xrTableCell70,
            this.xrTableCell64,
            this.xrTableCell52,
            this.xrTableCell68,
            this.xrTableCell63,
            this.xrTableCell69,
            this.xrTableCell54,
            this.xrTableCell73,
            this.xrTableCell62,
            this.xrTableCell74,
            this.xrTableCell50,
            this.xrTableCell66,
            this.xrTableCell57,
            this.xrTableCell61,
            this.xrTableCell56,
            this.xrTableCell59,
            this.xrTableCell58,
            this.xrTableCell60,
            this.xrTableCell55,
            this.xrTableCell53});
            this.xrTableRow17.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.StylePriority.UseBorders = false;
            this.xrTableRow17.StylePriority.UseFont = false;
            this.xrTableRow17.Weight = 1;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.StylePriority.UseFont = false;
            this.xrTableCell51.Text = "Company";
            this.xrTableCell51.Weight = 0.44387755102040793;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M01", "")});
            this.xrTableCell72.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.StylePriority.UseFont = false;
            this.xrTableCell72.Text = "xrTableCell72";
            this.xrTableCell72.Weight = 0.18601171846934769;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M02", "")});
            this.xrTableCell67.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.StylePriority.UseFont = false;
            this.xrTableCell67.Weight = 0.18601171846934772;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M03", "")});
            this.xrTableCell71.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.StylePriority.UseFont = false;
            this.xrTableCell71.Weight = 0.18601171846934769;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M04", "")});
            this.xrTableCell65.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.StylePriority.UseFont = false;
            this.xrTableCell65.Weight = 0.18594890203530276;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M05", "")});
            this.xrTableCell70.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.StylePriority.UseFont = false;
            this.xrTableCell70.Weight = 0.18692207722491891;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M06", "")});
            this.xrTableCell64.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.StylePriority.UseFont = false;
            this.xrTableCell64.Text = "xrTableCell64";
            this.xrTableCell64.Weight = 0.18480901023986329;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M07", "")});
            this.xrTableCell52.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.StylePriority.UseFont = false;
            this.xrTableCell52.Text = "xrTableCell52";
            this.xrTableCell52.Weight = 0.18282615846050218;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M08", "")});
            this.xrTableCell68.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.StylePriority.UseFont = false;
            this.xrTableCell68.Weight = 0.18692207722491891;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M09", "")});
            this.xrTableCell63.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.StylePriority.UseFont = false;
            this.xrTableCell63.Weight = 0.18692207722491885;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M10", "")});
            this.xrTableCell69.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.StylePriority.UseFont = false;
            this.xrTableCell69.Weight = 0.18692207722491883;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M11", "")});
            this.xrTableCell54.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.StylePriority.UseFont = false;
            this.xrTableCell54.Weight = 0.18692207722491888;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M12", "")});
            this.xrTableCell73.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.StylePriority.UseFont = false;
            this.xrTableCell73.Weight = 0.18692207722491883;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M13", "")});
            this.xrTableCell62.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.StylePriority.UseFont = false;
            this.xrTableCell62.Weight = 0.18692207722491888;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M14", "")});
            this.xrTableCell74.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.StylePriority.UseFont = false;
            this.xrTableCell74.Weight = 0.18692207722491888;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M15", "")});
            this.xrTableCell50.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.StylePriority.UseFont = false;
            this.xrTableCell50.Weight = 0.18692207722491866;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M16", "")});
            this.xrTableCell66.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.StylePriority.UseFont = false;
            this.xrTableCell66.Weight = 0.18861484967550241;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M17", "")});
            this.xrTableCell57.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.StylePriority.UseFont = false;
            this.xrTableCell57.Weight = 0.18586923151767038;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M18", "")});
            this.xrTableCell61.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.StylePriority.UseFont = false;
            this.xrTableCell61.Weight = 0.18888485710376113;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M19", "")});
            this.xrTableCell56.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.StylePriority.UseFont = false;
            this.xrTableCell56.Weight = 0.18793983110485579;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M20", "")});
            this.xrTableCell59.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.StylePriority.UseFont = false;
            this.xrTableCell59.Weight = 0.18807483481898493;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M21", "")});
            this.xrTableCell58.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.StylePriority.UseFont = false;
            this.xrTableCell58.Weight = 0.18888485710376105;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M22", "")});
            this.xrTableCell60.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.StylePriority.UseFont = false;
            this.xrTableCell60.Weight = 0.18681425751657579;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M23", "")});
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.StylePriority.UseFont = false;
            this.xrTableCell55.Weight = 0.18871569070953312;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell53.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M24", "")});
            this.xrTableCell53.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.StylePriority.UseBorders = false;
            this.xrTableCell53.StylePriority.UseFont = false;
            this.xrTableCell53.Weight = 0.16381838531246676;
            // 
            // xrPageInfo3
            // 
            this.xrPageInfo3.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrPageInfo3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrPageInfo3.Format = "Monthly Payment Behaviour as at {0:dd/MM/yyyy}";
            this.xrPageInfo3.Location = new System.Drawing.Point(8, 8);
            this.xrPageInfo3.Name = "xrPageInfo3";
            this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo3.Size = new System.Drawing.Size(792, 25);
            this.xrPageInfo3.StylePriority.UseBorderColor = false;
            this.xrPageInfo3.StylePriority.UseBorders = false;
            this.xrPageInfo3.StylePriority.UseTextAlignment = false;
            this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DetailReport3
            // 
            this.DetailReport3.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail4,
            this.GroupHeader4,
            this.GroupFooter1});
            this.DetailReport3.DataMember = "Definition";
            this.DetailReport3.Level = 4;
            this.DetailReport3.Name = "DetailReport3";
            // 
            // Detail4
            // 
            this.Detail4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable11});
            this.Detail4.Height = 25;
            this.Detail4.Name = "Detail4";
            // 
            // xrTable11
            // 
            this.xrTable11.Location = new System.Drawing.Point(8, 0);
            this.xrTable11.Name = "xrTable11";
            this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow19});
            this.xrTable11.Size = new System.Drawing.Size(792, 25);
            this.xrTable11.StylePriority.UseBorders = false;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell102,
            this.xrTableCell103});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 1;
            // 
            // xrTableCell102
            // 
            this.xrTableCell102.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTableCell102.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell102.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Definition.DefinitionDesc", "")});
            this.xrTableCell102.Name = "xrTableCell102";
            this.xrTableCell102.StylePriority.UseBorderColor = false;
            this.xrTableCell102.StylePriority.UseBorders = false;
            this.xrTableCell102.Text = "xrTableCell102";
            this.xrTableCell102.Weight = 2.2113564668769716;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTableCell103.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell103.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Definition.DefinitionCode", "")});
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.StylePriority.UseBorderColor = false;
            this.xrTableCell103.StylePriority.UseBorders = false;
            this.xrTableCell103.Weight = 5.2933753943217665;
            // 
            // GroupHeader4
            // 
            this.GroupHeader4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable10});
            this.GroupHeader4.Height = 29;
            this.GroupHeader4.Name = "GroupHeader4";
            // 
            // xrTable10
            // 
            this.xrTable10.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.xrTable10.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable10.Location = new System.Drawing.Point(8, 4);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow18});
            this.xrTable10.Size = new System.Drawing.Size(792, 25);
            this.xrTable10.StylePriority.UseBorderColor = false;
            this.xrTable10.StylePriority.UseBorders = false;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell99,
            this.xrTableCell100});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 1;
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell99.CanGrow = false;
            this.xrTableCell99.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Definition.DisplayText", "")});
            this.xrTableCell99.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Underline);
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.StylePriority.UseBorders = false;
            this.xrTableCell99.StylePriority.UseFont = false;
            this.xrTableCell99.Text = "xrTableCell99";
            this.xrTableCell99.Weight = 2.2113564668769716;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell100.CanGrow = false;
            this.xrTableCell100.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Underline);
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.StylePriority.UseBorders = false;
            this.xrTableCell100.StylePriority.UseFont = false;
            this.xrTableCell100.Text = "Indicators";
            this.xrTableCell100.Weight = 5.2933753943217665;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine2});
            this.GroupFooter1.Height = 8;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrLine2
            // 
            this.xrLine2.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.xrLine2.Location = new System.Drawing.Point(8, 0);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.Size = new System.Drawing.Size(792, 2);
            this.xrLine2.StylePriority.UseForeColor = false;
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.Name = "xrControlStyle1";
            this.xrControlStyle1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2,
            this.xrPageInfo1,
            this.xrTable1});
            this.ReportHeader.Height = 40;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrPageInfo2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPageInfo2.Format = "Page {0} of {1}";
            this.xrPageInfo2.Location = new System.Drawing.Point(558, 8);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo2.Size = new System.Drawing.Size(67, 26);
            this.xrPageInfo2.StylePriority.UseBorderColor = false;
            this.xrPageInfo2.StylePriority.UseBorders = false;
            this.xrPageInfo2.StylePriority.UseTextAlignment = false;
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrPageInfo1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPageInfo1.Format = "{0:\"Report Printed Date\" dd/MM/yyyy HH:mm:ss}";
            this.xrPageInfo1.Location = new System.Drawing.Point(316, 8);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo1.Size = new System.Drawing.Size(242, 26);
            this.xrPageInfo1.StylePriority.UseBorderColor = false;
            this.xrPageInfo1.StylePriority.UseBorders = false;
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable1
            // 
            this.xrTable1.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Location = new System.Drawing.Point(8, 8);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable1.Size = new System.Drawing.Size(308, 26);
            this.xrTable1.StylePriority.UseBorderColor = false;
            this.xrTable1.StylePriority.UseBorders = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.Text = "XDS Consumer Trace Report";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell5.Weight = 1.9059967585089144;
            // 
            // DetailReport4
            // 
            this.DetailReport4.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail5,
            this.GroupHeader5});
            this.DetailReport4.DataMember = "ConsumerAdverseInfo";
            this.DetailReport4.Level = 5;
            this.DetailReport4.Name = "DetailReport4";
            // 
            // Detail5
            // 
            this.Detail5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable14});
            this.Detail5.Height = 25;
            this.Detail5.Name = "Detail5";
            // 
            // xrTable14
            // 
            this.xrTable14.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable14.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable14.Location = new System.Drawing.Point(8, 0);
            this.xrTable14.Name = "xrTable14";
            this.xrTable14.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow22});
            this.xrTable14.Size = new System.Drawing.Size(800, 25);
            this.xrTable14.StylePriority.UseBorderColor = false;
            this.xrTable14.StylePriority.UseBorders = false;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell112,
            this.xrTableCell115,
            this.xrTableCell107,
            this.xrTableCell113,
            this.xrTableCell108,
            this.xrTableCell109});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 1;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdverseInfo.DataStatus", "")});
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.Weight = 0.47;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdverseInfo.ActionDate", "")});
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.Weight = 0.31;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdverseInfo.SubscriberName", "")});
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.Weight = 0.72249999999999992;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdverseInfo.AccountNo", "")});
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.Weight = 0.46625;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdverseInfo.CurrentBalanceAmt", "")});
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.Weight = 0.29000000000000004;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdverseInfo.Comments", "")});
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.Weight = 0.74125;
            // 
            // GroupHeader5
            // 
            this.GroupHeader5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable13,
            this.xrTable12});
            this.GroupHeader5.Height = 76;
            this.GroupHeader5.Name = "GroupHeader5";
            // 
            // xrTable13
            // 
            this.xrTable13.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.xrTable13.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable13.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable13.Location = new System.Drawing.Point(8, 51);
            this.xrTable13.Name = "xrTable13";
            this.xrTable13.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow21});
            this.xrTable13.Size = new System.Drawing.Size(800, 25);
            this.xrTable13.StylePriority.UseBorderColor = false;
            this.xrTable13.StylePriority.UseBorders = false;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell110,
            this.xrTableCell104,
            this.xrTableCell114,
            this.xrTableCell111,
            this.xrTableCell105,
            this.xrTableCell106});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 1;
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.CanGrow = false;
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.Text = "Status";
            this.xrTableCell110.Weight = 0.47;
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.CanGrow = false;
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.Text = "Action Date";
            this.xrTableCell104.Weight = 0.31250000000000006;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.CanGrow = false;
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.Text = " Subscriber";
            this.xrTableCell114.Weight = 0.72249999999999992;
            // 
            // xrTableCell111
            // 
            this.xrTableCell111.CanGrow = false;
            this.xrTableCell111.Name = "xrTableCell111";
            this.xrTableCell111.Text = "Account Number";
            this.xrTableCell111.Weight = 0.46374999999999994;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.CanGrow = false;
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.Text = "Amount";
            this.xrTableCell105.Weight = 0.29000000000000004;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.CanGrow = false;
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.Text = "Comment";
            this.xrTableCell106.Weight = 0.74125;
            // 
            // xrTable12
            // 
            this.xrTable12.ForeColor = System.Drawing.Color.Black;
            this.xrTable12.Location = new System.Drawing.Point(8, 17);
            this.xrTable12.Name = "xrTable12";
            this.xrTable12.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow20});
            this.xrTable12.Size = new System.Drawing.Size(433, 25);
            this.xrTable12.StylePriority.UseForeColor = false;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell101});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 1;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell101.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.StylePriority.UseFont = false;
            this.xrTableCell101.StylePriority.UseForeColor = false;
            this.xrTableCell101.Text = "PUBLIC DOMAIN - Adverse Information";
            this.xrTableCell101.Weight = 3;
            // 
            // DetailReport5
            // 
            this.DetailReport5.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail6,
            this.GroupHeader6});
            this.DetailReport5.DataMember = "ConsumerJudgement";
            this.DetailReport5.Level = 6;
            this.DetailReport5.Name = "DetailReport5";
            // 
            // Detail6
            // 
            this.Detail6.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Detail6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable18});
            this.Detail6.Height = 25;
            this.Detail6.Name = "Detail6";
            this.Detail6.StylePriority.UseBorders = false;
            // 
            // xrTable18
            // 
            this.xrTable18.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable18.Location = new System.Drawing.Point(8, 0);
            this.xrTable18.Name = "xrTable18";
            this.xrTable18.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow26});
            this.xrTable18.Size = new System.Drawing.Size(800, 25);
            this.xrTable18.StylePriority.UseBorderColor = false;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell135,
            this.xrTableCell136,
            this.xrTableCell137,
            this.xrTableCell138,
            this.xrTableCell139,
            this.xrTableCell140,
            this.xrTableCell141,
            this.xrTableCell142,
            this.xrTableCell143});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 1;
            // 
            // xrTableCell135
            // 
            this.xrTableCell135.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.CaseNumber", "")});
            this.xrTableCell135.Name = "xrTableCell135";
            this.xrTableCell135.Text = "xrTableCell126";
            this.xrTableCell135.Weight = 0.28;
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.CaseFilingDate", "")});
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.Text = "xrTableCell127";
            this.xrTableCell136.Weight = 0.18625;
            // 
            // xrTableCell137
            // 
            this.xrTableCell137.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.CaseType", "")});
            this.xrTableCell137.Name = "xrTableCell137";
            this.xrTableCell137.Text = "xrTableCell128";
            this.xrTableCell137.Weight = 0.37375;
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.DisputeAmt", "")});
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.Text = "xrTableCell129";
            this.xrTableCell138.Weight = 0.18625;
            // 
            // xrTableCell139
            // 
            this.xrTableCell139.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.PlaintiffName", "")});
            this.xrTableCell139.Name = "xrTableCell139";
            this.xrTableCell139.Text = "xrTableCell130";
            this.xrTableCell139.Weight = 0.34375;
            // 
            // xrTableCell140
            // 
            this.xrTableCell140.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.CourtName", "")});
            this.xrTableCell140.Name = "xrTableCell140";
            this.xrTableCell140.Text = "xrTableCell131";
            this.xrTableCell140.Weight = 0.34375;
            // 
            // xrTableCell141
            // 
            this.xrTableCell141.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.AttorneyName", "")});
            this.xrTableCell141.Name = "xrTableCell141";
            this.xrTableCell141.Text = "xrTableCell132";
            this.xrTableCell141.Weight = 0.40625000000000006;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.TelephoneNo", "")});
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.Text = "xrTableCell133";
            this.xrTableCell142.Weight = 0.3425;
            // 
            // xrTableCell143
            // 
            this.xrTableCell143.Name = "xrTableCell143";
            this.xrTableCell143.Weight = 0.5375;
            // 
            // GroupHeader6
            // 
            this.GroupHeader6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable16,
            this.xrTable15});
            this.GroupHeader6.Height = 67;
            this.GroupHeader6.Name = "GroupHeader6";
            // 
            // xrTable16
            // 
            this.xrTable16.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable16.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable16.Location = new System.Drawing.Point(8, 42);
            this.xrTable16.Name = "xrTable16";
            this.xrTable16.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow24});
            this.xrTable16.Size = new System.Drawing.Size(800, 25);
            this.xrTable16.StylePriority.UseBorderColor = false;
            this.xrTable16.StylePriority.UseBorders = false;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell123,
            this.xrTableCell120,
            this.xrTableCell124,
            this.xrTableCell117,
            this.xrTableCell125,
            this.xrTableCell121,
            this.xrTableCell118,
            this.xrTableCell122,
            this.xrTableCell119});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 1;
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.Text = "Case No.";
            this.xrTableCell123.Weight = 0.28;
            // 
            // xrTableCell120
            // 
            this.xrTableCell120.Name = "xrTableCell120";
            this.xrTableCell120.Text = "Issue date";
            this.xrTableCell120.Weight = 0.18625;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.Text = "Judgment Type";
            this.xrTableCell124.Weight = 0.37375;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.Text = "Amount";
            this.xrTableCell117.Weight = 0.18625;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.Text = "Plaintiff";
            this.xrTableCell125.Weight = 0.34374999999999994;
            // 
            // xrTableCell121
            // 
            this.xrTableCell121.Name = "xrTableCell121";
            this.xrTableCell121.Text = "Court";
            this.xrTableCell121.Weight = 0.34375;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.Text = "Attorney";
            this.xrTableCell118.Weight = 0.40625000000000006;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.Text = "Phone No";
            this.xrTableCell122.Weight = 0.34250000000000008;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.Text = "Comment";
            this.xrTableCell119.Weight = 0.5375;
            // 
            // xrTable15
            // 
            this.xrTable15.ForeColor = System.Drawing.Color.Black;
            this.xrTable15.Location = new System.Drawing.Point(8, 8);
            this.xrTable15.Name = "xrTable15";
            this.xrTable15.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow23});
            this.xrTable15.Size = new System.Drawing.Size(433, 25);
            this.xrTable15.StylePriority.UseForeColor = false;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell116});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 1;
            // 
            // xrTableCell116
            // 
            this.xrTableCell116.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell116.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell116.Name = "xrTableCell116";
            this.xrTableCell116.StylePriority.UseFont = false;
            this.xrTableCell116.StylePriority.UseForeColor = false;
            this.xrTableCell116.Text = "PUBLIC DOMAIN - Judgements/Court Files";
            this.xrTableCell116.Weight = 3;
            // 
            // DetailReport6
            // 
            this.DetailReport6.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail7,
            this.GroupHeader7});
            this.DetailReport6.DataMember = "ConsumerAdminOrder";
            this.DetailReport6.Level = 7;
            this.DetailReport6.Name = "DetailReport6";
            // 
            // Detail7
            // 
            this.Detail7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable20});
            this.Detail7.Height = 25;
            this.Detail7.Name = "Detail7";
            // 
            // xrTable20
            // 
            this.xrTable20.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable20.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable20.Location = new System.Drawing.Point(8, 0);
            this.xrTable20.Name = "xrTable20";
            this.xrTable20.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow28});
            this.xrTable20.Size = new System.Drawing.Size(800, 25);
            this.xrTable20.StylePriority.UseBorderColor = false;
            this.xrTable20.StylePriority.UseBorders = false;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell151,
            this.xrTableCell148,
            this.xrTableCell130,
            this.xrTableCell152,
            this.xrTableCell149,
            this.xrTableCell131,
            this.xrTableCell153,
            this.xrTableCell150,
            this.xrTableCell132});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 1;
            // 
            // xrTableCell151
            // 
            this.xrTableCell151.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.CaseNumber", "")});
            this.xrTableCell151.Name = "xrTableCell151";
            this.xrTableCell151.Weight = 0.31375;
            // 
            // xrTableCell148
            // 
            this.xrTableCell148.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.CaseFilingDate", "")});
            this.xrTableCell148.Name = "xrTableCell148";
            this.xrTableCell148.Weight = 0.25;
            // 
            // xrTableCell130
            // 
            this.xrTableCell130.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.LastUpdatedDate", "")});
            this.xrTableCell130.Name = "xrTableCell130";
            this.xrTableCell130.Weight = 0.27875000000000005;
            // 
            // xrTableCell152
            // 
            this.xrTableCell152.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.CaseType", "")});
            this.xrTableCell152.Name = "xrTableCell152";
            this.xrTableCell152.Text = "xrTableCell152";
            this.xrTableCell152.Weight = 0.34375;
            // 
            // xrTableCell149
            // 
            this.xrTableCell149.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.DisputeAmt", "")});
            this.xrTableCell149.Name = "xrTableCell149";
            this.xrTableCell149.Weight = 0.21625;
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.PlaintiffName", "")});
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.Weight = 0.5;
            // 
            // xrTableCell153
            // 
            this.xrTableCell153.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.CourtName", "")});
            this.xrTableCell153.Name = "xrTableCell153";
            this.xrTableCell153.Weight = 0.3475;
            // 
            // xrTableCell150
            // 
            this.xrTableCell150.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.AttorneyName", "")});
            this.xrTableCell150.Name = "xrTableCell150";
            this.xrTableCell150.Weight = 0.4;
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.TelephoneNo", "")});
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.Weight = 0.35;
            // 
            // GroupHeader7
            // 
            this.GroupHeader7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable19,
            this.xrTable17});
            this.GroupHeader7.Height = 68;
            this.GroupHeader7.Name = "GroupHeader7";
            // 
            // xrTable19
            // 
            this.xrTable19.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.xrTable19.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable19.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable19.Location = new System.Drawing.Point(8, 42);
            this.xrTable19.Name = "xrTable19";
            this.xrTable19.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow27});
            this.xrTable19.Size = new System.Drawing.Size(800, 25);
            this.xrTable19.StylePriority.UseBorderColor = false;
            this.xrTable19.StylePriority.UseBorders = false;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell134,
            this.xrTableCell133,
            this.xrTableCell127,
            this.xrTableCell146,
            this.xrTableCell144,
            this.xrTableCell128,
            this.xrTableCell147,
            this.xrTableCell145,
            this.xrTableCell129});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 1;
            // 
            // xrTableCell134
            // 
            this.xrTableCell134.CanGrow = false;
            this.xrTableCell134.Name = "xrTableCell134";
            this.xrTableCell134.Text = "Case No.";
            this.xrTableCell134.Weight = 0.31375;
            // 
            // xrTableCell133
            // 
            this.xrTableCell133.CanGrow = false;
            this.xrTableCell133.Name = "xrTableCell133";
            this.xrTableCell133.Text = "Issue Date";
            this.xrTableCell133.Weight = 0.25;
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.CanGrow = false;
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.Text = "Date loaded";
            this.xrTableCell127.Weight = 0.27875000000000005;
            // 
            // xrTableCell146
            // 
            this.xrTableCell146.CanGrow = false;
            this.xrTableCell146.Name = "xrTableCell146";
            this.xrTableCell146.Text = "Judgment Type";
            this.xrTableCell146.Weight = 0.34375000000000006;
            // 
            // xrTableCell144
            // 
            this.xrTableCell144.CanGrow = false;
            this.xrTableCell144.Name = "xrTableCell144";
            this.xrTableCell144.Text = "Amount";
            this.xrTableCell144.Weight = 0.21625;
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.CanGrow = false;
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.Text = "Plaintiff";
            this.xrTableCell128.Weight = 0.5;
            // 
            // xrTableCell147
            // 
            this.xrTableCell147.CanGrow = false;
            this.xrTableCell147.Name = "xrTableCell147";
            this.xrTableCell147.Text = "Court";
            this.xrTableCell147.Weight = 0.3475;
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.CanGrow = false;
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.Text = "Attorney";
            this.xrTableCell145.Weight = 0.4;
            // 
            // xrTableCell129
            // 
            this.xrTableCell129.CanGrow = false;
            this.xrTableCell129.Name = "xrTableCell129";
            this.xrTableCell129.Text = "Phone No";
            this.xrTableCell129.Weight = 0.35;
            // 
            // xrTable17
            // 
            this.xrTable17.ForeColor = System.Drawing.Color.Black;
            this.xrTable17.Location = new System.Drawing.Point(8, 8);
            this.xrTable17.Name = "xrTable17";
            this.xrTable17.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow25});
            this.xrTable17.Size = new System.Drawing.Size(433, 25);
            this.xrTable17.StylePriority.UseForeColor = false;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell126});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 1;
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell126.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.StylePriority.UseFont = false;
            this.xrTableCell126.StylePriority.UseForeColor = false;
            this.xrTableCell126.Text = "PUBLIC DOMAIN - AdministrationOrders";
            this.xrTableCell126.Weight = 3;
            // 
            // DetailReport8
            // 
            this.DetailReport8.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail9,
            this.GroupHeader8});
            this.DetailReport8.DataMember = "ConsumerSequestration";
            this.DetailReport8.Level = 8;
            this.DetailReport8.Name = "DetailReport8";
            // 
            // Detail9
            // 
            this.Detail9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable23});
            this.Detail9.Height = 31;
            this.Detail9.Name = "Detail9";
            // 
            // xrTable23
            // 
            this.xrTable23.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable23.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable23.Location = new System.Drawing.Point(8, 0);
            this.xrTable23.Name = "xrTable23";
            this.xrTable23.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow31});
            this.xrTable23.Size = new System.Drawing.Size(800, 25);
            this.xrTable23.StylePriority.UseBorderColor = false;
            this.xrTable23.StylePriority.UseBorders = false;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell164,
            this.xrTableCell165,
            this.xrTableCell166,
            this.xrTableCell167,
            this.xrTableCell168,
            this.xrTableCell169,
            this.xrTableCell170,
            this.xrTableCell171,
            this.xrTableCell172});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 1;
            // 
            // xrTableCell164
            // 
            this.xrTableCell164.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.CaseNumber", "")});
            this.xrTableCell164.Name = "xrTableCell164";
            this.xrTableCell164.Text = "xrTableCell164";
            this.xrTableCell164.Weight = 0.31375;
            // 
            // xrTableCell165
            // 
            this.xrTableCell165.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.CaseFilingDate", "")});
            this.xrTableCell165.Name = "xrTableCell165";
            this.xrTableCell165.Text = "xrTableCell165";
            this.xrTableCell165.Weight = 0.25;
            // 
            // xrTableCell166
            // 
            this.xrTableCell166.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.LastUpdatedDate", "")});
            this.xrTableCell166.Name = "xrTableCell166";
            this.xrTableCell166.Text = "xrTableCell166";
            this.xrTableCell166.Weight = 0.27875000000000005;
            // 
            // xrTableCell167
            // 
            this.xrTableCell167.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.CaseType", "")});
            this.xrTableCell167.Name = "xrTableCell167";
            this.xrTableCell167.Text = "xrTableCell167";
            this.xrTableCell167.Weight = 0.34375;
            // 
            // xrTableCell168
            // 
            this.xrTableCell168.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.DisputeAmt", "")});
            this.xrTableCell168.Name = "xrTableCell168";
            this.xrTableCell168.Text = "xrTableCell168";
            this.xrTableCell168.Weight = 0.21625;
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.PlaintiffName", "")});
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.Text = "xrTableCell169";
            this.xrTableCell169.Weight = 0.5;
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.CourtName", "")});
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.Text = "xrTableCell170";
            this.xrTableCell170.Weight = 0.3475;
            // 
            // xrTableCell171
            // 
            this.xrTableCell171.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.AttorneyName", "")});
            this.xrTableCell171.Name = "xrTableCell171";
            this.xrTableCell171.Text = "xrTableCell171";
            this.xrTableCell171.Weight = 0.4;
            // 
            // xrTableCell172
            // 
            this.xrTableCell172.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.TelephoneNo", "")});
            this.xrTableCell172.Name = "xrTableCell172";
            this.xrTableCell172.Text = "xrTableCell172";
            this.xrTableCell172.Weight = 0.35;
            // 
            // GroupHeader8
            // 
            this.GroupHeader8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable22,
            this.xrTable21});
            this.GroupHeader8.Height = 73;
            this.GroupHeader8.Name = "GroupHeader8";
            // 
            // xrTable22
            // 
            this.xrTable22.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.xrTable22.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable22.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable22.Location = new System.Drawing.Point(8, 48);
            this.xrTable22.Name = "xrTable22";
            this.xrTable22.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow30});
            this.xrTable22.Size = new System.Drawing.Size(800, 25);
            this.xrTable22.StylePriority.UseBorderColor = false;
            this.xrTable22.StylePriority.UseBorders = false;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell155,
            this.xrTableCell156,
            this.xrTableCell157,
            this.xrTableCell158,
            this.xrTableCell159,
            this.xrTableCell160,
            this.xrTableCell161,
            this.xrTableCell162,
            this.xrTableCell163});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Weight = 1;
            // 
            // xrTableCell155
            // 
            this.xrTableCell155.CanGrow = false;
            this.xrTableCell155.Name = "xrTableCell155";
            this.xrTableCell155.Text = "Case No.";
            this.xrTableCell155.Weight = 0.31375;
            // 
            // xrTableCell156
            // 
            this.xrTableCell156.CanGrow = false;
            this.xrTableCell156.Name = "xrTableCell156";
            this.xrTableCell156.Text = "Issue Date";
            this.xrTableCell156.Weight = 0.25;
            // 
            // xrTableCell157
            // 
            this.xrTableCell157.CanGrow = false;
            this.xrTableCell157.Name = "xrTableCell157";
            this.xrTableCell157.Text = "Date loaded";
            this.xrTableCell157.Weight = 0.27875000000000005;
            // 
            // xrTableCell158
            // 
            this.xrTableCell158.CanGrow = false;
            this.xrTableCell158.Name = "xrTableCell158";
            this.xrTableCell158.Text = "Judgment Type";
            this.xrTableCell158.Weight = 0.34375000000000006;
            // 
            // xrTableCell159
            // 
            this.xrTableCell159.CanGrow = false;
            this.xrTableCell159.Name = "xrTableCell159";
            this.xrTableCell159.Text = "Amount";
            this.xrTableCell159.Weight = 0.21625;
            // 
            // xrTableCell160
            // 
            this.xrTableCell160.CanGrow = false;
            this.xrTableCell160.Name = "xrTableCell160";
            this.xrTableCell160.Text = "Plaintiff";
            this.xrTableCell160.Weight = 0.5;
            // 
            // xrTableCell161
            // 
            this.xrTableCell161.CanGrow = false;
            this.xrTableCell161.Name = "xrTableCell161";
            this.xrTableCell161.Text = "Court";
            this.xrTableCell161.Weight = 0.3475;
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.CanGrow = false;
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.Text = "Attorney";
            this.xrTableCell162.Weight = 0.4;
            // 
            // xrTableCell163
            // 
            this.xrTableCell163.CanGrow = false;
            this.xrTableCell163.Name = "xrTableCell163";
            this.xrTableCell163.Text = "Phone No";
            this.xrTableCell163.Weight = 0.35;
            // 
            // xrTable21
            // 
            this.xrTable21.ForeColor = System.Drawing.Color.Black;
            this.xrTable21.Location = new System.Drawing.Point(8, 17);
            this.xrTable21.Name = "xrTable21";
            this.xrTable21.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow29});
            this.xrTable21.Size = new System.Drawing.Size(433, 25);
            this.xrTable21.StylePriority.UseForeColor = false;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell154});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 1;
            // 
            // xrTableCell154
            // 
            this.xrTableCell154.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell154.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell154.Name = "xrTableCell154";
            this.xrTableCell154.StylePriority.UseFont = false;
            this.xrTableCell154.StylePriority.UseForeColor = false;
            this.xrTableCell154.Text = "PUBLIC DOMAIN - Sequestrations";
            this.xrTableCell154.Weight = 3;
            // 
            // DetailReport9
            // 
            this.DetailReport9.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail10,
            this.GroupHeader9});
            this.DetailReport9.DataMember = "ConsumerDefaultAlert";
            this.DetailReport9.Level = 9;
            this.DetailReport9.Name = "DetailReport9";
            // 
            // Detail10
            // 
            this.Detail10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable26});
            this.Detail10.Height = 26;
            this.Detail10.Name = "Detail10";
            // 
            // xrTable26
            // 
            this.xrTable26.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable26.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable26.Location = new System.Drawing.Point(8, 0);
            this.xrTable26.Name = "xrTable26";
            this.xrTable26.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow34});
            this.xrTable26.Size = new System.Drawing.Size(792, 25);
            this.xrTable26.StylePriority.UseBorderColor = false;
            this.xrTable26.StylePriority.UseBorders = false;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell183,
            this.xrTableCell177,
            this.xrTableCell184,
            this.xrTableCell178,
            this.xrTableCell185,
            this.xrTableCell179});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 1;
            // 
            // xrTableCell183
            // 
            this.xrTableCell183.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDefaultAlert.Company", "")});
            this.xrTableCell183.Name = "xrTableCell183";
            this.xrTableCell183.Text = "xrTableCell183";
            this.xrTableCell183.Weight = 0.63257575757575757;
            // 
            // xrTableCell177
            // 
            this.xrTableCell177.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDefaultAlert.AccountNo", "")});
            this.xrTableCell177.Name = "xrTableCell177";
            this.xrTableCell177.Text = "xrTableCell177";
            this.xrTableCell177.Weight = 0.47348484848484851;
            // 
            // xrTableCell184
            // 
            this.xrTableCell184.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDefaultAlert.DateLoaded", "")});
            this.xrTableCell184.Name = "xrTableCell184";
            this.xrTableCell184.Text = "xrTableCell184";
            this.xrTableCell184.Weight = 0.40909090909090906;
            // 
            // xrTableCell178
            // 
            this.xrTableCell178.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDefaultAlert.Amount", "")});
            this.xrTableCell178.Name = "xrTableCell178";
            this.xrTableCell178.Text = "xrTableCell178";
            this.xrTableCell178.Weight = 0.40909090909090912;
            // 
            // xrTableCell185
            // 
            this.xrTableCell185.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDefaultAlert.Statuscode", "")});
            this.xrTableCell185.Name = "xrTableCell185";
            this.xrTableCell185.Text = "xrTableCell185";
            this.xrTableCell185.Weight = 0.44318181818181823;
            // 
            // xrTableCell179
            // 
            this.xrTableCell179.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDefaultAlert.Comments", "")});
            this.xrTableCell179.Name = "xrTableCell179";
            this.xrTableCell179.Text = "xrTableCell179";
            this.xrTableCell179.Weight = 0.63257575757575757;
            // 
            // GroupHeader9
            // 
            this.GroupHeader9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable25,
            this.xrTable24});
            this.GroupHeader9.Name = "GroupHeader9";
            // 
            // xrTable25
            // 
            this.xrTable25.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.xrTable25.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable25.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable25.Location = new System.Drawing.Point(8, 75);
            this.xrTable25.Name = "xrTable25";
            this.xrTable25.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow33});
            this.xrTable25.Size = new System.Drawing.Size(792, 25);
            this.xrTable25.StylePriority.UseBorderColor = false;
            this.xrTable25.StylePriority.UseBorders = false;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell180,
            this.xrTableCell174,
            this.xrTableCell181,
            this.xrTableCell175,
            this.xrTableCell182,
            this.xrTableCell176});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Weight = 1;
            // 
            // xrTableCell180
            // 
            this.xrTableCell180.CanGrow = false;
            this.xrTableCell180.Name = "xrTableCell180";
            this.xrTableCell180.Text = "Company";
            this.xrTableCell180.Weight = 0.63257575757575746;
            // 
            // xrTableCell174
            // 
            this.xrTableCell174.CanGrow = false;
            this.xrTableCell174.Name = "xrTableCell174";
            this.xrTableCell174.Text = "Account No.";
            this.xrTableCell174.Weight = 0.47348484848484845;
            // 
            // xrTableCell181
            // 
            this.xrTableCell181.CanGrow = false;
            this.xrTableCell181.Name = "xrTableCell181";
            this.xrTableCell181.Text = "Date Loaded";
            this.xrTableCell181.Weight = 0.40909090909090906;
            // 
            // xrTableCell175
            // 
            this.xrTableCell175.CanGrow = false;
            this.xrTableCell175.Name = "xrTableCell175";
            this.xrTableCell175.Text = "Amount";
            this.xrTableCell175.Weight = 0.40909090909090912;
            // 
            // xrTableCell182
            // 
            this.xrTableCell182.CanGrow = false;
            this.xrTableCell182.Name = "xrTableCell182";
            this.xrTableCell182.Text = "Status";
            this.xrTableCell182.Weight = 0.44318181818181818;
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.CanGrow = false;
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.Text = "Comments";
            this.xrTableCell176.Weight = 0.63257575757575757;
            // 
            // xrTable24
            // 
            this.xrTable24.ForeColor = System.Drawing.Color.Black;
            this.xrTable24.Location = new System.Drawing.Point(8, 17);
            this.xrTable24.Name = "xrTable24";
            this.xrTable24.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow32});
            this.xrTable24.Size = new System.Drawing.Size(433, 25);
            this.xrTable24.StylePriority.UseForeColor = false;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell173});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Weight = 1;
            // 
            // xrTableCell173
            // 
            this.xrTableCell173.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell173.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell173.Name = "xrTableCell173";
            this.xrTableCell173.StylePriority.UseFont = false;
            this.xrTableCell173.StylePriority.UseForeColor = false;
            this.xrTableCell173.Text = "PUBLIC DOMAIN - DefaultAlerts";
            this.xrTableCell173.Weight = 3;
            // 
            // DetailReport10
            // 
            this.DetailReport10.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail11,
            this.GroupHeader10});
            this.DetailReport10.DataMember = "ConsumerNameHistory";
            this.DetailReport10.Level = 10;
            this.DetailReport10.Name = "DetailReport10";
            // 
            // Detail11
            // 
            this.Detail11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable30});
            this.Detail11.Height = 25;
            this.Detail11.Name = "Detail11";
            // 
            // xrTable30
            // 
            this.xrTable30.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable30.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable30.Location = new System.Drawing.Point(8, 0);
            this.xrTable30.Name = "xrTable30";
            this.xrTable30.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow38});
            this.xrTable30.Size = new System.Drawing.Size(800, 25);
            this.xrTable30.StylePriority.UseBorderColor = false;
            this.xrTable30.StylePriority.UseBorders = false;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell203,
            this.xrTableCell200,
            this.xrTableCell191,
            this.xrTableCell204,
            this.xrTableCell201,
            this.xrTableCell192,
            this.xrTableCell205,
            this.xrTableCell202,
            this.xrTableCell193});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Weight = 1;
            // 
            // xrTableCell203
            // 
            this.xrTableCell203.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.LastUpdatedDate", "")});
            this.xrTableCell203.Name = "xrTableCell203";
            this.xrTableCell203.Weight = 0.25;
            // 
            // xrTableCell200
            // 
            this.xrTableCell200.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.Surname", "")});
            this.xrTableCell200.Name = "xrTableCell200";
            this.xrTableCell200.Weight = 0.4375;
            // 
            // xrTableCell191
            // 
            this.xrTableCell191.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.FirstName", "")});
            this.xrTableCell191.Name = "xrTableCell191";
            this.xrTableCell191.Weight = 0.47;
            // 
            // xrTableCell204
            // 
            this.xrTableCell204.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.SecondName", "")});
            this.xrTableCell204.Name = "xrTableCell204";
            this.xrTableCell204.Weight = 0.40375;
            // 
            // xrTableCell201
            // 
            this.xrTableCell201.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.Initials", "")});
            this.xrTableCell201.Name = "xrTableCell201";
            this.xrTableCell201.Weight = 0.18250000000000002;
            // 
            // xrTableCell192
            // 
            this.xrTableCell192.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.TitleDesc", "")});
            this.xrTableCell192.Name = "xrTableCell192";
            this.xrTableCell192.Weight = 0.25624999999999992;
            // 
            // xrTableCell205
            // 
            this.xrTableCell205.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.IDNo", "")});
            this.xrTableCell205.Name = "xrTableCell205";
            this.xrTableCell205.Weight = 0.37375;
            // 
            // xrTableCell202
            // 
            this.xrTableCell202.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.PassportNo", "")});
            this.xrTableCell202.Name = "xrTableCell202";
            this.xrTableCell202.Weight = 0.31375;
            // 
            // xrTableCell193
            // 
            this.xrTableCell193.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.BirthDate", "")});
            this.xrTableCell193.Name = "xrTableCell193";
            this.xrTableCell193.Text = "xrTableCell193";
            this.xrTableCell193.Weight = 0.3125;
            // 
            // GroupHeader10
            // 
            this.GroupHeader10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable31,
            this.xrTable28,
            this.xrTable27});
            this.GroupHeader10.Height = 135;
            this.GroupHeader10.Name = "GroupHeader10";
            // 
            // xrTable31
            // 
            this.xrTable31.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable31.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable31.Location = new System.Drawing.Point(8, 108);
            this.xrTable31.Name = "xrTable31";
            this.xrTable31.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow39});
            this.xrTable31.Size = new System.Drawing.Size(800, 25);
            this.xrTable31.StylePriority.UseBorderColor = false;
            this.xrTable31.StylePriority.UseBorders = false;
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell206,
            this.xrTableCell207,
            this.xrTableCell208,
            this.xrTableCell209,
            this.xrTableCell210,
            this.xrTableCell211,
            this.xrTableCell212,
            this.xrTableCell213,
            this.xrTableCell214});
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Weight = 1;
            // 
            // xrTableCell206
            // 
            this.xrTableCell206.Name = "xrTableCell206";
            this.xrTableCell206.Text = "Bureau Update";
            this.xrTableCell206.Weight = 0.25;
            // 
            // xrTableCell207
            // 
            this.xrTableCell207.Name = "xrTableCell207";
            this.xrTableCell207.Text = "Surname";
            this.xrTableCell207.Weight = 0.4375;
            // 
            // xrTableCell208
            // 
            this.xrTableCell208.Name = "xrTableCell208";
            this.xrTableCell208.Text = "First Name";
            this.xrTableCell208.Weight = 0.47;
            // 
            // xrTableCell209
            // 
            this.xrTableCell209.Name = "xrTableCell209";
            this.xrTableCell209.Text = "Second Name";
            this.xrTableCell209.Weight = 0.40375;
            // 
            // xrTableCell210
            // 
            this.xrTableCell210.Name = "xrTableCell210";
            this.xrTableCell210.Text = "Initial";
            this.xrTableCell210.Weight = 0.18250000000000002;
            // 
            // xrTableCell211
            // 
            this.xrTableCell211.Name = "xrTableCell211";
            this.xrTableCell211.Text = "Title";
            this.xrTableCell211.Weight = 0.25624999999999992;
            // 
            // xrTableCell212
            // 
            this.xrTableCell212.Name = "xrTableCell212";
            this.xrTableCell212.Text = "ID No";
            this.xrTableCell212.Weight = 0.37375;
            // 
            // xrTableCell213
            // 
            this.xrTableCell213.Name = "xrTableCell213";
            this.xrTableCell213.Text = "Other ID No";
            this.xrTableCell213.Weight = 0.31375;
            // 
            // xrTableCell214
            // 
            this.xrTableCell214.Name = "xrTableCell214";
            this.xrTableCell214.Text = "Birth Date";
            this.xrTableCell214.Weight = 0.3125;
            // 
            // xrTable28
            // 
            this.xrTable28.ForeColor = System.Drawing.Color.Black;
            this.xrTable28.Location = new System.Drawing.Point(8, 67);
            this.xrTable28.Name = "xrTable28";
            this.xrTable28.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow36});
            this.xrTable28.Size = new System.Drawing.Size(433, 25);
            this.xrTable28.StylePriority.UseForeColor = false;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell187});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 1;
            // 
            // xrTableCell187
            // 
            this.xrTableCell187.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell187.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell187.Name = "xrTableCell187";
            this.xrTableCell187.StylePriority.UseFont = false;
            this.xrTableCell187.StylePriority.UseForeColor = false;
            this.xrTableCell187.Text = "Name History";
            this.xrTableCell187.Weight = 3;
            // 
            // xrTable27
            // 
            this.xrTable27.Location = new System.Drawing.Point(8, 17);
            this.xrTable27.Name = "xrTable27";
            this.xrTable27.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow35});
            this.xrTable27.Size = new System.Drawing.Size(500, 33);
            this.xrTable27.StylePriority.UseTextAlignment = false;
            this.xrTable27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell186});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Weight = 1;
            // 
            // xrTableCell186
            // 
            this.xrTableCell186.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell186.Name = "xrTableCell186";
            this.xrTableCell186.StylePriority.UseFont = false;
            this.xrTableCell186.StylePriority.UseTextAlignment = false;
            this.xrTableCell186.Text = "DETAILED CONSUMER CONTACT INFORMATION";
            this.xrTableCell186.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell186.Weight = 3;
            // 
            // DetailReport11
            // 
            this.DetailReport11.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail12,
            this.GroupHeader11});
            this.DetailReport11.DataMember = "ConsumerAddressHistory";
            this.DetailReport11.Level = 11;
            this.DetailReport11.Name = "DetailReport11";
            // 
            // Detail12
            // 
            this.Detail12.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable33});
            this.Detail12.Height = 25;
            this.Detail12.Name = "Detail12";
            // 
            // xrTable33
            // 
            this.xrTable33.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable33.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable33.Location = new System.Drawing.Point(8, 0);
            this.xrTable33.Name = "xrTable33";
            this.xrTable33.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow41});
            this.xrTable33.Size = new System.Drawing.Size(792, 25);
            this.xrTable33.StylePriority.UseBorderColor = false;
            this.xrTable33.StylePriority.UseBorders = false;
            // 
            // xrTableRow41
            // 
            this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell217,
            this.xrTableCell195,
            this.xrTableCell218,
            this.xrTableCell196,
            this.xrTableCell219,
            this.xrTableCell220,
            this.xrTableCell197});
            this.xrTableRow41.Name = "xrTableRow41";
            this.xrTableRow41.Weight = 1;
            // 
            // xrTableCell217
            // 
            this.xrTableCell217.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.LastUpdatedDate", "")});
            this.xrTableCell217.Name = "xrTableCell217";
            this.xrTableCell217.Weight = 0.28409090909090906;
            // 
            // xrTableCell195
            // 
            this.xrTableCell195.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.AddressType", "")});
            this.xrTableCell195.Name = "xrTableCell195";
            this.xrTableCell195.Weight = 0.38257575757575762;
            // 
            // xrTableCell218
            // 
            this.xrTableCell218.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.Address1", "")});
            this.xrTableCell218.Name = "xrTableCell218";
            this.xrTableCell218.Weight = 0.83333333333333326;
            // 
            // xrTableCell196
            // 
            this.xrTableCell196.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.Address2", "")});
            this.xrTableCell196.Name = "xrTableCell196";
            this.xrTableCell196.Weight = 0.42803030303030304;
            // 
            // xrTableCell219
            // 
            this.xrTableCell219.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.Address3", "")});
            this.xrTableCell219.Name = "xrTableCell219";
            this.xrTableCell219.Weight = 0.44318181818181823;
            // 
            // xrTableCell220
            // 
            this.xrTableCell220.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.Address4", "")});
            this.xrTableCell220.Name = "xrTableCell220";
            this.xrTableCell220.Weight = 0.37878787878787878;
            // 
            // xrTableCell197
            // 
            this.xrTableCell197.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.PostalCode", "")});
            this.xrTableCell197.Name = "xrTableCell197";
            this.xrTableCell197.Weight = 0.25;
            // 
            // GroupHeader11
            // 
            this.GroupHeader11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable32,
            this.xrTable29});
            this.GroupHeader11.Height = 84;
            this.GroupHeader11.Name = "GroupHeader11";
            // 
            // xrTable32
            // 
            this.xrTable32.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.xrTable32.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable32.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable32.Location = new System.Drawing.Point(8, 50);
            this.xrTable32.Name = "xrTable32";
            this.xrTable32.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow40});
            this.xrTable32.Size = new System.Drawing.Size(792, 34);
            this.xrTable32.StylePriority.UseBorderColor = false;
            this.xrTable32.StylePriority.UseBorders = false;
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell198,
            this.xrTableCell189,
            this.xrTableCell199,
            this.xrTableCell190,
            this.xrTableCell215,
            this.xrTableCell216,
            this.xrTableCell194});
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.Weight = 1;
            // 
            // xrTableCell198
            // 
            this.xrTableCell198.CanGrow = false;
            this.xrTableCell198.Name = "xrTableCell198";
            this.xrTableCell198.Text = "Bureau Update";
            this.xrTableCell198.Weight = 0.28409090909090906;
            // 
            // xrTableCell189
            // 
            this.xrTableCell189.CanGrow = false;
            this.xrTableCell189.Name = "xrTableCell189";
            this.xrTableCell189.Text = "Type";
            this.xrTableCell189.Weight = 0.38257575757575757;
            // 
            // xrTableCell199
            // 
            this.xrTableCell199.CanGrow = false;
            this.xrTableCell199.Name = "xrTableCell199";
            this.xrTableCell199.Text = "Line1";
            this.xrTableCell199.Weight = 0.83333333333333326;
            // 
            // xrTableCell190
            // 
            this.xrTableCell190.CanGrow = false;
            this.xrTableCell190.Name = "xrTableCell190";
            this.xrTableCell190.Text = "Line2";
            this.xrTableCell190.Weight = 0.42803030303030304;
            // 
            // xrTableCell215
            // 
            this.xrTableCell215.CanGrow = false;
            this.xrTableCell215.Name = "xrTableCell215";
            this.xrTableCell215.Text = "Line 3";
            this.xrTableCell215.Weight = 0.44318181818181823;
            // 
            // xrTableCell216
            // 
            this.xrTableCell216.CanGrow = false;
            this.xrTableCell216.Name = "xrTableCell216";
            this.xrTableCell216.Text = "Line 4";
            this.xrTableCell216.Weight = 0.37878787878787878;
            // 
            // xrTableCell194
            // 
            this.xrTableCell194.CanGrow = false;
            this.xrTableCell194.Name = "xrTableCell194";
            this.xrTableCell194.Text = "Postal Code";
            this.xrTableCell194.Weight = 0.25;
            // 
            // xrTable29
            // 
            this.xrTable29.ForeColor = System.Drawing.Color.Black;
            this.xrTable29.Location = new System.Drawing.Point(8, 8);
            this.xrTable29.Name = "xrTable29";
            this.xrTable29.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow37});
            this.xrTable29.Size = new System.Drawing.Size(433, 25);
            this.xrTable29.StylePriority.UseForeColor = false;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell188});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Weight = 1;
            // 
            // xrTableCell188
            // 
            this.xrTableCell188.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell188.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell188.Name = "xrTableCell188";
            this.xrTableCell188.StylePriority.UseFont = false;
            this.xrTableCell188.StylePriority.UseForeColor = false;
            this.xrTableCell188.Text = "Address History";
            this.xrTableCell188.Weight = 3;
            // 
            // DetailReport12
            // 
            this.DetailReport12.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail13,
            this.GroupHeader12});
            this.DetailReport12.DataMember = "ConsumerTelephoneHistory";
            this.DetailReport12.Level = 12;
            this.DetailReport12.Name = "DetailReport12";
            // 
            // Detail13
            // 
            this.Detail13.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable36});
            this.Detail13.Height = 25;
            this.Detail13.Name = "Detail13";
            // 
            // xrTable36
            // 
            this.xrTable36.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable36.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable36.Location = new System.Drawing.Point(8, 0);
            this.xrTable36.Name = "xrTable36";
            this.xrTable36.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow44});
            this.xrTable36.Size = new System.Drawing.Size(792, 25);
            this.xrTable36.StylePriority.UseBorderColor = false;
            this.xrTable36.StylePriority.UseBorders = false;
            // 
            // xrTableRow44
            // 
            this.xrTableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell225,
            this.xrTableCell227,
            this.xrTableCell224,
            this.xrTableCell226});
            this.xrTableRow44.Name = "xrTableRow44";
            this.xrTableRow44.Weight = 1;
            // 
            // xrTableCell225
            // 
            this.xrTableCell225.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneHistory.LastUpdatedDate", "")});
            this.xrTableCell225.Name = "xrTableCell225";
            this.xrTableCell225.Weight = 0.34848484848484851;
            // 
            // xrTableCell227
            // 
            this.xrTableCell227.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneHistory.TelephoneType", "")});
            this.xrTableCell227.Name = "xrTableCell227";
            this.xrTableCell227.Text = "xrTableCell227";
            this.xrTableCell227.Weight = 0.21969696969696967;
            // 
            // xrTableCell224
            // 
            this.xrTableCell224.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneHistory.TelephoneNo", "")});
            this.xrTableCell224.Name = "xrTableCell224";
            this.xrTableCell224.Weight = 0.50378787878787867;
            // 
            // xrTableCell226
            // 
            this.xrTableCell226.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneHistory.EmailAddress", "")});
            this.xrTableCell226.Name = "xrTableCell226";
            this.xrTableCell226.Weight = 1.928030303030303;
            // 
            // GroupHeader12
            // 
            this.GroupHeader12.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable35,
            this.xrTable34});
            this.GroupHeader12.Height = 83;
            this.GroupHeader12.Name = "GroupHeader12";
            // 
            // xrTable35
            // 
            this.xrTable35.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.xrTable35.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable35.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable35.Location = new System.Drawing.Point(8, 58);
            this.xrTable35.Name = "xrTable35";
            this.xrTable35.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow43});
            this.xrTable35.Size = new System.Drawing.Size(792, 25);
            this.xrTable35.StylePriority.UseBorderColor = false;
            this.xrTable35.StylePriority.UseBorders = false;
            // 
            // xrTableRow43
            // 
            this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell228,
            this.xrTableCell222,
            this.xrTableCell229,
            this.xrTableCell223});
            this.xrTableRow43.Name = "xrTableRow43";
            this.xrTableRow43.Weight = 1;
            // 
            // xrTableCell228
            // 
            this.xrTableCell228.CanGrow = false;
            this.xrTableCell228.Name = "xrTableCell228";
            this.xrTableCell228.Text = "Bureau Update";
            this.xrTableCell228.Weight = 0.34848484848484851;
            // 
            // xrTableCell222
            // 
            this.xrTableCell222.CanGrow = false;
            this.xrTableCell222.Name = "xrTableCell222";
            this.xrTableCell222.Text = "Type";
            this.xrTableCell222.Weight = 0.21969696969696967;
            // 
            // xrTableCell229
            // 
            this.xrTableCell229.CanGrow = false;
            this.xrTableCell229.Name = "xrTableCell229";
            this.xrTableCell229.Text = "Telephone No";
            this.xrTableCell229.Weight = 0.5037878787878789;
            // 
            // xrTableCell223
            // 
            this.xrTableCell223.CanGrow = false;
            this.xrTableCell223.Name = "xrTableCell223";
            this.xrTableCell223.Text = "Email Address";
            this.xrTableCell223.Weight = 1.928030303030303;
            // 
            // xrTable34
            // 
            this.xrTable34.ForeColor = System.Drawing.Color.Black;
            this.xrTable34.Location = new System.Drawing.Point(8, 17);
            this.xrTable34.Name = "xrTable34";
            this.xrTable34.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow42});
            this.xrTable34.Size = new System.Drawing.Size(433, 25);
            this.xrTable34.StylePriority.UseForeColor = false;
            // 
            // xrTableRow42
            // 
            this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell221});
            this.xrTableRow42.Name = "xrTableRow42";
            this.xrTableRow42.Weight = 1;
            // 
            // xrTableCell221
            // 
            this.xrTableCell221.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell221.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell221.Name = "xrTableCell221";
            this.xrTableCell221.StylePriority.UseFont = false;
            this.xrTableCell221.StylePriority.UseForeColor = false;
            this.xrTableCell221.Text = "Telephone History";
            this.xrTableCell221.Weight = 3;
            // 
            // DetailReport13
            // 
            this.DetailReport13.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail14,
            this.GroupHeader13});
            this.DetailReport13.DataMember = "ConsumerEmploymentHistory";
            this.DetailReport13.Level = 13;
            this.DetailReport13.Name = "DetailReport13";
            // 
            // Detail14
            // 
            this.Detail14.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.Detail14.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Detail14.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable39});
            this.Detail14.Height = 27;
            this.Detail14.Name = "Detail14";
            this.Detail14.StylePriority.UseBorderColor = false;
            this.Detail14.StylePriority.UseBorders = false;
            // 
            // xrTable39
            // 
            this.xrTable39.Location = new System.Drawing.Point(8, 0);
            this.xrTable39.Name = "xrTable39";
            this.xrTable39.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow47});
            this.xrTable39.Size = new System.Drawing.Size(792, 25);
            // 
            // xrTableRow47
            // 
            this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell234,
            this.xrTableCell235,
            this.xrTableCell236});
            this.xrTableRow47.Name = "xrTableRow47";
            this.xrTableRow47.Weight = 1;
            // 
            // xrTableCell234
            // 
            this.xrTableCell234.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerEmploymentHistory.LastUpdatedDate", "")});
            this.xrTableCell234.Name = "xrTableCell234";
            this.xrTableCell234.Weight = 0.34848484848484851;
            // 
            // xrTableCell235
            // 
            this.xrTableCell235.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerEmploymentHistory.EmployerDetail", "")});
            this.xrTableCell235.Name = "xrTableCell235";
            this.xrTableCell235.Weight = 1.4204545454545454;
            // 
            // xrTableCell236
            // 
            this.xrTableCell236.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerEmploymentHistory.Designation", "")});
            this.xrTableCell236.Name = "xrTableCell236";
            this.xrTableCell236.Weight = 1.231060606060606;
            // 
            // GroupHeader13
            // 
            this.GroupHeader13.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable38,
            this.xrTable37});
            this.GroupHeader13.Height = 73;
            this.GroupHeader13.Name = "GroupHeader13";
            // 
            // xrTable38
            // 
            this.xrTable38.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.xrTable38.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable38.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable38.Location = new System.Drawing.Point(8, 48);
            this.xrTable38.Name = "xrTable38";
            this.xrTable38.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow46});
            this.xrTable38.Size = new System.Drawing.Size(792, 25);
            this.xrTable38.StylePriority.UseBorderColor = false;
            this.xrTable38.StylePriority.UseBorders = false;
            // 
            // xrTableRow46
            // 
            this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell231,
            this.xrTableCell232,
            this.xrTableCell233});
            this.xrTableRow46.Name = "xrTableRow46";
            this.xrTableRow46.Weight = 1;
            // 
            // xrTableCell231
            // 
            this.xrTableCell231.CanGrow = false;
            this.xrTableCell231.Name = "xrTableCell231";
            this.xrTableCell231.Text = "Bureau Update";
            this.xrTableCell231.Weight = 0.34848484848484851;
            // 
            // xrTableCell232
            // 
            this.xrTableCell232.CanGrow = false;
            this.xrTableCell232.Name = "xrTableCell232";
            this.xrTableCell232.Text = "Employer";
            this.xrTableCell232.Weight = 1.4204545454545454;
            // 
            // xrTableCell233
            // 
            this.xrTableCell233.CanGrow = false;
            this.xrTableCell233.Name = "xrTableCell233";
            this.xrTableCell233.Text = "Designation";
            this.xrTableCell233.Weight = 1.231060606060606;
            // 
            // xrTable37
            // 
            this.xrTable37.ForeColor = System.Drawing.Color.Black;
            this.xrTable37.Location = new System.Drawing.Point(8, 8);
            this.xrTable37.Name = "xrTable37";
            this.xrTable37.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow45});
            this.xrTable37.Size = new System.Drawing.Size(433, 25);
            this.xrTable37.StylePriority.UseForeColor = false;
            // 
            // xrTableRow45
            // 
            this.xrTableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell230});
            this.xrTableRow45.Name = "xrTableRow45";
            this.xrTableRow45.Weight = 1;
            // 
            // xrTableCell230
            // 
            this.xrTableCell230.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell230.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell230.Name = "xrTableCell230";
            this.xrTableCell230.StylePriority.UseFont = false;
            this.xrTableCell230.StylePriority.UseForeColor = false;
            this.xrTableCell230.Text = "Employment History";
            this.xrTableCell230.Weight = 3;
            // 
            // DetailReport7
            // 
            this.DetailReport7.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail8});
            this.DetailReport7.DataMember = "ConsumerIdentityVerification";
            this.DetailReport7.Level = 1;
            this.DetailReport7.Name = "DetailReport7";
            // 
            // Detail8
            // 
            this.Detail8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblHomeAffairsH,
            this.tblnote,
            this.tblHomeAffairsD});
            this.Detail8.Height = 250;
            this.Detail8.Name = "Detail8";
            // 
            // tblHomeAffairsH
            // 
            this.tblHomeAffairsH.ForeColor = System.Drawing.Color.Black;
            this.tblHomeAffairsH.Location = new System.Drawing.Point(8, 17);
            this.tblHomeAffairsH.Name = "tblHomeAffairsH";
            this.tblHomeAffairsH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow53});
            this.tblHomeAffairsH.Size = new System.Drawing.Size(433, 25);
            this.tblHomeAffairsH.StylePriority.UseForeColor = false;
            // 
            // xrTableRow53
            // 
            this.xrTableRow53.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell254});
            this.xrTableRow53.Name = "xrTableRow53";
            this.xrTableRow53.Weight = 1;
            // 
            // xrTableCell254
            // 
            this.xrTableCell254.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell254.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell254.Name = "xrTableCell254";
            this.xrTableCell254.StylePriority.UseFont = false;
            this.xrTableCell254.StylePriority.UseForeColor = false;
            this.xrTableCell254.Text = "Consumer Home Affairs";
            this.xrTableCell254.Weight = 3;
            // 
            // tblnote
            // 
            this.tblnote.Location = new System.Drawing.Point(8, 58);
            this.tblnote.Name = "tblnote";
            this.tblnote.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow52});
            this.tblnote.Size = new System.Drawing.Size(792, 58);
            // 
            // xrTableRow52
            // 
            this.xrTableRow52.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell253});
            this.xrTableRow52.Name = "xrTableRow52";
            this.xrTableRow52.Weight = 1;
            // 
            // xrTableCell253
            // 
            this.xrTableCell253.Name = "xrTableCell253";
            this.xrTableCell253.Text = resources.GetString("xrTableCell253.Text");
            this.xrTableCell253.Weight = 3;
            // 
            // tblHomeAffairsD
            // 
            this.tblHomeAffairsD.Location = new System.Drawing.Point(8, 117);
            this.tblHomeAffairsD.Name = "tblHomeAffairsD";
            this.tblHomeAffairsD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow48,
            this.xrTableRow49,
            this.xrTableRow50,
            this.xrTableRow51});
            this.tblHomeAffairsD.Size = new System.Drawing.Size(792, 100);
            // 
            // xrTableRow48
            // 
            this.xrTableRow48.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblXDSDeceasedStatus,
            this.lblXDSDeceasedStatusD,
            this.lblXDSDeceasedDate,
            this.lblXDSDeceasedDateD});
            this.xrTableRow48.Name = "xrTableRow48";
            this.xrTableRow48.Weight = 1;
            // 
            // lblXDSDeceasedStatus
            // 
            this.lblXDSDeceasedStatus.Name = "lblXDSDeceasedStatus";
            this.lblXDSDeceasedStatus.Text = "XDS Bureau Deceased Status";
            this.lblXDSDeceasedStatus.Weight = 0.85333333333333339;
            // 
            // lblXDSDeceasedStatusD
            // 
            this.lblXDSDeceasedStatusD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerIdentityVerification.XDSDeceasedStatus", "")});
            this.lblXDSDeceasedStatusD.Name = "lblXDSDeceasedStatusD";
            this.lblXDSDeceasedStatusD.Text = "xrTableCell4";
            this.lblXDSDeceasedStatusD.Weight = 0.70666666666666655;
            // 
            // lblXDSDeceasedDate
            // 
            this.lblXDSDeceasedDate.Name = "lblXDSDeceasedDate";
            this.lblXDSDeceasedDate.Text = "XDS Bureau deceased Date";
            this.lblXDSDeceasedDate.Weight = 0.77111111111111108;
            // 
            // lblXDSDeceasedDateD
            // 
            this.lblXDSDeceasedDateD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerIdentityVerification.XDSDeceasedDate", "")});
            this.lblXDSDeceasedDateD.Name = "lblXDSDeceasedDateD";
            this.lblXDSDeceasedDateD.Text = "xrTableCell6";
            this.lblXDSDeceasedDateD.Weight = 0.66888888888888887;
            // 
            // xrTableRow49
            // 
            this.xrTableRow49.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblHAFirstName,
            this.lblHAFirstNameD,
            this.lblHASurname,
            this.lblHASurnameD});
            this.xrTableRow49.Name = "xrTableRow49";
            this.xrTableRow49.Weight = 1;
            // 
            // lblHAFirstName
            // 
            this.lblHAFirstName.Name = "lblHAFirstName";
            this.lblHAFirstName.Text = "Home Affairs First Name";
            this.lblHAFirstName.Weight = 0.85333333333333339;
            // 
            // lblHAFirstNameD
            // 
            this.lblHAFirstNameD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerIdentityVerification.HomeAffairsFirstName", "")});
            this.lblHAFirstNameD.Name = "lblHAFirstNameD";
            this.lblHAFirstNameD.Text = "xrTableCell17";
            this.lblHAFirstNameD.Weight = 0.70666666666666655;
            // 
            // lblHASurname
            // 
            this.lblHASurname.Name = "lblHASurname";
            this.lblHASurname.Text = "Home Affairs Surname";
            this.lblHASurname.Weight = 0.77111111111111108;
            // 
            // lblHASurnameD
            // 
            this.lblHASurnameD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerIdentityVerification.HomeAffairsSurname", "")});
            this.lblHASurnameD.Name = "lblHASurnameD";
            this.lblHASurnameD.Text = "xrTableCell19";
            this.lblHASurnameD.Weight = 0.66888888888888887;
            // 
            // xrTableRow50
            // 
            this.xrTableRow50.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblHADeceasedStats,
            this.lblHADeceasedStatsD,
            this.lblHADeceasedDate,
            this.lblHADeceasedDateD});
            this.xrTableRow50.Name = "xrTableRow50";
            this.xrTableRow50.Weight = 1;
            // 
            // lblHADeceasedStats
            // 
            this.lblHADeceasedStats.Name = "lblHADeceasedStats";
            this.lblHADeceasedStats.Text = "Home Affairs Deceased Status ";
            this.lblHADeceasedStats.Weight = 0.85333333333333339;
            // 
            // lblHADeceasedStatsD
            // 
            this.lblHADeceasedStatsD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerIdentityVerification.HomeAffairsDeceasedStatus", "")});
            this.lblHADeceasedStatsD.Name = "lblHADeceasedStatsD";
            this.lblHADeceasedStatsD.Text = "xrTableCell13";
            this.lblHADeceasedStatsD.Weight = 0.70666666666666655;
            // 
            // lblHADeceasedDate
            // 
            this.lblHADeceasedDate.Name = "lblHADeceasedDate";
            this.lblHADeceasedDate.Text = "Home Affairs Deceased Date";
            this.lblHADeceasedDate.Weight = 0.77111111111111108;
            // 
            // lblHADeceasedDateD
            // 
            this.lblHADeceasedDateD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerIdentityVerification.HomeAffairsDeceasedDate", "")});
            this.lblHADeceasedDateD.Name = "lblHADeceasedDateD";
            this.lblHADeceasedDateD.Text = "xrTableCell15";
            this.lblHADeceasedDateD.Weight = 0.66888888888888887;
            // 
            // xrTableRow51
            // 
            this.xrTableRow51.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblHACauseofdeath,
            this.lblHACauseofdeathD,
            this.xrTableCell251,
            this.xrTableCell252});
            this.xrTableRow51.Name = "xrTableRow51";
            this.xrTableRow51.Weight = 1;
            // 
            // lblHACauseofdeath
            // 
            this.lblHACauseofdeath.Name = "lblHACauseofdeath";
            this.lblHACauseofdeath.Text = "Home Affairs Cause of Death";
            this.lblHACauseofdeath.Weight = 0.85333333333333339;
            // 
            // lblHACauseofdeathD
            // 
            this.lblHACauseofdeathD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerIdentityVerification.HomeAffairsCauseOfDeath", "")});
            this.lblHACauseofdeathD.Name = "lblHACauseofdeathD";
            this.lblHACauseofdeathD.Text = "xrTableCell9";
            this.lblHACauseofdeathD.Weight = 0.70666666666666655;
            // 
            // xrTableCell251
            // 
            this.xrTableCell251.Name = "xrTableCell251";
            this.xrTableCell251.Weight = 0.77111111111111108;
            // 
            // xrTableCell252
            // 
            this.xrTableCell252.Name = "xrTableCell252";
            this.xrTableCell252.Weight = 0.66888888888888887;
            // 
            // ConsumerPrescreenTraceReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageFooter,
            this.DetailReport,
            this.DetailReport1,
            this.DetailReport2,
            this.DetailReport3,
            this.ReportHeader,
            this.DetailReport4,
            this.DetailReport5,
            this.DetailReport6,
            this.DetailReport8,
            this.DetailReport9,
            this.DetailReport10,
            this.DetailReport11,
            this.DetailReport12,
            this.DetailReport13,
            this.DetailReport7});
            this.Margins = new System.Drawing.Printing.Margins(20, 20, 100, 100);
            this.Name = "ConsumerPrescreenTraceReport";
            this.PageHeight = 1100;
            this.PageWidth = 850;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1});
            this.Version = "9.1";
            this.XmlDataPath = "C:\\Consumer Report.xml";
            ((System.ComponentModel.ISupportInitialize)(this.TblBusInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblHomeAffairsH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblnote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblHomeAffairsD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTable TblBusInfo;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport1;
        private DevExpress.XtraReports.UI.DetailBand Detail2;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader2;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport2;
        private DevExpress.XtraReports.UI.DetailBand Detail3;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport3;
        private DevExpress.XtraReports.UI.DetailBand Detail4;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle1;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo2;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport4;
        private DevExpress.XtraReports.UI.DetailBand Detail5;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader5;
        private DevExpress.XtraReports.UI.XRTable xrTable12;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport5;
        private DevExpress.XtraReports.UI.DetailBand Detail6;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader6;
        private DevExpress.XtraReports.UI.XRTable xrTable15;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell116;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTable xrTable9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell98;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell96;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader3;
        private DevExpress.XtraReports.UI.XRTable xrTable8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo3;
        private DevExpress.XtraReports.UI.XRTable xrTable11;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell102;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell103;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader4;
        private DevExpress.XtraReports.UI.XRTable xrTable10;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell99;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRTable xrTable14;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        private DevExpress.XtraReports.UI.XRTable xrTable13;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell111;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTable xrTable16;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell123;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell120;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell124;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell117;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell121;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
        private DevExpress.XtraReports.UI.XRTable xrTable18;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell135;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell136;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell137;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell139;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell140;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell141;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell143;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport6;
        private DevExpress.XtraReports.UI.DetailBand Detail7;
        private DevExpress.XtraReports.UI.XRTable xrTable20;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell151;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell148;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell130;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell152;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell149;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell153;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell150;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell132;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader7;
        private DevExpress.XtraReports.UI.XRTable xrTable19;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell134;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell133;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell127;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell146;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell144;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell128;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell147;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell145;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell129;
        private DevExpress.XtraReports.UI.XRTable xrTable17;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell126;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport8;
        private DevExpress.XtraReports.UI.DetailBand Detail9;
        private DevExpress.XtraReports.UI.XRTable xrTable23;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell164;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell165;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell166;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell167;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell168;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell169;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell170;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell171;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell172;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader8;
        private DevExpress.XtraReports.UI.XRTable xrTable22;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell155;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell156;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell157;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell158;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell159;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell160;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell161;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell162;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell163;
        private DevExpress.XtraReports.UI.XRTable xrTable21;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell154;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport9;
        private DevExpress.XtraReports.UI.DetailBand Detail10;
        private DevExpress.XtraReports.UI.XRTable xrTable26;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell183;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell177;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell184;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell178;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell185;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell179;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader9;
        private DevExpress.XtraReports.UI.XRTable xrTable25;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell180;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell174;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell181;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell175;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell182;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell176;
        private DevExpress.XtraReports.UI.XRTable xrTable24;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell173;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport10;
        private DevExpress.XtraReports.UI.DetailBand Detail11;
        private DevExpress.XtraReports.UI.XRTable xrTable30;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell200;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell191;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell201;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell192;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell202;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell193;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader10;
        private DevExpress.XtraReports.UI.XRTable xrTable28;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell187;
        private DevExpress.XtraReports.UI.XRTable xrTable27;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell186;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell203;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell204;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell205;
        private DevExpress.XtraReports.UI.XRTable xrTable31;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell206;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell207;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell208;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell209;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell210;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell211;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell212;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell213;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell214;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport11;
        private DevExpress.XtraReports.UI.DetailBand Detail12;
        private DevExpress.XtraReports.UI.XRTable xrTable33;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell217;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell195;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell218;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell196;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell219;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell220;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell197;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader11;
        private DevExpress.XtraReports.UI.XRTable xrTable32;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell198;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell189;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell199;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell190;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell215;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell216;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell194;
        private DevExpress.XtraReports.UI.XRTable xrTable29;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell188;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport12;
        private DevExpress.XtraReports.UI.DetailBand Detail13;
        private DevExpress.XtraReports.UI.XRTable xrTable36;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell225;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell227;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell224;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell226;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader12;
        private DevExpress.XtraReports.UI.XRTable xrTable35;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell228;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell222;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell229;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell223;
        private DevExpress.XtraReports.UI.XRTable xrTable34;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell221;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport13;
        private DevExpress.XtraReports.UI.DetailBand Detail14;
        private DevExpress.XtraReports.UI.XRTable xrTable39;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell234;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell235;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell236;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader13;
        private DevExpress.XtraReports.UI.XRTable xrTable38;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell231;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell232;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell233;
        private DevExpress.XtraReports.UI.XRTable xrTable37;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell230;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport7;
        private DevExpress.XtraReports.UI.DetailBand Detail8;
        private DevExpress.XtraReports.UI.XRTable tblHomeAffairsD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow48;
        private DevExpress.XtraReports.UI.XRTableCell lblXDSDeceasedStatus;
        private DevExpress.XtraReports.UI.XRTableCell lblXDSDeceasedStatusD;
        private DevExpress.XtraReports.UI.XRTableCell lblXDSDeceasedDate;
        private DevExpress.XtraReports.UI.XRTableCell lblXDSDeceasedDateD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow49;
        private DevExpress.XtraReports.UI.XRTableCell lblHAFirstName;
        private DevExpress.XtraReports.UI.XRTableCell lblHAFirstNameD;
        private DevExpress.XtraReports.UI.XRTableCell lblHASurname;
        private DevExpress.XtraReports.UI.XRTableCell lblHASurnameD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow50;
        private DevExpress.XtraReports.UI.XRTableCell lblHADeceasedStats;
        private DevExpress.XtraReports.UI.XRTableCell lblHADeceasedStatsD;
        private DevExpress.XtraReports.UI.XRTableCell lblHADeceasedDate;
        private DevExpress.XtraReports.UI.XRTableCell lblHADeceasedDateD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow51;
        private DevExpress.XtraReports.UI.XRTableCell lblHACauseofdeath;
        private DevExpress.XtraReports.UI.XRTableCell lblHACauseofdeathD;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell251;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell252;
        private DevExpress.XtraReports.UI.XRTable tblHomeAffairsH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell254;
        private DevExpress.XtraReports.UI.XRTable tblnote;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell253;
    }
}
