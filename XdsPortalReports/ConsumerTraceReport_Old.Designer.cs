namespace XdsPortalReports
{
    partial class ConsumerTraceReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblHeaderText = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblPersonalDetail = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ConsumerDetail = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.PersonalDetailsSummary = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblReferenceNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblRefnoValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblExtRefNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblExternalRefNoValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblIDNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblIDnoValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPPNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPassportnoValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSurname = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSurnameValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblResAdd = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblResAddValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblFirstname = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblFirstnameValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbl = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSecondName = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSecondnameValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHomeTelNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHomeTelNoValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblTitle = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbltitlevalue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPostalAdd = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPostalAddvalue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblGender = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGenderValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbl2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbl2value = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDOB = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDOBValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblWorkTelNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblWorkTelNoValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblCurrentEmployer = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCurrentEmployerValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMobileNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMobileNoValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblEmailAddress = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblEmailAddressValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaritalStatus = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaritalStatusValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.FraudIndicatorSummary = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblFraudIndSummary = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblVerified = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblVerifiedVal = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDeceased = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDeceasedValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblFoundDB = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblFoundDBValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblEmployerFraudDB = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblEmployerFraudDBValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblProrectiveReg = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblProrectiveRegValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader2 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblFraudSummary = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Scoring = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail3 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblscrorevalues = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblUniqueIdentifierValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDateValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblScroreValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblExceptionValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader3 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblscoreColumns = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblUniqueIdentifier = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblScrore = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblException = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblScoreHeader = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DebtSummary = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail4 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblDebtSummary = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblActiveAcc = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblActiveAccValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblAccGood = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAccGoodvalue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDelinquentAcc = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDelinquentAccValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblMonthlyinst = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMonthlyinstValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblOutstandingDebt = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblOutstandingDebtValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblArrearamt = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblArrearamtvalue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblJdgDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJdgDateValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblJudAmt = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJudAmtValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDelinquentrating = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDelinquentratingValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDbtReview = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDbtReviewValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblAO = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAOValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblAdverseAmt = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAdverseAmtValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader4 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblDebtHeader = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.AccountGoodBadSummary = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail5 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblAccStatus = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblAccdesc = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGood = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblBad = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblRetail = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblRetailG = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblRetailB = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblCc = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCreditCradG = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCreditCardB = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblFurnAcc = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblFurnAccG = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblFurnAccB = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblInsurance = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblInsuranceG = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblInsuranceB = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblFinance = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblFinanceG = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblFinanceB = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblBankAcc = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblBankAccG = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblBankAccB = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblTelecomms = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTelecommsG = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTelecommsB = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblHomeLoan = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHomeLoanG = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHomeLoanB = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblVehicleFin = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblVehicleFinG = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblVehicleFinB = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblOtherAcc = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblOtherAccG = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblOtherAccB = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader5 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblCrAccStatus = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.NameConfirmation = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail6 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblNameHistoryValues = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow52 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblNameValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNHSurNameValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNHIDNoValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNHdatevalue = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader6 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblNameHistColumns = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow51 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblName = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNHSurName = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNHIDNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNHdate = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblNameHistoryHeader = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow50 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblpersonalConfirmationHeader = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow49 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblDetailedInfoHeader = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow48 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.AddressConfirmation = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail7 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow55 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblCDAddType = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCDAddLine1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCDAddLine2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCDAddLine3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCDAddLine4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCDdate = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader7 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblConfAddHistoryC = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow54 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblCAddType = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCAddLine1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCAddLine2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCAddLine3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCAddLine4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCdate = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblAddHistoryHeader = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow53 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ContactNoConfirmation = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail8 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblCContactNoD = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow58 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblCLLNoD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCConfirmedLLD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCCellNoD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCConfirmedCellNoD = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader8 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblCContactNoC = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow57 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblCLLNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCConfirmedLL = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCCellNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCConfirmedCellNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblCContactNoH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow56 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.EmploymentDetailConfirmation = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail9 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblEmpHisD = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow61 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblCEmployerD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCDesignationD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCConfirmeddateD = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader9 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblEmpHisC = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow60 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblCEmployer = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCDesignation = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCConfirmeddate = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblEmploymentHisH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow59 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.NameHistory = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail10 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblNamehistD = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow65 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDUpdatedDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHDSurname = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHDFirstname = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHDSecodnName = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHDInitial = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHDTitle = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHDIDno = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHDPPNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHDBirthDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader10 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblNameHisC = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow64 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblUpdatedDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHSurname = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHFirstname = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHSecodnName = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHInitial = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHTitle = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHIDno = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHPPNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHBirthDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblNameHistH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow63 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblConsInfoH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow62 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.AddressHistory = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail11 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblAddHD = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow68 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDAddUpdatedate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAddtype = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAddline1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAddLine2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAddLine3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAddLine4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDPostalcode = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader11 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblAddHC = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow67 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblAddUpdatedate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAddtype = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAddline1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAddLine2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAddLine3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAddLine4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPostalcode = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblAddHistoryH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow66 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell188 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TelephoneHistory = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail12 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblContactNoD = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow71 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDNoUpdatedate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDTelnoType = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDTelNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDEmail = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader12 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblContactNoC = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow70 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblNoUpdatedate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTelnoType = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTelNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblEmail = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblContactNoH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow69 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell221 = new DevExpress.XtraReports.UI.XRTableCell();
            this.EmploymentHistory = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail13 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblEmploymentD = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow74 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDEmpUpdatedate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDEmployer = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDDesignation = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader13 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblEmploymentC = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow73 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblEmpUpdatedate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblEmployer = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDesignation = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblEmploymentH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow72 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell230 = new DevExpress.XtraReports.UI.XRTableCell();
            this.AccountStatus = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail14 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblAccStatusD = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow76 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblAccOpenDateD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCompanyD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCreditLimitD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCurrBalanceD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblInstallementD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAccTypeD = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader14 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblAccStatusC = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow77 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblAccOpenDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCompany = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCreditLimit = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCurrBalance = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblInstallement = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblArrearsAmt = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAccType = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblAccStatusH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow75 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MonthlyPaymentHeader = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail15 = new DevExpress.XtraReports.UI.DetailBand();
            this.GroupHeader15 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblMonthlyPaymentH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow78 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblcompanyMP = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM01 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM02 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM03 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM04 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM05 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM06 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM08 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM09 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.Definition = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail16 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblDefinitionD = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow81 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDefDescD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbDefCodeD = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader16 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblDefinitionC = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow80 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDefDesc = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbDefCode = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.AdverseInfo = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail17 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblAdverseData = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow85 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDSubscriber = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAccNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDActionDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAmount = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAccStatus = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAdvComment = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader17 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblAdverseColumns = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow83 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSubscriber = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAccountNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblActionDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAmount = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAccStatus = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAdverseComment = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblAdverseH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow84 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblDomainH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow82 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Judgements = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail18 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblDJudg = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow88 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblJDCaseno = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJDIssueDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJDType = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJDAmount = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJDPlaintiff = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJDCourt = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJDAttorney = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJDPhoneNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJDComment = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader18 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblCJud = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow87 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblJCaseNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJIssueDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJType = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJAmount = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJPlaintiff = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJCourt = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJAttorney = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJPhoneNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblJComment = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblJudgmentsH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow86 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.AdminOrders = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail19 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblDAO = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow91 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblAODCaseno = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAODIssuedate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAODLoadeddate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAODType = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAODAmount = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAODPlaintiff = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAODCOurt = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAODAttorney = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAODPhoenNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader19 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblCAO = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow90 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblAOCaseno = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAOIssuedate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAODateLoaded = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAOType = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAOAmount = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAOPlaintiff = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAOCourt = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAOAttorney = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAOPhoneNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblAOH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow89 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Sequestration = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail20 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblSEQD = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow94 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSEQDCaseno = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSEQDIssuedate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSEQDLoadedDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSEQDType = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSEQDAmount = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSEQDPlaintiff = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSEQDCourt = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSEQDAttorney = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSEQDPhoneNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader20 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblSEQC = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow93 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSEQCaseno = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSEQIssueDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSEQLoadeddate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSEQType = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSEQAmount = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSEQplaintiff = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSEQCourt = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSEQAttorney = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSEQPhoneNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblSEQH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow92 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell156 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DebtReviewStatus = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail21 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblDbtReviewD = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow96 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDbtReviewDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDbtReviewDateD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow98 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblCounsellorName = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCounsellorNameD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow97 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblCounsellorTelephoneNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCounsellorTelephoneNoD = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader21 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tbldbtReviewH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow95 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell175 = new DevExpress.XtraReports.UI.XRTableCell();
            this.EnquiryHistory = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail22 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblEnquiryD = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow101 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblEnqDateD = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblsubscribernameD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblbusTypeD = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader22 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblEnquiryC = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow100 = new DevExpress.XtraReports.UI.XRTableRow();
            this.tblEnqDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblsubscribername = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblbusType = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblEnquiryH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow99 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell182 = new DevExpress.XtraReports.UI.XRTableCell();
            this.propertyInfo = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail23 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblPropInterests = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow120 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDeedNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDDeedNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblsiteno = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDsiteno = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow125 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDeedsoffice = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDDeedsoffice = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhyadd = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDPhyadd = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow124 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblPropType = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDPropType = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSize = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDSize = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow123 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblPurchasedate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDPurchasedate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPurchaseprice = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDPurchaseprice = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow122 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblshareperc = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDshareperc = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHolder = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDHolder = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow121 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblBondNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDBondNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblBondAmount = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDBondAmount = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblPropInterestH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow119 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell281 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader23 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblPropInterestsH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow118 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell280 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DirectorshipLink = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail24 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblDirInfo = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow113 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblCurrentPost = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDCurrentPost = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblInceptionDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDAppDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow116 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblCompanyName = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDCompanyName = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblRegNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDRegNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow117 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblCompanyAdd = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDPhysicalAdd = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhoneNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDPhoneNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow115 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblIndustryCatg = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDIndCateg = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblDirectorlinkH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow114 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell274 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader27 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblDirectorlinksH = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow112 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell272 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TelephoneLinkageHome = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail25 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblLinkagesHD = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow105 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblHNameD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHSurD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHHomenoD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHWorkNoD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHCellNoD = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader25 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblLikagesHC = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow104 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblHName = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHSur = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHHomeno = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHWorkNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHCellNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblLinkagesHome = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow103 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell239 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblLinkages = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow102 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell238 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TelephoneLinkageWork = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail26 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblLinkagesWD = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow108 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblWNameD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblWSurD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblWHomenoD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblWWorkNoD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblWCellNoD = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader24 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblLikagesWC = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow107 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblWName = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblWSur = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblWHomeno = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblWWorkNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblWCellNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblLinkagesWork = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow106 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell250 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TelephoneLinkageCellular = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail27 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblLinkagesCD = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow111 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblCNameD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCSurD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCHomenoD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCWorkNoD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCellNoD = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader26 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.tblLikagesCC = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow110 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblCName = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCSur = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCHomeno = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCWorkNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCellNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblLinkagesCell = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow109 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell261 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MonthlyPayment = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail28 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblMonthlyPaymentD = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow79 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblcompanyMPD = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM01D = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM02D = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM03D = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM04D = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM05D = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM06D = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM07D = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM08D = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM09D = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM10D = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM11D = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM12D = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM13D = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM14D = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM15D = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM16D = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM17D = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM18D = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM19D = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM20D = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM21D = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM22D = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM23D = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblM24D = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblPersonalDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonalDetailsSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblFraudIndSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblFraudSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblscrorevalues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblscoreColumns)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblScoreHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDebtSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDebtHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAccStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblCrAccStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblNameHistoryValues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblNameHistColumns)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblNameHistoryHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblpersonalConfirmationHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDetailedInfoHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblConfAddHistoryC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAddHistoryHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblCContactNoD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblCContactNoC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblCContactNoH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEmpHisD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEmpHisC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEmploymentHisH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblNamehistD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblNameHisC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblNameHistH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblConsInfoH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAddHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAddHC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAddHistoryH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblContactNoD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblContactNoC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblContactNoH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEmploymentD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEmploymentC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEmploymentH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAccStatusD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAccStatusC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAccStatusH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblMonthlyPaymentH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDefinitionD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDefinitionC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAdverseData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAdverseColumns)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAdverseH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDomainH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDJudg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblCJud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblJudgmentsH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDAO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblCAO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAOH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblSEQD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblSEQC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblSEQH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDbtReviewD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbldbtReviewH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEnquiryD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEnquiryC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEnquiryH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblPropInterests)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblPropInterestH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblPropInterestsH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDirInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDirectorlinkH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDirectorlinksH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblLinkagesHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblLikagesHC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblLinkagesHome)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblLinkages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblLinkagesWD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblLikagesWC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblLinkagesWork)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblLinkagesCD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblLikagesCC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblLinkagesCell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblMonthlyPaymentD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Height = 25;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 30;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2,
            this.xrPageInfo1,
            this.xrTable1});
            this.ReportHeader.Height = 53;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrPageInfo2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPageInfo2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrPageInfo2.Format = "Page {0} of {1}";
            this.xrPageInfo2.Location = new System.Drawing.Point(642, 8);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo2.Size = new System.Drawing.Size(67, 33);
            this.xrPageInfo2.StylePriority.UseBorderColor = false;
            this.xrPageInfo2.StylePriority.UseBorders = false;
            this.xrPageInfo2.StylePriority.UseFont = false;
            this.xrPageInfo2.StylePriority.UseTextAlignment = false;
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrPageInfo1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPageInfo1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrPageInfo1.Format = "{0:\"Report Printed Date\" dd/MM/yyyy HH:mm:ss}";
            this.xrPageInfo1.Location = new System.Drawing.Point(401, 8);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo1.Size = new System.Drawing.Size(240, 33);
            this.xrPageInfo1.StylePriority.UseBorderColor = false;
            this.xrPageInfo1.StylePriority.UseBorders = false;
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable1
            // 
            this.xrTable1.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Font = new System.Drawing.Font("Book Antiqua", 11F, System.Drawing.FontStyle.Bold);
            this.xrTable1.Location = new System.Drawing.Point(9, 8);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable1.Size = new System.Drawing.Size(392, 33);
            this.xrTable1.StylePriority.UseBorderColor = false;
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblHeaderText});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1;
            // 
            // lblHeaderText
            // 
            this.lblHeaderText.BackColor = System.Drawing.Color.Honeydew;
            this.lblHeaderText.Font = new System.Drawing.Font("Tahoma", 18.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeaderText.Name = "lblHeaderText";
            this.lblHeaderText.StylePriority.UseBackColor = false;
            this.lblHeaderText.StylePriority.UseFont = false;
            this.lblHeaderText.StylePriority.UseTextAlignment = false;
            this.lblHeaderText.Text = "XDS Consumer Trace Report";
            this.lblHeaderText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblHeaderText.Weight = 1.9059967585089144;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblPersonalDetail,
            this.xrTable2});
            this.GroupHeader1.Height = 101;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // tblPersonalDetail
            // 
            this.tblPersonalDetail.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblPersonalDetail.Location = new System.Drawing.Point(8, 67);
            this.tblPersonalDetail.Name = "tblPersonalDetail";
            this.tblPersonalDetail.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow12});
            this.tblPersonalDetail.Size = new System.Drawing.Size(292, 25);
            this.tblPersonalDetail.StylePriority.UseFont = false;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 1;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.Text = "Personal Details Summary";
            this.xrTableCell7.Weight = 3;
            // 
            // xrTable2
            // 
            this.xrTable2.Font = new System.Drawing.Font("Book Antiqua", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable2.Location = new System.Drawing.Point(8, 17);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow11});
            this.xrTable2.Size = new System.Drawing.Size(500, 25);
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 1;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Underline);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "SUMMARISED INFORMATION";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell6.Weight = 3;
            // 
            // ConsumerDetail
            // 
            this.ConsumerDetail.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1});
            this.ConsumerDetail.DataMember = "ConsumerDetail";
            this.ConsumerDetail.Level = 0;
            this.ConsumerDetail.Name = "ConsumerDetail";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.PersonalDetailsSummary});
            this.Detail1.Height = 278;
            this.Detail1.Name = "Detail1";
            // 
            // PersonalDetailsSummary
            // 
            this.PersonalDetailsSummary.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.PersonalDetailsSummary.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.PersonalDetailsSummary.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PersonalDetailsSummary.Location = new System.Drawing.Point(8, 17);
            this.PersonalDetailsSummary.Name = "PersonalDetailsSummary";
            this.PersonalDetailsSummary.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow4,
            this.xrTableRow3,
            this.xrTableRow13,
            this.xrTableRow10,
            this.xrTableRow9,
            this.xrTableRow8,
            this.xrTableRow7,
            this.xrTableRow6,
            this.xrTableRow5});
            this.PersonalDetailsSummary.Size = new System.Drawing.Size(692, 250);
            this.PersonalDetailsSummary.StylePriority.UseBorderColor = false;
            this.PersonalDetailsSummary.StylePriority.UseBorders = false;
            this.PersonalDetailsSummary.StylePriority.UseFont = false;
            this.PersonalDetailsSummary.StylePriority.UseTextAlignment = false;
            this.PersonalDetailsSummary.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblReferenceNo,
            this.lblRefnoValue,
            this.lblExtRefNo,
            this.lblExternalRefNoValue});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1;
            // 
            // lblReferenceNo
            // 
            this.lblReferenceNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblReferenceNo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReferenceNo.Name = "lblReferenceNo";
            this.lblReferenceNo.StylePriority.UseBackColor = false;
            this.lblReferenceNo.StylePriority.UseFont = false;
            this.lblReferenceNo.Text = "Reference No.";
            this.lblReferenceNo.Weight = 0.514218009478673;
            // 
            // lblRefnoValue
            // 
            this.lblRefnoValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.ReferenceNo", "")});
            this.lblRefnoValue.Name = "lblRefnoValue";
            this.lblRefnoValue.Weight = 0.97301591650001362;
            // 
            // lblExtRefNo
            // 
            this.lblExtRefNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblExtRefNo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExtRefNo.Name = "lblExtRefNo";
            this.lblExtRefNo.StylePriority.UseBackColor = false;
            this.lblExtRefNo.StylePriority.UseFont = false;
            this.lblExtRefNo.Text = "External Reference No.";
            this.lblExtRefNo.Weight = 0.59807413089335126;
            // 
            // lblExternalRefNoValue
            // 
            this.lblExternalRefNoValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.ExternalReference", "")});
            this.lblExternalRefNoValue.Name = "lblExternalRefNoValue";
            this.lblExternalRefNoValue.Weight = 0.91469194312796209;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblIDNo,
            this.lblIDnoValue,
            this.lblPPNo,
            this.lblPassportnoValue});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1;
            // 
            // lblIDNo
            // 
            this.lblIDNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblIDNo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIDNo.Name = "lblIDNo";
            this.lblIDNo.StylePriority.UseBackColor = false;
            this.lblIDNo.StylePriority.UseFont = false;
            this.lblIDNo.Text = "ID No.";
            this.lblIDNo.Weight = 0.514218009478673;
            // 
            // lblIDnoValue
            // 
            this.lblIDnoValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.IDNo", "")});
            this.lblIDnoValue.Name = "lblIDnoValue";
            this.lblIDnoValue.Weight = 0.97301591650001362;
            // 
            // lblPPNo
            // 
            this.lblPPNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblPPNo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPPNo.Name = "lblPPNo";
            this.lblPPNo.StylePriority.UseBackColor = false;
            this.lblPPNo.StylePriority.UseFont = false;
            this.lblPPNo.Text = "Passport or 2nd ID No.";
            this.lblPPNo.Weight = 0.59807413089335126;
            // 
            // lblPassportnoValue
            // 
            this.lblPassportnoValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.PassportNo", "")});
            this.lblPassportnoValue.Name = "lblPassportnoValue";
            this.lblPassportnoValue.Weight = 0.91469194312796209;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSurname,
            this.lblSurnameValue,
            this.lblResAdd,
            this.lblResAddValue});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1;
            // 
            // lblSurname
            // 
            this.lblSurname.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblSurname.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSurname.Name = "lblSurname";
            this.lblSurname.StylePriority.UseBackColor = false;
            this.lblSurname.StylePriority.UseFont = false;
            this.lblSurname.Text = "Surname";
            this.lblSurname.Weight = 0.514218009478673;
            // 
            // lblSurnameValue
            // 
            this.lblSurnameValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.Surname", "")});
            this.lblSurnameValue.Name = "lblSurnameValue";
            this.lblSurnameValue.Weight = 0.97301591650001362;
            // 
            // lblResAdd
            // 
            this.lblResAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblResAdd.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResAdd.Name = "lblResAdd";
            this.lblResAdd.StylePriority.UseBackColor = false;
            this.lblResAdd.StylePriority.UseFont = false;
            this.lblResAdd.Text = "Residential Address";
            this.lblResAdd.Weight = 0.59807413089335126;
            // 
            // lblResAddValue
            // 
            this.lblResAddValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.ResidentialAddress", "")});
            this.lblResAddValue.Name = "lblResAddValue";
            this.lblResAddValue.Text = "lblResAddValue";
            this.lblResAddValue.Weight = 0.91469194312796209;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblFirstname,
            this.lblFirstnameValue,
            this.lbl,
            this.lblValue});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1;
            // 
            // lblFirstname
            // 
            this.lblFirstname.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblFirstname.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFirstname.Name = "lblFirstname";
            this.lblFirstname.StylePriority.UseBackColor = false;
            this.lblFirstname.StylePriority.UseFont = false;
            this.lblFirstname.Text = "First Name";
            this.lblFirstname.Weight = 0.514218009478673;
            // 
            // lblFirstnameValue
            // 
            this.lblFirstnameValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.FirstName", "")});
            this.lblFirstnameValue.Name = "lblFirstnameValue";
            this.lblFirstnameValue.Weight = 0.97301591650001362;
            // 
            // lbl
            // 
            this.lbl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lbl.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl.Name = "lbl";
            this.lbl.StylePriority.UseBackColor = false;
            this.lbl.StylePriority.UseBorders = false;
            this.lbl.StylePriority.UseFont = false;
            this.lbl.Weight = 0.59807413089335126;
            // 
            // lblValue
            // 
            this.lblValue.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblValue.Name = "lblValue";
            this.lblValue.StylePriority.UseBorders = false;
            this.lblValue.Weight = 0.91469194312796209;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSecondName,
            this.lblSecondnameValue,
            this.lblHomeTelNo,
            this.lblHomeTelNoValue});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1;
            // 
            // lblSecondName
            // 
            this.lblSecondName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblSecondName.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSecondName.Name = "lblSecondName";
            this.lblSecondName.StylePriority.UseBackColor = false;
            this.lblSecondName.StylePriority.UseFont = false;
            this.lblSecondName.Text = "Second Name";
            this.lblSecondName.Weight = 0.514218009478673;
            // 
            // lblSecondnameValue
            // 
            this.lblSecondnameValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.SecondName", "")});
            this.lblSecondnameValue.Name = "lblSecondnameValue";
            this.lblSecondnameValue.Weight = 0.97301591650001362;
            // 
            // lblHomeTelNo
            // 
            this.lblHomeTelNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblHomeTelNo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHomeTelNo.Name = "lblHomeTelNo";
            this.lblHomeTelNo.StylePriority.UseBackColor = false;
            this.lblHomeTelNo.StylePriority.UseFont = false;
            this.lblHomeTelNo.Text = "Telephone No.(H)";
            this.lblHomeTelNo.Weight = 0.59807413089335126;
            // 
            // lblHomeTelNoValue
            // 
            this.lblHomeTelNoValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.HomeTelephoneNo", "")});
            this.lblHomeTelNoValue.Name = "lblHomeTelNoValue";
            this.lblHomeTelNoValue.Text = "lblHomeTelNoValue";
            this.lblHomeTelNoValue.Weight = 0.91469194312796209;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblTitle,
            this.lbltitlevalue,
            this.lblPostalAdd,
            this.lblPostalAddvalue});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1;
            // 
            // lblTitle
            // 
            this.lblTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblTitle.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.StylePriority.UseBackColor = false;
            this.lblTitle.StylePriority.UseFont = false;
            this.lblTitle.Text = "Title";
            this.lblTitle.Weight = 0.514218009478673;
            // 
            // lbltitlevalue
            // 
            this.lbltitlevalue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.TitleDesc", "")});
            this.lbltitlevalue.Name = "lbltitlevalue";
            this.lbltitlevalue.Weight = 0.97301591650001362;
            // 
            // lblPostalAdd
            // 
            this.lblPostalAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblPostalAdd.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPostalAdd.Name = "lblPostalAdd";
            this.lblPostalAdd.StylePriority.UseBackColor = false;
            this.lblPostalAdd.StylePriority.UseFont = false;
            this.lblPostalAdd.Text = "Postal Address";
            this.lblPostalAdd.Weight = 0.59807413089335126;
            // 
            // lblPostalAddvalue
            // 
            this.lblPostalAddvalue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.PostalAddress", "")});
            this.lblPostalAddvalue.Name = "lblPostalAddvalue";
            this.lblPostalAddvalue.Weight = 0.91469194312796209;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblGender,
            this.lblGenderValue,
            this.lbl2,
            this.lbl2value});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1;
            // 
            // lblGender
            // 
            this.lblGender.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblGender.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGender.Name = "lblGender";
            this.lblGender.StylePriority.UseBackColor = false;
            this.lblGender.StylePriority.UseFont = false;
            this.lblGender.Text = "Gender";
            this.lblGender.Weight = 0.514218009478673;
            // 
            // lblGenderValue
            // 
            this.lblGenderValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.Gender", "")});
            this.lblGenderValue.Name = "lblGenderValue";
            this.lblGenderValue.Weight = 0.97301591650001362;
            // 
            // lbl2
            // 
            this.lbl2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lbl2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl2.Name = "lbl2";
            this.lbl2.StylePriority.UseBackColor = false;
            this.lbl2.StylePriority.UseFont = false;
            this.lbl2.Weight = 0.59807413089335126;
            // 
            // lbl2value
            // 
            this.lbl2value.Name = "lbl2value";
            this.lbl2value.Weight = 0.91469194312796209;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDOB,
            this.lblDOBValue,
            this.lblWorkTelNo,
            this.lblWorkTelNoValue});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1;
            // 
            // lblDOB
            // 
            this.lblDOB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblDOB.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDOB.Name = "lblDOB";
            this.lblDOB.StylePriority.UseBackColor = false;
            this.lblDOB.StylePriority.UseFont = false;
            this.lblDOB.Text = "Date of Birth";
            this.lblDOB.Weight = 0.514218009478673;
            // 
            // lblDOBValue
            // 
            this.lblDOBValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.BirthDate", "")});
            this.lblDOBValue.Name = "lblDOBValue";
            this.lblDOBValue.Weight = 0.97301591650001362;
            // 
            // lblWorkTelNo
            // 
            this.lblWorkTelNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblWorkTelNo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWorkTelNo.Name = "lblWorkTelNo";
            this.lblWorkTelNo.StylePriority.UseBackColor = false;
            this.lblWorkTelNo.StylePriority.UseFont = false;
            this.lblWorkTelNo.Text = "Telephone No.(W)";
            this.lblWorkTelNo.Weight = 0.59807413089335126;
            // 
            // lblWorkTelNoValue
            // 
            this.lblWorkTelNoValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.WorkTelephoneNo", "")});
            this.lblWorkTelNoValue.Name = "lblWorkTelNoValue";
            this.lblWorkTelNoValue.Weight = 0.91469194312796209;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCurrentEmployer,
            this.lblCurrentEmployerValue,
            this.lblMobileNo,
            this.lblMobileNoValue});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1;
            // 
            // lblCurrentEmployer
            // 
            this.lblCurrentEmployer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblCurrentEmployer.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentEmployer.Name = "lblCurrentEmployer";
            this.lblCurrentEmployer.StylePriority.UseBackColor = false;
            this.lblCurrentEmployer.StylePriority.UseFont = false;
            this.lblCurrentEmployer.Text = "Current Employer";
            this.lblCurrentEmployer.Weight = 0.514218009478673;
            // 
            // lblCurrentEmployerValue
            // 
            this.lblCurrentEmployerValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.EmployerDetail", "")});
            this.lblCurrentEmployerValue.Name = "lblCurrentEmployerValue";
            this.lblCurrentEmployerValue.Text = "lblCurrentEmployerValue";
            this.lblCurrentEmployerValue.Weight = 0.97301591650001362;
            // 
            // lblMobileNo
            // 
            this.lblMobileNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblMobileNo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMobileNo.Name = "lblMobileNo";
            this.lblMobileNo.StylePriority.UseBackColor = false;
            this.lblMobileNo.StylePriority.UseFont = false;
            this.lblMobileNo.Text = "Cellular/Mobile";
            this.lblMobileNo.Weight = 0.59807413089335126;
            // 
            // lblMobileNoValue
            // 
            this.lblMobileNoValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.CellularNo", "")});
            this.lblMobileNoValue.Name = "lblMobileNoValue";
            this.lblMobileNoValue.Weight = 0.91469194312796209;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblEmailAddress,
            this.lblEmailAddressValue,
            this.lblMaritalStatus,
            this.lblMaritalStatusValue});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1;
            // 
            // lblEmailAddress
            // 
            this.lblEmailAddress.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblEmailAddress.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmailAddress.Name = "lblEmailAddress";
            this.lblEmailAddress.StylePriority.UseBackColor = false;
            this.lblEmailAddress.StylePriority.UseFont = false;
            this.lblEmailAddress.Text = "E-mail Address";
            this.lblEmailAddress.Weight = 0.514218009478673;
            // 
            // lblEmailAddressValue
            // 
            this.lblEmailAddressValue.Name = "lblEmailAddressValue";
            this.lblEmailAddressValue.Text = "lblEmailAddressValue";
            this.lblEmailAddressValue.Weight = 0.97301591650001362;
            // 
            // lblMaritalStatus
            // 
            this.lblMaritalStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblMaritalStatus.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaritalStatus.Name = "lblMaritalStatus";
            this.lblMaritalStatus.StylePriority.UseBackColor = false;
            this.lblMaritalStatus.StylePriority.UseFont = false;
            this.lblMaritalStatus.Text = "Marital Status";
            this.lblMaritalStatus.Weight = 0.59807413089335126;
            // 
            // lblMaritalStatusValue
            // 
            this.lblMaritalStatusValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.MaritalStatusDesc", "")});
            this.lblMaritalStatusValue.Name = "lblMaritalStatusValue";
            this.lblMaritalStatusValue.Weight = 0.91469194312796209;
            // 
            // FraudIndicatorSummary
            // 
            this.FraudIndicatorSummary.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail2,
            this.GroupHeader2});
            this.FraudIndicatorSummary.DataMember = "ConsumerFraudIndicatorsSummary";
            this.FraudIndicatorSummary.Level = 1;
            this.FraudIndicatorSummary.Name = "FraudIndicatorSummary";
            // 
            // Detail2
            // 
            this.Detail2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblFraudIndSummary});
            this.Detail2.Height = 155;
            this.Detail2.Name = "Detail2";
            // 
            // tblFraudIndSummary
            // 
            this.tblFraudIndSummary.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tblFraudIndSummary.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblFraudIndSummary.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblFraudIndSummary.Location = new System.Drawing.Point(8, 17);
            this.tblFraudIndSummary.Name = "tblFraudIndSummary";
            this.tblFraudIndSummary.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow15,
            this.xrTableRow19,
            this.xrTableRow18,
            this.xrTableRow17,
            this.xrTableRow16});
            this.tblFraudIndSummary.Size = new System.Drawing.Size(700, 125);
            this.tblFraudIndSummary.StylePriority.UseBorderColor = false;
            this.tblFraudIndSummary.StylePriority.UseBorders = false;
            this.tblFraudIndSummary.StylePriority.UseFont = false;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblVerified,
            this.lblVerifiedVal});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 1;
            // 
            // lblVerified
            // 
            this.lblVerified.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblVerified.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVerified.Name = "lblVerified";
            this.lblVerified.StylePriority.UseBackColor = false;
            this.lblVerified.StylePriority.UseFont = false;
            this.lblVerified.Text = "ID No. Verified Status At Home Affairs";
            this.lblVerified.Weight = 1.1457142857142857;
            // 
            // lblVerifiedVal
            // 
            this.lblVerifiedVal.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerFraudIndicatorsSummary.HomeAffairsVerificationYN", "")});
            this.lblVerifiedVal.Name = "lblVerifiedVal";
            this.lblVerifiedVal.Text = "lblVerifiedVal";
            this.lblVerifiedVal.Weight = 1.8542857142857143;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDeceased,
            this.lblDeceasedValue});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 1;
            // 
            // lblDeceased
            // 
            this.lblDeceased.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblDeceased.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDeceased.Name = "lblDeceased";
            this.lblDeceased.StylePriority.UseBackColor = false;
            this.lblDeceased.StylePriority.UseFont = false;
            this.lblDeceased.Text = "ID No.Deceased Status At Home Affairs";
            this.lblDeceased.Weight = 1.1457142857142857;
            // 
            // lblDeceasedValue
            // 
            this.lblDeceasedValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerFraudIndicatorsSummary.HomeAffairsDeceasedDate", "")});
            this.lblDeceasedValue.Name = "lblDeceasedValue";
            this.lblDeceasedValue.Text = "lblDeceasedValue";
            this.lblDeceasedValue.Weight = 1.8542857142857143;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblFoundDB,
            this.lblFoundDBValue});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 1;
            // 
            // lblFoundDB
            // 
            this.lblFoundDB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblFoundDB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFoundDB.Name = "lblFoundDB";
            this.lblFoundDB.StylePriority.UseBackColor = false;
            this.lblFoundDB.StylePriority.UseFont = false;
            this.lblFoundDB.Text = "ID No. Found on Fraud Database";
            this.lblFoundDB.Weight = 1.1457142857142857;
            // 
            // lblFoundDBValue
            // 
            this.lblFoundDBValue.Name = "lblFoundDBValue";
            this.lblFoundDBValue.Text = "lblFoundDBValue";
            this.lblFoundDBValue.Weight = 1.8542857142857143;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblEmployerFraudDB,
            this.lblEmployerFraudDBValue});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 1;
            // 
            // lblEmployerFraudDB
            // 
            this.lblEmployerFraudDB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblEmployerFraudDB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmployerFraudDB.Name = "lblEmployerFraudDB";
            this.lblEmployerFraudDB.StylePriority.UseBackColor = false;
            this.lblEmployerFraudDB.StylePriority.UseFont = false;
            this.lblEmployerFraudDB.Text = "ID No. Found on Employer Fraud Database";
            this.lblEmployerFraudDB.Weight = 1.1457142857142857;
            // 
            // lblEmployerFraudDBValue
            // 
            this.lblEmployerFraudDBValue.Name = "lblEmployerFraudDBValue";
            this.lblEmployerFraudDBValue.Text = "lblEmployerFraudDBValue";
            this.lblEmployerFraudDBValue.Weight = 1.8542857142857143;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblProrectiveReg,
            this.lblProrectiveRegValue});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 1;
            // 
            // lblProrectiveReg
            // 
            this.lblProrectiveReg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblProrectiveReg.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProrectiveReg.Name = "lblProrectiveReg";
            this.lblProrectiveReg.StylePriority.UseBackColor = false;
            this.lblProrectiveReg.StylePriority.UseFont = false;
            this.lblProrectiveReg.Text = "ID No. Found on Protective Register";
            this.lblProrectiveReg.Weight = 1.1457142857142857;
            // 
            // lblProrectiveRegValue
            // 
            this.lblProrectiveRegValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerFraudIndicatorsSummary.ProtectiveVerificationYN", "")});
            this.lblProrectiveRegValue.Name = "lblProrectiveRegValue";
            this.lblProrectiveRegValue.Text = "lblProrectiveRegValue";
            this.lblProrectiveRegValue.Weight = 1.8542857142857143;
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblFraudSummary});
            this.GroupHeader2.Height = 40;
            this.GroupHeader2.Name = "GroupHeader2";
            // 
            // tblFraudSummary
            // 
            this.tblFraudSummary.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblFraudSummary.Location = new System.Drawing.Point(8, 8);
            this.tblFraudSummary.Name = "tblFraudSummary";
            this.tblFraudSummary.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow14});
            this.tblFraudSummary.Size = new System.Drawing.Size(333, 25);
            this.tblFraudSummary.StylePriority.UseFont = false;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.Text = "Potential fraud Indicators Summary";
            this.xrTableCell1.Weight = 3;
            // 
            // Scoring
            // 
            this.Scoring.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail3,
            this.GroupHeader3});
            this.Scoring.DataMember = "ConsumerScoring";
            this.Scoring.Level = 2;
            this.Scoring.Name = "Scoring";
            // 
            // Detail3
            // 
            this.Detail3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblscrorevalues});
            this.Detail3.Height = 27;
            this.Detail3.Name = "Detail3";
            // 
            // tblscrorevalues
            // 
            this.tblscrorevalues.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tblscrorevalues.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblscrorevalues.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblscrorevalues.Location = new System.Drawing.Point(8, 0);
            this.tblscrorevalues.Name = "tblscrorevalues";
            this.tblscrorevalues.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow22});
            this.tblscrorevalues.Size = new System.Drawing.Size(700, 25);
            this.tblscrorevalues.StylePriority.UseBorderColor = false;
            this.tblscrorevalues.StylePriority.UseBorders = false;
            this.tblscrorevalues.StylePriority.UseFont = false;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblUniqueIdentifierValue,
            this.lblDateValue,
            this.lblScroreValue,
            this.lblExceptionValue});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 1;
            // 
            // lblUniqueIdentifierValue
            // 
            this.lblUniqueIdentifierValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerScoring.Unique_Identifier", "")});
            this.lblUniqueIdentifierValue.Name = "lblUniqueIdentifierValue";
            this.lblUniqueIdentifierValue.Text = "lblUniqueIdentifierValue";
            this.lblUniqueIdentifierValue.Weight = 0.6785714285714286;
            // 
            // lblDateValue
            // 
            this.lblDateValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerScoring.ScoreDate", "")});
            this.lblDateValue.Name = "lblDateValue";
            this.lblDateValue.Text = "lblDateValue";
            this.lblDateValue.Weight = 0.75571428571428567;
            // 
            // lblScroreValue
            // 
            this.lblScroreValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerScoring.FinalScore", "")});
            this.lblScroreValue.Name = "lblScroreValue";
            this.lblScroreValue.Text = "lblScroreValue";
            this.lblScroreValue.Weight = 0.78285714285714292;
            // 
            // lblExceptionValue
            // 
            this.lblExceptionValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerScoring.Exception_Code", "")});
            this.lblExceptionValue.Name = "lblExceptionValue";
            this.lblExceptionValue.Text = "lblExceptionValue";
            this.lblExceptionValue.Weight = 0.78285714285714292;
            // 
            // GroupHeader3
            // 
            this.GroupHeader3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblscoreColumns,
            this.tblScoreHeader});
            this.GroupHeader3.Height = 72;
            this.GroupHeader3.Name = "GroupHeader3";
            // 
            // tblscoreColumns
            // 
            this.tblscoreColumns.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tblscoreColumns.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.tblscoreColumns.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tblscoreColumns.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblscoreColumns.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblscoreColumns.Location = new System.Drawing.Point(8, 47);
            this.tblscoreColumns.Name = "tblscoreColumns";
            this.tblscoreColumns.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow21});
            this.tblscoreColumns.Size = new System.Drawing.Size(700, 25);
            this.tblscoreColumns.StylePriority.UseBackColor = false;
            this.tblscoreColumns.StylePriority.UseBorderColor = false;
            this.tblscoreColumns.StylePriority.UseBorders = false;
            this.tblscoreColumns.StylePriority.UseFont = false;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblUniqueIdentifier,
            this.lblDate,
            this.lblScrore,
            this.lblException});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 1;
            // 
            // lblUniqueIdentifier
            // 
            this.lblUniqueIdentifier.CanGrow = false;
            this.lblUniqueIdentifier.Name = "lblUniqueIdentifier";
            this.lblUniqueIdentifier.Text = "Unique Identifier";
            this.lblUniqueIdentifier.Weight = 0.67857142857142871;
            // 
            // lblDate
            // 
            this.lblDate.CanGrow = false;
            this.lblDate.Name = "lblDate";
            this.lblDate.Text = "Score Date";
            this.lblDate.Weight = 0.75571428571428567;
            // 
            // lblScrore
            // 
            this.lblScrore.CanGrow = false;
            this.lblScrore.Name = "lblScrore";
            this.lblScrore.Text = "Final Score";
            this.lblScrore.Weight = 0.78285714285714281;
            // 
            // lblException
            // 
            this.lblException.CanGrow = false;
            this.lblException.Name = "lblException";
            this.lblException.Text = "Exception Code";
            this.lblException.Weight = 0.78285714285714292;
            // 
            // tblScoreHeader
            // 
            this.tblScoreHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblScoreHeader.Location = new System.Drawing.Point(8, 8);
            this.tblScoreHeader.Name = "tblScoreHeader";
            this.tblScoreHeader.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow20});
            this.tblScoreHeader.Size = new System.Drawing.Size(333, 25);
            this.tblScoreHeader.StylePriority.UseFont = false;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 1;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.Text = "XDS Presage Collection Score";
            this.xrTableCell2.Weight = 3;
            // 
            // DebtSummary
            // 
            this.DebtSummary.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail4,
            this.GroupHeader4});
            this.DebtSummary.DataMember = "ConsumerDebtSummary";
            this.DebtSummary.Level = 3;
            this.DebtSummary.Name = "DebtSummary";
            // 
            // Detail4
            // 
            this.Detail4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblDebtSummary});
            this.Detail4.Height = 317;
            this.Detail4.Name = "Detail4";
            // 
            // tblDebtSummary
            // 
            this.tblDebtSummary.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tblDebtSummary.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblDebtSummary.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblDebtSummary.Location = new System.Drawing.Point(8, 8);
            this.tblDebtSummary.Name = "tblDebtSummary";
            this.tblDebtSummary.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow24,
            this.xrTableRow36,
            this.xrTableRow35,
            this.xrTableRow34,
            this.xrTableRow33,
            this.xrTableRow32,
            this.xrTableRow31,
            this.xrTableRow30,
            this.xrTableRow29,
            this.xrTableRow28,
            this.xrTableRow27,
            this.xrTableRow26});
            this.tblDebtSummary.Size = new System.Drawing.Size(700, 300);
            this.tblDebtSummary.StylePriority.UseBorderColor = false;
            this.tblDebtSummary.StylePriority.UseBorders = false;
            this.tblDebtSummary.StylePriority.UseFont = false;
            this.tblDebtSummary.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.tblDebtSummary_BeforePrint);
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblActiveAcc,
            this.lblActiveAccValue});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 1;
            // 
            // lblActiveAcc
            // 
            this.lblActiveAcc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblActiveAcc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblActiveAcc.Name = "lblActiveAcc";
            this.lblActiveAcc.StylePriority.UseBackColor = false;
            this.lblActiveAcc.StylePriority.UseFont = false;
            this.lblActiveAcc.Text = "Total No. Of Active Accounts";
            this.lblActiveAcc.Weight = 1.1071428571428572;
            // 
            // lblActiveAccValue
            // 
            this.lblActiveAccValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.NoOFActiveAccounts", "")});
            this.lblActiveAccValue.Name = "lblActiveAccValue";
            this.lblActiveAccValue.Weight = 1.8928571428571428;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblAccGood,
            this.lblAccGoodvalue});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 1;
            // 
            // lblAccGood
            // 
            this.lblAccGood.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblAccGood.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAccGood.Name = "lblAccGood";
            this.lblAccGood.StylePriority.UseBackColor = false;
            this.lblAccGood.StylePriority.UseFont = false;
            this.lblAccGood.Text = "Total No. Of Accounts In Good Standing";
            this.lblAccGood.Weight = 1.1071428571428572;
            // 
            // lblAccGoodvalue
            // 
            this.lblAccGoodvalue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.NoOfAccountInGoodStanding", "")});
            this.lblAccGoodvalue.Name = "lblAccGoodvalue";
            this.lblAccGoodvalue.Weight = 1.8928571428571428;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDelinquentAcc,
            this.lblDelinquentAccValue});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Weight = 1;
            // 
            // lblDelinquentAcc
            // 
            this.lblDelinquentAcc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblDelinquentAcc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDelinquentAcc.Name = "lblDelinquentAcc";
            this.lblDelinquentAcc.StylePriority.UseBackColor = false;
            this.lblDelinquentAcc.StylePriority.UseFont = false;
            this.lblDelinquentAcc.Text = "Total No. Of Delinquent Accounts";
            this.lblDelinquentAcc.Weight = 1.1071428571428572;
            // 
            // lblDelinquentAccValue
            // 
            this.lblDelinquentAccValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.NoOfAccountInBadStanding", "")});
            this.lblDelinquentAccValue.Name = "lblDelinquentAccValue";
            this.lblDelinquentAccValue.Weight = 1.8928571428571428;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblMonthlyinst,
            this.lblMonthlyinstValue});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 1;
            // 
            // lblMonthlyinst
            // 
            this.lblMonthlyinst.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblMonthlyinst.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMonthlyinst.Name = "lblMonthlyinst";
            this.lblMonthlyinst.StylePriority.UseBackColor = false;
            this.lblMonthlyinst.StylePriority.UseFont = false;
            this.lblMonthlyinst.Text = "Total Monthly Installments";
            this.lblMonthlyinst.Weight = 1.1071428571428572;
            // 
            // lblMonthlyinstValue
            // 
            this.lblMonthlyinstValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.TotalMonthlyInstallment", "")});
            this.lblMonthlyinstValue.Name = "lblMonthlyinstValue";
            this.lblMonthlyinstValue.Weight = 1.8928571428571428;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblOutstandingDebt,
            this.lblOutstandingDebtValue});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Weight = 1;
            // 
            // lblOutstandingDebt
            // 
            this.lblOutstandingDebt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblOutstandingDebt.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOutstandingDebt.Name = "lblOutstandingDebt";
            this.lblOutstandingDebt.StylePriority.UseBackColor = false;
            this.lblOutstandingDebt.StylePriority.UseFont = false;
            this.lblOutstandingDebt.Text = "Total Outstanding Debt";
            this.lblOutstandingDebt.Weight = 1.1071428571428572;
            // 
            // lblOutstandingDebtValue
            // 
            this.lblOutstandingDebtValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.TotalOutStandingDebt", "")});
            this.lblOutstandingDebtValue.Name = "lblOutstandingDebtValue";
            this.lblOutstandingDebtValue.Weight = 1.8928571428571428;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblArrearamt,
            this.lblArrearamtvalue});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Weight = 1;
            // 
            // lblArrearamt
            // 
            this.lblArrearamt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblArrearamt.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblArrearamt.Name = "lblArrearamt";
            this.lblArrearamt.StylePriority.UseBackColor = false;
            this.lblArrearamt.StylePriority.UseFont = false;
            this.lblArrearamt.Text = "Total Arrear Amount";
            this.lblArrearamt.Weight = 1.1071428571428572;
            // 
            // lblArrearamtvalue
            // 
            this.lblArrearamtvalue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.TotalArrearAmount", "")});
            this.lblArrearamtvalue.Name = "lblArrearamtvalue";
            this.lblArrearamtvalue.Weight = 1.8928571428571428;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblJdgDate,
            this.lblJdgDateValue});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 1;
            // 
            // lblJdgDate
            // 
            this.lblJdgDate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblJdgDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJdgDate.Name = "lblJdgDate";
            this.lblJdgDate.StylePriority.UseBackColor = false;
            this.lblJdgDate.StylePriority.UseFont = false;
            this.lblJdgDate.Text = "Most Recent Judgement Date";
            this.lblJdgDate.Weight = 1.1071428571428572;
            // 
            // lblJdgDateValue
            // 
            this.lblJdgDateValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.MostRecentJudgmentDate", "")});
            this.lblJdgDateValue.Name = "lblJdgDateValue";
            this.lblJdgDateValue.Weight = 1.8928571428571428;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblJudAmt,
            this.lblJudAmtValue});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Weight = 1;
            // 
            // lblJudAmt
            // 
            this.lblJudAmt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblJudAmt.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJudAmt.Name = "lblJudAmt";
            this.lblJudAmt.StylePriority.UseBackColor = false;
            this.lblJudAmt.StylePriority.UseFont = false;
            this.lblJudAmt.Text = "Highest Judgement Amount";
            this.lblJudAmt.Weight = 1.1071428571428572;
            // 
            // lblJudAmtValue
            // 
            this.lblJudAmtValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.HighestJudgmentAmount", "")});
            this.lblJudAmtValue.Name = "lblJudAmtValue";
            this.lblJudAmtValue.Weight = 1.8928571428571428;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDelinquentrating,
            this.lblDelinquentratingValue});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 1;
            // 
            // lblDelinquentrating
            // 
            this.lblDelinquentrating.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblDelinquentrating.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDelinquentrating.Name = "lblDelinquentrating";
            this.lblDelinquentrating.StylePriority.UseBackColor = false;
            this.lblDelinquentrating.StylePriority.UseFont = false;
            this.lblDelinquentrating.Text = "Highest Delinquent Rating (Last 24 Months)";
            this.lblDelinquentrating.Weight = 1.1071428571428572;
            // 
            // lblDelinquentratingValue
            // 
            this.lblDelinquentratingValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.HighestDelinquentRating", "")});
            this.lblDelinquentratingValue.Name = "lblDelinquentratingValue";
            this.lblDelinquentratingValue.Weight = 1.8928571428571428;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDbtReview,
            this.lblDbtReviewValue});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 1;
            // 
            // lblDbtReview
            // 
            this.lblDbtReview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblDbtReview.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDbtReview.Name = "lblDbtReview";
            this.lblDbtReview.StylePriority.UseBackColor = false;
            this.lblDbtReview.StylePriority.UseFont = false;
            this.lblDbtReview.Text = "Debt Review Status";
            this.lblDbtReview.Weight = 1.1071428571428572;
            // 
            // lblDbtReviewValue
            // 
            this.lblDbtReviewValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.DebtReviewStatus", "")});
            this.lblDbtReviewValue.Name = "lblDbtReviewValue";
            this.lblDbtReviewValue.Weight = 1.8928571428571428;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblAO,
            this.lblAOValue});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 1;
            // 
            // lblAO
            // 
            this.lblAO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblAO.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAO.Name = "lblAO";
            this.lblAO.StylePriority.UseBackColor = false;
            this.lblAO.StylePriority.UseFont = false;
            this.lblAO.Text = "Admin Order Status";
            this.lblAO.Weight = 1.1071428571428572;
            // 
            // lblAOValue
            // 
            this.lblAOValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.AdminOrderStatus", "")});
            this.lblAOValue.Name = "lblAOValue";
            this.lblAOValue.Weight = 1.8928571428571428;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblAdverseAmt,
            this.lblAdverseAmtValue});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 1;
            // 
            // lblAdverseAmt
            // 
            this.lblAdverseAmt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblAdverseAmt.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAdverseAmt.Name = "lblAdverseAmt";
            this.lblAdverseAmt.StylePriority.UseBackColor = false;
            this.lblAdverseAmt.StylePriority.UseFont = false;
            this.lblAdverseAmt.Text = "Total Adverse Amount";
            this.lblAdverseAmt.Weight = 1.1071428571428572;
            // 
            // lblAdverseAmtValue
            // 
            this.lblAdverseAmtValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtSummary.TotalAdverseAmount", "")});
            this.lblAdverseAmtValue.Name = "lblAdverseAmtValue";
            this.lblAdverseAmtValue.Weight = 1.8928571428571428;
            // 
            // GroupHeader4
            // 
            this.GroupHeader4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblDebtHeader});
            this.GroupHeader4.Height = 36;
            this.GroupHeader4.Name = "GroupHeader4";
            // 
            // tblDebtHeader
            // 
            this.tblDebtHeader.BackColor = System.Drawing.Color.Transparent;
            this.tblDebtHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblDebtHeader.Location = new System.Drawing.Point(8, 8);
            this.tblDebtHeader.Name = "tblDebtHeader";
            this.tblDebtHeader.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow23});
            this.tblDebtHeader.Size = new System.Drawing.Size(333, 25);
            this.tblDebtHeader.StylePriority.UseBackColor = false;
            this.tblDebtHeader.StylePriority.UseFont = false;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 1;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.Text = "Debt Summary";
            this.xrTableCell3.Weight = 3;
            // 
            // AccountGoodBadSummary
            // 
            this.AccountGoodBadSummary.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail5,
            this.GroupHeader5});
            this.AccountGoodBadSummary.DataMember = "ConsumerAccountGoodBadSummary";
            this.AccountGoodBadSummary.Level = 4;
            this.AccountGoodBadSummary.Name = "AccountGoodBadSummary";
            // 
            // Detail5
            // 
            this.Detail5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblAccStatus});
            this.Detail5.Height = 304;
            this.Detail5.Name = "Detail5";
            // 
            // tblAccStatus
            // 
            this.tblAccStatus.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tblAccStatus.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblAccStatus.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblAccStatus.Location = new System.Drawing.Point(8, 8);
            this.tblAccStatus.Name = "tblAccStatus";
            this.tblAccStatus.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow39,
            this.xrTableRow38,
            this.xrTableRow47,
            this.xrTableRow46,
            this.xrTableRow45,
            this.xrTableRow44,
            this.xrTableRow43,
            this.xrTableRow42,
            this.xrTableRow41,
            this.xrTableRow40,
            this.xrTableRow37});
            this.tblAccStatus.Size = new System.Drawing.Size(501, 275);
            this.tblAccStatus.StylePriority.UseBorderColor = false;
            this.tblAccStatus.StylePriority.UseBorders = false;
            this.tblAccStatus.StylePriority.UseFont = false;
            this.tblAccStatus.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.tblAccStatus_BeforePrint);
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblAccdesc,
            this.lblGood,
            this.lblBad});
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Weight = 1;
            // 
            // lblAccdesc
            // 
            this.lblAccdesc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblAccdesc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAccdesc.Name = "lblAccdesc";
            this.lblAccdesc.StylePriority.UseBackColor = false;
            this.lblAccdesc.StylePriority.UseFont = false;
            this.lblAccdesc.Text = "Account Description";
            this.lblAccdesc.Weight = 1.6414473684210527;
            // 
            // lblGood
            // 
            this.lblGood.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGood.Name = "lblGood";
            this.lblGood.StylePriority.UseFont = false;
            this.lblGood.Text = "Good";
            this.lblGood.Weight = 0.41282894736842096;
            // 
            // lblBad
            // 
            this.lblBad.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBad.Name = "lblBad";
            this.lblBad.StylePriority.UseFont = false;
            this.lblBad.Text = "Bad";
            this.lblBad.Weight = 0.41776315789473678;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblRetail,
            this.lblRetailG,
            this.lblRetailB});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Weight = 1;
            // 
            // lblRetail
            // 
            this.lblRetail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblRetail.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRetail.Name = "lblRetail";
            this.lblRetail.StylePriority.UseBackColor = false;
            this.lblRetail.StylePriority.UseFont = false;
            this.lblRetail.Text = "Total no. Of Active Retail Accounts";
            this.lblRetail.Weight = 1.6414473684210527;
            // 
            // lblRetailG
            // 
            this.lblRetailG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfRetailAccountsGood", "")});
            this.lblRetailG.Name = "lblRetailG";
            this.lblRetailG.StylePriority.UseForeColor = false;
            this.lblRetailG.Weight = 0.41282894736842096;
            // 
            // lblRetailB
            // 
            this.lblRetailB.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfRetailAccountsBad", "")});
            this.lblRetailB.Name = "lblRetailB";
            this.lblRetailB.Weight = 0.41776315789473678;
            // 
            // xrTableRow47
            // 
            this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCc,
            this.lblCreditCradG,
            this.lblCreditCardB});
            this.xrTableRow47.Name = "xrTableRow47";
            this.xrTableRow47.Weight = 1;
            // 
            // lblCc
            // 
            this.lblCc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblCc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCc.Name = "lblCc";
            this.lblCc.StylePriority.UseBackColor = false;
            this.lblCc.StylePriority.UseFont = false;
            this.lblCc.Text = "Total no. Of Active Credit card Accounts";
            this.lblCc.Weight = 1.6414473684210527;
            // 
            // lblCreditCradG
            // 
            this.lblCreditCradG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfCreditCardAccountsGood", "")});
            this.lblCreditCradG.Name = "lblCreditCradG";
            this.lblCreditCradG.Weight = 0.41282894736842096;
            // 
            // lblCreditCardB
            // 
            this.lblCreditCardB.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfCreditCardAccountsBad", "")});
            this.lblCreditCardB.Name = "lblCreditCardB";
            this.lblCreditCardB.Weight = 0.41776315789473678;
            // 
            // xrTableRow46
            // 
            this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblFurnAcc,
            this.lblFurnAccG,
            this.lblFurnAccB});
            this.xrTableRow46.Name = "xrTableRow46";
            this.xrTableRow46.Weight = 1;
            // 
            // lblFurnAcc
            // 
            this.lblFurnAcc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblFurnAcc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFurnAcc.Name = "lblFurnAcc";
            this.lblFurnAcc.StylePriority.UseBackColor = false;
            this.lblFurnAcc.StylePriority.UseFont = false;
            this.lblFurnAcc.Text = "Total no. Of Active Furniture Store Accounts";
            this.lblFurnAcc.Weight = 1.6414473684210527;
            // 
            // lblFurnAccG
            // 
            this.lblFurnAccG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfFurnitureAccountsGood", "")});
            this.lblFurnAccG.Name = "lblFurnAccG";
            this.lblFurnAccG.Weight = 0.41282894736842096;
            // 
            // lblFurnAccB
            // 
            this.lblFurnAccB.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfFurnitureAccountsBad", "")});
            this.lblFurnAccB.Name = "lblFurnAccB";
            this.lblFurnAccB.Weight = 0.41776315789473678;
            // 
            // xrTableRow45
            // 
            this.xrTableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblInsurance,
            this.lblInsuranceG,
            this.lblInsuranceB});
            this.xrTableRow45.Name = "xrTableRow45";
            this.xrTableRow45.Weight = 1;
            // 
            // lblInsurance
            // 
            this.lblInsurance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblInsurance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInsurance.Name = "lblInsurance";
            this.lblInsurance.StylePriority.UseBackColor = false;
            this.lblInsurance.StylePriority.UseFont = false;
            this.lblInsurance.Text = "Total no. Of Active Insurance Accounts";
            this.lblInsurance.Weight = 1.6414473684210527;
            // 
            // lblInsuranceG
            // 
            this.lblInsuranceG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfInsuranceAccountsGood", "")});
            this.lblInsuranceG.Name = "lblInsuranceG";
            this.lblInsuranceG.Weight = 0.41282894736842096;
            // 
            // lblInsuranceB
            // 
            this.lblInsuranceB.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfInsuranceAccountsBad", "")});
            this.lblInsuranceB.Name = "lblInsuranceB";
            this.lblInsuranceB.Weight = 0.41776315789473678;
            // 
            // xrTableRow44
            // 
            this.xrTableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblFinance,
            this.lblFinanceG,
            this.lblFinanceB});
            this.xrTableRow44.Name = "xrTableRow44";
            this.xrTableRow44.Weight = 1;
            // 
            // lblFinance
            // 
            this.lblFinance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblFinance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFinance.Name = "lblFinance";
            this.lblFinance.StylePriority.UseBackColor = false;
            this.lblFinance.StylePriority.UseFont = false;
            this.lblFinance.Text = "Total no. Of Active Personal Finance Accounts";
            this.lblFinance.Weight = 1.6414473684210527;
            // 
            // lblFinanceG
            // 
            this.lblFinanceG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfPersonalFinAccountsGood", "")});
            this.lblFinanceG.Name = "lblFinanceG";
            this.lblFinanceG.Weight = 0.41282894736842096;
            // 
            // lblFinanceB
            // 
            this.lblFinanceB.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfPersonalFinAccountsBad", "")});
            this.lblFinanceB.Name = "lblFinanceB";
            this.lblFinanceB.Weight = 0.41776315789473678;
            // 
            // xrTableRow43
            // 
            this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblBankAcc,
            this.lblBankAccG,
            this.lblBankAccB});
            this.xrTableRow43.Name = "xrTableRow43";
            this.xrTableRow43.Weight = 1;
            // 
            // lblBankAcc
            // 
            this.lblBankAcc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblBankAcc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBankAcc.Name = "lblBankAcc";
            this.lblBankAcc.StylePriority.UseBackColor = false;
            this.lblBankAcc.StylePriority.UseFont = false;
            this.lblBankAcc.Text = "Total no. Of Active Banl Accounts";
            this.lblBankAcc.Weight = 1.6414473684210527;
            // 
            // lblBankAccG
            // 
            this.lblBankAccG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfBankAccountsGood", "")});
            this.lblBankAccG.Name = "lblBankAccG";
            this.lblBankAccG.Weight = 0.41282894736842096;
            // 
            // lblBankAccB
            // 
            this.lblBankAccB.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfBankAccountsBad", "")});
            this.lblBankAccB.Name = "lblBankAccB";
            this.lblBankAccB.Weight = 0.41776315789473678;
            // 
            // xrTableRow42
            // 
            this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblTelecomms,
            this.lblTelecommsG,
            this.lblTelecommsB});
            this.xrTableRow42.Name = "xrTableRow42";
            this.xrTableRow42.Weight = 1;
            // 
            // lblTelecomms
            // 
            this.lblTelecomms.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblTelecomms.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelecomms.Name = "lblTelecomms";
            this.lblTelecomms.StylePriority.UseBackColor = false;
            this.lblTelecomms.StylePriority.UseFont = false;
            this.lblTelecomms.Text = "Total no. Of Active Telecomms Accounts";
            this.lblTelecomms.Weight = 1.6414473684210527;
            // 
            // lblTelecommsG
            // 
            this.lblTelecommsG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfTelecomAccountsGood", "")});
            this.lblTelecommsG.Name = "lblTelecommsG";
            this.lblTelecommsG.Weight = 0.41282894736842096;
            // 
            // lblTelecommsB
            // 
            this.lblTelecommsB.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfTelecomAccountsBad", "")});
            this.lblTelecommsB.Name = "lblTelecommsB";
            this.lblTelecommsB.Weight = 0.41776315789473678;
            // 
            // xrTableRow41
            // 
            this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblHomeLoan,
            this.lblHomeLoanG,
            this.lblHomeLoanB});
            this.xrTableRow41.Name = "xrTableRow41";
            this.xrTableRow41.Weight = 1;
            // 
            // lblHomeLoan
            // 
            this.lblHomeLoan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblHomeLoan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHomeLoan.Name = "lblHomeLoan";
            this.lblHomeLoan.StylePriority.UseBackColor = false;
            this.lblHomeLoan.StylePriority.UseFont = false;
            this.lblHomeLoan.Text = "Total no. Of Active Home Loan Accounts";
            this.lblHomeLoan.Weight = 1.6414473684210527;
            // 
            // lblHomeLoanG
            // 
            this.lblHomeLoanG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfHomeLoanAccountsGood", "")});
            this.lblHomeLoanG.Name = "lblHomeLoanG";
            this.lblHomeLoanG.Weight = 0.41282894736842096;
            // 
            // lblHomeLoanB
            // 
            this.lblHomeLoanB.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfHomeLoanAccountsBad", "")});
            this.lblHomeLoanB.Name = "lblHomeLoanB";
            this.lblHomeLoanB.Weight = 0.41776315789473678;
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblVehicleFin,
            this.lblVehicleFinG,
            this.lblVehicleFinB});
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.Weight = 1;
            // 
            // lblVehicleFin
            // 
            this.lblVehicleFin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblVehicleFin.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVehicleFin.Name = "lblVehicleFin";
            this.lblVehicleFin.StylePriority.UseBackColor = false;
            this.lblVehicleFin.StylePriority.UseFont = false;
            this.lblVehicleFin.Text = "Total no. Of Active Motor Vehicle Finance Accounts";
            this.lblVehicleFin.Weight = 1.6414473684210527;
            // 
            // lblVehicleFinG
            // 
            this.lblVehicleFinG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfMotorFinanceAccountsGood", "")});
            this.lblVehicleFinG.Name = "lblVehicleFinG";
            this.lblVehicleFinG.Weight = 0.41282894736842096;
            // 
            // lblVehicleFinB
            // 
            this.lblVehicleFinB.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfMotorFinanceAccountsBad", "")});
            this.lblVehicleFinB.Name = "lblVehicleFinB";
            this.lblVehicleFinB.Weight = 0.41776315789473678;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblOtherAcc,
            this.lblOtherAccG,
            this.lblOtherAccB});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Weight = 1;
            // 
            // lblOtherAcc
            // 
            this.lblOtherAcc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.lblOtherAcc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOtherAcc.Name = "lblOtherAcc";
            this.lblOtherAcc.StylePriority.UseBackColor = false;
            this.lblOtherAcc.StylePriority.UseFont = false;
            this.lblOtherAcc.Text = "Total no. Of Active Other Accounts";
            this.lblOtherAcc.Weight = 1.6414473684210527;
            // 
            // lblOtherAccG
            // 
            this.lblOtherAccG.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfOtherAccountsGood", "")});
            this.lblOtherAccG.Name = "lblOtherAccG";
            this.lblOtherAccG.Weight = 0.41282894736842096;
            // 
            // lblOtherAccB
            // 
            this.lblOtherAccB.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountGoodBadSummary.NoOfOtherAccountsBad", "")});
            this.lblOtherAccB.Name = "lblOtherAccB";
            this.lblOtherAccB.Weight = 0.41776315789473678;
            // 
            // GroupHeader5
            // 
            this.GroupHeader5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblCrAccStatus});
            this.GroupHeader5.Height = 34;
            this.GroupHeader5.Name = "GroupHeader5";
            // 
            // tblCrAccStatus
            // 
            this.tblCrAccStatus.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblCrAccStatus.Location = new System.Drawing.Point(8, 8);
            this.tblCrAccStatus.Name = "tblCrAccStatus";
            this.tblCrAccStatus.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow25});
            this.tblCrAccStatus.Size = new System.Drawing.Size(333, 25);
            this.tblCrAccStatus.StylePriority.UseFont = false;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 1;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.Text = "Credit Account Status Summary";
            this.xrTableCell4.Weight = 3;
            // 
            // NameConfirmation
            // 
            this.NameConfirmation.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail6,
            this.GroupHeader6});
            this.NameConfirmation.DataMember = "ConsumerDetailConfirmationName";
            this.NameConfirmation.Level = 5;
            this.NameConfirmation.Name = "NameConfirmation";
            // 
            // Detail6
            // 
            this.Detail6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Detail6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblNameHistoryValues});
            this.Detail6.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.Detail6.Height = 25;
            this.Detail6.Name = "Detail6";
            this.Detail6.StylePriority.UseBorders = false;
            this.Detail6.StylePriority.UseForeColor = false;
            // 
            // tblNameHistoryValues
            // 
            this.tblNameHistoryValues.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tblNameHistoryValues.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblNameHistoryValues.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tblNameHistoryValues.Location = new System.Drawing.Point(8, 0);
            this.tblNameHistoryValues.Name = "tblNameHistoryValues";
            this.tblNameHistoryValues.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow52});
            this.tblNameHistoryValues.Size = new System.Drawing.Size(700, 25);
            this.tblNameHistoryValues.StylePriority.UseBorderColor = false;
            this.tblNameHistoryValues.StylePriority.UseFont = false;
            this.tblNameHistoryValues.StylePriority.UseForeColor = false;
            // 
            // xrTableRow52
            // 
            this.xrTableRow52.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblNameValue,
            this.lblNHSurNameValue,
            this.lblNHIDNoValue,
            this.lblNHdatevalue});
            this.xrTableRow52.Name = "xrTableRow52";
            this.xrTableRow52.Weight = 1;
            // 
            // lblNameValue
            // 
            this.lblNameValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationName.Name", "")});
            this.lblNameValue.Name = "lblNameValue";
            this.lblNameValue.Weight = 1;
            // 
            // lblNHSurNameValue
            // 
            this.lblNHSurNameValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationName.Surname", "")});
            this.lblNHSurNameValue.Name = "lblNHSurNameValue";
            this.lblNHSurNameValue.Weight = 1;
            // 
            // lblNHIDNoValue
            // 
            this.lblNHIDNoValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationName.IDno", "")});
            this.lblNHIDNoValue.Name = "lblNHIDNoValue";
            this.lblNHIDNoValue.Weight = 0.5;
            // 
            // lblNHdatevalue
            // 
            this.lblNHdatevalue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationName.DateConfirmed", "")});
            this.lblNHdatevalue.Name = "lblNHdatevalue";
            this.lblNHdatevalue.Weight = 0.5;
            // 
            // GroupHeader6
            // 
            this.GroupHeader6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblNameHistColumns,
            this.tblNameHistoryHeader,
            this.tblpersonalConfirmationHeader,
            this.tblDetailedInfoHeader});
            this.GroupHeader6.Height = 158;
            this.GroupHeader6.Name = "GroupHeader6";
            // 
            // tblNameHistColumns
            // 
            this.tblNameHistColumns.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tblNameHistColumns.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.tblNameHistColumns.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tblNameHistColumns.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblNameHistColumns.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblNameHistColumns.Location = new System.Drawing.Point(8, 133);
            this.tblNameHistColumns.Name = "tblNameHistColumns";
            this.tblNameHistColumns.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow51});
            this.tblNameHistColumns.Size = new System.Drawing.Size(700, 25);
            this.tblNameHistColumns.StylePriority.UseBackColor = false;
            this.tblNameHistColumns.StylePriority.UseBorderColor = false;
            this.tblNameHistColumns.StylePriority.UseBorders = false;
            this.tblNameHistColumns.StylePriority.UseFont = false;
            // 
            // xrTableRow51
            // 
            this.xrTableRow51.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblName,
            this.lblNHSurName,
            this.lblNHIDNo,
            this.lblNHdate});
            this.xrTableRow51.Name = "xrTableRow51";
            this.xrTableRow51.Weight = 1;
            // 
            // lblName
            // 
            this.lblName.CanGrow = false;
            this.lblName.Name = "lblName";
            this.lblName.Text = "Name";
            this.lblName.Weight = 1;
            // 
            // lblNHSurName
            // 
            this.lblNHSurName.CanGrow = false;
            this.lblNHSurName.Name = "lblNHSurName";
            this.lblNHSurName.Text = "SurName";
            this.lblNHSurName.Weight = 1;
            // 
            // lblNHIDNo
            // 
            this.lblNHIDNo.CanGrow = false;
            this.lblNHIDNo.Name = "lblNHIDNo";
            this.lblNHIDNo.Text = "ID No";
            this.lblNHIDNo.Weight = 0.5;
            // 
            // lblNHdate
            // 
            this.lblNHdate.CanGrow = false;
            this.lblNHdate.Name = "lblNHdate";
            this.lblNHdate.Text = "Date Confirmed";
            this.lblNHdate.Weight = 0.5;
            // 
            // tblNameHistoryHeader
            // 
            this.tblNameHistoryHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblNameHistoryHeader.Location = new System.Drawing.Point(8, 92);
            this.tblNameHistoryHeader.Name = "tblNameHistoryHeader";
            this.tblNameHistoryHeader.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow50});
            this.tblNameHistoryHeader.Size = new System.Drawing.Size(375, 25);
            this.tblNameHistoryHeader.StylePriority.UseFont = false;
            // 
            // xrTableRow50
            // 
            this.xrTableRow50.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9});
            this.xrTableRow50.Name = "xrTableRow50";
            this.xrTableRow50.Weight = 1;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.Text = "Name History";
            this.xrTableCell9.Weight = 3;
            // 
            // tblpersonalConfirmationHeader
            // 
            this.tblpersonalConfirmationHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblpersonalConfirmationHeader.Location = new System.Drawing.Point(8, 50);
            this.tblpersonalConfirmationHeader.Name = "tblpersonalConfirmationHeader";
            this.tblpersonalConfirmationHeader.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow49});
            this.tblpersonalConfirmationHeader.Size = new System.Drawing.Size(375, 25);
            this.tblpersonalConfirmationHeader.StylePriority.UseFont = false;
            // 
            // xrTableRow49
            // 
            this.xrTableRow49.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8});
            this.xrTableRow49.Name = "xrTableRow49";
            this.xrTableRow49.Weight = 1;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.Text = "Personal Details Confirmation Table";
            this.xrTableCell8.Weight = 3;
            // 
            // tblDetailedInfoHeader
            // 
            this.tblDetailedInfoHeader.Font = new System.Drawing.Font("Book Antiqua", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblDetailedInfoHeader.Location = new System.Drawing.Point(8, 8);
            this.tblDetailedInfoHeader.Name = "tblDetailedInfoHeader";
            this.tblDetailedInfoHeader.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow48});
            this.tblDetailedInfoHeader.Size = new System.Drawing.Size(500, 25);
            this.tblDetailedInfoHeader.StylePriority.UseFont = false;
            this.tblDetailedInfoHeader.StylePriority.UseTextAlignment = false;
            this.tblDetailedInfoHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow48
            // 
            this.xrTableRow48.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5});
            this.xrTableRow48.Name = "xrTableRow48";
            this.xrTableRow48.Weight = 1;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Underline);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.Text = "DETAILED INFORMATION";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell5.Weight = 3;
            // 
            // AddressConfirmation
            // 
            this.AddressConfirmation.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail7,
            this.GroupHeader7});
            this.AddressConfirmation.DataMember = "ConsumerDetailConfirmationAddress";
            this.AddressConfirmation.Level = 6;
            this.AddressConfirmation.Name = "AddressConfirmation";
            // 
            // Detail7
            // 
            this.Detail7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.Detail7.Height = 25;
            this.Detail7.Name = "Detail7";
            // 
            // xrTable3
            // 
            this.xrTable3.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.Location = new System.Drawing.Point(8, 0);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow55});
            this.xrTable3.Size = new System.Drawing.Size(700, 25);
            this.xrTable3.StylePriority.UseBorderColor = false;
            this.xrTable3.StylePriority.UseBorders = false;
            // 
            // xrTableRow55
            // 
            this.xrTableRow55.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCDAddType,
            this.lblCDAddLine1,
            this.lblCDAddLine2,
            this.lblCDAddLine3,
            this.lblCDAddLine4,
            this.lblCDdate});
            this.xrTableRow55.Name = "xrTableRow55";
            this.xrTableRow55.Weight = 1;
            // 
            // lblCDAddType
            // 
            this.lblCDAddType.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationAddress.AddressType", "")});
            this.lblCDAddType.Name = "lblCDAddType";
            this.lblCDAddType.Weight = 0.2857142857142857;
            // 
            // lblCDAddLine1
            // 
            this.lblCDAddLine1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationAddress.Address1", "")});
            this.lblCDAddLine1.Name = "lblCDAddLine1";
            this.lblCDAddLine1.Weight = 0.96714285714285708;
            // 
            // lblCDAddLine2
            // 
            this.lblCDAddLine2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationAddress.Address2", "")});
            this.lblCDAddLine2.Name = "lblCDAddLine2";
            this.lblCDAddLine2.Weight = 0.35000000000000003;
            // 
            // lblCDAddLine3
            // 
            this.lblCDAddLine3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationAddress.Address3", "")});
            this.lblCDAddLine3.Name = "lblCDAddLine3";
            this.lblCDAddLine3.Weight = 0.50428571428571423;
            // 
            // lblCDAddLine4
            // 
            this.lblCDAddLine4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationAddress.Address4", "")});
            this.lblCDAddLine4.Name = "lblCDAddLine4";
            this.lblCDAddLine4.Weight = 0.46142857142857141;
            // 
            // lblCDdate
            // 
            this.lblCDdate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationAddress.DateConfirmed", "")});
            this.lblCDdate.Name = "lblCDdate";
            this.lblCDdate.Weight = 0.43142857142857144;
            // 
            // GroupHeader7
            // 
            this.GroupHeader7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblConfAddHistoryC,
            this.tblAddHistoryHeader});
            this.GroupHeader7.Height = 77;
            this.GroupHeader7.Name = "GroupHeader7";
            // 
            // tblConfAddHistoryC
            // 
            this.tblConfAddHistoryC.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tblConfAddHistoryC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.tblConfAddHistoryC.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tblConfAddHistoryC.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblConfAddHistoryC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblConfAddHistoryC.Location = new System.Drawing.Point(8, 52);
            this.tblConfAddHistoryC.Name = "tblConfAddHistoryC";
            this.tblConfAddHistoryC.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow54});
            this.tblConfAddHistoryC.Size = new System.Drawing.Size(700, 25);
            this.tblConfAddHistoryC.StylePriority.UseBackColor = false;
            this.tblConfAddHistoryC.StylePriority.UseBorderColor = false;
            this.tblConfAddHistoryC.StylePriority.UseBorders = false;
            this.tblConfAddHistoryC.StylePriority.UseFont = false;
            // 
            // xrTableRow54
            // 
            this.xrTableRow54.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCAddType,
            this.lblCAddLine1,
            this.lblCAddLine2,
            this.lblCAddLine3,
            this.lblCAddLine4,
            this.lblCdate});
            this.xrTableRow54.Name = "xrTableRow54";
            this.xrTableRow54.Weight = 1;
            // 
            // lblCAddType
            // 
            this.lblCAddType.CanGrow = false;
            this.lblCAddType.Name = "lblCAddType";
            this.lblCAddType.Text = "Type";
            this.lblCAddType.Weight = 0.2857142857142857;
            // 
            // lblCAddLine1
            // 
            this.lblCAddLine1.CanGrow = false;
            this.lblCAddLine1.Name = "lblCAddLine1";
            this.lblCAddLine1.Text = "Line 1";
            this.lblCAddLine1.Weight = 0.96714285714285708;
            // 
            // lblCAddLine2
            // 
            this.lblCAddLine2.CanGrow = false;
            this.lblCAddLine2.Name = "lblCAddLine2";
            this.lblCAddLine2.Text = "Line 2";
            this.lblCAddLine2.Weight = 0.35000000000000003;
            // 
            // lblCAddLine3
            // 
            this.lblCAddLine3.CanGrow = false;
            this.lblCAddLine3.Name = "lblCAddLine3";
            this.lblCAddLine3.Text = "Line 3";
            this.lblCAddLine3.Weight = 0.50428571428571423;
            // 
            // lblCAddLine4
            // 
            this.lblCAddLine4.CanGrow = false;
            this.lblCAddLine4.Name = "lblCAddLine4";
            this.lblCAddLine4.Text = "Line 4";
            this.lblCAddLine4.Weight = 0.46142857142857147;
            // 
            // lblCdate
            // 
            this.lblCdate.CanGrow = false;
            this.lblCdate.Name = "lblCdate";
            this.lblCdate.Text = "Date Confirmed";
            this.lblCdate.Weight = 0.43142857142857144;
            // 
            // tblAddHistoryHeader
            // 
            this.tblAddHistoryHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblAddHistoryHeader.Location = new System.Drawing.Point(8, 8);
            this.tblAddHistoryHeader.Name = "tblAddHistoryHeader";
            this.tblAddHistoryHeader.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow53});
            this.tblAddHistoryHeader.Size = new System.Drawing.Size(375, 25);
            this.tblAddHistoryHeader.StylePriority.UseFont = false;
            // 
            // xrTableRow53
            // 
            this.xrTableRow53.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10});
            this.xrTableRow53.Name = "xrTableRow53";
            this.xrTableRow53.Weight = 1;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StylePriority.UseFont = false;
            this.xrTableCell10.Text = "Address History";
            this.xrTableCell10.Weight = 3;
            // 
            // ContactNoConfirmation
            // 
            this.ContactNoConfirmation.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail8,
            this.GroupHeader8});
            this.ContactNoConfirmation.DataMember = "ConsumerDetailConfirmationContact";
            this.ContactNoConfirmation.Level = 7;
            this.ContactNoConfirmation.Name = "ContactNoConfirmation";
            // 
            // Detail8
            // 
            this.Detail8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblCContactNoD});
            this.Detail8.Height = 25;
            this.Detail8.Name = "Detail8";
            // 
            // tblCContactNoD
            // 
            this.tblCContactNoD.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tblCContactNoD.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblCContactNoD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblCContactNoD.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tblCContactNoD.Location = new System.Drawing.Point(8, 0);
            this.tblCContactNoD.Name = "tblCContactNoD";
            this.tblCContactNoD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow58});
            this.tblCContactNoD.Size = new System.Drawing.Size(700, 25);
            this.tblCContactNoD.StylePriority.UseBorderColor = false;
            this.tblCContactNoD.StylePriority.UseBorders = false;
            this.tblCContactNoD.StylePriority.UseFont = false;
            this.tblCContactNoD.StylePriority.UseForeColor = false;
            // 
            // xrTableRow58
            // 
            this.xrTableRow58.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCLLNoD,
            this.lblCConfirmedLLD,
            this.lblCCellNoD,
            this.lblCConfirmedCellNoD});
            this.xrTableRow58.Name = "xrTableRow58";
            this.xrTableRow58.Weight = 1;
            // 
            // lblCLLNoD
            // 
            this.lblCLLNoD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationContact.LandLineNumber", "")});
            this.lblCLLNoD.Name = "lblCLLNoD";
            this.lblCLLNoD.Text = "lblCLLNoD";
            this.lblCLLNoD.Weight = 1;
            // 
            // lblCConfirmedLLD
            // 
            this.lblCConfirmedLLD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationContact.LandLineNumberConfirmDate", "")});
            this.lblCConfirmedLLD.Name = "lblCConfirmedLLD";
            this.lblCConfirmedLLD.Weight = 0.75571428571428578;
            // 
            // lblCCellNoD
            // 
            this.lblCCellNoD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationContact.CellNumber", "")});
            this.lblCCellNoD.Name = "lblCCellNoD";
            this.lblCCellNoD.Text = "lblCCellNoD";
            this.lblCCellNoD.Weight = 0.74428571428571422;
            // 
            // lblCConfirmedCellNoD
            // 
            this.lblCConfirmedCellNoD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationContact.CellNoConfirmDate", "")});
            this.lblCConfirmedCellNoD.Name = "lblCConfirmedCellNoD";
            this.lblCConfirmedCellNoD.Weight = 0.5;
            // 
            // GroupHeader8
            // 
            this.GroupHeader8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblCContactNoC,
            this.tblCContactNoH});
            this.GroupHeader8.Height = 76;
            this.GroupHeader8.Name = "GroupHeader8";
            // 
            // tblCContactNoC
            // 
            this.tblCContactNoC.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tblCContactNoC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.tblCContactNoC.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tblCContactNoC.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblCContactNoC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblCContactNoC.Location = new System.Drawing.Point(8, 51);
            this.tblCContactNoC.Name = "tblCContactNoC";
            this.tblCContactNoC.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow57});
            this.tblCContactNoC.Size = new System.Drawing.Size(700, 25);
            this.tblCContactNoC.StylePriority.UseBackColor = false;
            this.tblCContactNoC.StylePriority.UseBorderColor = false;
            this.tblCContactNoC.StylePriority.UseBorders = false;
            this.tblCContactNoC.StylePriority.UseFont = false;
            // 
            // xrTableRow57
            // 
            this.xrTableRow57.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCLLNo,
            this.lblCConfirmedLL,
            this.lblCCellNo,
            this.lblCConfirmedCellNo});
            this.xrTableRow57.Name = "xrTableRow57";
            this.xrTableRow57.Weight = 1;
            // 
            // lblCLLNo
            // 
            this.lblCLLNo.CanGrow = false;
            this.lblCLLNo.Name = "lblCLLNo";
            this.lblCLLNo.Text = "Land Line Number";
            this.lblCLLNo.Weight = 1;
            // 
            // lblCConfirmedLL
            // 
            this.lblCConfirmedLL.CanGrow = false;
            this.lblCConfirmedLL.Name = "lblCConfirmedLL";
            this.lblCConfirmedLL.Text = "Date Confirmed";
            this.lblCConfirmedLL.Weight = 0.75571428571428578;
            // 
            // lblCCellNo
            // 
            this.lblCCellNo.CanGrow = false;
            this.lblCCellNo.Name = "lblCCellNo";
            this.lblCCellNo.Text = "Cell Number";
            this.lblCCellNo.Weight = 0.74428571428571422;
            // 
            // lblCConfirmedCellNo
            // 
            this.lblCConfirmedCellNo.CanGrow = false;
            this.lblCConfirmedCellNo.Name = "lblCConfirmedCellNo";
            this.lblCConfirmedCellNo.Text = "Date Confirmed";
            this.lblCConfirmedCellNo.Weight = 0.5;
            // 
            // tblCContactNoH
            // 
            this.tblCContactNoH.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblCContactNoH.Location = new System.Drawing.Point(8, 8);
            this.tblCContactNoH.Name = "tblCContactNoH";
            this.tblCContactNoH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow56});
            this.tblCContactNoH.Size = new System.Drawing.Size(375, 25);
            this.tblCContactNoH.StylePriority.UseFont = false;
            // 
            // xrTableRow56
            // 
            this.xrTableRow56.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell11});
            this.xrTableRow56.Name = "xrTableRow56";
            this.xrTableRow56.Weight = 1;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.StylePriority.UseFont = false;
            this.xrTableCell11.Text = "Contact No.History";
            this.xrTableCell11.Weight = 3;
            // 
            // EmploymentDetailConfirmation
            // 
            this.EmploymentDetailConfirmation.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail9,
            this.GroupHeader9});
            this.EmploymentDetailConfirmation.DataMember = "ConsumerDetailConfirmationEmloyment";
            this.EmploymentDetailConfirmation.Level = 8;
            this.EmploymentDetailConfirmation.Name = "EmploymentDetailConfirmation";
            // 
            // Detail9
            // 
            this.Detail9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblEmpHisD});
            this.Detail9.Height = 26;
            this.Detail9.Name = "Detail9";
            // 
            // tblEmpHisD
            // 
            this.tblEmpHisD.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tblEmpHisD.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblEmpHisD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblEmpHisD.Location = new System.Drawing.Point(8, 0);
            this.tblEmpHisD.Name = "tblEmpHisD";
            this.tblEmpHisD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow61});
            this.tblEmpHisD.Size = new System.Drawing.Size(700, 25);
            this.tblEmpHisD.StylePriority.UseBorderColor = false;
            this.tblEmpHisD.StylePriority.UseBorders = false;
            this.tblEmpHisD.StylePriority.UseFont = false;
            // 
            // xrTableRow61
            // 
            this.xrTableRow61.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCEmployerD,
            this.lblCDesignationD,
            this.lblCConfirmeddateD});
            this.xrTableRow61.Name = "xrTableRow61";
            this.xrTableRow61.Weight = 1;
            // 
            // lblCEmployerD
            // 
            this.lblCEmployerD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationEmloyment.EmployerDetail", "")});
            this.lblCEmployerD.Name = "lblCEmployerD";
            this.lblCEmployerD.Weight = 1;
            // 
            // lblCDesignationD
            // 
            this.lblCDesignationD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationEmloyment.Designation", "")});
            this.lblCDesignationD.Name = "lblCDesignationD";
            this.lblCDesignationD.Text = "lblCDesignationD";
            this.lblCDesignationD.Weight = 1.33;
            // 
            // lblCConfirmeddateD
            // 
            this.lblCConfirmeddateD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetailConfirmationEmloyment.DateConfirmed", "")});
            this.lblCConfirmeddateD.Name = "lblCConfirmeddateD";
            this.lblCConfirmeddateD.Weight = 0.67;
            // 
            // GroupHeader9
            // 
            this.GroupHeader9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblEmpHisC,
            this.tblEmploymentHisH});
            this.GroupHeader9.Height = 71;
            this.GroupHeader9.Name = "GroupHeader9";
            // 
            // tblEmpHisC
            // 
            this.tblEmpHisC.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tblEmpHisC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.tblEmpHisC.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tblEmpHisC.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblEmpHisC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblEmpHisC.Location = new System.Drawing.Point(8, 46);
            this.tblEmpHisC.Name = "tblEmpHisC";
            this.tblEmpHisC.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow60});
            this.tblEmpHisC.Size = new System.Drawing.Size(700, 25);
            this.tblEmpHisC.StylePriority.UseBackColor = false;
            this.tblEmpHisC.StylePriority.UseBorderColor = false;
            this.tblEmpHisC.StylePriority.UseBorders = false;
            this.tblEmpHisC.StylePriority.UseFont = false;
            // 
            // xrTableRow60
            // 
            this.xrTableRow60.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCEmployer,
            this.lblCDesignation,
            this.lblCConfirmeddate});
            this.xrTableRow60.Name = "xrTableRow60";
            this.xrTableRow60.Weight = 1;
            // 
            // lblCEmployer
            // 
            this.lblCEmployer.CanGrow = false;
            this.lblCEmployer.Name = "lblCEmployer";
            this.lblCEmployer.Text = "Employer";
            this.lblCEmployer.Weight = 1;
            // 
            // lblCDesignation
            // 
            this.lblCDesignation.CanGrow = false;
            this.lblCDesignation.Name = "lblCDesignation";
            this.lblCDesignation.Text = "Designation";
            this.lblCDesignation.Weight = 1.33;
            // 
            // lblCConfirmeddate
            // 
            this.lblCConfirmeddate.CanGrow = false;
            this.lblCConfirmeddate.Name = "lblCConfirmeddate";
            this.lblCConfirmeddate.Text = "Date Confirmed";
            this.lblCConfirmeddate.Weight = 0.67;
            // 
            // tblEmploymentHisH
            // 
            this.tblEmploymentHisH.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblEmploymentHisH.Location = new System.Drawing.Point(8, 8);
            this.tblEmploymentHisH.Name = "tblEmploymentHisH";
            this.tblEmploymentHisH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow59});
            this.tblEmploymentHisH.Size = new System.Drawing.Size(375, 25);
            this.tblEmploymentHisH.StylePriority.UseFont = false;
            // 
            // xrTableRow59
            // 
            this.xrTableRow59.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell26});
            this.xrTableRow59.Name = "xrTableRow59";
            this.xrTableRow59.Weight = 1;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.StylePriority.UseFont = false;
            this.xrTableCell26.Text = "Employment History";
            this.xrTableCell26.Weight = 3;
            // 
            // NameHistory
            // 
            this.NameHistory.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail10,
            this.GroupHeader10});
            this.NameHistory.DataMember = "ConsumerNameHistory";
            this.NameHistory.Level = 9;
            this.NameHistory.Name = "NameHistory";
            // 
            // Detail10
            // 
            this.Detail10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblNamehistD});
            this.Detail10.Height = 25;
            this.Detail10.Name = "Detail10";
            // 
            // tblNamehistD
            // 
            this.tblNamehistD.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblNamehistD.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblNamehistD.Location = new System.Drawing.Point(8, 0);
            this.tblNamehistD.Name = "tblNamehistD";
            this.tblNamehistD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow65});
            this.tblNamehistD.Size = new System.Drawing.Size(800, 25);
            this.tblNamehistD.StylePriority.UseBorderColor = false;
            this.tblNamehistD.StylePriority.UseBorders = false;
            // 
            // xrTableRow65
            // 
            this.xrTableRow65.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDUpdatedDate,
            this.lblHDSurname,
            this.lblHDFirstname,
            this.lblHDSecodnName,
            this.lblHDInitial,
            this.lblHDTitle,
            this.lblHDIDno,
            this.lblHDPPNo,
            this.lblHDBirthDate});
            this.xrTableRow65.Name = "xrTableRow65";
            this.xrTableRow65.Weight = 1;
            // 
            // lblDUpdatedDate
            // 
            this.lblDUpdatedDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.LastUpdatedDate", "")});
            this.lblDUpdatedDate.Name = "lblDUpdatedDate";
            this.lblDUpdatedDate.Weight = 0.25;
            // 
            // lblHDSurname
            // 
            this.lblHDSurname.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.Surname", "")});
            this.lblHDSurname.Name = "lblHDSurname";
            this.lblHDSurname.Weight = 0.4375;
            // 
            // lblHDFirstname
            // 
            this.lblHDFirstname.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.FirstName", "")});
            this.lblHDFirstname.Name = "lblHDFirstname";
            this.lblHDFirstname.Weight = 0.47;
            // 
            // lblHDSecodnName
            // 
            this.lblHDSecodnName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.SecondName", "")});
            this.lblHDSecodnName.Name = "lblHDSecodnName";
            this.lblHDSecodnName.Weight = 0.40375;
            // 
            // lblHDInitial
            // 
            this.lblHDInitial.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.Initials", "")});
            this.lblHDInitial.Name = "lblHDInitial";
            this.lblHDInitial.Weight = 0.18250000000000002;
            // 
            // lblHDTitle
            // 
            this.lblHDTitle.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.TitleDesc", "")});
            this.lblHDTitle.Name = "lblHDTitle";
            this.lblHDTitle.Weight = 0.25624999999999992;
            // 
            // lblHDIDno
            // 
            this.lblHDIDno.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.IDNo", "")});
            this.lblHDIDno.Name = "lblHDIDno";
            this.lblHDIDno.Weight = 0.37375;
            // 
            // lblHDPPNo
            // 
            this.lblHDPPNo.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.PassportNo", "")});
            this.lblHDPPNo.Name = "lblHDPPNo";
            this.lblHDPPNo.Weight = 0.31375;
            // 
            // lblHDBirthDate
            // 
            this.lblHDBirthDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNameHistory.BirthDate", "")});
            this.lblHDBirthDate.Name = "lblHDBirthDate";
            this.lblHDBirthDate.Text = "lblHDBirthDate";
            this.lblHDBirthDate.Weight = 0.3125;
            // 
            // GroupHeader10
            // 
            this.GroupHeader10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblNameHisC,
            this.tblNameHistH,
            this.tblConsInfoH});
            this.GroupHeader10.Height = 108;
            this.GroupHeader10.Name = "GroupHeader10";
            // 
            // tblNameHisC
            // 
            this.tblNameHisC.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblNameHisC.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblNameHisC.Location = new System.Drawing.Point(8, 83);
            this.tblNameHisC.Name = "tblNameHisC";
            this.tblNameHisC.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow64});
            this.tblNameHisC.Size = new System.Drawing.Size(800, 25);
            this.tblNameHisC.StylePriority.UseBorderColor = false;
            this.tblNameHisC.StylePriority.UseBorders = false;
            // 
            // xrTableRow64
            // 
            this.xrTableRow64.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblUpdatedDate,
            this.lblHSurname,
            this.lblHFirstname,
            this.lblHSecodnName,
            this.lblHInitial,
            this.lblHTitle,
            this.lblHIDno,
            this.lblHPPNo,
            this.lblHBirthDate});
            this.xrTableRow64.Name = "xrTableRow64";
            this.xrTableRow64.Weight = 1;
            // 
            // lblUpdatedDate
            // 
            this.lblUpdatedDate.Name = "lblUpdatedDate";
            this.lblUpdatedDate.Text = "Bureau Update";
            this.lblUpdatedDate.Weight = 0.25;
            // 
            // lblHSurname
            // 
            this.lblHSurname.Name = "lblHSurname";
            this.lblHSurname.Text = "Surname";
            this.lblHSurname.Weight = 0.4375;
            // 
            // lblHFirstname
            // 
            this.lblHFirstname.Name = "lblHFirstname";
            this.lblHFirstname.Text = "First Name";
            this.lblHFirstname.Weight = 0.47;
            // 
            // lblHSecodnName
            // 
            this.lblHSecodnName.Name = "lblHSecodnName";
            this.lblHSecodnName.Text = "Second Name";
            this.lblHSecodnName.Weight = 0.40375;
            // 
            // lblHInitial
            // 
            this.lblHInitial.Name = "lblHInitial";
            this.lblHInitial.Text = "Initial";
            this.lblHInitial.Weight = 0.18250000000000002;
            // 
            // lblHTitle
            // 
            this.lblHTitle.Name = "lblHTitle";
            this.lblHTitle.Text = "Title";
            this.lblHTitle.Weight = 0.25624999999999992;
            // 
            // lblHIDno
            // 
            this.lblHIDno.Name = "lblHIDno";
            this.lblHIDno.Text = "ID No";
            this.lblHIDno.Weight = 0.37375;
            // 
            // lblHPPNo
            // 
            this.lblHPPNo.Name = "lblHPPNo";
            this.lblHPPNo.Text = "Other ID No";
            this.lblHPPNo.Weight = 0.31375;
            // 
            // lblHBirthDate
            // 
            this.lblHBirthDate.Name = "lblHBirthDate";
            this.lblHBirthDate.Text = "Birth Date";
            this.lblHBirthDate.Weight = 0.3125;
            // 
            // tblNameHistH
            // 
            this.tblNameHistH.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblNameHistH.Location = new System.Drawing.Point(8, 50);
            this.tblNameHistH.Name = "tblNameHistH";
            this.tblNameHistH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow63});
            this.tblNameHistH.Size = new System.Drawing.Size(375, 25);
            this.tblNameHistH.StylePriority.UseFont = false;
            // 
            // xrTableRow63
            // 
            this.xrTableRow63.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell34});
            this.xrTableRow63.Name = "xrTableRow63";
            this.xrTableRow63.Weight = 1;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.StylePriority.UseFont = false;
            this.xrTableCell34.Text = "Name History";
            this.xrTableCell34.Weight = 3;
            // 
            // tblConsInfoH
            // 
            this.tblConsInfoH.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblConsInfoH.Location = new System.Drawing.Point(8, 0);
            this.tblConsInfoH.Name = "tblConsInfoH";
            this.tblConsInfoH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow62});
            this.tblConsInfoH.Size = new System.Drawing.Size(375, 25);
            this.tblConsInfoH.StylePriority.UseFont = false;
            // 
            // xrTableRow62
            // 
            this.xrTableRow62.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell33});
            this.xrTableRow62.Name = "xrTableRow62";
            this.xrTableRow62.Weight = 1;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.StylePriority.UseFont = false;
            this.xrTableCell33.Text = "Consumer Information";
            this.xrTableCell33.Weight = 3;
            // 
            // AddressHistory
            // 
            this.AddressHistory.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail11,
            this.GroupHeader11});
            this.AddressHistory.DataMember = "ConsumerAddressHistory";
            this.AddressHistory.Level = 10;
            this.AddressHistory.Name = "AddressHistory";
            // 
            // Detail11
            // 
            this.Detail11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblAddHD});
            this.Detail11.Height = 27;
            this.Detail11.Name = "Detail11";
            // 
            // tblAddHD
            // 
            this.tblAddHD.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblAddHD.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblAddHD.Location = new System.Drawing.Point(8, 0);
            this.tblAddHD.Name = "tblAddHD";
            this.tblAddHD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow68});
            this.tblAddHD.Size = new System.Drawing.Size(792, 25);
            this.tblAddHD.StylePriority.UseBorderColor = false;
            this.tblAddHD.StylePriority.UseBorders = false;
            // 
            // xrTableRow68
            // 
            this.xrTableRow68.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDAddUpdatedate,
            this.lblDAddtype,
            this.lblDAddline1,
            this.lblDAddLine2,
            this.lblDAddLine3,
            this.lblDAddLine4,
            this.lblDPostalcode});
            this.xrTableRow68.Name = "xrTableRow68";
            this.xrTableRow68.Weight = 1;
            // 
            // lblDAddUpdatedate
            // 
            this.lblDAddUpdatedate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.LastUpdatedDate", "")});
            this.lblDAddUpdatedate.Name = "lblDAddUpdatedate";
            this.lblDAddUpdatedate.Text = "lblDAddUpdatedate";
            this.lblDAddUpdatedate.Weight = 0.28409090909090906;
            // 
            // lblDAddtype
            // 
            this.lblDAddtype.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.AddressType", "")});
            this.lblDAddtype.Name = "lblDAddtype";
            this.lblDAddtype.Weight = 0.38257575757575762;
            // 
            // lblDAddline1
            // 
            this.lblDAddline1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.Address1", "")});
            this.lblDAddline1.Name = "lblDAddline1";
            this.lblDAddline1.Weight = 0.83333333333333326;
            // 
            // lblDAddLine2
            // 
            this.lblDAddLine2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.Address2", "")});
            this.lblDAddLine2.Name = "lblDAddLine2";
            this.lblDAddLine2.Weight = 0.42803030303030304;
            // 
            // lblDAddLine3
            // 
            this.lblDAddLine3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.Address3", "")});
            this.lblDAddLine3.Name = "lblDAddLine3";
            this.lblDAddLine3.Weight = 0.44318181818181823;
            // 
            // lblDAddLine4
            // 
            this.lblDAddLine4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.Address4", "")});
            this.lblDAddLine4.Name = "lblDAddLine4";
            this.lblDAddLine4.Weight = 0.37878787878787878;
            // 
            // lblDPostalcode
            // 
            this.lblDPostalcode.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAddressHistory.PostalCode", "")});
            this.lblDPostalcode.Name = "lblDPostalcode";
            this.lblDPostalcode.Weight = 0.25;
            // 
            // GroupHeader11
            // 
            this.GroupHeader11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblAddHC,
            this.tblAddHistoryH});
            this.GroupHeader11.Height = 68;
            this.GroupHeader11.Name = "GroupHeader11";
            // 
            // tblAddHC
            // 
            this.tblAddHC.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tblAddHC.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblAddHC.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblAddHC.Location = new System.Drawing.Point(8, 33);
            this.tblAddHC.Name = "tblAddHC";
            this.tblAddHC.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow67});
            this.tblAddHC.Size = new System.Drawing.Size(792, 34);
            this.tblAddHC.StylePriority.UseBorderColor = false;
            this.tblAddHC.StylePriority.UseBorders = false;
            // 
            // xrTableRow67
            // 
            this.xrTableRow67.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblAddUpdatedate,
            this.lblAddtype,
            this.lblAddline1,
            this.lblAddLine2,
            this.lblAddLine3,
            this.lblAddLine4,
            this.lblPostalcode});
            this.xrTableRow67.Name = "xrTableRow67";
            this.xrTableRow67.Weight = 1;
            // 
            // lblAddUpdatedate
            // 
            this.lblAddUpdatedate.CanGrow = false;
            this.lblAddUpdatedate.Name = "lblAddUpdatedate";
            this.lblAddUpdatedate.Text = "Bureau Update";
            this.lblAddUpdatedate.Weight = 0.28409090909090906;
            // 
            // lblAddtype
            // 
            this.lblAddtype.CanGrow = false;
            this.lblAddtype.Name = "lblAddtype";
            this.lblAddtype.Text = "Type";
            this.lblAddtype.Weight = 0.38257575757575757;
            // 
            // lblAddline1
            // 
            this.lblAddline1.CanGrow = false;
            this.lblAddline1.Name = "lblAddline1";
            this.lblAddline1.Text = "Line1";
            this.lblAddline1.Weight = 0.83333333333333326;
            // 
            // lblAddLine2
            // 
            this.lblAddLine2.CanGrow = false;
            this.lblAddLine2.Name = "lblAddLine2";
            this.lblAddLine2.Text = "Line2";
            this.lblAddLine2.Weight = 0.42803030303030304;
            // 
            // lblAddLine3
            // 
            this.lblAddLine3.CanGrow = false;
            this.lblAddLine3.Name = "lblAddLine3";
            this.lblAddLine3.Text = "Line 3";
            this.lblAddLine3.Weight = 0.44318181818181823;
            // 
            // lblAddLine4
            // 
            this.lblAddLine4.CanGrow = false;
            this.lblAddLine4.Name = "lblAddLine4";
            this.lblAddLine4.Text = "Line 4";
            this.lblAddLine4.Weight = 0.37878787878787878;
            // 
            // lblPostalcode
            // 
            this.lblPostalcode.CanGrow = false;
            this.lblPostalcode.Name = "lblPostalcode";
            this.lblPostalcode.Text = "Postal Code";
            this.lblPostalcode.Weight = 0.25;
            // 
            // tblAddHistoryH
            // 
            this.tblAddHistoryH.ForeColor = System.Drawing.Color.Black;
            this.tblAddHistoryH.Location = new System.Drawing.Point(8, 0);
            this.tblAddHistoryH.Name = "tblAddHistoryH";
            this.tblAddHistoryH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow66});
            this.tblAddHistoryH.Size = new System.Drawing.Size(433, 25);
            this.tblAddHistoryH.StylePriority.UseForeColor = false;
            // 
            // xrTableRow66
            // 
            this.xrTableRow66.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell188});
            this.xrTableRow66.Name = "xrTableRow66";
            this.xrTableRow66.Weight = 1;
            // 
            // xrTableCell188
            // 
            this.xrTableCell188.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell188.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell188.Name = "xrTableCell188";
            this.xrTableCell188.StylePriority.UseFont = false;
            this.xrTableCell188.StylePriority.UseForeColor = false;
            this.xrTableCell188.Text = "Address History";
            this.xrTableCell188.Weight = 3;
            // 
            // TelephoneHistory
            // 
            this.TelephoneHistory.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail12,
            this.GroupHeader12});
            this.TelephoneHistory.DataMember = "ConsumerTelephoneHistory";
            this.TelephoneHistory.Level = 11;
            this.TelephoneHistory.Name = "TelephoneHistory";
            // 
            // Detail12
            // 
            this.Detail12.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblContactNoD});
            this.Detail12.Height = 25;
            this.Detail12.Name = "Detail12";
            // 
            // tblContactNoD
            // 
            this.tblContactNoD.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblContactNoD.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblContactNoD.Location = new System.Drawing.Point(8, 0);
            this.tblContactNoD.Name = "tblContactNoD";
            this.tblContactNoD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow71});
            this.tblContactNoD.Size = new System.Drawing.Size(792, 25);
            this.tblContactNoD.StylePriority.UseBorderColor = false;
            this.tblContactNoD.StylePriority.UseBorders = false;
            // 
            // xrTableRow71
            // 
            this.xrTableRow71.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDNoUpdatedate,
            this.lblDTelnoType,
            this.lblDTelNo,
            this.lblDEmail});
            this.xrTableRow71.Name = "xrTableRow71";
            this.xrTableRow71.Weight = 1;
            // 
            // lblDNoUpdatedate
            // 
            this.lblDNoUpdatedate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneHistory.LastUpdatedDate", "")});
            this.lblDNoUpdatedate.Name = "lblDNoUpdatedate";
            this.lblDNoUpdatedate.Weight = 0.34848484848484851;
            // 
            // lblDTelnoType
            // 
            this.lblDTelnoType.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneHistory.TelephoneType", "")});
            this.lblDTelnoType.Name = "lblDTelnoType";
            this.lblDTelnoType.Text = "lblDTelnoType";
            this.lblDTelnoType.Weight = 0.21969696969696967;
            // 
            // lblDTelNo
            // 
            this.lblDTelNo.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneHistory.TelephoneNo", "")});
            this.lblDTelNo.Name = "lblDTelNo";
            this.lblDTelNo.Weight = 0.50378787878787867;
            // 
            // lblDEmail
            // 
            this.lblDEmail.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneHistory.EmailAddress", "")});
            this.lblDEmail.Name = "lblDEmail";
            this.lblDEmail.Weight = 1.928030303030303;
            // 
            // GroupHeader12
            // 
            this.GroupHeader12.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblContactNoC,
            this.tblContactNoH});
            this.GroupHeader12.Height = 71;
            this.GroupHeader12.Name = "GroupHeader12";
            // 
            // tblContactNoC
            // 
            this.tblContactNoC.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tblContactNoC.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblContactNoC.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblContactNoC.Location = new System.Drawing.Point(8, 46);
            this.tblContactNoC.Name = "tblContactNoC";
            this.tblContactNoC.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow70});
            this.tblContactNoC.Size = new System.Drawing.Size(792, 25);
            this.tblContactNoC.StylePriority.UseBorderColor = false;
            this.tblContactNoC.StylePriority.UseBorders = false;
            // 
            // xrTableRow70
            // 
            this.xrTableRow70.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblNoUpdatedate,
            this.lblTelnoType,
            this.lblTelNo,
            this.lblEmail});
            this.xrTableRow70.Name = "xrTableRow70";
            this.xrTableRow70.Weight = 1;
            // 
            // lblNoUpdatedate
            // 
            this.lblNoUpdatedate.CanGrow = false;
            this.lblNoUpdatedate.Name = "lblNoUpdatedate";
            this.lblNoUpdatedate.Text = "Bureau Update";
            this.lblNoUpdatedate.Weight = 0.34848484848484851;
            // 
            // lblTelnoType
            // 
            this.lblTelnoType.CanGrow = false;
            this.lblTelnoType.Name = "lblTelnoType";
            this.lblTelnoType.Text = "Type";
            this.lblTelnoType.Weight = 0.21969696969696967;
            // 
            // lblTelNo
            // 
            this.lblTelNo.CanGrow = false;
            this.lblTelNo.Name = "lblTelNo";
            this.lblTelNo.Text = "Telephone No";
            this.lblTelNo.Weight = 0.5037878787878789;
            // 
            // lblEmail
            // 
            this.lblEmail.CanGrow = false;
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Text = "Email Address";
            this.lblEmail.Weight = 1.928030303030303;
            // 
            // tblContactNoH
            // 
            this.tblContactNoH.ForeColor = System.Drawing.Color.Black;
            this.tblContactNoH.Location = new System.Drawing.Point(8, 8);
            this.tblContactNoH.Name = "tblContactNoH";
            this.tblContactNoH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow69});
            this.tblContactNoH.Size = new System.Drawing.Size(433, 25);
            this.tblContactNoH.StylePriority.UseForeColor = false;
            // 
            // xrTableRow69
            // 
            this.xrTableRow69.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell221});
            this.xrTableRow69.Name = "xrTableRow69";
            this.xrTableRow69.Weight = 1;
            // 
            // xrTableCell221
            // 
            this.xrTableCell221.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell221.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell221.Name = "xrTableCell221";
            this.xrTableCell221.StylePriority.UseFont = false;
            this.xrTableCell221.StylePriority.UseForeColor = false;
            this.xrTableCell221.Text = "Contact No.History";
            this.xrTableCell221.Weight = 3;
            // 
            // EmploymentHistory
            // 
            this.EmploymentHistory.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail13,
            this.GroupHeader13});
            this.EmploymentHistory.DataMember = "ConsumerEmploymentHistory";
            this.EmploymentHistory.Level = 12;
            this.EmploymentHistory.Name = "EmploymentHistory";
            // 
            // Detail13
            // 
            this.Detail13.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblEmploymentD});
            this.Detail13.Height = 25;
            this.Detail13.Name = "Detail13";
            // 
            // tblEmploymentD
            // 
            this.tblEmploymentD.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblEmploymentD.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblEmploymentD.Location = new System.Drawing.Point(8, 0);
            this.tblEmploymentD.Name = "tblEmploymentD";
            this.tblEmploymentD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow74});
            this.tblEmploymentD.Size = new System.Drawing.Size(792, 25);
            this.tblEmploymentD.StylePriority.UseBorderColor = false;
            this.tblEmploymentD.StylePriority.UseBorders = false;
            // 
            // xrTableRow74
            // 
            this.xrTableRow74.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDEmpUpdatedate,
            this.lblDEmployer,
            this.lblDDesignation});
            this.xrTableRow74.Name = "xrTableRow74";
            this.xrTableRow74.Weight = 1;
            // 
            // lblDEmpUpdatedate
            // 
            this.lblDEmpUpdatedate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerEmploymentHistory.LastUpdatedDate", "")});
            this.lblDEmpUpdatedate.Name = "lblDEmpUpdatedate";
            this.lblDEmpUpdatedate.Weight = 0.34848484848484851;
            // 
            // lblDEmployer
            // 
            this.lblDEmployer.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerEmploymentHistory.EmployerDetail", "")});
            this.lblDEmployer.Name = "lblDEmployer";
            this.lblDEmployer.Weight = 1.4204545454545454;
            // 
            // lblDDesignation
            // 
            this.lblDDesignation.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerEmploymentHistory.Designation", "")});
            this.lblDDesignation.Name = "lblDDesignation";
            this.lblDDesignation.Weight = 1.231060606060606;
            // 
            // GroupHeader13
            // 
            this.GroupHeader13.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblEmploymentC,
            this.tblEmploymentH});
            this.GroupHeader13.Height = 79;
            this.GroupHeader13.Name = "GroupHeader13";
            // 
            // tblEmploymentC
            // 
            this.tblEmploymentC.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tblEmploymentC.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblEmploymentC.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblEmploymentC.Location = new System.Drawing.Point(8, 42);
            this.tblEmploymentC.Name = "tblEmploymentC";
            this.tblEmploymentC.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow73});
            this.tblEmploymentC.Size = new System.Drawing.Size(792, 25);
            this.tblEmploymentC.StylePriority.UseBorderColor = false;
            this.tblEmploymentC.StylePriority.UseBorders = false;
            // 
            // xrTableRow73
            // 
            this.xrTableRow73.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblEmpUpdatedate,
            this.lblEmployer,
            this.lblDesignation});
            this.xrTableRow73.Name = "xrTableRow73";
            this.xrTableRow73.Weight = 1;
            // 
            // lblEmpUpdatedate
            // 
            this.lblEmpUpdatedate.CanGrow = false;
            this.lblEmpUpdatedate.Name = "lblEmpUpdatedate";
            this.lblEmpUpdatedate.Text = "Bureau Update";
            this.lblEmpUpdatedate.Weight = 0.34848484848484851;
            // 
            // lblEmployer
            // 
            this.lblEmployer.CanGrow = false;
            this.lblEmployer.Name = "lblEmployer";
            this.lblEmployer.Text = "Employer";
            this.lblEmployer.Weight = 1.4204545454545454;
            // 
            // lblDesignation
            // 
            this.lblDesignation.CanGrow = false;
            this.lblDesignation.Name = "lblDesignation";
            this.lblDesignation.Text = "Designation";
            this.lblDesignation.Weight = 1.231060606060606;
            // 
            // tblEmploymentH
            // 
            this.tblEmploymentH.ForeColor = System.Drawing.Color.Black;
            this.tblEmploymentH.Location = new System.Drawing.Point(8, 8);
            this.tblEmploymentH.Name = "tblEmploymentH";
            this.tblEmploymentH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow72});
            this.tblEmploymentH.Size = new System.Drawing.Size(433, 25);
            this.tblEmploymentH.StylePriority.UseForeColor = false;
            // 
            // xrTableRow72
            // 
            this.xrTableRow72.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell230});
            this.xrTableRow72.Name = "xrTableRow72";
            this.xrTableRow72.Weight = 1;
            // 
            // xrTableCell230
            // 
            this.xrTableCell230.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell230.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell230.Name = "xrTableCell230";
            this.xrTableCell230.StylePriority.UseFont = false;
            this.xrTableCell230.StylePriority.UseForeColor = false;
            this.xrTableCell230.Text = "Employment History";
            this.xrTableCell230.Weight = 3;
            // 
            // AccountStatus
            // 
            this.AccountStatus.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail14,
            this.GroupHeader14});
            this.AccountStatus.DataMember = "CounsumerAccountStatus";
            this.AccountStatus.Level = 13;
            this.AccountStatus.Name = "AccountStatus";
            // 
            // Detail14
            // 
            this.Detail14.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblAccStatusD});
            this.Detail14.Height = 25;
            this.Detail14.Name = "Detail14";
            // 
            // tblAccStatusD
            // 
            this.tblAccStatusD.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblAccStatusD.Location = new System.Drawing.Point(8, 0);
            this.tblAccStatusD.Name = "tblAccStatusD";
            this.tblAccStatusD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow76});
            this.tblAccStatusD.Size = new System.Drawing.Size(650, 25);
            this.tblAccStatusD.StylePriority.UseBorderColor = false;
            // 
            // xrTableRow76
            // 
            this.xrTableRow76.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblAccOpenDateD,
            this.lblCompanyD,
            this.lblCreditLimitD,
            this.lblCurrBalanceD,
            this.lblInstallementD,
            this.xrTableCell48,
            this.lblAccTypeD});
            this.xrTableRow76.Name = "xrTableRow76";
            this.xrTableRow76.StylePriority.UseBorders = false;
            this.xrTableRow76.Weight = 1;
            // 
            // lblAccOpenDateD
            // 
            this.lblAccOpenDateD.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblAccOpenDateD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CounsumerAccountStatus.AccountOpenedDate", "")});
            this.lblAccOpenDateD.Name = "lblAccOpenDateD";
            this.lblAccOpenDateD.StylePriority.UseBorders = false;
            this.lblAccOpenDateD.Text = "lblAccOpenDateD";
            this.lblAccOpenDateD.Weight = 0.38461538461538458;
            // 
            // lblCompanyD
            // 
            this.lblCompanyD.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblCompanyD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CounsumerAccountStatus.SubscriberName", "")});
            this.lblCompanyD.Name = "lblCompanyD";
            this.lblCompanyD.StylePriority.UseBorders = false;
            this.lblCompanyD.Text = "xrTableCell15";
            this.lblCompanyD.Weight = 0.73076923076923084;
            // 
            // lblCreditLimitD
            // 
            this.lblCreditLimitD.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblCreditLimitD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CounsumerAccountStatus.CreditLimitAmt", "")});
            this.lblCreditLimitD.Name = "lblCreditLimitD";
            this.lblCreditLimitD.StylePriority.UseBorders = false;
            this.lblCreditLimitD.Weight = 0.42615384615384611;
            // 
            // lblCurrBalanceD
            // 
            this.lblCurrBalanceD.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblCurrBalanceD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CounsumerAccountStatus.CurrentBalanceAmt", "")});
            this.lblCurrBalanceD.Name = "lblCurrBalanceD";
            this.lblCurrBalanceD.StylePriority.UseBorders = false;
            this.lblCurrBalanceD.Weight = 0.38461538461538464;
            // 
            // lblInstallementD
            // 
            this.lblInstallementD.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblInstallementD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CounsumerAccountStatus.MonthlyInstalmentAmt", "")});
            this.lblInstallementD.Name = "lblInstallementD";
            this.lblInstallementD.StylePriority.UseBorders = false;
            this.lblInstallementD.Weight = 0.34769230769230763;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell48.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CounsumerAccountStatus.ArrearsAmt", "")});
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.StylePriority.UseBorders = false;
            this.xrTableCell48.Weight = 0.34692307692307689;
            // 
            // lblAccTypeD
            // 
            this.lblAccTypeD.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblAccTypeD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "CounsumerAccountStatus.AccountType", "")});
            this.lblAccTypeD.Name = "lblAccTypeD";
            this.lblAccTypeD.StylePriority.UseBorders = false;
            this.lblAccTypeD.Weight = 0.37923076923076926;
            // 
            // GroupHeader14
            // 
            this.GroupHeader14.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblAccStatusC,
            this.tblAccStatusH});
            this.GroupHeader14.Height = 68;
            this.GroupHeader14.Name = "GroupHeader14";
            // 
            // tblAccStatusC
            // 
            this.tblAccStatusC.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblAccStatusC.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblAccStatusC.Location = new System.Drawing.Point(8, 42);
            this.tblAccStatusC.Name = "tblAccStatusC";
            this.tblAccStatusC.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow77});
            this.tblAccStatusC.Size = new System.Drawing.Size(650, 25);
            this.tblAccStatusC.StylePriority.UseBorderColor = false;
            this.tblAccStatusC.StylePriority.UseBorders = false;
            // 
            // xrTableRow77
            // 
            this.xrTableRow77.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblAccOpenDate,
            this.lblCompany,
            this.lblCreditLimit,
            this.lblCurrBalance,
            this.lblInstallement,
            this.lblArrearsAmt,
            this.lblAccType});
            this.xrTableRow77.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableRow77.Name = "xrTableRow77";
            this.xrTableRow77.StylePriority.UseFont = false;
            this.xrTableRow77.Weight = 1;
            // 
            // lblAccOpenDate
            // 
            this.lblAccOpenDate.Name = "lblAccOpenDate";
            this.lblAccOpenDate.Text = "Date Account Opened";
            this.lblAccOpenDate.Weight = 0.38461538461538458;
            // 
            // lblCompany
            // 
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Text = "Company";
            this.lblCompany.Weight = 0.73076923076923084;
            // 
            // lblCreditLimit
            // 
            this.lblCreditLimit.Name = "lblCreditLimit";
            this.lblCreditLimit.Text = "Account Credit Limit";
            this.lblCreditLimit.Weight = 0.42615384615384616;
            // 
            // lblCurrBalance
            // 
            this.lblCurrBalance.Name = "lblCurrBalance";
            this.lblCurrBalance.Text = "Current Balance";
            this.lblCurrBalance.Weight = 0.38461538461538458;
            // 
            // lblInstallement
            // 
            this.lblInstallement.Name = "lblInstallement";
            this.lblInstallement.Text = "Installment Amount";
            this.lblInstallement.Weight = 0.34769230769230763;
            // 
            // lblArrearsAmt
            // 
            this.lblArrearsAmt.Name = "lblArrearsAmt";
            this.lblArrearsAmt.Text = "Arrears Amount";
            this.lblArrearsAmt.Weight = 0.34692307692307689;
            // 
            // lblAccType
            // 
            this.lblAccType.Name = "lblAccType";
            this.lblAccType.Text = "Type of Account";
            this.lblAccType.Weight = 0.37923076923076926;
            // 
            // tblAccStatusH
            // 
            this.tblAccStatusH.ForeColor = System.Drawing.Color.Black;
            this.tblAccStatusH.Location = new System.Drawing.Point(8, 8);
            this.tblAccStatusH.Name = "tblAccStatusH";
            this.tblAccStatusH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow75});
            this.tblAccStatusH.Size = new System.Drawing.Size(433, 25);
            this.tblAccStatusH.StylePriority.UseForeColor = false;
            // 
            // xrTableRow75
            // 
            this.xrTableRow75.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell35});
            this.xrTableRow75.Name = "xrTableRow75";
            this.xrTableRow75.Weight = 1;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell35.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.StylePriority.UseFont = false;
            this.xrTableCell35.StylePriority.UseForeColor = false;
            this.xrTableCell35.Text = "Credit Account Status";
            this.xrTableCell35.Weight = 3;
            // 
            // MonthlyPaymentHeader
            // 
            this.MonthlyPaymentHeader.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail15,
            this.GroupHeader15});
            this.MonthlyPaymentHeader.DataMember = "Consumer24MonthlyPaymentHeader";
            this.MonthlyPaymentHeader.Level = 14;
            this.MonthlyPaymentHeader.Name = "MonthlyPaymentHeader";
            // 
            // Detail15
            // 
            this.Detail15.Height = 2;
            this.Detail15.Name = "Detail15";
            // 
            // GroupHeader15
            // 
            this.GroupHeader15.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblMonthlyPaymentH,
            this.xrPageInfo3});
            this.GroupHeader15.Height = 52;
            this.GroupHeader15.Name = "GroupHeader15";
            // 
            // tblMonthlyPaymentH
            // 
            this.tblMonthlyPaymentH.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblMonthlyPaymentH.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblMonthlyPaymentH.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.tblMonthlyPaymentH.Location = new System.Drawing.Point(8, 25);
            this.tblMonthlyPaymentH.Name = "tblMonthlyPaymentH";
            this.tblMonthlyPaymentH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow78});
            this.tblMonthlyPaymentH.Size = new System.Drawing.Size(792, 25);
            this.tblMonthlyPaymentH.StylePriority.UseBorderColor = false;
            this.tblMonthlyPaymentH.StylePriority.UseBorders = false;
            this.tblMonthlyPaymentH.StylePriority.UseFont = false;
            this.tblMonthlyPaymentH.StylePriority.UseTextAlignment = false;
            this.tblMonthlyPaymentH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableRow78
            // 
            this.xrTableRow78.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblcompanyMP,
            this.lblM01,
            this.lblM02,
            this.lblM03,
            this.lblM04,
            this.lblM05,
            this.lblM06,
            this.xrTableCell52,
            this.lblM08,
            this.lblM09,
            this.lblM10,
            this.lblM11,
            this.lblM12,
            this.lblM13,
            this.lblM14,
            this.lblM15,
            this.lblM16,
            this.lblM17,
            this.lblM18,
            this.lblM19,
            this.lblM20,
            this.lblM21,
            this.lblM22,
            this.lblM23,
            this.lblM24});
            this.xrTableRow78.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableRow78.Name = "xrTableRow78";
            this.xrTableRow78.StylePriority.UseBorders = false;
            this.xrTableRow78.StylePriority.UseFont = false;
            this.xrTableRow78.Weight = 1;
            // 
            // lblcompanyMP
            // 
            this.lblcompanyMP.Name = "lblcompanyMP";
            this.lblcompanyMP.StylePriority.UseFont = false;
            this.lblcompanyMP.Text = "Company";
            this.lblcompanyMP.Weight = 0.44387755102040793;
            // 
            // lblM01
            // 
            this.lblM01.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M01", "")});
            this.lblM01.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblM01.Name = "lblM01";
            this.lblM01.StylePriority.UseFont = false;
            this.lblM01.Text = "lblM01";
            this.lblM01.Weight = 0.18601171846934769;
            // 
            // lblM02
            // 
            this.lblM02.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M02", "")});
            this.lblM02.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblM02.Name = "lblM02";
            this.lblM02.StylePriority.UseFont = false;
            this.lblM02.Weight = 0.18601171846934772;
            // 
            // lblM03
            // 
            this.lblM03.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M03", "")});
            this.lblM03.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblM03.Name = "lblM03";
            this.lblM03.StylePriority.UseFont = false;
            this.lblM03.Weight = 0.18601171846934769;
            // 
            // lblM04
            // 
            this.lblM04.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M04", "")});
            this.lblM04.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblM04.Name = "lblM04";
            this.lblM04.StylePriority.UseFont = false;
            this.lblM04.Weight = 0.18594890203530276;
            // 
            // lblM05
            // 
            this.lblM05.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M05", "")});
            this.lblM05.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblM05.Name = "lblM05";
            this.lblM05.StylePriority.UseFont = false;
            this.lblM05.Weight = 0.18692207722491891;
            // 
            // lblM06
            // 
            this.lblM06.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M06", "")});
            this.lblM06.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblM06.Name = "lblM06";
            this.lblM06.StylePriority.UseFont = false;
            this.lblM06.Text = "lblM06";
            this.lblM06.Weight = 0.18480901023986329;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M07", "")});
            this.xrTableCell52.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.StylePriority.UseFont = false;
            this.xrTableCell52.Text = "xrTableCell52";
            this.xrTableCell52.Weight = 0.18282615846050218;
            // 
            // lblM08
            // 
            this.lblM08.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M08", "")});
            this.lblM08.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblM08.Name = "lblM08";
            this.lblM08.StylePriority.UseFont = false;
            this.lblM08.Weight = 0.18692207722491891;
            // 
            // lblM09
            // 
            this.lblM09.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M09", "")});
            this.lblM09.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblM09.Name = "lblM09";
            this.lblM09.StylePriority.UseFont = false;
            this.lblM09.Weight = 0.18692207722491885;
            // 
            // lblM10
            // 
            this.lblM10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M10", "")});
            this.lblM10.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblM10.Name = "lblM10";
            this.lblM10.StylePriority.UseFont = false;
            this.lblM10.Weight = 0.18692207722491883;
            // 
            // lblM11
            // 
            this.lblM11.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M11", "")});
            this.lblM11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblM11.Name = "lblM11";
            this.lblM11.StylePriority.UseFont = false;
            this.lblM11.Weight = 0.18692207722491888;
            // 
            // lblM12
            // 
            this.lblM12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M12", "")});
            this.lblM12.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblM12.Name = "lblM12";
            this.lblM12.StylePriority.UseFont = false;
            this.lblM12.Weight = 0.18692207722491883;
            // 
            // lblM13
            // 
            this.lblM13.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M13", "")});
            this.lblM13.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblM13.Name = "lblM13";
            this.lblM13.StylePriority.UseFont = false;
            this.lblM13.Weight = 0.18692207722491888;
            // 
            // lblM14
            // 
            this.lblM14.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M14", "")});
            this.lblM14.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblM14.Name = "lblM14";
            this.lblM14.StylePriority.UseFont = false;
            this.lblM14.Weight = 0.18692207722491888;
            // 
            // lblM15
            // 
            this.lblM15.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M15", "")});
            this.lblM15.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblM15.Name = "lblM15";
            this.lblM15.StylePriority.UseFont = false;
            this.lblM15.Weight = 0.18692207722491866;
            // 
            // lblM16
            // 
            this.lblM16.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M16", "")});
            this.lblM16.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblM16.Name = "lblM16";
            this.lblM16.StylePriority.UseFont = false;
            this.lblM16.Weight = 0.18861484967550241;
            // 
            // lblM17
            // 
            this.lblM17.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M17", "")});
            this.lblM17.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblM17.Name = "lblM17";
            this.lblM17.StylePriority.UseFont = false;
            this.lblM17.Weight = 0.18586923151767038;
            // 
            // lblM18
            // 
            this.lblM18.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M18", "")});
            this.lblM18.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblM18.Name = "lblM18";
            this.lblM18.StylePriority.UseFont = false;
            this.lblM18.Weight = 0.18888485710376113;
            // 
            // lblM19
            // 
            this.lblM19.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M19", "")});
            this.lblM19.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblM19.Name = "lblM19";
            this.lblM19.StylePriority.UseFont = false;
            this.lblM19.Weight = 0.18793983110485579;
            // 
            // lblM20
            // 
            this.lblM20.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M20", "")});
            this.lblM20.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblM20.Name = "lblM20";
            this.lblM20.StylePriority.UseFont = false;
            this.lblM20.Weight = 0.18807483481898493;
            // 
            // lblM21
            // 
            this.lblM21.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M21", "")});
            this.lblM21.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblM21.Name = "lblM21";
            this.lblM21.StylePriority.UseFont = false;
            this.lblM21.Weight = 0.18888485710376105;
            // 
            // lblM22
            // 
            this.lblM22.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M22", "")});
            this.lblM22.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblM22.Name = "lblM22";
            this.lblM22.StylePriority.UseFont = false;
            this.lblM22.Weight = 0.18681425751657579;
            // 
            // lblM23
            // 
            this.lblM23.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M23", "")});
            this.lblM23.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblM23.Name = "lblM23";
            this.lblM23.StylePriority.UseFont = false;
            this.lblM23.Weight = 0.18871569070953312;
            // 
            // lblM24
            // 
            this.lblM24.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblM24.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M24", "")});
            this.lblM24.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblM24.Name = "lblM24";
            this.lblM24.StylePriority.UseBorders = false;
            this.lblM24.StylePriority.UseFont = false;
            this.lblM24.Weight = 0.16381838531246676;
            // 
            // xrPageInfo3
            // 
            this.xrPageInfo3.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.xrPageInfo3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrPageInfo3.Format = "Monthly Payment Behaviour as at {0:dd/MM/yyyy}";
            this.xrPageInfo3.Location = new System.Drawing.Point(8, 0);
            this.xrPageInfo3.Name = "xrPageInfo3";
            this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo3.Size = new System.Drawing.Size(792, 25);
            this.xrPageInfo3.StylePriority.UseBorderColor = false;
            this.xrPageInfo3.StylePriority.UseBorders = false;
            this.xrPageInfo3.StylePriority.UseTextAlignment = false;
            this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Definition
            // 
            this.Definition.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail16,
            this.GroupHeader16,
            this.GroupFooter1});
            this.Definition.DataMember = "Definition";
            this.Definition.Level = 16;
            this.Definition.Name = "Definition";
            // 
            // Detail16
            // 
            this.Detail16.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblDefinitionD});
            this.Detail16.Height = 25;
            this.Detail16.Name = "Detail16";
            // 
            // tblDefinitionD
            // 
            this.tblDefinitionD.Location = new System.Drawing.Point(8, 0);
            this.tblDefinitionD.Name = "tblDefinitionD";
            this.tblDefinitionD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow81});
            this.tblDefinitionD.Size = new System.Drawing.Size(792, 25);
            this.tblDefinitionD.StylePriority.UseBorders = false;
            // 
            // xrTableRow81
            // 
            this.xrTableRow81.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDefDescD,
            this.lbDefCodeD});
            this.xrTableRow81.Name = "xrTableRow81";
            this.xrTableRow81.Weight = 1;
            // 
            // lblDefDescD
            // 
            this.lblDefDescD.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.lblDefDescD.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.lblDefDescD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Definition.DefinitionDesc", "")});
            this.lblDefDescD.Name = "lblDefDescD";
            this.lblDefDescD.StylePriority.UseBorderColor = false;
            this.lblDefDescD.StylePriority.UseBorders = false;
            this.lblDefDescD.Text = "lblDefDescD";
            this.lblDefDescD.Weight = 2.2113564668769716;
            // 
            // lbDefCodeD
            // 
            this.lbDefCodeD.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.lbDefCodeD.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lbDefCodeD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Definition.DefinitionCode", "")});
            this.lbDefCodeD.Name = "lbDefCodeD";
            this.lbDefCodeD.StylePriority.UseBorderColor = false;
            this.lbDefCodeD.StylePriority.UseBorders = false;
            this.lbDefCodeD.Weight = 5.2933753943217665;
            // 
            // GroupHeader16
            // 
            this.GroupHeader16.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblDefinitionC});
            this.GroupHeader16.Height = 25;
            this.GroupHeader16.Name = "GroupHeader16";
            // 
            // tblDefinitionC
            // 
            this.tblDefinitionC.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tblDefinitionC.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblDefinitionC.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.tblDefinitionC.Location = new System.Drawing.Point(8, 0);
            this.tblDefinitionC.Name = "tblDefinitionC";
            this.tblDefinitionC.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow80});
            this.tblDefinitionC.Size = new System.Drawing.Size(792, 25);
            this.tblDefinitionC.StylePriority.UseBorderColor = false;
            this.tblDefinitionC.StylePriority.UseBorders = false;
            // 
            // xrTableRow80
            // 
            this.xrTableRow80.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDefDesc,
            this.lbDefCode});
            this.xrTableRow80.Name = "xrTableRow80";
            this.xrTableRow80.Weight = 1;
            // 
            // lblDefDesc
            // 
            this.lblDefDesc.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.lblDefDesc.CanGrow = false;
            this.lblDefDesc.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Definition.DisplayText", "")});
            this.lblDefDesc.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Underline);
            this.lblDefDesc.Name = "lblDefDesc";
            this.lblDefDesc.StylePriority.UseBorders = false;
            this.lblDefDesc.StylePriority.UseFont = false;
            this.lblDefDesc.Text = "xrTableCell99";
            this.lblDefDesc.Weight = 2.2113564668769716;
            // 
            // lbDefCode
            // 
            this.lbDefCode.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lbDefCode.CanGrow = false;
            this.lbDefCode.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Underline);
            this.lbDefCode.Name = "lbDefCode";
            this.lbDefCode.StylePriority.UseBorders = false;
            this.lbDefCode.StylePriority.UseFont = false;
            this.lbDefCode.Text = "Indicators";
            this.lbDefCode.Weight = 5.2933753943217665;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine2});
            this.GroupFooter1.Height = 4;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrLine2
            // 
            this.xrLine2.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.xrLine2.Location = new System.Drawing.Point(8, 0);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.Size = new System.Drawing.Size(792, 2);
            this.xrLine2.StylePriority.UseForeColor = false;
            // 
            // AdverseInfo
            // 
            this.AdverseInfo.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail17,
            this.GroupHeader17});
            this.AdverseInfo.DataMember = "ConsumerAdverseInfo";
            this.AdverseInfo.Level = 17;
            this.AdverseInfo.Name = "AdverseInfo";
            // 
            // Detail17
            // 
            this.Detail17.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblAdverseData});
            this.Detail17.Height = 25;
            this.Detail17.Name = "Detail17";
            // 
            // tblAdverseData
            // 
            this.tblAdverseData.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblAdverseData.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblAdverseData.Location = new System.Drawing.Point(8, 0);
            this.tblAdverseData.Name = "tblAdverseData";
            this.tblAdverseData.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow85});
            this.tblAdverseData.Size = new System.Drawing.Size(800, 25);
            this.tblAdverseData.StylePriority.UseBorderColor = false;
            this.tblAdverseData.StylePriority.UseBorders = false;
            // 
            // xrTableRow85
            // 
            this.xrTableRow85.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDSubscriber,
            this.lblDAccNo,
            this.lblDActionDate,
            this.lblDAmount,
            this.lblDAccStatus,
            this.lblDAdvComment});
            this.xrTableRow85.Name = "xrTableRow85";
            this.xrTableRow85.Weight = 1;
            // 
            // lblDSubscriber
            // 
            this.lblDSubscriber.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdverseInfo.SubscriberName", "")});
            this.lblDSubscriber.Name = "lblDSubscriber";
            this.lblDSubscriber.Text = "lblDSubscriber";
            this.lblDSubscriber.Weight = 0.725;
            // 
            // lblDAccNo
            // 
            this.lblDAccNo.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdverseInfo.AccountNo", "")});
            this.lblDAccNo.Name = "lblDAccNo";
            this.lblDAccNo.Text = "lblDAccNo";
            this.lblDAccNo.Weight = 0.46374999999999994;
            // 
            // lblDActionDate
            // 
            this.lblDActionDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdverseInfo.ActionDate", "")});
            this.lblDActionDate.Name = "lblDActionDate";
            this.lblDActionDate.Text = "lblDActionDate";
            this.lblDActionDate.Weight = 0.31375;
            // 
            // lblDAmount
            // 
            this.lblDAmount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdverseInfo.CurrentBalanceAmt", "")});
            this.lblDAmount.Name = "lblDAmount";
            this.lblDAmount.Text = "lblDAmount";
            this.lblDAmount.Weight = 0.3425;
            // 
            // lblDAccStatus
            // 
            this.lblDAccStatus.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdverseInfo.DataStatus", "")});
            this.lblDAccStatus.Name = "lblDAccStatus";
            this.lblDAccStatus.Text = "lblDAccStatus";
            this.lblDAccStatus.Weight = 0.41375;
            // 
            // lblDAdvComment
            // 
            this.lblDAdvComment.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdverseInfo.Comments", "")});
            this.lblDAdvComment.Name = "lblDAdvComment";
            this.lblDAdvComment.Weight = 0.74125;
            // 
            // GroupHeader17
            // 
            this.GroupHeader17.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblAdverseColumns,
            this.tblAdverseH,
            this.tblDomainH});
            this.GroupHeader17.Name = "GroupHeader17";
            // 
            // tblAdverseColumns
            // 
            this.tblAdverseColumns.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tblAdverseColumns.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblAdverseColumns.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblAdverseColumns.Location = new System.Drawing.Point(8, 75);
            this.tblAdverseColumns.Name = "tblAdverseColumns";
            this.tblAdverseColumns.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow83});
            this.tblAdverseColumns.Size = new System.Drawing.Size(800, 25);
            this.tblAdverseColumns.StylePriority.UseBorderColor = false;
            this.tblAdverseColumns.StylePriority.UseBorders = false;
            // 
            // xrTableRow83
            // 
            this.xrTableRow83.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSubscriber,
            this.lblAccountNo,
            this.lblActionDate,
            this.lblAmount,
            this.lblAccStatus,
            this.lblAdverseComment});
            this.xrTableRow83.Name = "xrTableRow83";
            this.xrTableRow83.Weight = 1;
            // 
            // lblSubscriber
            // 
            this.lblSubscriber.CanGrow = false;
            this.lblSubscriber.Name = "lblSubscriber";
            this.lblSubscriber.Text = "Subscriber";
            this.lblSubscriber.Weight = 0.725;
            // 
            // lblAccountNo
            // 
            this.lblAccountNo.CanGrow = false;
            this.lblAccountNo.Name = "lblAccountNo";
            this.lblAccountNo.Text = "Account No.";
            this.lblAccountNo.Weight = 0.46625;
            // 
            // lblActionDate
            // 
            this.lblActionDate.CanGrow = false;
            this.lblActionDate.Name = "lblActionDate";
            this.lblActionDate.Text = "Action date";
            this.lblActionDate.Weight = 0.31375;
            // 
            // lblAmount
            // 
            this.lblAmount.CanGrow = false;
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Text = "Amount";
            this.lblAmount.Weight = 0.33999999999999997;
            // 
            // lblAccStatus
            // 
            this.lblAccStatus.CanGrow = false;
            this.lblAccStatus.Name = "lblAccStatus";
            this.lblAccStatus.Text = "Account Status";
            this.lblAccStatus.Weight = 0.41375;
            // 
            // lblAdverseComment
            // 
            this.lblAdverseComment.CanGrow = false;
            this.lblAdverseComment.Name = "lblAdverseComment";
            this.lblAdverseComment.Text = "Comment";
            this.lblAdverseComment.Weight = 0.74125;
            // 
            // tblAdverseH
            // 
            this.tblAdverseH.ForeColor = System.Drawing.Color.Black;
            this.tblAdverseH.Location = new System.Drawing.Point(8, 42);
            this.tblAdverseH.Name = "tblAdverseH";
            this.tblAdverseH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow84});
            this.tblAdverseH.Size = new System.Drawing.Size(433, 25);
            this.tblAdverseH.StylePriority.UseForeColor = false;
            // 
            // xrTableRow84
            // 
            this.xrTableRow84.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell108});
            this.xrTableRow84.Name = "xrTableRow84";
            this.xrTableRow84.Weight = 1;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell108.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.StylePriority.UseFont = false;
            this.xrTableCell108.StylePriority.UseForeColor = false;
            this.xrTableCell108.Text = "Adverse/Defaults";
            this.xrTableCell108.Weight = 3;
            // 
            // tblDomainH
            // 
            this.tblDomainH.ForeColor = System.Drawing.Color.Black;
            this.tblDomainH.Location = new System.Drawing.Point(8, 8);
            this.tblDomainH.Name = "tblDomainH";
            this.tblDomainH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow82});
            this.tblDomainH.Size = new System.Drawing.Size(433, 25);
            this.tblDomainH.StylePriority.UseForeColor = false;
            // 
            // xrTableRow82
            // 
            this.xrTableRow82.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell104});
            this.xrTableRow82.Name = "xrTableRow82";
            this.xrTableRow82.Weight = 1;
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell104.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.StylePriority.UseFont = false;
            this.xrTableCell104.StylePriority.UseForeColor = false;
            this.xrTableCell104.Text = "Public Domain Records ";
            this.xrTableCell104.Weight = 3;
            // 
            // Judgements
            // 
            this.Judgements.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail18,
            this.GroupHeader18});
            this.Judgements.DataMember = "ConsumerJudgement";
            this.Judgements.Level = 18;
            this.Judgements.Name = "Judgements";
            // 
            // Detail18
            // 
            this.Detail18.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblDJudg});
            this.Detail18.Height = 26;
            this.Detail18.Name = "Detail18";
            // 
            // tblDJudg
            // 
            this.tblDJudg.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblDJudg.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblDJudg.Location = new System.Drawing.Point(8, 0);
            this.tblDJudg.Name = "tblDJudg";
            this.tblDJudg.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow88});
            this.tblDJudg.Size = new System.Drawing.Size(800, 25);
            this.tblDJudg.StylePriority.UseBorderColor = false;
            this.tblDJudg.StylePriority.UseBorders = false;
            // 
            // xrTableRow88
            // 
            this.xrTableRow88.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblJDCaseno,
            this.lblJDIssueDate,
            this.lblJDType,
            this.lblJDAmount,
            this.lblJDPlaintiff,
            this.lblJDCourt,
            this.lblJDAttorney,
            this.lblJDPhoneNo,
            this.lblJDComment});
            this.xrTableRow88.Name = "xrTableRow88";
            this.xrTableRow88.Weight = 1;
            // 
            // lblJDCaseno
            // 
            this.lblJDCaseno.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.CaseNumber", "")});
            this.lblJDCaseno.Name = "lblJDCaseno";
            this.lblJDCaseno.Text = "xrTableCell126";
            this.lblJDCaseno.Weight = 0.28;
            // 
            // lblJDIssueDate
            // 
            this.lblJDIssueDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.CaseFilingDate", "")});
            this.lblJDIssueDate.Name = "lblJDIssueDate";
            this.lblJDIssueDate.Text = "xrTableCell127";
            this.lblJDIssueDate.Weight = 0.18625;
            // 
            // lblJDType
            // 
            this.lblJDType.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.CaseType", "")});
            this.lblJDType.Name = "lblJDType";
            this.lblJDType.Text = "xrTableCell128";
            this.lblJDType.Weight = 0.37375;
            // 
            // lblJDAmount
            // 
            this.lblJDAmount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.DisputeAmt", "")});
            this.lblJDAmount.Name = "lblJDAmount";
            this.lblJDAmount.Text = "xrTableCell129";
            this.lblJDAmount.Weight = 0.18625;
            // 
            // lblJDPlaintiff
            // 
            this.lblJDPlaintiff.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.PlaintiffName", "")});
            this.lblJDPlaintiff.Name = "lblJDPlaintiff";
            this.lblJDPlaintiff.Text = "xrTableCell130";
            this.lblJDPlaintiff.Weight = 0.34375;
            // 
            // lblJDCourt
            // 
            this.lblJDCourt.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.CourtName", "")});
            this.lblJDCourt.Name = "lblJDCourt";
            this.lblJDCourt.Text = "xrTableCell131";
            this.lblJDCourt.Weight = 0.34375;
            // 
            // lblJDAttorney
            // 
            this.lblJDAttorney.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.AttorneyName", "")});
            this.lblJDAttorney.Name = "lblJDAttorney";
            this.lblJDAttorney.Text = "xrTableCell132";
            this.lblJDAttorney.Weight = 0.40625000000000006;
            // 
            // lblJDPhoneNo
            // 
            this.lblJDPhoneNo.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.TelephoneNo", "")});
            this.lblJDPhoneNo.Name = "lblJDPhoneNo";
            this.lblJDPhoneNo.Text = "xrTableCell133";
            this.lblJDPhoneNo.Weight = 0.3425;
            // 
            // lblJDComment
            // 
            this.lblJDComment.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.Comments", "")});
            this.lblJDComment.Name = "lblJDComment";
            this.lblJDComment.Weight = 0.5375;
            // 
            // GroupHeader18
            // 
            this.GroupHeader18.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblCJud,
            this.tblJudgmentsH});
            this.GroupHeader18.Height = 75;
            this.GroupHeader18.Name = "GroupHeader18";
            // 
            // tblCJud
            // 
            this.tblCJud.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblCJud.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblCJud.Location = new System.Drawing.Point(8, 50);
            this.tblCJud.Name = "tblCJud";
            this.tblCJud.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow87});
            this.tblCJud.Size = new System.Drawing.Size(800, 25);
            this.tblCJud.StylePriority.UseBorderColor = false;
            this.tblCJud.StylePriority.UseBorders = false;
            // 
            // xrTableRow87
            // 
            this.xrTableRow87.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblJCaseNo,
            this.lblJIssueDate,
            this.lblJType,
            this.lblJAmount,
            this.lblJPlaintiff,
            this.lblJCourt,
            this.lblJAttorney,
            this.lblJPhoneNo,
            this.lblJComment});
            this.xrTableRow87.Name = "xrTableRow87";
            this.xrTableRow87.Weight = 1;
            // 
            // lblJCaseNo
            // 
            this.lblJCaseNo.Name = "lblJCaseNo";
            this.lblJCaseNo.Text = "Case No.";
            this.lblJCaseNo.Weight = 0.28;
            // 
            // lblJIssueDate
            // 
            this.lblJIssueDate.Name = "lblJIssueDate";
            this.lblJIssueDate.Text = "Issue date";
            this.lblJIssueDate.Weight = 0.18625;
            // 
            // lblJType
            // 
            this.lblJType.Name = "lblJType";
            this.lblJType.Text = "Judgment Type";
            this.lblJType.Weight = 0.37375;
            // 
            // lblJAmount
            // 
            this.lblJAmount.Name = "lblJAmount";
            this.lblJAmount.Text = "Amount";
            this.lblJAmount.Weight = 0.18625;
            // 
            // lblJPlaintiff
            // 
            this.lblJPlaintiff.Name = "lblJPlaintiff";
            this.lblJPlaintiff.Text = "Plaintiff";
            this.lblJPlaintiff.Weight = 0.34374999999999994;
            // 
            // lblJCourt
            // 
            this.lblJCourt.Name = "lblJCourt";
            this.lblJCourt.Text = "Court";
            this.lblJCourt.Weight = 0.34375;
            // 
            // lblJAttorney
            // 
            this.lblJAttorney.Name = "lblJAttorney";
            this.lblJAttorney.Text = "Attorney";
            this.lblJAttorney.Weight = 0.40625000000000006;
            // 
            // lblJPhoneNo
            // 
            this.lblJPhoneNo.Name = "lblJPhoneNo";
            this.lblJPhoneNo.Text = "Phone No";
            this.lblJPhoneNo.Weight = 0.34250000000000008;
            // 
            // lblJComment
            // 
            this.lblJComment.Name = "lblJComment";
            this.lblJComment.Text = "Comment";
            this.lblJComment.Weight = 0.5375;
            // 
            // tblJudgmentsH
            // 
            this.tblJudgmentsH.ForeColor = System.Drawing.Color.Black;
            this.tblJudgmentsH.Location = new System.Drawing.Point(8, 0);
            this.tblJudgmentsH.Name = "tblJudgmentsH";
            this.tblJudgmentsH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow86});
            this.tblJudgmentsH.Size = new System.Drawing.Size(433, 25);
            this.tblJudgmentsH.StylePriority.UseForeColor = false;
            // 
            // xrTableRow86
            // 
            this.xrTableRow86.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell118});
            this.xrTableRow86.Name = "xrTableRow86";
            this.xrTableRow86.Weight = 1;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell118.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.StylePriority.UseFont = false;
            this.xrTableCell118.StylePriority.UseForeColor = false;
            this.xrTableCell118.Text = "Judgements";
            this.xrTableCell118.Weight = 3;
            // 
            // AdminOrders
            // 
            this.AdminOrders.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail19,
            this.GroupHeader19});
            this.AdminOrders.DataMember = "ConsumerAdminOrder";
            this.AdminOrders.Level = 19;
            this.AdminOrders.Name = "AdminOrders";
            // 
            // Detail19
            // 
            this.Detail19.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblDAO});
            this.Detail19.Height = 25;
            this.Detail19.Name = "Detail19";
            // 
            // tblDAO
            // 
            this.tblDAO.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblDAO.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblDAO.Location = new System.Drawing.Point(8, 0);
            this.tblDAO.Name = "tblDAO";
            this.tblDAO.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow91});
            this.tblDAO.Size = new System.Drawing.Size(800, 25);
            this.tblDAO.StylePriority.UseBorderColor = false;
            this.tblDAO.StylePriority.UseBorders = false;
            // 
            // xrTableRow91
            // 
            this.xrTableRow91.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblAODCaseno,
            this.lblAODIssuedate,
            this.lblAODLoadeddate,
            this.lblAODType,
            this.lblAODAmount,
            this.lblAODPlaintiff,
            this.lblAODCOurt,
            this.lblAODAttorney,
            this.lblAODPhoenNo});
            this.xrTableRow91.Name = "xrTableRow91";
            this.xrTableRow91.Weight = 1;
            // 
            // lblAODCaseno
            // 
            this.lblAODCaseno.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.CaseNumber", "")});
            this.lblAODCaseno.Name = "lblAODCaseno";
            this.lblAODCaseno.Weight = 0.31375;
            // 
            // lblAODIssuedate
            // 
            this.lblAODIssuedate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.CaseFilingDate", "")});
            this.lblAODIssuedate.Name = "lblAODIssuedate";
            this.lblAODIssuedate.Weight = 0.25;
            // 
            // lblAODLoadeddate
            // 
            this.lblAODLoadeddate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.LastUpdatedDate", "")});
            this.lblAODLoadeddate.Name = "lblAODLoadeddate";
            this.lblAODLoadeddate.Weight = 0.27875000000000005;
            // 
            // lblAODType
            // 
            this.lblAODType.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.CaseType", "")});
            this.lblAODType.Name = "lblAODType";
            this.lblAODType.Text = "lblAODType";
            this.lblAODType.Weight = 0.34375;
            // 
            // lblAODAmount
            // 
            this.lblAODAmount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.DisputeAmt", "")});
            this.lblAODAmount.Name = "lblAODAmount";
            this.lblAODAmount.Weight = 0.21625;
            // 
            // lblAODPlaintiff
            // 
            this.lblAODPlaintiff.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.PlaintiffName", "")});
            this.lblAODPlaintiff.Name = "lblAODPlaintiff";
            this.lblAODPlaintiff.Weight = 0.5;
            // 
            // lblAODCOurt
            // 
            this.lblAODCOurt.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.CourtName", "")});
            this.lblAODCOurt.Name = "lblAODCOurt";
            this.lblAODCOurt.Weight = 0.3475;
            // 
            // lblAODAttorney
            // 
            this.lblAODAttorney.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.AttorneyName", "")});
            this.lblAODAttorney.Name = "lblAODAttorney";
            this.lblAODAttorney.Weight = 0.4;
            // 
            // lblAODPhoenNo
            // 
            this.lblAODPhoenNo.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.TelephoneNo", "")});
            this.lblAODPhoenNo.Name = "lblAODPhoenNo";
            this.lblAODPhoenNo.Weight = 0.35;
            // 
            // GroupHeader19
            // 
            this.GroupHeader19.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblCAO,
            this.tblAOH});
            this.GroupHeader19.Height = 75;
            this.GroupHeader19.Name = "GroupHeader19";
            // 
            // tblCAO
            // 
            this.tblCAO.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tblCAO.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblCAO.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblCAO.Location = new System.Drawing.Point(8, 50);
            this.tblCAO.Name = "tblCAO";
            this.tblCAO.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow90});
            this.tblCAO.Size = new System.Drawing.Size(800, 25);
            this.tblCAO.StylePriority.UseBorderColor = false;
            this.tblCAO.StylePriority.UseBorders = false;
            // 
            // xrTableRow90
            // 
            this.xrTableRow90.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblAOCaseno,
            this.lblAOIssuedate,
            this.lblAODateLoaded,
            this.lblAOType,
            this.lblAOAmount,
            this.lblAOPlaintiff,
            this.lblAOCourt,
            this.lblAOAttorney,
            this.lblAOPhoneNo});
            this.xrTableRow90.Name = "xrTableRow90";
            this.xrTableRow90.Weight = 1;
            // 
            // lblAOCaseno
            // 
            this.lblAOCaseno.CanGrow = false;
            this.lblAOCaseno.Name = "lblAOCaseno";
            this.lblAOCaseno.Text = "Case No.";
            this.lblAOCaseno.Weight = 0.31375;
            // 
            // lblAOIssuedate
            // 
            this.lblAOIssuedate.CanGrow = false;
            this.lblAOIssuedate.Name = "lblAOIssuedate";
            this.lblAOIssuedate.Text = "Issue Date";
            this.lblAOIssuedate.Weight = 0.25;
            // 
            // lblAODateLoaded
            // 
            this.lblAODateLoaded.CanGrow = false;
            this.lblAODateLoaded.Name = "lblAODateLoaded";
            this.lblAODateLoaded.Text = "Date loaded";
            this.lblAODateLoaded.Weight = 0.27875000000000005;
            // 
            // lblAOType
            // 
            this.lblAOType.CanGrow = false;
            this.lblAOType.Name = "lblAOType";
            this.lblAOType.Text = "Judgment Type";
            this.lblAOType.Weight = 0.34375000000000006;
            // 
            // lblAOAmount
            // 
            this.lblAOAmount.CanGrow = false;
            this.lblAOAmount.Name = "lblAOAmount";
            this.lblAOAmount.Text = "Amount";
            this.lblAOAmount.Weight = 0.21625;
            // 
            // lblAOPlaintiff
            // 
            this.lblAOPlaintiff.CanGrow = false;
            this.lblAOPlaintiff.Name = "lblAOPlaintiff";
            this.lblAOPlaintiff.Text = "Plaintiff";
            this.lblAOPlaintiff.Weight = 0.5;
            // 
            // lblAOCourt
            // 
            this.lblAOCourt.CanGrow = false;
            this.lblAOCourt.Name = "lblAOCourt";
            this.lblAOCourt.Text = "Court";
            this.lblAOCourt.Weight = 0.3475;
            // 
            // lblAOAttorney
            // 
            this.lblAOAttorney.CanGrow = false;
            this.lblAOAttorney.Name = "lblAOAttorney";
            this.lblAOAttorney.Text = "Attorney";
            this.lblAOAttorney.Weight = 0.4;
            // 
            // lblAOPhoneNo
            // 
            this.lblAOPhoneNo.CanGrow = false;
            this.lblAOPhoneNo.Name = "lblAOPhoneNo";
            this.lblAOPhoneNo.Text = "Phone No";
            this.lblAOPhoneNo.Weight = 0.35;
            // 
            // tblAOH
            // 
            this.tblAOH.ForeColor = System.Drawing.Color.Black;
            this.tblAOH.Location = new System.Drawing.Point(8, 17);
            this.tblAOH.Name = "tblAOH";
            this.tblAOH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow89});
            this.tblAOH.Size = new System.Drawing.Size(433, 25);
            this.tblAOH.StylePriority.UseForeColor = false;
            // 
            // xrTableRow89
            // 
            this.xrTableRow89.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell128});
            this.xrTableRow89.Name = "xrTableRow89";
            this.xrTableRow89.Weight = 1;
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell128.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.StylePriority.UseFont = false;
            this.xrTableCell128.StylePriority.UseForeColor = false;
            this.xrTableCell128.Text = "Admin Orders";
            this.xrTableCell128.Weight = 3;
            // 
            // Sequestration
            // 
            this.Sequestration.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail20,
            this.GroupHeader20});
            this.Sequestration.DataMember = "ConsumerSequestration";
            this.Sequestration.Level = 20;
            this.Sequestration.Name = "Sequestration";
            // 
            // Detail20
            // 
            this.Detail20.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblSEQD});
            this.Detail20.Height = 25;
            this.Detail20.Name = "Detail20";
            // 
            // tblSEQD
            // 
            this.tblSEQD.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblSEQD.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblSEQD.Location = new System.Drawing.Point(8, 0);
            this.tblSEQD.Name = "tblSEQD";
            this.tblSEQD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow94});
            this.tblSEQD.Size = new System.Drawing.Size(800, 25);
            this.tblSEQD.StylePriority.UseBorderColor = false;
            this.tblSEQD.StylePriority.UseBorders = false;
            // 
            // xrTableRow94
            // 
            this.xrTableRow94.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSEQDCaseno,
            this.lblSEQDIssuedate,
            this.lblSEQDLoadedDate,
            this.lblSEQDType,
            this.lblSEQDAmount,
            this.lblSEQDPlaintiff,
            this.lblSEQDCourt,
            this.lblSEQDAttorney,
            this.lblSEQDPhoneNo});
            this.xrTableRow94.Name = "xrTableRow94";
            this.xrTableRow94.Weight = 1;
            // 
            // lblSEQDCaseno
            // 
            this.lblSEQDCaseno.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.CaseNumber", "")});
            this.lblSEQDCaseno.Name = "lblSEQDCaseno";
            this.lblSEQDCaseno.Text = "xrTableCell164";
            this.lblSEQDCaseno.Weight = 0.31375;
            // 
            // lblSEQDIssuedate
            // 
            this.lblSEQDIssuedate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.CaseFilingDate", "")});
            this.lblSEQDIssuedate.Name = "lblSEQDIssuedate";
            this.lblSEQDIssuedate.Text = "xrTableCell165";
            this.lblSEQDIssuedate.Weight = 0.25;
            // 
            // lblSEQDLoadedDate
            // 
            this.lblSEQDLoadedDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.LastUpdatedDate", "")});
            this.lblSEQDLoadedDate.Name = "lblSEQDLoadedDate";
            this.lblSEQDLoadedDate.Text = "xrTableCell166";
            this.lblSEQDLoadedDate.Weight = 0.27875000000000005;
            // 
            // lblSEQDType
            // 
            this.lblSEQDType.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.CaseType", "")});
            this.lblSEQDType.Name = "lblSEQDType";
            this.lblSEQDType.Text = "xrTableCell167";
            this.lblSEQDType.Weight = 0.34375;
            // 
            // lblSEQDAmount
            // 
            this.lblSEQDAmount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.DisputeAmt", "")});
            this.lblSEQDAmount.Name = "lblSEQDAmount";
            this.lblSEQDAmount.Text = "xrTableCell168";
            this.lblSEQDAmount.Weight = 0.21625;
            // 
            // lblSEQDPlaintiff
            // 
            this.lblSEQDPlaintiff.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.PlaintiffName", "")});
            this.lblSEQDPlaintiff.Name = "lblSEQDPlaintiff";
            this.lblSEQDPlaintiff.Text = "xrTableCell169";
            this.lblSEQDPlaintiff.Weight = 0.5;
            // 
            // lblSEQDCourt
            // 
            this.lblSEQDCourt.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.CourtName", "")});
            this.lblSEQDCourt.Name = "lblSEQDCourt";
            this.lblSEQDCourt.Text = "xrTableCell170";
            this.lblSEQDCourt.Weight = 0.3475;
            // 
            // lblSEQDAttorney
            // 
            this.lblSEQDAttorney.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.AttorneyName", "")});
            this.lblSEQDAttorney.Name = "lblSEQDAttorney";
            this.lblSEQDAttorney.Text = "xrTableCell171";
            this.lblSEQDAttorney.Weight = 0.4;
            // 
            // lblSEQDPhoneNo
            // 
            this.lblSEQDPhoneNo.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.TelephoneNo", "")});
            this.lblSEQDPhoneNo.Name = "lblSEQDPhoneNo";
            this.lblSEQDPhoneNo.Text = "xrTableCell172";
            this.lblSEQDPhoneNo.Weight = 0.35;
            // 
            // GroupHeader20
            // 
            this.GroupHeader20.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblSEQC,
            this.tblSEQH});
            this.GroupHeader20.Height = 75;
            this.GroupHeader20.Name = "GroupHeader20";
            // 
            // tblSEQC
            // 
            this.tblSEQC.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tblSEQC.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblSEQC.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblSEQC.Location = new System.Drawing.Point(8, 50);
            this.tblSEQC.Name = "tblSEQC";
            this.tblSEQC.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow93});
            this.tblSEQC.Size = new System.Drawing.Size(800, 25);
            this.tblSEQC.StylePriority.UseBorderColor = false;
            this.tblSEQC.StylePriority.UseBorders = false;
            // 
            // xrTableRow93
            // 
            this.xrTableRow93.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSEQCaseno,
            this.lblSEQIssueDate,
            this.lblSEQLoadeddate,
            this.lblSEQType,
            this.lblSEQAmount,
            this.lblSEQplaintiff,
            this.lblSEQCourt,
            this.lblSEQAttorney,
            this.lblSEQPhoneNo});
            this.xrTableRow93.Name = "xrTableRow93";
            this.xrTableRow93.Weight = 1;
            // 
            // lblSEQCaseno
            // 
            this.lblSEQCaseno.CanGrow = false;
            this.lblSEQCaseno.Name = "lblSEQCaseno";
            this.lblSEQCaseno.Text = "Case No.";
            this.lblSEQCaseno.Weight = 0.31375;
            // 
            // lblSEQIssueDate
            // 
            this.lblSEQIssueDate.CanGrow = false;
            this.lblSEQIssueDate.Name = "lblSEQIssueDate";
            this.lblSEQIssueDate.Text = "Issue Date";
            this.lblSEQIssueDate.Weight = 0.25;
            // 
            // lblSEQLoadeddate
            // 
            this.lblSEQLoadeddate.CanGrow = false;
            this.lblSEQLoadeddate.Name = "lblSEQLoadeddate";
            this.lblSEQLoadeddate.Text = "Date loaded";
            this.lblSEQLoadeddate.Weight = 0.27875000000000005;
            // 
            // lblSEQType
            // 
            this.lblSEQType.CanGrow = false;
            this.lblSEQType.Name = "lblSEQType";
            this.lblSEQType.Text = "Judgment Type";
            this.lblSEQType.Weight = 0.34375000000000006;
            // 
            // lblSEQAmount
            // 
            this.lblSEQAmount.CanGrow = false;
            this.lblSEQAmount.Name = "lblSEQAmount";
            this.lblSEQAmount.Text = "Amount";
            this.lblSEQAmount.Weight = 0.21625;
            // 
            // lblSEQplaintiff
            // 
            this.lblSEQplaintiff.CanGrow = false;
            this.lblSEQplaintiff.Name = "lblSEQplaintiff";
            this.lblSEQplaintiff.Text = "Plaintiff";
            this.lblSEQplaintiff.Weight = 0.5;
            // 
            // lblSEQCourt
            // 
            this.lblSEQCourt.CanGrow = false;
            this.lblSEQCourt.Name = "lblSEQCourt";
            this.lblSEQCourt.Text = "Court";
            this.lblSEQCourt.Weight = 0.3475;
            // 
            // lblSEQAttorney
            // 
            this.lblSEQAttorney.CanGrow = false;
            this.lblSEQAttorney.Name = "lblSEQAttorney";
            this.lblSEQAttorney.Text = "Attorney";
            this.lblSEQAttorney.Weight = 0.4;
            // 
            // lblSEQPhoneNo
            // 
            this.lblSEQPhoneNo.CanGrow = false;
            this.lblSEQPhoneNo.Name = "lblSEQPhoneNo";
            this.lblSEQPhoneNo.Text = "Phone No";
            this.lblSEQPhoneNo.Weight = 0.35;
            // 
            // tblSEQH
            // 
            this.tblSEQH.ForeColor = System.Drawing.Color.Black;
            this.tblSEQH.Location = new System.Drawing.Point(8, 8);
            this.tblSEQH.Name = "tblSEQH";
            this.tblSEQH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow92});
            this.tblSEQH.Size = new System.Drawing.Size(433, 25);
            this.tblSEQH.StylePriority.UseForeColor = false;
            // 
            // xrTableRow92
            // 
            this.xrTableRow92.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell156});
            this.xrTableRow92.Name = "xrTableRow92";
            this.xrTableRow92.Weight = 1;
            // 
            // xrTableCell156
            // 
            this.xrTableCell156.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell156.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell156.Name = "xrTableCell156";
            this.xrTableCell156.StylePriority.UseFont = false;
            this.xrTableCell156.StylePriority.UseForeColor = false;
            this.xrTableCell156.Text = "Sequestrations";
            this.xrTableCell156.Weight = 3;
            // 
            // DebtReviewStatus
            // 
            this.DebtReviewStatus.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail21,
            this.GroupHeader21});
            this.DebtReviewStatus.DataMember = "ConsumerDebtReviewStatus";
            this.DebtReviewStatus.Level = 21;
            this.DebtReviewStatus.Name = "DebtReviewStatus";
            // 
            // Detail21
            // 
            this.Detail21.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblDbtReviewD});
            this.Detail21.Name = "Detail21";
            // 
            // tblDbtReviewD
            // 
            this.tblDbtReviewD.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblDbtReviewD.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblDbtReviewD.Location = new System.Drawing.Point(8, 17);
            this.tblDbtReviewD.Name = "tblDbtReviewD";
            this.tblDbtReviewD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow96,
            this.xrTableRow98,
            this.xrTableRow97});
            this.tblDbtReviewD.Size = new System.Drawing.Size(792, 75);
            this.tblDbtReviewD.StylePriority.UseBorderColor = false;
            this.tblDbtReviewD.StylePriority.UseBorders = false;
            // 
            // xrTableRow96
            // 
            this.xrTableRow96.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDbtReviewDate,
            this.lblDbtReviewDateD});
            this.xrTableRow96.Name = "xrTableRow96";
            this.xrTableRow96.Weight = 1;
            // 
            // lblDbtReviewDate
            // 
            this.lblDbtReviewDate.Name = "lblDbtReviewDate";
            this.lblDbtReviewDate.Text = "Debt Review date";
            this.lblDbtReviewDate.Weight = 0.66287878787878785;
            // 
            // lblDbtReviewDateD
            // 
            this.lblDbtReviewDateD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtReviewStatus.DebtReviewStatusDate", "")});
            this.lblDbtReviewDateD.Name = "lblDbtReviewDateD";
            this.lblDbtReviewDateD.Text = "lblDbtReviewDateD";
            this.lblDbtReviewDateD.Weight = 2.3371212121212119;
            // 
            // xrTableRow98
            // 
            this.xrTableRow98.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCounsellorName,
            this.lblCounsellorNameD});
            this.xrTableRow98.Name = "xrTableRow98";
            this.xrTableRow98.Weight = 1;
            // 
            // lblCounsellorName
            // 
            this.lblCounsellorName.Name = "lblCounsellorName";
            this.lblCounsellorName.Text = "Debt Counsellor Name";
            this.lblCounsellorName.Weight = 0.66287878787878785;
            // 
            // lblCounsellorNameD
            // 
            this.lblCounsellorNameD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtReviewStatus.DebtCounsellorName", "")});
            this.lblCounsellorNameD.Name = "lblCounsellorNameD";
            this.lblCounsellorNameD.Text = "lblCounsellorNameD";
            this.lblCounsellorNameD.Weight = 2.3371212121212119;
            // 
            // xrTableRow97
            // 
            this.xrTableRow97.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCounsellorTelephoneNo,
            this.lblCounsellorTelephoneNoD});
            this.xrTableRow97.Name = "xrTableRow97";
            this.xrTableRow97.Weight = 1;
            // 
            // lblCounsellorTelephoneNo
            // 
            this.lblCounsellorTelephoneNo.Name = "lblCounsellorTelephoneNo";
            this.lblCounsellorTelephoneNo.Text = "Debt Counsellor Contact No.";
            this.lblCounsellorTelephoneNo.Weight = 0.66287878787878785;
            // 
            // lblCounsellorTelephoneNoD
            // 
            this.lblCounsellorTelephoneNoD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtReviewStatus.DebtCounsellorTelephoneNo", "")});
            this.lblCounsellorTelephoneNoD.Name = "lblCounsellorTelephoneNoD";
            this.lblCounsellorTelephoneNoD.Text = "lblCounsellorTelephoneNoD";
            this.lblCounsellorTelephoneNoD.Weight = 2.3371212121212119;
            // 
            // GroupHeader21
            // 
            this.GroupHeader21.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tbldbtReviewH});
            this.GroupHeader21.Height = 41;
            this.GroupHeader21.Name = "GroupHeader21";
            // 
            // tbldbtReviewH
            // 
            this.tbldbtReviewH.ForeColor = System.Drawing.Color.Black;
            this.tbldbtReviewH.Location = new System.Drawing.Point(8, 8);
            this.tbldbtReviewH.Name = "tbldbtReviewH";
            this.tbldbtReviewH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow95});
            this.tbldbtReviewH.Size = new System.Drawing.Size(433, 25);
            this.tbldbtReviewH.StylePriority.UseForeColor = false;
            // 
            // xrTableRow95
            // 
            this.xrTableRow95.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell175});
            this.xrTableRow95.Name = "xrTableRow95";
            this.xrTableRow95.Weight = 1;
            // 
            // xrTableCell175
            // 
            this.xrTableCell175.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell175.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell175.Name = "xrTableCell175";
            this.xrTableCell175.StylePriority.UseFont = false;
            this.xrTableCell175.StylePriority.UseForeColor = false;
            this.xrTableCell175.Text = "Debt Review Status";
            this.xrTableCell175.Weight = 3;
            // 
            // EnquiryHistory
            // 
            this.EnquiryHistory.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail22,
            this.GroupHeader22});
            this.EnquiryHistory.DataMember = "ConsumerEnquiryHistory";
            this.EnquiryHistory.Level = 22;
            this.EnquiryHistory.Name = "EnquiryHistory";
            // 
            // Detail22
            // 
            this.Detail22.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblEnquiryD});
            this.Detail22.Height = 25;
            this.Detail22.Name = "Detail22";
            // 
            // tblEnquiryD
            // 
            this.tblEnquiryD.Location = new System.Drawing.Point(8, 0);
            this.tblEnquiryD.Name = "tblEnquiryD";
            this.tblEnquiryD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow101});
            this.tblEnquiryD.Size = new System.Drawing.Size(800, 25);
            // 
            // xrTableRow101
            // 
            this.xrTableRow101.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblEnqDateD,
            this.tblsubscribernameD,
            this.lblbusTypeD});
            this.xrTableRow101.Name = "xrTableRow101";
            this.xrTableRow101.Weight = 1;
            // 
            // lblEnqDateD
            // 
            this.lblEnqDateD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerEnquiryHistory.EnquiryDate", "")});
            this.lblEnqDateD.Name = "lblEnqDateD";
            this.lblEnqDateD.Text = "lblEnqDateD";
            this.lblEnqDateD.Weight = 0.34375;
            // 
            // tblsubscribernameD
            // 
            this.tblsubscribernameD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerEnquiryHistory.SubscriberName", "")});
            this.tblsubscribernameD.Name = "tblsubscribernameD";
            this.tblsubscribernameD.Text = "tblsubscribernameD";
            this.tblsubscribernameD.Weight = 1.28125;
            // 
            // lblbusTypeD
            // 
            this.lblbusTypeD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerEnquiryHistory.SubscriberBusinessTypeDesc", "")});
            this.lblbusTypeD.Name = "lblbusTypeD";
            this.lblbusTypeD.Text = "lblbusTypeD";
            this.lblbusTypeD.Weight = 1.375;
            // 
            // GroupHeader22
            // 
            this.GroupHeader22.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblEnquiryC,
            this.tblEnquiryH});
            this.GroupHeader22.Height = 75;
            this.GroupHeader22.Name = "GroupHeader22";
            // 
            // tblEnquiryC
            // 
            this.tblEnquiryC.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tblEnquiryC.Location = new System.Drawing.Point(8, 42);
            this.tblEnquiryC.Name = "tblEnquiryC";
            this.tblEnquiryC.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow100});
            this.tblEnquiryC.Size = new System.Drawing.Size(800, 33);
            // 
            // xrTableRow100
            // 
            this.xrTableRow100.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tblEnqDate,
            this.tblsubscribername,
            this.lblbusType});
            this.xrTableRow100.Name = "xrTableRow100";
            this.xrTableRow100.Weight = 1;
            // 
            // tblEnqDate
            // 
            this.tblEnqDate.CanGrow = false;
            this.tblEnqDate.Name = "tblEnqDate";
            this.tblEnqDate.Text = "Enquiry Date";
            this.tblEnqDate.Weight = 0.34375;
            // 
            // tblsubscribername
            // 
            this.tblsubscribername.CanGrow = false;
            this.tblsubscribername.Name = "tblsubscribername";
            this.tblsubscribername.Text = "Name Of Credit Grantor";
            this.tblsubscribername.Weight = 1.28125;
            // 
            // lblbusType
            // 
            this.lblbusType.CanGrow = false;
            this.lblbusType.Name = "lblbusType";
            this.lblbusType.Text = "Type/Category of Credit Grantor";
            this.lblbusType.Weight = 1.375;
            // 
            // tblEnquiryH
            // 
            this.tblEnquiryH.ForeColor = System.Drawing.Color.Black;
            this.tblEnquiryH.Location = new System.Drawing.Point(8, 8);
            this.tblEnquiryH.Name = "tblEnquiryH";
            this.tblEnquiryH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow99});
            this.tblEnquiryH.Size = new System.Drawing.Size(433, 25);
            this.tblEnquiryH.StylePriority.UseForeColor = false;
            // 
            // xrTableRow99
            // 
            this.xrTableRow99.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell182});
            this.xrTableRow99.Name = "xrTableRow99";
            this.xrTableRow99.Weight = 1;
            // 
            // xrTableCell182
            // 
            this.xrTableCell182.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell182.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell182.Name = "xrTableCell182";
            this.xrTableCell182.StylePriority.UseFont = false;
            this.xrTableCell182.StylePriority.UseForeColor = false;
            this.xrTableCell182.Text = "Trace Enquiry History";
            this.xrTableCell182.Weight = 3;
            // 
            // propertyInfo
            // 
            this.propertyInfo.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail23,
            this.GroupHeader23});
            this.propertyInfo.DataMember = "ConsumerPropertyInformation";
            this.propertyInfo.Level = 23;
            this.propertyInfo.Name = "propertyInfo";
            // 
            // Detail23
            // 
            this.Detail23.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblPropInterests,
            this.tblPropInterestH});
            this.Detail23.Height = 204;
            this.Detail23.Name = "Detail23";
            // 
            // tblPropInterests
            // 
            this.tblPropInterests.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblPropInterests.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblPropInterests.Location = new System.Drawing.Point(8, 50);
            this.tblPropInterests.Name = "tblPropInterests";
            this.tblPropInterests.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow120,
            this.xrTableRow125,
            this.xrTableRow124,
            this.xrTableRow123,
            this.xrTableRow122,
            this.xrTableRow121});
            this.tblPropInterests.Size = new System.Drawing.Size(800, 145);
            this.tblPropInterests.StylePriority.UseBorderColor = false;
            this.tblPropInterests.StylePriority.UseBorders = false;
            // 
            // xrTableRow120
            // 
            this.xrTableRow120.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDeedNo,
            this.lblDDeedNo,
            this.lblsiteno,
            this.lblDsiteno});
            this.xrTableRow120.Name = "xrTableRow120";
            this.xrTableRow120.Weight = 1;
            // 
            // lblDeedNo
            // 
            this.lblDeedNo.Name = "lblDeedNo";
            this.lblDeedNo.Text = "Title Deed number";
            this.lblDeedNo.Weight = 0.625;
            // 
            // lblDDeedNo
            // 
            this.lblDDeedNo.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.TitleDeedNo", "")});
            this.lblDDeedNo.Name = "lblDDeedNo";
            this.lblDDeedNo.Text = "lblDDeedNo";
            this.lblDDeedNo.Weight = 0.90625;
            // 
            // lblsiteno
            // 
            this.lblsiteno.Name = "lblsiteno";
            this.lblsiteno.Text = "Erf/Site No.";
            this.lblsiteno.Weight = 0.59375;
            // 
            // lblDsiteno
            // 
            this.lblDsiteno.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.ErfNo", "")});
            this.lblDsiteno.Name = "lblDsiteno";
            this.lblDsiteno.Text = "lblDsiteno";
            this.lblDsiteno.Weight = 0.875;
            // 
            // xrTableRow125
            // 
            this.xrTableRow125.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDeedsoffice,
            this.lblDDeedsoffice,
            this.lblPhyadd,
            this.lblDPhyadd});
            this.xrTableRow125.Name = "xrTableRow125";
            this.xrTableRow125.Weight = 1;
            // 
            // lblDeedsoffice
            // 
            this.lblDeedsoffice.Name = "lblDeedsoffice";
            this.lblDeedsoffice.Text = "Deeds Office";
            this.lblDeedsoffice.Weight = 0.625;
            // 
            // lblDDeedsoffice
            // 
            this.lblDDeedsoffice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.DeedsOffice", "")});
            this.lblDDeedsoffice.Name = "lblDDeedsoffice";
            this.lblDDeedsoffice.Text = "lblDDeedsoffice";
            this.lblDDeedsoffice.Weight = 0.90625;
            // 
            // lblPhyadd
            // 
            this.lblPhyadd.Name = "lblPhyadd";
            this.lblPhyadd.Text = "Physical Address";
            this.lblPhyadd.Weight = 0.59375;
            // 
            // lblDPhyadd
            // 
            this.lblDPhyadd.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.PhysicalAddress", "")});
            this.lblDPhyadd.Name = "lblDPhyadd";
            this.lblDPhyadd.Text = "lblDPhyadd";
            this.lblDPhyadd.Weight = 0.875;
            // 
            // xrTableRow124
            // 
            this.xrTableRow124.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblPropType,
            this.lblDPropType,
            this.lblSize,
            this.lblDSize});
            this.xrTableRow124.Name = "xrTableRow124";
            this.xrTableRow124.Weight = 1;
            // 
            // lblPropType
            // 
            this.lblPropType.Name = "lblPropType";
            this.lblPropType.Text = "Property Type";
            this.lblPropType.Weight = 0.625;
            // 
            // lblDPropType
            // 
            this.lblDPropType.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.PropertyTypeDesc", "")});
            this.lblDPropType.Name = "lblDPropType";
            this.lblDPropType.Text = "lblDPropType";
            this.lblDPropType.Weight = 0.90625;
            // 
            // lblSize
            // 
            this.lblSize.Name = "lblSize";
            this.lblSize.Text = "Extent / Size";
            this.lblSize.Weight = 0.59375;
            // 
            // lblDSize
            // 
            this.lblDSize.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.ErfSize", "")});
            this.lblDSize.Name = "lblDSize";
            this.lblDSize.Text = "lblDSize";
            this.lblDSize.Weight = 0.875;
            // 
            // xrTableRow123
            // 
            this.xrTableRow123.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblPurchasedate,
            this.lblDPurchasedate,
            this.lblPurchaseprice,
            this.lblDPurchaseprice});
            this.xrTableRow123.Name = "xrTableRow123";
            this.xrTableRow123.Weight = 1;
            // 
            // lblPurchasedate
            // 
            this.lblPurchasedate.Name = "lblPurchasedate";
            this.lblPurchasedate.Text = "Purchase Date";
            this.lblPurchasedate.Weight = 0.625;
            // 
            // lblDPurchasedate
            // 
            this.lblDPurchasedate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.PurchaseDate", "")});
            this.lblDPurchasedate.Name = "lblDPurchasedate";
            this.lblDPurchasedate.Text = "lblDPurchasedate";
            this.lblDPurchasedate.Weight = 0.90625;
            // 
            // lblPurchaseprice
            // 
            this.lblPurchaseprice.Name = "lblPurchaseprice";
            this.lblPurchaseprice.Text = "Purchase price";
            this.lblPurchaseprice.Weight = 0.59375;
            // 
            // lblDPurchaseprice
            // 
            this.lblDPurchaseprice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.PurchasePriceAmt", "")});
            this.lblDPurchaseprice.Name = "lblDPurchaseprice";
            this.lblDPurchaseprice.Text = "lblDPurchaseprice";
            this.lblDPurchaseprice.Weight = 0.875;
            // 
            // xrTableRow122
            // 
            this.xrTableRow122.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblshareperc,
            this.lblDshareperc,
            this.lblHolder,
            this.lblDHolder});
            this.xrTableRow122.Name = "xrTableRow122";
            this.xrTableRow122.Weight = 1;
            // 
            // lblshareperc
            // 
            this.lblshareperc.Name = "lblshareperc";
            this.lblshareperc.Text = "% Ownership";
            this.lblshareperc.Weight = 0.625;
            // 
            // lblDshareperc
            // 
            this.lblDshareperc.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.BuyerSharePerc", "")});
            this.lblDshareperc.Name = "lblDshareperc";
            this.lblDshareperc.Text = "lblDshareperc";
            this.lblDshareperc.Weight = 0.90625;
            // 
            // lblHolder
            // 
            this.lblHolder.Name = "lblHolder";
            this.lblHolder.Text = "Bond Holder";
            this.lblHolder.Weight = 0.59375;
            // 
            // lblDHolder
            // 
            this.lblDHolder.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.BondHolderName", "")});
            this.lblDHolder.Name = "lblDHolder";
            this.lblDHolder.Text = "lblDHolder";
            this.lblDHolder.Weight = 0.875;
            // 
            // xrTableRow121
            // 
            this.xrTableRow121.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblBondNo,
            this.lblDBondNo,
            this.lblBondAmount,
            this.lblDBondAmount});
            this.xrTableRow121.Name = "xrTableRow121";
            this.xrTableRow121.Weight = 1;
            // 
            // lblBondNo
            // 
            this.lblBondNo.Name = "lblBondNo";
            this.lblBondNo.Text = "Bond Number";
            this.lblBondNo.Weight = 0.625;
            // 
            // lblDBondNo
            // 
            this.lblDBondNo.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.BondAccountNo", "")});
            this.lblDBondNo.Name = "lblDBondNo";
            this.lblDBondNo.Text = "lblDBondNo";
            this.lblDBondNo.Weight = 0.90625;
            // 
            // lblBondAmount
            // 
            this.lblBondAmount.Name = "lblBondAmount";
            this.lblBondAmount.Text = "Bond Amount";
            this.lblBondAmount.Weight = 0.59375;
            // 
            // lblDBondAmount
            // 
            this.lblDBondAmount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.BondAmt", "")});
            this.lblDBondAmount.Name = "lblDBondAmount";
            this.lblDBondAmount.Text = "lblDBondAmount";
            this.lblDBondAmount.Weight = 0.875;
            // 
            // tblPropInterestH
            // 
            this.tblPropInterestH.ForeColor = System.Drawing.Color.Black;
            this.tblPropInterestH.Location = new System.Drawing.Point(8, 17);
            this.tblPropInterestH.Name = "tblPropInterestH";
            this.tblPropInterestH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow119});
            this.tblPropInterestH.Size = new System.Drawing.Size(433, 25);
            this.tblPropInterestH.StylePriority.UseForeColor = false;
            // 
            // xrTableRow119
            // 
            this.xrTableRow119.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell281});
            this.xrTableRow119.Name = "xrTableRow119";
            this.xrTableRow119.Weight = 1;
            // 
            // xrTableCell281
            // 
            this.xrTableCell281.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.DisplayText", "")});
            this.xrTableCell281.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell281.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell281.Name = "xrTableCell281";
            this.xrTableCell281.StylePriority.UseFont = false;
            this.xrTableCell281.StylePriority.UseForeColor = false;
            xrSummary1.FormatString = "Property Interest {0}";
            xrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.Count;
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell281.Summary = xrSummary1;
            this.xrTableCell281.Text = "xrTableCell281";
            this.xrTableCell281.Weight = 3;
            // 
            // GroupHeader23
            // 
            this.GroupHeader23.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblPropInterestsH});
            this.GroupHeader23.Height = 46;
            this.GroupHeader23.Name = "GroupHeader23";
            // 
            // tblPropInterestsH
            // 
            this.tblPropInterestsH.ForeColor = System.Drawing.Color.Black;
            this.tblPropInterestsH.Location = new System.Drawing.Point(8, 8);
            this.tblPropInterestsH.Name = "tblPropInterestsH";
            this.tblPropInterestsH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow118});
            this.tblPropInterestsH.Size = new System.Drawing.Size(433, 25);
            this.tblPropInterestsH.StylePriority.UseForeColor = false;
            // 
            // xrTableRow118
            // 
            this.xrTableRow118.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell280});
            this.xrTableRow118.Name = "xrTableRow118";
            this.xrTableRow118.Weight = 1;
            // 
            // xrTableCell280
            // 
            this.xrTableCell280.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell280.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell280.Name = "xrTableCell280";
            this.xrTableCell280.StylePriority.UseFont = false;
            this.xrTableCell280.StylePriority.UseForeColor = false;
            this.xrTableCell280.Text = "Property Interests";
            this.xrTableCell280.Weight = 3;
            // 
            // DirectorshipLink
            // 
            this.DirectorshipLink.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail24,
            this.GroupHeader27});
            this.DirectorshipLink.DataMember = "ConsumerDirectorShipLink";
            this.DirectorshipLink.Level = 24;
            this.DirectorshipLink.Name = "DirectorshipLink";
            // 
            // Detail24
            // 
            this.Detail24.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblDirInfo,
            this.tblDirectorlinkH});
            this.Detail24.Height = 151;
            this.Detail24.Name = "Detail24";
            // 
            // tblDirInfo
            // 
            this.tblDirInfo.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblDirInfo.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblDirInfo.Location = new System.Drawing.Point(8, 42);
            this.tblDirInfo.Name = "tblDirInfo";
            this.tblDirInfo.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow113,
            this.xrTableRow116,
            this.xrTableRow117,
            this.xrTableRow115});
            this.tblDirInfo.Size = new System.Drawing.Size(792, 97);
            this.tblDirInfo.StylePriority.UseBorderColor = false;
            this.tblDirInfo.StylePriority.UseBorders = false;
            // 
            // xrTableRow113
            // 
            this.xrTableRow113.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCurrentPost,
            this.lblDCurrentPost,
            this.lblInceptionDate,
            this.lblDAppDate});
            this.xrTableRow113.Name = "xrTableRow113";
            this.xrTableRow113.Weight = 1;
            // 
            // lblCurrentPost
            // 
            this.lblCurrentPost.Name = "lblCurrentPost";
            this.lblCurrentPost.Text = "Current Post";
            this.lblCurrentPost.Weight = 1;
            // 
            // lblDCurrentPost
            // 
            this.lblDCurrentPost.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.DirectorDesignationDesc", "")});
            this.lblDCurrentPost.Name = "lblDCurrentPost";
            this.lblDCurrentPost.Text = "lblDCurrentPost";
            this.lblDCurrentPost.Weight = 0.67045454545454541;
            // 
            // lblInceptionDate
            // 
            this.lblInceptionDate.Name = "lblInceptionDate";
            this.lblInceptionDate.Text = "Date of Inception";
            this.lblInceptionDate.Weight = 0.63636363636363646;
            // 
            // lblDAppDate
            // 
            this.lblDAppDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.AppointmentDate", "")});
            this.lblDAppDate.Name = "lblDAppDate";
            this.lblDAppDate.Text = "lblDAppDate";
            this.lblDAppDate.Weight = 0.69318181818181812;
            // 
            // xrTableRow116
            // 
            this.xrTableRow116.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCompanyName,
            this.lblDCompanyName,
            this.lblRegNo,
            this.lblDRegNo});
            this.xrTableRow116.Name = "xrTableRow116";
            this.xrTableRow116.Weight = 1;
            // 
            // lblCompanyName
            // 
            this.lblCompanyName.Name = "lblCompanyName";
            this.lblCompanyName.Text = "Company Name";
            this.lblCompanyName.Weight = 1;
            // 
            // lblDCompanyName
            // 
            this.lblDCompanyName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.CommercialName", "")});
            this.lblDCompanyName.Name = "lblDCompanyName";
            this.lblDCompanyName.Text = "lblDCompanyName";
            this.lblDCompanyName.Weight = 0.67045454545454541;
            // 
            // lblRegNo
            // 
            this.lblRegNo.Name = "lblRegNo";
            this.lblRegNo.Text = "Company Reg No.";
            this.lblRegNo.Weight = 0.63636363636363646;
            // 
            // lblDRegNo
            // 
            this.lblDRegNo.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.RegistrationNo", "")});
            this.lblDRegNo.Name = "lblDRegNo";
            this.lblDRegNo.Text = "lblDRegNo";
            this.lblDRegNo.Weight = 0.69318181818181812;
            // 
            // xrTableRow117
            // 
            this.xrTableRow117.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCompanyAdd,
            this.lblDPhysicalAdd,
            this.lblPhoneNo,
            this.lblDPhoneNo});
            this.xrTableRow117.Name = "xrTableRow117";
            this.xrTableRow117.Weight = 1;
            // 
            // lblCompanyAdd
            // 
            this.lblCompanyAdd.Name = "lblCompanyAdd";
            this.lblCompanyAdd.Text = "Company Address";
            this.lblCompanyAdd.Weight = 1;
            // 
            // lblDPhysicalAdd
            // 
            this.lblDPhysicalAdd.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.PhysicalAddress", "")});
            this.lblDPhysicalAdd.Name = "lblDPhysicalAdd";
            this.lblDPhysicalAdd.Text = "lblDPhysicalAdd";
            this.lblDPhysicalAdd.Weight = 0.67045454545454541;
            // 
            // lblPhoneNo
            // 
            this.lblPhoneNo.Name = "lblPhoneNo";
            this.lblPhoneNo.Text = "Company Phone No.";
            this.lblPhoneNo.Weight = 0.63636363636363646;
            // 
            // lblDPhoneNo
            // 
            this.lblDPhoneNo.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.TelephoneNo", "")});
            this.lblDPhoneNo.Name = "lblDPhoneNo";
            this.lblDPhoneNo.Text = "lblDPhoneNo";
            this.lblDPhoneNo.Weight = 0.69318181818181812;
            // 
            // xrTableRow115
            // 
            this.xrTableRow115.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblIndustryCatg,
            this.lblDIndCateg});
            this.xrTableRow115.Name = "xrTableRow115";
            this.xrTableRow115.Weight = 1;
            // 
            // lblIndustryCatg
            // 
            this.lblIndustryCatg.Name = "lblIndustryCatg";
            this.lblIndustryCatg.Text = "Industry category";
            this.lblIndustryCatg.Weight = 1;
            // 
            // lblDIndCateg
            // 
            this.lblDIndCateg.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.SICDesc", "")});
            this.lblDIndCateg.Name = "lblDIndCateg";
            this.lblDIndCateg.Text = "lblDIndCateg";
            this.lblDIndCateg.Weight = 2;
            // 
            // tblDirectorlinkH
            // 
            this.tblDirectorlinkH.ForeColor = System.Drawing.Color.Black;
            this.tblDirectorlinkH.Location = new System.Drawing.Point(8, 8);
            this.tblDirectorlinkH.Name = "tblDirectorlinkH";
            this.tblDirectorlinkH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow114});
            this.tblDirectorlinkH.Size = new System.Drawing.Size(625, 25);
            this.tblDirectorlinkH.StylePriority.UseForeColor = false;
            // 
            // xrTableRow114
            // 
            this.xrTableRow114.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell274});
            this.xrTableRow114.Name = "xrTableRow114";
            this.xrTableRow114.Weight = 1;
            // 
            // xrTableCell274
            // 
            this.xrTableCell274.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.DisplayText", "")});
            this.xrTableCell274.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell274.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell274.Name = "xrTableCell274";
            this.xrTableCell274.StylePriority.UseFont = false;
            this.xrTableCell274.StylePriority.UseForeColor = false;
            xrSummary2.FormatString = "Directorship Link {0}";
            xrSummary2.Func = DevExpress.XtraReports.UI.SummaryFunc.Count;
            xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell274.Summary = xrSummary2;
            this.xrTableCell274.Text = "xrTableCell274";
            this.xrTableCell274.Weight = 3;
            // 
            // GroupHeader27
            // 
            this.GroupHeader27.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblDirectorlinksH});
            this.GroupHeader27.Height = 40;
            this.GroupHeader27.Name = "GroupHeader27";
            // 
            // tblDirectorlinksH
            // 
            this.tblDirectorlinksH.ForeColor = System.Drawing.Color.Black;
            this.tblDirectorlinksH.Location = new System.Drawing.Point(8, 8);
            this.tblDirectorlinksH.Name = "tblDirectorlinksH";
            this.tblDirectorlinksH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow112});
            this.tblDirectorlinksH.Size = new System.Drawing.Size(433, 25);
            this.tblDirectorlinksH.StylePriority.UseForeColor = false;
            // 
            // xrTableRow112
            // 
            this.xrTableRow112.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell272});
            this.xrTableRow112.Name = "xrTableRow112";
            this.xrTableRow112.Weight = 1;
            // 
            // xrTableCell272
            // 
            this.xrTableCell272.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell272.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell272.Name = "xrTableCell272";
            this.xrTableCell272.StylePriority.UseFont = false;
            this.xrTableCell272.StylePriority.UseForeColor = false;
            this.xrTableCell272.Text = "Directorship Links";
            this.xrTableCell272.Weight = 3;
            // 
            // TelephoneLinkageHome
            // 
            this.TelephoneLinkageHome.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail25,
            this.GroupHeader25});
            this.TelephoneLinkageHome.DataMember = "TelephoneLinkageHome";
            this.TelephoneLinkageHome.Level = 25;
            this.TelephoneLinkageHome.Name = "TelephoneLinkageHome";
            // 
            // Detail25
            // 
            this.Detail25.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblLinkagesHD});
            this.Detail25.Height = 25;
            this.Detail25.Name = "Detail25";
            // 
            // tblLinkagesHD
            // 
            this.tblLinkagesHD.Location = new System.Drawing.Point(8, 0);
            this.tblLinkagesHD.Name = "tblLinkagesHD";
            this.tblLinkagesHD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow105});
            this.tblLinkagesHD.Size = new System.Drawing.Size(800, 25);
            // 
            // xrTableRow105
            // 
            this.xrTableRow105.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblHNameD,
            this.lblHSurD,
            this.lblHHomenoD,
            this.lblHWorkNoD,
            this.lblHCellNoD});
            this.xrTableRow105.Name = "xrTableRow105";
            this.xrTableRow105.Weight = 1;
            // 
            // lblHNameD
            // 
            this.lblHNameD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TelephoneLinkageHome.ConsumerName", "")});
            this.lblHNameD.Name = "lblHNameD";
            this.lblHNameD.Text = "lblHNameD";
            this.lblHNameD.Weight = 0.625;
            // 
            // lblHSurD
            // 
            this.lblHSurD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TelephoneLinkageHome.Surname", "")});
            this.lblHSurD.Name = "lblHSurD";
            this.lblHSurD.Text = "lblHSurD";
            this.lblHSurD.Weight = 0.625;
            // 
            // lblHHomenoD
            // 
            this.lblHHomenoD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TelephoneLinkageHome.HomeTelephone", "")});
            this.lblHHomenoD.Name = "lblHHomenoD";
            this.lblHHomenoD.Text = "lblHHomenoD";
            this.lblHHomenoD.Weight = 0.56125;
            // 
            // lblHWorkNoD
            // 
            this.lblHWorkNoD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TelephoneLinkageHome.WorkTelephone", "")});
            this.lblHWorkNoD.Name = "lblHWorkNoD";
            this.lblHWorkNoD.Text = "lblHWorkNoD";
            this.lblHWorkNoD.Weight = 0.595;
            // 
            // lblHCellNoD
            // 
            this.lblHCellNoD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TelephoneLinkageHome.CellularNo", "")});
            this.lblHCellNoD.Name = "lblHCellNoD";
            this.lblHCellNoD.Text = "lblHCellNoD";
            this.lblHCellNoD.Weight = 0.59375;
            // 
            // GroupHeader25
            // 
            this.GroupHeader25.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblLikagesHC,
            this.tblLinkagesHome,
            this.tblLinkages});
            this.GroupHeader25.Height = 117;
            this.GroupHeader25.Name = "GroupHeader25";
            // 
            // tblLikagesHC
            // 
            this.tblLikagesHC.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tblLikagesHC.Location = new System.Drawing.Point(8, 92);
            this.tblLikagesHC.Name = "tblLikagesHC";
            this.tblLikagesHC.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow104});
            this.tblLikagesHC.Size = new System.Drawing.Size(800, 25);
            // 
            // xrTableRow104
            // 
            this.xrTableRow104.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblHName,
            this.lblHSur,
            this.lblHHomeno,
            this.lblHWorkNo,
            this.lblHCellNo});
            this.xrTableRow104.Name = "xrTableRow104";
            this.xrTableRow104.Weight = 1;
            // 
            // lblHName
            // 
            this.lblHName.CanGrow = false;
            this.lblHName.Name = "lblHName";
            this.lblHName.Text = "Name";
            this.lblHName.Weight = 0.6275;
            // 
            // lblHSur
            // 
            this.lblHSur.CanGrow = false;
            this.lblHSur.Name = "lblHSur";
            this.lblHSur.Text = "Surname";
            this.lblHSur.Weight = 0.6275;
            // 
            // lblHHomeno
            // 
            this.lblHHomeno.CanGrow = false;
            this.lblHHomeno.Name = "lblHHomeno";
            this.lblHHomeno.Text = "Home Number";
            this.lblHHomeno.Weight = 0.56375;
            // 
            // lblHWorkNo
            // 
            this.lblHWorkNo.CanGrow = false;
            this.lblHWorkNo.Name = "lblHWorkNo";
            this.lblHWorkNo.Text = "Work Number";
            this.lblHWorkNo.Weight = 0.5975;
            // 
            // lblHCellNo
            // 
            this.lblHCellNo.CanGrow = false;
            this.lblHCellNo.Name = "lblHCellNo";
            this.lblHCellNo.Text = "Cell Number";
            this.lblHCellNo.Weight = 0.58375;
            // 
            // tblLinkagesHome
            // 
            this.tblLinkagesHome.ForeColor = System.Drawing.Color.Black;
            this.tblLinkagesHome.Location = new System.Drawing.Point(8, 42);
            this.tblLinkagesHome.Name = "tblLinkagesHome";
            this.tblLinkagesHome.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow103});
            this.tblLinkagesHome.Size = new System.Drawing.Size(625, 25);
            this.tblLinkagesHome.StylePriority.UseForeColor = false;
            // 
            // xrTableRow103
            // 
            this.xrTableRow103.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell239});
            this.xrTableRow103.Name = "xrTableRow103";
            this.xrTableRow103.Weight = 1;
            // 
            // xrTableCell239
            // 
            this.xrTableCell239.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.HomeTelephoneNo", "Latest Home Telephone Link to {0}")});
            this.xrTableCell239.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell239.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell239.Name = "xrTableCell239";
            this.xrTableCell239.StylePriority.UseFont = false;
            this.xrTableCell239.StylePriority.UseForeColor = false;
            this.xrTableCell239.Text = "xrTableCell239";
            this.xrTableCell239.Weight = 3;
            // 
            // tblLinkages
            // 
            this.tblLinkages.ForeColor = System.Drawing.Color.Black;
            this.tblLinkages.Location = new System.Drawing.Point(8, 0);
            this.tblLinkages.Name = "tblLinkages";
            this.tblLinkages.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow102});
            this.tblLinkages.Size = new System.Drawing.Size(433, 25);
            this.tblLinkages.StylePriority.UseForeColor = false;
            // 
            // xrTableRow102
            // 
            this.xrTableRow102.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell238});
            this.xrTableRow102.Name = "xrTableRow102";
            this.xrTableRow102.Weight = 1;
            // 
            // xrTableCell238
            // 
            this.xrTableCell238.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell238.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell238.Name = "xrTableCell238";
            this.xrTableCell238.StylePriority.UseFont = false;
            this.xrTableCell238.StylePriority.UseForeColor = false;
            this.xrTableCell238.Text = "LINKAGES";
            this.xrTableCell238.Weight = 3;
            // 
            // TelephoneLinkageWork
            // 
            this.TelephoneLinkageWork.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail26,
            this.GroupHeader24});
            this.TelephoneLinkageWork.DataMember = "TelephoneLinkageWork";
            this.TelephoneLinkageWork.Level = 26;
            this.TelephoneLinkageWork.Name = "TelephoneLinkageWork";
            // 
            // Detail26
            // 
            this.Detail26.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblLinkagesWD});
            this.Detail26.Height = 25;
            this.Detail26.Name = "Detail26";
            // 
            // tblLinkagesWD
            // 
            this.tblLinkagesWD.Location = new System.Drawing.Point(8, 0);
            this.tblLinkagesWD.Name = "tblLinkagesWD";
            this.tblLinkagesWD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow108});
            this.tblLinkagesWD.Size = new System.Drawing.Size(800, 25);
            // 
            // xrTableRow108
            // 
            this.xrTableRow108.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblWNameD,
            this.lblWSurD,
            this.lblWHomenoD,
            this.lblWWorkNoD,
            this.lblWCellNoD});
            this.xrTableRow108.Name = "xrTableRow108";
            this.xrTableRow108.Weight = 1;
            // 
            // lblWNameD
            // 
            this.lblWNameD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TelephoneLinkageWork.ConsumerName", "")});
            this.lblWNameD.Name = "lblWNameD";
            this.lblWNameD.Text = "lblWNameD";
            this.lblWNameD.Weight = 0.625;
            // 
            // lblWSurD
            // 
            this.lblWSurD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TelephoneLinkageWork.Surname", "")});
            this.lblWSurD.Name = "lblWSurD";
            this.lblWSurD.Text = "lblWSurD";
            this.lblWSurD.Weight = 0.625;
            // 
            // lblWHomenoD
            // 
            this.lblWHomenoD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TelephoneLinkageWork.HomeTelephone", "")});
            this.lblWHomenoD.Name = "lblWHomenoD";
            this.lblWHomenoD.Text = "lblWHomenoD";
            this.lblWHomenoD.Weight = 0.56125;
            // 
            // lblWWorkNoD
            // 
            this.lblWWorkNoD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TelephoneLinkageWork.WorkTelephone", "")});
            this.lblWWorkNoD.Name = "lblWWorkNoD";
            this.lblWWorkNoD.Text = "lblWWorkNoD";
            this.lblWWorkNoD.Weight = 0.595;
            // 
            // lblWCellNoD
            // 
            this.lblWCellNoD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TelephoneLinkageWork.CellularNo", "")});
            this.lblWCellNoD.Name = "lblWCellNoD";
            this.lblWCellNoD.Text = "lblWCellNoD";
            this.lblWCellNoD.Weight = 0.59375;
            // 
            // GroupHeader24
            // 
            this.GroupHeader24.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblLikagesWC,
            this.tblLinkagesWork});
            this.GroupHeader24.Height = 75;
            this.GroupHeader24.Name = "GroupHeader24";
            // 
            // tblLikagesWC
            // 
            this.tblLikagesWC.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tblLikagesWC.Location = new System.Drawing.Point(8, 50);
            this.tblLikagesWC.Name = "tblLikagesWC";
            this.tblLikagesWC.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow107});
            this.tblLikagesWC.Size = new System.Drawing.Size(800, 25);
            // 
            // xrTableRow107
            // 
            this.xrTableRow107.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblWName,
            this.lblWSur,
            this.lblWHomeno,
            this.lblWWorkNo,
            this.lblWCellNo});
            this.xrTableRow107.Name = "xrTableRow107";
            this.xrTableRow107.Weight = 1;
            // 
            // lblWName
            // 
            this.lblWName.CanGrow = false;
            this.lblWName.Name = "lblWName";
            this.lblWName.Text = "Name";
            this.lblWName.Weight = 0.6275;
            // 
            // lblWSur
            // 
            this.lblWSur.CanGrow = false;
            this.lblWSur.Name = "lblWSur";
            this.lblWSur.Text = "Surname";
            this.lblWSur.Weight = 0.6275;
            // 
            // lblWHomeno
            // 
            this.lblWHomeno.CanGrow = false;
            this.lblWHomeno.Name = "lblWHomeno";
            this.lblWHomeno.Text = "Home Number";
            this.lblWHomeno.Weight = 0.56375;
            // 
            // lblWWorkNo
            // 
            this.lblWWorkNo.CanGrow = false;
            this.lblWWorkNo.Name = "lblWWorkNo";
            this.lblWWorkNo.Text = "Work Number";
            this.lblWWorkNo.Weight = 0.5975;
            // 
            // lblWCellNo
            // 
            this.lblWCellNo.CanGrow = false;
            this.lblWCellNo.Name = "lblWCellNo";
            this.lblWCellNo.Text = "Cell Number";
            this.lblWCellNo.Weight = 0.58375;
            // 
            // tblLinkagesWork
            // 
            this.tblLinkagesWork.ForeColor = System.Drawing.Color.Black;
            this.tblLinkagesWork.Location = new System.Drawing.Point(8, 0);
            this.tblLinkagesWork.Name = "tblLinkagesWork";
            this.tblLinkagesWork.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow106});
            this.tblLinkagesWork.Size = new System.Drawing.Size(625, 25);
            this.tblLinkagesWork.StylePriority.UseForeColor = false;
            // 
            // xrTableRow106
            // 
            this.xrTableRow106.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell250});
            this.xrTableRow106.Name = "xrTableRow106";
            this.xrTableRow106.Weight = 1;
            // 
            // xrTableCell250
            // 
            this.xrTableCell250.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.WorkTelephoneNo", "Latest Work Telephone Link to {0}")});
            this.xrTableCell250.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell250.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell250.Name = "xrTableCell250";
            this.xrTableCell250.StylePriority.UseFont = false;
            this.xrTableCell250.StylePriority.UseForeColor = false;
            this.xrTableCell250.Text = "xrTableCell250";
            this.xrTableCell250.Weight = 3;
            // 
            // TelephoneLinkageCellular
            // 
            this.TelephoneLinkageCellular.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail27,
            this.GroupHeader26});
            this.TelephoneLinkageCellular.DataMember = "TelephoneLinkageCellular";
            this.TelephoneLinkageCellular.Level = 27;
            this.TelephoneLinkageCellular.Name = "TelephoneLinkageCellular";
            // 
            // Detail27
            // 
            this.Detail27.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblLinkagesCD});
            this.Detail27.Height = 25;
            this.Detail27.Name = "Detail27";
            // 
            // tblLinkagesCD
            // 
            this.tblLinkagesCD.Location = new System.Drawing.Point(8, 0);
            this.tblLinkagesCD.Name = "tblLinkagesCD";
            this.tblLinkagesCD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow111});
            this.tblLinkagesCD.Size = new System.Drawing.Size(800, 25);
            // 
            // xrTableRow111
            // 
            this.xrTableRow111.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCNameD,
            this.lblCSurD,
            this.lblCHomenoD,
            this.lblCWorkNoD,
            this.lblCellNoD});
            this.xrTableRow111.Name = "xrTableRow111";
            this.xrTableRow111.Weight = 1;
            // 
            // lblCNameD
            // 
            this.lblCNameD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TelephoneLinkageCellular.ConsumerName", "")});
            this.lblCNameD.Name = "lblCNameD";
            this.lblCNameD.Text = "xrTableCell256";
            this.lblCNameD.Weight = 0.625;
            // 
            // lblCSurD
            // 
            this.lblCSurD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TelephoneLinkageCellular.Surname", "")});
            this.lblCSurD.Name = "lblCSurD";
            this.lblCSurD.Text = "xrTableCell257";
            this.lblCSurD.Weight = 0.625;
            // 
            // lblCHomenoD
            // 
            this.lblCHomenoD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TelephoneLinkageCellular.HomeTelephone", "")});
            this.lblCHomenoD.Name = "lblCHomenoD";
            this.lblCHomenoD.Text = "xrTableCell258";
            this.lblCHomenoD.Weight = 0.56125;
            // 
            // lblCWorkNoD
            // 
            this.lblCWorkNoD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TelephoneLinkageCellular.WorkTelephone", "")});
            this.lblCWorkNoD.Name = "lblCWorkNoD";
            this.lblCWorkNoD.Text = "xrTableCell259";
            this.lblCWorkNoD.Weight = 0.595;
            // 
            // lblCellNoD
            // 
            this.lblCellNoD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TelephoneLinkageCellular.CellularNo", "")});
            this.lblCellNoD.Name = "lblCellNoD";
            this.lblCellNoD.Text = "xrTableCell260";
            this.lblCellNoD.Weight = 0.59375;
            // 
            // GroupHeader26
            // 
            this.GroupHeader26.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblLikagesCC,
            this.tblLinkagesCell});
            this.GroupHeader26.Height = 75;
            this.GroupHeader26.Name = "GroupHeader26";
            // 
            // tblLikagesCC
            // 
            this.tblLikagesCC.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.tblLikagesCC.Location = new System.Drawing.Point(8, 50);
            this.tblLikagesCC.Name = "tblLikagesCC";
            this.tblLikagesCC.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow110});
            this.tblLikagesCC.Size = new System.Drawing.Size(800, 25);
            // 
            // xrTableRow110
            // 
            this.xrTableRow110.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCName,
            this.lblCSur,
            this.lblCHomeno,
            this.lblCWorkNo,
            this.lblCellNo});
            this.xrTableRow110.Name = "xrTableRow110";
            this.xrTableRow110.Weight = 1;
            // 
            // lblCName
            // 
            this.lblCName.CanGrow = false;
            this.lblCName.Name = "lblCName";
            this.lblCName.Text = "Name";
            this.lblCName.Weight = 0.6275;
            // 
            // lblCSur
            // 
            this.lblCSur.CanGrow = false;
            this.lblCSur.Name = "lblCSur";
            this.lblCSur.Text = "Surname";
            this.lblCSur.Weight = 0.6275;
            // 
            // lblCHomeno
            // 
            this.lblCHomeno.CanGrow = false;
            this.lblCHomeno.Name = "lblCHomeno";
            this.lblCHomeno.Text = "Home Number";
            this.lblCHomeno.Weight = 0.56375;
            // 
            // lblCWorkNo
            // 
            this.lblCWorkNo.CanGrow = false;
            this.lblCWorkNo.Name = "lblCWorkNo";
            this.lblCWorkNo.Text = "Work Number";
            this.lblCWorkNo.Weight = 0.5975;
            // 
            // lblCellNo
            // 
            this.lblCellNo.CanGrow = false;
            this.lblCellNo.Name = "lblCellNo";
            this.lblCellNo.Text = "Cell Number";
            this.lblCellNo.Weight = 0.58375;
            // 
            // tblLinkagesCell
            // 
            this.tblLinkagesCell.ForeColor = System.Drawing.Color.Black;
            this.tblLinkagesCell.Location = new System.Drawing.Point(8, 0);
            this.tblLinkagesCell.Name = "tblLinkagesCell";
            this.tblLinkagesCell.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow109});
            this.tblLinkagesCell.Size = new System.Drawing.Size(625, 25);
            this.tblLinkagesCell.StylePriority.UseForeColor = false;
            // 
            // xrTableRow109
            // 
            this.xrTableRow109.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell261});
            this.xrTableRow109.Name = "xrTableRow109";
            this.xrTableRow109.Weight = 1;
            // 
            // xrTableCell261
            // 
            this.xrTableCell261.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.CellularNo", "Latest Cell Number Link to {0}")});
            this.xrTableCell261.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.xrTableCell261.ForeColor = System.Drawing.Color.DarkGray;
            this.xrTableCell261.Name = "xrTableCell261";
            this.xrTableCell261.StylePriority.UseFont = false;
            this.xrTableCell261.StylePriority.UseForeColor = false;
            this.xrTableCell261.Text = "xrTableCell250";
            this.xrTableCell261.Weight = 3;
            // 
            // MonthlyPayment
            // 
            this.MonthlyPayment.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail28});
            this.MonthlyPayment.DataMember = "Consumer24MonthlyPayment";
            this.MonthlyPayment.Level = 15;
            this.MonthlyPayment.Name = "MonthlyPayment";
            // 
            // Detail28
            // 
            this.Detail28.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblMonthlyPaymentD});
            this.Detail28.Height = 25;
            this.Detail28.Name = "Detail28";
            // 
            // tblMonthlyPaymentD
            // 
            this.tblMonthlyPaymentD.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.tblMonthlyPaymentD.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblMonthlyPaymentD.Location = new System.Drawing.Point(8, 0);
            this.tblMonthlyPaymentD.Name = "tblMonthlyPaymentD";
            this.tblMonthlyPaymentD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow79});
            this.tblMonthlyPaymentD.Size = new System.Drawing.Size(792, 25);
            this.tblMonthlyPaymentD.StylePriority.UseBorderColor = false;
            this.tblMonthlyPaymentD.StylePriority.UseBorders = false;
            this.tblMonthlyPaymentD.StylePriority.UseTextAlignment = false;
            this.tblMonthlyPaymentD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow79
            // 
            this.xrTableRow79.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblcompanyMPD,
            this.lblM01D,
            this.lblM02D,
            this.lblM03D,
            this.lblM04D,
            this.lblM05D,
            this.lblM06D,
            this.lblM07D,
            this.lblM08D,
            this.lblM09D,
            this.lblM10D,
            this.lblM11D,
            this.lblM12D,
            this.lblM13D,
            this.lblM14D,
            this.lblM15D,
            this.lblM16D,
            this.lblM17D,
            this.lblM18D,
            this.lblM19D,
            this.lblM20D,
            this.lblM21D,
            this.lblM22D,
            this.lblM23D,
            this.lblM24D});
            this.xrTableRow79.Name = "xrTableRow79";
            this.xrTableRow79.Weight = 1;
            // 
            // lblcompanyMPD
            // 
            this.lblcompanyMPD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.SubscriberName", "")});
            this.lblcompanyMPD.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblcompanyMPD.Name = "lblcompanyMPD";
            this.lblcompanyMPD.StylePriority.UseFont = false;
            this.lblcompanyMPD.StylePriority.UseTextAlignment = false;
            this.lblcompanyMPD.Text = "xrTableCell49";
            this.lblcompanyMPD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblcompanyMPD.Weight = 0.45036567069356459;
            // 
            // lblM01D
            // 
            this.lblM01D.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M01", "")});
            this.lblM01D.Name = "lblM01D";
            this.lblM01D.Weight = 0.18908346833704748;
            // 
            // lblM02D
            // 
            this.lblM02D.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M02", "")});
            this.lblM02D.Name = "lblM02D";
            this.lblM02D.Weight = 0.18935347576530615;
            // 
            // lblM03D
            // 
            this.lblM03D.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M03", "")});
            this.lblM03D.Name = "lblM03D";
            this.lblM03D.Weight = 0.18546715561224492;
            // 
            // lblM04D
            // 
            this.lblM04D.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M04", "")});
            this.lblM04D.Name = "lblM04D";
            this.lblM04D.Weight = 0.1869021045918369;
            // 
            // lblM05D
            // 
            this.lblM05D.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M05", "")});
            this.lblM05D.Name = "lblM05D";
            this.lblM05D.Text = "xrTableCell78";
            this.lblM05D.Weight = 0.18546715561224489;
            // 
            // lblM06D
            // 
            this.lblM06D.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M06", "")});
            this.lblM06D.Name = "lblM06D";
            this.lblM06D.Weight = 0.18391262755102042;
            // 
            // lblM07D
            // 
            this.lblM07D.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M07", "")});
            this.lblM07D.Name = "lblM07D";
            this.lblM07D.Weight = 0.18391262755102053;
            // 
            // lblM08D
            // 
            this.lblM08D.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M08", "")});
            this.lblM08D.Name = "lblM08D";
            this.lblM08D.Weight = 0.18558673469387751;
            // 
            // lblM09D
            // 
            this.lblM09D.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M09", "")});
            this.lblM09D.Name = "lblM09D";
            this.lblM09D.Weight = 0.18415178571428584;
            // 
            // lblM10D
            // 
            this.lblM10D.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M10", "")});
            this.lblM10D.Name = "lblM10D";
            this.lblM10D.Weight = 0.18534757653061221;
            // 
            // lblM11D
            // 
            this.lblM11D.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M11", "")});
            this.lblM11D.Name = "lblM11D";
            this.lblM11D.Weight = 0.18534757653061221;
            // 
            // lblM12D
            // 
            this.lblM12D.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M12", "")});
            this.lblM12D.Name = "lblM12D";
            this.lblM12D.Weight = 0.18534757653061229;
            // 
            // lblM13D
            // 
            this.lblM13D.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M13", "")});
            this.lblM13D.Name = "lblM13D";
            this.lblM13D.Weight = 0.18534757653061232;
            // 
            // lblM14D
            // 
            this.lblM14D.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M14", "")});
            this.lblM14D.Name = "lblM14D";
            this.lblM14D.Weight = 0.18534757653061224;
            // 
            // lblM15D
            // 
            this.lblM15D.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M15", "")});
            this.lblM15D.Name = "lblM15D";
            this.lblM15D.Weight = 0.18582589285714291;
            // 
            // lblM16D
            // 
            this.lblM16D.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M16", "")});
            this.lblM16D.Name = "lblM16D";
            this.lblM16D.Weight = 0.18343431122448989;
            // 
            // lblM17D
            // 
            this.lblM17D.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M17", "")});
            this.lblM17D.Name = "lblM17D";
            this.lblM17D.Weight = 0.18917410714285726;
            // 
            // lblM18D
            // 
            this.lblM18D.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M18", "")});
            this.lblM18D.Name = "lblM18D";
            this.lblM18D.Weight = 0.1889349489795919;
            // 
            // lblM19D
            // 
            this.lblM19D.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M19", "")});
            this.lblM19D.Name = "lblM19D";
            this.lblM19D.Weight = 0.18606505102040827;
            // 
            // lblM20D
            // 
            this.lblM20D.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M20", "")});
            this.lblM20D.Name = "lblM20D";
            this.lblM20D.Weight = 0.18893494897959184;
            // 
            // lblM21D
            // 
            this.lblM21D.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M21", "")});
            this.lblM21D.Name = "lblM21D";
            this.lblM21D.Weight = 0.18749999999999992;
            // 
            // lblM22D
            // 
            this.lblM22D.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M22", "")});
            this.lblM22D.Name = "lblM22D";
            this.lblM22D.Weight = 0.18749999999999994;
            // 
            // lblM23D
            // 
            this.lblM23D.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M23", "")});
            this.lblM23D.Name = "lblM23D";
            this.lblM23D.Weight = 0.1879783163265307;
            // 
            // lblM24D
            // 
            this.lblM24D.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblM24D.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M24", "")});
            this.lblM24D.Name = "lblM24D";
            this.lblM24D.StylePriority.UseBorders = false;
            this.lblM24D.Weight = 0.18845663265306129;
            // 
            // ConsumerTraceReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.PageFooter,
            this.ReportHeader,
            this.GroupHeader1,
            this.ConsumerDetail,
            this.FraudIndicatorSummary,
            this.Scoring,
            this.DebtSummary,
            this.AccountGoodBadSummary,
            this.NameConfirmation,
            this.AddressConfirmation,
            this.ContactNoConfirmation,
            this.EmploymentDetailConfirmation,
            this.NameHistory,
            this.AddressHistory,
            this.TelephoneHistory,
            this.EmploymentHistory,
            this.AccountStatus,
            this.MonthlyPaymentHeader,
            this.Definition,
            this.AdverseInfo,
            this.Judgements,
            this.AdminOrders,
            this.Sequestration,
            this.DebtReviewStatus,
            this.EnquiryHistory,
            this.propertyInfo,
            this.DirectorshipLink,
            this.TelephoneLinkageHome,
            this.TelephoneLinkageWork,
            this.TelephoneLinkageCellular,
            this.MonthlyPayment});
            this.Margins = new System.Drawing.Printing.Margins(20, 20, 100, 100);
            this.Name = "ConsumerTraceReport";
            this.PageHeight = 1100;
            this.PageWidth = 850;
            this.RequestParameters = false;
            this.Version = "9.1";
            this.XmlDataPath = "C:\\ConsumerTraceReport.xml";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblPersonalDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonalDetailsSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblFraudIndSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblFraudSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblscrorevalues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblscoreColumns)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblScoreHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDebtSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDebtHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAccStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblCrAccStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblNameHistoryValues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblNameHistColumns)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblNameHistoryHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblpersonalConfirmationHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDetailedInfoHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblConfAddHistoryC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAddHistoryHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblCContactNoD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblCContactNoC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblCContactNoH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEmpHisD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEmpHisC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEmploymentHisH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblNamehistD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblNameHisC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblNameHistH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblConsInfoH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAddHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAddHC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAddHistoryH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblContactNoD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblContactNoC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblContactNoH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEmploymentD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEmploymentC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEmploymentH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAccStatusD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAccStatusC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAccStatusH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblMonthlyPaymentH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDefinitionD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDefinitionC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAdverseData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAdverseColumns)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAdverseH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDomainH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDJudg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblCJud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblJudgmentsH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDAO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblCAO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAOH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblSEQD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblSEQC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblSEQH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDbtReviewD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbldbtReviewH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEnquiryD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEnquiryC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEnquiryH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblPropInterests)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblPropInterestH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblPropInterestsH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDirInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDirectorlinkH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDirectorlinksH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblLinkagesHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblLikagesHC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblLinkagesHome)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblLinkages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblLinkagesWD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblLikagesWC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblLinkagesWork)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblLinkagesCD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblLikagesCC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblLinkagesCell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblMonthlyPaymentD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo2;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell lblHeaderText;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTable tblPersonalDetail;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.DetailReportBand ConsumerDetail;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.XRTable PersonalDetailsSummary;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell lblReferenceNo;
        private DevExpress.XtraReports.UI.XRTableCell lblRefnoValue;
        private DevExpress.XtraReports.UI.XRTableCell lblExtRefNo;
        private DevExpress.XtraReports.UI.XRTableCell lblExternalRefNoValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell lblIDNo;
        private DevExpress.XtraReports.UI.XRTableCell lblIDnoValue;
        private DevExpress.XtraReports.UI.XRTableCell lblPPNo;
        private DevExpress.XtraReports.UI.XRTableCell lblPassportnoValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell lblSurname;
        private DevExpress.XtraReports.UI.XRTableCell lblSurnameValue;
        private DevExpress.XtraReports.UI.XRTableCell lblResAdd;
        private DevExpress.XtraReports.UI.XRTableCell lblResAddValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell lblFirstname;
        private DevExpress.XtraReports.UI.XRTableCell lblFirstnameValue;
        private DevExpress.XtraReports.UI.XRTableCell lbl;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell lblSecondName;
        private DevExpress.XtraReports.UI.XRTableCell lblSecondnameValue;
        private DevExpress.XtraReports.UI.XRTableCell lblHomeTelNo;
        private DevExpress.XtraReports.UI.XRTableCell lblHomeTelNoValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell lblTitle;
        private DevExpress.XtraReports.UI.XRTableCell lbltitlevalue;
        private DevExpress.XtraReports.UI.XRTableCell lblPostalAdd;
        private DevExpress.XtraReports.UI.XRTableCell lblPostalAddvalue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell lblGender;
        private DevExpress.XtraReports.UI.XRTableCell lblGenderValue;
        private DevExpress.XtraReports.UI.XRTableCell lbl2;
        private DevExpress.XtraReports.UI.XRTableCell lbl2value;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell lblDOB;
        private DevExpress.XtraReports.UI.XRTableCell lblDOBValue;
        private DevExpress.XtraReports.UI.XRTableCell lblWorkTelNo;
        private DevExpress.XtraReports.UI.XRTableCell lblWorkTelNoValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell lblCurrentEmployer;
        private DevExpress.XtraReports.UI.XRTableCell lblCurrentEmployerValue;
        private DevExpress.XtraReports.UI.XRTableCell lblMobileNo;
        private DevExpress.XtraReports.UI.XRTableCell lblMobileNoValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell lblEmailAddress;
        private DevExpress.XtraReports.UI.XRTableCell lblEmailAddressValue;
        private DevExpress.XtraReports.UI.XRTableCell lblMaritalStatus;
        private DevExpress.XtraReports.UI.XRTableCell lblMaritalStatusValue;
        private DevExpress.XtraReports.UI.XRTableCell lblValue;
        private DevExpress.XtraReports.UI.DetailReportBand FraudIndicatorSummary;
        private DevExpress.XtraReports.UI.DetailBand Detail2;
        private DevExpress.XtraReports.UI.XRTable tblFraudIndSummary;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell lblVerified;
        private DevExpress.XtraReports.UI.XRTableCell lblVerifiedVal;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell lblDeceased;
        private DevExpress.XtraReports.UI.XRTableCell lblDeceasedValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell lblFoundDB;
        private DevExpress.XtraReports.UI.XRTableCell lblFoundDBValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell lblEmployerFraudDB;
        private DevExpress.XtraReports.UI.XRTableCell lblEmployerFraudDBValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell lblProrectiveReg;
        private DevExpress.XtraReports.UI.XRTableCell lblProrectiveRegValue;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader2;
        private DevExpress.XtraReports.UI.XRTable tblFraudSummary;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.DetailReportBand Scoring;
        private DevExpress.XtraReports.UI.DetailBand Detail3;
        private DevExpress.XtraReports.UI.XRTable tblscrorevalues;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell lblUniqueIdentifierValue;
        private DevExpress.XtraReports.UI.XRTableCell lblDateValue;
        private DevExpress.XtraReports.UI.XRTableCell lblScroreValue;
        private DevExpress.XtraReports.UI.XRTableCell lblExceptionValue;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader3;
        private DevExpress.XtraReports.UI.XRTable tblscoreColumns;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell lblUniqueIdentifier;
        private DevExpress.XtraReports.UI.XRTableCell lblDate;
        private DevExpress.XtraReports.UI.XRTableCell lblScrore;
        private DevExpress.XtraReports.UI.XRTableCell lblException;
        private DevExpress.XtraReports.UI.XRTable tblScoreHeader;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.DetailReportBand DebtSummary;
        private DevExpress.XtraReports.UI.DetailBand Detail4;
        private DevExpress.XtraReports.UI.XRTable tblDebtSummary;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell lblActiveAcc;
        private DevExpress.XtraReports.UI.XRTableCell lblActiveAccValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell lblDelinquentAcc;
        private DevExpress.XtraReports.UI.XRTableCell lblDelinquentAccValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell lblMonthlyinst;
        private DevExpress.XtraReports.UI.XRTableCell lblMonthlyinstValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell lblOutstandingDebt;
        private DevExpress.XtraReports.UI.XRTableCell lblOutstandingDebtValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell lblArrearamt;
        private DevExpress.XtraReports.UI.XRTableCell lblArrearamtvalue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell lblJdgDate;
        private DevExpress.XtraReports.UI.XRTableCell lblJdgDateValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell lblJudAmt;
        private DevExpress.XtraReports.UI.XRTableCell lblJudAmtValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell lblDelinquentrating;
        private DevExpress.XtraReports.UI.XRTableCell lblDelinquentratingValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell lblDbtReview;
        private DevExpress.XtraReports.UI.XRTableCell lblDbtReviewValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell lblAO;
        private DevExpress.XtraReports.UI.XRTableCell lblAOValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell lblAdverseAmt;
        private DevExpress.XtraReports.UI.XRTableCell lblAdverseAmtValue;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader4;
        private DevExpress.XtraReports.UI.XRTable tblDebtHeader;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell lblAccGood;
        private DevExpress.XtraReports.UI.XRTableCell lblAccGoodvalue;
        private DevExpress.XtraReports.UI.DetailReportBand AccountGoodBadSummary;
        private DevExpress.XtraReports.UI.DetailBand Detail5;
        private DevExpress.XtraReports.UI.XRTable tblAccStatus;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell lblRetail;
        private DevExpress.XtraReports.UI.XRTableCell lblRetailG;
        private DevExpress.XtraReports.UI.XRTableCell lblRetailB;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader5;
        private DevExpress.XtraReports.UI.XRTable tblCrAccStatus;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell lblAccdesc;
        private DevExpress.XtraReports.UI.XRTableCell lblGood;
        private DevExpress.XtraReports.UI.XRTableCell lblBad;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow47;
        private DevExpress.XtraReports.UI.XRTableCell lblCc;
        private DevExpress.XtraReports.UI.XRTableCell lblCreditCradG;
        private DevExpress.XtraReports.UI.XRTableCell lblCreditCardB;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow46;
        private DevExpress.XtraReports.UI.XRTableCell lblFurnAcc;
        private DevExpress.XtraReports.UI.XRTableCell lblFurnAccG;
        private DevExpress.XtraReports.UI.XRTableCell lblFurnAccB;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow45;
        private DevExpress.XtraReports.UI.XRTableCell lblInsurance;
        private DevExpress.XtraReports.UI.XRTableCell lblInsuranceG;
        private DevExpress.XtraReports.UI.XRTableCell lblInsuranceB;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow44;
        private DevExpress.XtraReports.UI.XRTableCell lblFinance;
        private DevExpress.XtraReports.UI.XRTableCell lblFinanceG;
        private DevExpress.XtraReports.UI.XRTableCell lblFinanceB;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow43;
        private DevExpress.XtraReports.UI.XRTableCell lblBankAcc;
        private DevExpress.XtraReports.UI.XRTableCell lblBankAccG;
        private DevExpress.XtraReports.UI.XRTableCell lblBankAccB;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow42;
        private DevExpress.XtraReports.UI.XRTableCell lblTelecomms;
        private DevExpress.XtraReports.UI.XRTableCell lblTelecommsG;
        private DevExpress.XtraReports.UI.XRTableCell lblTelecommsB;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow41;
        private DevExpress.XtraReports.UI.XRTableCell lblHomeLoan;
        private DevExpress.XtraReports.UI.XRTableCell lblHomeLoanG;
        private DevExpress.XtraReports.UI.XRTableCell lblHomeLoanB;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
        private DevExpress.XtraReports.UI.XRTableCell lblVehicleFin;
        private DevExpress.XtraReports.UI.XRTableCell lblVehicleFinG;
        private DevExpress.XtraReports.UI.XRTableCell lblVehicleFinB;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell lblOtherAcc;
        private DevExpress.XtraReports.UI.XRTableCell lblOtherAccG;
        private DevExpress.XtraReports.UI.XRTableCell lblOtherAccB;
        private DevExpress.XtraReports.UI.DetailReportBand NameConfirmation;
        private DevExpress.XtraReports.UI.DetailBand Detail6;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader6;
        private DevExpress.XtraReports.UI.XRTable tblDetailedInfoHeader;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTable tblNameHistoryValues;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow52;
        private DevExpress.XtraReports.UI.XRTableCell lblNameValue;
        private DevExpress.XtraReports.UI.XRTableCell lblNHSurNameValue;
        private DevExpress.XtraReports.UI.XRTableCell lblNHdatevalue;
        private DevExpress.XtraReports.UI.XRTable tblNameHistColumns;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow51;
        private DevExpress.XtraReports.UI.XRTableCell lblName;
        private DevExpress.XtraReports.UI.XRTableCell lblNHSurName;
        private DevExpress.XtraReports.UI.XRTableCell lblNHIDNo;
        private DevExpress.XtraReports.UI.XRTableCell lblNHdate;
        private DevExpress.XtraReports.UI.XRTable tblNameHistoryHeader;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTable tblpersonalConfirmationHeader;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell lblNHIDNoValue;
        private DevExpress.XtraReports.UI.DetailReportBand AddressConfirmation;
        private DevExpress.XtraReports.UI.DetailBand Detail7;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader7;
        private DevExpress.XtraReports.UI.XRTable tblConfAddHistoryC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow54;
        private DevExpress.XtraReports.UI.XRTableCell lblCAddLine1;
        private DevExpress.XtraReports.UI.XRTableCell lblCAddLine3;
        private DevExpress.XtraReports.UI.XRTableCell lblCAddLine4;
        private DevExpress.XtraReports.UI.XRTableCell lblCdate;
        private DevExpress.XtraReports.UI.XRTable tblAddHistoryHeader;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow55;
        private DevExpress.XtraReports.UI.XRTableCell lblCDAddType;
        private DevExpress.XtraReports.UI.XRTableCell lblCDAddLine1;
        private DevExpress.XtraReports.UI.XRTableCell lblCDAddLine2;
        private DevExpress.XtraReports.UI.XRTableCell lblCDAddLine3;
        private DevExpress.XtraReports.UI.XRTableCell lblCDAddLine4;
        private DevExpress.XtraReports.UI.XRTableCell lblCDdate;
        private DevExpress.XtraReports.UI.XRTableCell lblCAddType;
        private DevExpress.XtraReports.UI.XRTableCell lblCAddLine2;
        private DevExpress.XtraReports.UI.DetailReportBand ContactNoConfirmation;
        private DevExpress.XtraReports.UI.DetailBand Detail8;
        private DevExpress.XtraReports.UI.XRTable tblCContactNoD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow58;
        private DevExpress.XtraReports.UI.XRTableCell lblCLLNoD;
        private DevExpress.XtraReports.UI.XRTableCell lblCConfirmedLLD;
        private DevExpress.XtraReports.UI.XRTableCell lblCCellNoD;
        private DevExpress.XtraReports.UI.XRTableCell lblCConfirmedCellNoD;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader8;
        private DevExpress.XtraReports.UI.XRTable tblCContactNoC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow57;
        private DevExpress.XtraReports.UI.XRTableCell lblCLLNo;
        private DevExpress.XtraReports.UI.XRTableCell lblCConfirmedLL;
        private DevExpress.XtraReports.UI.XRTableCell lblCCellNo;
        private DevExpress.XtraReports.UI.XRTableCell lblCConfirmedCellNo;
        private DevExpress.XtraReports.UI.XRTable tblCContactNoH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.DetailReportBand EmploymentDetailConfirmation;
        private DevExpress.XtraReports.UI.DetailBand Detail9;
        private DevExpress.XtraReports.UI.XRTable tblEmpHisD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow61;
        private DevExpress.XtraReports.UI.XRTableCell lblCEmployerD;
        private DevExpress.XtraReports.UI.XRTableCell lblCDesignationD;
        private DevExpress.XtraReports.UI.XRTableCell lblCConfirmeddateD;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader9;
        private DevExpress.XtraReports.UI.XRTable tblEmpHisC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow60;
        private DevExpress.XtraReports.UI.XRTableCell lblCEmployer;
        private DevExpress.XtraReports.UI.XRTableCell lblCDesignation;
        private DevExpress.XtraReports.UI.XRTableCell lblCConfirmeddate;
        private DevExpress.XtraReports.UI.XRTable tblEmploymentHisH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.DetailReportBand NameHistory;
        private DevExpress.XtraReports.UI.DetailBand Detail10;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader10;
        private DevExpress.XtraReports.UI.DetailReportBand AddressHistory;
        private DevExpress.XtraReports.UI.DetailBand Detail11;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader11;
        private DevExpress.XtraReports.UI.DetailReportBand TelephoneHistory;
        private DevExpress.XtraReports.UI.DetailBand Detail12;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader12;
        private DevExpress.XtraReports.UI.DetailReportBand EmploymentHistory;
        private DevExpress.XtraReports.UI.DetailBand Detail13;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader13;
        private DevExpress.XtraReports.UI.DetailReportBand AccountStatus;
        private DevExpress.XtraReports.UI.DetailBand Detail14;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader14;
        private DevExpress.XtraReports.UI.XRTable tblAccStatusH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTable tblAccStatusC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow77;
        private DevExpress.XtraReports.UI.XRTableCell lblAccOpenDate;
        private DevExpress.XtraReports.UI.XRTableCell lblCompany;
        private DevExpress.XtraReports.UI.XRTableCell lblCreditLimit;
        private DevExpress.XtraReports.UI.XRTableCell lblCurrBalance;
        private DevExpress.XtraReports.UI.XRTableCell lblInstallement;
        private DevExpress.XtraReports.UI.XRTableCell lblArrearsAmt;
        private DevExpress.XtraReports.UI.XRTableCell lblAccType;
        private DevExpress.XtraReports.UI.XRTable tblAccStatusD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow76;
        private DevExpress.XtraReports.UI.XRTableCell lblAccOpenDateD;
        private DevExpress.XtraReports.UI.XRTableCell lblCompanyD;
        private DevExpress.XtraReports.UI.XRTableCell lblCreditLimitD;
        private DevExpress.XtraReports.UI.XRTableCell lblCurrBalanceD;
        private DevExpress.XtraReports.UI.XRTableCell lblInstallementD;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRTableCell lblAccTypeD;
        private DevExpress.XtraReports.UI.DetailReportBand MonthlyPaymentHeader;
        private DevExpress.XtraReports.UI.DetailBand Detail15;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader15;
        private DevExpress.XtraReports.UI.XRTable tblMonthlyPaymentH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow78;
        private DevExpress.XtraReports.UI.XRTableCell lblcompanyMP;
        private DevExpress.XtraReports.UI.XRTableCell lblM01;
        private DevExpress.XtraReports.UI.XRTableCell lblM02;
        private DevExpress.XtraReports.UI.XRTableCell lblM03;
        private DevExpress.XtraReports.UI.XRTableCell lblM04;
        private DevExpress.XtraReports.UI.XRTableCell lblM05;
        private DevExpress.XtraReports.UI.XRTableCell lblM06;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTableCell lblM08;
        private DevExpress.XtraReports.UI.XRTableCell lblM09;
        private DevExpress.XtraReports.UI.XRTableCell lblM10;
        private DevExpress.XtraReports.UI.XRTableCell lblM11;
        private DevExpress.XtraReports.UI.XRTableCell lblM12;
        private DevExpress.XtraReports.UI.XRTableCell lblM13;
        private DevExpress.XtraReports.UI.XRTableCell lblM14;
        private DevExpress.XtraReports.UI.XRTableCell lblM15;
        private DevExpress.XtraReports.UI.XRTableCell lblM16;
        private DevExpress.XtraReports.UI.XRTableCell lblM17;
        private DevExpress.XtraReports.UI.XRTableCell lblM18;
        private DevExpress.XtraReports.UI.XRTableCell lblM19;
        private DevExpress.XtraReports.UI.XRTableCell lblM20;
        private DevExpress.XtraReports.UI.XRTableCell lblM21;
        private DevExpress.XtraReports.UI.XRTableCell lblM22;
        private DevExpress.XtraReports.UI.XRTableCell lblM23;
        private DevExpress.XtraReports.UI.XRTableCell lblM24;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo3;
        private DevExpress.XtraReports.UI.DetailReportBand Definition;
        private DevExpress.XtraReports.UI.DetailBand Detail16;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader16;
        private DevExpress.XtraReports.UI.XRTable tblDefinitionC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow80;
        private DevExpress.XtraReports.UI.XRTableCell lblDefDesc;
        private DevExpress.XtraReports.UI.XRTableCell lbDefCode;
        private DevExpress.XtraReports.UI.XRTable tblDefinitionD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow81;
        private DevExpress.XtraReports.UI.XRTableCell lblDefDescD;
        private DevExpress.XtraReports.UI.XRTableCell lbDefCodeD;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.DetailReportBand AdverseInfo;
        private DevExpress.XtraReports.UI.DetailBand Detail17;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader17;
        private DevExpress.XtraReports.UI.DetailReportBand Judgements;
        private DevExpress.XtraReports.UI.DetailBand Detail18;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader18;
        private DevExpress.XtraReports.UI.DetailReportBand AdminOrders;
        private DevExpress.XtraReports.UI.DetailBand Detail19;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader19;
        private DevExpress.XtraReports.UI.DetailReportBand Sequestration;
        private DevExpress.XtraReports.UI.DetailBand Detail20;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader20;
        private DevExpress.XtraReports.UI.DetailReportBand DebtReviewStatus;
        private DevExpress.XtraReports.UI.DetailBand Detail21;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader21;
        private DevExpress.XtraReports.UI.DetailReportBand EnquiryHistory;
        private DevExpress.XtraReports.UI.DetailBand Detail22;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader22;
        private DevExpress.XtraReports.UI.XRTable tblEnquiryD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow101;
        private DevExpress.XtraReports.UI.XRTableCell lblEnqDateD;
        private DevExpress.XtraReports.UI.XRTableCell tblsubscribernameD;
        private DevExpress.XtraReports.UI.XRTableCell lblbusTypeD;
        private DevExpress.XtraReports.UI.XRTable tblEnquiryC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow100;
        private DevExpress.XtraReports.UI.XRTableCell tblEnqDate;
        private DevExpress.XtraReports.UI.XRTableCell tblsubscribername;
        private DevExpress.XtraReports.UI.XRTableCell lblbusType;
        private DevExpress.XtraReports.UI.XRTable tblEnquiryH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow99;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell182;
        private DevExpress.XtraReports.UI.DetailReportBand propertyInfo;
        private DevExpress.XtraReports.UI.DetailBand Detail23;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader23;
        private DevExpress.XtraReports.UI.DetailReportBand DirectorshipLink;
        private DevExpress.XtraReports.UI.DetailBand Detail24;
        private DevExpress.XtraReports.UI.DetailReportBand TelephoneLinkageHome;
        private DevExpress.XtraReports.UI.DetailBand Detail25;
        private DevExpress.XtraReports.UI.XRTable tblLinkagesHD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow105;
        private DevExpress.XtraReports.UI.XRTableCell lblHNameD;
        private DevExpress.XtraReports.UI.XRTableCell lblHSurD;
        private DevExpress.XtraReports.UI.XRTableCell lblHHomenoD;
        private DevExpress.XtraReports.UI.XRTableCell lblHWorkNoD;
        private DevExpress.XtraReports.UI.XRTableCell lblHCellNoD;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader25;
        private DevExpress.XtraReports.UI.XRTable tblLikagesHC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow104;
        private DevExpress.XtraReports.UI.XRTableCell lblHName;
        private DevExpress.XtraReports.UI.XRTableCell lblHSur;
        private DevExpress.XtraReports.UI.XRTableCell lblHHomeno;
        private DevExpress.XtraReports.UI.XRTableCell lblHWorkNo;
        private DevExpress.XtraReports.UI.XRTableCell lblHCellNo;
        private DevExpress.XtraReports.UI.XRTable tblLinkagesHome;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow103;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell239;
        private DevExpress.XtraReports.UI.XRTable tblLinkages;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow102;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell238;
        private DevExpress.XtraReports.UI.DetailReportBand TelephoneLinkageWork;
        private DevExpress.XtraReports.UI.DetailBand Detail26;
        private DevExpress.XtraReports.UI.XRTable tblLinkagesWD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow108;
        private DevExpress.XtraReports.UI.XRTableCell lblWNameD;
        private DevExpress.XtraReports.UI.XRTableCell lblWSurD;
        private DevExpress.XtraReports.UI.XRTableCell lblWHomenoD;
        private DevExpress.XtraReports.UI.XRTableCell lblWWorkNoD;
        private DevExpress.XtraReports.UI.XRTableCell lblWCellNoD;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader24;
        private DevExpress.XtraReports.UI.XRTable tblLikagesWC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow107;
        private DevExpress.XtraReports.UI.XRTableCell lblWName;
        private DevExpress.XtraReports.UI.XRTableCell lblWSur;
        private DevExpress.XtraReports.UI.XRTableCell lblWHomeno;
        private DevExpress.XtraReports.UI.XRTableCell lblWWorkNo;
        private DevExpress.XtraReports.UI.XRTableCell lblWCellNo;
        private DevExpress.XtraReports.UI.XRTable tblLinkagesWork;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow106;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell250;
        private DevExpress.XtraReports.UI.DetailReportBand TelephoneLinkageCellular;
        private DevExpress.XtraReports.UI.DetailBand Detail27;
        private DevExpress.XtraReports.UI.XRTable tblLinkagesCD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow111;
        private DevExpress.XtraReports.UI.XRTableCell lblCNameD;
        private DevExpress.XtraReports.UI.XRTableCell lblCSurD;
        private DevExpress.XtraReports.UI.XRTableCell lblCHomenoD;
        private DevExpress.XtraReports.UI.XRTableCell lblCWorkNoD;
        private DevExpress.XtraReports.UI.XRTableCell lblCellNoD;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader26;
        private DevExpress.XtraReports.UI.XRTable tblLikagesCC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow110;
        private DevExpress.XtraReports.UI.XRTableCell lblCName;
        private DevExpress.XtraReports.UI.XRTableCell lblCSur;
        private DevExpress.XtraReports.UI.XRTableCell lblCHomeno;
        private DevExpress.XtraReports.UI.XRTableCell lblCWorkNo;
        private DevExpress.XtraReports.UI.XRTableCell lblCellNo;
        private DevExpress.XtraReports.UI.XRTable tblLinkagesCell;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow109;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell261;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader27;
        private DevExpress.XtraReports.UI.XRTable tblNamehistD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow65;
        private DevExpress.XtraReports.UI.XRTableCell lblDUpdatedDate;
        private DevExpress.XtraReports.UI.XRTableCell lblHDSurname;
        private DevExpress.XtraReports.UI.XRTableCell lblHDFirstname;
        private DevExpress.XtraReports.UI.XRTableCell lblHDSecodnName;
        private DevExpress.XtraReports.UI.XRTableCell lblHDInitial;
        private DevExpress.XtraReports.UI.XRTableCell lblHDTitle;
        private DevExpress.XtraReports.UI.XRTableCell lblHDIDno;
        private DevExpress.XtraReports.UI.XRTableCell lblHDPPNo;
        private DevExpress.XtraReports.UI.XRTableCell lblHDBirthDate;
        private DevExpress.XtraReports.UI.XRTable tblNameHisC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow64;
        private DevExpress.XtraReports.UI.XRTableCell lblUpdatedDate;
        private DevExpress.XtraReports.UI.XRTableCell lblHSurname;
        private DevExpress.XtraReports.UI.XRTableCell lblHFirstname;
        private DevExpress.XtraReports.UI.XRTableCell lblHSecodnName;
        private DevExpress.XtraReports.UI.XRTableCell lblHInitial;
        private DevExpress.XtraReports.UI.XRTableCell lblHTitle;
        private DevExpress.XtraReports.UI.XRTableCell lblHIDno;
        private DevExpress.XtraReports.UI.XRTableCell lblHPPNo;
        private DevExpress.XtraReports.UI.XRTableCell lblHBirthDate;
        private DevExpress.XtraReports.UI.XRTable tblNameHistH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTable tblConsInfoH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTable tblAddHC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow67;
        private DevExpress.XtraReports.UI.XRTableCell lblAddUpdatedate;
        private DevExpress.XtraReports.UI.XRTableCell lblAddtype;
        private DevExpress.XtraReports.UI.XRTableCell lblAddline1;
        private DevExpress.XtraReports.UI.XRTableCell lblAddLine2;
        private DevExpress.XtraReports.UI.XRTableCell lblAddLine3;
        private DevExpress.XtraReports.UI.XRTableCell lblAddLine4;
        private DevExpress.XtraReports.UI.XRTableCell lblPostalcode;
        private DevExpress.XtraReports.UI.XRTable tblAddHistoryH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell188;
        private DevExpress.XtraReports.UI.XRTable tblAddHD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow68;
        private DevExpress.XtraReports.UI.XRTableCell lblDAddUpdatedate;
        private DevExpress.XtraReports.UI.XRTableCell lblDAddtype;
        private DevExpress.XtraReports.UI.XRTableCell lblDAddline1;
        private DevExpress.XtraReports.UI.XRTableCell lblDAddLine2;
        private DevExpress.XtraReports.UI.XRTableCell lblDAddLine3;
        private DevExpress.XtraReports.UI.XRTableCell lblDAddLine4;
        private DevExpress.XtraReports.UI.XRTableCell lblDPostalcode;
        private DevExpress.XtraReports.UI.XRTable tblContactNoC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow70;
        private DevExpress.XtraReports.UI.XRTableCell lblNoUpdatedate;
        private DevExpress.XtraReports.UI.XRTableCell lblTelnoType;
        private DevExpress.XtraReports.UI.XRTableCell lblTelNo;
        private DevExpress.XtraReports.UI.XRTableCell lblEmail;
        private DevExpress.XtraReports.UI.XRTable tblContactNoH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell221;
        private DevExpress.XtraReports.UI.XRTable tblContactNoD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow71;
        private DevExpress.XtraReports.UI.XRTableCell lblDNoUpdatedate;
        private DevExpress.XtraReports.UI.XRTableCell lblDTelnoType;
        private DevExpress.XtraReports.UI.XRTableCell lblDTelNo;
        private DevExpress.XtraReports.UI.XRTableCell lblDEmail;
        private DevExpress.XtraReports.UI.XRTable tblEmploymentC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow73;
        private DevExpress.XtraReports.UI.XRTableCell lblEmpUpdatedate;
        private DevExpress.XtraReports.UI.XRTableCell lblEmployer;
        private DevExpress.XtraReports.UI.XRTableCell lblDesignation;
        private DevExpress.XtraReports.UI.XRTable tblEmploymentH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow72;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell230;
        private DevExpress.XtraReports.UI.XRTable tblEmploymentD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow74;
        private DevExpress.XtraReports.UI.XRTableCell lblDEmpUpdatedate;
        private DevExpress.XtraReports.UI.XRTableCell lblDEmployer;
        private DevExpress.XtraReports.UI.XRTableCell lblDDesignation;
        private DevExpress.XtraReports.UI.XRTable tblAdverseColumns;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow83;
        private DevExpress.XtraReports.UI.XRTableCell lblSubscriber;
        private DevExpress.XtraReports.UI.XRTableCell lblAccountNo;
        private DevExpress.XtraReports.UI.XRTableCell lblActionDate;
        private DevExpress.XtraReports.UI.XRTableCell lblAmount;
        private DevExpress.XtraReports.UI.XRTableCell lblAccStatus;
        private DevExpress.XtraReports.UI.XRTableCell lblAdverseComment;
        private DevExpress.XtraReports.UI.XRTable tblAdverseH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow84;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        private DevExpress.XtraReports.UI.XRTable tblDomainH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow82;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
        private DevExpress.XtraReports.UI.XRTable tblAdverseData;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow85;
        private DevExpress.XtraReports.UI.XRTableCell lblDSubscriber;
        private DevExpress.XtraReports.UI.XRTableCell lblDAccNo;
        private DevExpress.XtraReports.UI.XRTableCell lblDActionDate;
        private DevExpress.XtraReports.UI.XRTableCell lblDAmount;
        private DevExpress.XtraReports.UI.XRTableCell lblDAccStatus;
        private DevExpress.XtraReports.UI.XRTableCell lblDAdvComment;
        private DevExpress.XtraReports.UI.XRTable tblCJud;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow87;
        private DevExpress.XtraReports.UI.XRTableCell lblJCaseNo;
        private DevExpress.XtraReports.UI.XRTableCell lblJIssueDate;
        private DevExpress.XtraReports.UI.XRTableCell lblJType;
        private DevExpress.XtraReports.UI.XRTableCell lblJAmount;
        private DevExpress.XtraReports.UI.XRTableCell lblJPlaintiff;
        private DevExpress.XtraReports.UI.XRTableCell lblJCourt;
        private DevExpress.XtraReports.UI.XRTableCell lblJAttorney;
        private DevExpress.XtraReports.UI.XRTableCell lblJPhoneNo;
        private DevExpress.XtraReports.UI.XRTableCell lblJComment;
        private DevExpress.XtraReports.UI.XRTable tblJudgmentsH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow86;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRTable tblDJudg;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow88;
        private DevExpress.XtraReports.UI.XRTableCell lblJDCaseno;
        private DevExpress.XtraReports.UI.XRTableCell lblJDIssueDate;
        private DevExpress.XtraReports.UI.XRTableCell lblJDType;
        private DevExpress.XtraReports.UI.XRTableCell lblJDAmount;
        private DevExpress.XtraReports.UI.XRTableCell lblJDPlaintiff;
        private DevExpress.XtraReports.UI.XRTableCell lblJDCourt;
        private DevExpress.XtraReports.UI.XRTableCell lblJDAttorney;
        private DevExpress.XtraReports.UI.XRTableCell lblJDPhoneNo;
        private DevExpress.XtraReports.UI.XRTableCell lblJDComment;
        private DevExpress.XtraReports.UI.XRTable tblCAO;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow90;
        private DevExpress.XtraReports.UI.XRTableCell lblAOCaseno;
        private DevExpress.XtraReports.UI.XRTableCell lblAOIssuedate;
        private DevExpress.XtraReports.UI.XRTableCell lblAODateLoaded;
        private DevExpress.XtraReports.UI.XRTableCell lblAOType;
        private DevExpress.XtraReports.UI.XRTableCell lblAOAmount;
        private DevExpress.XtraReports.UI.XRTableCell lblAOPlaintiff;
        private DevExpress.XtraReports.UI.XRTableCell lblAOCourt;
        private DevExpress.XtraReports.UI.XRTableCell lblAOAttorney;
        private DevExpress.XtraReports.UI.XRTableCell lblAOPhoneNo;
        private DevExpress.XtraReports.UI.XRTable tblAOH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow89;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell128;
        private DevExpress.XtraReports.UI.XRTable tblDAO;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow91;
        private DevExpress.XtraReports.UI.XRTableCell lblAODCaseno;
        private DevExpress.XtraReports.UI.XRTableCell lblAODIssuedate;
        private DevExpress.XtraReports.UI.XRTableCell lblAODLoadeddate;
        private DevExpress.XtraReports.UI.XRTableCell lblAODType;
        private DevExpress.XtraReports.UI.XRTableCell lblAODAmount;
        private DevExpress.XtraReports.UI.XRTableCell lblAODPlaintiff;
        private DevExpress.XtraReports.UI.XRTableCell lblAODCOurt;
        private DevExpress.XtraReports.UI.XRTableCell lblAODAttorney;
        private DevExpress.XtraReports.UI.XRTableCell lblAODPhoenNo;
        private DevExpress.XtraReports.UI.XRTable tblSEQC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow93;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQCaseno;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQIssueDate;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQLoadeddate;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQType;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQAmount;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQplaintiff;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQCourt;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQAttorney;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQPhoneNo;
        private DevExpress.XtraReports.UI.XRTable tblSEQH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow92;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell156;
        private DevExpress.XtraReports.UI.XRTable tblSEQD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow94;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQDCaseno;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQDIssuedate;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQDLoadedDate;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQDType;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQDAmount;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQDPlaintiff;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQDCourt;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQDAttorney;
        private DevExpress.XtraReports.UI.XRTableCell lblSEQDPhoneNo;
        private DevExpress.XtraReports.UI.XRTable tblPropInterestsH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow118;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell280;
        private DevExpress.XtraReports.UI.XRTable tblPropInterests;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow120;
        private DevExpress.XtraReports.UI.XRTableCell lblDeedNo;
        private DevExpress.XtraReports.UI.XRTableCell lblDDeedNo;
        private DevExpress.XtraReports.UI.XRTableCell lblsiteno;
        private DevExpress.XtraReports.UI.XRTableCell lblDsiteno;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow125;
        private DevExpress.XtraReports.UI.XRTableCell lblDeedsoffice;
        private DevExpress.XtraReports.UI.XRTableCell lblDDeedsoffice;
        private DevExpress.XtraReports.UI.XRTableCell lblPhyadd;
        private DevExpress.XtraReports.UI.XRTableCell lblDPhyadd;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow124;
        private DevExpress.XtraReports.UI.XRTableCell lblPropType;
        private DevExpress.XtraReports.UI.XRTableCell lblDPropType;
        private DevExpress.XtraReports.UI.XRTableCell lblSize;
        private DevExpress.XtraReports.UI.XRTableCell lblDSize;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow123;
        private DevExpress.XtraReports.UI.XRTableCell lblPurchasedate;
        private DevExpress.XtraReports.UI.XRTableCell lblDPurchasedate;
        private DevExpress.XtraReports.UI.XRTableCell lblPurchaseprice;
        private DevExpress.XtraReports.UI.XRTableCell lblDPurchaseprice;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow122;
        private DevExpress.XtraReports.UI.XRTableCell lblshareperc;
        private DevExpress.XtraReports.UI.XRTableCell lblDshareperc;
        private DevExpress.XtraReports.UI.XRTableCell lblHolder;
        private DevExpress.XtraReports.UI.XRTableCell lblDHolder;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow121;
        private DevExpress.XtraReports.UI.XRTableCell lblBondNo;
        private DevExpress.XtraReports.UI.XRTableCell lblDBondNo;
        private DevExpress.XtraReports.UI.XRTableCell lblBondAmount;
        private DevExpress.XtraReports.UI.XRTableCell lblDBondAmount;
        private DevExpress.XtraReports.UI.XRTable tblPropInterestH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow119;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell281;
        private DevExpress.XtraReports.UI.XRTable tblDirectorlinksH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow112;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell272;
        private DevExpress.XtraReports.UI.XRTable tblDirInfo;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow113;
        private DevExpress.XtraReports.UI.XRTableCell lblCurrentPost;
        private DevExpress.XtraReports.UI.XRTableCell lblDCurrentPost;
        private DevExpress.XtraReports.UI.XRTableCell lblInceptionDate;
        private DevExpress.XtraReports.UI.XRTableCell lblDAppDate;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow116;
        private DevExpress.XtraReports.UI.XRTableCell lblCompanyName;
        private DevExpress.XtraReports.UI.XRTableCell lblDCompanyName;
        private DevExpress.XtraReports.UI.XRTableCell lblRegNo;
        private DevExpress.XtraReports.UI.XRTableCell lblDRegNo;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow117;
        private DevExpress.XtraReports.UI.XRTableCell lblCompanyAdd;
        private DevExpress.XtraReports.UI.XRTableCell lblDPhysicalAdd;
        private DevExpress.XtraReports.UI.XRTableCell lblPhoneNo;
        private DevExpress.XtraReports.UI.XRTableCell lblDPhoneNo;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow115;
        private DevExpress.XtraReports.UI.XRTableCell lblIndustryCatg;
        private DevExpress.XtraReports.UI.XRTableCell lblDIndCateg;
        private DevExpress.XtraReports.UI.XRTable tblDirectorlinkH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow114;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell274;
        private DevExpress.XtraReports.UI.DetailReportBand MonthlyPayment;
        private DevExpress.XtraReports.UI.DetailBand Detail28;
        private DevExpress.XtraReports.UI.XRTable tblMonthlyPaymentD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow79;
        private DevExpress.XtraReports.UI.XRTableCell lblcompanyMPD;
        private DevExpress.XtraReports.UI.XRTableCell lblM01D;
        private DevExpress.XtraReports.UI.XRTableCell lblM02D;
        private DevExpress.XtraReports.UI.XRTableCell lblM03D;
        private DevExpress.XtraReports.UI.XRTableCell lblM04D;
        private DevExpress.XtraReports.UI.XRTableCell lblM05D;
        private DevExpress.XtraReports.UI.XRTableCell lblM06D;
        private DevExpress.XtraReports.UI.XRTableCell lblM07D;
        private DevExpress.XtraReports.UI.XRTableCell lblM08D;
        private DevExpress.XtraReports.UI.XRTableCell lblM09D;
        private DevExpress.XtraReports.UI.XRTableCell lblM10D;
        private DevExpress.XtraReports.UI.XRTableCell lblM11D;
        private DevExpress.XtraReports.UI.XRTableCell lblM12D;
        private DevExpress.XtraReports.UI.XRTableCell lblM13D;
        private DevExpress.XtraReports.UI.XRTableCell lblM14D;
        private DevExpress.XtraReports.UI.XRTableCell lblM15D;
        private DevExpress.XtraReports.UI.XRTableCell lblM16D;
        private DevExpress.XtraReports.UI.XRTableCell lblM17D;
        private DevExpress.XtraReports.UI.XRTableCell lblM18D;
        private DevExpress.XtraReports.UI.XRTableCell lblM19D;
        private DevExpress.XtraReports.UI.XRTableCell lblM20D;
        private DevExpress.XtraReports.UI.XRTableCell lblM21D;
        private DevExpress.XtraReports.UI.XRTableCell lblM22D;
        private DevExpress.XtraReports.UI.XRTableCell lblM23D;
        private DevExpress.XtraReports.UI.XRTableCell lblM24D;
        private DevExpress.XtraReports.UI.XRTable tblDbtReviewD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow96;
        private DevExpress.XtraReports.UI.XRTableCell lblDbtReviewDate;
        private DevExpress.XtraReports.UI.XRTableCell lblDbtReviewDateD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow98;
        private DevExpress.XtraReports.UI.XRTableCell lblCounsellorName;
        private DevExpress.XtraReports.UI.XRTableCell lblCounsellorNameD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow97;
        private DevExpress.XtraReports.UI.XRTableCell lblCounsellorTelephoneNo;
        private DevExpress.XtraReports.UI.XRTableCell lblCounsellorTelephoneNoD;
        private DevExpress.XtraReports.UI.XRTable tbldbtReviewH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow95;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell175;
    }
}
