namespace XdsPortalReports
{
    partial class AccountVerificationReport_Old
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AccountVerificationReport_Old));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrPictureBox2 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow51 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPageInfo4 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblEnquiryDateValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblEnquiryStatusC = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblEnquiryStatusD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblXDSRefNoC = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblXDSRefNoD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblexternalRefNoC = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblexternalRefNoD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport1 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
            this.tblresult = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDescH = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblResultH = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblInputH = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblAccExistC = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAccExistR = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAccExistI = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblInitialsH = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblInitialsR = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblInitialsI = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSurNameC = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSurNameR = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSurNameI = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblIDNoC = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblIDNoR = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblIDNoI = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblAccStatusC = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAccStatusR = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAccStatusI = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport2 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail3 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblIAccNoC = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblIAccNoD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblIBranchCodeC = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblIBranchCodeD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblIAccTypeC = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblIAccTypeD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblIBankNameC = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblIBankNameD = new DevExpress.XtraReports.UI.XRTableCell();
            this.tblUserInput = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblresult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblUserInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.HeightF = 14F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 6F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 20F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox2,
            this.xrTable2,
            this.xrTable5});
            this.ReportHeader.HeightF = 100F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrPictureBox2
            // 
            this.xrPictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox2.Image")));
            this.xrPictureBox2.LocationFloat = new DevExpress.Utils.PointFloat(58F, 0F);
            this.xrPictureBox2.Name = "xrPictureBox2";
            this.xrPictureBox2.SizeF = new System.Drawing.SizeF(167F, 100F);
            this.xrPictureBox2.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // xrTable2
            // 
            this.xrTable2.BorderColor = System.Drawing.Color.White;
            this.xrTable2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable2.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(225F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow31});
            this.xrTable2.SizeF = new System.Drawing.SizeF(574.5F, 100F);
            this.xrTable2.StylePriority.UseBorderColor = false;
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 1.5334063526834612;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(216)))), ((int)(((byte)(22)))));
            this.xrTableCell3.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell3.Font = new System.Drawing.Font("Arial", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell3.ForeColor = System.Drawing.Color.White;
            this.xrTableCell3.Multiline = true;
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseBackColor = false;
            this.xrTableCell3.StylePriority.UseBorderColor = false;
            this.xrTableCell3.StylePriority.UseBorders = false;
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.StylePriority.UseForeColor = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "Xpert Account Verification Report";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell3.Weight = 1.9059967585089144;
            // 
            // xrTable5
            // 
            this.xrTable5.BorderColor = System.Drawing.Color.DimGray;
            this.xrTable5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable5.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(8F, 0F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow51});
            this.xrTable5.SizeF = new System.Drawing.SizeF(50F, 100F);
            this.xrTable5.StylePriority.UseBorderColor = false;
            this.xrTable5.StylePriority.UseBorders = false;
            this.xrTable5.StylePriority.UseFont = false;
            // 
            // xrTableRow51
            // 
            this.xrTableRow51.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2});
            this.xrTableRow51.Name = "xrTableRow51";
            this.xrTableRow51.Weight = 1.5334063526834612;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(216)))), ((int)(((byte)(22)))));
            this.xrTableCell2.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell2.Font = new System.Drawing.Font("Arial", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell2.ForeColor = System.Drawing.Color.White;
            this.xrTableCell2.Multiline = true;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseBackColor = false;
            this.xrTableCell2.StylePriority.UseBorderColor = false;
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.StylePriority.UseForeColor = false;
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell2.Weight = 1.9059967585089144;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.xrPageInfo4,
            this.xrPageInfo3});
            this.PageFooter.Expanded = false;
            this.PageFooter.HeightF = 25F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrLabel1
            // 
            this.xrLabel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrLabel1.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.ForeColor = System.Drawing.Color.DimGray;
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(707.0834F, 0F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(42.91663F, 25F);
            this.xrLabel1.StylePriority.UseBackColor = false;
            this.xrLabel1.StylePriority.UseBorderColor = false;
            this.xrLabel1.StylePriority.UseBorders = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseForeColor = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Page:";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrPageInfo4
            // 
            this.xrPageInfo4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrPageInfo4.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrPageInfo4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPageInfo4.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrPageInfo4.ForeColor = System.Drawing.Color.DimGray;
            this.xrPageInfo4.LocationFloat = new DevExpress.Utils.PointFloat(750.5F, 0F);
            this.xrPageInfo4.Name = "xrPageInfo4";
            this.xrPageInfo4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo4.SizeF = new System.Drawing.SizeF(54.49994F, 25F);
            this.xrPageInfo4.StylePriority.UseBackColor = false;
            this.xrPageInfo4.StylePriority.UseBorderColor = false;
            this.xrPageInfo4.StylePriority.UseBorders = false;
            this.xrPageInfo4.StylePriority.UseFont = false;
            this.xrPageInfo4.StylePriority.UseForeColor = false;
            this.xrPageInfo4.StylePriority.UseTextAlignment = false;
            this.xrPageInfo4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrPageInfo3
            // 
            this.xrPageInfo3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrPageInfo3.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrPageInfo3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPageInfo3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrPageInfo3.ForeColor = System.Drawing.Color.DimGray;
            this.xrPageInfo3.Format = "Printed Date: {0:dddd, dd MMMM yyyy H:mm}";
            this.xrPageInfo3.LocationFloat = new DevExpress.Utils.PointFloat(8F, 0F);
            this.xrPageInfo3.Name = "xrPageInfo3";
            this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo3.SizeF = new System.Drawing.SizeF(699.0834F, 25F);
            this.xrPageInfo3.StylePriority.UseBackColor = false;
            this.xrPageInfo3.StylePriority.UseBorderColor = false;
            this.xrPageInfo3.StylePriority.UseBorders = false;
            this.xrPageInfo3.StylePriority.UseFont = false;
            this.xrPageInfo3.StylePriority.UseForeColor = false;
            this.xrPageInfo3.StylePriority.UsePadding = false;
            this.xrPageInfo3.StylePriority.UseTextAlignment = false;
            this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1});
            this.DetailReport.DataMember = "SubscriberInputDetails";
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4,
            this.xrTable3});
            this.Detail1.HeightF = 206F;
            this.Detail1.Name = "Detail1";
            // 
            // xrTable4
            // 
            this.xrTable4.BorderColor = System.Drawing.Color.White;
            this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(8F, 40.5F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10,
            this.xrTableRow12,
            this.xrTableRow13,
            this.xrTableRow27,
            this.xrTableRow34,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow8});
            this.xrTable4.SizeF = new System.Drawing.SizeF(792F, 160F);
            this.xrTable4.StylePriority.UseBorderColor = false;
            this.xrTable4.StylePriority.UseBorders = false;
            this.xrTable4.StylePriority.UseFont = false;
            this.xrTable4.StylePriority.UsePadding = false;
            this.xrTable4.StylePriority.UseTextAlignment = false;
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8,
            this.lblEnquiryDateValue});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 0.8;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell8.BorderColor = System.Drawing.Color.White;
            this.xrTableCell8.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell8.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell8.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell8.StylePriority.UseBackColor = false;
            this.xrTableCell8.StylePriority.UseBorderColor = false;
            this.xrTableCell8.StylePriority.UseBorders = false;
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.StylePriority.UseForeColor = false;
            this.xrTableCell8.StylePriority.UsePadding = false;
            this.xrTableCell8.Text = "Enquiry Date";
            this.xrTableCell8.Weight = 0.652582038146617;
            // 
            // lblEnquiryDateValue
            // 
            this.lblEnquiryDateValue.BackColor = System.Drawing.Color.White;
            this.lblEnquiryDateValue.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblEnquiryDateValue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblEnquiryDateValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.EnquiryDate")});
            this.lblEnquiryDateValue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblEnquiryDateValue.ForeColor = System.Drawing.Color.DimGray;
            this.lblEnquiryDateValue.Name = "lblEnquiryDateValue";
            this.lblEnquiryDateValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblEnquiryDateValue.StylePriority.UseBackColor = false;
            this.lblEnquiryDateValue.StylePriority.UseBorderColor = false;
            this.lblEnquiryDateValue.StylePriority.UseBorders = false;
            this.lblEnquiryDateValue.StylePriority.UseFont = false;
            this.lblEnquiryDateValue.StylePriority.UseForeColor = false;
            this.lblEnquiryDateValue.StylePriority.UsePadding = false;
            this.lblEnquiryDateValue.Text = "lblEnquiryDateValue";
            this.lblEnquiryDateValue.Weight = 2.3360543254897466;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.xrTableCell14});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 0.79999999999999993;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell9.BorderColor = System.Drawing.Color.White;
            this.xrTableCell9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell9.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell9.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell9.StylePriority.UseBackColor = false;
            this.xrTableCell9.StylePriority.UseBorderColor = false;
            this.xrTableCell9.StylePriority.UseBorders = false;
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.StylePriority.UseForeColor = false;
            this.xrTableCell9.StylePriority.UsePadding = false;
            this.xrTableCell9.Text = "Enquiry Type";
            this.xrTableCell9.Weight = 0.652582038146617;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.BackColor = System.Drawing.Color.White;
            this.xrTableCell14.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell14.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell14.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.EnquiryType")});
            this.xrTableCell14.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell14.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell14.StylePriority.UseBackColor = false;
            this.xrTableCell14.StylePriority.UseBorderColor = false;
            this.xrTableCell14.StylePriority.UseBorders = false;
            this.xrTableCell14.StylePriority.UseFont = false;
            this.xrTableCell14.StylePriority.UseForeColor = false;
            this.xrTableCell14.StylePriority.UsePadding = false;
            this.xrTableCell14.Text = "xrTableCell14";
            this.xrTableCell14.Weight = 2.3360543254897466;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.xrTableCell18});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 0.79999999999999993;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell10.BorderColor = System.Drawing.Color.White;
            this.xrTableCell10.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell10.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell10.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell10.StylePriority.UseBackColor = false;
            this.xrTableCell10.StylePriority.UseBorderColor = false;
            this.xrTableCell10.StylePriority.UseBorders = false;
            this.xrTableCell10.StylePriority.UseFont = false;
            this.xrTableCell10.StylePriority.UseForeColor = false;
            this.xrTableCell10.StylePriority.UsePadding = false;
            this.xrTableCell10.Text = "Subscriber Name";
            this.xrTableCell10.Weight = 0.652582038146617;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.BackColor = System.Drawing.Color.White;
            this.xrTableCell18.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell18.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell18.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.SubscriberName")});
            this.xrTableCell18.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell18.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell18.StylePriority.UseBackColor = false;
            this.xrTableCell18.StylePriority.UseBorderColor = false;
            this.xrTableCell18.StylePriority.UseBorders = false;
            this.xrTableCell18.StylePriority.UseFont = false;
            this.xrTableCell18.StylePriority.UseForeColor = false;
            this.xrTableCell18.StylePriority.UsePadding = false;
            this.xrTableCell18.Text = "xrTableCell18";
            this.xrTableCell18.Weight = 2.3360543254897466;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell19,
            this.xrTableCell22});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 0.8;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell19.BorderColor = System.Drawing.Color.White;
            this.xrTableCell19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell19.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell19.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell19.StylePriority.UseBackColor = false;
            this.xrTableCell19.StylePriority.UseBorderColor = false;
            this.xrTableCell19.StylePriority.UseBorders = false;
            this.xrTableCell19.StylePriority.UseFont = false;
            this.xrTableCell19.StylePriority.UseForeColor = false;
            this.xrTableCell19.StylePriority.UsePadding = false;
            this.xrTableCell19.Text = "Subscriber User Name";
            this.xrTableCell19.Weight = 0.652582038146617;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.BackColor = System.Drawing.Color.White;
            this.xrTableCell22.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell22.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell22.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.SubscriberUserName")});
            this.xrTableCell22.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell22.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell22.StylePriority.UseBackColor = false;
            this.xrTableCell22.StylePriority.UseBorderColor = false;
            this.xrTableCell22.StylePriority.UseBorders = false;
            this.xrTableCell22.StylePriority.UseFont = false;
            this.xrTableCell22.StylePriority.UseForeColor = false;
            this.xrTableCell22.StylePriority.UsePadding = false;
            this.xrTableCell22.Text = "xrTableCell22";
            this.xrTableCell22.Weight = 2.3360543254897466;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell23,
            this.xrTableCell26});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 0.8;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTableCell23.BorderColor = System.Drawing.Color.White;
            this.xrTableCell23.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell23.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell23.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell23.StylePriority.UseBackColor = false;
            this.xrTableCell23.StylePriority.UseBorderColor = false;
            this.xrTableCell23.StylePriority.UseBorders = false;
            this.xrTableCell23.StylePriority.UseFont = false;
            this.xrTableCell23.StylePriority.UseForeColor = false;
            this.xrTableCell23.StylePriority.UsePadding = false;
            this.xrTableCell23.Text = "Enquiry Input";
            this.xrTableCell23.Weight = 0.652582038146617;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.BackColor = System.Drawing.Color.White;
            this.xrTableCell26.BorderColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell26.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell26.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.EnquiryInput")});
            this.xrTableCell26.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell26.ForeColor = System.Drawing.Color.DimGray;
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell26.StylePriority.UseBackColor = false;
            this.xrTableCell26.StylePriority.UseBorderColor = false;
            this.xrTableCell26.StylePriority.UseBorders = false;
            this.xrTableCell26.StylePriority.UseFont = false;
            this.xrTableCell26.StylePriority.UseForeColor = false;
            this.xrTableCell26.StylePriority.UsePadding = false;
            this.xrTableCell26.Text = "xrTableCell26";
            this.xrTableCell26.Weight = 2.3360543254897466;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblEnquiryStatusC,
            this.lblEnquiryStatusD});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 0.8;
            // 
            // lblEnquiryStatusC
            // 
            this.lblEnquiryStatusC.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblEnquiryStatusC.BorderColor = System.Drawing.Color.White;
            this.lblEnquiryStatusC.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblEnquiryStatusC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnquiryStatusC.ForeColor = System.Drawing.Color.Gray;
            this.lblEnquiryStatusC.Name = "lblEnquiryStatusC";
            this.lblEnquiryStatusC.StylePriority.UseBackColor = false;
            this.lblEnquiryStatusC.StylePriority.UseBorderColor = false;
            this.lblEnquiryStatusC.StylePriority.UseBorders = false;
            this.lblEnquiryStatusC.StylePriority.UseFont = false;
            this.lblEnquiryStatusC.StylePriority.UseForeColor = false;
            this.lblEnquiryStatusC.Text = "Enquiry Status";
            this.lblEnquiryStatusC.Weight = 0.652582038146617;
            // 
            // lblEnquiryStatusD
            // 
            this.lblEnquiryStatusD.BackColor = System.Drawing.Color.White;
            this.lblEnquiryStatusD.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblEnquiryStatusD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblEnquiryStatusD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.EnquiryStatus")});
            this.lblEnquiryStatusD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnquiryStatusD.ForeColor = System.Drawing.Color.DimGray;
            this.lblEnquiryStatusD.Name = "lblEnquiryStatusD";
            this.lblEnquiryStatusD.StylePriority.UseBackColor = false;
            this.lblEnquiryStatusD.StylePriority.UseBorderColor = false;
            this.lblEnquiryStatusD.StylePriority.UseBorders = false;
            this.lblEnquiryStatusD.StylePriority.UseFont = false;
            this.lblEnquiryStatusD.StylePriority.UseForeColor = false;
            this.lblEnquiryStatusD.Text = "lblEnquiryStatusD";
            this.lblEnquiryStatusD.Weight = 2.3360543254897466;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblXDSRefNoC,
            this.lblXDSRefNoD});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 0.8;
            // 
            // lblXDSRefNoC
            // 
            this.lblXDSRefNoC.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblXDSRefNoC.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblXDSRefNoC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblXDSRefNoC.ForeColor = System.Drawing.Color.Gray;
            this.lblXDSRefNoC.Name = "lblXDSRefNoC";
            this.lblXDSRefNoC.StylePriority.UseBackColor = false;
            this.lblXDSRefNoC.StylePriority.UseBorders = false;
            this.lblXDSRefNoC.StylePriority.UseFont = false;
            this.lblXDSRefNoC.StylePriority.UseForeColor = false;
            this.lblXDSRefNoC.Text = "XDS Reference Number";
            this.lblXDSRefNoC.Weight = 0.652582038146617;
            // 
            // lblXDSRefNoD
            // 
            this.lblXDSRefNoD.BackColor = System.Drawing.Color.White;
            this.lblXDSRefNoD.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblXDSRefNoD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblXDSRefNoD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.XDSRefNo")});
            this.lblXDSRefNoD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblXDSRefNoD.ForeColor = System.Drawing.Color.DimGray;
            this.lblXDSRefNoD.Name = "lblXDSRefNoD";
            this.lblXDSRefNoD.StylePriority.UseBackColor = false;
            this.lblXDSRefNoD.StylePriority.UseBorderColor = false;
            this.lblXDSRefNoD.StylePriority.UseBorders = false;
            this.lblXDSRefNoD.StylePriority.UseFont = false;
            this.lblXDSRefNoD.StylePriority.UseForeColor = false;
            this.lblXDSRefNoD.Text = "lblXDSRefNoD";
            this.lblXDSRefNoD.Weight = 2.3360543254897466;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblexternalRefNoC,
            this.lblexternalRefNoD});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 0.8;
            // 
            // lblexternalRefNoC
            // 
            this.lblexternalRefNoC.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblexternalRefNoC.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblexternalRefNoC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblexternalRefNoC.ForeColor = System.Drawing.Color.Gray;
            this.lblexternalRefNoC.Name = "lblexternalRefNoC";
            this.lblexternalRefNoC.StylePriority.UseBackColor = false;
            this.lblexternalRefNoC.StylePriority.UseBorders = false;
            this.lblexternalRefNoC.StylePriority.UseFont = false;
            this.lblexternalRefNoC.StylePriority.UseForeColor = false;
            this.lblexternalRefNoC.Text = "External Reference Number";
            this.lblexternalRefNoC.Weight = 0.652582038146617;
            // 
            // lblexternalRefNoD
            // 
            this.lblexternalRefNoD.BackColor = System.Drawing.Color.White;
            this.lblexternalRefNoD.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblexternalRefNoD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblexternalRefNoD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.ExternalRef")});
            this.lblexternalRefNoD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblexternalRefNoD.ForeColor = System.Drawing.Color.DimGray;
            this.lblexternalRefNoD.Name = "lblexternalRefNoD";
            this.lblexternalRefNoD.StylePriority.UseBackColor = false;
            this.lblexternalRefNoD.StylePriority.UseBorderColor = false;
            this.lblexternalRefNoD.StylePriority.UseBorders = false;
            this.lblexternalRefNoD.StylePriority.UseFont = false;
            this.lblexternalRefNoD.StylePriority.UseForeColor = false;
            this.lblexternalRefNoD.Text = "lblexternalRefNoD";
            this.lblexternalRefNoD.Weight = 2.3360543254897466;
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(8F, 3F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9});
            this.xrTable3.SizeF = new System.Drawing.SizeF(792F, 33F);
            this.xrTable3.StylePriority.UseBorders = false;
            this.xrTable3.StylePriority.UseFont = false;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(216)))), ((int)(((byte)(22)))));
            this.xrTableCell7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell7.BorderWidth = 2;
            this.xrTableCell7.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell7.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell7.StylePriority.UseBorderColor = false;
            this.xrTableCell7.StylePriority.UseBorders = false;
            this.xrTableCell7.StylePriority.UseBorderWidth = false;
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.StylePriority.UseForeColor = false;
            this.xrTableCell7.StylePriority.UsePadding = false;
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            this.xrTableCell7.Text = "Enquiry Details";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell7.Weight = 3;
            // 
            // DetailReport1
            // 
            this.DetailReport1.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail2});
            this.DetailReport1.DataMember = "return";
            this.DetailReport1.Level = 2;
            this.DetailReport1.Name = "DetailReport1";
            // 
            // Detail2
            // 
            this.Detail2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblresult});
            this.Detail2.HeightF = 143F;
            this.Detail2.Name = "Detail2";
            // 
            // tblresult
            // 
            this.tblresult.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.tblresult.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tblresult.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.tblresult.LocationFloat = new DevExpress.Utils.PointFloat(8F, 12F);
            this.tblresult.Name = "tblresult";
            this.tblresult.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.tblresult.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow39,
            this.xrTableRow38,
            this.xrTableRow47,
            this.xrTableRow46,
            this.xrTableRow45,
            this.xrTableRow44});
            this.tblresult.SizeF = new System.Drawing.SizeF(792F, 125F);
            this.tblresult.StylePriority.UseBorderColor = false;
            this.tblresult.StylePriority.UseBorders = false;
            this.tblresult.StylePriority.UseFont = false;
            this.tblresult.StylePriority.UsePadding = false;
            this.tblresult.StylePriority.UseTextAlignment = false;
            this.tblresult.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDescH,
            this.lblResultH,
            this.lblInputH});
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Weight = 1;
            // 
            // lblDescH
            // 
            this.lblDescH.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblDescH.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(216)))), ((int)(((byte)(22)))));
            this.lblDescH.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDescH.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.lblDescH.ForeColor = System.Drawing.Color.Gray;
            this.lblDescH.Name = "lblDescH";
            this.lblDescH.StylePriority.UseBackColor = false;
            this.lblDescH.StylePriority.UseBorderColor = false;
            this.lblDescH.StylePriority.UseBorders = false;
            this.lblDescH.StylePriority.UseFont = false;
            this.lblDescH.StylePriority.UseForeColor = false;
            this.lblDescH.Text = "Description";
            this.lblDescH.Weight = 0.84487513064197384;
            // 
            // lblResultH
            // 
            this.lblResultH.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblResultH.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(216)))), ((int)(((byte)(22)))));
            this.lblResultH.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblResultH.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.lblResultH.ForeColor = System.Drawing.Color.Gray;
            this.lblResultH.Name = "lblResultH";
            this.lblResultH.StylePriority.UseBackColor = false;
            this.lblResultH.StylePriority.UseBorderColor = false;
            this.lblResultH.StylePriority.UseBorders = false;
            this.lblResultH.StylePriority.UseFont = false;
            this.lblResultH.StylePriority.UseForeColor = false;
            this.lblResultH.StylePriority.UseTextAlignment = false;
            this.lblResultH.Text = "Xpert Account Result";
            this.lblResultH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblResultH.Weight = 0.78673023099534245;
            // 
            // lblInputH
            // 
            this.lblInputH.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblInputH.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(216)))), ((int)(((byte)(22)))));
            this.lblInputH.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblInputH.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.lblInputH.ForeColor = System.Drawing.Color.Gray;
            this.lblInputH.Name = "lblInputH";
            this.lblInputH.StylePriority.UseBackColor = false;
            this.lblInputH.StylePriority.UseBorderColor = false;
            this.lblInputH.StylePriority.UseBorders = false;
            this.lblInputH.StylePriority.UseFont = false;
            this.lblInputH.StylePriority.UseForeColor = false;
            this.lblInputH.StylePriority.UseTextAlignment = false;
            this.lblInputH.Text = "User Request";
            this.lblInputH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblInputH.Weight = 0.84043411204689411;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblAccExistC,
            this.lblAccExistR,
            this.lblAccExistI});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Weight = 0.8;
            // 
            // lblAccExistC
            // 
            this.lblAccExistC.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblAccExistC.BorderColor = System.Drawing.Color.White;
            this.lblAccExistC.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblAccExistC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAccExistC.ForeColor = System.Drawing.Color.Gray;
            this.lblAccExistC.Name = "lblAccExistC";
            this.lblAccExistC.StylePriority.UseBackColor = false;
            this.lblAccExistC.StylePriority.UseBorderColor = false;
            this.lblAccExistC.StylePriority.UseBorders = false;
            this.lblAccExistC.StylePriority.UseFont = false;
            this.lblAccExistC.StylePriority.UseForeColor = false;
            this.lblAccExistC.Text = "Account Exists";
            this.lblAccExistC.Weight = 0.84487513064197384;
            // 
            // lblAccExistR
            // 
            this.lblAccExistR.BackColor = System.Drawing.Color.White;
            this.lblAccExistR.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblAccExistR.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblAccExistR.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "return.accexistdesc")});
            this.lblAccExistR.ForeColor = System.Drawing.Color.DimGray;
            this.lblAccExistR.Name = "lblAccExistR";
            this.lblAccExistR.StylePriority.UseBackColor = false;
            this.lblAccExistR.StylePriority.UseBorderColor = false;
            this.lblAccExistR.StylePriority.UseBorders = false;
            this.lblAccExistR.StylePriority.UseForeColor = false;
            this.lblAccExistR.StylePriority.UseTextAlignment = false;
            this.lblAccExistR.Text = "lblAccExistR";
            this.lblAccExistR.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblAccExistR.Weight = 0.78673023099534245;
            // 
            // lblAccExistI
            // 
            this.lblAccExistI.BackColor = System.Drawing.Color.White;
            this.lblAccExistI.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblAccExistI.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblAccExistI.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.AccountNo")});
            this.lblAccExistI.ForeColor = System.Drawing.Color.DimGray;
            this.lblAccExistI.Name = "lblAccExistI";
            this.lblAccExistI.StylePriority.UseBackColor = false;
            this.lblAccExistI.StylePriority.UseBorderColor = false;
            this.lblAccExistI.StylePriority.UseBorders = false;
            this.lblAccExistI.StylePriority.UseForeColor = false;
            this.lblAccExistI.StylePriority.UseTextAlignment = false;
            this.lblAccExistI.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblAccExistI.Weight = 0.84043411204689411;
            // 
            // xrTableRow47
            // 
            this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblInitialsH,
            this.lblInitialsR,
            this.lblInitialsI});
            this.xrTableRow47.Name = "xrTableRow47";
            this.xrTableRow47.Weight = 0.79999999999999993;
            // 
            // lblInitialsH
            // 
            this.lblInitialsH.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblInitialsH.BorderColor = System.Drawing.Color.White;
            this.lblInitialsH.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblInitialsH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInitialsH.ForeColor = System.Drawing.Color.Gray;
            this.lblInitialsH.Name = "lblInitialsH";
            this.lblInitialsH.StylePriority.UseBackColor = false;
            this.lblInitialsH.StylePriority.UseBorderColor = false;
            this.lblInitialsH.StylePriority.UseBorders = false;
            this.lblInitialsH.StylePriority.UseFont = false;
            this.lblInitialsH.StylePriority.UseForeColor = false;
            this.lblInitialsH.Text = "Initials Match";
            this.lblInitialsH.Weight = 0.84487513064197384;
            // 
            // lblInitialsR
            // 
            this.lblInitialsR.BackColor = System.Drawing.Color.White;
            this.lblInitialsR.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblInitialsR.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblInitialsR.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "return.initialsmatchdesc")});
            this.lblInitialsR.ForeColor = System.Drawing.Color.DimGray;
            this.lblInitialsR.Name = "lblInitialsR";
            this.lblInitialsR.StylePriority.UseBackColor = false;
            this.lblInitialsR.StylePriority.UseBorderColor = false;
            this.lblInitialsR.StylePriority.UseBorders = false;
            this.lblInitialsR.StylePriority.UseForeColor = false;
            this.lblInitialsR.StylePriority.UseTextAlignment = false;
            this.lblInitialsR.Text = "lblInitialsR";
            this.lblInitialsR.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblInitialsR.Weight = 0.78673023099534245;
            // 
            // lblInitialsI
            // 
            this.lblInitialsI.BackColor = System.Drawing.Color.White;
            this.lblInitialsI.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblInitialsI.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblInitialsI.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.Initials")});
            this.lblInitialsI.ForeColor = System.Drawing.Color.DimGray;
            this.lblInitialsI.Name = "lblInitialsI";
            this.lblInitialsI.StylePriority.UseBackColor = false;
            this.lblInitialsI.StylePriority.UseBorderColor = false;
            this.lblInitialsI.StylePriority.UseBorders = false;
            this.lblInitialsI.StylePriority.UseForeColor = false;
            this.lblInitialsI.StylePriority.UseTextAlignment = false;
            this.lblInitialsI.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblInitialsI.Weight = 0.84043411204689411;
            // 
            // xrTableRow46
            // 
            this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSurNameC,
            this.lblSurNameR,
            this.lblSurNameI});
            this.xrTableRow46.Name = "xrTableRow46";
            this.xrTableRow46.Weight = 0.79999999999999993;
            // 
            // lblSurNameC
            // 
            this.lblSurNameC.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblSurNameC.BorderColor = System.Drawing.Color.White;
            this.lblSurNameC.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSurNameC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSurNameC.ForeColor = System.Drawing.Color.Gray;
            this.lblSurNameC.Name = "lblSurNameC";
            this.lblSurNameC.StylePriority.UseBackColor = false;
            this.lblSurNameC.StylePriority.UseBorderColor = false;
            this.lblSurNameC.StylePriority.UseBorders = false;
            this.lblSurNameC.StylePriority.UseFont = false;
            this.lblSurNameC.StylePriority.UseForeColor = false;
            this.lblSurNameC.Text = "Surname/ Company Name";
            this.lblSurNameC.Weight = 0.84487513064197384;
            // 
            // lblSurNameR
            // 
            this.lblSurNameR.BackColor = System.Drawing.Color.White;
            this.lblSurNameR.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblSurNameR.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblSurNameR.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "return.surnamematchdesc")});
            this.lblSurNameR.ForeColor = System.Drawing.Color.DimGray;
            this.lblSurNameR.Name = "lblSurNameR";
            this.lblSurNameR.StylePriority.UseBackColor = false;
            this.lblSurNameR.StylePriority.UseBorderColor = false;
            this.lblSurNameR.StylePriority.UseBorders = false;
            this.lblSurNameR.StylePriority.UseForeColor = false;
            this.lblSurNameR.StylePriority.UseTextAlignment = false;
            this.lblSurNameR.Text = "lblSurNameR";
            this.lblSurNameR.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSurNameR.Weight = 0.78673023099534245;
            // 
            // lblSurNameI
            // 
            this.lblSurNameI.BackColor = System.Drawing.Color.White;
            this.lblSurNameI.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblSurNameI.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblSurNameI.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.SurName")});
            this.lblSurNameI.ForeColor = System.Drawing.Color.DimGray;
            this.lblSurNameI.Name = "lblSurNameI";
            this.lblSurNameI.StylePriority.UseBackColor = false;
            this.lblSurNameI.StylePriority.UseBorderColor = false;
            this.lblSurNameI.StylePriority.UseBorders = false;
            this.lblSurNameI.StylePriority.UseForeColor = false;
            this.lblSurNameI.StylePriority.UseTextAlignment = false;
            this.lblSurNameI.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSurNameI.Weight = 0.84043411204689411;
            // 
            // xrTableRow45
            // 
            this.xrTableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblIDNoC,
            this.lblIDNoR,
            this.lblIDNoI});
            this.xrTableRow45.Name = "xrTableRow45";
            this.xrTableRow45.Weight = 0.8;
            // 
            // lblIDNoC
            // 
            this.lblIDNoC.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblIDNoC.BorderColor = System.Drawing.Color.White;
            this.lblIDNoC.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblIDNoC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIDNoC.ForeColor = System.Drawing.Color.Gray;
            this.lblIDNoC.Name = "lblIDNoC";
            this.lblIDNoC.StylePriority.UseBackColor = false;
            this.lblIDNoC.StylePriority.UseBorderColor = false;
            this.lblIDNoC.StylePriority.UseBorders = false;
            this.lblIDNoC.StylePriority.UseFont = false;
            this.lblIDNoC.StylePriority.UseForeColor = false;
            this.lblIDNoC.Text = "ID Number/ Company Registration Number";
            this.lblIDNoC.Weight = 0.84487513064197384;
            // 
            // lblIDNoR
            // 
            this.lblIDNoR.BackColor = System.Drawing.Color.White;
            this.lblIDNoR.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblIDNoR.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblIDNoR.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "return.idmatchdesc")});
            this.lblIDNoR.ForeColor = System.Drawing.Color.DimGray;
            this.lblIDNoR.Name = "lblIDNoR";
            this.lblIDNoR.StylePriority.UseBackColor = false;
            this.lblIDNoR.StylePriority.UseBorderColor = false;
            this.lblIDNoR.StylePriority.UseBorders = false;
            this.lblIDNoR.StylePriority.UseForeColor = false;
            this.lblIDNoR.StylePriority.UseTextAlignment = false;
            this.lblIDNoR.Text = "lblIDNoR";
            this.lblIDNoR.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblIDNoR.Weight = 0.78673023099534245;
            // 
            // lblIDNoI
            // 
            this.lblIDNoI.BackColor = System.Drawing.Color.White;
            this.lblIDNoI.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblIDNoI.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblIDNoI.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.IDNo")});
            this.lblIDNoI.ForeColor = System.Drawing.Color.DimGray;
            this.lblIDNoI.Name = "lblIDNoI";
            this.lblIDNoI.StylePriority.UseBackColor = false;
            this.lblIDNoI.StylePriority.UseBorderColor = false;
            this.lblIDNoI.StylePriority.UseBorders = false;
            this.lblIDNoI.StylePriority.UseForeColor = false;
            this.lblIDNoI.StylePriority.UseTextAlignment = false;
            this.lblIDNoI.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblIDNoI.Weight = 0.84043411204689411;
            // 
            // xrTableRow44
            // 
            this.xrTableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblAccStatusC,
            this.lblAccStatusR,
            this.lblAccStatusI});
            this.xrTableRow44.Name = "xrTableRow44";
            this.xrTableRow44.Weight = 0.8;
            // 
            // lblAccStatusC
            // 
            this.lblAccStatusC.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblAccStatusC.BorderColor = System.Drawing.Color.White;
            this.lblAccStatusC.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblAccStatusC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAccStatusC.ForeColor = System.Drawing.Color.Gray;
            this.lblAccStatusC.Name = "lblAccStatusC";
            this.lblAccStatusC.StylePriority.UseBackColor = false;
            this.lblAccStatusC.StylePriority.UseBorderColor = false;
            this.lblAccStatusC.StylePriority.UseBorders = false;
            this.lblAccStatusC.StylePriority.UseFont = false;
            this.lblAccStatusC.StylePriority.UseForeColor = false;
            this.lblAccStatusC.Text = "Account Active";
            this.lblAccStatusC.Weight = 0.84487513064197384;
            // 
            // lblAccStatusR
            // 
            this.lblAccStatusR.BackColor = System.Drawing.Color.White;
            this.lblAccStatusR.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblAccStatusR.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblAccStatusR.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "return.accactivedesc")});
            this.lblAccStatusR.ForeColor = System.Drawing.Color.DimGray;
            this.lblAccStatusR.Name = "lblAccStatusR";
            this.lblAccStatusR.StylePriority.UseBackColor = false;
            this.lblAccStatusR.StylePriority.UseBorderColor = false;
            this.lblAccStatusR.StylePriority.UseBorders = false;
            this.lblAccStatusR.StylePriority.UseForeColor = false;
            this.lblAccStatusR.StylePriority.UseTextAlignment = false;
            this.lblAccStatusR.Text = "lblAccStatusR";
            this.lblAccStatusR.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblAccStatusR.Weight = 0.78673023099534245;
            // 
            // lblAccStatusI
            // 
            this.lblAccStatusI.BackColor = System.Drawing.Color.White;
            this.lblAccStatusI.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblAccStatusI.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblAccStatusI.ForeColor = System.Drawing.Color.DimGray;
            this.lblAccStatusI.Name = "lblAccStatusI";
            this.lblAccStatusI.StylePriority.UseBackColor = false;
            this.lblAccStatusI.StylePriority.UseBorderColor = false;
            this.lblAccStatusI.StylePriority.UseBorders = false;
            this.lblAccStatusI.StylePriority.UseForeColor = false;
            this.lblAccStatusI.StylePriority.UseTextAlignment = false;
            this.lblAccStatusI.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblAccStatusI.Weight = 0.84043411204689411;
            // 
            // DetailReport2
            // 
            this.DetailReport2.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail3});
            this.DetailReport2.DataMember = "SubscriberInputDetails";
            this.DetailReport2.Level = 1;
            this.DetailReport2.Name = "DetailReport2";
            // 
            // Detail3
            // 
            this.Detail3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6,
            this.tblUserInput});
            this.Detail3.HeightF = 143F;
            this.Detail3.Name = "Detail3";
            // 
            // xrTable6
            // 
            this.xrTable6.BorderColor = System.Drawing.Color.White;
            this.xrTable6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(8F, 46F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow5});
            this.xrTable6.SizeF = new System.Drawing.SizeF(792F, 80F);
            this.xrTable6.StylePriority.UseBorderColor = false;
            this.xrTable6.StylePriority.UseBorders = false;
            this.xrTable6.StylePriority.UseFont = false;
            this.xrTable6.StylePriority.UsePadding = false;
            this.xrTable6.StylePriority.UseTextAlignment = false;
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblIAccNoC,
            this.lblIAccNoD});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 0.8;
            // 
            // lblIAccNoC
            // 
            this.lblIAccNoC.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblIAccNoC.BorderColor = System.Drawing.Color.White;
            this.lblIAccNoC.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblIAccNoC.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIAccNoC.ForeColor = System.Drawing.Color.Gray;
            this.lblIAccNoC.Name = "lblIAccNoC";
            this.lblIAccNoC.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblIAccNoC.StylePriority.UseBackColor = false;
            this.lblIAccNoC.StylePriority.UseBorderColor = false;
            this.lblIAccNoC.StylePriority.UseBorders = false;
            this.lblIAccNoC.StylePriority.UseFont = false;
            this.lblIAccNoC.StylePriority.UseForeColor = false;
            this.lblIAccNoC.StylePriority.UsePadding = false;
            this.lblIAccNoC.Text = "Account Number";
            this.lblIAccNoC.Weight = 0.59755134281200639;
            // 
            // lblIAccNoD
            // 
            this.lblIAccNoD.BackColor = System.Drawing.Color.White;
            this.lblIAccNoD.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblIAccNoD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblIAccNoD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.AccountNo")});
            this.lblIAccNoD.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblIAccNoD.ForeColor = System.Drawing.Color.DimGray;
            this.lblIAccNoD.Name = "lblIAccNoD";
            this.lblIAccNoD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblIAccNoD.StylePriority.UseBackColor = false;
            this.lblIAccNoD.StylePriority.UseBorderColor = false;
            this.lblIAccNoD.StylePriority.UseBorders = false;
            this.lblIAccNoD.StylePriority.UseFont = false;
            this.lblIAccNoD.StylePriority.UseForeColor = false;
            this.lblIAccNoD.StylePriority.UsePadding = false;
            this.lblIAccNoD.Text = "lblEnquiryDateValue";
            this.lblIAccNoD.Weight = 2.3910850208243573;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblIBranchCodeC,
            this.lblIBranchCodeD});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 0.79999999999999993;
            // 
            // lblIBranchCodeC
            // 
            this.lblIBranchCodeC.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblIBranchCodeC.BorderColor = System.Drawing.Color.White;
            this.lblIBranchCodeC.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblIBranchCodeC.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIBranchCodeC.ForeColor = System.Drawing.Color.Gray;
            this.lblIBranchCodeC.Name = "lblIBranchCodeC";
            this.lblIBranchCodeC.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblIBranchCodeC.StylePriority.UseBackColor = false;
            this.lblIBranchCodeC.StylePriority.UseBorderColor = false;
            this.lblIBranchCodeC.StylePriority.UseBorders = false;
            this.lblIBranchCodeC.StylePriority.UseFont = false;
            this.lblIBranchCodeC.StylePriority.UseForeColor = false;
            this.lblIBranchCodeC.StylePriority.UsePadding = false;
            this.lblIBranchCodeC.Text = "Branch Code";
            this.lblIBranchCodeC.Weight = 0.59755134281200639;
            // 
            // lblIBranchCodeD
            // 
            this.lblIBranchCodeD.BackColor = System.Drawing.Color.White;
            this.lblIBranchCodeD.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblIBranchCodeD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblIBranchCodeD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.BranchCode")});
            this.lblIBranchCodeD.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblIBranchCodeD.ForeColor = System.Drawing.Color.DimGray;
            this.lblIBranchCodeD.Name = "lblIBranchCodeD";
            this.lblIBranchCodeD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblIBranchCodeD.StylePriority.UseBackColor = false;
            this.lblIBranchCodeD.StylePriority.UseBorderColor = false;
            this.lblIBranchCodeD.StylePriority.UseBorders = false;
            this.lblIBranchCodeD.StylePriority.UseFont = false;
            this.lblIBranchCodeD.StylePriority.UseForeColor = false;
            this.lblIBranchCodeD.StylePriority.UsePadding = false;
            this.lblIBranchCodeD.Text = "xrTableCell14";
            this.lblIBranchCodeD.Weight = 2.3910850208243573;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblIAccTypeC,
            this.lblIAccTypeD});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 0.79999999999999993;
            // 
            // lblIAccTypeC
            // 
            this.lblIAccTypeC.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblIAccTypeC.BorderColor = System.Drawing.Color.White;
            this.lblIAccTypeC.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblIAccTypeC.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIAccTypeC.ForeColor = System.Drawing.Color.Gray;
            this.lblIAccTypeC.Name = "lblIAccTypeC";
            this.lblIAccTypeC.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblIAccTypeC.StylePriority.UseBackColor = false;
            this.lblIAccTypeC.StylePriority.UseBorderColor = false;
            this.lblIAccTypeC.StylePriority.UseBorders = false;
            this.lblIAccTypeC.StylePriority.UseFont = false;
            this.lblIAccTypeC.StylePriority.UseForeColor = false;
            this.lblIAccTypeC.StylePriority.UsePadding = false;
            this.lblIAccTypeC.Text = "Account Type";
            this.lblIAccTypeC.Weight = 0.59755134281200639;
            // 
            // lblIAccTypeD
            // 
            this.lblIAccTypeD.BackColor = System.Drawing.Color.White;
            this.lblIAccTypeD.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblIAccTypeD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblIAccTypeD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.AccountType")});
            this.lblIAccTypeD.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblIAccTypeD.ForeColor = System.Drawing.Color.DimGray;
            this.lblIAccTypeD.Name = "lblIAccTypeD";
            this.lblIAccTypeD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblIAccTypeD.StylePriority.UseBackColor = false;
            this.lblIAccTypeD.StylePriority.UseBorderColor = false;
            this.lblIAccTypeD.StylePriority.UseBorders = false;
            this.lblIAccTypeD.StylePriority.UseFont = false;
            this.lblIAccTypeD.StylePriority.UseForeColor = false;
            this.lblIAccTypeD.StylePriority.UsePadding = false;
            this.lblIAccTypeD.Text = "xrTableCell18";
            this.lblIAccTypeD.Weight = 2.3910850208243573;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblIBankNameC,
            this.lblIBankNameD});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 0.8;
            // 
            // lblIBankNameC
            // 
            this.lblIBankNameC.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblIBankNameC.BorderColor = System.Drawing.Color.White;
            this.lblIBankNameC.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblIBankNameC.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIBankNameC.ForeColor = System.Drawing.Color.Gray;
            this.lblIBankNameC.Name = "lblIBankNameC";
            this.lblIBankNameC.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblIBankNameC.StylePriority.UseBackColor = false;
            this.lblIBankNameC.StylePriority.UseBorderColor = false;
            this.lblIBankNameC.StylePriority.UseBorders = false;
            this.lblIBankNameC.StylePriority.UseFont = false;
            this.lblIBankNameC.StylePriority.UseForeColor = false;
            this.lblIBankNameC.StylePriority.UsePadding = false;
            this.lblIBankNameC.Text = "Bank Name";
            this.lblIBankNameC.Weight = 0.59755134281200639;
            // 
            // lblIBankNameD
            // 
            this.lblIBankNameD.BackColor = System.Drawing.Color.White;
            this.lblIBankNameD.BorderColor = System.Drawing.Color.Gainsboro;
            this.lblIBankNameD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblIBankNameD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.BankName")});
            this.lblIBankNameD.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblIBankNameD.ForeColor = System.Drawing.Color.DimGray;
            this.lblIBankNameD.Name = "lblIBankNameD";
            this.lblIBankNameD.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.lblIBankNameD.StylePriority.UseBackColor = false;
            this.lblIBankNameD.StylePriority.UseBorderColor = false;
            this.lblIBankNameD.StylePriority.UseBorders = false;
            this.lblIBankNameD.StylePriority.UseFont = false;
            this.lblIBankNameD.StylePriority.UseForeColor = false;
            this.lblIBankNameD.StylePriority.UsePadding = false;
            this.lblIBankNameD.Text = "xrTableCell22";
            this.lblIBankNameD.Weight = 2.3910850208243573;
            // 
            // tblUserInput
            // 
            this.tblUserInput.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.tblUserInput.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblUserInput.LocationFloat = new DevExpress.Utils.PointFloat(8F, 6F);
            this.tblUserInput.Name = "tblUserInput";
            this.tblUserInput.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.tblUserInput.SizeF = new System.Drawing.SizeF(792F, 33F);
            this.tblUserInput.StylePriority.UseBorders = false;
            this.tblUserInput.StylePriority.UseFont = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(216)))), ((int)(((byte)(22)))));
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell1.BorderWidth = 2;
            this.xrTableCell1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.xrTableCell1.ForeColor = System.Drawing.Color.Gray;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 2, 2, 100F);
            this.xrTableCell1.StylePriority.UseBorderColor = false;
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.StylePriority.UseBorderWidth = false;
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.StylePriority.UseForeColor = false;
            this.xrTableCell1.StylePriority.UsePadding = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "User Input";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell1.Weight = 3;
            // 
            // AccountVerificationReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.PageFooter,
            this.DetailReport,
            this.DetailReport1,
            this.DetailReport2});
            this.Margins = new System.Drawing.Printing.Margins(20, 21, 6, 20);
            this.Version = "9.3";
            this.XmlDataPath = "C:\\Documents and Settings\\skothamasu\\Desktop\\AccXML.xml";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblresult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblUserInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox2;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo4;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo3;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell lblEnquiryDateValue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport1;
        private DevExpress.XtraReports.UI.DetailBand Detail2;
        private DevExpress.XtraReports.UI.XRTable tblresult;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell lblDescH;
        private DevExpress.XtraReports.UI.XRTableCell lblResultH;
        private DevExpress.XtraReports.UI.XRTableCell lblInputH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell lblAccExistC;
        private DevExpress.XtraReports.UI.XRTableCell lblAccExistI;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow47;
        private DevExpress.XtraReports.UI.XRTableCell lblInitialsH;
        private DevExpress.XtraReports.UI.XRTableCell lblInitialsR;
        private DevExpress.XtraReports.UI.XRTableCell lblInitialsI;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow46;
        private DevExpress.XtraReports.UI.XRTableCell lblSurNameC;
        private DevExpress.XtraReports.UI.XRTableCell lblSurNameR;
        private DevExpress.XtraReports.UI.XRTableCell lblSurNameI;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow45;
        private DevExpress.XtraReports.UI.XRTableCell lblIDNoC;
        private DevExpress.XtraReports.UI.XRTableCell lblIDNoR;
        private DevExpress.XtraReports.UI.XRTableCell lblIDNoI;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow44;
        private DevExpress.XtraReports.UI.XRTableCell lblAccStatusC;
        private DevExpress.XtraReports.UI.XRTableCell lblAccStatusR;
        private DevExpress.XtraReports.UI.XRTableCell lblAccStatusI;
        private DevExpress.XtraReports.UI.XRTableCell lblAccExistR;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport2;
        private DevExpress.XtraReports.UI.DetailBand Detail3;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell lblIAccNoC;
        private DevExpress.XtraReports.UI.XRTableCell lblIAccNoD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell lblIBranchCodeC;
        private DevExpress.XtraReports.UI.XRTableCell lblIBranchCodeD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell lblIAccTypeC;
        private DevExpress.XtraReports.UI.XRTableCell lblIAccTypeD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell lblIBankNameC;
        private DevExpress.XtraReports.UI.XRTableCell lblIBankNameD;
        private DevExpress.XtraReports.UI.XRTable tblUserInput;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell lblEnquiryStatusC;
        private DevExpress.XtraReports.UI.XRTableCell lblEnquiryStatusD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell lblXDSRefNoC;
        private DevExpress.XtraReports.UI.XRTableCell lblXDSRefNoD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell lblexternalRefNoC;
        private DevExpress.XtraReports.UI.XRTableCell lblexternalRefNoD;
    }
}
