using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace XdsPortalReports
{
    public partial class ConsumerPrescreenTraceReport : DevExpress.XtraReports.UI.XtraReport
    {
        public ConsumerPrescreenTraceReport(string strXML)
        {
            InitializeComponent();
            System.IO.StringReader strReader = new System.IO.StringReader(strXML);
            DataSet dsXML = new DataSet();
            dsXML.ReadXml(strReader);

            if (dsXML.Tables.Contains("ConsumerAdverseInfo"))
            {
                dsXML.Tables["ConsumerAdverseInfo"].Columns.Add("FActionDate", System.Type.GetType("System.DateTime"), "Iif(ActionDate = '' , '1900-01-01T00:00:00', ActionDate)");
                this.xrTableCell115.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerAdverseInfo.FActionDate", "{0: dd-MM-yyyy}") });
            }
            this.DataSource = dsXML;
        }

    }
}
