﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace XDSPortalEnquiry.Business
{
   public class xdsBilling
    {

        public xdsBilling()
        {
        }

        public XDSPortalEnquiry.Entity.SubscriberProductReports GetPrice(string conString, int SubscriberID, int productID)
        {
            XDSPortalEnquiry.Entity.SubscriberProductReports objSubscriberProductReports = new XDSPortalEnquiry.Entity.SubscriberProductReports();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "select * from SubscriberProductReports where SubscriberID = " + SubscriberID + " and productID = " + productID + " and Active = 1";

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSubscriberProductReports.BillingTypeID = Convert.ToInt32(Dr["BillingTypeID"]);
                objSubscriberProductReports.ProductID = Convert.ToInt32(Dr["ProductID"]);
                objSubscriberProductReports.ReportID = Convert.ToInt32(Dr["ReportID"]);
                objSubscriberProductReports.SubscriberID = Convert.ToInt32(Dr["SubscriberID"]);
                objSubscriberProductReports.SubscriberProductReportID = Convert.ToInt32(Dr["SubscriberProductReportID"]);
                objSubscriberProductReports.UnitPrice = Convert.ToDouble(Dr["UnitPrice"]);
              
            }
            Objsqlcom.Dispose();
            objsqladp.Dispose();
            ds.Dispose();
            return objSubscriberProductReports;
        }


        public XDSPortalEnquiry.Entity.SubscriberProductReports GetPrice(SqlConnection ObjConstring, int SubscriberID, int productID)
        {
            XDSPortalEnquiry.Entity.SubscriberProductReports objSubscriberProductReports = new XDSPortalEnquiry.Entity.SubscriberProductReports();

            string sqlSelect = "select * from SubscriberProductReports where SubscriberID = " + SubscriberID + " and productID = " + productID + " and Active = 1";
            
            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSubscriberProductReports.BillingTypeID = Convert.ToInt32(Dr["BillingTypeID"]);
                objSubscriberProductReports.ProductID = Convert.ToInt32(Dr["ProductID"]);
                objSubscriberProductReports.ReportID = Convert.ToInt32(Dr["ReportID"]);
                objSubscriberProductReports.SubscriberID = Convert.ToInt32(Dr["SubscriberID"]);
                objSubscriberProductReports.SubscriberProductReportID = Convert.ToInt32(Dr["SubscriberProductReportID"]);
                objSubscriberProductReports.UnitPrice = Convert.ToDouble(Dr["UnitPrice"]);

            }
            Objsqlcom.Dispose();
            objsqladp.Dispose();
            ds.Dispose();
            return objSubscriberProductReports;
        }
       
       
    }
}
