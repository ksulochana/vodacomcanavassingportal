﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalEnquiry.Business
{
    public class NLR_Closures
    {
        XDSPortalLibrary.Entity_Layer.NLRDailyClosures eoNLRDailyClosures;

        public NLR_Closures()
        {
            eoNLRDailyClosures = new XDSPortalLibrary.Entity_Layer.NLRDailyClosures();
        }

        public XDSPortalLibrary.Entity_Layer.Response InsertLoanClosures(SqlConnection con, SqlConnection AdminConnection, int intEnqSubscriberID, int intProductID, int intSystemUserID, string strLoanRegNo, string strSupplierRefNo, string strEnquiryRefNo, string strConsumerIdNo, string strPassportNo, string strConsumerBirthdate, string strSurname, string strForename1, string strForename2, string strForename3, string strBranchCode, string strAccno, string strSubAccno, string strDateOFcLOSURE, string strStatusCode, string strVoucherCode, string strExternalReference)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            string strValidationStatus = string.Empty;
            double Totalcost = 0;
            string rXml = "";
            DataSet ds = new DataSet();


            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            Data.NLR dNLR = new XDSPortalEnquiry.Data.NLR();


            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {



                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intEnqSubscriberID, intProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intEnqSubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);



                //Data.DefaultAlerts oDefaultAlerts = new XDSPortalEnquiry.Data.DefaultAlerts();


                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {

                    // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML 
                    // use this XML data for generating the Report 

                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {

                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intEnqSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1)
                    {
                        //Calculate The Total cost of the Report , including Bonus Segments

                        Totalcost = spr.UnitPrice;


                    }


                    if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                    {

                        eoNLRDailyClosures.SupplierRefNo = strSupplierRefNo;
                        eoNLRDailyClosures.NLREnquiryReferenceNumber = strEnquiryRefNo;
                        eoNLRDailyClosures.Surname = strSurname;
                        eoNLRDailyClosures.Forename1 = strForename1;
                        eoNLRDailyClosures.Forename2 = strForename2;
                        eoNLRDailyClosures.Forename3 = strForename3;
                        eoNLRDailyClosures.DateOfBirth = strConsumerBirthdate;
                        eoNLRDailyClosures.SAIDNumber = strConsumerIdNo;
                        eoNLRDailyClosures.BranchCode = strBranchCode;
                        eoNLRDailyClosures.AccountNumber = strAccno;
                        eoNLRDailyClosures.SubAccountNumber = strSubAccno;
                        eoNLRDailyClosures.PassportNo = strPassportNo;
                        eoNLRDailyClosures.EnquirySubscriberID = intEnqSubscriberID;
                        eoNLRDailyClosures.Username = sys.SystemUserFullName;
                        eoNLRDailyClosures.SubscriberName = sub.SubscriberName;
                        eoNLRDailyClosures.SystemUserID = sys.SystemUserID;
                        eoNLRDailyClosures.ChangedByUser = sys.SystemUserFullName;
                        eoNLRDailyClosures.StatusInd = XDSPortalLibrary.Entity_Layer.NLRDailyRegistrations.EnquiryStatusInd.P.ToString();
                        eoNLRDailyClosures.VoucherCode = strVoucherCode;
                        eoNLRDailyClosures.SubscriberReference = strExternalReference;
                        eoNLRDailyClosures.Billable = false;
                        eoNLRDailyClosures.Payasyougo = sub.PayAsYouGo;
                        eoNLRDailyClosures.productID = intProductID;
                        eoNLRDailyClosures.BillingtypeId = spr.BillingTypeID;
                        eoNLRDailyClosures.subscriberEnquiryDate = DateTime.Now;
                        eoNLRDailyClosures.CreatedOnDate = DateTime.Now;
                        eoNLRDailyClosures.StatusCode = strStatusCode;
                        eoNLRDailyClosures.DateOfClosureCancellation = strDateOFcLOSURE;
                        eoNLRDailyClosures.NLRLoanRegistrationNumber = strLoanRegNo;


                        eoNLRDailyClosures.NLRDailyRegistrationsLogID = dNLR.InsertLog(con, eoNLRDailyClosures);

                        string constr = dNLR.GetNLRDailyRegistrationsLogObject(con, eoNLRDailyClosures.NLRDailyRegistrationsLogID);
                        SqlConnection objConstring = new SqlConnection(constr);

                        dNLR.ExecValidations(objConstring, eoNLRDailyClosures);

                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, constr);
                        rp = ra.NLRClosure(eoNLRDailyClosures);


                        // Get Response data
                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eoNLRDailyClosures.StatusInd = "F";
                            eoNLRDailyClosures.ErrorDescription = rp.ResponseData.ToString();
                            eoNLRDailyClosures.ChangedByUser = sys.Username;
                            eoNLRDailyClosures.ChangedOnDate = DateTime.Now;
                            dNLR.UpdateError(con, eoNLRDailyClosures);
                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            eoNLRDailyClosures.StatusInd = "C";
                            eoNLRDailyClosures.NLRDailyClosuresID = rp.ResponseKey;
                            eoNLRDailyClosures.Billable = false;
                            eoNLRDailyClosures.DetailsViewedYN = true;
                            eoNLRDailyClosures.XMLData = rp.ResponseData;
                            eoNLRDailyClosures.ChangedOnDate = DateTime.Now;
                            eoNLRDailyClosures.ChangedByUser = sys.Username;
                            eoNLRDailyClosures.Detailsvieweddate = DateTime.Now;

                            dNLR.UpdateResult(con, eoNLRDailyClosures);

                            if (!(string.IsNullOrEmpty(strVoucherCode)))
                            {
                                dSV.UpdateSubscriberVoucher(AdminConnection, strVoucherCode, eoNLRDailyClosures.CreatedByUser);
                            }
                        }

                    }
                    else
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                    }


                }

                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";

                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception ex)
            {
                eoNLRDailyClosures.StatusInd = "F";
                eoNLRDailyClosures.ErrorDescription = ex.Message;
                eoNLRDailyClosures.NLRDailyRegistrationsLogID = dNLR.UpdateError(con, eoNLRDailyClosures);

                rp.EnquiryID = eoNLRDailyClosures.NLRDailyRegistrationsLogID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = ex.Message;

                if (ex.Message.Contains("A .NET Framework"))
                {
                    rp.ResponseData = "Error: The System is unable to process this request. Please contact XDS Support";
                }

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }

            return rp;
        }


    }
}
