﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalEnquiry.Business
{
    public class BankingEnquiry_AccountVerificationNed
    {
        private XDSPortalLibrary.Business_Layer.AccountVerificationNed moAccountVerificationWseManager;
        private XDSPortalLibrary.Entity_Layer.AccountVerificationNed eoAccountVerificationWseManager;

        public BankingEnquiry_AccountVerificationNed()
        {
            moAccountVerificationWseManager = new XDSPortalLibrary.Business_Layer.AccountVerificationNed();
            eoAccountVerificationWseManager = new XDSPortalLibrary.Entity_Layer.AccountVerificationNed();
        }
        public void ValidateInput(Entity.SubscriberEnquiryLog Validate)
        {
            if (string.IsNullOrEmpty(Validate.BankName) || string.IsNullOrEmpty(Validate.BranchName) || string.IsNullOrEmpty(Validate.BranchCode) || string.IsNullOrEmpty(Validate.AccountNo) || string.IsNullOrEmpty(Validate.EntityType))
            {
                throw new Exception("Please enter data to all mandatory fields");
            }

            else if (!string.IsNullOrEmpty(Validate.EntityType))
            {
                if (Validate.EntityType.ToLower().Contains("company") && ((Validate.RegNumber == "//" )|| string.IsNullOrEmpty(Validate.SurName)))
                {
                    throw new Exception("Registration no and name are mandatory for the selected entity");
                }
                else if (Validate.EntityType.ToLower().Contains("sole") && (string.IsNullOrEmpty(Validate.IDNo) || string.IsNullOrEmpty(Validate.SurName)))
                {
                    throw new Exception("ID no,Companyname are mandatory for the selected entity");
                }
                else if (Validate.EntityType.ToLower().Contains("trust") && (string.IsNullOrEmpty(Validate.Trust) || string.IsNullOrEmpty(Validate.SurName)))
                {
                    throw new Exception("Trust no and name are mandatory for the selected entity");
                }
                else if (Validate.EntityType.ToLower().Contains("individual") && (string.IsNullOrEmpty(Validate.IDNo) || string.IsNullOrEmpty(Validate.FirstName) || string.IsNullOrEmpty(Validate.SurName)))
                {
                    throw new Exception("IDno,Firstname and initals are mandatory for the selected entity");
                }
                else if (string.IsNullOrEmpty(Validate.AccHolder) || string.IsNullOrEmpty(Validate.BusBusinessName) || string.IsNullOrEmpty(Validate.EmailAddress))
                {
                    throw new Exception("FirstName, SurName, Email Address are Mandatory");
                }
            }           
        }
        public XDSPortalLibrary.Entity_Layer.Response SubmitAccountVerification(SqlConnection con, SqlConnection AdminConnection,SqlConnection AVSConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName,string TypeOfVerification, string strEntityType, string strRegNo1, string strRegNo2, string strRegNo3, string Trust, string strIDNo, string strSurName, string strInitials, string strAccNo, string strBranchCode, string strAcctype, string strBankName, string ClientFirstName, string ClientSurName, string strEmailAddress, bool bConfirmationChkBox, string strExtRef, bool bBonusChecking, string strVoucherCode)
        {

            
            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            int intSubscriberEnquiryLogID = 0;
            string rXml = "";
            int MatchResult = 0;
            DataSet ds = new DataSet();
            string strValidationStatus = "";
            Boolean boolSubmitEnquiry = true;
            DataSet dsLogon = new DataSet();
            DataSet dsBankName = new DataSet();
            DataSet dsAcctype = new DataSet();

           


            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            Entity.SubscriberEnquiryLog sel = new XDSPortalEnquiry.Entity.SubscriberEnquiryLog();
            Data.SubscriberEnquiryLog dsel = new XDSPortalEnquiry.Data.SubscriberEnquiryLog();


            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            try
            {
                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {


                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }


                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {
                        strEntityType = TypeOfVerification.ToLower() == "individual" ? "Individual" : ((TypeOfVerification.ToLower() == "company" && strEntityType.ToLower() == "none") ? "" : strEntityType);

                        sel.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        sel.SubscriberID = intSubscriberID;
                        sel.ProductID = intProductId;
                        sel.SubscriberEnquiryDate = DateTime.Now;
                        sel.SubscriberName = strSubscriberName;
                        sel.SubscriberReference = strExtRef;
                        sel.SystemUserID = intSystemUserID;
                        sel.BusRegistrationNo = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        sel.IDNo = strIDNo;
                        //sel.BankName = strBankName;
                        sel.BankName = strBankName;
                        sel.AccountNo = strAccNo;
                        sel.BranchCode = strBranchCode;
                        sel.FirstName = strInitials;
                        sel.SurName = strSurName;
                        sel.EmailAddress = strEmailAddress;
                        //sel.BranchName = strAcctype;
                        sel.BranchName = strAcctype;
                        sel.SystemUserID = intSystemUserID;
                        sel.SystemUserName = sys.SystemUserFullName;
                        sel.CreatedByUser = sys.Username;
                        sel.CreatedOnDate = DateTime.Now;
                        sel.Searchinput = "";
                        sel.KeyType = "F";
                        sel.AccHolder = ClientFirstName;
                        sel.BusBusinessName = ClientSurName;
                        sel.Trust = Trust;
                        sel.EntityType = strEntityType;
                        
                        //sel.KeyType = 

                        ValidateInput(sel);

                        if (!string.IsNullOrEmpty(sel.IDNo))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.IDNo;
                        }
                        if (!string.IsNullOrEmpty(sel.BusRegistrationNo))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        }
                        if (!string.IsNullOrEmpty(sel.AccountNo))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.AccountNo;
                        }
                        if (!string.IsNullOrEmpty(sel.SurName))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.SurName;
                        }
                        if (!string.IsNullOrEmpty(sel.BranchCode))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.BranchCode;
                        }
                        if (!string.IsNullOrEmpty(sel.Trust))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.Trust;
                        }

                        if (sel.Searchinput.Length > 0)
                        {
                            sel.Searchinput = sel.Searchinput.Substring(3, sel.Searchinput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table
                        intSubscriberEnquiryLogID = dsel.InsertSubscriberEnquiryLog(con, sel);
                        sel.SubscriberEnquiryLogID = intSubscriberEnquiryLogID;

                        if (!(string.IsNullOrEmpty(strIDNo)))
                        {
                            eoAccountVerificationWseManager.IDNo = strIDNo;
                        }
                        else
                        {
                            eoAccountVerificationWseManager.IDNo = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        }

                        moAccountVerificationWseManager.ConnectionString = AVSConnection;
                        eoAccountVerificationWseManager.ExternalReference = strExtRef;
                        eoAccountVerificationWseManager.Initials = strInitials;
                        eoAccountVerificationWseManager.AccNo = strAccNo;
                        eoAccountVerificationWseManager.Acctype = strAcctype;
                        eoAccountVerificationWseManager.BankName = strBankName;
                        eoAccountVerificationWseManager.BranchCode = strBranchCode;
                        eoAccountVerificationWseManager.SurName = strSurName;
                        eoAccountVerificationWseManager.EmailAddress = strEmailAddress;
                        eoAccountVerificationWseManager.RequesterFirstName = ClientFirstName;
                        eoAccountVerificationWseManager.RequesterSurName = ClientSurName;
                        eoAccountVerificationWseManager.IDType = TypeOfVerification == "Individual" ? "SID" : (TypeOfVerification == "Company" ? "SBR" : "Unknown");


                        // Submit data for matching 
                        rp = moAccountVerificationWseManager.search(eoAccountVerificationWseManager);

                        // Get Response data
                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                        sel.KeyID = rp.ResponseKey;
                        sel.KeyType = rp.ResponseKeyType;
                        sel.ExtraVarInput1 = rp.TmpReference;
                        sel.IsAHVRequest = true;
                        rp.EnquiryLogID = intSubscriberEnquiryLogID;

                        dsel.UpdateSubscriberEnquiryLog(con, sel);

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            sel.EnquiryResult = "E";
                            sel.ErrorDescription = rp.ResponseData.ToString();
                            dsel.UpdateSubscriberEnquiryLogError(con, sel);
                        }

                        if (sub.PayAsYouGo == 1)
                        {
                            dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                        }
                        if (!(strVoucherCode == string.Empty))
                        {
                            dSV.UpdateSubscriberVoucher(AdminConnection, strVoucherCode, sys.Username);
                        }

                    }



                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }

            catch (Exception oException)
            {
                sel.EnquiryResult = "E";
                sel.ErrorDescription = oException.Message;
                sel.SubscriberEnquiryLogID = dsel.UpdateSubscriberEnquiryLogError(con, sel);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                MatchResult = sel.SubscriberEnquiryLogID;
                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }

            ds.Dispose();
            dsLogon.Dispose();
            dsBankName.Dispose();
            dsAcctype.Dispose();

            return rp;
        }
        


    }
}
