﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalEnquiry.Business
{
    public class BlockedConsumers
    {
        public BlockedConsumers()
        {

        }

        public XDSPortalLibrary.Entity_Layer.Response BlockorUnblockConsumer(SqlConnection con, XDSPortalLibrary.Entity_Layer.BlockedConsumers ObjSearch)
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                int iMode = 101;
                if (ObjSearch.BlockID > 0)
                    iMode = 102;

                string strSQL = string.Format("execute USP_IU_BlockedConsumers '{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}'", iMode, ObjSearch.BlockID, ObjSearch.ConsumerID, ObjSearch.IDNo, ObjSearch.PassportNo, ObjSearch.SurName, ObjSearch.FirstName, ObjSearch.SecondName, ObjSearch.DateofBirth, ObjSearch.SystemUserID, ObjSearch.SubscriberID, ObjSearch.SubscriberEnquiryID, ObjSearch.SubscriberEnquiryResultID, ObjSearch.ProductID, ObjSearch.IsBlocked, ObjSearch.BlockedByUser, ObjSearch.UnblockedByUser);

                SqlCommand objcmd = new SqlCommand(strSQL, con);
                objcmd.CommandType = CommandType.Text;
                objcmd.CommandTimeout = 0;

                objcmd.ExecuteNonQuery();

                ObjResponse.ResponseKey = 0;
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = "Consumer Blocked/Unblocked Successfully.";

                con.Close();
                SqlConnection.ClearPool(con);
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                SqlConnection.ClearPool(con);
            }

            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetBlockedConsumers(SqlConnection con, int iSubscriberID)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                //SqlCommand objcmd = new SqlCommand("select a.BlockID,a.ConsumerID,isnull(a.IDNo,'') IDNo,isnull(a.PassportNo,'') PassportNo,isnull(a.Surname,'') Surname,isnull(a.FirstName,'') FirstName,isnull(a.SecondName,'') SecondName,case when a.BirthDate is null then '' when convert(varchar,a.BirthDate,103)='01/01/1900' then '' else convert(varchar,a.BirthDate,103) end as BirthDate from BlockedConsumers a (nolock) inner join SubscriberCreditAuthentication sca (nolock) on a.SubscriberID = sca.SubscriberID where a.IsBlocked=1 and sca.UnblockInd = 1 and sca.SubscriberGroupCode in (Select Subscribergroupcode from SubscriberCreditAuthentication nolock where SubscriberID =  " + iSubscriberID.ToString() + ")", con);
                //objcmd.CommandType = CommandType.Text;
                //objcmd.CommandTimeout = 0;

                SqlCommand objcmd = new SqlCommand("sp_Get_BlockedConsumers", con);
                objcmd.CommandType = CommandType.StoredProcedure;
                objcmd.CommandTimeout = 0;
                objcmd.Parameters.AddWithValue("@SubscriberID",iSubscriberID);

                DataSet dsBC = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(objcmd);
                da.Fill(dsBC);
                dsBC.DataSetName = "BlockedConsumer";

                ObjResponse.ResponseKey = 0;
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = dsBC.GetXml();
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = "<Error>" + ex.Message + "</Error>";
            }

            return ObjResponse;
        }

        public bool GetCreditAuthenticationSubscriber(SqlConnection con, int iSubscriberId)
        {
            try
            {
                SqlCommand objcmd = new SqlCommand("Select * from SubscriberCreditAuthentication Where SubscriberID=" + iSubscriberId.ToString() + " and RecordStatusInd='A'", con);
                objcmd.CommandType = CommandType.Text;
                objcmd.CommandTimeout = 0;

                DataSet dsSubscriber = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(objcmd);
                da.Fill(dsSubscriber);

                if (dsSubscriber.Tables.Count > 0 && dsSubscriber.Tables[0].Rows.Count > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public DataSet GetCreditAuthenticationSubscriberDataSet(SqlConnection con, int iSubscriberId)
        {
            DataSet dsSubscriber = new DataSet();

            SqlCommand objcmd = new SqlCommand("Select SubscriberCreditAuthenticationID,SubscriberID,isnull(SubscriberName,'')SubscriberName,isnull(AuthenticationSubscriberName,'')AuthenticationSubscriberName,isnull(SubscriberTypeInd,'')SubscriberTypeInd,isnull(RecordStatusInd,'')RecordStatusInd,CreatedByUser,CreatedOnDate,isnull(EmailAddress,'') EmailAddress from SubscriberCreditAuthentication Where SubscriberID=" + iSubscriberId.ToString() + " and RecordStatusInd='A'", con);
            objcmd.CommandType = CommandType.Text;

            SqlDataAdapter da = new SqlDataAdapter(objcmd);
            da.Fill(dsSubscriber);
            return dsSubscriber;
        }

        public bool GetUnblockUser(SqlConnection con, int iSysUserId)
        {
            try
            {
                SqlCommand objcmd = new SqlCommand("Select * from SystemUserUnblock Where SystemUserID=" + iSysUserId.ToString() + " and RecordStatusInd='A'", con);
                objcmd.CommandType = CommandType.Text;
                objcmd.CommandTimeout = 0;

                DataSet dsUser = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(objcmd);
                da.Fill(dsUser);

                if (dsUser.Tables.Count > 0 && dsUser.Tables[0].Rows.Count > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public int GetAuthenticationStatus(SqlConnection con, int iConsumerID, int iSubscriberID, int iProductID, string sAssociationCode, int ireportID)
        {
            int bAuthenticationcheck = 1;
            SqlParameter op = new SqlParameter();
            op.Direction = ParameterDirection.ReturnValue;

            try
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();

                SqlCommand objcmd = new SqlCommand("spCheckblockingStatus", con);
                objcmd.CommandType = CommandType.StoredProcedure;
                objcmd.CommandTimeout = 0;

                objcmd.Parameters.AddWithValue("@SubscriberID", iSubscriberID);
                objcmd.Parameters.AddWithValue("@ConsumerID", iConsumerID);
                objcmd.Parameters.AddWithValue("@productID", iProductID);
                objcmd.Parameters.AddWithValue("@SubscriberAssociationCode", sAssociationCode);
                objcmd.Parameters.AddWithValue("@ReportID", ireportID);
                objcmd.Parameters.Add(op);

                objcmd.ExecuteNonQuery();

                bAuthenticationcheck = int.Parse(op.Value.ToString());                    

                if (con.State == ConnectionState.Open)
                    con.Close();

            }
            catch (Exception ex)
            {
                if (op.Value != null)
                {
                        bAuthenticationcheck = int.Parse (op.Value.ToString());                    
                }
                else
                    throw new Exception(ex.Message);
            }
            return bAuthenticationcheck;
        }
    }
}
