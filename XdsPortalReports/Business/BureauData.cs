﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using XDSPortalLibrary;

namespace XDSPortalEnquiry.Business
{
    public class BureauData
    {
        private XDSPortalLibrary.Business_Layer.ConsumerTrace moConsumerTraceWseManager;
        private XDSPortalLibrary.Entity_Layer.ConsumerTrace eoConsumerTraceWseManager;
        
        

        public BureauData()
        {
            moConsumerTraceWseManager = new XDSPortalLibrary.Business_Layer.ConsumerTrace();
            eoConsumerTraceWseManager = new XDSPortalLibrary.Entity_Layer.ConsumerTrace();
        }
        public DataSet SubmitBureauData(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, bool bBonusChecking, string strVoucherCode)
        {
            
            
            string strAdminCon = AdminConnection.ConnectionString;
            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            double Totalcost = 0;

            DataSet dsResult = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            

            try
            {


                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, intSubscriberEnquiryResultID);

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);


                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                if (spr.ReportID > 0)
                {

                    // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML 
                    // use this XML data for generating the Report 

                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {

                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {
                            eoConsumerTraceWseManager.IDno = eSe.IDNo;
                            eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                            eoConsumerTraceWseManager.Surname = eSe.Surname;
                            eoConsumerTraceWseManager.FirstName = eSe.FirstName;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;

                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            dsResult = ra.GetBureauData(eoConsumerTraceWseManager);

                           
                           if (dsResult.Tables.Contains("Error"))
                            {
                                throw new Exception(dsResult.Tables[0].Rows[0]["ErrorDescription"].ToString());
                            }
                           else 
                           {
                               eSC.BillingTypeID = spr.BillingTypeID;
                               eSC.BillingPrice = spr.UnitPrice;

                               eSC.XMLData = rXml.Replace("'", "''");
                               eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                               eSC.ProductID = eSe.ProductID;

                               dSC.UpdateSubscriberEnquiryResult(con, eSC);

                           }
                            // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                            if (sub.PayAsYouGo == 1)
                            {
                                dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                            }
                            if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                            {
                                dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                            }

                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.

                            throw new Exception("Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account");

                        }
                    }
                    else
                    {
                        // When User want to Re-Open a report 
                        System.IO.StringReader srXml = new System.IO.StringReader(eSC.XMLData);
                        dsResult.ReadXml(srXml);
                    }
                }
                else
                {
                    throw new Exception("This enquiry Product is not activated in your profile. Please contact XDS to activate the product");
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
               
                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

                dsResult.ReadXml(oException.Message);
            }
            return dsResult;
            
            
        }
    }
     
}
