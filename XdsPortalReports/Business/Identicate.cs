﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalEnquiry.Business
{
    public class Identicate
    {
        public XDSPortalLibrary.Entity_Layer.Response SaveTransaction(SqlConnection con, string ReferenceNo, string IDNumber, int StatusID, DateTime Date)
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                string strSQL = string.Format("Insert into EnrolmentLog(ReferenceNo,IDNo,StatusID,CreatedOnDate) values('{0}','{1}',{2},'{3}')", ReferenceNo, IDNumber, StatusID, Date);

                SqlCommand objcmd = new SqlCommand(strSQL, con);
                objcmd.CommandType = CommandType.Text;
                objcmd.CommandTimeout = 0;

                objcmd.ExecuteNonQuery();

                ObjResponse.ResponseKey = 0;
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = "Enrolment Logged Successfully.";

                con.Close();
                SqlConnection.ClearPool(con);
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                SqlConnection.ClearPool(con);
            }

            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response SetStatus(SqlConnection con, string ReferenceNo, int StatusID, DateTime Date)
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                string strSQL = string.Format("Update EnrolmentLog Set StatusID = {0},ChangedOnDate = '{1}' where ReferenceNo = '{2}' ", StatusID, Date, ReferenceNo);

                SqlCommand objcmd = new SqlCommand(strSQL, con);
                objcmd.CommandType = CommandType.Text;
                objcmd.CommandTimeout = 0;

                int result = objcmd.ExecuteNonQuery();

                ObjResponse.ResponseKey = 0;
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                
                if (result > 0)
                    ObjResponse.ResponseData = "Status Updated Successfully.";
                else
                    throw new Exception("Invalid Reference Number Supplied.");
                
                con.Close();
                SqlConnection.ClearPool(con);
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                SqlConnection.ClearPool(con);
            }

            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetStatus(SqlConnection con, string ReferenceNo)
        {
            if (con.State == ConnectionState.Closed)
                con.Open();

            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                string strSQL = string.Format("Select StatusID from EnrolmentLog nolock where ReferenceNo = '{0}'", ReferenceNo);

                DataSet ds = new DataSet("EnrolmentLog");

                using (SqlDataAdapter oDA = new SqlDataAdapter(strSQL, con))
                {
                    oDA.Fill(ds);

                    if (ds.Tables.Count < 1)
                        throw new Exception("Invalid Reference Number Supplied.");
                    else
                    {
                        if (ds.Tables[0].Rows.Count < 1)
                            throw new Exception("Invalid Reference Number Supplied.");
                    }

                    ObjResponse.ResponseKey = 0;
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                    ObjResponse.ResponseData = ds.GetXml();
                }

                con.Close();
                SqlConnection.ClearPool(con);
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                SqlConnection.ClearPool(con);
            }

            return ObjResponse;
        }
    }
}
