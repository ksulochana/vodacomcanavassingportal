﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalEnquiry.Business
{
    public class CommercialEnquiry_BusinessInvestigativeEnquiry
    {
        public void ValidateInput(Entity.SubscriberEnquiryLog Validate)
        {
            if (string.IsNullOrEmpty(Validate.BusBusinessName))
            {
                throw new Exception("Business Name is mandatory");
            }
            else if (string.IsNullOrEmpty(Validate.FirstName) || (string.IsNullOrEmpty(Validate.SurName)) || (string.IsNullOrEmpty(Validate.CompanyName)) || (string.IsNullOrEmpty(Validate.ContactNo)) || (string.IsNullOrEmpty(Validate.EmailAddress)))
            {
                throw new Exception("FirstName, SurName, Company Name, Conatct No, Email Address are Mandatory");
            }
        }
        public XDSPortalLibrary.Entity_Layer.Response SubmitBusinessInvestigativeEnquiry(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strCountry, string strRegNo1, string strRegNo2, string strRegNo3, string strBusinessName,  string strSurname,  string strFirstName,string strCOmpanyName,string strContactNo,string strEmailAddress, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode,string strReporttype,string strTimeFrame,string strIDNo,string strAdditionalInfo,string strComapnyContactNo,string strTerms,double damount)
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            int intSubscriberEnquiryLogID = 0;
            
            int MatchResult = 0;
            DataSet ds = new DataSet();
            string strValidationStatus = "";
            Boolean boolSubmitEnquiry = true;

            
            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            Entity.SubscriberEnquiryLog sel = new XDSPortalEnquiry.Entity.SubscriberEnquiryLog();
            Data.SubscriberEnquiryLog dsel = new XDSPortalEnquiry.Data.SubscriberEnquiryLog();

            
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);


            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            try
            {

                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {

                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }
                        

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {
                        strRegNo2 = string.IsNullOrEmpty(strRegNo2) ? string.Empty : "/" + strRegNo2;
                        strRegNo3 = string.IsNullOrEmpty(strRegNo3) ? string.Empty : "/" + strRegNo3;

                        sel.BusRegistrationNo = strRegNo1 + strRegNo2 + strRegNo3;
                        sel.BusBusinessName = strBusinessName;
                        sel.FirstName = strFirstName;
                        sel.SurName = strSurname;
                        sel.CompanyName = strCOmpanyName;
                        sel.ContactNo = strContactNo;
                        sel.EmailAddress = strEmailAddress;
                        sel.VoucherCode = strVoucherCode;
                        sel.SubscriberReference = strExtRef;
                        sel.SubscriberID = intSubscriberID;
                        sel.SubscriberName = sub.SubscriberName;
                        sel.SystemUserID = intSystemUserID;
                        sel.SystemUserName = sys.SystemUserFullName;
                        sel.CreatedByUser = sys.Username;
                        sel.CreatedOnDate = DateTime.Now;
                        sel.SubscriberEnquiryDate = DateTime.Now;
                        sel.EnquiryStatus = XDSPortalEnquiry.Entity.SubscriberEnquiryLog.EnquiryStatusInd.P.ToString();
                        sel.ReportType = strReporttype;
                        sel.TimeFrame = strTimeFrame;
                        sel.ExtraVarInput1 = strAdditionalInfo;
                        sel.IDNo = strIDNo;
                        sel.CompanyContactNo = strComapnyContactNo;
                        sel.BillingTypeID = spr.BillingTypeID;
                        sel.Terms = strTerms;
                        sel.Amount = damount;
                        sel.ExtraVarInput2 = strCountry;



                        ValidateInput(sel);

                       // Generate serach input string

                        if (!string.IsNullOrEmpty(sel.BusBusinessName))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.BusBusinessName;
                        }
                        if (!string.IsNullOrEmpty(sel.BusRegistrationNo.Replace("/", "")))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.BusRegistrationNo;
                        }

                        if (sel.Searchinput.Length > 0)
                        {
                            sel.Searchinput = sel.Searchinput.Substring(3, sel.Searchinput.Length - 3).ToUpper();
                        }


                        //eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        //eSe.SubscriberID = intSubscriberID;
                        //eSe.ProductID = intProductId;
                        //eSe.SubscriberEnquiryDate = DateTime.Now;
                        //eSe.SubscriberName = strSubscriberName;
                        //eSe.SubscriberReference = strExtRef;
                        //eSe.SystemUserID = intSystemUserID;
                        //eSe.BusRegistrationNo = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        //eSe.BusBusinessName = strBusinessName;
                        //eSe.SystemUserID = intSystemUserID;
                        //eSe.SystemUser = sys.SystemUserFullName;
                        //eSe.CreatedByUser = sys.Username;
                        //eSe.CreatedOnDate = DateTime.Now;
                        //eSe.SearchInput = "";
                        //eSe.FirstName = strFirstName;
                        //eSe.Surname = strSurname;
                        //eSe.TelephoneNo = strContactNo;
                        //eSe.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();


                        //Generate serach input string

                        //if (!string.IsNullOrEmpty(eSe.BusBusinessName))
                        //{
                        //    eSe.SearchInput = eSe.SearchInput + " | " + eSe.BusBusinessName;
                        //}
                        //if (!string.IsNullOrEmpty(eSe.BusRegistrationNo.Replace("/","")))
                        //{
                        //    eSe.SearchInput = eSe.SearchInput + " | " + strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        //}

                        //if (eSe.SearchInput.Length > 0)
                        //{
                        //    eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        //}

                        //// Log the Current Enquiry to SubscriberEnquiry Table
                        //intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);

                        //sel.SubscriberEnquiryID = intSubscriberEnquiryID;
                        sel.KeyType = "B";
                        sel.ProductID = intProductId;
                        sel.BillingPrice = spr.UnitPrice;

                        if (sub.PayAsYouGo == 1 || !(strVoucherCode == string.Empty))
                        {
                            sel.Billable = false;
                        }
                        else
                        {
                            sel.Billable = true;
                        }

                        intSubscriberEnquiryLogID = dsel.InsertSubscriberEnquiryLog(con, sel);

                        //eSC.SearchOutput = "";
                        //eSC.BusBusinessName = strBusinessName;
                        //eSC.BusRegistrationNo = eSe.BusRegistrationNo;
                        //eSC.IDNo = "";
                        //eSC.PassportNo = "";
                        //eSC.Surname = strSurname;
                        //eSC.FirstName = strFirstName;
                        //eSC.Gender = "";
                        //eSC.DetailsViewedDate = DateTime.Now;
                        //eSC.DetailsViewedYN = true;
                        //eSC.VoucherCode = strVoucherCode;

                        //if (string.IsNullOrEmpty(eSC.VoucherCode))
                        //    eSC.Billable = true;
                        //else
                        //    eSC.Billable = false;
                        //eSC.CreatedByUser = eSe.CreatedByUser;
                        //eSC.CreatedOnDate = DateTime.Now;



                        //eSC.KeyID = intSubscriberEnquiryLogID;
                        //eSC.KeyType = "B";

                        //eSC.BillingTypeID = spr.BillingTypeID;
                        //eSC.BillingPrice = spr.UnitPrice;


                        //eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                        //eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                        //eSC.ProductID = intProductId;

                        //intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                        if (sub.PayAsYouGo == 1)
                        {
                            dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                        }
                        if (!(strVoucherCode == string.Empty))
                        {
                            dSV.UpdateSubscriberVoucher(AdminConnection, strVoucherCode, sys.Username);
                        }

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = "Enquiry Submitted successfully";
                        rp.EnquiryLogID = intSubscriberEnquiryLogID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }

            catch (Exception oException)
            {
                sel.EnquiryResult = "E";
                sel.ErrorDescription = oException.Message;
                sel.SubscriberEnquiryLogID = dsel.UpdateSubscriberEnquiryLogError(con, sel);

                MatchResult = sel.SubscriberEnquiryLogID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;
            }
            
            ds.Dispose();
            return rp;
        }

        }
    }

