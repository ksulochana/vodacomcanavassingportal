﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalEnquiry.Business
{
    public class MultipleCommercial_DirectorEnquiry
    {

        private XDSPortalLibrary.Business_Layer.DirectorCreditEnquiry moCommercialEnquiryWseManager;
        private XDSPortalLibrary.Entity_Layer.DirectorCreditEnquiry eoCommercialEnquiryWseManager;


         public MultipleCommercial_DirectorEnquiry()
        {
            moCommercialEnquiryWseManager = new XDSPortalLibrary.Business_Layer.DirectorCreditEnquiry();
            eoCommercialEnquiryWseManager = new XDSPortalLibrary.Entity_Layer.DirectorCreditEnquiry();
        }

         public XDSPortalLibrary.Entity_Layer.Response SubmitMulipleDirectorEnquiry(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, DataSet dsBonusDataSegment, bool bBonusChecking, string strVoucherCode)
         {
             string strAdminCon = AdminConnection.ConnectionString;

             if (con.State == ConnectionState.Closed)
                 con.Open();
             if (AdminConnection.State == ConnectionState.Closed)
                 AdminConnection.Open();
             
             
             string rXml = "";
             double Totalcost = 0;
             DataSet ds = new DataSet();

             Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
             Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

             Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
             Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

             Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
             Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

             Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

             XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

             try
             {

                 eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                 eSC = dSC.GetSubscriberEnquiryResultObject(con, intSubscriberEnquiryResultID);

                 SqlConnection objConstring = new SqlConnection(eSe.ExtraVarInput1.ToString());

                 xdsBilling xb = new xdsBilling();
                 Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                 Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                 Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                 Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                 Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                 if (spr.ReportID > 0)
                 {
                     eoCommercialEnquiryWseManager.BonusSegments = null;
                     if (dsBonusDataSegment != null)
                     {
                         if (dsBonusDataSegment.Tables.Count > 0 && dsBonusDataSegment.Tables[0].Rows.Count > 0)
                         {
                             dsBonusDataSegment.DataSetName = "BonusSegments";
                             dsBonusDataSegment.Tables[0].TableName = "Segment";
                             eoCommercialEnquiryWseManager.BonusSegments = dsBonusDataSegment;
                         }
                     }

                     // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML 
                     // use this XML data for generating the Report 


                     if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                     {
                         string strValidationStatus = "";
                         if (!string.IsNullOrEmpty(eSC.VoucherCode))
                         {
                             SubscriberVoucher sv = new SubscriberVoucher();
                             strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                             if (strValidationStatus == "")
                             {
                                 throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                             }
                             else if (!(strValidationStatus == "1"))
                             {
                                 throw new Exception(strValidationStatus);
                             }

                         }
                        else if (sub.PayAsYouGo == 1)
                        {
                            //Calculate The Total cost of the Report , including Bonus Segments

                            Totalcost = spr.UnitPrice;

                             if (eoCommercialEnquiryWseManager.BonusSegments != null)
                             {

                                 foreach (DataRow dr in eoCommercialEnquiryWseManager.BonusSegments.Tables[0].Rows)
                                 {
                                     if (dr["BonusViewed"].ToString().ToLower() == "true")
                                     {
                                         Totalcost = Totalcost + Convert.ToInt16(dr["BonusPrice"].ToString());
                                     }
                                 }
                             }
                         }
                         // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                         if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                         {
                             eoCommercialEnquiryWseManager.DirectorID = eSC.KeyID;
                             eoCommercialEnquiryWseManager.ExternalReference = eSe.SubscriberReference;
                             eoCommercialEnquiryWseManager.ProductID = eSe.ProductID;
                             eoCommercialEnquiryWseManager.ReferenceNo = eSC.Reference;
                             eoCommercialEnquiryWseManager.subscriberID = eSe.SubscriberID;
                             eoCommercialEnquiryWseManager.TmpReference = eSC.ExtraVarOutput1;
                             moCommercialEnquiryWseManager.ConnectionString = objConstring;
                             eoCommercialEnquiryWseManager.DataSegments = eSe.ExtraVarInput2;
                             eoCommercialEnquiryWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                             eoCommercialEnquiryWseManager.SAFPSIndicator = sub.SAFPSIndicator;
                             
                             XDSPortalEnquiry.Data.Consumer oConsumer = new XDSPortalEnquiry.Data.Consumer();

                             eoCommercialEnquiryWseManager.ConsumerID = oConsumer.GetConsumerID(objConstring, eSC.KeyID, 0);


                             XDSPortalEnquiry.Data.Consumer oEnquiryConsumer = new XDSPortalEnquiry.Data.Consumer();
                             oEnquiryConsumer.CheckBlockedStatus(AdminConnection, int.Parse(eSe.SubscriberID.ToString()), int.Parse(eoCommercialEnquiryWseManager.ConsumerID.ToString()), eSe.ProductID, sub.SubscriberAssociationCode, eSe.ProductID);


                             XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                             rp = ra.GetDirectorCreditEnquiryReport(eoCommercialEnquiryWseManager);

                             if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                             {
                                 eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                 ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                 rp = ra.GetDirectorCreditEnquiryReport(eoCommercialEnquiryWseManager);
                             }

                             rXml = rp.ResponseData;

                             System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                             if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                             {

                                 eSC.DetailsViewedYN = false;
                                 eSC.Billable = false;
                                 eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                 eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                                 eSC.CreatedByUser = eSe.CreatedByUser;
                                 eSC.CreatedOnDate = DateTime.Now;

                                 dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                 rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                 rp.EnquiryID = eSC.SubscriberEnquiryID;

                             }


                             else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                             {

                                 eSC.EnquiryResult = "E";
                                 eSe.ErrorDescription = rp.ResponseData.ToString();
                                 dSe.UpdateSubscriberEnquiryError(con, eSe);


                             }
                             else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Bonus)
                             {
                                 ds.ReadXml(xmlSR);
                                 if (ds.Tables.Contains("Segments"))
                                 {

                                     eSC.DetailsViewedYN = false;
                                     eSC.Billable = false;
                                     eSC.BonusIncluded = true;
                                     eSC.KeyID = rp.ResponseKey;
                                     eSC.KeyType = rp.ResponseKeyType;
                                     eSC.XMLBonus = rXml.Replace("'", "''");
                                     eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                     eSC.CreatedByUser = eSe.CreatedByUser;
                                     eSC.CreatedOnDate = DateTime.Now;
                                     eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.B.ToString();

                                     dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                     rp.EnquiryID = intSubscriberEnquiryID;
                                     rp.EnquiryResultID = intSubscriberEnquiryResultID;


                                     foreach (DataRow r in ds.Tables["Segments"].Rows)
                                     {
                                         eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                         eSB.DataSegmentID = int.Parse(r["DataSegmentID"].ToString());
                                         eSB.DataSegmentName = r["DataSegmentName"].ToString().Replace("'", "''");
                                         eSB.DataSegmentDisplayText = r["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                         eSB.BonusViewed = bool.Parse(r["BonusViewed"].ToString());
                                         eSB.Billable = false;
                                         eSB.CreatedByUser = eSe.CreatedByUser;
                                         eSB.CreatedOnDate = DateTime.Now;

                                         dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                     }
                                 }
                             }

                             else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                             {
                                 ds.ReadXml(xmlSR);
                                 if (ds.Tables.Contains("ConsumerDetail"))
                                 {
                                     Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                     Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, eSe.ProductID);

                                     DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                     dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                     dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                     dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                     dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                     dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                     DataRow drSubscriberInput;
                                     drSubscriberInput = dtSubscriberInput.NewRow();

                                     drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                     drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                     drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                     drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                     drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();

                                     dtSubscriberInput.Rows.Add(drSubscriberInput);

                                     ds.Tables.Add(dtSubscriberInput);

                                     if (eSC.BonusIncluded == true)
                                     {
                                         Data.SubscriberEnquiryResultBonus oSubscriberEnquiryResultBonus = new XDSPortalEnquiry.Data.SubscriberEnquiryResultBonus();
                                         //DataSet dsBonus = oSubscriberEnquiryResultBonus.GetSelectedSubscriberEnquiryResultBonus(con, eSC.SubscriberEnquiryResultID);
                                         DataSet dsBonus = eoCommercialEnquiryWseManager.BonusSegments;

                                         DataTable dtBonusSelected = new DataTable("ConsumerBonusSelected");

                                         dtBonusSelected.Columns.Add("DataSegmentID", typeof(String));
                                         dtBonusSelected.Columns.Add("DataSegmentName", typeof(String));
                                         dtBonusSelected.Columns.Add("DataSegmentDisplayText", typeof(String));

                                         DataRow drBonusSelected;
                                         if (dsBonus != null)
                                        {
                                            if (dsBonus.Tables[0].Rows.Count > 0)
                                            {
                                                foreach (DataRow r in dsBonus.Tables[0].Rows)
                                                {
                                                    drBonusSelected = dtBonusSelected.NewRow();
                                                    drBonusSelected["DataSegmentID"] = r["DataSegmentID"];
                                                    drBonusSelected["DataSegmentName"] = r["DataSegmentName"];
                                                    drBonusSelected["DataSegmentDisplayText"] = r["DataSegmentDisplayText"];
                                                    dtBonusSelected.Rows.Add(drBonusSelected);

                                                    eSB.SubscriberEnquiryResultBonusID = 0;
                                                    eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                                    eSB.DataSegmentID = Convert.ToInt32(r["DataSegmentID"]);
                                                    eSB.DataSegmentName = r["DataSegmentName"].ToString();
                                                    eSB.DataSegmentDisplayText = r["DataSegmentDisplayText"].ToString();
                                                    eSB.BonusViewed = Convert.ToBoolean(r["BonusViewed"].ToString());
                                                    if (eSB.BonusViewed)
                                                    {
                                                        eSB.Billable = true;
                                                    }
                                                    else
                                                    {
                                                        eSB.Billable = false;
                                                    }
                                                    eSB.ChangedByUser = eSB.CreatedByUser;
                                                    eSB.ChangedOnDate = DateTime.Now;
                                                    dSB.UpdateSubscriberEnquiryResultBonus(con, eSB);
                                                }
                                                ds.Tables.Add(dtBonusSelected);
                                            }
                                         }
                                         dsBonus.Dispose();
                                     }

                                     rXml = ds.GetXml();
                                     rp.ResponseData = rXml;

                                     foreach (DataRow r in ds.Tables["ConsumerDetail"].Rows)
                                     {
                                         Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                         Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                         eSC.DetailsViewedDate = DateTime.Now;
                                         eSC.DetailsViewedYN = true;
                                         if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                         {
                                             eSC.Billable = false;
                                         }
                                         else
                                         {
                                             eSC.Billable = true;
                                         }
                                         eSC.ChangedByUser = eSe.CreatedByUser;
                                         eSC.ChangedOnDate = DateTime.Now;
                                         eSC.SearchOutput = "";


                                         if (r.Table.Columns.Contains("IDNo"))
                                         {
                                             if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                             {
                                                 eSC.IDNo = r["IDNo"].ToString();
                                                 eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                             }
                                         }
                                         if (r.Table.Columns.Contains("PassportNo"))
                                         {
                                             if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                             {
                                                 eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                                 eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                             }
                                         }
                                         if (r.Table.Columns.Contains("Surname"))
                                         {
                                             if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                             {
                                                 eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                                 eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                             }
                                         }

                                         if (r.Table.Columns.Contains("FirstName"))
                                         {
                                             if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                             {
                                                 eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                                 eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                             }
                                         }

                                         if (eSC.SearchOutput.Length > 0)
                                         {
                                             eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                         }

                                         eSC.KeyID = rp.ResponseKey;
                                         eSC.KeyType = rp.ResponseKeyType;
                                         eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                         eSC.BillingTypeID = spr.BillingTypeID;
                                         eSC.BillingPrice = spr.UnitPrice;

                                         eSC.XMLData = rXml.Replace("'", "''");
                                         eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                         eSC.ProductID = eSe.ProductID;

                                         dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                         rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                         rp.EnquiryID = eSC.SubscriberEnquiryID;

                                         // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                         if (sub.PayAsYouGo == 1)
                                         {
                                             dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                         }
                                         if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                         {
                                             dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                         }

                                         if (String.IsNullOrEmpty(eSC.VoucherCode))
                                         {
                                             //Log FootPrint

                                             eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                             eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                             eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                             eSubscriberFootPrint.KeyID = eSC.KeyID;
                                             eSubscriberFootPrint.KeyType = eSC.KeyType;
                                             eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                             eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                             if (string.IsNullOrEmpty(sub.CompanyTelephoneCode) && string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                             {
                                                 eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                             }
                                             eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                             eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                             eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                             eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                             eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                             eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                             eSubscriberFootPrint.CreatedByUser = sys.Username;
                                             eSubscriberFootPrint.CreatedOnDate = DateTime.Now;


                                             int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                         }

                                     }
                                 }
                             }
                             else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                             {
                                 ds.ReadXml(xmlSR);
                                 if (ds.Tables.Contains("DirectorDetails"))
                                 {
                                     foreach (DataRow r in ds.Tables["DirectorDetails"].Rows)
                                     {

                                         eSC.SearchOutput = "";

                                         if (r.Table.Columns.Contains("IDNo"))
                                         {
                                             if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                             {
                                                 eSC.IDNo = r["IDNo"].ToString();
                                                 eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                             }
                                         }
                                         if (r.Table.Columns.Contains("PassportNo"))
                                         {
                                             if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                             {
                                                 eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                                 eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                             }
                                         }
                                         if (r.Table.Columns.Contains("Surname"))
                                         {
                                             if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                             {
                                                 eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                                 eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                             }
                                         }

                                         if (r.Table.Columns.Contains("FirstName"))
                                         {
                                             if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                             {
                                                 eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                                 eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                             }
                                         }

                                         if (eSC.SearchOutput.Length > 0)
                                         {
                                             eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                         }

                                         eSC.DetailsViewedYN = false;
                                         eSC.Billable = false;
                                         eSC.ChangedByUser = eSe.CreatedByUser;
                                         eSC.ChangedOnDate = DateTime.Now;
                                         eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                         eSC.KeyType = rp.ResponseKeyType;

                                         eSC.BillingTypeID = spr.BillingTypeID;
                                         eSC.BillingPrice = spr.UnitPrice;

                                         eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                         eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();

                                         dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                         rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                         rp.EnquiryID = eSC.SubscriberEnquiryID;

                                     }
                                 }
                             }
                             else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                             {
                                 ds.ReadXml(xmlSR);
                                 if (ds.Tables.Contains("DirectorDetails"))
                                 {
                                     foreach (DataRow r in ds.Tables["DirectorDetails"].Rows)
                                     {
                                         eSC.SearchOutput = "";

                                         if (r.Table.Columns.Contains("IDNo"))
                                         {
                                             if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                             {
                                                 eSC.IDNo = r["IDNo"].ToString();
                                                 eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                             }
                                         }
                                         if (r.Table.Columns.Contains("PassportNo"))
                                         {
                                             if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                             {
                                                 eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                                 eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                             }
                                         }
                                         if (r.Table.Columns.Contains("Surname"))
                                         {
                                             if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                             {
                                                 eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                                 eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                             }
                                         }

                                         if (r.Table.Columns.Contains("FirstName"))
                                         {
                                             if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                             {
                                                 eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                                 eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                             }
                                         }

                                         if (eSC.SearchOutput.Length > 0)
                                         {
                                             eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                         }


                                         eSC.DetailsViewedYN = false;
                                         eSC.Billable = false;
                                         eSC.CreatedByUser = eSe.CreatedByUser;
                                         eSC.CreatedOnDate = DateTime.Now;
                                         eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                         eSC.KeyType = rp.ResponseKeyType;

                                         eSC.BillingTypeID = spr.BillingTypeID;
                                         eSC.BillingPrice = spr.UnitPrice;

                                         eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                         eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();

                                         dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                         rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                         rp.EnquiryID = eSC.SubscriberEnquiryID;
                                     }
                                 }
                             }

                         }
                         else
                         {
                             // When Subscriberdoesn't have enough credit Limit.
                             rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                             rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                         }
                     }
                     else
                     {
                         // When User want to Re - Open a report 
                         rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                         rp.ResponseData = eSC.XMLData;
                         rp.EnquiryID = eSC.SubscriberEnquiryID;
                         rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                     }
                 }
                 else
                 {
                     rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                     rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                 }

                 con.Close();
                 AdminConnection.Close();
                 SqlConnection.ClearPool(con);
                 SqlConnection.ClearPool(AdminConnection);
             }
             catch (Exception oException)
             {
                 eSe.EnquiryResult = "E";
                 eSe.ErrorDescription = oException.Message;
                 eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                 rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                 rp.ResponseData = oException.Message;


                 if (con.State == ConnectionState.Open)
                     con.Close();
                 if (AdminConnection.State == ConnectionState.Open)
                     AdminConnection.Close();
                 SqlConnection.ClearPool(con);
                 SqlConnection.ClearPool(AdminConnection);
             }
             ds.Dispose();
             return rp;
         }
    
    }
}
