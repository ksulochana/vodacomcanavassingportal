using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using XDSPortalLibrary;

namespace XDSPortalEnquiry.Business
{
    public class ConsumerTrace_AddressTrace
    {

        private XDSPortalLibrary.Business_Layer.ConsumerAddress moConsumerTraceWseManager;
        private XDSPortalLibrary.Entity_Layer.ConsumerAddress eoConsumerTraceWseManager;

        public ConsumerTrace_AddressTrace()
        {
            moConsumerTraceWseManager = new XDSPortalLibrary.Business_Layer.ConsumerAddress();
            eoConsumerTraceWseManager = new XDSPortalLibrary.Entity_Layer.ConsumerAddress();
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitAddressTrace(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strBuildingName, string strStreetNo, string strStreetName, string strSuburb, string strPostalCode, string strProvince, string strCity, string strCountry, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            string rXml = "";
            int MatchResult = 0;
            DataSet ds = new DataSet();
            int intSubscriberEnquiryResultID = 0;
              Boolean boolSubmitEnquiry = true;
              string strValidationStatus = "";

          
            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

           try
            {
                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {
                    // Check the PayAsYouGo settings for the subscriber

                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }
                        

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {
                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.ProductID = intProductId;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";


                        //Generate serach input string

                        if (!string.IsNullOrEmpty(strSuburb))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + strSuburb;
                        }

                        if (!string.IsNullOrEmpty(strPostalCode))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + strPostalCode;
                        }

                        if (!string.IsNullOrEmpty(strStreetName))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + strStreetName;
                        }
                        if (!string.IsNullOrEmpty(strStreetNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + strStreetNo;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                       // moConsumerTraceWseManager.ConnectionString = eSe.ExtraVarInput3;
                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                        
                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoConsumerTraceWseManager.ConfirmationChkBox = bConfirmationChkBox;
                        eoConsumerTraceWseManager.ExternalReference = strExtRef;
                        eoConsumerTraceWseManager.ProductID = intProductId;
                        eoConsumerTraceWseManager.subscriberID = intSubscriberID;
                        
                        eoConsumerTraceWseManager.StreetName = strStreetName;
                        eoConsumerTraceWseManager.StreetNo = strStreetNo;
                        eoConsumerTraceWseManager.Suburb = strSuburb;
                        eoConsumerTraceWseManager.BuildingName = strBuildingName;
                        eoConsumerTraceWseManager.City = strCity;
                        eoConsumerTraceWseManager.Country = strCountry;
                        eoConsumerTraceWseManager.PostalCode = strPostalCode;
                        eoConsumerTraceWseManager.Province = strProvince;
                        eoConsumerTraceWseManager.BonusCheck = bBonusChecking;

                        // Submit data for matching 
                        //rp = moConsumerTraceWseManager.search(eoConsumerTraceWseManager);
                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.ConsumerAddressMatch(eoConsumerTraceWseManager);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.ConsumerAddressMatch(eoConsumerTraceWseManager);
                        }

                        rp.EnquiryID = intSubscriberEnquiryID;

                        // Get Response data
                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);


                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.DetailsViewedYN = false;
                            eSC.Billable = false;
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;

                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);


                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            ds.ReadXml(xmlSR);
                            if (ds.Tables.Contains("ConsumerDetail"))
                            {
                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);
                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                                foreach (DataRow r in ds.Tables["ConsumerDetail"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    eSC.Billable = true;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();

                                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    // Change the PayAsYouGoEnquiryLimit  after viewing the report
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                                }
                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {

                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }
                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;


                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {
                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");


                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;
                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                            }



                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {

                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }

                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
                                    eSC.ExtraVarOutput1 = ""; 
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {


                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");

                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

                                            }

                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {

                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }


                        }

                    }

                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }
                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                MatchResult = eSe.SubscriberEnquiryID;
                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
           ds.Dispose();
            return rp;
        }


        public DataSet SubmitAddressIDTrace(SqlConnection AdminConnection, int ConsumerAddressID)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string strSQL = "select isnull(c.Address1,isnull(a.OriginalAddress1,'')) Address1, isnull(c.Address2,isnull(a.OriginalAddress2,'')) Address2, isnull(c.Address3,isnull(a.OriginalAddress3,'')) Address3, isnull(c.Address4,isnull(a.OriginalAddress4,'')) Address4, isnull(c.PostalCode,isnull(a.OriginalPostalCode,'')) PostalCode, isnull(c.Suburb,'') Suburb, isnull(c.Town,'') Town, isnull(d.Surname,'') Surname from ConsumerAddress a (nolock)  left outer join AddressInformation b (nolock)  on a.ConsumerAddressID=b.OldConsumerAddressID left outer join Address c (nolock) on c.ConsumerAddressID=b.ConsumerAddressID inner join Consumer d (nolock) on d.ConsumerID=a.ConsumerID where a.ConsumerAddressID= " + ConsumerAddressID.ToString() + " and a.RecordStatusInd = 'A'";
            SqlCommand sqlcmd = new SqlCommand(strSQL, AdminConnection);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet dsAddressDetails = new DataSet();
            sqlda.Fill(dsAddressDetails);

            sqlda.Dispose();
            sqlcmd.Dispose();
            AdminConnection.Close();

            return dsAddressDetails;
        }
    

    }
}
