﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace XDSPortalEnquiry.Business
{
    public class MultipleBanking_BankCodeEnquiry
    {
        XDSPortalLibrary.Business_Layer.BankCodeEnquiry moBankCodeWseManager;
        XDSPortalLibrary.Entity_Layer.BankCodes eoBankCodeWseManager;

        public MultipleBanking_BankCodeEnquiry()
        {
            moBankCodeWseManager = new XDSPortalLibrary.Business_Layer.BankCodeEnquiry();
            eoBankCodeWseManager = new XDSPortalLibrary.Entity_Layer.BankCodes();
        }
        public XDSPortalLibrary.Entity_Layer.Response GetBankCodesResult(SqlConnection con, SqlConnection AdminConnection, SqlConnection BCConnection, int intProductId, int intSubscriberEnquiryID, int intSubscriberEnquiryLogID)
        {
            string strUserName = string.Empty, strPassword = string.Empty, strAccRef = string.Empty;
            if (con.State == ConnectionState.Closed)
                con.Open();

            string rXml = "";
            double Totalcost = 0;
            DataSet ds = new DataSet();
            DataSet dsLogo = new DataSet();
            
            Entity.SubscriberEnquiryLog sel = new XDSPortalEnquiry.Entity.SubscriberEnquiryLog();
            Data.SubscriberEnquiryLog dsel = new XDSPortalEnquiry.Data.SubscriberEnquiryLog();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            Data.IdecoreturnCodes Icode = new XDSPortalEnquiry.Data.IdecoreturnCodes();



            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                sel = dsel.GetSubscriberEnquiryLogObject(con, intSubscriberEnquiryLogID);

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, sel.SubscriberID, intProductId);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, sel.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, sel.SystemUserID);

                if (spr.ReportID > 0)
                {

                    // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML 
                    // use this XML data for generating the Report 

                    //if (string.IsNullOrEmpty(sel.XMLData) || sel.XMLData == string.Empty)
                    //{
                    string strValidationStatus = "";
                    if (!string.IsNullOrEmpty(sel.VoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, sel.SubscriberID, sel.VoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1)
                    {
                        //Calculate The Total cost of the Report , including Bonus Segments

                        Totalcost = spr.UnitPrice;
                    }

                    // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                    if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                    {
                        sel.SubscriberEnquiryID = intSubscriberEnquiryID;
                        sel.SubscriberEnquiryLogID = intSubscriberEnquiryLogID;

                        eoBankCodeWseManager.Key = sel.KeyID;
                        moBankCodeWseManager.ConnectionString = BCConnection;

                        rp = moBankCodeWseManager.GetData(eoBankCodeWseManager);

                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);


                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            sel.EnquiryResult = "N";
                            sel.EnquiryStatus = "P";
                            sel.ErrorDescription = rp.ResponseData.ToString();
                            dsel.UpdateSubscriberEnquiryLogErrorAccv(con, sel);


                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            ds.ReadXml(xmlSR);
                            Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                            Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, sel.ProductID);
                            dsLogo = dsel.GetSubscriberLogo(AdminConnection, intProductId, spr.ReportID, sel.SubscriberID);
                            string status = string.Empty;
                            if (ds.Tables.Contains("BankCodeResponse"))
                            {
                                status = ds.Tables["BankCodeResponse"].Rows[0].Field<string>("Status").ToString();
                                if (ds.Tables["BankCodeResponse"].Rows[0].Field<string>("Status").ToString() == "P")
                                {

                                    DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                    dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                    dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                    dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                    dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                    dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                    dtSubscriberInput.Columns.Add("EnquiryStatus", typeof(String));
                                    dtSubscriberInput.Columns.Add("XDsRefNo", typeof(String));
                                    dtSubscriberInput.Columns.Add("ExternalRef", typeof(String));
                                    dtSubscriberInput.Columns.Add("IDNo", typeof(String));
                                    dtSubscriberInput.Columns.Add("AccountHolderName", typeof(String));                                    
                                    dtSubscriberInput.Columns.Add("AccountNumber", typeof(String));
                                    dtSubscriberInput.Columns.Add("Bank", typeof(String));
                                    dtSubscriberInput.Columns.Add("BranchCode", typeof(String));
                                    dtSubscriberInput.Columns.Add("BranchName", typeof(String));
                                    dtSubscriberInput.Columns.Add("Amount", typeof(String));
                                    dtSubscriberInput.Columns.Add("Terms", typeof(String));
                                    dtSubscriberInput.Columns.Add("ClientRefNumber", typeof(String));
                                    dtSubscriberInput.Columns.Add("refnumber", typeof(String));
                                    dtSubscriberInput.Columns.Add("LegalEntity", typeof(String));
                                    dtSubscriberInput.Columns.Add("ReportDate", typeof(String));
                                    dtSubscriberInput.Columns.Add("EmailAddress", typeof(String));
                                    dtSubscriberInput.Columns.Add("Region", typeof(String));

                                    DataRow drSubscriberInput;
                                    drSubscriberInput = dtSubscriberInput.NewRow();

                                    DataTable dtSubscriberLogoInput = new DataTable("SubscriberLogo");
                                    dtSubscriberLogoInput.Columns.Add("SubscriberID", typeof(int));
                                    dtSubscriberLogoInput.Columns.Add("ProductID", typeof(int));
                                    dtSubscriberLogoInput.Columns.Add("ReportID", typeof(int));
                                    dtSubscriberLogoInput.Columns.Add("LogoImage", typeof(byte[]));
                                    DataRow drSubscriberLogoInput;
                                    drSubscriberLogoInput = dtSubscriberLogoInput.NewRow();



                                    foreach (DataRow dr in dsLogo.Tables[0].Rows)
                                    {

                                        drSubscriberLogoInput["LogoImage"] = dr["LogoImage"];
                                        drSubscriberLogoInput["SubscriberID"] = dr["SubscriberID"];
                                        drSubscriberLogoInput["ProductID"] = dr["ProductID"];
                                        drSubscriberLogoInput["ReportID"] = dr["ReportID"];
                                    }

                                    dtSubscriberLogoInput.Rows.Add(drSubscriberLogoInput);

                                    drSubscriberInput["Region"] = sel.BusBusinessName;
                                    drSubscriberInput["EmailAddress"] = sel.EmailAddress;
                                    drSubscriberInput["ReportDate"] = DateTime.Now;
                                    drSubscriberInput["EnquiryDate"] = sel.SubscriberEnquiryDate;
                                    drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                    drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                    drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                    drSubscriberInput["EnquiryInput"] = sel.Searchinput.Trim();
                                    drSubscriberInput["LegalEntity"] = sel.EntityType;

                                    if (!(String.IsNullOrEmpty(sel.IDNo.Trim())))
                                    {
                                        drSubscriberInput["IDNo"] = sel.IDNo.Trim();                                        
                                    }
                                    else if (!(String.IsNullOrEmpty(sel.Trust.Trim())))
                                    {
                                        drSubscriberInput["IDNo"] = sel.Trust.Trim();
                                    }
                                    else if ((!(String.IsNullOrEmpty(sel.BusRegistrationNo.Trim()))) && sel.BusRegistrationNo != "//")
                                    {
                                        drSubscriberInput["IDNo"] = sel.BusRegistrationNo.Trim();
                                    }

                                    drSubscriberInput["AccountHolderName"] = sel.AccHolder.Trim();
                                    drSubscriberInput["AccountNumber"] = sel.AccountNo.Trim();
                                    drSubscriberInput["Bank"] = sel.BankName.Trim();
                                    drSubscriberInput["BranchCode"] = sel.BranchCode.Trim();
                                    drSubscriberInput["BranchName"] = sel.BranchName.Trim();
                                    drSubscriberInput["EnquiryStatus"] = "Processed";
                                    drSubscriberInput["XDSRefNo"] = sel.Reference.ToString();
                                    drSubscriberInput["ExternalRef"] = sel.SubscriberReference.ToString();
                                    drSubscriberInput["Amount"] = sel.Amount.ToString();
                                    drSubscriberInput["Terms"] = sel.Terms.ToString();
                                    drSubscriberInput["ClientRefNumber"] = sel.SubscriberID.ToString();
                                    drSubscriberInput["refnumber"] = intSubscriberEnquiryLogID;

                                    dtSubscriberInput.Rows.Add(drSubscriberInput);

                                    string resultXml = ds.Tables["BankCodeResponse"].Rows[0].Field<string>("ResultXML").ToString();

                                    DataSet dsResult = new DataSet();
                                    System.IO.StringReader xmlSR1 = new System.IO.StringReader(resultXml);
                                    dsResult.ReadXml(xmlSR1);

                                    dsResult.Tables.Add(dtSubscriberInput);
                                    dsResult.Tables.Add(dtSubscriberLogoInput);

                                    rXml = dsResult.GetXml();
                                    rp.ResponseData = rXml;
                                    rp.EnquiryLogID = sel.SubscriberEnquiryLogID;
                                    rp.EnquiryID = sel.SubscriberEnquiryID;

                                    sel.SearchOutput = "";

                                    sel.DetailsViewedDate = DateTime.Now;
                                    sel.DetailsViewedYN = true;
                                    if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(sel.VoucherCode)))
                                    {
                                        sel.Billable = false;
                                    }
                                    else
                                    {
                                        sel.Billable = true;
                                    }
                                    sel.ChangedByUser = sel.CreatedByUser;
                                    sel.ChangedOnDate = DateTime.Now;
                                    sel.SearchOutput = "";

                                    sel.KeyID = rp.ResponseKey;
                                    sel.KeyType = rp.ResponseKeyType;

                                    sel.BillingTypeID = spr.BillingTypeID;
                                    sel.BillingPrice = spr.UnitPrice;

                                    sel.XMLData = rXml;
                                    sel.EnquiryResult = Entity.SubscriberEnquiryLog.EnquiryResultInd.R.ToString();
                                    sel.EnquiryStatus = Entity.SubscriberEnquiryLog.EnquiryStatusInd.C.ToString();
                                    sel.ProductID = intProductId;

                                    // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost

                                    if (sub.PayAsYouGo == 1)
                                    {
                                        dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, sel.SubscriberID, Totalcost);
                                    }
                                    if (!(string.IsNullOrEmpty(sel.VoucherCode)))
                                    {
                                        dSV.UpdateSubscriberVoucher(AdminConnection, sel.VoucherCode, sel.CreatedByUser);
                                    }
                                    rp.ResponseData = dsResult.GetXml();
                                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                                }

                                else
                                {

                                    DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                    dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                    dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                    dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                    dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                    dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                    dtSubscriberInput.Columns.Add("EnquiryStatus", typeof(String));
                                    dtSubscriberInput.Columns.Add("XDsRefNo", typeof(String));
                                    dtSubscriberInput.Columns.Add("ExternalRef", typeof(String));
                                    dtSubscriberInput.Columns.Add("IDNo", typeof(String));
                                    dtSubscriberInput.Columns.Add("AccountHolderName", typeof(String));
                                    dtSubscriberInput.Columns.Add("AccountNumber", typeof(String));
                                    dtSubscriberInput.Columns.Add("Bank", typeof(String));
                                    dtSubscriberInput.Columns.Add("BranchCode", typeof(String));
                                    dtSubscriberInput.Columns.Add("BranchName", typeof(String));
                                    dtSubscriberInput.Columns.Add("Amount", typeof(String));
                                    dtSubscriberInput.Columns.Add("Terms", typeof(String));
                                    dtSubscriberInput.Columns.Add("ClientRefNumber", typeof(String));
                                    dtSubscriberInput.Columns.Add("refnumber", typeof(String));
                                    dtSubscriberInput.Columns.Add("LegalEntity", typeof(String));
                                    dtSubscriberInput.Columns.Add("ReportDate", typeof(String));
                                    dtSubscriberInput.Columns.Add("EmailAddress", typeof(String));
                                    dtSubscriberInput.Columns.Add("Region", typeof(String));

                                    DataRow drSubscriberInput;
                                    drSubscriberInput = dtSubscriberInput.NewRow();

                                    DataTable dtSubscriberLogoInput = new DataTable("SubscriberLogo");
                                    dtSubscriberLogoInput.Columns.Add("SubscriberID", typeof(int));
                                    dtSubscriberLogoInput.Columns.Add("ProductID", typeof(int));
                                    dtSubscriberLogoInput.Columns.Add("ReportID", typeof(int));
                                    dtSubscriberLogoInput.Columns.Add("LogoImage", typeof(byte[]));
                                    DataRow drSubscriberLogoInput;
                                    drSubscriberLogoInput = dtSubscriberLogoInput.NewRow();


                                    foreach (DataRow dr in dsLogo.Tables[0].Rows)
                                    {

                                        drSubscriberLogoInput["LogoImage"] = dr["LogoImage"];
                                        drSubscriberLogoInput["SubscriberID"] = dr["SubscriberID"];
                                        drSubscriberLogoInput["ProductID"] = dr["ProductID"];
                                        drSubscriberLogoInput["ReportID"] = dr["ReportID"];
                                    }

                                    dtSubscriberLogoInput.Rows.Add(drSubscriberLogoInput);

                                    drSubscriberInput["Region"] = sel.BusBusinessName;
                                    drSubscriberInput["EmailAddress"] = sel.EmailAddress;
                                    drSubscriberInput["ReportDate"] = DateTime.Now;
                                    drSubscriberInput["EnquiryDate"] = sel.SubscriberEnquiryDate;
                                    drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                    drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                    drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                    drSubscriberInput["EnquiryInput"] = sel.Searchinput.Trim();
                                    drSubscriberInput["LegalEntity"] = sel.EntityType;

                                    if (!(String.IsNullOrEmpty(sel.IDNo.Trim())))
                                    {
                                        drSubscriberInput["IDNo"] = sel.IDNo.Trim();
                                    }
                                    else if (!(String.IsNullOrEmpty(sel.Trust.Trim())))
                                    {
                                        drSubscriberInput["IDNo"] = sel.Trust.Trim();
                                    }
                                    else if ((!(String.IsNullOrEmpty(sel.BusRegistrationNo.Trim()))) && sel.BusRegistrationNo != "//")
                                    {
                                        drSubscriberInput["IDNo"] = sel.BusRegistrationNo.Trim();
                                    }

                                    drSubscriberInput["AccountHolderName"] = sel.AccHolder.Trim();
                                    drSubscriberInput["AccountNumber"] = sel.AccountNo.Trim();
                                    drSubscriberInput["Bank"] = sel.BankName.Trim();
                                    drSubscriberInput["BranchCode"] = sel.BranchCode.Trim();
                                    drSubscriberInput["BranchName"] = sel.BranchName.Trim();
                                    drSubscriberInput["EnquiryStatus"] = status == "H" ? "On Hold" : (status == "N" ? "Processing" : "Unknown");
                                    drSubscriberInput["XDSRefNo"] = sel.Reference.ToString();
                                    drSubscriberInput["ExternalRef"] = sel.SubscriberReference.ToString();
                                    drSubscriberInput["Amount"] = sel.Amount.ToString();
                                    drSubscriberInput["Terms"] = sel.Terms.ToString();
                                    drSubscriberInput["ClientRefNumber"] = sel.SubscriberID.ToString();
                                    drSubscriberInput["refnumber"] = intSubscriberEnquiryLogID;
                                    
                                    dtSubscriberInput.Rows.Add(drSubscriberInput);

                                    ds.Tables.Add(dtSubscriberInput);
                                    ds.Tables.Add(dtSubscriberLogoInput);

                                    rXml = ds.GetXml();
                                    rp.ResponseData = rXml;
                                    rp.EnquiryLogID = sel.SubscriberEnquiryLogID;
                                    rp.EnquiryID = sel.SubscriberEnquiryID;

                                    sel.SearchOutput = "";

                                    sel.DetailsViewedYN = false;
                                    sel.Billable = false;

                                    sel.ChangedByUser = sel.CreatedByUser;
                                    sel.ChangedOnDate = DateTime.Now;
                                    sel.SearchOutput = "";

                                    sel.KeyID = rp.ResponseKey;
                                    sel.KeyType = rp.ResponseKeyType;

                                    sel.BillingTypeID = spr.BillingTypeID;
                                    sel.BillingPrice = spr.UnitPrice;

                                    sel.XMLData = rXml;
                                    sel.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                                    sel.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    sel.ProductID = intProductId;

                                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;

                                }

                                XdsPortalReports.BankCodesReport oBankCodesReport = new XdsPortalReports.BankCodesReport(rp.ResponseData);
                                MemoryStream oStream = new MemoryStream();
                                oBankCodesReport.ExportToPdf(oStream);

                                oStream.Position = 0;

                                sel.ReportFile = oStream.ToArray();
                                sel.FileName = oBankCodesReport.Name;

                            }
                            else
                            {
                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                            }





                            dsel.UpdateSubscriberEnquiryLog(con, sel);

                            rp.ResponseKey = intSubscriberEnquiryLogID;
                        }
                        else
                        {
                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                            rp.ResponseKey = 0;
                        }

                    }

                    else
                    {
                        // When Subscriberdoesn't have enough credit Limit.
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }

            catch (Exception oException)
            {
                sel.EnquiryResult = "E";
                sel.ErrorDescription = oException.Message;
                sel.SubscriberEnquiryID = dsel.UpdateSubscriberEnquiryLogError(con, sel);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;

        }

    }
}
