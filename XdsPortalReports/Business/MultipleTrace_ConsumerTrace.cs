﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace XDSPortalEnquiry.Business
{
    public class MultipleTrace_ConsumerTrace
    {

        private XDSPortalLibrary.Business_Layer.ConsumerTrace moConsumerTraceWseManager;
        private XDSPortalLibrary.Entity_Layer.ConsumerTrace eoConsumerTraceWseManager;


        public MultipleTrace_ConsumerTrace()
        {
            moConsumerTraceWseManager = new XDSPortalLibrary.Business_Layer.ConsumerTrace();
            eoConsumerTraceWseManager = new XDSPortalLibrary.Entity_Layer.ConsumerTrace();
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitMultipleConsumerTrace(SqlConnection Enquirycon, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, DataSet dsBonusDataSegment, bool bBonusChecking, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            try
            {
                if (Enquirycon.State == ConnectionState.Closed)
                    Enquirycon.Open();
                if (AdminConnection.State == ConnectionState.Closed)
                    AdminConnection.Open();
            }
            catch (Exception ex)
            {
            }

            string rXml = "";
            double Totalcost = 0;
            string strValidationStatus = "";
           
            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();
   
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(Enquirycon, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(Enquirycon, intSubscriberEnquiryResultID);

                SqlConnection objConstring = new SqlConnection(eSe.ExtraVarInput1.ToString());

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                if (spr.ReportID > 0)
                {

                    eoConsumerTraceWseManager.BonusSegments = null;
                    if (dsBonusDataSegment != null)
                    {
                        if (dsBonusDataSegment.Tables.Count > 0 && dsBonusDataSegment.Tables[0].Rows.Count > 0)
                        {
                            dsBonusDataSegment.DataSetName = "BonusSegments";
                            dsBonusDataSegment.Tables[0].TableName = "Segment";
                            eoConsumerTraceWseManager.BonusSegments = dsBonusDataSegment;
                        }
                    }


                    // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML 
                    // use this XML data for generating the Report 



                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }
                        else if (sub.PayAsYouGo == 1)
                        {
                            //Calculate The Total cost of the Report , including Bonus Segments

                            Totalcost = spr.UnitPrice;

                            if (eoConsumerTraceWseManager.BonusSegments != null)
                            {

                                foreach (DataRow dr in eoConsumerTraceWseManager.BonusSegments.Tables[0].Rows)
                                {
                                    if (dr["BonusViewed"].ToString().ToLower() == "true")
                                    {
                                        Totalcost = Totalcost + Convert.ToDouble(dr["BonusPrice"].ToString());
                                    }
                                }
                            }
                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {

                            
                            eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.ProductID = eSe.ProductID;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerTraceWseManager.TmpReference = eSC.ExtraVarOutput1;
                            moConsumerTraceWseManager.ConnectionString = objConstring;
                            eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                            eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                            eoConsumerTraceWseManager.AssociationTypeCode = sub.AssociationTypeCode;
                            eoConsumerTraceWseManager.SAFPSIndicator = sub.SAFPSIndicator;

                            XDSPortalEnquiry.Data.Consumer oEnquiryConsumer = new XDSPortalEnquiry.Data.Consumer();
                            oEnquiryConsumer.CheckBlockedStatus(AdminConnection, int.Parse(eSe.SubscriberID.ToString()), int.Parse(eSC.KeyID.ToString()), eSe.ProductID, sub.SubscriberAssociationCode, eSe.ProductID);


                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.GetConsumerTraceReport(eoConsumerTraceWseManager);
                            //rp = moConsumerTraceWseManager.GetData(eoConsumerTraceWseManager);
                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(Enquirycon, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.GetConsumerTraceReport(eoConsumerTraceWseManager);                            
                            }

                            rXml = rp.ResponseData;

                            System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                            {

                                eSC.DetailsViewedYN = false;
                                eSC.Billable = false;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                                eSC.CreatedByUser = eSe.CreatedByUser;
                                eSC.CreatedOnDate = DateTime.Now;

                                dSC.UpdateSubscriberEnquiryResult(Enquirycon, eSC);
                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {

                                eSC.EnquiryResult = "E";
                                eSe.ErrorDescription = rp.ResponseData.ToString();
                                dSe.UpdateSubscriberEnquiryError(Enquirycon, eSe);


                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Bonus)
                            {
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("Segments"))
                                {

                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.BonusIncluded = true;
                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;
                                    eSC.XMLBonus = rXml.Replace("'", "''");
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.B.ToString();

                                    dSC.UpdateSubscriberEnquiryResult(Enquirycon, eSC);
                                    rp.EnquiryID = intSubscriberEnquiryID;
                                    rp.EnquiryResultID = intSubscriberEnquiryResultID;


                                    foreach (DataRow r in ds.Tables["Segments"].Rows)
                                    {
                                        eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                        eSB.DataSegmentID = int.Parse(r["DataSegmentID"].ToString());
                                        eSB.DataSegmentName = r["DataSegmentName"].ToString().Replace("'", "''");
                                        eSB.DataSegmentDisplayText = r["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                        eSB.BonusViewed = bool.Parse(r["BonusViewed"].ToString());
                                        eSB.Billable = false;
                                        eSB.CreatedByUser = eSe.CreatedByUser;
                                        eSB.CreatedOnDate = DateTime.Now;

                                        dSB.InsertSubscriberEnquiryResultBonus(Enquirycon, eSB);
                                    }
                                }
                            }

                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("ConsumerDetail"))
                                {
                                    Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                    Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, eSe.ProductID);

                                    DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                    dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                    dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                    dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                    dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                    dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                    dtSubscriberInput.Columns.Add("EnquiryReason", typeof(String));

                                    DataRow drSubscriberInput;
                                    drSubscriberInput = dtSubscriberInput.NewRow();

                                    drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                    drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                    drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                    drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                    drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();
                                    drSubscriberInput["EnquiryReason"] = eSe.EnquiryReason.Trim();

                                    dtSubscriberInput.Rows.Add(drSubscriberInput);

                                    ds.Tables.Add(dtSubscriberInput);

                                    if (eSC.BonusIncluded == true)
                                    {
                                        Data.SubscriberEnquiryResultBonus oSubscriberEnquiryResultBonus = new XDSPortalEnquiry.Data.SubscriberEnquiryResultBonus();
                                       // DataSet dsBonus = oSubscriberEnquiryResultBonus.GetSelectedSubscriberEnquiryResultBonus(con, eSC.SubscriberEnquiryResultID);
                                        DataSet dsBonus = eoConsumerTraceWseManager.BonusSegments;

                                        DataTable dtBonusSelected = new DataTable("ConsumerBonusSelected");

                                        dtBonusSelected.Columns.Add("DataSegmentID", typeof(String));
                                        dtBonusSelected.Columns.Add("DataSegmentName", typeof(String));
                                        dtBonusSelected.Columns.Add("DataSegmentDisplayText", typeof(String));

                                        DataRow drBonusSelected;
                                        if (dsBonus != null)
                                        {
                                            if (dsBonus.Tables[0].Rows.Count > 0)
                                            {
                                                foreach (DataRow r in dsBonus.Tables[0].Rows)
                                                {
                                                    drBonusSelected = dtBonusSelected.NewRow();
                                                    drBonusSelected["DataSegmentID"] = r["DataSegmentID"];
                                                    drBonusSelected["DataSegmentName"] = r["DataSegmentName"];
                                                    drBonusSelected["DataSegmentDisplayText"] = r["DataSegmentDisplayText"];
                                                    dtBonusSelected.Rows.Add(drBonusSelected);

                                                    eSB.SubscriberEnquiryResultBonusID = 0;
                                                    eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                                    eSB.DataSegmentID = Convert.ToInt32(r["DataSegmentID"]);
                                                    eSB.DataSegmentName = r["DataSegmentName"].ToString();
                                                    eSB.DataSegmentDisplayText = r["DataSegmentDisplayText"].ToString();
                                                    eSB.BonusViewed = Convert.ToBoolean(r["BonusViewed"].ToString());
                                                    if (eSB.BonusViewed)
                                                    {
                                                        eSB.Billable = true;
                                                    }
                                                    else
                                                    {
                                                        eSB.Billable = false;
                                                    }
                                                    eSB.ChangedByUser = eSB.CreatedByUser;
                                                    eSB.ChangedOnDate = DateTime.Now;
                                                    dSB.UpdateSubscriberEnquiryResultBonus(Enquirycon, eSB);
                                                }
                                                ds.Tables.Add(dtBonusSelected);
                                            }
                                            dsBonus.Dispose();
                                        }
                                       
                                    }

                                    rXml = ds.GetXml();
                                    rp.ResponseData = rXml;

                                    foreach (DataRow r in ds.Tables["ConsumerDetail"].Rows)
                                    {
                                        eSC.DetailsViewedDate = DateTime.Now;
                                        eSC.DetailsViewedYN = true;
                                        if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                        {
                                            eSC.Billable = false;
                                        }
                                        else
                                        {
                                            eSC.Billable = true;
                                        }
                                        eSC.ChangedByUser = eSe.CreatedByUser;
                                        eSC.ChangedOnDate = DateTime.Now;
                                        eSC.SearchOutput = "";

                                        if (r.Table.Columns.Contains("IDNo"))
                                        {
                                            if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                            {
                                                eSC.IDNo = r["IDNo"].ToString();
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                            }
                                        }
                                        if (r.Table.Columns.Contains("PassportNo"))
                                        {
                                            if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                            {
                                                eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                            }
                                        }
                                        if (r.Table.Columns.Contains("Surname"))
                                        {
                                            if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                            {
                                                eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                            }
                                        }

                                        if (r.Table.Columns.Contains("FirstName"))
                                        {
                                            if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                            {
                                                eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                            }
                                        }

                                        if (eSC.SearchOutput.Length > 0)
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                        }

                                        eSC.KeyID = rp.ResponseKey;
                                        eSC.KeyType = rp.ResponseKeyType;
                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                        eSC.BillingTypeID = spr.BillingTypeID;
                                        eSC.BillingPrice = spr.UnitPrice;

                                        eSC.XMLData = rXml.Replace("'", "''");
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                        eSC.ProductID = eSe.ProductID;

                                        if ((ds.Tables.Contains("ConsumerNLRDebtSummary") && ds.Tables["ConsumerNLRDebtSummary"].Rows.Count > 0) || (ds.Tables.Contains("ConsumerCPANLRDebtSummary") && ds.Tables["ConsumerCPANLRDebtSummary"].Rows.Count > 0) || (ds.Tables.Contains("ConsumerNLRAccountStatus") && ds.Tables["ConsumerNLRAccountStatus"].Rows.Count > 0) || (ds.Tables.Contains("ConsumerNLR24MonthlyPayment") && ds.Tables["ConsumerNLR24MonthlyPayment"].Rows.Count > 0))
                                            eSC.NLRDataIncluded = true;

                                        dSC.UpdateSubscriberEnquiryResult(Enquirycon, eSC);
                                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                        // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                        if (sub.PayAsYouGo == 1)
                                        {
                                            dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                        }
                                        if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                        {
                                            dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                        }
                                    }
                                }
                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                            {
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("ConsumerDetails"))
                                {
                                    foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                    {

                                        eSC.SearchOutput = "";

                                        if (r.Table.Columns.Contains("IDNo"))
                                        {
                                            if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                            {
                                                eSC.IDNo = r["IDNo"].ToString();
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                            }
                                        }
                                        if (r.Table.Columns.Contains("PassportNo"))
                                        {
                                            if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                            {
                                                eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                            }
                                        }
                                        if (r.Table.Columns.Contains("Surname"))
                                        {
                                            if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                            {
                                                eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                            }
                                        }

                                        if (r.Table.Columns.Contains("FirstName"))
                                        {
                                            if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                            {
                                                eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                            }
                                        }

                                        if (eSC.SearchOutput.Length > 0)
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                        }

                                        eSC.DetailsViewedYN = false;
                                        eSC.Billable = false;
                                        eSC.ChangedByUser = eSe.CreatedByUser;
                                        eSC.ChangedOnDate = DateTime.Now;
                                        eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                        eSC.KeyType = rp.ResponseKeyType;

                                        eSC.BillingTypeID = spr.BillingTypeID;
                                        eSC.BillingPrice = spr.UnitPrice;

                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();

                                        dSC.UpdateSubscriberEnquiryResult(Enquirycon, eSC);
                                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                }
                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                            {
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("ConsumerDetails"))
                                {
                                    foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                    {
                                        eSC.SearchOutput = "";

                                        if (r.Table.Columns.Contains("IDNo"))
                                        {
                                            if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                            {
                                                eSC.IDNo = r["IDNo"].ToString();
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                            }
                                        }
                                        if (r.Table.Columns.Contains("PassportNo"))
                                        {
                                            if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                            {
                                                eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                            }
                                        }
                                        if (r.Table.Columns.Contains("Surname"))
                                        {
                                            if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                            {
                                                eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                            }
                                        }

                                        if (r.Table.Columns.Contains("FirstName"))
                                        {
                                            if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                            {
                                                eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                            }
                                        }

                                        if (eSC.SearchOutput.Length > 0)
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                        }

                                        eSC.DetailsViewedYN = false;
                                        eSC.Billable = false;
                                        eSC.CreatedByUser = eSe.CreatedByUser;
                                        eSC.CreatedOnDate = DateTime.Now;
                                        eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                        eSC.KeyType = rp.ResponseKeyType;

                                        eSC.BillingTypeID = spr.BillingTypeID;
                                        eSC.BillingPrice = spr.UnitPrice;

                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();

                                        dSC.UpdateSubscriberEnquiryResult(Enquirycon, eSC);
                                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                }
                            }

                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.

                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }
                    }
                    else
                    {
                        // When User want to Re-Open a report 

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                Enquirycon.Close();
                AdminConnection.Close();

                SqlConnection.ClearPool(Enquirycon);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(Enquirycon, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (Enquirycon.State == ConnectionState.Open)
                    Enquirycon.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(Enquirycon);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }
    
    }
}
