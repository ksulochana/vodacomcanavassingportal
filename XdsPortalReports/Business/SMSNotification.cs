﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalEnquiry.Business
{
    public  class SMSNotification
    {
        XDSPortalLibrary.Entity_Layer.SMSNotification eSMSNotification = null;

        public SMSNotification()
        {
            eSMSNotification = new XDSPortalLibrary.Entity_Layer.SMSNotification();
        }

        public XDSPortalLibrary.Entity_Layer.Response SendSMSNotification(SqlConnection con, SqlConnection AdminConnection, SqlConnection SMSConnection, int intProductID, int intSubscriberID,int intSystemUserID,int intConsumerDefaultAlertID,string strEnquiryType, XDSPortalLibrary.Entity_Layer.SMSNotification oSMSNotification)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();            
            
            string rXml = "";                        
            DataSet ds = new DataSet();
            

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            Data.DefaultAlerts oDefaultAlerts = new XDSPortalEnquiry.Data.DefaultAlerts();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductID);
            

            try
            {
                if (spr.ReportID > 0)
                {
                    eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                    eSe.SubscriberID = intSubscriberID;


                    eSe.IDNo = intConsumerDefaultAlertID.ToString();
                    eSe.ProductID = intProductID;
                    eSe.SecondName = oSMSNotification.ContactNo;
                    eSe.SubscriberEnquiryDate = DateTime.Now;
                    eSe.SubscriberName = sub.SubscriberName;                    
                    eSe.SystemUserID = intSystemUserID;
                    eSe.SystemUser = sys.SystemUserFullName;
                    eSe.CreatedByUser = sys.Username;
                    eSe.CreatedOnDate = DateTime.Now;
                    eSe.FirstInitial = "true";                    
                    eSe.AccountNo = oSMSNotification.ClientContactDetails;
                    eSe.SearchInput = "";

                    //Generate serach input string

                    if (!string.IsNullOrEmpty(eSe.IDNo))
                    {
                        eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                    }
                    if (!string.IsNullOrEmpty(eSe.PassportNo))
                    {
                        eSe.SearchInput = eSe.SearchInput + " | " + eSe.PassportNo;
                    }
                    if (!string.IsNullOrEmpty(eSe.Surname))
                    {
                        eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                    }
                    if (!string.IsNullOrEmpty(eSe.FirstInitial))
                    {
                        eSe.SearchInput = eSe.SearchInput + " | " + eSe.FirstName;
                    }

                    if (eSe.SearchInput.Length > 0)
                    {
                        eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                    }

                    // Log the Current Enquiry to SubscriberEnquiry Table

                    int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                    eSe.SubscriberEnquiryID = intSubscriberEnquiryID;
                    oSMSNotification.SubscriberID = intSubscriberID;

                    int QueueID = 0; 
                    QueueID = oDefaultAlerts.InsertSMSNotifications(SMSConnection, oSMSNotification);                    

                    eSC.SubscriberEnquiryID = eSe.SubscriberEnquiryID;
                    eSC.IDNo = eSe.IDNo;
                    eSC.ProductID = eSe.ProductID;
                    eSC.DetailsViewedYN = true;
                    eSC.DetailsViewedDate = DateTime.Now;
                    if (sub.PayAsYouGo == 1)
                    {
                        eSC.Billable = false;
                    }
                    else
                    {
                        eSC.Billable = true;
                    }
                    eSC.BillingTypeID = spr.BillingTypeID;
                    eSC.KeyID = intConsumerDefaultAlertID;
                    eSC.KeyType = "S";
                    eSC.BillingPrice = spr.UnitPrice;
                    eSC.XMLData = rXml;
                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                    eSC.BonusIncluded = false;
                    eSC.BusBusinessName = strEnquiryType;

                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                    rp.ResponseKey = intConsumerDefaultAlertID;
                    rp.ResponseReferenceNo = QueueID.ToString(); 

                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "You are not authorized for sending the SMS. Please contact XDS to activate";                    
                }

            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                
                rp.EnquiryID = eSe.SubscriberEnquiryID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (oException.Message.Contains("A .NET Framework"))
                {
                    rp.ResponseData = "Error: The System is unable to process this request. Please contact XDS Support";
                }

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            return rp;
        }
    }
}
