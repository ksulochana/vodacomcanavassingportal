﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalEnquiry.Business
{
    public class BankingEnquiry_AccountVerification
    {
        private XDSPortalLibrary.Business_Layer.AccountVerificationIdeco moAccountVerificationWseManager;
        private XDSPortalLibrary.Entity_Layer.AccountVerification eoAccountVerificationWseManager;

        public BankingEnquiry_AccountVerification()
        {
            moAccountVerificationWseManager = new XDSPortalLibrary.Business_Layer.AccountVerificationIdeco();
            eoAccountVerificationWseManager = new XDSPortalLibrary.Entity_Layer.AccountVerification();
        }
        public XDSPortalLibrary.Entity_Layer.Response SubmitAccountVerification(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strEntityType, string strRegNo1, string strRegNo2, string strRegNo3, string Trust, string strIDNo, string strSurName, string strInitials, string strAccNo, string strBranchCode, string strAcctype, string strBankName, string ClientFirstName, string ClientSurName, string strEmailAddress, bool bConfirmationChkBox, string strExtRef, bool bBonusChecking, string strVoucherCode)
        {

            string strUserName = string.Empty, strPassword = string.Empty, strAccRef = string.Empty;
            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            int intSubscriberEnquiryLogID = 0;
            string rXml = "";
            int MatchResult = 0;
            DataSet ds = new DataSet();
            string strValidationStatus = "";
            Boolean boolSubmitEnquiry = true;
            DataSet dsLogon = new DataSet();
            DataSet dsBankName = new DataSet();
            DataSet dsAcctype = new DataSet();

            Data.IdecoLogonDetails dlogin = new XDSPortalEnquiry.Data.IdecoLogonDetails();
            dsLogon = dlogin.GetIdecoLogonDetailsDataSet(AdminConnection);

            strUserName = dsLogon.Tables[0].Rows[0].Field<string>("Username").ToString();
            strPassword = dsLogon.Tables[0].Rows[0].Field<string>("Password").ToString();
            strAccRef = dsLogon.Tables[0].Rows[0].Field<string>("AccRef").ToString();

            Data.IdecoBankNames dBankName = new XDSPortalEnquiry.Data.IdecoBankNames();
            Data.IdecoAccountTypes dAcctype = new XDSPortalEnquiry.Data.IdecoAccountTypes();

            dsBankName = dBankName.GetIdecoBankNamesDataSet(AdminConnection);
            dsAcctype = dAcctype.GetIdecoAccountTypesDataSet(AdminConnection);

            DataColumn[] dcolpkBank = new DataColumn[1];
            dcolpkBank[0] = dsBankName.Tables[0].Columns["Code"];

            dsBankName.Tables[0].PrimaryKey = dcolpkBank;

            DataColumn[] dcolpkAcctype = new DataColumn[1];
            dcolpkAcctype[0] = dsAcctype.Tables[0].Columns["Code"];

            dsAcctype.Tables[0].PrimaryKey = dcolpkAcctype;

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            Entity.SubscriberEnquiryLog sel = new XDSPortalEnquiry.Entity.SubscriberEnquiryLog();
            Data.SubscriberEnquiryLog dsel = new XDSPortalEnquiry.Data.SubscriberEnquiryLog();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            try
            {
                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {


                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }


                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {

                        sel.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        sel.SubscriberID = intSubscriberID;
                        sel.ProductID = intProductId;
                        sel.SubscriberEnquiryDate = DateTime.Now;
                        sel.SubscriberName = strSubscriberName;
                        sel.SubscriberReference = strExtRef;
                        sel.SystemUserID = intSystemUserID;
                        sel.BusRegistrationNo = strRegNo1 +"/" +strRegNo2 + "/"+strRegNo3;
                        sel.IDNo = strIDNo;
                        //sel.BankName = strBankName;
                        sel.BankName = dsBankName.Tables[0].Rows.Find(strBankName).Field<string>("BankName").ToString();
                        sel.AccountNo = strAccNo;
                        sel.BranchCode = strBranchCode;
                        sel.FirstName = strInitials;
                        sel.SurName = strSurName;
                        sel.EmailAddress = strEmailAddress;
                        //sel.BranchName = strAcctype;
                        sel.BranchName = dsAcctype.Tables[0].Rows.Find(strAcctype).Field<string>("AccountType").ToString();
                        sel.SystemUserID = intSystemUserID;
                        sel.SystemUserName = sys.SystemUserFullName;
                        sel.CreatedByUser = sys.Username;
                        sel.CreatedOnDate = DateTime.Now;
                        sel.Searchinput = "";
                        sel.KeyType = "F";
                        sel.AccHolder = ClientFirstName;
                        sel.BusBusinessName = ClientSurName;
                        sel.Trust = Trust;
                        sel.EntityType = strEntityType;
                        //sel.KeyType = 


                        if (!string.IsNullOrEmpty(sel.IDNo))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.IDNo;
                        }
                        if (!string.IsNullOrEmpty(sel.BusRegistrationNo))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        }
                        if (!string.IsNullOrEmpty(sel.AccountNo))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.AccountNo;
                        }
                        if (!string.IsNullOrEmpty(sel.SurName))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.SurName;
                        }
                        if (!string.IsNullOrEmpty(sel.BranchCode))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.BranchCode;
                        }
                         if (!string.IsNullOrEmpty(sel.Trust ))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.Trust;
                        }

                        if (sel.Searchinput.Length > 0)
                        {
                            sel.Searchinput = sel.Searchinput.Substring(3, sel.Searchinput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table
                        intSubscriberEnquiryLogID = dsel.InsertSubscriberEnquiryLog(con, sel);
                        sel.SubscriberEnquiryLogID = intSubscriberEnquiryLogID;

                        if (!(string.IsNullOrEmpty(strIDNo)))
                        {
                            eoAccountVerificationWseManager.IDNo = strIDNo;
                        }
                        else
                        {
                            eoAccountVerificationWseManager.IDNo = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        }
                        eoAccountVerificationWseManager.ExternalReference = strExtRef;
                        eoAccountVerificationWseManager.Initials = strInitials;
                        eoAccountVerificationWseManager.AccNo = strAccNo;
                        eoAccountVerificationWseManager.Acctype = strAcctype;
                        eoAccountVerificationWseManager.BankName = strBankName;
                        eoAccountVerificationWseManager.BranchCode = strBranchCode;
                        eoAccountVerificationWseManager.SurName = strSurName;
                        eoAccountVerificationWseManager.UserName = strUserName;
                        eoAccountVerificationWseManager.Password = strPassword;
                        eoAccountVerificationWseManager.AccountRef = strAccRef;
                        eoAccountVerificationWseManager.EmailAddress = strEmailAddress;
                        eoAccountVerificationWseManager.RequesterFirstName = ClientFirstName;
                        eoAccountVerificationWseManager.RequesterSurName = ClientSurName;
                        

                        // Submit data for matching 
                        rp = moAccountVerificationWseManager.search(eoAccountVerificationWseManager);

                        // Get Response data
                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                        sel.KeyID = rp.ResponseKey;
                        sel.KeyType = rp.ResponseKeyType;
                        sel.ExtraVarInput1 = rp.TmpReference;
                        sel.IsAHVRequest = true;
                        rp.EnquiryLogID = intSubscriberEnquiryLogID;

                        dsel.UpdateSubscriberEnquiryLog(con, sel);

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            sel.EnquiryResult = "E";
                            sel.ErrorDescription = rp.ResponseData.ToString();
                            dsel.UpdateSubscriberEnquiryLogError(con, sel);
                        }

                        if (sub.PayAsYouGo == 1)
                        {
                            dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                        }
                        if (!(strVoucherCode == string.Empty))
                        {
                            dSV.UpdateSubscriberVoucher(AdminConnection, strVoucherCode, sys.Username);
                        }

                    }

                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }

            catch (Exception oException)
            {
                sel.EnquiryResult = "E";
                sel.ErrorDescription = oException.Message;
                sel.SubscriberEnquiryLogID = dsel.UpdateSubscriberEnquiryLogError(con, sel);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                MatchResult = sel.SubscriberEnquiryLogID;
                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }

            ds.Dispose();
            dsLogon.Dispose();
            dsBankName.Dispose();
            dsAcctype.Dispose();

            return rp;
        }
    }
}
