using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace XdsPortalReports
{
    public partial class ConsumerTraceReport : DevExpress.XtraReports.UI.XtraReport
    {
        public ConsumerTraceReport(string strXML)
        {
            InitializeComponent();
            System.IO.StringReader Objsr = new System.IO.StringReader(strXML);
            DataSet dsXML = new DataSet();
            dsXML.ReadXml(Objsr);

            if (dsXML.Tables.Contains("ConsumerDetail"))
            {
                dsXML.Tables["ConsumerDetail"].Columns.Add("FBirthDate", System.Type.GetType("System.DateTime"), "Iif(BirthDate = '', '1900-01-01T00:00:00', BirthDate)");
                this.lblDOBValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDetail.FBirthDate", "{0:dd/MM/yyyy}") });
            }
            else
            {
                ConsumerDetail.Visible = false;
            }
            if (!(dsXML.Tables.Contains("ConsumerFraudIndicatorsSummary")))
            {
                FraudIndicatorSummary.Visible = false;
            }
            if (!(dsXML.Tables.Contains("ConsumerScoring")))
            {
                Scoring.Visible = false;
            }
            else
            {
                dsXML.Tables["ConsumerScoring"].Columns.Add("FScoreDate", System.Type.GetType("System.DateTime"), "Iif(ScoreDate = '', '1900-01-01T00:00:00', ScoreDate)");
                this.lblDateValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerScoring.FScoreDate", "{0:dd/MM/yyyy}") });
            }

            if (!(dsXML.Tables.Contains("ConsumerDebtSummary")))
            {
                DebtSummary.Visible = false;
            }
            else
            {
                dsXML.Tables["ConsumerDebtSummary"].Columns.Add("FMostRecentJudgmentDate", System.Type.GetType("System.DateTime"), "Iif(MostRecentJudgmentDate = '', '1900-01-01T00:00:00', MostRecentJudgmentDate)");
                this.lblJdgDateValue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.FMostRecentJudgmentDate", "{0:dd/MM/yyyy}") });
            }

            if (!(dsXML.Tables.Contains("ConsumerAccountGoodBadSummary")))
            {
                AccountGoodBadSummary.Visible = false;
            }

            if (!(dsXML.Tables.Contains("ConsumerDetailConfirmationName")))
            {
                NameConfirmation.Visible = false;
            }
            else
            {
                dsXML.Tables["ConsumerDetailConfirmationName"].Columns.Add("FDateConfirmed", System.Type.GetType("System.DateTime"), "Iif(DateConfirmed = '', '1900-01-01T00:00:00', DateConfirmed)");
                this.lblNHdatevalue.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDetailConfirmationName.FDateConfirmed", "{0:dd/MM/yyyy}") });
            }

            if (!(dsXML.Tables.Contains("ConsumerDetailConfirmationAddress")))
            {
                AddressConfirmation.Visible = false;
            }
            else
            {
                dsXML.Tables["ConsumerDetailConfirmationAddress"].Columns.Add("FDateConfirmed", System.Type.GetType("System.DateTime"), "Iif(DateConfirmed = '', '1900-01-01T00:00:00', DateConfirmed)");
                this.lblCDdate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDetailConfirmationAddress.FDateConfirmed", "{0:dd/MM/yyyy}") });
            }
            if (!(dsXML.Tables.Contains("ConsumerDetailConfirmationContact")))
            {
                ContactNoConfirmation.Visible = false;
            }
            else
            {
                dsXML.Tables["ConsumerDetailConfirmationContact"].Columns.Add("FLandLineNumberConfirmDate", System.Type.GetType("System.DateTime"), "Iif(LandLineNumberConfirmDate = '', '1900-01-01T00:00:00', LandLineNumberConfirmDate)");
                dsXML.Tables["ConsumerDetailConfirmationContact"].Columns.Add("FCellNoConfirmDate", System.Type.GetType("System.DateTime"), "Iif(CellNoConfirmDate = '', '1900-01-01T00:00:00', CellNoConfirmDate)");
                this.lblCConfirmedLLD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDetailConfirmationContact.FLandLineNumberConfirmDate", "{0:dd/MM/yyyy}") });
                this.lblCConfirmedCellNoD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDetailConfirmationContact.FCellNoConfirmDate", "{0:dd/MM/yyyy}") });
            }
            if (!(dsXML.Tables.Contains("ConsumerDetailConfirmationEmloyment")))
            {
                EmploymentDetailConfirmation.Visible = false;
            }
            else
            {
                dsXML.Tables["ConsumerDetailConfirmationEmloyment"].Columns.Add("FDateConfirmed", System.Type.GetType("System.DateTime"), "Iif(DateConfirmed = '', '1900-01-01T00:00:00', DateConfirmed)");
                this.lblCConfirmeddateD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDetailConfirmationEmloyment.FDateConfirmed", "{0:dd/MM/yyyy}") });
            }

            if (!(dsXML.Tables.Contains("ConsumerNameHistory")))
            {
                NameHistory.Visible = false;
            }
            else
            {
                dsXML.Tables["ConsumerNameHistory"].Columns.Add("FLastUpdatedDate", System.Type.GetType("System.DateTime"), "Iif(LastUpdatedDate='','1900-01-01T00:00:00',LastUpdatedDate)");
                dsXML.Tables["ConsumerNameHistory"].Columns.Add("FBirthDate", System.Type.GetType("System.DateTime"), "Iif(BirthDate='','1900-01-01T00:00:00',BirthDate)");
                this.lblDUpdatedDate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNameHistory.FLastUpdatedDate", "{0:dd/MM/yyyy}") });
                this.lblHDBirthDate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerNameHistory.FBirthDate", "{0:dd/MM/yyyy}") });
            }

            if (!(dsXML.Tables.Contains("ConsumerAddressHistory")))
            {
                AddressHistory.Visible = false;
            }
            else
            {
                dsXML.Tables["ConsumerAddressHistory"].Columns.Add("FLastUpdatedDate", System.Type.GetType("System.DateTime"), "Iif(LastUpdatedDate='','1900-01-01T00:00:00',LastUpdatedDate)");
                this.lblDAddUpdatedate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerAddressHistory.FLastUpdatedDate", "{0:dd/MM/yyyy}") });
            }

            if (!(dsXML.Tables.Contains("ConsumerTelephoneHistory")))
            {
                TelephoneHistory.Visible = false;
            }
            else
            {
                dsXML.Tables["ConsumerTelephoneHistory"].Columns.Add("FLastUpdatedDate", System.Type.GetType("System.DateTime"), "Iif(LastUpdatedDate='','1900-01-01T00:00:00',LastUpdatedDate)");
                this.lblDNoUpdatedate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerTelephoneHistory.FLastUpdatedDate", "{0:dd/MM/yyyy}") });
            }

            if (!(dsXML.Tables.Contains("ConsumerEmploymentHistory")))
            {
                EmploymentHistory.Visible = false;
            }
            else
            {
                dsXML.Tables["ConsumerEmploymentHistory"].Columns.Add("FLastUpdatedDate", System.Type.GetType("System.DateTime"), "Iif(LastUpdatedDate='','1900-01-01T00:00:00',LastUpdatedDate)");
                this.lblDEmpUpdatedate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerEmploymentHistory.FLastUpdatedDate", "{0:dd/MM/yyyy}") });
            }
            
            if (!(dsXML.Tables.Contains("CounsumerAccountStatus")))
            {
                AccountStatus.Visible = false;
            }
            else
            {
                dsXML.Tables["CounsumerAccountStatus"].Columns.Add("FDateAccountOpened", System.Type.GetType("System.DateTime"), "Iif(AccountOpenedDate='','1900-01-01T00:00:00',AccountOpenedDate)");
                this.lblAccOpenDateD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "CounsumerAccountStatus.FDateAccountOpened", "{0:dd/MM/yyyy}") });
            }
            if ((!(dsXML.Tables.Contains("Consumer24MonthlyPaymentHeader"))) || (!(dsXML.Tables.Contains("Consumer24MonthlyPaymentHeader"))))
            {
                MonthlyPaymentHeader.Visible = false;
                MonthlyPayment.Visible = false;
                Definition.Visible = false;
            }
            if (!(dsXML.Tables.Contains("ConsumerAdverseInfo")))
            {
                AdverseInfo.Visible = false;
            }
            else
            {
                dsXML.Tables["ConsumerAdverseInfo"].Columns.Add("FActionDate", System.Type.GetType("System.DateTime"), "Iif(ActionDate='','1900-01-01T00:00:00',ActionDate)");
                this.lblDActionDate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerAdverseInfo.FActionDate", "{0:dd/MM/yyyy}") });
            }

            if (!(dsXML.Tables.Contains("ConsumerJudgement")))
            {
                Judgements.Visible = false;
            }
            else
            {
                dsXML.Tables["ConsumerJudgement"].Columns.Add("FCaseFilingDate", System.Type.GetType("System.DateTime"), "Iif(CaseFilingDate='','1900-01-01T00:00:00',CaseFilingDate)");
                this.lblJDIssueDate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerJudgement.FCaseFilingDate", "{0:dd/MM/yyyy}") });
            }

            if (!(dsXML.Tables.Contains("ConsumerAdminOrder")))
            {
                AdminOrders.Visible = false;
            }
            else
            {
                dsXML.Tables["ConsumerAdminOrder"].Columns.Add("FCaseFilingDate", System.Type.GetType("System.DateTime"), "Iif(CaseFilingDate='','1900-01-01T00:00:00',CaseFilingDate)");
                dsXML.Tables["ConsumerAdminOrder"].Columns.Add("FLastUpdatedDate", System.Type.GetType("System.DateTime"), "Iif(LastUpdatedDate='','1900-01-01T00:00:00',LastUpdatedDate)");
                this.lblAODIssuedate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerAdminOrder.FCaseFilingDate", "{0:dd/MM/yyyy}") });
                this.lblAODLoadeddate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerAdminOrder.FLastUpdatedDate", "{0:dd/MM/yyyy}") });
            }

            if (!(dsXML.Tables.Contains("ConsumerSequestration")))
            {
                Sequestration.Visible = false;
            }
            else
            {
                dsXML.Tables["ConsumerSequestration"].Columns.Add("FCaseFilingDate", System.Type.GetType("System.DateTime"), "Iif(CaseFilingDate='','1900-01-01T00:00:00',CaseFilingDate)");
                dsXML.Tables["ConsumerSequestration"].Columns.Add("FLastUpdatedDate", System.Type.GetType("System.DateTime"), "Iif(LastUpdatedDate='','1900-01-01T00:00:00',LastUpdatedDate)");
                this.lblSEQDIssuedate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerSequestration.FCaseFilingDate", "{0:dd/MM/yyyy}") });
                this.lblSEQDLoadedDate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerSequestration.FLastUpdatedDate", "{0:dd/MM/yyyy}") });
            }

            if (!(dsXML.Tables.Contains("ConsumerDebtReviewStatus")))
            {
                DebtReviewStatus.Visible = false;
            }
            else
            {
                dsXML.Tables["ConsumerDebtReviewStatus"].Columns.Add("FDebtReviewStatusDate", System.Type.GetType("System.DateTime"), "Iif(DebtReviewStatusDate='','1900-01-01T00:00:00',DebtReviewStatusDate)");
                this.lblDbtReviewDateD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtReviewStatus.FDebtReviewStatusDate", "{0:dd/MM/yyyy}") });
            }

            if (!(dsXML.Tables.Contains("ConsumerEnquiryHistory")))
            {
                EnquiryHistory.Visible = false;
            }
            else
            {
                dsXML.Tables["ConsumerEnquiryHistory"].Columns.Add("FEnquiryDate", System.Type.GetType("System.DateTime"), "Iif(EnquiryDate='','1900-01-01T00:00:00',EnquiryDate)");
                this.lblEnqDateD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerEnquiryHistory.FEnquiryDate", "{0:dd/MM/yyyy}") });
            }

            if (!(dsXML.Tables.Contains("ConsumerPropertyInformation")))
            {
                propertyInfo.Visible = false;
            }
            else
            {
                dsXML.Tables["ConsumerPropertyInformation"].Columns.Add("FPurchaseDate", System.Type.GetType("System.DateTime"), "Iif(PurchaseDate='','1900-01-01T00:00:00',PurchaseDate)");
                this.lblDPurchasedate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerPropertyInformation.FPurchaseDate", "{0:dd/MM/yyyy}") });
            }


            if (!(dsXML.Tables.Contains("ConsumerDirectorShipLink")))
            {
                DirectorshipLink.Visible = false;
            }
            else
            {
                dsXML.Tables["ConsumerDirectorShipLink"].Columns.Add("FAppointmentDate", System.Type.GetType("System.DateTime"), "Iif(AppointmentDate='','1900-01-01T00:00:00',AppointmentDate)");
                this.lblDAppDate.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDirectorShipLink.FAppointmentDate", "{0:dd/MM/yyyy}") });
            }

            if (!(dsXML.Tables.Contains("TelephoneLinkageHome")))
            {
                TelephoneLinkageHome.Visible = false;
            }
            if (!(dsXML.Tables.Contains("TelephoneLinkageWork")))
            {
                TelephoneLinkageWork.Visible = false;
            }
            if (!(dsXML.Tables.Contains("TelephoneLinkageCellular")))
            {
                TelephoneLinkageCellular.Visible = false;
            }
           

            this.DataSource = dsXML;
        }

        private void tblAccStatus_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToInt16(lblRetailG.Text) > 0)
            {
                lblRetailG.BackColor = Color.Green;
            }
            if (Convert.ToInt16(lblCreditCradG.Text) > 0)
            {
                lblCreditCradG.BackColor = Color.Green;
            }
            if (Convert.ToInt16(lblFurnAccG.Text) > 0)
            {
                lblFurnAccG.BackColor = Color.Green;
            }
            if (Convert.ToInt16(lblInsuranceG.Text) > 0)
            {
                lblInsuranceG.BackColor = Color.Green;
            }
            if (Convert.ToInt16(lblFinanceG.Text) > 0)
            {
                lblFinanceG.BackColor = Color.Green;
            }
            if (Convert.ToInt16(lblBankAccG.Text) > 0)
            {
                lblBankAccG.BackColor = Color.Green;
            }
            if (Convert.ToInt16(lblTelecommsG.Text) > 0)
            {
                lblTelecommsG.BackColor = Color.Green;
            }
            if (Convert.ToInt16(lblHomeLoanG.Text) > 0)
            {
                lblHomeLoanG.BackColor = Color.Green;
            }
            if (Convert.ToInt16(lblVehicleFinG.Text) > 0)
            {
                lblVehicleFinG.BackColor = Color.Green;
            }
            if (Convert.ToInt16(lblOtherAccG.Text) > 0)
            {
                lblOtherAccG.BackColor = Color.Green;
            }
            if (Convert.ToInt16(lblRetailB.Text) > 0)
            {
                lblRetailB.BackColor = Color.Red;
            }
            if (Convert.ToInt16(lblCreditCardB.Text) > 0)
            {
                lblCreditCardB.BackColor = Color.Red;
            }
            if (Convert.ToInt16(lblFurnAccB.Text) > 0)
            {
                lblFurnAccB.BackColor = Color.Red;
            }
            if (Convert.ToInt16(lblInsuranceB.Text) > 0)
            {
                lblInsuranceB.BackColor = Color.Red;
            }
            if (Convert.ToInt16(lblFinanceB.Text) > 0)
            {
                lblFinanceB.BackColor = Color.Red;
            }
            if (Convert.ToInt16(lblBankAccB.Text) > 0)
            {
                lblBankAccB.BackColor = Color.Red;
            }
            if (Convert.ToInt16(lblTelecommsB.Text) > 0)
            {
                lblTelecommsB.BackColor = Color.Red;
            }
            if (Convert.ToInt16(lblHomeLoanB.Text) > 0)
            {
                lblHomeLoanB.BackColor = Color.Red;
            }
            if (Convert.ToInt16(lblVehicleFinB.Text) > 0)
            {
                lblVehicleFinB.BackColor = Color.Red;
            }
            if (Convert.ToInt16(lblOtherAccB.Text) > 0)
            {
                lblOtherAccB.BackColor = Color.Red;
            }
        }

        private void tblDebtSummary_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToInt16(lblAccGoodvalue.Text) > 0)
            {
                lblAccGoodvalue.BackColor = Color.Green;
            }
            if (Convert.ToInt16(lblDelinquentAccValue.Text) > 0)
            {
                lblDelinquentAccValue.BackColor = Color.Red;
            }
        }

    }
}
