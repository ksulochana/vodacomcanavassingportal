﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.IO;
using System.Data;
using System.Xml;

namespace XDS.REPORTS
{
  public partial class MotorVehicleVerificationReport : DevExpress.XtraReports.UI.XtraReport
  {
    public MotorVehicleVerificationReport(string strXML)
    {

      //strXML = XmlFormatNodesCurrency(strXML, "Amt");
      //strXML = XmlFormatNodesCurrency(strXML, "Price");
      //strXML = XmlFormatNodesCurrency(strXML, "instalment");
      //strXML = XmlFormatNodesCurrency(strXML, "Income");
      //strXML = XmlFormatNodesCurrency(strXML, "Commitments");
      strXML = XmlFormatNodesCurrencyNoCent(strXML, "Amt");
      strXML = XmlFormatNodesCurrencyNoCent(strXML, "Price");
      strXML = XmlFormatNodesCurrencyNoCent(strXML, "Income");
      strXML = XmlFormatNodesCurrencyNoCent(strXML, "Commitments");
      strXML = XmlFormatNodesCurrencyNoCent(strXML, "instalment");
      strXML = XmlFormatNodesDate(strXML, "Date");
      strXML = XmlFormatNodesInt(strXML, "FinalScore");


      
      InitializeComponent();
      //read from flat file xml

      

     // string strXML = ReadFromTextfile();
      System.IO.StringReader Objsr = new System.IO.StringReader(strXML);
      DataSet dsXML = new DataSet(); 
      dsXML.ReadXml(Objsr);



      if (dsXML.Tables.Contains("SubscriberInputDetails"))
      {
      //  dsXML.Tables["SubscriberInputDetails"].Columns.Add("FEnquiryDate", System.Type.GetType("System.DateTime"), "Iif(EnquiryDate = '', '1900-01-01', EnquiryDate)");
      //  this.xrRichText1.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "SubscriberInputDetails.FEnquiryDate", "{0:dd/MM/yyyy - hh:mm:ss}") });
        //this.xrTableCell2.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "SubscriberInputDetails.FEnquiryDate", "{0:dd/MM/yyyy - hh:mm:ss}") });
        //this.xrTableCell10.DataBindings.AddRange(new XRBinding[] {new XRBinding("Test", dsXML, "SubscriberInputDetails.FConsumerName") });
      }


      if (dsXML.Tables.Contains("SAPInformation"))
      {
        dsXML.Tables["SAPInformation"].Columns.Add("FTest", System.Type.GetType("System.String"), "IIf(ResultFound = 'N', 'NO USABLE DATA PROVIDED', IIf(ResultCode = 'N', 'No', 'Yes'))");
        this.lblTest.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "SAPInformation.FTest") });
//        dsXML.Tables["CommercialBusinessInformation"].Columns.Add("FSIC", System.Type.GetType("System.String"), "Iif(SIC = '0 - Unkown Data', 'No Information Available',SIC)");
//        this.lblSICCodeD.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "CommercialBusinessInformation.FSIC") });
//        this.lblJudgCount.DataBindings.AddRange(new XRBinding[] { new XRBinding("Text", dsXML, "ConsumerDebtSummary.JudgementCount") });
      }

          this.DataSource = dsXML;
            dsXML.Dispose();

    }

    //private string XmlFormatNodesCurrency(string strXML, string nodetofind)
    //{
    //  XmlDocument xml = new XmlDocument();
    //  xml.LoadXml(strXML);  // suppose that str string contains "<Names>...</Names>"

    //  XmlNodeList xnList = xml.SelectNodes("//*[contains(name(),'" + nodetofind + "')]");
    //  foreach (XmlNode xn in xnList)
    //  {
    //    try
    //    {
    //      if (!String.IsNullOrEmpty(xn.FirstChild.Value))
    //      {
    //        double temp = Convert.ToDouble(xn.FirstChild.Value);
    //        xn.FirstChild.Value = temp.ToString("C", CultureInfo.GetCultureInfo("en-ZA")).Replace(",", ".");
    //      }
    //    }
    //    catch (Exception ex) { }
    //  }

    //  StringWriter sw = new StringWriter();
    //  XmlTextWriter tx = new XmlTextWriter(sw);
    //  xml.WriteTo(tx);

    //  strXML = sw.ToString();
    //  return strXML;

    //}

    private string XmlFormatNodesCurrencyNoCent(string strXML, string nodetofind)//, string formaat)
    {
      XmlDocument xml = new XmlDocument();
      xml.LoadXml(strXML);  // suppose that str string contains "<Names>...</Names>"

      XmlNodeList xnList = xml.SelectNodes("//*[contains(name(),'" + nodetofind + "')]");
      foreach (XmlNode xn in xnList)
      {
        try
        {
          if (!String.IsNullOrEmpty(xn.FirstChild.Value))
          {
            String Source = xn.FirstChild.Value;
            int Lengte = Source.LastIndexOf(".");
            if (Lengte < 0)
            {
              Lengte = Source.Length;
            }
            String pretemp = Source.Substring(0, Lengte);
            int lengte2 = pretemp.Length;
            Int32 temp = Convert.ToInt32(pretemp);

            switch (lengte2)
            {
              case 1:
                xn.FirstChild.Value = String.Format("{0,0:R 0}", temp);
                break;

              case 2:
                xn.FirstChild.Value = String.Format("{0,0:R 00}", temp);
                break;

              case 3:
                xn.FirstChild.Value = String.Format("{0,0:R 000}", temp);
                break;

              case 4:
                xn.FirstChild.Value = String.Format("{0,0:R 0 000}", temp);
                break;

              case 5:
                xn.FirstChild.Value = String.Format("{0,0:R 00 000}", temp);
                break;

              case 6:
                xn.FirstChild.Value = String.Format("{0,0:R 000 000}", temp);
                break;

              case 7:
                xn.FirstChild.Value = String.Format("{0,0:R 0 000 000}", temp);
                break;

              case 8:
                xn.FirstChild.Value = String.Format("{0,0:R 00 000 000}", temp);
                break;

              case 9:
                xn.FirstChild.Value = String.Format("{0,0:R 000 000 000}", temp);
                break;
            }

          }
        }
        catch (Exception ex) { }
      }


      StringWriter sw = new StringWriter();
      XmlTextWriter tx = new XmlTextWriter(sw);
      xml.WriteTo(tx);

      strXML = sw.ToString();
      return strXML;
    }

    private string XmlFormatNodesDate(string strXML, string nodetofind)
    {
      XmlDocument xml = new XmlDocument();
      xml.LoadXml(strXML);  // suppose that str string contains "<Names>...</Names>"

      XmlNodeList xnList = xml.SelectNodes("//*[contains(name(),'" + nodetofind + "')]");
      foreach (XmlNode xn in xnList)
      {
        try
        {
          if (!String.IsNullOrEmpty(xn.FirstChild.Value))
          {
            if (!(xn.Name == "EnquiryDate") && !(xn.ParentNode.Name == "SubscriberInputDetails"))
            {
              xn.FirstChild.Value = xn.FirstChild.Value.Replace("-", "/");
            }
            else
            {
              string Source;
              string Datum;
              string Tyd;
              int Begin;
              int Lengte;
              Source = xn.FirstChild.Value.Replace("-", "/");
              Datum = Source.Substring(0, 10);
              Begin = Source.LastIndexOf("T") + 1;
              Lengte = Source.LastIndexOf(".") - Begin;
              Tyd = Source.Substring(Begin, Lengte);

              xn.FirstChild.Value = Datum + " " + Tyd;
            }
          }
          else
          {

            xn.FirstChild.Value = xn.FirstChild.Value.Replace("-", "/");
          }
        }
        catch (Exception ex) { }
      }

      StringWriter sw = new StringWriter();
      XmlTextWriter tx = new XmlTextWriter(sw);
      xml.WriteTo(tx);
      strXML = sw.ToString();
      return strXML;
    }

    private string XmlFormatNodesInt(string strXML, string nodetofind)
    {
      XmlDocument xml = new XmlDocument();
      xml.LoadXml(strXML);  // suppose that str string contains "<Names>...</Names>"

      XmlNodeList xnList = xml.SelectNodes("//*[contains(name(),'" + nodetofind + "')]");
      foreach (XmlNode xn in xnList)
      {
        try
        {
          if (!String.IsNullOrEmpty(xn.FirstChild.Value))
          {
            int Lengte = xn.FirstChild.Value.LastIndexOf(".");

            if (!(Convert.ToString(Lengte) == "0"))
            {
              xn.FirstChild.Value = xn.FirstChild.Value.Substring(0, Lengte);
            }
            else
            {
              xn.FirstChild.Value = xn.FirstChild.Value;
            }
          }
        }
        catch (Exception ex) { }
      }
      StringWriter sw = new StringWriter();
      XmlTextWriter tx = new XmlTextWriter(sw);
      xml.WriteTo(tx);

      strXML = sw.ToString();
      return strXML;
    }
            
  

  }


}