﻿namespace XDS.REPORTS
{
  partial class AuthenticationReport
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AuthenticationReport));
      this.Detail = new DevExpress.XtraReports.UI.DetailBand();
      this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
      this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
      this.sqlDataSource1 = new DevExpress.DataAccess.Sql.SqlDataSource();
      this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
      this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
      this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
      this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
      this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
      this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
      this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
      this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
      this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
      this.xrLabel95 = new DevExpress.XtraReports.UI.XRLabel();
      this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
      this.SumamryBlockA = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail12 = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow65 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
      // 
      // Detail
      // 
      resources.ApplyResources(this.Detail, "Detail");
      this.Detail.Name = "Detail";
      this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      this.Detail.StylePriority.UseBorderColor = false;
      this.Detail.StylePriority.UseBorders = false;
      this.Detail.StylePriority.UseBorderWidth = false;
      // 
      // TopMargin
      // 
      resources.ApplyResources(this.TopMargin, "TopMargin");
      this.TopMargin.Name = "TopMargin";
      this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      // 
      // BottomMargin
      // 
      resources.ApplyResources(this.BottomMargin, "BottomMargin");
      this.BottomMargin.Name = "BottomMargin";
      this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      // 
      // sqlDataSource1
      // 
      this.sqlDataSource1.Name = "sqlDataSource1";
      // 
      // ReportHeader
      // 
      this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine2,
            this.xrLabel21,
            this.xrLabel22,
            this.xrPictureBox1});
      resources.ApplyResources(this.ReportHeader, "ReportHeader");
      this.ReportHeader.Name = "ReportHeader";
      this.ReportHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      // 
      // xrLine2
      // 
      resources.ApplyResources(this.xrLine2, "xrLine2");
      this.xrLine2.LineWidth = 5;
      this.xrLine2.Name = "xrLine2";
      this.xrLine2.StylePriority.UseForeColor = false;
      // 
      // xrLabel21
      // 
      resources.ApplyResources(this.xrLabel21, "xrLabel21");
      this.xrLabel21.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrLabel21.Name = "xrLabel21";
      this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
      this.xrLabel21.StylePriority.UseBackColor = false;
      this.xrLabel21.StylePriority.UseBorderColor = false;
      this.xrLabel21.StylePriority.UseBorders = false;
      this.xrLabel21.StylePriority.UseFont = false;
      this.xrLabel21.StylePriority.UseForeColor = false;
      this.xrLabel21.StylePriority.UseTextAlignment = false;
      // 
      // xrLabel22
      // 
      resources.ApplyResources(this.xrLabel22, "xrLabel22");
      this.xrLabel22.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrLabel22.Multiline = true;
      this.xrLabel22.Name = "xrLabel22";
      this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrLabel22.StylePriority.UseBackColor = false;
      this.xrLabel22.StylePriority.UseBorderColor = false;
      this.xrLabel22.StylePriority.UseBorders = false;
      this.xrLabel22.StylePriority.UseFont = false;
      this.xrLabel22.StylePriority.UseForeColor = false;
      this.xrLabel22.StylePriority.UsePadding = false;
      this.xrLabel22.StylePriority.UseTextAlignment = false;
      // 
      // xrPictureBox1
      // 
      resources.ApplyResources(this.xrPictureBox1, "xrPictureBox1");
      this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
      this.xrPictureBox1.Name = "xrPictureBox1";
      this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
      this.xrPictureBox1.StylePriority.UsePadding = false;
      // 
      // PageFooter
      // 
      this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel1});
      resources.ApplyResources(this.PageFooter, "PageFooter");
      this.PageFooter.Name = "PageFooter";
      // 
      // xrPanel1
      // 
      resources.ApplyResources(this.xrPanel1, "xrPanel1");
      this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2,
            this.xrLabel16,
            this.xrLabel95,
            this.xrPageInfo1});
      this.xrPanel1.Name = "xrPanel1";
      this.xrPanel1.StylePriority.UseBackColor = false;
      // 
      // xrPageInfo2
      // 
      resources.ApplyResources(this.xrPageInfo2, "xrPageInfo2");
      this.xrPageInfo2.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrPageInfo2.LockedInUserDesigner = true;
      this.xrPageInfo2.Name = "xrPageInfo2";
      this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
      this.xrPageInfo2.StylePriority.UseBackColor = false;
      this.xrPageInfo2.StylePriority.UseBorderColor = false;
      this.xrPageInfo2.StylePriority.UseBorders = false;
      this.xrPageInfo2.StylePriority.UseFont = false;
      this.xrPageInfo2.StylePriority.UseForeColor = false;
      this.xrPageInfo2.StylePriority.UsePadding = false;
      this.xrPageInfo2.StylePriority.UseTextAlignment = false;
      // 
      // xrLabel16
      // 
      resources.ApplyResources(this.xrLabel16, "xrLabel16");
      this.xrLabel16.LockedInUserDesigner = true;
      this.xrLabel16.Name = "xrLabel16";
      this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
      this.xrLabel16.StylePriority.UseBackColor = false;
      this.xrLabel16.StylePriority.UseBorderColor = false;
      this.xrLabel16.StylePriority.UseFont = false;
      this.xrLabel16.StylePriority.UseForeColor = false;
      this.xrLabel16.StylePriority.UseTextAlignment = false;
      // 
      // xrLabel95
      // 
      resources.ApplyResources(this.xrLabel95, "xrLabel95");
      this.xrLabel95.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrLabel95.LockedInUserDesigner = true;
      this.xrLabel95.Name = "xrLabel95";
      this.xrLabel95.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
      this.xrLabel95.StylePriority.UseBackColor = false;
      this.xrLabel95.StylePriority.UseBorderColor = false;
      this.xrLabel95.StylePriority.UseBorders = false;
      this.xrLabel95.StylePriority.UseFont = false;
      this.xrLabel95.StylePriority.UseForeColor = false;
      this.xrLabel95.StylePriority.UseTextAlignment = false;
      // 
      // xrPageInfo1
      // 
      resources.ApplyResources(this.xrPageInfo1, "xrPageInfo1");
      this.xrPageInfo1.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrPageInfo1.LockedInUserDesigner = true;
      this.xrPageInfo1.Name = "xrPageInfo1";
      this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
      this.xrPageInfo1.StylePriority.UseBackColor = false;
      this.xrPageInfo1.StylePriority.UseBorderColor = false;
      this.xrPageInfo1.StylePriority.UseBorders = false;
      this.xrPageInfo1.StylePriority.UseFont = false;
      this.xrPageInfo1.StylePriority.UseForeColor = false;
      this.xrPageInfo1.StylePriority.UseTextAlignment = false;
      // 
      // SumamryBlockA
      // 
      this.SumamryBlockA.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail12});
      resources.ApplyResources(this.SumamryBlockA, "SumamryBlockA");
      this.SumamryBlockA.Level = 0;
      this.SumamryBlockA.Name = "SumamryBlockA";
      // 
      // Detail12
      // 
      resources.ApplyResources(this.Detail12, "Detail12");
      this.Detail12.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1,
            this.xrTable10});
      this.Detail12.Name = "Detail12";
      this.Detail12.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      this.Detail12.StylePriority.UseBorderColor = false;
      this.Detail12.StylePriority.UsePadding = false;
      // 
      // xrTable1
      // 
      resources.ApplyResources(this.xrTable1, "xrTable1");
      this.xrTable1.KeepTogether = true;
      this.xrTable1.Name = "xrTable1";
      this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow8});
      this.xrTable1.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow3
      // 
      this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell16,
            this.xrTableCell17});
      resources.ApplyResources(this.xrTableRow3, "xrTableRow3");
      this.xrTableRow3.Name = "xrTableRow3";
      // 
      // xrTableCell16
      // 
      resources.ApplyResources(this.xrTableCell16, "xrTableCell16");
      this.xrTableCell16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell16.Name = "xrTableCell16";
      this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell16.StylePriority.UseBackColor = false;
      this.xrTableCell16.StylePriority.UseBorderColor = false;
      this.xrTableCell16.StylePriority.UseBorders = false;
      this.xrTableCell16.StylePriority.UseFont = false;
      this.xrTableCell16.StylePriority.UseForeColor = false;
      this.xrTableCell16.StylePriority.UsePadding = false;
      this.xrTableCell16.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell17
      // 
      resources.ApplyResources(this.xrTableCell17, "xrTableCell17");
      this.xrTableCell17.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell17.Name = "xrTableCell17";
      this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell17.StylePriority.UseBackColor = false;
      this.xrTableCell17.StylePriority.UseBorderColor = false;
      this.xrTableCell17.StylePriority.UseBorders = false;
      this.xrTableCell17.StylePriority.UseFont = false;
      this.xrTableCell17.StylePriority.UseForeColor = false;
      this.xrTableCell17.StylePriority.UsePadding = false;
      this.xrTableCell17.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow4
      // 
      this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell19,
            this.xrTableCell20});
      resources.ApplyResources(this.xrTableRow4, "xrTableRow4");
      this.xrTableRow4.Name = "xrTableRow4";
      // 
      // xrTableCell19
      // 
      resources.ApplyResources(this.xrTableCell19, "xrTableCell19");
      this.xrTableCell19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell19.Name = "xrTableCell19";
      this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell19.StylePriority.UseBackColor = false;
      this.xrTableCell19.StylePriority.UseBorderColor = false;
      this.xrTableCell19.StylePriority.UseBorders = false;
      this.xrTableCell19.StylePriority.UseFont = false;
      this.xrTableCell19.StylePriority.UseForeColor = false;
      this.xrTableCell19.StylePriority.UsePadding = false;
      this.xrTableCell19.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell20
      // 
      resources.ApplyResources(this.xrTableCell20, "xrTableCell20");
      this.xrTableCell20.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell20.Name = "xrTableCell20";
      this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell20.StylePriority.UseBackColor = false;
      this.xrTableCell20.StylePriority.UseBorderColor = false;
      this.xrTableCell20.StylePriority.UseBorders = false;
      this.xrTableCell20.StylePriority.UseFont = false;
      this.xrTableCell20.StylePriority.UseForeColor = false;
      this.xrTableCell20.StylePriority.UsePadding = false;
      this.xrTableCell20.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow5
      // 
      this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell22,
            this.xrTableCell23});
      resources.ApplyResources(this.xrTableRow5, "xrTableRow5");
      this.xrTableRow5.Name = "xrTableRow5";
      // 
      // xrTableCell22
      // 
      resources.ApplyResources(this.xrTableCell22, "xrTableCell22");
      this.xrTableCell22.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell22.Name = "xrTableCell22";
      this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell22.StylePriority.UseBackColor = false;
      this.xrTableCell22.StylePriority.UseBorderColor = false;
      this.xrTableCell22.StylePriority.UseBorders = false;
      this.xrTableCell22.StylePriority.UseFont = false;
      this.xrTableCell22.StylePriority.UseForeColor = false;
      this.xrTableCell22.StylePriority.UsePadding = false;
      this.xrTableCell22.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell23
      // 
      resources.ApplyResources(this.xrTableCell23, "xrTableCell23");
      this.xrTableCell23.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell23.Name = "xrTableCell23";
      this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell23.StylePriority.UseBackColor = false;
      this.xrTableCell23.StylePriority.UseBorderColor = false;
      this.xrTableCell23.StylePriority.UseBorders = false;
      this.xrTableCell23.StylePriority.UseFont = false;
      this.xrTableCell23.StylePriority.UseForeColor = false;
      this.xrTableCell23.StylePriority.UsePadding = false;
      this.xrTableCell23.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow6
      // 
      this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.xrTableCell26});
      resources.ApplyResources(this.xrTableRow6, "xrTableRow6");
      this.xrTableRow6.Name = "xrTableRow6";
      // 
      // xrTableCell25
      // 
      resources.ApplyResources(this.xrTableCell25, "xrTableCell25");
      this.xrTableCell25.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell25.Name = "xrTableCell25";
      this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell25.StylePriority.UseBackColor = false;
      this.xrTableCell25.StylePriority.UseBorderColor = false;
      this.xrTableCell25.StylePriority.UseBorders = false;
      this.xrTableCell25.StylePriority.UseFont = false;
      this.xrTableCell25.StylePriority.UseForeColor = false;
      this.xrTableCell25.StylePriority.UsePadding = false;
      this.xrTableCell25.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell26
      // 
      resources.ApplyResources(this.xrTableCell26, "xrTableCell26");
      this.xrTableCell26.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell26.Name = "xrTableCell26";
      this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell26.StylePriority.UseBackColor = false;
      this.xrTableCell26.StylePriority.UseBorderColor = false;
      this.xrTableCell26.StylePriority.UseBorders = false;
      this.xrTableCell26.StylePriority.UseFont = false;
      this.xrTableCell26.StylePriority.UseForeColor = false;
      this.xrTableCell26.StylePriority.UsePadding = false;
      this.xrTableCell26.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow7
      // 
      this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell28,
            this.xrTableCell29});
      resources.ApplyResources(this.xrTableRow7, "xrTableRow7");
      this.xrTableRow7.Name = "xrTableRow7";
      // 
      // xrTableCell28
      // 
      resources.ApplyResources(this.xrTableCell28, "xrTableCell28");
      this.xrTableCell28.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell28.Name = "xrTableCell28";
      this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell28.StylePriority.UseBackColor = false;
      this.xrTableCell28.StylePriority.UseBorderColor = false;
      this.xrTableCell28.StylePriority.UseBorders = false;
      this.xrTableCell28.StylePriority.UseFont = false;
      this.xrTableCell28.StylePriority.UseForeColor = false;
      this.xrTableCell28.StylePriority.UsePadding = false;
      this.xrTableCell28.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell29
      // 
      resources.ApplyResources(this.xrTableCell29, "xrTableCell29");
      this.xrTableCell29.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell29.Name = "xrTableCell29";
      this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell29.StylePriority.UseBackColor = false;
      this.xrTableCell29.StylePriority.UseBorderColor = false;
      this.xrTableCell29.StylePriority.UseBorders = false;
      this.xrTableCell29.StylePriority.UseFont = false;
      this.xrTableCell29.StylePriority.UseForeColor = false;
      this.xrTableCell29.StylePriority.UsePadding = false;
      this.xrTableCell29.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow8
      // 
      this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell31,
            this.xrTableCell32});
      resources.ApplyResources(this.xrTableRow8, "xrTableRow8");
      this.xrTableRow8.Name = "xrTableRow8";
      // 
      // xrTableCell31
      // 
      resources.ApplyResources(this.xrTableCell31, "xrTableCell31");
      this.xrTableCell31.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell31.Name = "xrTableCell31";
      this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell31.StylePriority.UseBackColor = false;
      this.xrTableCell31.StylePriority.UseBorderColor = false;
      this.xrTableCell31.StylePriority.UseBorders = false;
      this.xrTableCell31.StylePriority.UseFont = false;
      this.xrTableCell31.StylePriority.UseForeColor = false;
      this.xrTableCell31.StylePriority.UsePadding = false;
      this.xrTableCell31.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell32
      // 
      resources.ApplyResources(this.xrTableCell32, "xrTableCell32");
      this.xrTableCell32.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell32.Name = "xrTableCell32";
      this.xrTableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell32.StylePriority.UseBackColor = false;
      this.xrTableCell32.StylePriority.UseBorderColor = false;
      this.xrTableCell32.StylePriority.UseBorders = false;
      this.xrTableCell32.StylePriority.UseFont = false;
      this.xrTableCell32.StylePriority.UseForeColor = false;
      this.xrTableCell32.StylePriority.UsePadding = false;
      this.xrTableCell32.StylePriority.UseTextAlignment = false;
      // 
      // xrTable10
      // 
      resources.ApplyResources(this.xrTable10, "xrTable10");
      this.xrTable10.KeepTogether = true;
      this.xrTable10.Name = "xrTable10";
      this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow43,
            this.xrTableRow65});
      this.xrTable10.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow43
      // 
      this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell141,
            this.xrTableCell122});
      resources.ApplyResources(this.xrTableRow43, "xrTableRow43");
      this.xrTableRow43.Name = "xrTableRow43";
      // 
      // xrTableCell141
      // 
      resources.ApplyResources(this.xrTableCell141, "xrTableCell141");
      this.xrTableCell141.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell141.CanGrow = false;
      this.xrTableCell141.Name = "xrTableCell141";
      this.xrTableCell141.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell141.StylePriority.UseBackColor = false;
      this.xrTableCell141.StylePriority.UseBorderColor = false;
      this.xrTableCell141.StylePriority.UseBorders = false;
      this.xrTableCell141.StylePriority.UseFont = false;
      this.xrTableCell141.StylePriority.UseForeColor = false;
      this.xrTableCell141.StylePriority.UsePadding = false;
      this.xrTableCell141.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell122
      // 
      resources.ApplyResources(this.xrTableCell122, "xrTableCell122");
      this.xrTableCell122.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell122.CanGrow = false;
      this.xrTableCell122.Name = "xrTableCell122";
      this.xrTableCell122.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell122.StylePriority.UseBackColor = false;
      this.xrTableCell122.StylePriority.UseBorderColor = false;
      this.xrTableCell122.StylePriority.UseBorderDashStyle = false;
      this.xrTableCell122.StylePriority.UseBorders = false;
      this.xrTableCell122.StylePriority.UseBorderWidth = false;
      this.xrTableCell122.StylePriority.UseFont = false;
      this.xrTableCell122.StylePriority.UseForeColor = false;
      this.xrTableCell122.StylePriority.UsePadding = false;
      this.xrTableCell122.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow65
      // 
      this.xrTableRow65.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell144});
      resources.ApplyResources(this.xrTableRow65, "xrTableRow65");
      this.xrTableRow65.Name = "xrTableRow65";
      this.xrTableRow65.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      this.xrTableRow65.StylePriority.UsePadding = false;
      // 
      // xrTableCell144
      // 
      resources.ApplyResources(this.xrTableCell144, "xrTableCell144");
      this.xrTableCell144.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTableCell144.CanGrow = false;
      this.xrTableCell144.Name = "xrTableCell144";
      this.xrTableCell144.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell144.StylePriority.UseBackColor = false;
      this.xrTableCell144.StylePriority.UseBorderColor = false;
      this.xrTableCell144.StylePriority.UseBorders = false;
      this.xrTableCell144.StylePriority.UseFont = false;
      this.xrTableCell144.StylePriority.UseForeColor = false;
      this.xrTableCell144.StylePriority.UsePadding = false;
      this.xrTableCell144.StylePriority.UseTextAlignment = false;
      // 
      // AuthenticationReport
      // 
      this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.PageFooter,
            this.SumamryBlockA});
      resources.ApplyResources(this, "$this");
      this.ScriptsSource = "\r\n";
      this.Version = "14.2";
      this.XmlDataPath = "D:\\Projects\\shbs\\DevExpress\\XML Datasource\\Commercial XML\\Authentication_1.XML";
      ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

    }

    #endregion

    private DevExpress.XtraReports.UI.DetailBand Detail;
    private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
    private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
    private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
    private DevExpress.XtraReports.UI.XRLabel xrLabel21;
    private DevExpress.XtraReports.UI.XRLabel xrLabel22;
    private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
    private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
    private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
    private DevExpress.XtraReports.UI.XRLabel xrLabel95;
    private DevExpress.XtraReports.UI.XRLabel xrLabel16;
    private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo2;
    private DevExpress.XtraReports.UI.XRPanel xrPanel1;
    private DevExpress.XtraReports.UI.DetailReportBand SumamryBlockA;
    private DevExpress.XtraReports.UI.DetailBand Detail12;
    private DevExpress.XtraReports.UI.XRTable xrTable10;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow43;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
    private DevExpress.DataAccess.Sql.SqlDataSource sqlDataSource1;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow65;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell144;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell141;
    private DevExpress.XtraReports.UI.XRLine xrLine2;
    private DevExpress.XtraReports.UI.XRTable xrTable1;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
  }
}
