﻿namespace XDS.REPORTS
{
  partial class LinkageReport_NewLook


  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LinkageReport_NewLook));
      this.Detail = new DevExpress.XtraReports.UI.DetailBand();
      this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
      this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
      this.SubscriberInputDetailsOther = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable19 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow48 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow49 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow50 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow51 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow52 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell175 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow53 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell178 = new DevExpress.XtraReports.UI.XRTableCell();
      this.SubscriberInutDetails = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell167 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell166 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell168 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell173 = new DevExpress.XtraReports.UI.XRTableCell();
      this.ConfirmedLinkage = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail3 = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable20 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow55 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell183 = new DevExpress.XtraReports.UI.XRTableCell();
      this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell179 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell182 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell181 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
      this.Detail4 = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow54 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow56 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell186 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow61 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell203 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell204 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell205 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell206 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow60 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell199 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell200 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell201 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell202 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow59 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell195 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell196 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell197 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell198 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow58 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell191 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell192 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell193 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell194 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow57 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell188 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell190 = new DevExpress.XtraReports.UI.XRTableCell();
      this.ConsumerOtherDetail = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail5 = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell135 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell139 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell147 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow62 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell207 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell208 = new DevExpress.XtraReports.UI.XRTableCell();
      this.Detail6 = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
      this.GroupHeader2 = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
      this.ConsumerOtherDetail_Address = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail7 = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
      this.GroupHeader3 = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow116 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell342 = new DevExpress.XtraReports.UI.XRTableCell();
      this.Detail8 = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow67 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
      this.GroupHeader4 = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow63 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
      this.ConsumerOtherDetail_Telephone = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail9 = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable12 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
      this.GroupHeader5 = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow66 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow118 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell344 = new DevExpress.XtraReports.UI.XRTableCell();
      this.Detail10 = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable15 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow74 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
      this.GroupHeader7 = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable13 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow68 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow69 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow70 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
      this.ConsumerOtherDetail_Employment = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail11 = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable16 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell151 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
      this.GroupHeader6 = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable14 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow71 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow72 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow73 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
      this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
      this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
      this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
      this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
      this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
      this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
      this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
      this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
      this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
      this.xrLabel95 = new DevExpress.XtraReports.UI.XRLabel();
      this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
      this.PersonalDetail_Summary = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail12 = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable17 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell156 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell157 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell158 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell161 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell163 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell165 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell209 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell210 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell211 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell212 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow75 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell213 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell214 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell215 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell216 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow76 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell217 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell218 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell219 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell220 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow77 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell221 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell222 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell223 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell224 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow78 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell225 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell226 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell227 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell228 = new DevExpress.XtraReports.UI.XRTableCell();
      this.GroupHeader8 = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable18 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow79 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell229 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow81 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell233 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell234 = new DevExpress.XtraReports.UI.XRTableCell();
      this.PersonalDetails_Details = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail13 = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable21 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow82 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell232 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell235 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell236 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell237 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow83 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell238 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell239 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell240 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell241 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow84 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell242 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell243 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell244 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell245 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow85 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell246 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell247 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell248 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell249 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow86 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell250 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell251 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell252 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell253 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow87 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell254 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell255 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell256 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell257 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow88 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell258 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell259 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell260 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell261 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow89 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell262 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell263 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell264 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell265 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow90 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell266 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell267 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell268 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell269 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow102 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell311 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell274 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow96 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell287 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell288 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell289 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell290 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow100 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell303 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell304 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell305 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell306 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow99 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell299 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell300 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell301 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell302 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow98 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell295 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell296 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell297 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell298 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow97 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell291 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell292 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell293 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell294 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow95 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell283 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell284 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell285 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell286 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow94 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell279 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell280 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell281 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell282 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow93 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell275 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell276 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell277 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell278 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow101 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell273 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell307 = new DevExpress.XtraReports.UI.XRTableCell();
      this.ReportHeader1 = new DevExpress.XtraReports.UI.ReportHeaderBand();
      this.xrTable22 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow91 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell270 = new DevExpress.XtraReports.UI.XRTableCell();
      this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail14 = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable24 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow106 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell318 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell319 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell320 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell321 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell322 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell323 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell324 = new DevExpress.XtraReports.UI.XRTableCell();
      this.GroupHeader11 = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable23 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow115 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell341 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow104 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell309 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow105 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell310 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell312 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell313 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell314 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell315 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell316 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell317 = new DevExpress.XtraReports.UI.XRTableCell();
      this.DetailReport1 = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail15 = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable26 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow110 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell330 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell331 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell332 = new DevExpress.XtraReports.UI.XRTableCell();
      this.GroupHeader10 = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable25 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow117 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell343 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow108 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell326 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow109 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell327 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell328 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell329 = new DevExpress.XtraReports.UI.XRTableCell();
      this.DetailReport2 = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail16 = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable28 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow114 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell338 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell339 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell340 = new DevExpress.XtraReports.UI.XRTableCell();
      this.GroupHeader9 = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable27 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow119 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell345 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow112 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell334 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow113 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell335 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell336 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell337 = new DevExpress.XtraReports.UI.XRTableCell();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable19)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable20)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable18)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable21)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable22)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable24)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable23)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable26)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable25)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable28)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable27)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
      // 
      // Detail
      // 
      this.Detail.Dpi = 254F;
      this.Detail.Expanded = false;
      this.Detail.HeightF = 230F;
      this.Detail.Name = "Detail";
      this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
      this.Detail.Visible = false;
      // 
      // TopMargin
      // 
      this.TopMargin.Dpi = 254F;
      this.TopMargin.HeightF = 41F;
      this.TopMargin.Name = "TopMargin";
      this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
      // 
      // BottomMargin
      // 
      this.BottomMargin.Dpi = 254F;
      this.BottomMargin.HeightF = 91F;
      this.BottomMargin.Name = "BottomMargin";
      this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
      // 
      // SubscriberInputDetailsOther
      // 
      this.SubscriberInputDetailsOther.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1});
      this.SubscriberInputDetailsOther.DataMember = "SubscriberInputDetailsOther";
      this.SubscriberInputDetailsOther.Dpi = 254F;
      this.SubscriberInputDetailsOther.Expanded = false;
      this.SubscriberInputDetailsOther.Level = 2;
      this.SubscriberInputDetailsOther.Name = "SubscriberInputDetailsOther";
      this.SubscriberInputDetailsOther.Visible = false;
      // 
      // Detail1
      // 
      this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable19});
      this.Detail1.Dpi = 254F;
      this.Detail1.HeightF = 450F;
      this.Detail1.Name = "Detail1";
      // 
      // xrTable19
      // 
      this.xrTable19.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTable19.Dpi = 254F;
      this.xrTable19.KeepTogether = true;
      this.xrTable19.LocationFloat = new DevExpress.Utils.PointFloat(0F, 25F);
      this.xrTable19.Name = "xrTable19";
      this.xrTable19.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTable19.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow47,
            this.xrTableRow48,
            this.xrTableRow49,
            this.xrTableRow50,
            this.xrTableRow51,
            this.xrTableRow52,
            this.xrTableRow53});
      this.xrTable19.SizeF = new System.Drawing.SizeF(2058F, 425F);
      this.xrTable19.StylePriority.UseBorders = false;
      this.xrTable19.StylePriority.UsePadding = false;
      // 
      // xrTableRow47
      // 
      this.xrTableRow47.Borders = DevExpress.XtraPrinting.BorderSide.Top;
      this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell23,
            this.xrTableCell24});
      this.xrTableRow47.Dpi = 254F;
      this.xrTableRow47.Name = "xrTableRow47";
      this.xrTableRow47.StylePriority.UseBorders = false;
      this.xrTableRow47.Weight = 1.5748031496062991D;
      // 
      // xrTableCell23
      // 
      this.xrTableCell23.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell23.BorderColor = System.Drawing.Color.Gainsboro;
      this.xrTableCell23.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell23.Dpi = 254F;
      this.xrTableCell23.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
      this.xrTableCell23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
      this.xrTableCell23.Name = "xrTableCell23";
      this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTableCell23.StylePriority.UseBackColor = false;
      this.xrTableCell23.StylePriority.UseBorderColor = false;
      this.xrTableCell23.StylePriority.UseBorders = false;
      this.xrTableCell23.StylePriority.UseFont = false;
      this.xrTableCell23.StylePriority.UseForeColor = false;
      this.xrTableCell23.StylePriority.UsePadding = false;
      this.xrTableCell23.StylePriority.UseTextAlignment = false;
      this.xrTableCell23.Text = "Enquiry Input Details - Party Of Interest";
      this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell23.Weight = 2D;
      // 
      // xrTableCell24
      // 
      this.xrTableCell24.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell24.BorderColor = System.Drawing.Color.Gainsboro;
      this.xrTableCell24.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell24.Dpi = 254F;
      this.xrTableCell24.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
      this.xrTableCell24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
      this.xrTableCell24.Name = "xrTableCell24";
      this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTableCell24.StylePriority.UseBackColor = false;
      this.xrTableCell24.StylePriority.UseBorderColor = false;
      this.xrTableCell24.StylePriority.UseBorders = false;
      this.xrTableCell24.StylePriority.UseFont = false;
      this.xrTableCell24.StylePriority.UseForeColor = false;
      this.xrTableCell24.StylePriority.UsePadding = false;
      this.xrTableCell24.StylePriority.UseTextAlignment = false;
      this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell24.Weight = 2D;
      // 
      // xrTableRow48
      // 
      this.xrTableRow48.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell27});
      this.xrTableRow48.Dpi = 254F;
      this.xrTableRow48.Name = "xrTableRow48";
      this.xrTableRow48.Weight = 0.39370078740157477D;
      // 
      // xrTableCell27
      // 
      this.xrTableCell27.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTableCell27.Dpi = 254F;
      this.xrTableCell27.Name = "xrTableCell27";
      this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      this.xrTableCell27.StylePriority.UseBorders = false;
      this.xrTableCell27.StylePriority.UsePadding = false;
      this.xrTableCell27.Text = "\r\n";
      this.xrTableCell27.Weight = 4D;
      // 
      // xrTableRow49
      // 
      this.xrTableRow49.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell28,
            this.xrTableCell31});
      this.xrTableRow49.Dpi = 254F;
      this.xrTableRow49.Name = "xrTableRow49";
      this.xrTableRow49.Weight = 0.94488188976377985D;
      // 
      // xrTableCell28
      // 
      this.xrTableCell28.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell28.BorderColor = System.Drawing.Color.White;
      this.xrTableCell28.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell28.Dpi = 254F;
      this.xrTableCell28.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell28.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell28.Name = "xrTableCell28";
      this.xrTableCell28.StylePriority.UseBackColor = false;
      this.xrTableCell28.StylePriority.UseBorderColor = false;
      this.xrTableCell28.StylePriority.UseBorders = false;
      this.xrTableCell28.StylePriority.UseFont = false;
      this.xrTableCell28.StylePriority.UseForeColor = false;
      this.xrTableCell28.StylePriority.UseTextAlignment = false;
      this.xrTableCell28.Text = "Enquiry Date:";
      this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell28.Weight = 0.66666666666666674D;
      // 
      // xrTableCell31
      // 
      this.xrTableCell31.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell31.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell31.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetailsOther.EnquiryDate", "{0:dd MMMM}")});
      this.xrTableCell31.Dpi = 254F;
      this.xrTableCell31.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell31.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell31.Name = "xrTableCell31";
      this.xrTableCell31.StylePriority.UseBorderColor = false;
      this.xrTableCell31.StylePriority.UseBorders = false;
      this.xrTableCell31.StylePriority.UseFont = false;
      this.xrTableCell31.StylePriority.UseForeColor = false;
      this.xrTableCell31.StylePriority.UseTextAlignment = false;
      this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell31.Weight = 3.333333333333333D;
      // 
      // xrTableRow50
      // 
      this.xrTableRow50.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell32,
            this.xrTableCell169});
      this.xrTableRow50.Dpi = 254F;
      this.xrTableRow50.Name = "xrTableRow50";
      this.xrTableRow50.Weight = 0.94488188976377929D;
      // 
      // xrTableCell32
      // 
      this.xrTableCell32.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell32.BorderColor = System.Drawing.Color.White;
      this.xrTableCell32.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell32.Dpi = 254F;
      this.xrTableCell32.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell32.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell32.Name = "xrTableCell32";
      this.xrTableCell32.StylePriority.UseBackColor = false;
      this.xrTableCell32.StylePriority.UseBorderColor = false;
      this.xrTableCell32.StylePriority.UseBorders = false;
      this.xrTableCell32.StylePriority.UseFont = false;
      this.xrTableCell32.StylePriority.UseForeColor = false;
      this.xrTableCell32.StylePriority.UseTextAlignment = false;
      this.xrTableCell32.Text = "Enquiry Type:";
      this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell32.Weight = 0.66666666666666674D;
      // 
      // xrTableCell169
      // 
      this.xrTableCell169.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell169.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell169.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetailsOther.EnquiryType")});
      this.xrTableCell169.Dpi = 254F;
      this.xrTableCell169.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell169.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell169.Name = "xrTableCell169";
      this.xrTableCell169.StylePriority.UseBorderColor = false;
      this.xrTableCell169.StylePriority.UseBorders = false;
      this.xrTableCell169.StylePriority.UseFont = false;
      this.xrTableCell169.StylePriority.UseForeColor = false;
      this.xrTableCell169.StylePriority.UseTextAlignment = false;
      this.xrTableCell169.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell169.Weight = 3.333333333333333D;
      // 
      // xrTableRow51
      // 
      this.xrTableRow51.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell170,
            this.xrTableCell174});
      this.xrTableRow51.Dpi = 254F;
      this.xrTableRow51.Name = "xrTableRow51";
      this.xrTableRow51.Weight = 0.9448818897637794D;
      // 
      // xrTableCell170
      // 
      this.xrTableCell170.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell170.BorderColor = System.Drawing.Color.White;
      this.xrTableCell170.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell170.Dpi = 254F;
      this.xrTableCell170.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell170.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell170.Name = "xrTableCell170";
      this.xrTableCell170.StylePriority.UseBackColor = false;
      this.xrTableCell170.StylePriority.UseBorderColor = false;
      this.xrTableCell170.StylePriority.UseBorders = false;
      this.xrTableCell170.StylePriority.UseFont = false;
      this.xrTableCell170.StylePriority.UseForeColor = false;
      this.xrTableCell170.StylePriority.UseTextAlignment = false;
      this.xrTableCell170.Text = "Subscriber Name:";
      this.xrTableCell170.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell170.Weight = 0.66666666666666674D;
      // 
      // xrTableCell174
      // 
      this.xrTableCell174.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell174.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell174.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetailsOther.SubscriberName")});
      this.xrTableCell174.Dpi = 254F;
      this.xrTableCell174.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell174.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell174.Name = "xrTableCell174";
      this.xrTableCell174.StylePriority.UseBorderColor = false;
      this.xrTableCell174.StylePriority.UseBorders = false;
      this.xrTableCell174.StylePriority.UseFont = false;
      this.xrTableCell174.StylePriority.UseForeColor = false;
      this.xrTableCell174.StylePriority.UseTextAlignment = false;
      this.xrTableCell174.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell174.Weight = 3.333333333333333D;
      // 
      // xrTableRow52
      // 
      this.xrTableRow52.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell175,
            this.xrTableCell176});
      this.xrTableRow52.Dpi = 254F;
      this.xrTableRow52.Name = "xrTableRow52";
      this.xrTableRow52.Weight = 0.9448818897637794D;
      // 
      // xrTableCell175
      // 
      this.xrTableCell175.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell175.BorderColor = System.Drawing.Color.White;
      this.xrTableCell175.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell175.Dpi = 254F;
      this.xrTableCell175.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell175.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell175.Name = "xrTableCell175";
      this.xrTableCell175.StylePriority.UseBackColor = false;
      this.xrTableCell175.StylePriority.UseBorderColor = false;
      this.xrTableCell175.StylePriority.UseBorders = false;
      this.xrTableCell175.StylePriority.UseFont = false;
      this.xrTableCell175.StylePriority.UseForeColor = false;
      this.xrTableCell175.StylePriority.UseTextAlignment = false;
      this.xrTableCell175.Text = "Subscriber User Name:";
      this.xrTableCell175.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell175.Weight = 0.66666666666666674D;
      // 
      // xrTableCell176
      // 
      this.xrTableCell176.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell176.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell176.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetailsOther.SubscriberUserName")});
      this.xrTableCell176.Dpi = 254F;
      this.xrTableCell176.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell176.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell176.Name = "xrTableCell176";
      this.xrTableCell176.StylePriority.UseBorderColor = false;
      this.xrTableCell176.StylePriority.UseBorders = false;
      this.xrTableCell176.StylePriority.UseFont = false;
      this.xrTableCell176.StylePriority.UseForeColor = false;
      this.xrTableCell176.StylePriority.UseTextAlignment = false;
      this.xrTableCell176.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell176.Weight = 3.333333333333333D;
      // 
      // xrTableRow53
      // 
      this.xrTableRow53.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell177,
            this.xrTableCell178});
      this.xrTableRow53.Dpi = 254F;
      this.xrTableRow53.Name = "xrTableRow53";
      this.xrTableRow53.Weight = 0.9448818897637794D;
      // 
      // xrTableCell177
      // 
      this.xrTableCell177.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell177.BorderColor = System.Drawing.Color.White;
      this.xrTableCell177.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell177.Dpi = 254F;
      this.xrTableCell177.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell177.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell177.Name = "xrTableCell177";
      this.xrTableCell177.StylePriority.UseBackColor = false;
      this.xrTableCell177.StylePriority.UseBorderColor = false;
      this.xrTableCell177.StylePriority.UseBorders = false;
      this.xrTableCell177.StylePriority.UseFont = false;
      this.xrTableCell177.StylePriority.UseForeColor = false;
      this.xrTableCell177.StylePriority.UseTextAlignment = false;
      this.xrTableCell177.Text = "Enquiry Input:";
      this.xrTableCell177.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell177.Weight = 0.66666666666666674D;
      // 
      // xrTableCell178
      // 
      this.xrTableCell178.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell178.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell178.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetailsOther.EnquiryInput")});
      this.xrTableCell178.Dpi = 254F;
      this.xrTableCell178.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell178.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell178.Name = "xrTableCell178";
      this.xrTableCell178.StylePriority.UseBorderColor = false;
      this.xrTableCell178.StylePriority.UseBorders = false;
      this.xrTableCell178.StylePriority.UseFont = false;
      this.xrTableCell178.StylePriority.UseForeColor = false;
      this.xrTableCell178.StylePriority.UseTextAlignment = false;
      this.xrTableCell178.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell178.Weight = 3.333333333333333D;
      // 
      // SubscriberInutDetails
      // 
      this.SubscriberInutDetails.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail2});
      this.SubscriberInutDetails.DataMember = "SubscriberInputDetails";
      this.SubscriberInutDetails.Dpi = 254F;
      this.SubscriberInutDetails.Level = 0;
      this.SubscriberInutDetails.Name = "SubscriberInutDetails";
      // 
      // Detail2
      // 
      this.Detail2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
      this.Detail2.Dpi = 254F;
      this.Detail2.HeightF = 425F;
      this.Detail2.Name = "Detail2";
      // 
      // xrTable2
      // 
      this.xrTable2.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTable2.Dpi = 254F;
      this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
      this.xrTable2.Name = "xrTable2";
      this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow8,
            this.xrTableRow45,
            this.xrTableRow46});
      this.xrTable2.SizeF = new System.Drawing.SizeF(2058F, 400F);
      this.xrTable2.StylePriority.UseBorders = false;
      this.xrTable2.StylePriority.UsePadding = false;
      // 
      // xrTableRow5
      // 
      this.xrTableRow5.Borders = DevExpress.XtraPrinting.BorderSide.Top;
      this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell167});
      this.xrTableRow5.Dpi = 254F;
      this.xrTableRow5.Name = "xrTableRow5";
      this.xrTableRow5.StylePriority.UseBorders = false;
      this.xrTableRow5.Weight = 1.5748031496062991D;
      // 
      // xrTableCell167
      // 
      this.xrTableCell167.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell167.BorderColor = System.Drawing.Color.Gainsboro;
      this.xrTableCell167.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell167.CanGrow = false;
      this.xrTableCell167.Dpi = 254F;
      this.xrTableCell167.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
      this.xrTableCell167.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
      this.xrTableCell167.Name = "xrTableCell167";
      this.xrTableCell167.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTableCell167.StylePriority.UseBackColor = false;
      this.xrTableCell167.StylePriority.UseBorderColor = false;
      this.xrTableCell167.StylePriority.UseBorders = false;
      this.xrTableCell167.StylePriority.UseFont = false;
      this.xrTableCell167.StylePriority.UseForeColor = false;
      this.xrTableCell167.StylePriority.UsePadding = false;
      this.xrTableCell167.StylePriority.UseTextAlignment = false;
      this.xrTableCell167.Text = "Enquiry Input Details";
      this.xrTableCell167.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell167.Weight = 4D;
      // 
      // xrTableRow6
      // 
      this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell21,
            this.xrTableCell22});
      this.xrTableRow6.Dpi = 254F;
      this.xrTableRow6.Name = "xrTableRow6";
      this.xrTableRow6.Weight = 0.94488188976377985D;
      // 
      // xrTableCell21
      // 
      this.xrTableCell21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell21.BorderColor = System.Drawing.Color.White;
      this.xrTableCell21.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell21.Dpi = 254F;
      this.xrTableCell21.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell21.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell21.Name = "xrTableCell21";
      this.xrTableCell21.StylePriority.UseBackColor = false;
      this.xrTableCell21.StylePriority.UseBorderColor = false;
      this.xrTableCell21.StylePriority.UseBorders = false;
      this.xrTableCell21.StylePriority.UseFont = false;
      this.xrTableCell21.StylePriority.UseForeColor = false;
      this.xrTableCell21.StylePriority.UseTextAlignment = false;
      this.xrTableCell21.Text = "Enquiry Date:";
      this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell21.Weight = 0.66666666666666674D;
      // 
      // xrTableCell22
      // 
      this.xrTableCell22.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell22.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell22.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.EnquiryDate", "{0:dd MMMM}")});
      this.xrTableCell22.Dpi = 254F;
      this.xrTableCell22.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell22.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell22.Name = "xrTableCell22";
      this.xrTableCell22.Scripts.OnBeforePrint = "xrTableCell22_BeforePrint";
      this.xrTableCell22.StylePriority.UseBorderColor = false;
      this.xrTableCell22.StylePriority.UseBorders = false;
      this.xrTableCell22.StylePriority.UseFont = false;
      this.xrTableCell22.StylePriority.UseForeColor = false;
      this.xrTableCell22.StylePriority.UseTextAlignment = false;
      this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell22.Weight = 3.333333333333333D;
      // 
      // xrTableRow7
      // 
      this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.xrTableCell26});
      this.xrTableRow7.Dpi = 254F;
      this.xrTableRow7.Name = "xrTableRow7";
      this.xrTableRow7.Weight = 0.94488188976377929D;
      // 
      // xrTableCell25
      // 
      this.xrTableCell25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell25.BorderColor = System.Drawing.Color.White;
      this.xrTableCell25.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell25.Dpi = 254F;
      this.xrTableCell25.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell25.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell25.Name = "xrTableCell25";
      this.xrTableCell25.StylePriority.UseBackColor = false;
      this.xrTableCell25.StylePriority.UseBorderColor = false;
      this.xrTableCell25.StylePriority.UseBorders = false;
      this.xrTableCell25.StylePriority.UseFont = false;
      this.xrTableCell25.StylePriority.UseForeColor = false;
      this.xrTableCell25.StylePriority.UseTextAlignment = false;
      this.xrTableCell25.Text = "Enquiry Type:";
      this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell25.Weight = 0.66666666666666674D;
      // 
      // xrTableCell26
      // 
      this.xrTableCell26.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell26.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell26.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.EnquiryType")});
      this.xrTableCell26.Dpi = 254F;
      this.xrTableCell26.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell26.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell26.Name = "xrTableCell26";
      this.xrTableCell26.StylePriority.UseBorderColor = false;
      this.xrTableCell26.StylePriority.UseBorders = false;
      this.xrTableCell26.StylePriority.UseFont = false;
      this.xrTableCell26.StylePriority.UseForeColor = false;
      this.xrTableCell26.StylePriority.UseTextAlignment = false;
      this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell26.Weight = 3.333333333333333D;
      // 
      // xrTableRow8
      // 
      this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell29,
            this.xrTableCell30});
      this.xrTableRow8.Dpi = 254F;
      this.xrTableRow8.Name = "xrTableRow8";
      this.xrTableRow8.Weight = 0.9448818897637794D;
      // 
      // xrTableCell29
      // 
      this.xrTableCell29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell29.BorderColor = System.Drawing.Color.White;
      this.xrTableCell29.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell29.Dpi = 254F;
      this.xrTableCell29.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell29.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell29.Name = "xrTableCell29";
      this.xrTableCell29.StylePriority.UseBackColor = false;
      this.xrTableCell29.StylePriority.UseBorderColor = false;
      this.xrTableCell29.StylePriority.UseBorders = false;
      this.xrTableCell29.StylePriority.UseFont = false;
      this.xrTableCell29.StylePriority.UseForeColor = false;
      this.xrTableCell29.StylePriority.UseTextAlignment = false;
      this.xrTableCell29.Text = "Subscriber Name:";
      this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell29.Weight = 0.66666666666666674D;
      // 
      // xrTableCell30
      // 
      this.xrTableCell30.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell30.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell30.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.SubscriberName")});
      this.xrTableCell30.Dpi = 254F;
      this.xrTableCell30.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell30.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell30.Name = "xrTableCell30";
      this.xrTableCell30.StylePriority.UseBorderColor = false;
      this.xrTableCell30.StylePriority.UseBorders = false;
      this.xrTableCell30.StylePriority.UseFont = false;
      this.xrTableCell30.StylePriority.UseForeColor = false;
      this.xrTableCell30.StylePriority.UseTextAlignment = false;
      this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell30.Weight = 3.333333333333333D;
      // 
      // xrTableRow45
      // 
      this.xrTableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell166,
            this.xrTableCell168});
      this.xrTableRow45.Dpi = 254F;
      this.xrTableRow45.Name = "xrTableRow45";
      this.xrTableRow45.Weight = 0.9448818897637794D;
      // 
      // xrTableCell166
      // 
      this.xrTableCell166.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell166.BorderColor = System.Drawing.Color.White;
      this.xrTableCell166.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell166.Dpi = 254F;
      this.xrTableCell166.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell166.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell166.Name = "xrTableCell166";
      this.xrTableCell166.StylePriority.UseBackColor = false;
      this.xrTableCell166.StylePriority.UseBorderColor = false;
      this.xrTableCell166.StylePriority.UseBorders = false;
      this.xrTableCell166.StylePriority.UseFont = false;
      this.xrTableCell166.StylePriority.UseForeColor = false;
      this.xrTableCell166.StylePriority.UseTextAlignment = false;
      this.xrTableCell166.Text = "Subscriber User Name:";
      this.xrTableCell166.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell166.Weight = 0.66666666666666674D;
      // 
      // xrTableCell168
      // 
      this.xrTableCell168.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell168.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell168.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.SubscriberUserName")});
      this.xrTableCell168.Dpi = 254F;
      this.xrTableCell168.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell168.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell168.Name = "xrTableCell168";
      this.xrTableCell168.StylePriority.UseBorderColor = false;
      this.xrTableCell168.StylePriority.UseBorders = false;
      this.xrTableCell168.StylePriority.UseFont = false;
      this.xrTableCell168.StylePriority.UseForeColor = false;
      this.xrTableCell168.StylePriority.UseTextAlignment = false;
      this.xrTableCell168.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell168.Weight = 3.333333333333333D;
      // 
      // xrTableRow46
      // 
      this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell172,
            this.xrTableCell3,
            this.xrTableCell4,
            this.xrTableCell173});
      this.xrTableRow46.Dpi = 254F;
      this.xrTableRow46.Name = "xrTableRow46";
      this.xrTableRow46.Weight = 0.9448818897637794D;
      // 
      // xrTableCell172
      // 
      this.xrTableCell172.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell172.BorderColor = System.Drawing.Color.White;
      this.xrTableCell172.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell172.Dpi = 254F;
      this.xrTableCell172.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell172.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell172.Name = "xrTableCell172";
      this.xrTableCell172.StylePriority.UseBackColor = false;
      this.xrTableCell172.StylePriority.UseBorderColor = false;
      this.xrTableCell172.StylePriority.UseBorders = false;
      this.xrTableCell172.StylePriority.UseFont = false;
      this.xrTableCell172.StylePriority.UseForeColor = false;
      this.xrTableCell172.StylePriority.UseTextAlignment = false;
      this.xrTableCell172.Text = "Enquiry Input:";
      this.xrTableCell172.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell172.Weight = 0.66666666666666674D;
      // 
      // xrTableCell3
      // 
      this.xrTableCell3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SubscriberInputDetails.EnquiryInput")});
      this.xrTableCell3.Dpi = 254F;
      this.xrTableCell3.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell3.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell3.Name = "xrTableCell3";
      this.xrTableCell3.StylePriority.UseBorderColor = false;
      this.xrTableCell3.StylePriority.UseBorders = false;
      this.xrTableCell3.StylePriority.UseFont = false;
      this.xrTableCell3.StylePriority.UseForeColor = false;
      this.xrTableCell3.StylePriority.UseTextAlignment = false;
      this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell3.Weight = 1.3333329774432094D;
      // 
      // xrTableCell4
      // 
      this.xrTableCell4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell4.BorderColor = System.Drawing.Color.White;
      this.xrTableCell4.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell4.Dpi = 254F;
      this.xrTableCell4.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell4.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell4.Name = "xrTableCell4";
      this.xrTableCell4.StylePriority.UseBackColor = false;
      this.xrTableCell4.StylePriority.UseBorderColor = false;
      this.xrTableCell4.StylePriority.UseBorders = false;
      this.xrTableCell4.StylePriority.UseFont = false;
      this.xrTableCell4.StylePriority.UseForeColor = false;
      this.xrTableCell4.StylePriority.UseTextAlignment = false;
      this.xrTableCell4.Text = "Party B ID:";
      this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell4.Weight = 0.66666666666666663D;
      // 
      // xrTableCell173
      // 
      this.xrTableCell173.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell173.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell173.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.IDNo")});
      this.xrTableCell173.Dpi = 254F;
      this.xrTableCell173.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell173.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell173.Name = "xrTableCell173";
      this.xrTableCell173.StylePriority.UseBorderColor = false;
      this.xrTableCell173.StylePriority.UseBorders = false;
      this.xrTableCell173.StylePriority.UseFont = false;
      this.xrTableCell173.StylePriority.UseForeColor = false;
      this.xrTableCell173.StylePriority.UseTextAlignment = false;
      this.xrTableCell173.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell173.Weight = 1.3333336892234571D;
      // 
      // ConfirmedLinkage
      // 
      this.ConfirmedLinkage.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail3,
            this.GroupHeader1});
      this.ConfirmedLinkage.DataMember = "ConfirmedLinkages.ConfirmedLinkages_ConfirmedLinkage";
      this.ConfirmedLinkage.Dpi = 254F;
      this.ConfirmedLinkage.Level = 3;
      this.ConfirmedLinkage.Name = "ConfirmedLinkage";
      // 
      // Detail3
      // 
      this.Detail3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable20});
      this.Detail3.Dpi = 254F;
      this.Detail3.HeightF = 85F;
      this.Detail3.Name = "Detail3";
      // 
      // xrTable20
      // 
      this.xrTable20.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTable20.Dpi = 254F;
      this.xrTable20.KeepTogether = true;
      this.xrTable20.LocationFloat = new DevExpress.Utils.PointFloat(0F, 25F);
      this.xrTable20.Name = "xrTable20";
      this.xrTable20.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTable20.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow55});
      this.xrTable20.SizeF = new System.Drawing.SizeF(2058F, 60F);
      this.xrTable20.StylePriority.UseBorders = false;
      this.xrTable20.StylePriority.UsePadding = false;
      // 
      // xrTableRow55
      // 
      this.xrTableRow55.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell38,
            this.xrTableCell39,
            this.xrTableCell40,
            this.xrTableCell41,
            this.xrTableCell42,
            this.xrTableCell183});
      this.xrTableRow55.Dpi = 254F;
      this.xrTableRow55.Name = "xrTableRow55";
      this.xrTableRow55.Weight = 0.94488188976377985D;
      // 
      // xrTableCell38
      // 
      this.xrTableCell38.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell38.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell38.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell38.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell38.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConfirmedLinkages.ConfirmedLinkages_ConfirmedLinkage.LinkageCategory")});
      this.xrTableCell38.Dpi = 254F;
      this.xrTableCell38.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell38.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell38.Name = "xrTableCell38";
      this.xrTableCell38.StylePriority.UseBackColor = false;
      this.xrTableCell38.StylePriority.UseBorderColor = false;
      this.xrTableCell38.StylePriority.UseBorderDashStyle = false;
      this.xrTableCell38.StylePriority.UseBorders = false;
      this.xrTableCell38.StylePriority.UseFont = false;
      this.xrTableCell38.StylePriority.UseForeColor = false;
      this.xrTableCell38.StylePriority.UseTextAlignment = false;
      this.xrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell38.Weight = 0.66666666666666674D;
      // 
      // xrTableCell39
      // 
      this.xrTableCell39.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell39.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell39.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell39.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell39.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConfirmedLinkages.ConfirmedLinkages_ConfirmedLinkage.ConsumerRecordDate")});
      this.xrTableCell39.Dpi = 254F;
      this.xrTableCell39.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell39.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell39.Name = "xrTableCell39";
      this.xrTableCell39.Scripts.OnBeforePrint = "xrTableCell39_BeforePrint";
      this.xrTableCell39.StylePriority.UseBackColor = false;
      this.xrTableCell39.StylePriority.UseBorderColor = false;
      this.xrTableCell39.StylePriority.UseBorderDashStyle = false;
      this.xrTableCell39.StylePriority.UseBorders = false;
      this.xrTableCell39.StylePriority.UseFont = false;
      this.xrTableCell39.StylePriority.UseForeColor = false;
      this.xrTableCell39.StylePriority.UseTextAlignment = false;
      this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell39.Weight = 0.66666666666666663D;
      // 
      // xrTableCell40
      // 
      this.xrTableCell40.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell40.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell40.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell40.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell40.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConfirmedLinkages.ConfirmedLinkages_ConfirmedLinkage.ConsumerRecordType")});
      this.xrTableCell40.Dpi = 254F;
      this.xrTableCell40.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell40.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell40.Name = "xrTableCell40";
      this.xrTableCell40.StylePriority.UseBackColor = false;
      this.xrTableCell40.StylePriority.UseBorderColor = false;
      this.xrTableCell40.StylePriority.UseBorderDashStyle = false;
      this.xrTableCell40.StylePriority.UseBorders = false;
      this.xrTableCell40.StylePriority.UseFont = false;
      this.xrTableCell40.StylePriority.UseForeColor = false;
      this.xrTableCell40.StylePriority.UseTextAlignment = false;
      this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell40.Weight = 0.66666666666666652D;
      // 
      // xrTableCell41
      // 
      this.xrTableCell41.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell41.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell41.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell41.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell41.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConfirmedLinkages.ConfirmedLinkages_ConfirmedLinkage.OtherRecordDate")});
      this.xrTableCell41.Dpi = 254F;
      this.xrTableCell41.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell41.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell41.Name = "xrTableCell41";
      this.xrTableCell41.Scripts.OnBeforePrint = "xrTableCell41_BeforePrint";
      this.xrTableCell41.StylePriority.UseBackColor = false;
      this.xrTableCell41.StylePriority.UseBorderColor = false;
      this.xrTableCell41.StylePriority.UseBorderDashStyle = false;
      this.xrTableCell41.StylePriority.UseBorders = false;
      this.xrTableCell41.StylePriority.UseFont = false;
      this.xrTableCell41.StylePriority.UseForeColor = false;
      this.xrTableCell41.StylePriority.UseTextAlignment = false;
      this.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell41.Weight = 0.66666666666666663D;
      // 
      // xrTableCell42
      // 
      this.xrTableCell42.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell42.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell42.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell42.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell42.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConfirmedLinkages.ConfirmedLinkages_ConfirmedLinkage.OtherRecordType")});
      this.xrTableCell42.Dpi = 254F;
      this.xrTableCell42.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell42.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell42.Name = "xrTableCell42";
      this.xrTableCell42.StylePriority.UseBackColor = false;
      this.xrTableCell42.StylePriority.UseBorderColor = false;
      this.xrTableCell42.StylePriority.UseBorderDashStyle = false;
      this.xrTableCell42.StylePriority.UseBorders = false;
      this.xrTableCell42.StylePriority.UseFont = false;
      this.xrTableCell42.StylePriority.UseForeColor = false;
      this.xrTableCell42.StylePriority.UseTextAlignment = false;
      this.xrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell42.Weight = 0.66666666666666663D;
      // 
      // xrTableCell183
      // 
      this.xrTableCell183.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell183.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell183.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell183.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell183.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConfirmedLinkages.ConfirmedLinkages_ConfirmedLinkage.MatchedData")});
      this.xrTableCell183.Dpi = 254F;
      this.xrTableCell183.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell183.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell183.Name = "xrTableCell183";
      this.xrTableCell183.StylePriority.UseBackColor = false;
      this.xrTableCell183.StylePriority.UseBorderColor = false;
      this.xrTableCell183.StylePriority.UseBorderDashStyle = false;
      this.xrTableCell183.StylePriority.UseBorders = false;
      this.xrTableCell183.StylePriority.UseFont = false;
      this.xrTableCell183.StylePriority.UseForeColor = false;
      this.xrTableCell183.StylePriority.UseTextAlignment = false;
      this.xrTableCell183.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell183.Weight = 0.66666666666666663D;
      // 
      // GroupHeader1
      // 
      this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
      this.GroupHeader1.Dpi = 254F;
      this.GroupHeader1.HeightF = 185F;
      this.GroupHeader1.Name = "GroupHeader1";
      // 
      // xrTable3
      // 
      this.xrTable3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTable3.Dpi = 254F;
      this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 25F);
      this.xrTable3.Name = "xrTable3";
      this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9,
            this.xrTableRow11});
      this.xrTable3.SizeF = new System.Drawing.SizeF(2058F, 160F);
      this.xrTable3.StylePriority.UseBorders = false;
      this.xrTable3.StylePriority.UsePadding = false;
      // 
      // xrTableRow9
      // 
      this.xrTableRow9.Borders = DevExpress.XtraPrinting.BorderSide.Top;
      this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell17});
      this.xrTableRow9.Dpi = 254F;
      this.xrTableRow9.Name = "xrTableRow9";
      this.xrTableRow9.StylePriority.UseBorders = false;
      this.xrTableRow9.Weight = 1.5748031496062991D;
      // 
      // xrTableCell17
      // 
      this.xrTableCell17.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell17.BorderColor = System.Drawing.Color.Gainsboro;
      this.xrTableCell17.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell17.CanGrow = false;
      this.xrTableCell17.Dpi = 254F;
      this.xrTableCell17.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
      this.xrTableCell17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
      this.xrTableCell17.Name = "xrTableCell17";
      this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTableCell17.StylePriority.UseBackColor = false;
      this.xrTableCell17.StylePriority.UseBorderColor = false;
      this.xrTableCell17.StylePriority.UseBorders = false;
      this.xrTableCell17.StylePriority.UseFont = false;
      this.xrTableCell17.StylePriority.UseForeColor = false;
      this.xrTableCell17.StylePriority.UsePadding = false;
      this.xrTableCell17.StylePriority.UseTextAlignment = false;
      this.xrTableCell17.Text = "Confirmed Linkages";
      this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell17.Weight = 4D;
      // 
      // xrTableRow11
      // 
      this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell33,
            this.xrTableCell180,
            this.xrTableCell179,
            this.xrTableCell182,
            this.xrTableCell181,
            this.xrTableCell34});
      this.xrTableRow11.Dpi = 254F;
      this.xrTableRow11.Name = "xrTableRow11";
      this.xrTableRow11.StylePriority.UseBackColor = false;
      this.xrTableRow11.Weight = 0.94488188976377985D;
      // 
      // xrTableCell33
      // 
      this.xrTableCell33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell33.BorderColor = System.Drawing.Color.Black;
      this.xrTableCell33.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell33.CanGrow = false;
      this.xrTableCell33.Dpi = 254F;
      this.xrTableCell33.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell33.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell33.Name = "xrTableCell33";
      this.xrTableCell33.StylePriority.UseBackColor = false;
      this.xrTableCell33.StylePriority.UseBorderColor = false;
      this.xrTableCell33.StylePriority.UseBorders = false;
      this.xrTableCell33.StylePriority.UseFont = false;
      this.xrTableCell33.StylePriority.UseForeColor = false;
      this.xrTableCell33.StylePriority.UseTextAlignment = false;
      this.xrTableCell33.Text = "Category";
      this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell33.Weight = 0.66666666666666674D;
      // 
      // xrTableCell180
      // 
      this.xrTableCell180.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell180.BorderColor = System.Drawing.Color.Black;
      this.xrTableCell180.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell180.CanGrow = false;
      this.xrTableCell180.Dpi = 254F;
      this.xrTableCell180.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell180.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell180.Name = "xrTableCell180";
      this.xrTableCell180.StylePriority.UseBackColor = false;
      this.xrTableCell180.StylePriority.UseBorderColor = false;
      this.xrTableCell180.StylePriority.UseBorders = false;
      this.xrTableCell180.StylePriority.UseFont = false;
      this.xrTableCell180.StylePriority.UseForeColor = false;
      this.xrTableCell180.StylePriority.UseTextAlignment = false;
      this.xrTableCell180.Text = "Date - Party A";
      this.xrTableCell180.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell180.Weight = 0.66666666666666663D;
      // 
      // xrTableCell179
      // 
      this.xrTableCell179.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell179.BorderColor = System.Drawing.Color.Black;
      this.xrTableCell179.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell179.CanGrow = false;
      this.xrTableCell179.Dpi = 254F;
      this.xrTableCell179.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell179.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell179.Name = "xrTableCell179";
      this.xrTableCell179.StylePriority.UseBackColor = false;
      this.xrTableCell179.StylePriority.UseBorderColor = false;
      this.xrTableCell179.StylePriority.UseBorders = false;
      this.xrTableCell179.StylePriority.UseFont = false;
      this.xrTableCell179.StylePriority.UseForeColor = false;
      this.xrTableCell179.StylePriority.UseTextAlignment = false;
      this.xrTableCell179.Text = "Type - Party A";
      this.xrTableCell179.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell179.Weight = 0.66666666666666652D;
      // 
      // xrTableCell182
      // 
      this.xrTableCell182.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell182.BorderColor = System.Drawing.Color.Black;
      this.xrTableCell182.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell182.CanGrow = false;
      this.xrTableCell182.Dpi = 254F;
      this.xrTableCell182.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell182.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell182.Name = "xrTableCell182";
      this.xrTableCell182.StylePriority.UseBackColor = false;
      this.xrTableCell182.StylePriority.UseBorderColor = false;
      this.xrTableCell182.StylePriority.UseBorders = false;
      this.xrTableCell182.StylePriority.UseFont = false;
      this.xrTableCell182.StylePriority.UseForeColor = false;
      this.xrTableCell182.StylePriority.UseTextAlignment = false;
      this.xrTableCell182.Text = "Date Party - B";
      this.xrTableCell182.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell182.Weight = 0.66666666666666663D;
      // 
      // xrTableCell181
      // 
      this.xrTableCell181.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell181.BorderColor = System.Drawing.Color.Black;
      this.xrTableCell181.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell181.CanGrow = false;
      this.xrTableCell181.Dpi = 254F;
      this.xrTableCell181.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell181.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell181.Name = "xrTableCell181";
      this.xrTableCell181.StylePriority.UseBackColor = false;
      this.xrTableCell181.StylePriority.UseBorderColor = false;
      this.xrTableCell181.StylePriority.UseBorders = false;
      this.xrTableCell181.StylePriority.UseFont = false;
      this.xrTableCell181.StylePriority.UseForeColor = false;
      this.xrTableCell181.StylePriority.UseTextAlignment = false;
      this.xrTableCell181.Text = "Type - Party B";
      this.xrTableCell181.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell181.Weight = 0.66666666666666663D;
      // 
      // xrTableCell34
      // 
      this.xrTableCell34.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell34.BorderColor = System.Drawing.Color.Black;
      this.xrTableCell34.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell34.CanGrow = false;
      this.xrTableCell34.Dpi = 254F;
      this.xrTableCell34.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell34.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell34.Name = "xrTableCell34";
      this.xrTableCell34.StylePriority.UseBackColor = false;
      this.xrTableCell34.StylePriority.UseBorderColor = false;
      this.xrTableCell34.StylePriority.UseBorders = false;
      this.xrTableCell34.StylePriority.UseFont = false;
      this.xrTableCell34.StylePriority.UseForeColor = false;
      this.xrTableCell34.StylePriority.UseTextAlignment = false;
      this.xrTableCell34.Text = "Match";
      this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell34.Weight = 0.66666666666666663D;
      // 
      // Detail4
      // 
      this.Detail4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
      this.Detail4.Dpi = 254F;
      this.Detail4.HeightF = 690F;
      this.Detail4.KeepTogether = true;
      this.Detail4.Name = "Detail4";
      // 
      // xrTable4
      // 
      this.xrTable4.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTable4.Dpi = 254F;
      this.xrTable4.KeepTogether = true;
      this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 25F);
      this.xrTable4.Name = "xrTable4";
      this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow12,
            this.xrTableRow15,
            this.xrTableRow16,
            this.xrTableRow17,
            this.xrTableRow54,
            this.xrTableRow56,
            this.xrTableRow61,
            this.xrTableRow60,
            this.xrTableRow59,
            this.xrTableRow58,
            this.xrTableRow57});
      this.xrTable4.SizeF = new System.Drawing.SizeF(2058F, 665F);
      this.xrTable4.StylePriority.UseBorders = false;
      this.xrTable4.StylePriority.UsePadding = false;
      // 
      // xrTableRow12
      // 
      this.xrTableRow12.Borders = DevExpress.XtraPrinting.BorderSide.Top;
      this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell35,
            this.xrTableCell36});
      this.xrTableRow12.Dpi = 254F;
      this.xrTableRow12.Name = "xrTableRow12";
      this.xrTableRow12.StylePriority.UseBorders = false;
      this.xrTableRow12.Weight = 1.5748031496062991D;
      // 
      // xrTableCell35
      // 
      this.xrTableCell35.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell35.BorderColor = System.Drawing.Color.Gainsboro;
      this.xrTableCell35.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell35.Dpi = 254F;
      this.xrTableCell35.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
      this.xrTableCell35.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
      this.xrTableCell35.Name = "xrTableCell35";
      this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTableCell35.StylePriority.UseBackColor = false;
      this.xrTableCell35.StylePriority.UseBorderColor = false;
      this.xrTableCell35.StylePriority.UseBorders = false;
      this.xrTableCell35.StylePriority.UseFont = false;
      this.xrTableCell35.StylePriority.UseForeColor = false;
      this.xrTableCell35.StylePriority.UsePadding = false;
      this.xrTableCell35.StylePriority.UseTextAlignment = false;
      this.xrTableCell35.Text = "Personal Details Summary";
      this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell35.Weight = 2D;
      // 
      // xrTableCell36
      // 
      this.xrTableCell36.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell36.BorderColor = System.Drawing.Color.Gainsboro;
      this.xrTableCell36.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell36.Dpi = 254F;
      this.xrTableCell36.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
      this.xrTableCell36.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
      this.xrTableCell36.Name = "xrTableCell36";
      this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTableCell36.StylePriority.UseBackColor = false;
      this.xrTableCell36.StylePriority.UseBorderColor = false;
      this.xrTableCell36.StylePriority.UseBorders = false;
      this.xrTableCell36.StylePriority.UseFont = false;
      this.xrTableCell36.StylePriority.UseForeColor = false;
      this.xrTableCell36.StylePriority.UsePadding = false;
      this.xrTableCell36.StylePriority.UseTextAlignment = false;
      this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell36.Weight = 2D;
      // 
      // xrTableRow15
      // 
      this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell37});
      this.xrTableRow15.Dpi = 254F;
      this.xrTableRow15.Name = "xrTableRow15";
      this.xrTableRow15.Weight = 0.39370078740157477D;
      // 
      // xrTableCell37
      // 
      this.xrTableCell37.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTableCell37.Dpi = 254F;
      this.xrTableCell37.Name = "xrTableCell37";
      this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      this.xrTableCell37.StylePriority.UseBorders = false;
      this.xrTableCell37.StylePriority.UsePadding = false;
      this.xrTableCell37.Text = "\r\n";
      this.xrTableCell37.Weight = 4D;
      // 
      // xrTableRow16
      // 
      this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell48,
            this.xrTableCell60,
            this.xrTableCell59,
            this.xrTableCell49});
      this.xrTableRow16.Dpi = 254F;
      this.xrTableRow16.Name = "xrTableRow16";
      this.xrTableRow16.Weight = 0.94488188976377985D;
      // 
      // xrTableCell48
      // 
      this.xrTableCell48.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell48.BorderColor = System.Drawing.Color.White;
      this.xrTableCell48.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell48.Dpi = 254F;
      this.xrTableCell48.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell48.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell48.Name = "xrTableCell48";
      this.xrTableCell48.StylePriority.UseBackColor = false;
      this.xrTableCell48.StylePriority.UseBorderColor = false;
      this.xrTableCell48.StylePriority.UseBorders = false;
      this.xrTableCell48.StylePriority.UseFont = false;
      this.xrTableCell48.StylePriority.UseForeColor = false;
      this.xrTableCell48.StylePriority.UseTextAlignment = false;
      this.xrTableCell48.Text = "Reference No:";
      this.xrTableCell48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell48.Weight = 0.66666666666666674D;
      // 
      // xrTableCell60
      // 
      this.xrTableCell60.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell60.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell60.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.ReferenceNo")});
      this.xrTableCell60.Dpi = 254F;
      this.xrTableCell60.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell60.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell60.Name = "xrTableCell60";
      this.xrTableCell60.StylePriority.UseBorderColor = false;
      this.xrTableCell60.StylePriority.UseBorders = false;
      this.xrTableCell60.StylePriority.UseFont = false;
      this.xrTableCell60.StylePriority.UseForeColor = false;
      this.xrTableCell60.StylePriority.UseTextAlignment = false;
      this.xrTableCell60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell60.Weight = 1.333333333333333D;
      // 
      // xrTableCell59
      // 
      this.xrTableCell59.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell59.BorderColor = System.Drawing.Color.White;
      this.xrTableCell59.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell59.Dpi = 254F;
      this.xrTableCell59.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell59.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell59.Name = "xrTableCell59";
      this.xrTableCell59.StylePriority.UseBackColor = false;
      this.xrTableCell59.StylePriority.UseBorderColor = false;
      this.xrTableCell59.StylePriority.UseBorders = false;
      this.xrTableCell59.StylePriority.UseFont = false;
      this.xrTableCell59.StylePriority.UseForeColor = false;
      this.xrTableCell59.StylePriority.UseTextAlignment = false;
      this.xrTableCell59.Text = "External Reference No:";
      this.xrTableCell59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell59.Weight = 0.66666666666666663D;
      // 
      // xrTableCell49
      // 
      this.xrTableCell49.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell49.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell49.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.ExternalReference")});
      this.xrTableCell49.Dpi = 254F;
      this.xrTableCell49.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell49.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell49.Name = "xrTableCell49";
      this.xrTableCell49.StylePriority.UseBorderColor = false;
      this.xrTableCell49.StylePriority.UseBorders = false;
      this.xrTableCell49.StylePriority.UseFont = false;
      this.xrTableCell49.StylePriority.UseForeColor = false;
      this.xrTableCell49.StylePriority.UseTextAlignment = false;
      this.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell49.Weight = 1.333333333333333D;
      // 
      // xrTableRow17
      // 
      this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell50,
            this.xrTableCell51,
            this.xrTableCell52,
            this.xrTableCell53});
      this.xrTableRow17.Dpi = 254F;
      this.xrTableRow17.Name = "xrTableRow17";
      this.xrTableRow17.Weight = 0.94488188976377985D;
      // 
      // xrTableCell50
      // 
      this.xrTableCell50.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell50.BorderColor = System.Drawing.Color.White;
      this.xrTableCell50.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell50.Dpi = 254F;
      this.xrTableCell50.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell50.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell50.Name = "xrTableCell50";
      this.xrTableCell50.StylePriority.UseBackColor = false;
      this.xrTableCell50.StylePriority.UseBorderColor = false;
      this.xrTableCell50.StylePriority.UseBorders = false;
      this.xrTableCell50.StylePriority.UseFont = false;
      this.xrTableCell50.StylePriority.UseForeColor = false;
      this.xrTableCell50.StylePriority.UseTextAlignment = false;
      this.xrTableCell50.Text = "ID No:";
      this.xrTableCell50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell50.Weight = 0.66666666666666674D;
      // 
      // xrTableCell51
      // 
      this.xrTableCell51.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell51.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell51.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.IDNo")});
      this.xrTableCell51.Dpi = 254F;
      this.xrTableCell51.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell51.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell51.Name = "xrTableCell51";
      this.xrTableCell51.StylePriority.UseBorderColor = false;
      this.xrTableCell51.StylePriority.UseBorders = false;
      this.xrTableCell51.StylePriority.UseFont = false;
      this.xrTableCell51.StylePriority.UseForeColor = false;
      this.xrTableCell51.StylePriority.UseTextAlignment = false;
      this.xrTableCell51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell51.Weight = 1.333333333333333D;
      // 
      // xrTableCell52
      // 
      this.xrTableCell52.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell52.BorderColor = System.Drawing.Color.White;
      this.xrTableCell52.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell52.Dpi = 254F;
      this.xrTableCell52.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell52.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell52.Name = "xrTableCell52";
      this.xrTableCell52.StylePriority.UseBackColor = false;
      this.xrTableCell52.StylePriority.UseBorderColor = false;
      this.xrTableCell52.StylePriority.UseBorders = false;
      this.xrTableCell52.StylePriority.UseFont = false;
      this.xrTableCell52.StylePriority.UseForeColor = false;
      this.xrTableCell52.StylePriority.UseTextAlignment = false;
      this.xrTableCell52.Text = "Passport or 2nd ID No:";
      this.xrTableCell52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell52.Weight = 0.66666666666666663D;
      // 
      // xrTableCell53
      // 
      this.xrTableCell53.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell53.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell53.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.PassportNo")});
      this.xrTableCell53.Dpi = 254F;
      this.xrTableCell53.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell53.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell53.Name = "xrTableCell53";
      this.xrTableCell53.StylePriority.UseBorderColor = false;
      this.xrTableCell53.StylePriority.UseBorders = false;
      this.xrTableCell53.StylePriority.UseFont = false;
      this.xrTableCell53.StylePriority.UseForeColor = false;
      this.xrTableCell53.StylePriority.UseTextAlignment = false;
      this.xrTableCell53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell53.Weight = 1.333333333333333D;
      // 
      // xrTableRow54
      // 
      this.xrTableRow54.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell54,
            this.xrTableCell55,
            this.xrTableCell56,
            this.xrTableCell58});
      this.xrTableRow54.Dpi = 254F;
      this.xrTableRow54.Name = "xrTableRow54";
      this.xrTableRow54.Weight = 0.94488188976377985D;
      // 
      // xrTableCell54
      // 
      this.xrTableCell54.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell54.BorderColor = System.Drawing.Color.White;
      this.xrTableCell54.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell54.Dpi = 254F;
      this.xrTableCell54.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell54.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell54.Name = "xrTableCell54";
      this.xrTableCell54.StylePriority.UseBackColor = false;
      this.xrTableCell54.StylePriority.UseBorderColor = false;
      this.xrTableCell54.StylePriority.UseBorders = false;
      this.xrTableCell54.StylePriority.UseFont = false;
      this.xrTableCell54.StylePriority.UseForeColor = false;
      this.xrTableCell54.StylePriority.UseTextAlignment = false;
      this.xrTableCell54.Text = "Surname:";
      this.xrTableCell54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell54.Weight = 0.66666666666666674D;
      // 
      // xrTableCell55
      // 
      this.xrTableCell55.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell55.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell55.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.Surname")});
      this.xrTableCell55.Dpi = 254F;
      this.xrTableCell55.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell55.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell55.Name = "xrTableCell55";
      this.xrTableCell55.StylePriority.UseBorderColor = false;
      this.xrTableCell55.StylePriority.UseBorders = false;
      this.xrTableCell55.StylePriority.UseFont = false;
      this.xrTableCell55.StylePriority.UseForeColor = false;
      this.xrTableCell55.StylePriority.UseTextAlignment = false;
      this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell55.Weight = 1.333333333333333D;
      // 
      // xrTableCell56
      // 
      this.xrTableCell56.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell56.BorderColor = System.Drawing.Color.White;
      this.xrTableCell56.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell56.Dpi = 254F;
      this.xrTableCell56.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell56.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell56.Name = "xrTableCell56";
      this.xrTableCell56.StylePriority.UseBackColor = false;
      this.xrTableCell56.StylePriority.UseBorderColor = false;
      this.xrTableCell56.StylePriority.UseBorders = false;
      this.xrTableCell56.StylePriority.UseFont = false;
      this.xrTableCell56.StylePriority.UseForeColor = false;
      this.xrTableCell56.StylePriority.UseTextAlignment = false;
      this.xrTableCell56.Text = "Residential Address:";
      this.xrTableCell56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell56.Weight = 0.66666666666666663D;
      // 
      // xrTableCell58
      // 
      this.xrTableCell58.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell58.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell58.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.ResidentialAddress")});
      this.xrTableCell58.Dpi = 254F;
      this.xrTableCell58.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell58.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell58.Name = "xrTableCell58";
      this.xrTableCell58.StylePriority.UseBorderColor = false;
      this.xrTableCell58.StylePriority.UseBorders = false;
      this.xrTableCell58.StylePriority.UseFont = false;
      this.xrTableCell58.StylePriority.UseForeColor = false;
      this.xrTableCell58.StylePriority.UseTextAlignment = false;
      this.xrTableCell58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell58.Weight = 1.333333333333333D;
      // 
      // xrTableRow56
      // 
      this.xrTableRow56.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell62,
            this.xrTableCell184,
            this.xrTableCell185,
            this.xrTableCell186});
      this.xrTableRow56.Dpi = 254F;
      this.xrTableRow56.Name = "xrTableRow56";
      this.xrTableRow56.Weight = 0.94488188976377985D;
      // 
      // xrTableCell62
      // 
      this.xrTableCell62.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell62.BorderColor = System.Drawing.Color.White;
      this.xrTableCell62.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell62.Dpi = 254F;
      this.xrTableCell62.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell62.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell62.Name = "xrTableCell62";
      this.xrTableCell62.StylePriority.UseBackColor = false;
      this.xrTableCell62.StylePriority.UseBorderColor = false;
      this.xrTableCell62.StylePriority.UseBorders = false;
      this.xrTableCell62.StylePriority.UseFont = false;
      this.xrTableCell62.StylePriority.UseForeColor = false;
      this.xrTableCell62.StylePriority.UseTextAlignment = false;
      this.xrTableCell62.Text = "First Name:";
      this.xrTableCell62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell62.Weight = 0.66666666666666674D;
      // 
      // xrTableCell184
      // 
      this.xrTableCell184.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell184.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell184.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.FirstName")});
      this.xrTableCell184.Dpi = 254F;
      this.xrTableCell184.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell184.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell184.Name = "xrTableCell184";
      this.xrTableCell184.StylePriority.UseBorderColor = false;
      this.xrTableCell184.StylePriority.UseBorders = false;
      this.xrTableCell184.StylePriority.UseFont = false;
      this.xrTableCell184.StylePriority.UseForeColor = false;
      this.xrTableCell184.StylePriority.UseTextAlignment = false;
      this.xrTableCell184.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell184.Weight = 1.333333333333333D;
      // 
      // xrTableCell185
      // 
      this.xrTableCell185.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell185.BorderColor = System.Drawing.Color.White;
      this.xrTableCell185.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell185.Dpi = 254F;
      this.xrTableCell185.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell185.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell185.Name = "xrTableCell185";
      this.xrTableCell185.StylePriority.UseBackColor = false;
      this.xrTableCell185.StylePriority.UseBorderColor = false;
      this.xrTableCell185.StylePriority.UseBorders = false;
      this.xrTableCell185.StylePriority.UseFont = false;
      this.xrTableCell185.StylePriority.UseForeColor = false;
      this.xrTableCell185.StylePriority.UseTextAlignment = false;
      this.xrTableCell185.Text = "Postal Address:";
      this.xrTableCell185.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell185.Weight = 0.66666666666666663D;
      // 
      // xrTableCell186
      // 
      this.xrTableCell186.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell186.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell186.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.PostalAddress")});
      this.xrTableCell186.Dpi = 254F;
      this.xrTableCell186.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell186.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell186.Name = "xrTableCell186";
      this.xrTableCell186.StylePriority.UseBorderColor = false;
      this.xrTableCell186.StylePriority.UseBorders = false;
      this.xrTableCell186.StylePriority.UseFont = false;
      this.xrTableCell186.StylePriority.UseForeColor = false;
      this.xrTableCell186.StylePriority.UseTextAlignment = false;
      this.xrTableCell186.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell186.Weight = 1.333333333333333D;
      // 
      // xrTableRow61
      // 
      this.xrTableRow61.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell203,
            this.xrTableCell204,
            this.xrTableCell205,
            this.xrTableCell206});
      this.xrTableRow61.Dpi = 254F;
      this.xrTableRow61.Name = "xrTableRow61";
      this.xrTableRow61.Weight = 0.94488188976377985D;
      // 
      // xrTableCell203
      // 
      this.xrTableCell203.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell203.BorderColor = System.Drawing.Color.White;
      this.xrTableCell203.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell203.Dpi = 254F;
      this.xrTableCell203.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell203.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell203.Name = "xrTableCell203";
      this.xrTableCell203.StylePriority.UseBackColor = false;
      this.xrTableCell203.StylePriority.UseBorderColor = false;
      this.xrTableCell203.StylePriority.UseBorders = false;
      this.xrTableCell203.StylePriority.UseFont = false;
      this.xrTableCell203.StylePriority.UseForeColor = false;
      this.xrTableCell203.StylePriority.UseTextAlignment = false;
      this.xrTableCell203.Text = "Second Name:";
      this.xrTableCell203.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell203.Weight = 0.66666666666666674D;
      // 
      // xrTableCell204
      // 
      this.xrTableCell204.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell204.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell204.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.SecondName")});
      this.xrTableCell204.Dpi = 254F;
      this.xrTableCell204.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell204.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell204.Name = "xrTableCell204";
      this.xrTableCell204.StylePriority.UseBorderColor = false;
      this.xrTableCell204.StylePriority.UseBorders = false;
      this.xrTableCell204.StylePriority.UseFont = false;
      this.xrTableCell204.StylePriority.UseForeColor = false;
      this.xrTableCell204.StylePriority.UseTextAlignment = false;
      this.xrTableCell204.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell204.Weight = 1.333333333333333D;
      // 
      // xrTableCell205
      // 
      this.xrTableCell205.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell205.BorderColor = System.Drawing.Color.White;
      this.xrTableCell205.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell205.Dpi = 254F;
      this.xrTableCell205.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell205.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell205.Name = "xrTableCell205";
      this.xrTableCell205.StylePriority.UseBackColor = false;
      this.xrTableCell205.StylePriority.UseBorderColor = false;
      this.xrTableCell205.StylePriority.UseBorders = false;
      this.xrTableCell205.StylePriority.UseFont = false;
      this.xrTableCell205.StylePriority.UseForeColor = false;
      this.xrTableCell205.StylePriority.UseTextAlignment = false;
      this.xrTableCell205.Text = "Telephone No. (H):";
      this.xrTableCell205.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell205.Weight = 0.66666666666666663D;
      // 
      // xrTableCell206
      // 
      this.xrTableCell206.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell206.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell206.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.HomeTelephoneNo")});
      this.xrTableCell206.Dpi = 254F;
      this.xrTableCell206.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell206.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell206.Name = "xrTableCell206";
      this.xrTableCell206.StylePriority.UseBorderColor = false;
      this.xrTableCell206.StylePriority.UseBorders = false;
      this.xrTableCell206.StylePriority.UseFont = false;
      this.xrTableCell206.StylePriority.UseForeColor = false;
      this.xrTableCell206.StylePriority.UseTextAlignment = false;
      this.xrTableCell206.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell206.Weight = 1.333333333333333D;
      // 
      // xrTableRow60
      // 
      this.xrTableRow60.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell199,
            this.xrTableCell200,
            this.xrTableCell201,
            this.xrTableCell202});
      this.xrTableRow60.Dpi = 254F;
      this.xrTableRow60.Name = "xrTableRow60";
      this.xrTableRow60.Weight = 0.94488188976377985D;
      // 
      // xrTableCell199
      // 
      this.xrTableCell199.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell199.BorderColor = System.Drawing.Color.White;
      this.xrTableCell199.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell199.Dpi = 254F;
      this.xrTableCell199.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell199.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell199.Name = "xrTableCell199";
      this.xrTableCell199.StylePriority.UseBackColor = false;
      this.xrTableCell199.StylePriority.UseBorderColor = false;
      this.xrTableCell199.StylePriority.UseBorders = false;
      this.xrTableCell199.StylePriority.UseFont = false;
      this.xrTableCell199.StylePriority.UseForeColor = false;
      this.xrTableCell199.StylePriority.UseTextAlignment = false;
      this.xrTableCell199.Text = "Title:";
      this.xrTableCell199.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell199.Weight = 0.66666666666666674D;
      // 
      // xrTableCell200
      // 
      this.xrTableCell200.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell200.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell200.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.TitleDesc")});
      this.xrTableCell200.Dpi = 254F;
      this.xrTableCell200.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell200.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell200.Name = "xrTableCell200";
      this.xrTableCell200.StylePriority.UseBorderColor = false;
      this.xrTableCell200.StylePriority.UseBorders = false;
      this.xrTableCell200.StylePriority.UseFont = false;
      this.xrTableCell200.StylePriority.UseForeColor = false;
      this.xrTableCell200.StylePriority.UseTextAlignment = false;
      this.xrTableCell200.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell200.Weight = 1.333333333333333D;
      // 
      // xrTableCell201
      // 
      this.xrTableCell201.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell201.BorderColor = System.Drawing.Color.White;
      this.xrTableCell201.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell201.Dpi = 254F;
      this.xrTableCell201.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell201.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell201.Name = "xrTableCell201";
      this.xrTableCell201.StylePriority.UseBackColor = false;
      this.xrTableCell201.StylePriority.UseBorderColor = false;
      this.xrTableCell201.StylePriority.UseBorders = false;
      this.xrTableCell201.StylePriority.UseFont = false;
      this.xrTableCell201.StylePriority.UseForeColor = false;
      this.xrTableCell201.StylePriority.UseTextAlignment = false;
      this.xrTableCell201.Text = "Telephone No. (W):";
      this.xrTableCell201.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell201.Weight = 0.66666666666666663D;
      // 
      // xrTableCell202
      // 
      this.xrTableCell202.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell202.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell202.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.WorkTelephoneNo")});
      this.xrTableCell202.Dpi = 254F;
      this.xrTableCell202.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell202.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell202.Name = "xrTableCell202";
      this.xrTableCell202.StylePriority.UseBorderColor = false;
      this.xrTableCell202.StylePriority.UseBorders = false;
      this.xrTableCell202.StylePriority.UseFont = false;
      this.xrTableCell202.StylePriority.UseForeColor = false;
      this.xrTableCell202.StylePriority.UseTextAlignment = false;
      this.xrTableCell202.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell202.Weight = 1.333333333333333D;
      // 
      // xrTableRow59
      // 
      this.xrTableRow59.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell195,
            this.xrTableCell196,
            this.xrTableCell197,
            this.xrTableCell198});
      this.xrTableRow59.Dpi = 254F;
      this.xrTableRow59.Name = "xrTableRow59";
      this.xrTableRow59.Weight = 0.94488188976377985D;
      // 
      // xrTableCell195
      // 
      this.xrTableCell195.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell195.BorderColor = System.Drawing.Color.White;
      this.xrTableCell195.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell195.Dpi = 254F;
      this.xrTableCell195.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell195.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell195.Name = "xrTableCell195";
      this.xrTableCell195.StylePriority.UseBackColor = false;
      this.xrTableCell195.StylePriority.UseBorderColor = false;
      this.xrTableCell195.StylePriority.UseBorders = false;
      this.xrTableCell195.StylePriority.UseFont = false;
      this.xrTableCell195.StylePriority.UseForeColor = false;
      this.xrTableCell195.StylePriority.UseTextAlignment = false;
      this.xrTableCell195.Text = "Gender:";
      this.xrTableCell195.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell195.Weight = 0.66666666666666674D;
      // 
      // xrTableCell196
      // 
      this.xrTableCell196.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell196.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell196.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.Gender")});
      this.xrTableCell196.Dpi = 254F;
      this.xrTableCell196.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell196.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell196.Name = "xrTableCell196";
      this.xrTableCell196.StylePriority.UseBorderColor = false;
      this.xrTableCell196.StylePriority.UseBorders = false;
      this.xrTableCell196.StylePriority.UseFont = false;
      this.xrTableCell196.StylePriority.UseForeColor = false;
      this.xrTableCell196.StylePriority.UseTextAlignment = false;
      this.xrTableCell196.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell196.Weight = 1.333333333333333D;
      // 
      // xrTableCell197
      // 
      this.xrTableCell197.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell197.BorderColor = System.Drawing.Color.White;
      this.xrTableCell197.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell197.Dpi = 254F;
      this.xrTableCell197.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell197.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell197.Multiline = true;
      this.xrTableCell197.Name = "xrTableCell197";
      this.xrTableCell197.StylePriority.UseBackColor = false;
      this.xrTableCell197.StylePriority.UseBorderColor = false;
      this.xrTableCell197.StylePriority.UseBorders = false;
      this.xrTableCell197.StylePriority.UseFont = false;
      this.xrTableCell197.StylePriority.UseForeColor = false;
      this.xrTableCell197.StylePriority.UseTextAlignment = false;
      this.xrTableCell197.Text = "Cellular/Mobile:\r\n";
      this.xrTableCell197.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell197.Weight = 0.66666666666666663D;
      // 
      // xrTableCell198
      // 
      this.xrTableCell198.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell198.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell198.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.CellularNo")});
      this.xrTableCell198.Dpi = 254F;
      this.xrTableCell198.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell198.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell198.Name = "xrTableCell198";
      this.xrTableCell198.StylePriority.UseBorderColor = false;
      this.xrTableCell198.StylePriority.UseBorders = false;
      this.xrTableCell198.StylePriority.UseFont = false;
      this.xrTableCell198.StylePriority.UseForeColor = false;
      this.xrTableCell198.StylePriority.UseTextAlignment = false;
      this.xrTableCell198.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell198.Weight = 1.333333333333333D;
      // 
      // xrTableRow58
      // 
      this.xrTableRow58.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell191,
            this.xrTableCell192,
            this.xrTableCell193,
            this.xrTableCell194});
      this.xrTableRow58.Dpi = 254F;
      this.xrTableRow58.Name = "xrTableRow58";
      this.xrTableRow58.Weight = 0.94488188976377985D;
      // 
      // xrTableCell191
      // 
      this.xrTableCell191.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell191.BorderColor = System.Drawing.Color.White;
      this.xrTableCell191.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell191.Dpi = 254F;
      this.xrTableCell191.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell191.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell191.Name = "xrTableCell191";
      this.xrTableCell191.StylePriority.UseBackColor = false;
      this.xrTableCell191.StylePriority.UseBorderColor = false;
      this.xrTableCell191.StylePriority.UseBorders = false;
      this.xrTableCell191.StylePriority.UseFont = false;
      this.xrTableCell191.StylePriority.UseForeColor = false;
      this.xrTableCell191.StylePriority.UseTextAlignment = false;
      this.xrTableCell191.Text = "Date of Birth:";
      this.xrTableCell191.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell191.Weight = 0.66666666666666674D;
      // 
      // xrTableCell192
      // 
      this.xrTableCell192.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell192.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell192.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.BirthDate")});
      this.xrTableCell192.Dpi = 254F;
      this.xrTableCell192.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell192.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell192.Name = "xrTableCell192";
      this.xrTableCell192.StylePriority.UseBorderColor = false;
      this.xrTableCell192.StylePriority.UseBorders = false;
      this.xrTableCell192.StylePriority.UseFont = false;
      this.xrTableCell192.StylePriority.UseForeColor = false;
      this.xrTableCell192.StylePriority.UseTextAlignment = false;
      this.xrTableCell192.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell192.Weight = 1.333333333333333D;
      // 
      // xrTableCell193
      // 
      this.xrTableCell193.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell193.BorderColor = System.Drawing.Color.White;
      this.xrTableCell193.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell193.Dpi = 254F;
      this.xrTableCell193.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell193.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell193.Name = "xrTableCell193";
      this.xrTableCell193.StylePriority.UseBackColor = false;
      this.xrTableCell193.StylePriority.UseBorderColor = false;
      this.xrTableCell193.StylePriority.UseBorders = false;
      this.xrTableCell193.StylePriority.UseFont = false;
      this.xrTableCell193.StylePriority.UseForeColor = false;
      this.xrTableCell193.StylePriority.UseTextAlignment = false;
      this.xrTableCell193.Text = "Email Address:";
      this.xrTableCell193.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell193.Weight = 0.66666666666666663D;
      // 
      // xrTableCell194
      // 
      this.xrTableCell194.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell194.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell194.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.EmailAddress")});
      this.xrTableCell194.Dpi = 254F;
      this.xrTableCell194.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell194.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell194.Name = "xrTableCell194";
      this.xrTableCell194.StylePriority.UseBorderColor = false;
      this.xrTableCell194.StylePriority.UseBorders = false;
      this.xrTableCell194.StylePriority.UseFont = false;
      this.xrTableCell194.StylePriority.UseForeColor = false;
      this.xrTableCell194.StylePriority.UseTextAlignment = false;
      this.xrTableCell194.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell194.Weight = 1.333333333333333D;
      // 
      // xrTableRow57
      // 
      this.xrTableRow57.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell187,
            this.xrTableCell188,
            this.xrTableCell189,
            this.xrTableCell190});
      this.xrTableRow57.Dpi = 254F;
      this.xrTableRow57.Name = "xrTableRow57";
      this.xrTableRow57.Weight = 0.94488188976377985D;
      // 
      // xrTableCell187
      // 
      this.xrTableCell187.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell187.BorderColor = System.Drawing.Color.White;
      this.xrTableCell187.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell187.Dpi = 254F;
      this.xrTableCell187.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell187.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell187.Name = "xrTableCell187";
      this.xrTableCell187.StylePriority.UseBackColor = false;
      this.xrTableCell187.StylePriority.UseBorderColor = false;
      this.xrTableCell187.StylePriority.UseBorders = false;
      this.xrTableCell187.StylePriority.UseFont = false;
      this.xrTableCell187.StylePriority.UseForeColor = false;
      this.xrTableCell187.StylePriority.UseTextAlignment = false;
      this.xrTableCell187.Text = "Marital Status:";
      this.xrTableCell187.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell187.Weight = 0.66666666666666674D;
      // 
      // xrTableCell188
      // 
      this.xrTableCell188.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell188.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell188.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.MaritalStatusDesc")});
      this.xrTableCell188.Dpi = 254F;
      this.xrTableCell188.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell188.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell188.Name = "xrTableCell188";
      this.xrTableCell188.StylePriority.UseBorderColor = false;
      this.xrTableCell188.StylePriority.UseBorders = false;
      this.xrTableCell188.StylePriority.UseFont = false;
      this.xrTableCell188.StylePriority.UseForeColor = false;
      this.xrTableCell188.StylePriority.UseTextAlignment = false;
      this.xrTableCell188.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell188.Weight = 1.333333333333333D;
      // 
      // xrTableCell189
      // 
      this.xrTableCell189.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell189.BorderColor = System.Drawing.Color.White;
      this.xrTableCell189.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell189.Dpi = 254F;
      this.xrTableCell189.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell189.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell189.Name = "xrTableCell189";
      this.xrTableCell189.StylePriority.UseBackColor = false;
      this.xrTableCell189.StylePriority.UseBorderColor = false;
      this.xrTableCell189.StylePriority.UseBorders = false;
      this.xrTableCell189.StylePriority.UseFont = false;
      this.xrTableCell189.StylePriority.UseForeColor = false;
      this.xrTableCell189.StylePriority.UseTextAlignment = false;
      this.xrTableCell189.Text = "Current Employer:";
      this.xrTableCell189.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell189.Weight = 0.66666666666666663D;
      // 
      // xrTableCell190
      // 
      this.xrTableCell190.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell190.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell190.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.EmployerDetail")});
      this.xrTableCell190.Dpi = 254F;
      this.xrTableCell190.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell190.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell190.Name = "xrTableCell190";
      this.xrTableCell190.StylePriority.UseBorderColor = false;
      this.xrTableCell190.StylePriority.UseBorders = false;
      this.xrTableCell190.StylePriority.UseFont = false;
      this.xrTableCell190.StylePriority.UseForeColor = false;
      this.xrTableCell190.StylePriority.UseTextAlignment = false;
      this.xrTableCell190.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell190.Weight = 1.333333333333333D;
      // 
      // ConsumerOtherDetail
      // 
      this.ConsumerOtherDetail.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail5});
      this.ConsumerOtherDetail.DataMember = "ConsumerOther.ConsumerOther_ConsumerDetail";
      this.ConsumerOtherDetail.Dpi = 254F;
      this.ConsumerOtherDetail.Expanded = false;
      this.ConsumerOtherDetail.Level = 4;
      this.ConsumerOtherDetail.Name = "ConsumerOtherDetail";
      this.ConsumerOtherDetail.Visible = false;
      // 
      // Detail5
      // 
      this.Detail5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
      this.Detail5.Dpi = 254F;
      this.Detail5.HeightF = 690F;
      this.Detail5.KeepTogether = true;
      this.Detail5.Name = "Detail5";
      // 
      // xrTable5
      // 
      this.xrTable5.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTable5.Dpi = 254F;
      this.xrTable5.KeepTogether = true;
      this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 25F);
      this.xrTable5.Name = "xrTable5";
      this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow32,
            this.xrTableRow33,
            this.xrTableRow34,
            this.xrTableRow35,
            this.xrTableRow36,
            this.xrTableRow37,
            this.xrTableRow38,
            this.xrTableRow39,
            this.xrTableRow40,
            this.xrTableRow41,
            this.xrTableRow62});
      this.xrTable5.SizeF = new System.Drawing.SizeF(2058F, 665F);
      this.xrTable5.StylePriority.UseBorders = false;
      this.xrTable5.StylePriority.UsePadding = false;
      // 
      // xrTableRow32
      // 
      this.xrTableRow32.Borders = DevExpress.XtraPrinting.BorderSide.Top;
      this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell114,
            this.xrTableCell115});
      this.xrTableRow32.Dpi = 254F;
      this.xrTableRow32.Name = "xrTableRow32";
      this.xrTableRow32.StylePriority.UseBorders = false;
      this.xrTableRow32.Weight = 1.5748031496062991D;
      // 
      // xrTableCell114
      // 
      this.xrTableCell114.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell114.BorderColor = System.Drawing.Color.Gainsboro;
      this.xrTableCell114.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell114.Dpi = 254F;
      this.xrTableCell114.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
      this.xrTableCell114.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
      this.xrTableCell114.Name = "xrTableCell114";
      this.xrTableCell114.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTableCell114.StylePriority.UseBackColor = false;
      this.xrTableCell114.StylePriority.UseBorderColor = false;
      this.xrTableCell114.StylePriority.UseBorders = false;
      this.xrTableCell114.StylePriority.UseFont = false;
      this.xrTableCell114.StylePriority.UseForeColor = false;
      this.xrTableCell114.StylePriority.UsePadding = false;
      this.xrTableCell114.StylePriority.UseTextAlignment = false;
      this.xrTableCell114.Text = "Personal Details Summary";
      this.xrTableCell114.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell114.Weight = 2D;
      // 
      // xrTableCell115
      // 
      this.xrTableCell115.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell115.BorderColor = System.Drawing.Color.Gainsboro;
      this.xrTableCell115.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell115.Dpi = 254F;
      this.xrTableCell115.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
      this.xrTableCell115.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
      this.xrTableCell115.Name = "xrTableCell115";
      this.xrTableCell115.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTableCell115.StylePriority.UseBackColor = false;
      this.xrTableCell115.StylePriority.UseBorderColor = false;
      this.xrTableCell115.StylePriority.UseBorders = false;
      this.xrTableCell115.StylePriority.UseFont = false;
      this.xrTableCell115.StylePriority.UseForeColor = false;
      this.xrTableCell115.StylePriority.UsePadding = false;
      this.xrTableCell115.StylePriority.UseTextAlignment = false;
      this.xrTableCell115.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell115.Weight = 2D;
      // 
      // xrTableRow33
      // 
      this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell116});
      this.xrTableRow33.Dpi = 254F;
      this.xrTableRow33.Name = "xrTableRow33";
      this.xrTableRow33.Weight = 0.39370078740157477D;
      // 
      // xrTableCell116
      // 
      this.xrTableCell116.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTableCell116.Dpi = 254F;
      this.xrTableCell116.Name = "xrTableCell116";
      this.xrTableCell116.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      this.xrTableCell116.StylePriority.UseBorders = false;
      this.xrTableCell116.StylePriority.UsePadding = false;
      this.xrTableCell116.Text = "\r\n";
      this.xrTableCell116.Weight = 4D;
      // 
      // xrTableRow34
      // 
      this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell117,
            this.xrTableCell118,
            this.xrTableCell119,
            this.xrTableCell120});
      this.xrTableRow34.Dpi = 254F;
      this.xrTableRow34.Name = "xrTableRow34";
      this.xrTableRow34.Weight = 0.94488188976377985D;
      // 
      // xrTableCell117
      // 
      this.xrTableCell117.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell117.BorderColor = System.Drawing.Color.White;
      this.xrTableCell117.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell117.Dpi = 254F;
      this.xrTableCell117.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell117.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell117.Name = "xrTableCell117";
      this.xrTableCell117.StylePriority.UseBackColor = false;
      this.xrTableCell117.StylePriority.UseBorderColor = false;
      this.xrTableCell117.StylePriority.UseBorders = false;
      this.xrTableCell117.StylePriority.UseFont = false;
      this.xrTableCell117.StylePriority.UseForeColor = false;
      this.xrTableCell117.StylePriority.UseTextAlignment = false;
      this.xrTableCell117.Text = "Reference No:";
      this.xrTableCell117.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell117.Weight = 0.66666666666666674D;
      // 
      // xrTableCell118
      // 
      this.xrTableCell118.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell118.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell118.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.ReferenceNo")});
      this.xrTableCell118.Dpi = 254F;
      this.xrTableCell118.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell118.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell118.Name = "xrTableCell118";
      this.xrTableCell118.StylePriority.UseBorderColor = false;
      this.xrTableCell118.StylePriority.UseBorders = false;
      this.xrTableCell118.StylePriority.UseFont = false;
      this.xrTableCell118.StylePriority.UseForeColor = false;
      this.xrTableCell118.StylePriority.UseTextAlignment = false;
      this.xrTableCell118.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell118.Weight = 1.333333333333333D;
      // 
      // xrTableCell119
      // 
      this.xrTableCell119.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell119.BorderColor = System.Drawing.Color.White;
      this.xrTableCell119.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell119.Dpi = 254F;
      this.xrTableCell119.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell119.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell119.Name = "xrTableCell119";
      this.xrTableCell119.StylePriority.UseBackColor = false;
      this.xrTableCell119.StylePriority.UseBorderColor = false;
      this.xrTableCell119.StylePriority.UseBorders = false;
      this.xrTableCell119.StylePriority.UseFont = false;
      this.xrTableCell119.StylePriority.UseForeColor = false;
      this.xrTableCell119.StylePriority.UseTextAlignment = false;
      this.xrTableCell119.Text = "External Reference No:";
      this.xrTableCell119.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell119.Weight = 0.66666666666666663D;
      // 
      // xrTableCell120
      // 
      this.xrTableCell120.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell120.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell120.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.ExternalReference")});
      this.xrTableCell120.Dpi = 254F;
      this.xrTableCell120.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell120.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell120.Name = "xrTableCell120";
      this.xrTableCell120.StylePriority.UseBorderColor = false;
      this.xrTableCell120.StylePriority.UseBorders = false;
      this.xrTableCell120.StylePriority.UseFont = false;
      this.xrTableCell120.StylePriority.UseForeColor = false;
      this.xrTableCell120.StylePriority.UseTextAlignment = false;
      this.xrTableCell120.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell120.Weight = 1.333333333333333D;
      // 
      // xrTableRow35
      // 
      this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell121,
            this.xrTableCell122,
            this.xrTableCell123,
            this.xrTableCell124});
      this.xrTableRow35.Dpi = 254F;
      this.xrTableRow35.Name = "xrTableRow35";
      this.xrTableRow35.Weight = 0.94488188976377985D;
      // 
      // xrTableCell121
      // 
      this.xrTableCell121.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell121.BorderColor = System.Drawing.Color.White;
      this.xrTableCell121.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell121.Dpi = 254F;
      this.xrTableCell121.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell121.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell121.Name = "xrTableCell121";
      this.xrTableCell121.StylePriority.UseBackColor = false;
      this.xrTableCell121.StylePriority.UseBorderColor = false;
      this.xrTableCell121.StylePriority.UseBorders = false;
      this.xrTableCell121.StylePriority.UseFont = false;
      this.xrTableCell121.StylePriority.UseForeColor = false;
      this.xrTableCell121.StylePriority.UseTextAlignment = false;
      this.xrTableCell121.Text = "ID No:";
      this.xrTableCell121.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell121.Weight = 0.66666666666666674D;
      // 
      // xrTableCell122
      // 
      this.xrTableCell122.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell122.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell122.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.ConsumerOther_Id")});
      this.xrTableCell122.Dpi = 254F;
      this.xrTableCell122.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell122.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell122.Name = "xrTableCell122";
      this.xrTableCell122.StylePriority.UseBorderColor = false;
      this.xrTableCell122.StylePriority.UseBorders = false;
      this.xrTableCell122.StylePriority.UseFont = false;
      this.xrTableCell122.StylePriority.UseForeColor = false;
      this.xrTableCell122.StylePriority.UseTextAlignment = false;
      this.xrTableCell122.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell122.Weight = 1.333333333333333D;
      // 
      // xrTableCell123
      // 
      this.xrTableCell123.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell123.BorderColor = System.Drawing.Color.White;
      this.xrTableCell123.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell123.Dpi = 254F;
      this.xrTableCell123.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell123.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell123.Name = "xrTableCell123";
      this.xrTableCell123.StylePriority.UseBackColor = false;
      this.xrTableCell123.StylePriority.UseBorderColor = false;
      this.xrTableCell123.StylePriority.UseBorders = false;
      this.xrTableCell123.StylePriority.UseFont = false;
      this.xrTableCell123.StylePriority.UseForeColor = false;
      this.xrTableCell123.StylePriority.UseTextAlignment = false;
      this.xrTableCell123.Text = "Passport or 2nd ID No:";
      this.xrTableCell123.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell123.Weight = 0.66666666666666663D;
      // 
      // xrTableCell124
      // 
      this.xrTableCell124.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell124.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell124.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.PassportNo")});
      this.xrTableCell124.Dpi = 254F;
      this.xrTableCell124.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell124.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell124.Name = "xrTableCell124";
      this.xrTableCell124.StylePriority.UseBorderColor = false;
      this.xrTableCell124.StylePriority.UseBorders = false;
      this.xrTableCell124.StylePriority.UseFont = false;
      this.xrTableCell124.StylePriority.UseForeColor = false;
      this.xrTableCell124.StylePriority.UseTextAlignment = false;
      this.xrTableCell124.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell124.Weight = 1.333333333333333D;
      // 
      // xrTableRow36
      // 
      this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell125,
            this.xrTableCell126,
            this.xrTableCell127,
            this.xrTableCell128});
      this.xrTableRow36.Dpi = 254F;
      this.xrTableRow36.Name = "xrTableRow36";
      this.xrTableRow36.Weight = 0.94488188976377985D;
      // 
      // xrTableCell125
      // 
      this.xrTableCell125.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell125.BorderColor = System.Drawing.Color.White;
      this.xrTableCell125.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell125.Dpi = 254F;
      this.xrTableCell125.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell125.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell125.Name = "xrTableCell125";
      this.xrTableCell125.StylePriority.UseBackColor = false;
      this.xrTableCell125.StylePriority.UseBorderColor = false;
      this.xrTableCell125.StylePriority.UseBorders = false;
      this.xrTableCell125.StylePriority.UseFont = false;
      this.xrTableCell125.StylePriority.UseForeColor = false;
      this.xrTableCell125.StylePriority.UseTextAlignment = false;
      this.xrTableCell125.Text = "Surname:";
      this.xrTableCell125.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell125.Weight = 0.66666666666666674D;
      // 
      // xrTableCell126
      // 
      this.xrTableCell126.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell126.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell126.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.Surname")});
      this.xrTableCell126.Dpi = 254F;
      this.xrTableCell126.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell126.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell126.Name = "xrTableCell126";
      this.xrTableCell126.StylePriority.UseBorderColor = false;
      this.xrTableCell126.StylePriority.UseBorders = false;
      this.xrTableCell126.StylePriority.UseFont = false;
      this.xrTableCell126.StylePriority.UseForeColor = false;
      this.xrTableCell126.StylePriority.UseTextAlignment = false;
      this.xrTableCell126.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell126.Weight = 1.333333333333333D;
      // 
      // xrTableCell127
      // 
      this.xrTableCell127.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell127.BorderColor = System.Drawing.Color.White;
      this.xrTableCell127.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell127.Dpi = 254F;
      this.xrTableCell127.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell127.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell127.Name = "xrTableCell127";
      this.xrTableCell127.StylePriority.UseBackColor = false;
      this.xrTableCell127.StylePriority.UseBorderColor = false;
      this.xrTableCell127.StylePriority.UseBorders = false;
      this.xrTableCell127.StylePriority.UseFont = false;
      this.xrTableCell127.StylePriority.UseForeColor = false;
      this.xrTableCell127.StylePriority.UseTextAlignment = false;
      this.xrTableCell127.Text = "Residential Address:";
      this.xrTableCell127.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell127.Weight = 0.66666666666666663D;
      // 
      // xrTableCell128
      // 
      this.xrTableCell128.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell128.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell128.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.ResidentialAddress")});
      this.xrTableCell128.Dpi = 254F;
      this.xrTableCell128.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell128.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell128.Name = "xrTableCell128";
      this.xrTableCell128.StylePriority.UseBorderColor = false;
      this.xrTableCell128.StylePriority.UseBorders = false;
      this.xrTableCell128.StylePriority.UseFont = false;
      this.xrTableCell128.StylePriority.UseForeColor = false;
      this.xrTableCell128.StylePriority.UseTextAlignment = false;
      this.xrTableCell128.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell128.Weight = 1.333333333333333D;
      // 
      // xrTableRow37
      // 
      this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell129,
            this.xrTableCell130,
            this.xrTableCell131,
            this.xrTableCell132});
      this.xrTableRow37.Dpi = 254F;
      this.xrTableRow37.Name = "xrTableRow37";
      this.xrTableRow37.Weight = 0.94488188976377985D;
      // 
      // xrTableCell129
      // 
      this.xrTableCell129.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell129.BorderColor = System.Drawing.Color.White;
      this.xrTableCell129.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell129.Dpi = 254F;
      this.xrTableCell129.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell129.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell129.Name = "xrTableCell129";
      this.xrTableCell129.StylePriority.UseBackColor = false;
      this.xrTableCell129.StylePriority.UseBorderColor = false;
      this.xrTableCell129.StylePriority.UseBorders = false;
      this.xrTableCell129.StylePriority.UseFont = false;
      this.xrTableCell129.StylePriority.UseForeColor = false;
      this.xrTableCell129.StylePriority.UseTextAlignment = false;
      this.xrTableCell129.Text = "First Name:";
      this.xrTableCell129.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell129.Weight = 0.66666666666666674D;
      // 
      // xrTableCell130
      // 
      this.xrTableCell130.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell130.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell130.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.FirstName")});
      this.xrTableCell130.Dpi = 254F;
      this.xrTableCell130.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell130.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell130.Name = "xrTableCell130";
      this.xrTableCell130.StylePriority.UseBorderColor = false;
      this.xrTableCell130.StylePriority.UseBorders = false;
      this.xrTableCell130.StylePriority.UseFont = false;
      this.xrTableCell130.StylePriority.UseForeColor = false;
      this.xrTableCell130.StylePriority.UseTextAlignment = false;
      this.xrTableCell130.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell130.Weight = 1.333333333333333D;
      // 
      // xrTableCell131
      // 
      this.xrTableCell131.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell131.BorderColor = System.Drawing.Color.White;
      this.xrTableCell131.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell131.Dpi = 254F;
      this.xrTableCell131.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell131.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell131.Name = "xrTableCell131";
      this.xrTableCell131.StylePriority.UseBackColor = false;
      this.xrTableCell131.StylePriority.UseBorderColor = false;
      this.xrTableCell131.StylePriority.UseBorders = false;
      this.xrTableCell131.StylePriority.UseFont = false;
      this.xrTableCell131.StylePriority.UseForeColor = false;
      this.xrTableCell131.StylePriority.UseTextAlignment = false;
      this.xrTableCell131.Text = "Postal Address:";
      this.xrTableCell131.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell131.Weight = 0.66666666666666663D;
      // 
      // xrTableCell132
      // 
      this.xrTableCell132.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell132.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell132.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.PostalAddress")});
      this.xrTableCell132.Dpi = 254F;
      this.xrTableCell132.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell132.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell132.Name = "xrTableCell132";
      this.xrTableCell132.StylePriority.UseBorderColor = false;
      this.xrTableCell132.StylePriority.UseBorders = false;
      this.xrTableCell132.StylePriority.UseFont = false;
      this.xrTableCell132.StylePriority.UseForeColor = false;
      this.xrTableCell132.StylePriority.UseTextAlignment = false;
      this.xrTableCell132.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell132.Weight = 1.333333333333333D;
      // 
      // xrTableRow38
      // 
      this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell133,
            this.xrTableCell134,
            this.xrTableCell135,
            this.xrTableCell136});
      this.xrTableRow38.Dpi = 254F;
      this.xrTableRow38.Name = "xrTableRow38";
      this.xrTableRow38.Weight = 0.94488188976377985D;
      // 
      // xrTableCell133
      // 
      this.xrTableCell133.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell133.BorderColor = System.Drawing.Color.White;
      this.xrTableCell133.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell133.Dpi = 254F;
      this.xrTableCell133.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell133.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell133.Name = "xrTableCell133";
      this.xrTableCell133.StylePriority.UseBackColor = false;
      this.xrTableCell133.StylePriority.UseBorderColor = false;
      this.xrTableCell133.StylePriority.UseBorders = false;
      this.xrTableCell133.StylePriority.UseFont = false;
      this.xrTableCell133.StylePriority.UseForeColor = false;
      this.xrTableCell133.StylePriority.UseTextAlignment = false;
      this.xrTableCell133.Text = "Second Name:";
      this.xrTableCell133.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell133.Weight = 0.66666666666666674D;
      // 
      // xrTableCell134
      // 
      this.xrTableCell134.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell134.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell134.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.SecondName")});
      this.xrTableCell134.Dpi = 254F;
      this.xrTableCell134.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell134.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell134.Name = "xrTableCell134";
      this.xrTableCell134.StylePriority.UseBorderColor = false;
      this.xrTableCell134.StylePriority.UseBorders = false;
      this.xrTableCell134.StylePriority.UseFont = false;
      this.xrTableCell134.StylePriority.UseForeColor = false;
      this.xrTableCell134.StylePriority.UseTextAlignment = false;
      this.xrTableCell134.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell134.Weight = 1.333333333333333D;
      // 
      // xrTableCell135
      // 
      this.xrTableCell135.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell135.BorderColor = System.Drawing.Color.White;
      this.xrTableCell135.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell135.Dpi = 254F;
      this.xrTableCell135.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell135.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell135.Name = "xrTableCell135";
      this.xrTableCell135.StylePriority.UseBackColor = false;
      this.xrTableCell135.StylePriority.UseBorderColor = false;
      this.xrTableCell135.StylePriority.UseBorders = false;
      this.xrTableCell135.StylePriority.UseFont = false;
      this.xrTableCell135.StylePriority.UseForeColor = false;
      this.xrTableCell135.StylePriority.UseTextAlignment = false;
      this.xrTableCell135.Text = "Telephone No. (H):";
      this.xrTableCell135.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell135.Weight = 0.66666666666666663D;
      // 
      // xrTableCell136
      // 
      this.xrTableCell136.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell136.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell136.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.HomeTelephoneNo")});
      this.xrTableCell136.Dpi = 254F;
      this.xrTableCell136.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell136.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell136.Name = "xrTableCell136";
      this.xrTableCell136.StylePriority.UseBorderColor = false;
      this.xrTableCell136.StylePriority.UseBorders = false;
      this.xrTableCell136.StylePriority.UseFont = false;
      this.xrTableCell136.StylePriority.UseForeColor = false;
      this.xrTableCell136.StylePriority.UseTextAlignment = false;
      this.xrTableCell136.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell136.Weight = 1.333333333333333D;
      // 
      // xrTableRow39
      // 
      this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell137,
            this.xrTableCell138,
            this.xrTableCell139,
            this.xrTableCell140});
      this.xrTableRow39.Dpi = 254F;
      this.xrTableRow39.Name = "xrTableRow39";
      this.xrTableRow39.Weight = 0.94488188976377985D;
      // 
      // xrTableCell137
      // 
      this.xrTableCell137.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell137.BorderColor = System.Drawing.Color.White;
      this.xrTableCell137.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell137.Dpi = 254F;
      this.xrTableCell137.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell137.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell137.Name = "xrTableCell137";
      this.xrTableCell137.StylePriority.UseBackColor = false;
      this.xrTableCell137.StylePriority.UseBorderColor = false;
      this.xrTableCell137.StylePriority.UseBorders = false;
      this.xrTableCell137.StylePriority.UseFont = false;
      this.xrTableCell137.StylePriority.UseForeColor = false;
      this.xrTableCell137.StylePriority.UseTextAlignment = false;
      this.xrTableCell137.Text = "Title:";
      this.xrTableCell137.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell137.Weight = 0.66666666666666674D;
      // 
      // xrTableCell138
      // 
      this.xrTableCell138.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell138.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell138.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.TitleDesc")});
      this.xrTableCell138.Dpi = 254F;
      this.xrTableCell138.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell138.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell138.Name = "xrTableCell138";
      this.xrTableCell138.StylePriority.UseBorderColor = false;
      this.xrTableCell138.StylePriority.UseBorders = false;
      this.xrTableCell138.StylePriority.UseFont = false;
      this.xrTableCell138.StylePriority.UseForeColor = false;
      this.xrTableCell138.StylePriority.UseTextAlignment = false;
      this.xrTableCell138.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell138.Weight = 1.333333333333333D;
      // 
      // xrTableCell139
      // 
      this.xrTableCell139.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell139.BorderColor = System.Drawing.Color.White;
      this.xrTableCell139.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell139.Dpi = 254F;
      this.xrTableCell139.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell139.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell139.Name = "xrTableCell139";
      this.xrTableCell139.StylePriority.UseBackColor = false;
      this.xrTableCell139.StylePriority.UseBorderColor = false;
      this.xrTableCell139.StylePriority.UseBorders = false;
      this.xrTableCell139.StylePriority.UseFont = false;
      this.xrTableCell139.StylePriority.UseForeColor = false;
      this.xrTableCell139.StylePriority.UseTextAlignment = false;
      this.xrTableCell139.Text = "Telephone No. (W):";
      this.xrTableCell139.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell139.Weight = 0.66666666666666663D;
      // 
      // xrTableCell140
      // 
      this.xrTableCell140.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell140.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell140.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.WorkTelephoneNo")});
      this.xrTableCell140.Dpi = 254F;
      this.xrTableCell140.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell140.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell140.Name = "xrTableCell140";
      this.xrTableCell140.StylePriority.UseBorderColor = false;
      this.xrTableCell140.StylePriority.UseBorders = false;
      this.xrTableCell140.StylePriority.UseFont = false;
      this.xrTableCell140.StylePriority.UseForeColor = false;
      this.xrTableCell140.StylePriority.UseTextAlignment = false;
      this.xrTableCell140.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell140.Weight = 1.333333333333333D;
      // 
      // xrTableRow40
      // 
      this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell141,
            this.xrTableCell142,
            this.xrTableCell143,
            this.xrTableCell144});
      this.xrTableRow40.Dpi = 254F;
      this.xrTableRow40.Name = "xrTableRow40";
      this.xrTableRow40.Weight = 0.94488188976377985D;
      // 
      // xrTableCell141
      // 
      this.xrTableCell141.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell141.BorderColor = System.Drawing.Color.White;
      this.xrTableCell141.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell141.Dpi = 254F;
      this.xrTableCell141.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell141.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell141.Name = "xrTableCell141";
      this.xrTableCell141.StylePriority.UseBackColor = false;
      this.xrTableCell141.StylePriority.UseBorderColor = false;
      this.xrTableCell141.StylePriority.UseBorders = false;
      this.xrTableCell141.StylePriority.UseFont = false;
      this.xrTableCell141.StylePriority.UseForeColor = false;
      this.xrTableCell141.StylePriority.UseTextAlignment = false;
      this.xrTableCell141.Text = "Gender:";
      this.xrTableCell141.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell141.Weight = 0.66666666666666674D;
      // 
      // xrTableCell142
      // 
      this.xrTableCell142.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell142.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell142.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.Gender")});
      this.xrTableCell142.Dpi = 254F;
      this.xrTableCell142.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell142.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell142.Name = "xrTableCell142";
      this.xrTableCell142.StylePriority.UseBorderColor = false;
      this.xrTableCell142.StylePriority.UseBorders = false;
      this.xrTableCell142.StylePriority.UseFont = false;
      this.xrTableCell142.StylePriority.UseForeColor = false;
      this.xrTableCell142.StylePriority.UseTextAlignment = false;
      this.xrTableCell142.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell142.Weight = 1.333333333333333D;
      // 
      // xrTableCell143
      // 
      this.xrTableCell143.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell143.BorderColor = System.Drawing.Color.White;
      this.xrTableCell143.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell143.Dpi = 254F;
      this.xrTableCell143.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell143.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell143.Multiline = true;
      this.xrTableCell143.Name = "xrTableCell143";
      this.xrTableCell143.StylePriority.UseBackColor = false;
      this.xrTableCell143.StylePriority.UseBorderColor = false;
      this.xrTableCell143.StylePriority.UseBorders = false;
      this.xrTableCell143.StylePriority.UseFont = false;
      this.xrTableCell143.StylePriority.UseForeColor = false;
      this.xrTableCell143.StylePriority.UseTextAlignment = false;
      this.xrTableCell143.Text = "Cellular/Mobile:\r\n";
      this.xrTableCell143.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell143.Weight = 0.66666666666666663D;
      // 
      // xrTableCell144
      // 
      this.xrTableCell144.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell144.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell144.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.CellularNo")});
      this.xrTableCell144.Dpi = 254F;
      this.xrTableCell144.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell144.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell144.Name = "xrTableCell144";
      this.xrTableCell144.StylePriority.UseBorderColor = false;
      this.xrTableCell144.StylePriority.UseBorders = false;
      this.xrTableCell144.StylePriority.UseFont = false;
      this.xrTableCell144.StylePriority.UseForeColor = false;
      this.xrTableCell144.StylePriority.UseTextAlignment = false;
      this.xrTableCell144.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell144.Weight = 1.333333333333333D;
      // 
      // xrTableRow41
      // 
      this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell145,
            this.xrTableCell146,
            this.xrTableCell147,
            this.xrTableCell148});
      this.xrTableRow41.Dpi = 254F;
      this.xrTableRow41.Name = "xrTableRow41";
      this.xrTableRow41.Weight = 0.94488188976377985D;
      // 
      // xrTableCell145
      // 
      this.xrTableCell145.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell145.BorderColor = System.Drawing.Color.White;
      this.xrTableCell145.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell145.Dpi = 254F;
      this.xrTableCell145.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell145.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell145.Name = "xrTableCell145";
      this.xrTableCell145.StylePriority.UseBackColor = false;
      this.xrTableCell145.StylePriority.UseBorderColor = false;
      this.xrTableCell145.StylePriority.UseBorders = false;
      this.xrTableCell145.StylePriority.UseFont = false;
      this.xrTableCell145.StylePriority.UseForeColor = false;
      this.xrTableCell145.StylePriority.UseTextAlignment = false;
      this.xrTableCell145.Text = "Date of Birth:";
      this.xrTableCell145.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell145.Weight = 0.66666666666666674D;
      // 
      // xrTableCell146
      // 
      this.xrTableCell146.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell146.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell146.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.BirthDate")});
      this.xrTableCell146.Dpi = 254F;
      this.xrTableCell146.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell146.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell146.Name = "xrTableCell146";
      this.xrTableCell146.StylePriority.UseBorderColor = false;
      this.xrTableCell146.StylePriority.UseBorders = false;
      this.xrTableCell146.StylePriority.UseFont = false;
      this.xrTableCell146.StylePriority.UseForeColor = false;
      this.xrTableCell146.StylePriority.UseTextAlignment = false;
      this.xrTableCell146.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell146.Weight = 1.333333333333333D;
      // 
      // xrTableCell147
      // 
      this.xrTableCell147.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell147.BorderColor = System.Drawing.Color.White;
      this.xrTableCell147.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell147.Dpi = 254F;
      this.xrTableCell147.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell147.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell147.Name = "xrTableCell147";
      this.xrTableCell147.StylePriority.UseBackColor = false;
      this.xrTableCell147.StylePriority.UseBorderColor = false;
      this.xrTableCell147.StylePriority.UseBorders = false;
      this.xrTableCell147.StylePriority.UseFont = false;
      this.xrTableCell147.StylePriority.UseForeColor = false;
      this.xrTableCell147.StylePriority.UseTextAlignment = false;
      this.xrTableCell147.Text = "Email Address:";
      this.xrTableCell147.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell147.Weight = 0.66666666666666663D;
      // 
      // xrTableCell148
      // 
      this.xrTableCell148.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell148.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell148.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.EmailAddress")});
      this.xrTableCell148.Dpi = 254F;
      this.xrTableCell148.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell148.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell148.Name = "xrTableCell148";
      this.xrTableCell148.StylePriority.UseBorderColor = false;
      this.xrTableCell148.StylePriority.UseBorders = false;
      this.xrTableCell148.StylePriority.UseFont = false;
      this.xrTableCell148.StylePriority.UseForeColor = false;
      this.xrTableCell148.StylePriority.UseTextAlignment = false;
      this.xrTableCell148.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell148.Weight = 1.333333333333333D;
      // 
      // xrTableRow62
      // 
      this.xrTableRow62.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell149,
            this.xrTableCell150,
            this.xrTableCell207,
            this.xrTableCell208});
      this.xrTableRow62.Dpi = 254F;
      this.xrTableRow62.Name = "xrTableRow62";
      this.xrTableRow62.Weight = 0.94488188976377985D;
      // 
      // xrTableCell149
      // 
      this.xrTableCell149.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell149.BorderColor = System.Drawing.Color.White;
      this.xrTableCell149.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell149.Dpi = 254F;
      this.xrTableCell149.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell149.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell149.Name = "xrTableCell149";
      this.xrTableCell149.StylePriority.UseBackColor = false;
      this.xrTableCell149.StylePriority.UseBorderColor = false;
      this.xrTableCell149.StylePriority.UseBorders = false;
      this.xrTableCell149.StylePriority.UseFont = false;
      this.xrTableCell149.StylePriority.UseForeColor = false;
      this.xrTableCell149.StylePriority.UseTextAlignment = false;
      this.xrTableCell149.Text = "Marital Status:";
      this.xrTableCell149.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell149.Weight = 0.66666666666666674D;
      // 
      // xrTableCell150
      // 
      this.xrTableCell150.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell150.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell150.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.MaritalStatusDesc")});
      this.xrTableCell150.Dpi = 254F;
      this.xrTableCell150.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell150.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell150.Name = "xrTableCell150";
      this.xrTableCell150.StylePriority.UseBorderColor = false;
      this.xrTableCell150.StylePriority.UseBorders = false;
      this.xrTableCell150.StylePriority.UseFont = false;
      this.xrTableCell150.StylePriority.UseForeColor = false;
      this.xrTableCell150.StylePriority.UseTextAlignment = false;
      this.xrTableCell150.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell150.Weight = 1.333333333333333D;
      // 
      // xrTableCell207
      // 
      this.xrTableCell207.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell207.BorderColor = System.Drawing.Color.White;
      this.xrTableCell207.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell207.Dpi = 254F;
      this.xrTableCell207.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell207.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell207.Name = "xrTableCell207";
      this.xrTableCell207.StylePriority.UseBackColor = false;
      this.xrTableCell207.StylePriority.UseBorderColor = false;
      this.xrTableCell207.StylePriority.UseBorders = false;
      this.xrTableCell207.StylePriority.UseFont = false;
      this.xrTableCell207.StylePriority.UseForeColor = false;
      this.xrTableCell207.StylePriority.UseTextAlignment = false;
      this.xrTableCell207.Text = "Current Employer:";
      this.xrTableCell207.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell207.Weight = 0.66666666666666663D;
      // 
      // xrTableCell208
      // 
      this.xrTableCell208.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell208.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell208.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.EmployerDetail")});
      this.xrTableCell208.Dpi = 254F;
      this.xrTableCell208.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell208.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell208.Name = "xrTableCell208";
      this.xrTableCell208.StylePriority.UseBorderColor = false;
      this.xrTableCell208.StylePriority.UseBorders = false;
      this.xrTableCell208.StylePriority.UseFont = false;
      this.xrTableCell208.StylePriority.UseForeColor = false;
      this.xrTableCell208.StylePriority.UseTextAlignment = false;
      this.xrTableCell208.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell208.Weight = 1.333333333333333D;
      // 
      // Detail6
      // 
      this.Detail6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable7});
      this.Detail6.Dpi = 254F;
      this.Detail6.HeightF = 60F;
      this.Detail6.KeepTogether = true;
      this.Detail6.Name = "Detail6";
      // 
      // xrTable7
      // 
      this.xrTable7.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTable7.Dpi = 254F;
      this.xrTable7.KeepTogether = true;
      this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
      this.xrTable7.Name = "xrTable7";
      this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow30});
      this.xrTable7.SizeF = new System.Drawing.SizeF(2058F, 60F);
      this.xrTable7.StylePriority.UseBorders = false;
      this.xrTable7.StylePriority.UsePadding = false;
      this.xrTable7.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTable7_BeforePrint);
      // 
      // xrTableRow30
      // 
      this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.xrTableCell11,
            this.xrTableCell12,
            this.xrTableCell13,
            this.xrTableCell14,
            this.xrTableCell91,
            this.xrTableCell92});
      this.xrTableRow30.Dpi = 254F;
      this.xrTableRow30.Name = "xrTableRow30";
      this.xrTableRow30.Weight = 0.94488188976377985D;
      // 
      // xrTableCell10
      // 
      this.xrTableCell10.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell10.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell10.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerAddressHistory.LastUpdatedDate")});
      this.xrTableCell10.Dpi = 254F;
      this.xrTableCell10.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell10.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell10.Name = "xrTableCell10";
      this.xrTableCell10.StylePriority.UseBackColor = false;
      this.xrTableCell10.StylePriority.UseBorderColor = false;
      this.xrTableCell10.StylePriority.UseBorders = false;
      this.xrTableCell10.StylePriority.UseFont = false;
      this.xrTableCell10.StylePriority.UseForeColor = false;
      this.xrTableCell10.StylePriority.UseTextAlignment = false;
      this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell10.Weight = 0.5714285714285714D;
      // 
      // xrTableCell11
      // 
      this.xrTableCell11.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell11.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell11.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell11.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerAddressHistory.AddressType")});
      this.xrTableCell11.Dpi = 254F;
      this.xrTableCell11.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell11.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell11.Name = "xrTableCell11";
      this.xrTableCell11.StylePriority.UseBackColor = false;
      this.xrTableCell11.StylePriority.UseBorderColor = false;
      this.xrTableCell11.StylePriority.UseBorders = false;
      this.xrTableCell11.StylePriority.UseFont = false;
      this.xrTableCell11.StylePriority.UseForeColor = false;
      this.xrTableCell11.StylePriority.UseTextAlignment = false;
      this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell11.Weight = 0.5714285714285714D;
      // 
      // xrTableCell12
      // 
      this.xrTableCell12.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell12.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell12.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerAddressHistory.Address1")});
      this.xrTableCell12.Dpi = 254F;
      this.xrTableCell12.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell12.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell12.Name = "xrTableCell12";
      this.xrTableCell12.StylePriority.UseBackColor = false;
      this.xrTableCell12.StylePriority.UseBorderColor = false;
      this.xrTableCell12.StylePriority.UseBorders = false;
      this.xrTableCell12.StylePriority.UseFont = false;
      this.xrTableCell12.StylePriority.UseForeColor = false;
      this.xrTableCell12.StylePriority.UseTextAlignment = false;
      this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell12.Weight = 0.57142857142857129D;
      // 
      // xrTableCell13
      // 
      this.xrTableCell13.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell13.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell13.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell13.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerAddressHistory.Address2")});
      this.xrTableCell13.Dpi = 254F;
      this.xrTableCell13.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell13.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell13.Name = "xrTableCell13";
      this.xrTableCell13.StylePriority.UseBackColor = false;
      this.xrTableCell13.StylePriority.UseBorderColor = false;
      this.xrTableCell13.StylePriority.UseBorders = false;
      this.xrTableCell13.StylePriority.UseFont = false;
      this.xrTableCell13.StylePriority.UseForeColor = false;
      this.xrTableCell13.StylePriority.UseTextAlignment = false;
      this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell13.Weight = 0.57142857142857129D;
      // 
      // xrTableCell14
      // 
      this.xrTableCell14.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell14.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell14.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell14.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerAddressHistory.Address3")});
      this.xrTableCell14.Dpi = 254F;
      this.xrTableCell14.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell14.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell14.Name = "xrTableCell14";
      this.xrTableCell14.StylePriority.UseBackColor = false;
      this.xrTableCell14.StylePriority.UseBorderColor = false;
      this.xrTableCell14.StylePriority.UseBorders = false;
      this.xrTableCell14.StylePriority.UseFont = false;
      this.xrTableCell14.StylePriority.UseForeColor = false;
      this.xrTableCell14.StylePriority.UseTextAlignment = false;
      this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell14.Weight = 0.5714285714285714D;
      // 
      // xrTableCell91
      // 
      this.xrTableCell91.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell91.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell91.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell91.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerAddressHistory.Address4")});
      this.xrTableCell91.Dpi = 254F;
      this.xrTableCell91.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell91.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell91.Name = "xrTableCell91";
      this.xrTableCell91.StylePriority.UseBackColor = false;
      this.xrTableCell91.StylePriority.UseBorderColor = false;
      this.xrTableCell91.StylePriority.UseBorders = false;
      this.xrTableCell91.StylePriority.UseFont = false;
      this.xrTableCell91.StylePriority.UseForeColor = false;
      this.xrTableCell91.StylePriority.UseTextAlignment = false;
      this.xrTableCell91.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell91.Weight = 0.5714285714285714D;
      // 
      // xrTableCell92
      // 
      this.xrTableCell92.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell92.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell92.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell92.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerAddressHistory.PostalCode")});
      this.xrTableCell92.Dpi = 254F;
      this.xrTableCell92.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell92.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell92.Name = "xrTableCell92";
      this.xrTableCell92.StylePriority.UseBackColor = false;
      this.xrTableCell92.StylePriority.UseBorderColor = false;
      this.xrTableCell92.StylePriority.UseBorders = false;
      this.xrTableCell92.StylePriority.UseFont = false;
      this.xrTableCell92.StylePriority.UseForeColor = false;
      this.xrTableCell92.StylePriority.UseTextAlignment = false;
      this.xrTableCell92.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell92.Weight = 0.57142857142857117D;
      // 
      // GroupHeader2
      // 
      this.GroupHeader2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
      this.GroupHeader2.Dpi = 254F;
      this.GroupHeader2.HeightF = 270F;
      this.GroupHeader2.Name = "GroupHeader2";
      // 
      // xrTable6
      // 
      this.xrTable6.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTable6.Dpi = 254F;
      this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 25F);
      this.xrTable6.Name = "xrTable6";
      this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow22,
            this.xrTableRow23,
            this.xrTableRow25,
            this.xrTableRow24});
      this.xrTable6.SizeF = new System.Drawing.SizeF(2058F, 245F);
      this.xrTable6.StylePriority.UseBorders = false;
      this.xrTable6.StylePriority.UsePadding = false;
      // 
      // xrTableRow22
      // 
      this.xrTableRow22.Borders = DevExpress.XtraPrinting.BorderSide.Top;
      this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell77,
            this.xrTableCell78});
      this.xrTableRow22.Dpi = 254F;
      this.xrTableRow22.Name = "xrTableRow22";
      this.xrTableRow22.StylePriority.UseBorders = false;
      this.xrTableRow22.Weight = 1.5748031496062991D;
      // 
      // xrTableCell77
      // 
      this.xrTableCell77.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell77.BorderColor = System.Drawing.Color.Gainsboro;
      this.xrTableCell77.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell77.Dpi = 254F;
      this.xrTableCell77.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
      this.xrTableCell77.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
      this.xrTableCell77.Name = "xrTableCell77";
      this.xrTableCell77.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTableCell77.StylePriority.UseBackColor = false;
      this.xrTableCell77.StylePriority.UseBorderColor = false;
      this.xrTableCell77.StylePriority.UseBorders = false;
      this.xrTableCell77.StylePriority.UseFont = false;
      this.xrTableCell77.StylePriority.UseForeColor = false;
      this.xrTableCell77.StylePriority.UsePadding = false;
      this.xrTableCell77.StylePriority.UseTextAlignment = false;
      this.xrTableCell77.Text = "Address History";
      this.xrTableCell77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell77.Weight = 2D;
      // 
      // xrTableCell78
      // 
      this.xrTableCell78.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell78.BorderColor = System.Drawing.Color.Gainsboro;
      this.xrTableCell78.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell78.Dpi = 254F;
      this.xrTableCell78.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
      this.xrTableCell78.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
      this.xrTableCell78.Name = "xrTableCell78";
      this.xrTableCell78.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTableCell78.StylePriority.UseBackColor = false;
      this.xrTableCell78.StylePriority.UseBorderColor = false;
      this.xrTableCell78.StylePriority.UseBorders = false;
      this.xrTableCell78.StylePriority.UseFont = false;
      this.xrTableCell78.StylePriority.UseForeColor = false;
      this.xrTableCell78.StylePriority.UsePadding = false;
      this.xrTableCell78.StylePriority.UseTextAlignment = false;
      this.xrTableCell78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell78.Weight = 2D;
      // 
      // xrTableRow23
      // 
      this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell79});
      this.xrTableRow23.Dpi = 254F;
      this.xrTableRow23.Name = "xrTableRow23";
      this.xrTableRow23.Weight = 0.39370078740157477D;
      // 
      // xrTableCell79
      // 
      this.xrTableCell79.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTableCell79.Dpi = 254F;
      this.xrTableCell79.Name = "xrTableCell79";
      this.xrTableCell79.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      this.xrTableCell79.StylePriority.UseBorders = false;
      this.xrTableCell79.StylePriority.UsePadding = false;
      this.xrTableCell79.Text = "\r\n";
      this.xrTableCell79.Weight = 4D;
      // 
      // xrTableRow25
      // 
      this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell93});
      this.xrTableRow25.Dpi = 254F;
      this.xrTableRow25.Name = "xrTableRow25";
      this.xrTableRow25.Weight = 0.94488188976377985D;
      // 
      // xrTableCell93
      // 
      this.xrTableCell93.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell93.BorderColor = System.Drawing.Color.Black;
      this.xrTableCell93.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell93.Dpi = 254F;
      this.xrTableCell93.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell93.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell93.Name = "xrTableCell93";
      this.xrTableCell93.StylePriority.UseBackColor = false;
      this.xrTableCell93.StylePriority.UseBorderColor = false;
      this.xrTableCell93.StylePriority.UseBorders = false;
      this.xrTableCell93.StylePriority.UseFont = false;
      this.xrTableCell93.StylePriority.UseForeColor = false;
      this.xrTableCell93.StylePriority.UseTextAlignment = false;
      this.xrTableCell93.Text = "Address History - Party A";
      this.xrTableCell93.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell93.Weight = 3.9999999999999991D;
      // 
      // xrTableRow24
      // 
      this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell88,
            this.xrTableCell80,
            this.xrTableCell89,
            this.xrTableCell81,
            this.xrTableCell90,
            this.xrTableCell82,
            this.xrTableCell83});
      this.xrTableRow24.Dpi = 254F;
      this.xrTableRow24.Name = "xrTableRow24";
      this.xrTableRow24.Weight = 0.94488188976377985D;
      // 
      // xrTableCell88
      // 
      this.xrTableCell88.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell88.BorderColor = System.Drawing.Color.White;
      this.xrTableCell88.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell88.Dpi = 254F;
      this.xrTableCell88.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell88.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell88.Name = "xrTableCell88";
      this.xrTableCell88.StylePriority.UseBackColor = false;
      this.xrTableCell88.StylePriority.UseBorderColor = false;
      this.xrTableCell88.StylePriority.UseBorders = false;
      this.xrTableCell88.StylePriority.UseFont = false;
      this.xrTableCell88.StylePriority.UseForeColor = false;
      this.xrTableCell88.StylePriority.UseTextAlignment = false;
      this.xrTableCell88.Text = "Bureau Update";
      this.xrTableCell88.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell88.Weight = 0.5714285714285714D;
      // 
      // xrTableCell80
      // 
      this.xrTableCell80.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell80.BorderColor = System.Drawing.Color.White;
      this.xrTableCell80.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell80.Dpi = 254F;
      this.xrTableCell80.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell80.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell80.Name = "xrTableCell80";
      this.xrTableCell80.StylePriority.UseBackColor = false;
      this.xrTableCell80.StylePriority.UseBorderColor = false;
      this.xrTableCell80.StylePriority.UseBorders = false;
      this.xrTableCell80.StylePriority.UseFont = false;
      this.xrTableCell80.StylePriority.UseForeColor = false;
      this.xrTableCell80.StylePriority.UseTextAlignment = false;
      this.xrTableCell80.Text = "Type";
      this.xrTableCell80.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell80.Weight = 0.5714285714285714D;
      // 
      // xrTableCell89
      // 
      this.xrTableCell89.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell89.BorderColor = System.Drawing.Color.White;
      this.xrTableCell89.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell89.Dpi = 254F;
      this.xrTableCell89.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell89.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell89.Name = "xrTableCell89";
      this.xrTableCell89.StylePriority.UseBackColor = false;
      this.xrTableCell89.StylePriority.UseBorderColor = false;
      this.xrTableCell89.StylePriority.UseBorders = false;
      this.xrTableCell89.StylePriority.UseFont = false;
      this.xrTableCell89.StylePriority.UseForeColor = false;
      this.xrTableCell89.StylePriority.UseTextAlignment = false;
      this.xrTableCell89.Text = "Line 1";
      this.xrTableCell89.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell89.Weight = 0.57142857142857129D;
      // 
      // xrTableCell81
      // 
      this.xrTableCell81.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell81.BorderColor = System.Drawing.Color.White;
      this.xrTableCell81.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell81.Dpi = 254F;
      this.xrTableCell81.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell81.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell81.Name = "xrTableCell81";
      this.xrTableCell81.StylePriority.UseBackColor = false;
      this.xrTableCell81.StylePriority.UseBorderColor = false;
      this.xrTableCell81.StylePriority.UseBorders = false;
      this.xrTableCell81.StylePriority.UseFont = false;
      this.xrTableCell81.StylePriority.UseForeColor = false;
      this.xrTableCell81.StylePriority.UseTextAlignment = false;
      this.xrTableCell81.Text = "Line 2";
      this.xrTableCell81.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell81.Weight = 0.57142857142857129D;
      // 
      // xrTableCell90
      // 
      this.xrTableCell90.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell90.BorderColor = System.Drawing.Color.White;
      this.xrTableCell90.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell90.Dpi = 254F;
      this.xrTableCell90.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell90.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell90.Name = "xrTableCell90";
      this.xrTableCell90.StylePriority.UseBackColor = false;
      this.xrTableCell90.StylePriority.UseBorderColor = false;
      this.xrTableCell90.StylePriority.UseBorders = false;
      this.xrTableCell90.StylePriority.UseFont = false;
      this.xrTableCell90.StylePriority.UseForeColor = false;
      this.xrTableCell90.StylePriority.UseTextAlignment = false;
      this.xrTableCell90.Text = "Line 3";
      this.xrTableCell90.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell90.Weight = 0.5714285714285714D;
      // 
      // xrTableCell82
      // 
      this.xrTableCell82.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell82.BorderColor = System.Drawing.Color.White;
      this.xrTableCell82.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell82.Dpi = 254F;
      this.xrTableCell82.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell82.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell82.Name = "xrTableCell82";
      this.xrTableCell82.StylePriority.UseBackColor = false;
      this.xrTableCell82.StylePriority.UseBorderColor = false;
      this.xrTableCell82.StylePriority.UseBorders = false;
      this.xrTableCell82.StylePriority.UseFont = false;
      this.xrTableCell82.StylePriority.UseForeColor = false;
      this.xrTableCell82.StylePriority.UseTextAlignment = false;
      this.xrTableCell82.Text = "Line 4";
      this.xrTableCell82.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell82.Weight = 0.5714285714285714D;
      // 
      // xrTableCell83
      // 
      this.xrTableCell83.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell83.BorderColor = System.Drawing.Color.White;
      this.xrTableCell83.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell83.Dpi = 254F;
      this.xrTableCell83.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell83.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell83.Name = "xrTableCell83";
      this.xrTableCell83.StylePriority.UseBackColor = false;
      this.xrTableCell83.StylePriority.UseBorderColor = false;
      this.xrTableCell83.StylePriority.UseBorders = false;
      this.xrTableCell83.StylePriority.UseFont = false;
      this.xrTableCell83.StylePriority.UseForeColor = false;
      this.xrTableCell83.StylePriority.UseTextAlignment = false;
      this.xrTableCell83.Text = "Postal Code";
      this.xrTableCell83.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell83.Weight = 0.57142857142857117D;
      // 
      // ConsumerOtherDetail_Address
      // 
      this.ConsumerOtherDetail_Address.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail7,
            this.GroupHeader3});
      this.ConsumerOtherDetail_Address.DataMember = "ConsumerOther.ConsumerOther_ConsumerAddressHistory";
      this.ConsumerOtherDetail_Address.Dpi = 254F;
      this.ConsumerOtherDetail_Address.Level = 7;
      this.ConsumerOtherDetail_Address.Name = "ConsumerOtherDetail_Address";
      // 
      // Detail7
      // 
      this.Detail7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable8});
      this.Detail7.Dpi = 254F;
      this.Detail7.HeightF = 60F;
      this.Detail7.KeepTogether = true;
      this.Detail7.Name = "Detail7";
      this.Detail7.StylePriority.UseBorders = false;
      // 
      // xrTable8
      // 
      this.xrTable8.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTable8.Dpi = 254F;
      this.xrTable8.KeepTogether = true;
      this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
      this.xrTable8.Name = "xrTable8";
      this.xrTable8.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
      this.xrTable8.SizeF = new System.Drawing.SizeF(2058F, 60F);
      this.xrTable8.StylePriority.UseBorders = false;
      this.xrTable8.StylePriority.UsePadding = false;
      // 
      // xrTableRow4
      // 
      this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2,
            this.xrTableCell9,
            this.xrTableCell57,
            this.xrTableCell61,
            this.xrTableCell63,
            this.xrTableCell64,
            this.xrTableCell65});
      this.xrTableRow4.Dpi = 254F;
      this.xrTableRow4.Name = "xrTableRow4";
      this.xrTableRow4.Weight = 0.94488188976377985D;
      // 
      // xrTableCell2
      // 
      this.xrTableCell2.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerAddressHistory.LastUpdatedDate")});
      this.xrTableCell2.Dpi = 254F;
      this.xrTableCell2.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell2.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell2.Name = "xrTableCell2";
      this.xrTableCell2.Scripts.OnBeforePrint = "xrTableCell2_BeforePrint";
      this.xrTableCell2.StylePriority.UseBackColor = false;
      this.xrTableCell2.StylePriority.UseBorderColor = false;
      this.xrTableCell2.StylePriority.UseBorders = false;
      this.xrTableCell2.StylePriority.UseFont = false;
      this.xrTableCell2.StylePriority.UseForeColor = false;
      this.xrTableCell2.StylePriority.UseTextAlignment = false;
      this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell2.Weight = 0.5714285714285714D;
      // 
      // xrTableCell9
      // 
      this.xrTableCell9.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell9.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell9.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerAddressHistory.AddressType")});
      this.xrTableCell9.Dpi = 254F;
      this.xrTableCell9.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell9.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell9.Name = "xrTableCell9";
      this.xrTableCell9.StylePriority.UseBackColor = false;
      this.xrTableCell9.StylePriority.UseBorderColor = false;
      this.xrTableCell9.StylePriority.UseBorders = false;
      this.xrTableCell9.StylePriority.UseFont = false;
      this.xrTableCell9.StylePriority.UseForeColor = false;
      this.xrTableCell9.StylePriority.UseTextAlignment = false;
      this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell9.Weight = 0.5714285714285714D;
      // 
      // xrTableCell57
      // 
      this.xrTableCell57.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell57.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell57.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell57.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerAddressHistory.Address1")});
      this.xrTableCell57.Dpi = 254F;
      this.xrTableCell57.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell57.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell57.Name = "xrTableCell57";
      this.xrTableCell57.StylePriority.UseBackColor = false;
      this.xrTableCell57.StylePriority.UseBorderColor = false;
      this.xrTableCell57.StylePriority.UseBorders = false;
      this.xrTableCell57.StylePriority.UseFont = false;
      this.xrTableCell57.StylePriority.UseForeColor = false;
      this.xrTableCell57.StylePriority.UseTextAlignment = false;
      this.xrTableCell57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell57.Weight = 0.57142857142857129D;
      // 
      // xrTableCell61
      // 
      this.xrTableCell61.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell61.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell61.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell61.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerAddressHistory.Address2")});
      this.xrTableCell61.Dpi = 254F;
      this.xrTableCell61.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell61.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell61.Name = "xrTableCell61";
      this.xrTableCell61.StylePriority.UseBackColor = false;
      this.xrTableCell61.StylePriority.UseBorderColor = false;
      this.xrTableCell61.StylePriority.UseBorders = false;
      this.xrTableCell61.StylePriority.UseFont = false;
      this.xrTableCell61.StylePriority.UseForeColor = false;
      this.xrTableCell61.StylePriority.UseTextAlignment = false;
      this.xrTableCell61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell61.Weight = 0.57142857142857129D;
      // 
      // xrTableCell63
      // 
      this.xrTableCell63.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell63.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell63.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell63.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerAddressHistory.Address3")});
      this.xrTableCell63.Dpi = 254F;
      this.xrTableCell63.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell63.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell63.Name = "xrTableCell63";
      this.xrTableCell63.StylePriority.UseBackColor = false;
      this.xrTableCell63.StylePriority.UseBorderColor = false;
      this.xrTableCell63.StylePriority.UseBorders = false;
      this.xrTableCell63.StylePriority.UseFont = false;
      this.xrTableCell63.StylePriority.UseForeColor = false;
      this.xrTableCell63.StylePriority.UseTextAlignment = false;
      this.xrTableCell63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell63.Weight = 0.5714285714285714D;
      // 
      // xrTableCell64
      // 
      this.xrTableCell64.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell64.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell64.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell64.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerAddressHistory.Address4")});
      this.xrTableCell64.Dpi = 254F;
      this.xrTableCell64.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell64.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell64.Name = "xrTableCell64";
      this.xrTableCell64.StylePriority.UseBackColor = false;
      this.xrTableCell64.StylePriority.UseBorderColor = false;
      this.xrTableCell64.StylePriority.UseBorders = false;
      this.xrTableCell64.StylePriority.UseFont = false;
      this.xrTableCell64.StylePriority.UseForeColor = false;
      this.xrTableCell64.StylePriority.UseTextAlignment = false;
      this.xrTableCell64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell64.Weight = 0.5714285714285714D;
      // 
      // xrTableCell65
      // 
      this.xrTableCell65.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell65.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell65.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell65.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerAddressHistory.PostalCode")});
      this.xrTableCell65.Dpi = 254F;
      this.xrTableCell65.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell65.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell65.Name = "xrTableCell65";
      this.xrTableCell65.StylePriority.UseBackColor = false;
      this.xrTableCell65.StylePriority.UseBorderColor = false;
      this.xrTableCell65.StylePriority.UseBorders = false;
      this.xrTableCell65.StylePriority.UseFont = false;
      this.xrTableCell65.StylePriority.UseForeColor = false;
      this.xrTableCell65.StylePriority.UseTextAlignment = false;
      this.xrTableCell65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell65.Weight = 0.57142857142857117D;
      // 
      // GroupHeader3
      // 
      this.GroupHeader3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
      this.GroupHeader3.Dpi = 254F;
      this.GroupHeader3.HeightF = 145F;
      this.GroupHeader3.Name = "GroupHeader3";
      this.GroupHeader3.StylePriority.UseBorders = false;
      // 
      // xrTable1
      // 
      this.xrTable1.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTable1.Dpi = 254F;
      this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 25F);
      this.xrTable1.Name = "xrTable1";
      this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow28,
            this.xrTableRow116});
      this.xrTable1.SizeF = new System.Drawing.SizeF(2058F, 120F);
      this.xrTable1.StylePriority.UseBorders = false;
      this.xrTable1.StylePriority.UsePadding = false;
      // 
      // xrTableRow28
      // 
      this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5});
      this.xrTableRow28.Dpi = 254F;
      this.xrTableRow28.Name = "xrTableRow28";
      this.xrTableRow28.Weight = 0.3405676669451424D;
      // 
      // xrTableCell5
      // 
      this.xrTableCell5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(187)))), ((int)(((byte)(186)))));
      this.xrTableCell5.BorderColor = System.Drawing.Color.Black;
      this.xrTableCell5.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell5.BorderWidth = 0.5F;
      this.xrTableCell5.CanGrow = false;
      this.xrTableCell5.Dpi = 254F;
      this.xrTableCell5.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold);
      this.xrTableCell5.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell5.Multiline = true;
      this.xrTableCell5.Name = "xrTableCell5";
      this.xrTableCell5.StylePriority.UseBackColor = false;
      this.xrTableCell5.StylePriority.UseBorderColor = false;
      this.xrTableCell5.StylePriority.UseBorders = false;
      this.xrTableCell5.StylePriority.UseBorderWidth = false;
      this.xrTableCell5.StylePriority.UseFont = false;
      this.xrTableCell5.StylePriority.UseForeColor = false;
      this.xrTableCell5.StylePriority.UseTextAlignment = false;
      this.xrTableCell5.Text = "Party B";
      this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell5.Weight = 3.9999999999999991D;
      // 
      // xrTableRow116
      // 
      this.xrTableRow116.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrTableCell7,
            this.xrTableCell84,
            this.xrTableCell85,
            this.xrTableCell86,
            this.xrTableCell87,
            this.xrTableCell342});
      this.xrTableRow116.Dpi = 254F;
      this.xrTableRow116.Name = "xrTableRow116";
      this.xrTableRow116.Weight = 0.34056760670799885D;
      // 
      // xrTableCell6
      // 
      this.xrTableCell6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell6.BorderColor = System.Drawing.Color.White;
      this.xrTableCell6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell6.CanGrow = false;
      this.xrTableCell6.Dpi = 254F;
      this.xrTableCell6.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell6.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell6.Name = "xrTableCell6";
      this.xrTableCell6.StylePriority.UseBackColor = false;
      this.xrTableCell6.StylePriority.UseBorderColor = false;
      this.xrTableCell6.StylePriority.UseBorders = false;
      this.xrTableCell6.StylePriority.UseFont = false;
      this.xrTableCell6.StylePriority.UseForeColor = false;
      this.xrTableCell6.StylePriority.UseTextAlignment = false;
      this.xrTableCell6.Text = "Bureau Update";
      this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell6.Weight = 0.5714285714285714D;
      // 
      // xrTableCell7
      // 
      this.xrTableCell7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell7.BorderColor = System.Drawing.Color.White;
      this.xrTableCell7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell7.CanGrow = false;
      this.xrTableCell7.Dpi = 254F;
      this.xrTableCell7.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell7.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell7.Name = "xrTableCell7";
      this.xrTableCell7.StylePriority.UseBackColor = false;
      this.xrTableCell7.StylePriority.UseBorderColor = false;
      this.xrTableCell7.StylePriority.UseBorders = false;
      this.xrTableCell7.StylePriority.UseFont = false;
      this.xrTableCell7.StylePriority.UseForeColor = false;
      this.xrTableCell7.StylePriority.UseTextAlignment = false;
      this.xrTableCell7.Text = "Type";
      this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell7.Weight = 0.5714285714285714D;
      // 
      // xrTableCell84
      // 
      this.xrTableCell84.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell84.BorderColor = System.Drawing.Color.White;
      this.xrTableCell84.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell84.CanGrow = false;
      this.xrTableCell84.Dpi = 254F;
      this.xrTableCell84.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell84.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell84.Name = "xrTableCell84";
      this.xrTableCell84.StylePriority.UseBackColor = false;
      this.xrTableCell84.StylePriority.UseBorderColor = false;
      this.xrTableCell84.StylePriority.UseBorders = false;
      this.xrTableCell84.StylePriority.UseFont = false;
      this.xrTableCell84.StylePriority.UseForeColor = false;
      this.xrTableCell84.StylePriority.UseTextAlignment = false;
      this.xrTableCell84.Text = "Line 1";
      this.xrTableCell84.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell84.Weight = 0.57142857142857129D;
      // 
      // xrTableCell85
      // 
      this.xrTableCell85.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell85.BorderColor = System.Drawing.Color.White;
      this.xrTableCell85.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell85.CanGrow = false;
      this.xrTableCell85.Dpi = 254F;
      this.xrTableCell85.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell85.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell85.Name = "xrTableCell85";
      this.xrTableCell85.StylePriority.UseBackColor = false;
      this.xrTableCell85.StylePriority.UseBorderColor = false;
      this.xrTableCell85.StylePriority.UseBorders = false;
      this.xrTableCell85.StylePriority.UseFont = false;
      this.xrTableCell85.StylePriority.UseForeColor = false;
      this.xrTableCell85.StylePriority.UseTextAlignment = false;
      this.xrTableCell85.Text = "Line 2";
      this.xrTableCell85.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell85.Weight = 0.57142857142857129D;
      // 
      // xrTableCell86
      // 
      this.xrTableCell86.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell86.BorderColor = System.Drawing.Color.White;
      this.xrTableCell86.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell86.CanGrow = false;
      this.xrTableCell86.Dpi = 254F;
      this.xrTableCell86.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell86.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell86.Name = "xrTableCell86";
      this.xrTableCell86.StylePriority.UseBackColor = false;
      this.xrTableCell86.StylePriority.UseBorderColor = false;
      this.xrTableCell86.StylePriority.UseBorders = false;
      this.xrTableCell86.StylePriority.UseFont = false;
      this.xrTableCell86.StylePriority.UseForeColor = false;
      this.xrTableCell86.StylePriority.UseTextAlignment = false;
      this.xrTableCell86.Text = "Line 3";
      this.xrTableCell86.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell86.Weight = 0.5714285714285714D;
      // 
      // xrTableCell87
      // 
      this.xrTableCell87.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell87.BorderColor = System.Drawing.Color.White;
      this.xrTableCell87.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell87.CanGrow = false;
      this.xrTableCell87.Dpi = 254F;
      this.xrTableCell87.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell87.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell87.Name = "xrTableCell87";
      this.xrTableCell87.StylePriority.UseBackColor = false;
      this.xrTableCell87.StylePriority.UseBorderColor = false;
      this.xrTableCell87.StylePriority.UseBorders = false;
      this.xrTableCell87.StylePriority.UseFont = false;
      this.xrTableCell87.StylePriority.UseForeColor = false;
      this.xrTableCell87.StylePriority.UseTextAlignment = false;
      this.xrTableCell87.Text = "Line 4";
      this.xrTableCell87.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell87.Weight = 0.5714285714285714D;
      // 
      // xrTableCell342
      // 
      this.xrTableCell342.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell342.BorderColor = System.Drawing.Color.White;
      this.xrTableCell342.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell342.CanGrow = false;
      this.xrTableCell342.Dpi = 254F;
      this.xrTableCell342.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell342.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell342.Name = "xrTableCell342";
      this.xrTableCell342.StylePriority.UseBackColor = false;
      this.xrTableCell342.StylePriority.UseBorderColor = false;
      this.xrTableCell342.StylePriority.UseBorders = false;
      this.xrTableCell342.StylePriority.UseFont = false;
      this.xrTableCell342.StylePriority.UseForeColor = false;
      this.xrTableCell342.StylePriority.UseTextAlignment = false;
      this.xrTableCell342.Text = "Postal Code";
      this.xrTableCell342.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell342.Weight = 0.57142857142857117D;
      // 
      // Detail8
      // 
      this.Detail8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable11});
      this.Detail8.Dpi = 254F;
      this.Detail8.HeightF = 60F;
      this.Detail8.KeepTogether = true;
      this.Detail8.Name = "Detail8";
      // 
      // xrTable11
      // 
      this.xrTable11.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTable11.Dpi = 254F;
      this.xrTable11.KeepTogether = true;
      this.xrTable11.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
      this.xrTable11.Name = "xrTable11";
      this.xrTable11.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow67});
      this.xrTable11.SizeF = new System.Drawing.SizeF(2058F, 60F);
      this.xrTable11.StylePriority.UseBorders = false;
      this.xrTable11.StylePriority.UsePadding = false;
      // 
      // xrTableRow67
      // 
      this.xrTableRow67.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell74,
            this.xrTableCell76,
            this.xrTableCell96});
      this.xrTableRow67.Dpi = 254F;
      this.xrTableRow67.Name = "xrTableRow67";
      this.xrTableRow67.Weight = 0.94488188976377985D;
      // 
      // xrTableCell74
      // 
      this.xrTableCell74.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell74.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell74.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell74.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerTelephoneHistory.FirstReportedDate")});
      this.xrTableCell74.Dpi = 254F;
      this.xrTableCell74.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell74.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell74.Name = "xrTableCell74";
      this.xrTableCell74.StylePriority.UseBackColor = false;
      this.xrTableCell74.StylePriority.UseBorderColor = false;
      this.xrTableCell74.StylePriority.UseBorders = false;
      this.xrTableCell74.StylePriority.UseFont = false;
      this.xrTableCell74.StylePriority.UseForeColor = false;
      this.xrTableCell74.StylePriority.UseTextAlignment = false;
      this.xrTableCell74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell74.Weight = 1.333333333333333D;
      // 
      // xrTableCell76
      // 
      this.xrTableCell76.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell76.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell76.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell76.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerTelephoneHistory.TelephoneType")});
      this.xrTableCell76.Dpi = 254F;
      this.xrTableCell76.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell76.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell76.Name = "xrTableCell76";
      this.xrTableCell76.StylePriority.UseBackColor = false;
      this.xrTableCell76.StylePriority.UseBorderColor = false;
      this.xrTableCell76.StylePriority.UseBorders = false;
      this.xrTableCell76.StylePriority.UseFont = false;
      this.xrTableCell76.StylePriority.UseForeColor = false;
      this.xrTableCell76.StylePriority.UseTextAlignment = false;
      this.xrTableCell76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell76.Weight = 1.333333333333333D;
      // 
      // xrTableCell96
      // 
      this.xrTableCell96.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell96.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell96.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell96.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerTelephoneHistory.TelephoneNo")});
      this.xrTableCell96.Dpi = 254F;
      this.xrTableCell96.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell96.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell96.Name = "xrTableCell96";
      this.xrTableCell96.StylePriority.UseBackColor = false;
      this.xrTableCell96.StylePriority.UseBorderColor = false;
      this.xrTableCell96.StylePriority.UseBorders = false;
      this.xrTableCell96.StylePriority.UseFont = false;
      this.xrTableCell96.StylePriority.UseForeColor = false;
      this.xrTableCell96.StylePriority.UseTextAlignment = false;
      this.xrTableCell96.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell96.Weight = 1.333333333333333D;
      // 
      // GroupHeader4
      // 
      this.GroupHeader4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable9});
      this.GroupHeader4.Dpi = 254F;
      this.GroupHeader4.HeightF = 270F;
      this.GroupHeader4.Name = "GroupHeader4";
      // 
      // xrTable9
      // 
      this.xrTable9.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTable9.Dpi = 254F;
      this.xrTable9.LocationFloat = new DevExpress.Utils.PointFloat(0F, 25F);
      this.xrTable9.Name = "xrTable9";
      this.xrTable9.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3,
            this.xrTableRow29,
            this.xrTableRow31,
            this.xrTableRow63});
      this.xrTable9.SizeF = new System.Drawing.SizeF(2058F, 245F);
      this.xrTable9.StylePriority.UseBorders = false;
      this.xrTable9.StylePriority.UsePadding = false;
      // 
      // xrTableRow3
      // 
      this.xrTableRow3.Borders = DevExpress.XtraPrinting.BorderSide.Top;
      this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell15,
            this.xrTableCell16});
      this.xrTableRow3.Dpi = 254F;
      this.xrTableRow3.Name = "xrTableRow3";
      this.xrTableRow3.StylePriority.UseBorders = false;
      this.xrTableRow3.Weight = 1.5748031496062991D;
      // 
      // xrTableCell15
      // 
      this.xrTableCell15.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell15.BorderColor = System.Drawing.Color.Gainsboro;
      this.xrTableCell15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell15.Dpi = 254F;
      this.xrTableCell15.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
      this.xrTableCell15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
      this.xrTableCell15.Name = "xrTableCell15";
      this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTableCell15.StylePriority.UseBackColor = false;
      this.xrTableCell15.StylePriority.UseBorderColor = false;
      this.xrTableCell15.StylePriority.UseBorders = false;
      this.xrTableCell15.StylePriority.UseFont = false;
      this.xrTableCell15.StylePriority.UseForeColor = false;
      this.xrTableCell15.StylePriority.UsePadding = false;
      this.xrTableCell15.StylePriority.UseTextAlignment = false;
      this.xrTableCell15.Text = "Telephone History";
      this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell15.Weight = 2D;
      // 
      // xrTableCell16
      // 
      this.xrTableCell16.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell16.BorderColor = System.Drawing.Color.Gainsboro;
      this.xrTableCell16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell16.Dpi = 254F;
      this.xrTableCell16.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
      this.xrTableCell16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
      this.xrTableCell16.Name = "xrTableCell16";
      this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTableCell16.StylePriority.UseBackColor = false;
      this.xrTableCell16.StylePriority.UseBorderColor = false;
      this.xrTableCell16.StylePriority.UseBorders = false;
      this.xrTableCell16.StylePriority.UseFont = false;
      this.xrTableCell16.StylePriority.UseForeColor = false;
      this.xrTableCell16.StylePriority.UsePadding = false;
      this.xrTableCell16.StylePriority.UseTextAlignment = false;
      this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell16.Weight = 2D;
      // 
      // xrTableRow29
      // 
      this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell43});
      this.xrTableRow29.Dpi = 254F;
      this.xrTableRow29.Name = "xrTableRow29";
      this.xrTableRow29.Weight = 0.39370078740157477D;
      // 
      // xrTableCell43
      // 
      this.xrTableCell43.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTableCell43.Dpi = 254F;
      this.xrTableCell43.Name = "xrTableCell43";
      this.xrTableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      this.xrTableCell43.StylePriority.UseBorders = false;
      this.xrTableCell43.StylePriority.UsePadding = false;
      this.xrTableCell43.Text = "\r\n";
      this.xrTableCell43.Weight = 4D;
      // 
      // xrTableRow31
      // 
      this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell44});
      this.xrTableRow31.Dpi = 254F;
      this.xrTableRow31.Name = "xrTableRow31";
      this.xrTableRow31.Weight = 0.94488188976377985D;
      // 
      // xrTableCell44
      // 
      this.xrTableCell44.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell44.BorderColor = System.Drawing.Color.Black;
      this.xrTableCell44.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell44.Dpi = 254F;
      this.xrTableCell44.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell44.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell44.Name = "xrTableCell44";
      this.xrTableCell44.StylePriority.UseBackColor = false;
      this.xrTableCell44.StylePriority.UseBorderColor = false;
      this.xrTableCell44.StylePriority.UseBorders = false;
      this.xrTableCell44.StylePriority.UseFont = false;
      this.xrTableCell44.StylePriority.UseForeColor = false;
      this.xrTableCell44.StylePriority.UseTextAlignment = false;
      this.xrTableCell44.Text = "Contact Number History - Party A";
      this.xrTableCell44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell44.Weight = 3.9999999999999991D;
      // 
      // xrTableRow63
      // 
      this.xrTableRow63.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell45,
            this.xrTableCell46,
            this.xrTableCell66});
      this.xrTableRow63.Dpi = 254F;
      this.xrTableRow63.Name = "xrTableRow63";
      this.xrTableRow63.Weight = 0.94488188976377985D;
      // 
      // xrTableCell45
      // 
      this.xrTableCell45.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell45.BorderColor = System.Drawing.Color.White;
      this.xrTableCell45.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell45.Dpi = 254F;
      this.xrTableCell45.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell45.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell45.Name = "xrTableCell45";
      this.xrTableCell45.StylePriority.UseBackColor = false;
      this.xrTableCell45.StylePriority.UseBorderColor = false;
      this.xrTableCell45.StylePriority.UseBorders = false;
      this.xrTableCell45.StylePriority.UseFont = false;
      this.xrTableCell45.StylePriority.UseForeColor = false;
      this.xrTableCell45.StylePriority.UseTextAlignment = false;
      this.xrTableCell45.Text = "Bureau Update";
      this.xrTableCell45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell45.Weight = 1.333333333333333D;
      // 
      // xrTableCell46
      // 
      this.xrTableCell46.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell46.BorderColor = System.Drawing.Color.White;
      this.xrTableCell46.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell46.Dpi = 254F;
      this.xrTableCell46.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell46.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell46.Name = "xrTableCell46";
      this.xrTableCell46.StylePriority.UseBackColor = false;
      this.xrTableCell46.StylePriority.UseBorderColor = false;
      this.xrTableCell46.StylePriority.UseBorders = false;
      this.xrTableCell46.StylePriority.UseFont = false;
      this.xrTableCell46.StylePriority.UseForeColor = false;
      this.xrTableCell46.StylePriority.UseTextAlignment = false;
      this.xrTableCell46.Text = "Type";
      this.xrTableCell46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell46.Weight = 1.333333333333333D;
      // 
      // xrTableCell66
      // 
      this.xrTableCell66.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell66.BorderColor = System.Drawing.Color.White;
      this.xrTableCell66.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell66.Dpi = 254F;
      this.xrTableCell66.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell66.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell66.Name = "xrTableCell66";
      this.xrTableCell66.StylePriority.UseBackColor = false;
      this.xrTableCell66.StylePriority.UseBorderColor = false;
      this.xrTableCell66.StylePriority.UseBorders = false;
      this.xrTableCell66.StylePriority.UseFont = false;
      this.xrTableCell66.StylePriority.UseForeColor = false;
      this.xrTableCell66.StylePriority.UseTextAlignment = false;
      this.xrTableCell66.Text = "Telephone No.";
      this.xrTableCell66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell66.Weight = 1.333333333333333D;
      // 
      // ConsumerOtherDetail_Telephone
      // 
      this.ConsumerOtherDetail_Telephone.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail9,
            this.GroupHeader5});
      this.ConsumerOtherDetail_Telephone.DataMember = "ConsumerOther.ConsumerOther_ConsumerTelephoneHistory";
      this.ConsumerOtherDetail_Telephone.Dpi = 254F;
      this.ConsumerOtherDetail_Telephone.Level = 9;
      this.ConsumerOtherDetail_Telephone.Name = "ConsumerOtherDetail_Telephone";
      // 
      // Detail9
      // 
      this.Detail9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable12});
      this.Detail9.Dpi = 254F;
      this.Detail9.HeightF = 60F;
      this.Detail9.KeepTogether = true;
      this.Detail9.Name = "Detail9";
      // 
      // xrTable12
      // 
      this.xrTable12.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTable12.Dpi = 254F;
      this.xrTable12.KeepTogether = true;
      this.xrTable12.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
      this.xrTable12.Name = "xrTable12";
      this.xrTable12.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTable12.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow19});
      this.xrTable12.SizeF = new System.Drawing.SizeF(2058F, 60F);
      this.xrTable12.StylePriority.UseBorders = false;
      this.xrTable12.StylePriority.UsePadding = false;
      // 
      // xrTableRow19
      // 
      this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell75,
            this.xrTableCell97,
            this.xrTableCell98});
      this.xrTableRow19.Dpi = 254F;
      this.xrTableRow19.Name = "xrTableRow19";
      this.xrTableRow19.Weight = 0.94488188976377985D;
      // 
      // xrTableCell75
      // 
      this.xrTableCell75.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell75.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell75.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell75.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerTelephoneHistory.LastUpdatedDate")});
      this.xrTableCell75.Dpi = 254F;
      this.xrTableCell75.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell75.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell75.Name = "xrTableCell75";
      this.xrTableCell75.Scripts.OnBeforePrint = "xrTableCell75_BeforePrint";
      this.xrTableCell75.StylePriority.UseBackColor = false;
      this.xrTableCell75.StylePriority.UseBorderColor = false;
      this.xrTableCell75.StylePriority.UseBorders = false;
      this.xrTableCell75.StylePriority.UseFont = false;
      this.xrTableCell75.StylePriority.UseForeColor = false;
      this.xrTableCell75.StylePriority.UseTextAlignment = false;
      this.xrTableCell75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell75.Weight = 1.333333333333333D;
      // 
      // xrTableCell97
      // 
      this.xrTableCell97.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell97.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell97.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell97.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerTelephoneHistory.TelephoneType")});
      this.xrTableCell97.Dpi = 254F;
      this.xrTableCell97.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell97.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell97.Name = "xrTableCell97";
      this.xrTableCell97.StylePriority.UseBackColor = false;
      this.xrTableCell97.StylePriority.UseBorderColor = false;
      this.xrTableCell97.StylePriority.UseBorders = false;
      this.xrTableCell97.StylePriority.UseFont = false;
      this.xrTableCell97.StylePriority.UseForeColor = false;
      this.xrTableCell97.StylePriority.UseTextAlignment = false;
      this.xrTableCell97.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell97.Weight = 1.333333333333333D;
      // 
      // xrTableCell98
      // 
      this.xrTableCell98.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell98.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell98.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell98.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerTelephoneHistory.TelephoneNo")});
      this.xrTableCell98.Dpi = 254F;
      this.xrTableCell98.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell98.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell98.Name = "xrTableCell98";
      this.xrTableCell98.StylePriority.UseBackColor = false;
      this.xrTableCell98.StylePriority.UseBorderColor = false;
      this.xrTableCell98.StylePriority.UseBorders = false;
      this.xrTableCell98.StylePriority.UseFont = false;
      this.xrTableCell98.StylePriority.UseForeColor = false;
      this.xrTableCell98.StylePriority.UseTextAlignment = false;
      this.xrTableCell98.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell98.Weight = 1.333333333333333D;
      // 
      // GroupHeader5
      // 
      this.GroupHeader5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable10});
      this.GroupHeader5.Dpi = 254F;
      this.GroupHeader5.HeightF = 145F;
      this.GroupHeader5.Name = "GroupHeader5";
      // 
      // xrTable10
      // 
      this.xrTable10.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTable10.Dpi = 254F;
      this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(0F, 25F);
      this.xrTable10.Name = "xrTable10";
      this.xrTable10.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow66,
            this.xrTableRow118});
      this.xrTable10.SizeF = new System.Drawing.SizeF(2058F, 120F);
      this.xrTable10.StylePriority.UseBorders = false;
      this.xrTable10.StylePriority.UsePadding = false;
      // 
      // xrTableRow66
      // 
      this.xrTableRow66.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell71});
      this.xrTableRow66.Dpi = 254F;
      this.xrTableRow66.Name = "xrTableRow66";
      this.xrTableRow66.Weight = 0.52676344800174157D;
      // 
      // xrTableCell71
      // 
      this.xrTableCell71.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(187)))), ((int)(((byte)(186)))));
      this.xrTableCell71.BorderColor = System.Drawing.Color.Black;
      this.xrTableCell71.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell71.BorderWidth = 0.5F;
      this.xrTableCell71.CanGrow = false;
      this.xrTableCell71.Dpi = 254F;
      this.xrTableCell71.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold);
      this.xrTableCell71.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell71.Name = "xrTableCell71";
      this.xrTableCell71.StylePriority.UseBackColor = false;
      this.xrTableCell71.StylePriority.UseBorderColor = false;
      this.xrTableCell71.StylePriority.UseBorders = false;
      this.xrTableCell71.StylePriority.UseBorderWidth = false;
      this.xrTableCell71.StylePriority.UseFont = false;
      this.xrTableCell71.StylePriority.UseForeColor = false;
      this.xrTableCell71.StylePriority.UseTextAlignment = false;
      this.xrTableCell71.Text = "Party B";
      this.xrTableCell71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell71.Weight = 3.9999999999999991D;
      // 
      // xrTableRow118
      // 
      this.xrTableRow118.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell94,
            this.xrTableCell95,
            this.xrTableCell344});
      this.xrTableRow118.Dpi = 254F;
      this.xrTableRow118.Name = "xrTableRow118";
      this.xrTableRow118.Weight = 0.5267635149831813D;
      // 
      // xrTableCell94
      // 
      this.xrTableCell94.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell94.BorderColor = System.Drawing.Color.White;
      this.xrTableCell94.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell94.CanGrow = false;
      this.xrTableCell94.Dpi = 254F;
      this.xrTableCell94.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell94.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell94.Name = "xrTableCell94";
      this.xrTableCell94.StylePriority.UseBackColor = false;
      this.xrTableCell94.StylePriority.UseBorderColor = false;
      this.xrTableCell94.StylePriority.UseBorders = false;
      this.xrTableCell94.StylePriority.UseFont = false;
      this.xrTableCell94.StylePriority.UseForeColor = false;
      this.xrTableCell94.StylePriority.UseTextAlignment = false;
      this.xrTableCell94.Text = "Bureau Update";
      this.xrTableCell94.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell94.Weight = 1.333333333333333D;
      // 
      // xrTableCell95
      // 
      this.xrTableCell95.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell95.BorderColor = System.Drawing.Color.White;
      this.xrTableCell95.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell95.CanGrow = false;
      this.xrTableCell95.Dpi = 254F;
      this.xrTableCell95.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell95.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell95.Name = "xrTableCell95";
      this.xrTableCell95.StylePriority.UseBackColor = false;
      this.xrTableCell95.StylePriority.UseBorderColor = false;
      this.xrTableCell95.StylePriority.UseBorders = false;
      this.xrTableCell95.StylePriority.UseFont = false;
      this.xrTableCell95.StylePriority.UseForeColor = false;
      this.xrTableCell95.StylePriority.UseTextAlignment = false;
      this.xrTableCell95.Text = "Type";
      this.xrTableCell95.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell95.Weight = 1.333333333333333D;
      // 
      // xrTableCell344
      // 
      this.xrTableCell344.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell344.BorderColor = System.Drawing.Color.White;
      this.xrTableCell344.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell344.CanGrow = false;
      this.xrTableCell344.Dpi = 254F;
      this.xrTableCell344.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell344.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell344.Name = "xrTableCell344";
      this.xrTableCell344.StylePriority.UseBackColor = false;
      this.xrTableCell344.StylePriority.UseBorderColor = false;
      this.xrTableCell344.StylePriority.UseBorders = false;
      this.xrTableCell344.StylePriority.UseFont = false;
      this.xrTableCell344.StylePriority.UseForeColor = false;
      this.xrTableCell344.StylePriority.UseTextAlignment = false;
      this.xrTableCell344.Text = "Telephone No.";
      this.xrTableCell344.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell344.Weight = 1.333333333333333D;
      // 
      // Detail10
      // 
      this.Detail10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable15});
      this.Detail10.Dpi = 254F;
      this.Detail10.HeightF = 60F;
      this.Detail10.KeepTogether = true;
      this.Detail10.Name = "Detail10";
      // 
      // xrTable15
      // 
      this.xrTable15.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTable15.Dpi = 254F;
      this.xrTable15.KeepTogether = true;
      this.xrTable15.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
      this.xrTable15.Name = "xrTable15";
      this.xrTable15.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTable15.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow74});
      this.xrTable15.SizeF = new System.Drawing.SizeF(2058F, 60F);
      this.xrTable15.StylePriority.UseBorders = false;
      this.xrTable15.StylePriority.UsePadding = false;
      // 
      // xrTableRow74
      // 
      this.xrTableRow74.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell110,
            this.xrTableCell111,
            this.xrTableCell112});
      this.xrTableRow74.Dpi = 254F;
      this.xrTableRow74.Name = "xrTableRow74";
      this.xrTableRow74.Weight = 0.94488188976377985D;
      // 
      // xrTableCell110
      // 
      this.xrTableCell110.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell110.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell110.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell110.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerEmploymentHistory.LastUpdatedDate")});
      this.xrTableCell110.Dpi = 254F;
      this.xrTableCell110.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell110.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell110.Name = "xrTableCell110";
      this.xrTableCell110.StylePriority.UseBackColor = false;
      this.xrTableCell110.StylePriority.UseBorderColor = false;
      this.xrTableCell110.StylePriority.UseBorders = false;
      this.xrTableCell110.StylePriority.UseFont = false;
      this.xrTableCell110.StylePriority.UseForeColor = false;
      this.xrTableCell110.StylePriority.UseTextAlignment = false;
      this.xrTableCell110.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell110.Weight = 1.333333333333333D;
      // 
      // xrTableCell111
      // 
      this.xrTableCell111.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell111.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell111.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell111.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerEmploymentHistory.EmployerDetail")});
      this.xrTableCell111.Dpi = 254F;
      this.xrTableCell111.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell111.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell111.Name = "xrTableCell111";
      this.xrTableCell111.StylePriority.UseBackColor = false;
      this.xrTableCell111.StylePriority.UseBorderColor = false;
      this.xrTableCell111.StylePriority.UseBorders = false;
      this.xrTableCell111.StylePriority.UseFont = false;
      this.xrTableCell111.StylePriority.UseForeColor = false;
      this.xrTableCell111.StylePriority.UseTextAlignment = false;
      this.xrTableCell111.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell111.Weight = 1.333333333333333D;
      // 
      // xrTableCell112
      // 
      this.xrTableCell112.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell112.BorderColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell112.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell112.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerEmploymentHistory.Designation")});
      this.xrTableCell112.Dpi = 254F;
      this.xrTableCell112.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell112.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell112.Name = "xrTableCell112";
      this.xrTableCell112.StylePriority.UseBackColor = false;
      this.xrTableCell112.StylePriority.UseBorderColor = false;
      this.xrTableCell112.StylePriority.UseBorders = false;
      this.xrTableCell112.StylePriority.UseFont = false;
      this.xrTableCell112.StylePriority.UseForeColor = false;
      this.xrTableCell112.StylePriority.UseTextAlignment = false;
      this.xrTableCell112.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell112.Weight = 1.333333333333333D;
      // 
      // GroupHeader7
      // 
      this.GroupHeader7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable13});
      this.GroupHeader7.Dpi = 254F;
      this.GroupHeader7.HeightF = 270F;
      this.GroupHeader7.Name = "GroupHeader7";
      // 
      // xrTable13
      // 
      this.xrTable13.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTable13.Dpi = 254F;
      this.xrTable13.KeepTogether = true;
      this.xrTable13.LocationFloat = new DevExpress.Utils.PointFloat(0F, 25F);
      this.xrTable13.Name = "xrTable13";
      this.xrTable13.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTable13.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow18,
            this.xrTableRow68,
            this.xrTableRow69,
            this.xrTableRow70});
      this.xrTable13.SizeF = new System.Drawing.SizeF(2058F, 245F);
      this.xrTable13.StylePriority.UseBorders = false;
      this.xrTable13.StylePriority.UsePadding = false;
      // 
      // xrTableRow18
      // 
      this.xrTableRow18.Borders = DevExpress.XtraPrinting.BorderSide.Top;
      this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell69,
            this.xrTableCell72});
      this.xrTableRow18.Dpi = 254F;
      this.xrTableRow18.Name = "xrTableRow18";
      this.xrTableRow18.StylePriority.UseBorders = false;
      this.xrTableRow18.Weight = 1.5748031496062991D;
      // 
      // xrTableCell69
      // 
      this.xrTableCell69.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell69.BorderColor = System.Drawing.Color.Gainsboro;
      this.xrTableCell69.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell69.Dpi = 254F;
      this.xrTableCell69.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
      this.xrTableCell69.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
      this.xrTableCell69.Name = "xrTableCell69";
      this.xrTableCell69.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTableCell69.StylePriority.UseBackColor = false;
      this.xrTableCell69.StylePriority.UseBorderColor = false;
      this.xrTableCell69.StylePriority.UseBorders = false;
      this.xrTableCell69.StylePriority.UseFont = false;
      this.xrTableCell69.StylePriority.UseForeColor = false;
      this.xrTableCell69.StylePriority.UsePadding = false;
      this.xrTableCell69.StylePriority.UseTextAlignment = false;
      this.xrTableCell69.Text = "Employment History";
      this.xrTableCell69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell69.Weight = 2D;
      // 
      // xrTableCell72
      // 
      this.xrTableCell72.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell72.BorderColor = System.Drawing.Color.Gainsboro;
      this.xrTableCell72.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell72.Dpi = 254F;
      this.xrTableCell72.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
      this.xrTableCell72.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
      this.xrTableCell72.Name = "xrTableCell72";
      this.xrTableCell72.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTableCell72.StylePriority.UseBackColor = false;
      this.xrTableCell72.StylePriority.UseBorderColor = false;
      this.xrTableCell72.StylePriority.UseBorders = false;
      this.xrTableCell72.StylePriority.UseFont = false;
      this.xrTableCell72.StylePriority.UseForeColor = false;
      this.xrTableCell72.StylePriority.UsePadding = false;
      this.xrTableCell72.StylePriority.UseTextAlignment = false;
      this.xrTableCell72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell72.Weight = 2D;
      // 
      // xrTableRow68
      // 
      this.xrTableRow68.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell73});
      this.xrTableRow68.Dpi = 254F;
      this.xrTableRow68.Name = "xrTableRow68";
      this.xrTableRow68.Weight = 0.39370078740157477D;
      // 
      // xrTableCell73
      // 
      this.xrTableCell73.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTableCell73.Dpi = 254F;
      this.xrTableCell73.Name = "xrTableCell73";
      this.xrTableCell73.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      this.xrTableCell73.StylePriority.UseBorders = false;
      this.xrTableCell73.StylePriority.UsePadding = false;
      this.xrTableCell73.Text = "\r\n";
      this.xrTableCell73.Weight = 4D;
      // 
      // xrTableRow69
      // 
      this.xrTableRow69.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell99});
      this.xrTableRow69.Dpi = 254F;
      this.xrTableRow69.Name = "xrTableRow69";
      this.xrTableRow69.Weight = 0.94488188976377985D;
      // 
      // xrTableCell99
      // 
      this.xrTableCell99.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell99.BorderColor = System.Drawing.Color.Black;
      this.xrTableCell99.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell99.Dpi = 254F;
      this.xrTableCell99.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell99.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell99.Name = "xrTableCell99";
      this.xrTableCell99.StylePriority.UseBackColor = false;
      this.xrTableCell99.StylePriority.UseBorderColor = false;
      this.xrTableCell99.StylePriority.UseBorders = false;
      this.xrTableCell99.StylePriority.UseFont = false;
      this.xrTableCell99.StylePriority.UseForeColor = false;
      this.xrTableCell99.StylePriority.UseTextAlignment = false;
      this.xrTableCell99.Text = "Employment History - Party A";
      this.xrTableCell99.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell99.Weight = 3.9999999999999991D;
      // 
      // xrTableRow70
      // 
      this.xrTableRow70.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell100,
            this.xrTableCell101,
            this.xrTableCell102});
      this.xrTableRow70.Dpi = 254F;
      this.xrTableRow70.Name = "xrTableRow70";
      this.xrTableRow70.Weight = 0.94488188976377985D;
      // 
      // xrTableCell100
      // 
      this.xrTableCell100.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell100.BorderColor = System.Drawing.Color.White;
      this.xrTableCell100.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell100.Dpi = 254F;
      this.xrTableCell100.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell100.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell100.Name = "xrTableCell100";
      this.xrTableCell100.StylePriority.UseBackColor = false;
      this.xrTableCell100.StylePriority.UseBorderColor = false;
      this.xrTableCell100.StylePriority.UseBorders = false;
      this.xrTableCell100.StylePriority.UseFont = false;
      this.xrTableCell100.StylePriority.UseForeColor = false;
      this.xrTableCell100.StylePriority.UseTextAlignment = false;
      this.xrTableCell100.Text = "Bureau Update";
      this.xrTableCell100.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell100.Weight = 1.333333333333333D;
      // 
      // xrTableCell101
      // 
      this.xrTableCell101.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell101.BorderColor = System.Drawing.Color.White;
      this.xrTableCell101.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell101.Dpi = 254F;
      this.xrTableCell101.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell101.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell101.Name = "xrTableCell101";
      this.xrTableCell101.StylePriority.UseBackColor = false;
      this.xrTableCell101.StylePriority.UseBorderColor = false;
      this.xrTableCell101.StylePriority.UseBorders = false;
      this.xrTableCell101.StylePriority.UseFont = false;
      this.xrTableCell101.StylePriority.UseForeColor = false;
      this.xrTableCell101.StylePriority.UseTextAlignment = false;
      this.xrTableCell101.Text = "Employer";
      this.xrTableCell101.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell101.Weight = 1.333333333333333D;
      // 
      // xrTableCell102
      // 
      this.xrTableCell102.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrTableCell102.BorderColor = System.Drawing.Color.White;
      this.xrTableCell102.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell102.Dpi = 254F;
      this.xrTableCell102.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell102.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell102.Name = "xrTableCell102";
      this.xrTableCell102.StylePriority.UseBackColor = false;
      this.xrTableCell102.StylePriority.UseBorderColor = false;
      this.xrTableCell102.StylePriority.UseBorders = false;
      this.xrTableCell102.StylePriority.UseFont = false;
      this.xrTableCell102.StylePriority.UseForeColor = false;
      this.xrTableCell102.StylePriority.UseTextAlignment = false;
      this.xrTableCell102.Text = "Designation";
      this.xrTableCell102.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell102.Weight = 1.333333333333333D;
      // 
      // ConsumerOtherDetail_Employment
      // 
      this.ConsumerOtherDetail_Employment.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail11,
            this.GroupHeader6});
      this.ConsumerOtherDetail_Employment.DataMember = "ConsumerOther.ConsumerOther_ConsumerEmploymentHistory";
      this.ConsumerOtherDetail_Employment.Dpi = 254F;
      this.ConsumerOtherDetail_Employment.Level = 11;
      this.ConsumerOtherDetail_Employment.Name = "ConsumerOtherDetail_Employment";
      // 
      // Detail11
      // 
      this.Detail11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable16});
      this.Detail11.Dpi = 254F;
      this.Detail11.HeightF = 60F;
      this.Detail11.KeepTogether = true;
      this.Detail11.Name = "Detail11";
      // 
      // xrTable16
      // 
      this.xrTable16.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTable16.Dpi = 254F;
      this.xrTable16.KeepTogether = true;
      this.xrTable16.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
      this.xrTable16.Name = "xrTable16";
      this.xrTable16.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTable16.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow42});
      this.xrTable16.SizeF = new System.Drawing.SizeF(2058F, 60F);
      this.xrTable16.StylePriority.UseBorders = false;
      this.xrTable16.StylePriority.UsePadding = false;
      this.xrTable16.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTable16_BeforePrint);
      // 
      // xrTableRow42
      // 
      this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell113,
            this.xrTableCell151,
            this.xrTableCell152});
      this.xrTableRow42.Dpi = 254F;
      this.xrTableRow42.Name = "xrTableRow42";
      this.xrTableRow42.Weight = 0.94488188976377985D;
      // 
      // xrTableCell113
      // 
      this.xrTableCell113.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell113.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell113.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell113.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerEmploymentHistory.LastUpdatedDate")});
      this.xrTableCell113.Dpi = 254F;
      this.xrTableCell113.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell113.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell113.Name = "xrTableCell113";
      this.xrTableCell113.Scripts.OnBeforePrint = "xrTableCell113_BeforePrint";
      this.xrTableCell113.StylePriority.UseBackColor = false;
      this.xrTableCell113.StylePriority.UseBorderColor = false;
      this.xrTableCell113.StylePriority.UseBorders = false;
      this.xrTableCell113.StylePriority.UseFont = false;
      this.xrTableCell113.StylePriority.UseForeColor = false;
      this.xrTableCell113.StylePriority.UseTextAlignment = false;
      this.xrTableCell113.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell113.Weight = 1.333333333333333D;
      // 
      // xrTableCell151
      // 
      this.xrTableCell151.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell151.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell151.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell151.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerEmploymentHistory.EmployerDetail")});
      this.xrTableCell151.Dpi = 254F;
      this.xrTableCell151.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell151.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell151.Name = "xrTableCell151";
      this.xrTableCell151.StylePriority.UseBackColor = false;
      this.xrTableCell151.StylePriority.UseBorderColor = false;
      this.xrTableCell151.StylePriority.UseBorders = false;
      this.xrTableCell151.StylePriority.UseFont = false;
      this.xrTableCell151.StylePriority.UseForeColor = false;
      this.xrTableCell151.StylePriority.UseTextAlignment = false;
      this.xrTableCell151.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell151.Weight = 1.333333333333333D;
      // 
      // xrTableCell152
      // 
      this.xrTableCell152.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell152.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell152.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell152.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerEmploymentHistory.Designation")});
      this.xrTableCell152.Dpi = 254F;
      this.xrTableCell152.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell152.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell152.Name = "xrTableCell152";
      this.xrTableCell152.StylePriority.UseBackColor = false;
      this.xrTableCell152.StylePriority.UseBorderColor = false;
      this.xrTableCell152.StylePriority.UseBorders = false;
      this.xrTableCell152.StylePriority.UseFont = false;
      this.xrTableCell152.StylePriority.UseForeColor = false;
      this.xrTableCell152.StylePriority.UseTextAlignment = false;
      this.xrTableCell152.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell152.Weight = 1.333333333333333D;
      // 
      // GroupHeader6
      // 
      this.GroupHeader6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable14});
      this.GroupHeader6.Dpi = 254F;
      this.GroupHeader6.HeightF = 145F;
      this.GroupHeader6.KeepTogether = true;
      this.GroupHeader6.Name = "GroupHeader6";
      // 
      // xrTable14
      // 
      this.xrTable14.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTable14.Dpi = 254F;
      this.xrTable14.KeepTogether = true;
      this.xrTable14.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
      this.xrTable14.Name = "xrTable14";
      this.xrTable14.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTable14.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow71,
            this.xrTableRow72,
            this.xrTableRow73});
      this.xrTable14.SizeF = new System.Drawing.SizeF(2058F, 145F);
      this.xrTable14.StylePriority.UseBorders = false;
      this.xrTable14.StylePriority.UsePadding = false;
      // 
      // xrTableRow71
      // 
      this.xrTableRow71.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell105});
      this.xrTableRow71.Dpi = 254F;
      this.xrTableRow71.Name = "xrTableRow71";
      this.xrTableRow71.Weight = 0.39370078740157477D;
      // 
      // xrTableCell105
      // 
      this.xrTableCell105.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTableCell105.CanGrow = false;
      this.xrTableCell105.Dpi = 254F;
      this.xrTableCell105.Name = "xrTableCell105";
      this.xrTableCell105.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      this.xrTableCell105.StylePriority.UseBorders = false;
      this.xrTableCell105.StylePriority.UsePadding = false;
      this.xrTableCell105.Text = "\r\n";
      this.xrTableCell105.Weight = 4D;
      // 
      // xrTableRow72
      // 
      this.xrTableRow72.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell106});
      this.xrTableRow72.Dpi = 254F;
      this.xrTableRow72.Name = "xrTableRow72";
      this.xrTableRow72.Weight = 0.94488188976377985D;
      // 
      // xrTableCell106
      // 
      this.xrTableCell106.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(187)))), ((int)(((byte)(186)))));
      this.xrTableCell106.BorderColor = System.Drawing.Color.Black;
      this.xrTableCell106.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell106.BorderWidth = 0.5F;
      this.xrTableCell106.CanGrow = false;
      this.xrTableCell106.Dpi = 254F;
      this.xrTableCell106.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold);
      this.xrTableCell106.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell106.Name = "xrTableCell106";
      this.xrTableCell106.StylePriority.UseBackColor = false;
      this.xrTableCell106.StylePriority.UseBorderColor = false;
      this.xrTableCell106.StylePriority.UseBorders = false;
      this.xrTableCell106.StylePriority.UseBorderWidth = false;
      this.xrTableCell106.StylePriority.UseFont = false;
      this.xrTableCell106.StylePriority.UseForeColor = false;
      this.xrTableCell106.StylePriority.UseTextAlignment = false;
      this.xrTableCell106.Text = "Party B";
      this.xrTableCell106.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell106.Weight = 3.9999999999999991D;
      // 
      // xrTableRow73
      // 
      this.xrTableRow73.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell107,
            this.xrTableCell108,
            this.xrTableCell109});
      this.xrTableRow73.Dpi = 254F;
      this.xrTableRow73.Name = "xrTableRow73";
      this.xrTableRow73.Weight = 0.94488188976377985D;
      // 
      // xrTableCell107
      // 
      this.xrTableCell107.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell107.BorderColor = System.Drawing.Color.White;
      this.xrTableCell107.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell107.CanGrow = false;
      this.xrTableCell107.Dpi = 254F;
      this.xrTableCell107.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell107.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell107.Name = "xrTableCell107";
      this.xrTableCell107.StylePriority.UseBackColor = false;
      this.xrTableCell107.StylePriority.UseBorderColor = false;
      this.xrTableCell107.StylePriority.UseBorders = false;
      this.xrTableCell107.StylePriority.UseFont = false;
      this.xrTableCell107.StylePriority.UseForeColor = false;
      this.xrTableCell107.StylePriority.UseTextAlignment = false;
      this.xrTableCell107.Text = "Bureau Update";
      this.xrTableCell107.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell107.Weight = 1.333333333333333D;
      // 
      // xrTableCell108
      // 
      this.xrTableCell108.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell108.BorderColor = System.Drawing.Color.White;
      this.xrTableCell108.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell108.CanGrow = false;
      this.xrTableCell108.Dpi = 254F;
      this.xrTableCell108.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell108.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell108.Name = "xrTableCell108";
      this.xrTableCell108.StylePriority.UseBackColor = false;
      this.xrTableCell108.StylePriority.UseBorderColor = false;
      this.xrTableCell108.StylePriority.UseBorders = false;
      this.xrTableCell108.StylePriority.UseFont = false;
      this.xrTableCell108.StylePriority.UseForeColor = false;
      this.xrTableCell108.StylePriority.UseTextAlignment = false;
      this.xrTableCell108.Text = "Employer";
      this.xrTableCell108.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell108.Weight = 1.333333333333333D;
      // 
      // xrTableCell109
      // 
      this.xrTableCell109.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell109.BorderColor = System.Drawing.Color.White;
      this.xrTableCell109.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell109.CanGrow = false;
      this.xrTableCell109.Dpi = 254F;
      this.xrTableCell109.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell109.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell109.Name = "xrTableCell109";
      this.xrTableCell109.StylePriority.UseBackColor = false;
      this.xrTableCell109.StylePriority.UseBorderColor = false;
      this.xrTableCell109.StylePriority.UseBorders = false;
      this.xrTableCell109.StylePriority.UseFont = false;
      this.xrTableCell109.StylePriority.UseForeColor = false;
      this.xrTableCell109.StylePriority.UseTextAlignment = false;
      this.xrTableCell109.Text = "Designation";
      this.xrTableCell109.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell109.Weight = 1.333333333333333D;
      // 
      // ReportHeader
      // 
      this.ReportHeader.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.ReportHeader.BorderWidth = 3F;
      this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine2,
            this.xrLabel21,
            this.xrLabel22,
            this.xrPictureBox1});
      this.ReportHeader.Dpi = 254F;
      this.ReportHeader.HeightF = 350F;
      this.ReportHeader.Name = "ReportHeader";
      this.ReportHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      this.ReportHeader.StylePriority.UseBorderColor = false;
      this.ReportHeader.StylePriority.UseBorders = false;
      this.ReportHeader.StylePriority.UseBorderWidth = false;
      // 
      // xrLine2
      // 
      this.xrLine2.Dpi = 254F;
      this.xrLine2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(18)))), ((int)(((byte)(48)))));
      this.xrLine2.LineWidth = 5;
      this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 300F);
      this.xrLine2.Name = "xrLine2";
      this.xrLine2.SizeF = new System.Drawing.SizeF(2058F, 50F);
      this.xrLine2.StylePriority.UseForeColor = false;
      // 
      // xrLabel21
      // 
      this.xrLabel21.BackColor = System.Drawing.Color.White;
      this.xrLabel21.BorderColor = System.Drawing.Color.Gray;
      this.xrLabel21.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrLabel21.Dpi = 254F;
      this.xrLabel21.Font = new System.Drawing.Font("Trebuchet MS", 20F, System.Drawing.FontStyle.Bold);
      this.xrLabel21.ForeColor = System.Drawing.Color.Navy;
      this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(1059F, 0F);
      this.xrLabel21.Name = "xrLabel21";
      this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
      this.xrLabel21.SizeF = new System.Drawing.SizeF(1000F, 120F);
      this.xrLabel21.StylePriority.UseBackColor = false;
      this.xrLabel21.StylePriority.UseBorderColor = false;
      this.xrLabel21.StylePriority.UseBorders = false;
      this.xrLabel21.StylePriority.UseFont = false;
      this.xrLabel21.StylePriority.UseForeColor = false;
      this.xrLabel21.StylePriority.UseTextAlignment = false;
      this.xrLabel21.Text = "Linkage Report";
      this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
      // 
      // xrLabel22
      // 
      this.xrLabel22.BackColor = System.Drawing.Color.White;
      this.xrLabel22.BorderColor = System.Drawing.Color.Gray;
      this.xrLabel22.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrLabel22.Dpi = 254F;
      this.xrLabel22.Font = new System.Drawing.Font("Trebuchet MS", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.xrLabel22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
      this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(1059F, 120F);
      this.xrLabel22.Multiline = true;
      this.xrLabel22.Name = "xrLabel22";
      this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrLabel22.SizeF = new System.Drawing.SizeF(1000F, 170F);
      this.xrLabel22.StylePriority.UseBackColor = false;
      this.xrLabel22.StylePriority.UseBorderColor = false;
      this.xrLabel22.StylePriority.UseBorders = false;
      this.xrLabel22.StylePriority.UseFont = false;
      this.xrLabel22.StylePriority.UseForeColor = false;
      this.xrLabel22.StylePriority.UsePadding = false;
      this.xrLabel22.StylePriority.UseTextAlignment = false;
      this.xrLabel22.Text = "11-13 St. Andrews Street, \r\nOakhurst Building,Parktown, Johannesburg\r\nTel No:+27 " +
    "11 645 9100, Fax No:+27 11 484 6588\r\nwebsite:www.xds.co.za\r\nEmail : info@xds.co." +
    "za";
      this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
      // 
      // xrPictureBox1
      // 
      this.xrPictureBox1.Dpi = 254F;
      this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
      this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(20F, 25F);
      this.xrPictureBox1.Name = "xrPictureBox1";
      this.xrPictureBox1.SizeF = new System.Drawing.SizeF(290F, 230F);
      this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
      this.xrPictureBox1.StylePriority.UsePadding = false;
      // 
      // PageFooter
      // 
      this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel1});
      this.PageFooter.Dpi = 254F;
      this.PageFooter.HeightF = 86F;
      this.PageFooter.Name = "PageFooter";
      // 
      // xrPanel1
      // 
      this.xrPanel1.BackColor = System.Drawing.Color.WhiteSmoke;
      this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2,
            this.xrLabel16,
            this.xrLabel95,
            this.xrPageInfo1});
      this.xrPanel1.Dpi = 254F;
      this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 25F);
      this.xrPanel1.Name = "xrPanel1";
      this.xrPanel1.SizeF = new System.Drawing.SizeF(2058F, 61F);
      this.xrPanel1.StylePriority.UseBackColor = false;
      // 
      // xrPageInfo2
      // 
      this.xrPageInfo2.BackColor = System.Drawing.Color.Transparent;
      this.xrPageInfo2.BorderColor = System.Drawing.Color.Black;
      this.xrPageInfo2.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrPageInfo2.Dpi = 254F;
      this.xrPageInfo2.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrPageInfo2.ForeColor = System.Drawing.Color.DimGray;
      this.xrPageInfo2.Format = "{0:dddd, dd MMMM yyyy H:mm}";
      this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
      this.xrPageInfo2.LockedInUserDesigner = true;
      this.xrPageInfo2.Name = "xrPageInfo2";
      this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
      this.xrPageInfo2.SizeF = new System.Drawing.SizeF(500F, 60F);
      this.xrPageInfo2.StylePriority.UseBackColor = false;
      this.xrPageInfo2.StylePriority.UseBorderColor = false;
      this.xrPageInfo2.StylePriority.UseBorders = false;
      this.xrPageInfo2.StylePriority.UseFont = false;
      this.xrPageInfo2.StylePriority.UseForeColor = false;
      this.xrPageInfo2.StylePriority.UsePadding = false;
      this.xrPageInfo2.StylePriority.UseTextAlignment = false;
      this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      // 
      // xrLabel16
      // 
      this.xrLabel16.BackColor = System.Drawing.Color.Transparent;
      this.xrLabel16.BorderColor = System.Drawing.Color.Black;
      this.xrLabel16.Dpi = 254F;
      this.xrLabel16.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrLabel16.ForeColor = System.Drawing.Color.DimGray;
      this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(500F, 0F);
      this.xrLabel16.LockedInUserDesigner = true;
      this.xrLabel16.Name = "xrLabel16";
      this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
      this.xrLabel16.SizeF = new System.Drawing.SizeF(870F, 60F);
      this.xrLabel16.StylePriority.UseBackColor = false;
      this.xrLabel16.StylePriority.UseBorderColor = false;
      this.xrLabel16.StylePriority.UseFont = false;
      this.xrLabel16.StylePriority.UseForeColor = false;
      this.xrLabel16.StylePriority.UseTextAlignment = false;
      this.xrLabel16.Text = "Registered with the National Credit Regulator – Reg# NCR-CB5";
      this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      // 
      // xrLabel95
      // 
      this.xrLabel95.BackColor = System.Drawing.Color.Transparent;
      this.xrLabel95.BorderColor = System.Drawing.Color.Black;
      this.xrLabel95.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrLabel95.Dpi = 254F;
      this.xrLabel95.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrLabel95.ForeColor = System.Drawing.Color.DimGray;
      this.xrLabel95.LocationFloat = new DevExpress.Utils.PointFloat(1888F, 0.0001888275F);
      this.xrLabel95.LockedInUserDesigner = true;
      this.xrLabel95.Name = "xrLabel95";
      this.xrLabel95.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
      this.xrLabel95.SizeF = new System.Drawing.SizeF(100F, 60F);
      this.xrLabel95.StylePriority.UseBackColor = false;
      this.xrLabel95.StylePriority.UseBorderColor = false;
      this.xrLabel95.StylePriority.UseBorders = false;
      this.xrLabel95.StylePriority.UseFont = false;
      this.xrLabel95.StylePriority.UseForeColor = false;
      this.xrLabel95.StylePriority.UseTextAlignment = false;
      this.xrLabel95.Text = "Page:";
      this.xrLabel95.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
      // 
      // xrPageInfo1
      // 
      this.xrPageInfo1.BackColor = System.Drawing.Color.Transparent;
      this.xrPageInfo1.BorderColor = System.Drawing.Color.Black;
      this.xrPageInfo1.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrPageInfo1.Dpi = 254F;
      this.xrPageInfo1.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrPageInfo1.ForeColor = System.Drawing.Color.DimGray;
      this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(1988F, 0.0001670574F);
      this.xrPageInfo1.LockedInUserDesigner = true;
      this.xrPageInfo1.Name = "xrPageInfo1";
      this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
      this.xrPageInfo1.SizeF = new System.Drawing.SizeF(70F, 60F);
      this.xrPageInfo1.StylePriority.UseBackColor = false;
      this.xrPageInfo1.StylePriority.UseBorderColor = false;
      this.xrPageInfo1.StylePriority.UseBorders = false;
      this.xrPageInfo1.StylePriority.UseFont = false;
      this.xrPageInfo1.StylePriority.UseForeColor = false;
      this.xrPageInfo1.StylePriority.UseTextAlignment = false;
      this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      // 
      // PersonalDetail_Summary
      // 
      this.PersonalDetail_Summary.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail12,
            this.GroupHeader8});
      this.PersonalDetail_Summary.Dpi = 254F;
      this.PersonalDetail_Summary.Level = 1;
      this.PersonalDetail_Summary.Name = "PersonalDetail_Summary";
      // 
      // Detail12
      // 
      this.Detail12.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable17});
      this.Detail12.Dpi = 254F;
      this.Detail12.HeightF = 540F;
      this.Detail12.Name = "Detail12";
      // 
      // xrTable17
      // 
      this.xrTable17.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTable17.Dpi = 254F;
      this.xrTable17.KeepTogether = true;
      this.xrTable17.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
      this.xrTable17.Name = "xrTable17";
      this.xrTable17.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTable17.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow13,
            this.xrTableRow14,
            this.xrTableRow20,
            this.xrTableRow21,
            this.xrTableRow43,
            this.xrTableRow75,
            this.xrTableRow76,
            this.xrTableRow77,
            this.xrTableRow78});
      this.xrTable17.SizeF = new System.Drawing.SizeF(2058F, 540F);
      this.xrTable17.StylePriority.UseBorders = false;
      this.xrTable17.StylePriority.UsePadding = false;
      // 
      // xrTableRow13
      // 
      this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell67,
            this.xrTableCell103,
            this.xrTableCell104,
            this.xrTableCell153});
      this.xrTableRow13.Dpi = 254F;
      this.xrTableRow13.Name = "xrTableRow13";
      this.xrTableRow13.Weight = 0.94488188976377985D;
      // 
      // xrTableCell67
      // 
      this.xrTableCell67.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell67.BorderColor = System.Drawing.Color.White;
      this.xrTableCell67.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell67.Dpi = 254F;
      this.xrTableCell67.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell67.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell67.Name = "xrTableCell67";
      this.xrTableCell67.StylePriority.UseBackColor = false;
      this.xrTableCell67.StylePriority.UseBorderColor = false;
      this.xrTableCell67.StylePriority.UseBorders = false;
      this.xrTableCell67.StylePriority.UseFont = false;
      this.xrTableCell67.StylePriority.UseForeColor = false;
      this.xrTableCell67.StylePriority.UseTextAlignment = false;
      this.xrTableCell67.Text = "Reference No:";
      this.xrTableCell67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell67.Weight = 0.66666666666666674D;
      // 
      // xrTableCell103
      // 
      this.xrTableCell103.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell103.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell103.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.ReferenceNo")});
      this.xrTableCell103.Dpi = 254F;
      this.xrTableCell103.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell103.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell103.Name = "xrTableCell103";
      this.xrTableCell103.StylePriority.UseBorderColor = false;
      this.xrTableCell103.StylePriority.UseBorders = false;
      this.xrTableCell103.StylePriority.UseFont = false;
      this.xrTableCell103.StylePriority.UseForeColor = false;
      this.xrTableCell103.StylePriority.UseTextAlignment = false;
      this.xrTableCell103.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell103.Weight = 1.333333333333333D;
      // 
      // xrTableCell104
      // 
      this.xrTableCell104.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell104.BorderColor = System.Drawing.Color.White;
      this.xrTableCell104.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell104.Dpi = 254F;
      this.xrTableCell104.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell104.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell104.Name = "xrTableCell104";
      this.xrTableCell104.StylePriority.UseBackColor = false;
      this.xrTableCell104.StylePriority.UseBorderColor = false;
      this.xrTableCell104.StylePriority.UseBorders = false;
      this.xrTableCell104.StylePriority.UseFont = false;
      this.xrTableCell104.StylePriority.UseForeColor = false;
      this.xrTableCell104.StylePriority.UseTextAlignment = false;
      this.xrTableCell104.Text = "Reference No:";
      this.xrTableCell104.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell104.Weight = 0.66666666666666663D;
      // 
      // xrTableCell153
      // 
      this.xrTableCell153.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell153.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell153.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.ReferenceNo")});
      this.xrTableCell153.Dpi = 254F;
      this.xrTableCell153.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell153.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell153.Name = "xrTableCell153";
      this.xrTableCell153.StylePriority.UseBorderColor = false;
      this.xrTableCell153.StylePriority.UseBorders = false;
      this.xrTableCell153.StylePriority.UseFont = false;
      this.xrTableCell153.StylePriority.UseForeColor = false;
      this.xrTableCell153.StylePriority.UseTextAlignment = false;
      this.xrTableCell153.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell153.Weight = 1.333333333333333D;
      // 
      // xrTableRow14
      // 
      this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell154,
            this.xrTableCell155,
            this.xrTableCell156,
            this.xrTableCell157});
      this.xrTableRow14.Dpi = 254F;
      this.xrTableRow14.Name = "xrTableRow14";
      this.xrTableRow14.Weight = 0.94488188976377985D;
      // 
      // xrTableCell154
      // 
      this.xrTableCell154.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell154.BorderColor = System.Drawing.Color.White;
      this.xrTableCell154.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell154.Dpi = 254F;
      this.xrTableCell154.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell154.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell154.Name = "xrTableCell154";
      this.xrTableCell154.StylePriority.UseBackColor = false;
      this.xrTableCell154.StylePriority.UseBorderColor = false;
      this.xrTableCell154.StylePriority.UseBorders = false;
      this.xrTableCell154.StylePriority.UseFont = false;
      this.xrTableCell154.StylePriority.UseForeColor = false;
      this.xrTableCell154.StylePriority.UseTextAlignment = false;
      this.xrTableCell154.Text = "ID No:";
      this.xrTableCell154.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell154.Weight = 0.66666666666666674D;
      // 
      // xrTableCell155
      // 
      this.xrTableCell155.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell155.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell155.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.IDNo")});
      this.xrTableCell155.Dpi = 254F;
      this.xrTableCell155.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell155.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell155.Name = "xrTableCell155";
      this.xrTableCell155.StylePriority.UseBorderColor = false;
      this.xrTableCell155.StylePriority.UseBorders = false;
      this.xrTableCell155.StylePriority.UseFont = false;
      this.xrTableCell155.StylePriority.UseForeColor = false;
      this.xrTableCell155.StylePriority.UseTextAlignment = false;
      this.xrTableCell155.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell155.Weight = 1.333333333333333D;
      // 
      // xrTableCell156
      // 
      this.xrTableCell156.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell156.BorderColor = System.Drawing.Color.White;
      this.xrTableCell156.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell156.Dpi = 254F;
      this.xrTableCell156.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell156.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell156.Name = "xrTableCell156";
      this.xrTableCell156.StylePriority.UseBackColor = false;
      this.xrTableCell156.StylePriority.UseBorderColor = false;
      this.xrTableCell156.StylePriority.UseBorders = false;
      this.xrTableCell156.StylePriority.UseFont = false;
      this.xrTableCell156.StylePriority.UseForeColor = false;
      this.xrTableCell156.StylePriority.UseTextAlignment = false;
      this.xrTableCell156.Text = "ID No:";
      this.xrTableCell156.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell156.Weight = 0.66666666666666663D;
      // 
      // xrTableCell157
      // 
      this.xrTableCell157.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell157.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell157.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.IDNo")});
      this.xrTableCell157.Dpi = 254F;
      this.xrTableCell157.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell157.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell157.Name = "xrTableCell157";
      this.xrTableCell157.StylePriority.UseBorderColor = false;
      this.xrTableCell157.StylePriority.UseBorders = false;
      this.xrTableCell157.StylePriority.UseFont = false;
      this.xrTableCell157.StylePriority.UseForeColor = false;
      this.xrTableCell157.StylePriority.UseTextAlignment = false;
      this.xrTableCell157.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell157.Weight = 1.333333333333333D;
      // 
      // xrTableRow20
      // 
      this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell158,
            this.xrTableCell159,
            this.xrTableCell160,
            this.xrTableCell161});
      this.xrTableRow20.Dpi = 254F;
      this.xrTableRow20.Name = "xrTableRow20";
      this.xrTableRow20.Weight = 0.94488188976377985D;
      // 
      // xrTableCell158
      // 
      this.xrTableCell158.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell158.BorderColor = System.Drawing.Color.White;
      this.xrTableCell158.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell158.Dpi = 254F;
      this.xrTableCell158.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell158.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell158.Name = "xrTableCell158";
      this.xrTableCell158.StylePriority.UseBackColor = false;
      this.xrTableCell158.StylePriority.UseBorderColor = false;
      this.xrTableCell158.StylePriority.UseBorders = false;
      this.xrTableCell158.StylePriority.UseFont = false;
      this.xrTableCell158.StylePriority.UseForeColor = false;
      this.xrTableCell158.StylePriority.UseTextAlignment = false;
      this.xrTableCell158.Text = "Surname:";
      this.xrTableCell158.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell158.Weight = 0.66666666666666674D;
      // 
      // xrTableCell159
      // 
      this.xrTableCell159.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell159.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell159.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.Surname")});
      this.xrTableCell159.Dpi = 254F;
      this.xrTableCell159.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell159.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell159.Name = "xrTableCell159";
      this.xrTableCell159.StylePriority.UseBorderColor = false;
      this.xrTableCell159.StylePriority.UseBorders = false;
      this.xrTableCell159.StylePriority.UseFont = false;
      this.xrTableCell159.StylePriority.UseForeColor = false;
      this.xrTableCell159.StylePriority.UseTextAlignment = false;
      this.xrTableCell159.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell159.Weight = 1.333333333333333D;
      // 
      // xrTableCell160
      // 
      this.xrTableCell160.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell160.BorderColor = System.Drawing.Color.White;
      this.xrTableCell160.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell160.Dpi = 254F;
      this.xrTableCell160.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell160.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell160.Name = "xrTableCell160";
      this.xrTableCell160.StylePriority.UseBackColor = false;
      this.xrTableCell160.StylePriority.UseBorderColor = false;
      this.xrTableCell160.StylePriority.UseBorders = false;
      this.xrTableCell160.StylePriority.UseFont = false;
      this.xrTableCell160.StylePriority.UseForeColor = false;
      this.xrTableCell160.StylePriority.UseTextAlignment = false;
      this.xrTableCell160.Text = "Surname:";
      this.xrTableCell160.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell160.Weight = 0.66666666666666663D;
      // 
      // xrTableCell161
      // 
      this.xrTableCell161.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell161.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell161.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.Surname")});
      this.xrTableCell161.Dpi = 254F;
      this.xrTableCell161.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell161.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell161.Name = "xrTableCell161";
      this.xrTableCell161.StylePriority.UseBorderColor = false;
      this.xrTableCell161.StylePriority.UseBorders = false;
      this.xrTableCell161.StylePriority.UseFont = false;
      this.xrTableCell161.StylePriority.UseForeColor = false;
      this.xrTableCell161.StylePriority.UseTextAlignment = false;
      this.xrTableCell161.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell161.Weight = 1.333333333333333D;
      // 
      // xrTableRow21
      // 
      this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell162,
            this.xrTableCell163,
            this.xrTableCell164,
            this.xrTableCell165});
      this.xrTableRow21.Dpi = 254F;
      this.xrTableRow21.Name = "xrTableRow21";
      this.xrTableRow21.Weight = 0.94488188976377985D;
      // 
      // xrTableCell162
      // 
      this.xrTableCell162.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell162.BorderColor = System.Drawing.Color.White;
      this.xrTableCell162.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell162.Dpi = 254F;
      this.xrTableCell162.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell162.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell162.Name = "xrTableCell162";
      this.xrTableCell162.StylePriority.UseBackColor = false;
      this.xrTableCell162.StylePriority.UseBorderColor = false;
      this.xrTableCell162.StylePriority.UseBorders = false;
      this.xrTableCell162.StylePriority.UseFont = false;
      this.xrTableCell162.StylePriority.UseForeColor = false;
      this.xrTableCell162.StylePriority.UseTextAlignment = false;
      this.xrTableCell162.Text = "First Name:";
      this.xrTableCell162.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell162.Weight = 0.66666666666666674D;
      // 
      // xrTableCell163
      // 
      this.xrTableCell163.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell163.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell163.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.FirstName")});
      this.xrTableCell163.Dpi = 254F;
      this.xrTableCell163.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell163.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell163.Name = "xrTableCell163";
      this.xrTableCell163.StylePriority.UseBorderColor = false;
      this.xrTableCell163.StylePriority.UseBorders = false;
      this.xrTableCell163.StylePriority.UseFont = false;
      this.xrTableCell163.StylePriority.UseForeColor = false;
      this.xrTableCell163.StylePriority.UseTextAlignment = false;
      this.xrTableCell163.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell163.Weight = 1.333333333333333D;
      // 
      // xrTableCell164
      // 
      this.xrTableCell164.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell164.BorderColor = System.Drawing.Color.White;
      this.xrTableCell164.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell164.Dpi = 254F;
      this.xrTableCell164.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell164.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell164.Name = "xrTableCell164";
      this.xrTableCell164.StylePriority.UseBackColor = false;
      this.xrTableCell164.StylePriority.UseBorderColor = false;
      this.xrTableCell164.StylePriority.UseBorders = false;
      this.xrTableCell164.StylePriority.UseFont = false;
      this.xrTableCell164.StylePriority.UseForeColor = false;
      this.xrTableCell164.StylePriority.UseTextAlignment = false;
      this.xrTableCell164.Text = "First Name:";
      this.xrTableCell164.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell164.Weight = 0.66666666666666663D;
      // 
      // xrTableCell165
      // 
      this.xrTableCell165.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell165.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell165.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.FirstName")});
      this.xrTableCell165.Dpi = 254F;
      this.xrTableCell165.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell165.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell165.Name = "xrTableCell165";
      this.xrTableCell165.StylePriority.UseBorderColor = false;
      this.xrTableCell165.StylePriority.UseBorders = false;
      this.xrTableCell165.StylePriority.UseFont = false;
      this.xrTableCell165.StylePriority.UseForeColor = false;
      this.xrTableCell165.StylePriority.UseTextAlignment = false;
      this.xrTableCell165.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell165.Weight = 1.333333333333333D;
      // 
      // xrTableRow43
      // 
      this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell209,
            this.xrTableCell210,
            this.xrTableCell211,
            this.xrTableCell212});
      this.xrTableRow43.Dpi = 254F;
      this.xrTableRow43.Name = "xrTableRow43";
      this.xrTableRow43.Weight = 0.94488188976377985D;
      // 
      // xrTableCell209
      // 
      this.xrTableCell209.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell209.BorderColor = System.Drawing.Color.White;
      this.xrTableCell209.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell209.Dpi = 254F;
      this.xrTableCell209.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell209.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell209.Name = "xrTableCell209";
      this.xrTableCell209.StylePriority.UseBackColor = false;
      this.xrTableCell209.StylePriority.UseBorderColor = false;
      this.xrTableCell209.StylePriority.UseBorders = false;
      this.xrTableCell209.StylePriority.UseFont = false;
      this.xrTableCell209.StylePriority.UseForeColor = false;
      this.xrTableCell209.StylePriority.UseTextAlignment = false;
      this.xrTableCell209.Text = "Second Name:";
      this.xrTableCell209.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell209.Weight = 0.66666666666666674D;
      // 
      // xrTableCell210
      // 
      this.xrTableCell210.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell210.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell210.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.SecondName")});
      this.xrTableCell210.Dpi = 254F;
      this.xrTableCell210.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell210.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell210.Name = "xrTableCell210";
      this.xrTableCell210.StylePriority.UseBorderColor = false;
      this.xrTableCell210.StylePriority.UseBorders = false;
      this.xrTableCell210.StylePriority.UseFont = false;
      this.xrTableCell210.StylePriority.UseForeColor = false;
      this.xrTableCell210.StylePriority.UseTextAlignment = false;
      this.xrTableCell210.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell210.Weight = 1.333333333333333D;
      // 
      // xrTableCell211
      // 
      this.xrTableCell211.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell211.BorderColor = System.Drawing.Color.White;
      this.xrTableCell211.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell211.Dpi = 254F;
      this.xrTableCell211.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell211.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell211.Name = "xrTableCell211";
      this.xrTableCell211.StylePriority.UseBackColor = false;
      this.xrTableCell211.StylePriority.UseBorderColor = false;
      this.xrTableCell211.StylePriority.UseBorders = false;
      this.xrTableCell211.StylePriority.UseFont = false;
      this.xrTableCell211.StylePriority.UseForeColor = false;
      this.xrTableCell211.StylePriority.UseTextAlignment = false;
      this.xrTableCell211.Text = "Second Name:";
      this.xrTableCell211.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell211.Weight = 0.66666666666666663D;
      // 
      // xrTableCell212
      // 
      this.xrTableCell212.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell212.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell212.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.SecondName")});
      this.xrTableCell212.Dpi = 254F;
      this.xrTableCell212.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell212.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell212.Name = "xrTableCell212";
      this.xrTableCell212.StylePriority.UseBorderColor = false;
      this.xrTableCell212.StylePriority.UseBorders = false;
      this.xrTableCell212.StylePriority.UseFont = false;
      this.xrTableCell212.StylePriority.UseForeColor = false;
      this.xrTableCell212.StylePriority.UseTextAlignment = false;
      this.xrTableCell212.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell212.Weight = 1.333333333333333D;
      // 
      // xrTableRow75
      // 
      this.xrTableRow75.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell213,
            this.xrTableCell214,
            this.xrTableCell215,
            this.xrTableCell216});
      this.xrTableRow75.Dpi = 254F;
      this.xrTableRow75.Name = "xrTableRow75";
      this.xrTableRow75.Weight = 0.94488188976377985D;
      // 
      // xrTableCell213
      // 
      this.xrTableCell213.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell213.BorderColor = System.Drawing.Color.White;
      this.xrTableCell213.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell213.Dpi = 254F;
      this.xrTableCell213.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell213.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell213.Name = "xrTableCell213";
      this.xrTableCell213.StylePriority.UseBackColor = false;
      this.xrTableCell213.StylePriority.UseBorderColor = false;
      this.xrTableCell213.StylePriority.UseBorders = false;
      this.xrTableCell213.StylePriority.UseFont = false;
      this.xrTableCell213.StylePriority.UseForeColor = false;
      this.xrTableCell213.StylePriority.UseTextAlignment = false;
      this.xrTableCell213.Text = "Title:";
      this.xrTableCell213.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell213.Weight = 0.66666666666666674D;
      // 
      // xrTableCell214
      // 
      this.xrTableCell214.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell214.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell214.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.TitleDesc")});
      this.xrTableCell214.Dpi = 254F;
      this.xrTableCell214.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell214.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell214.Name = "xrTableCell214";
      this.xrTableCell214.StylePriority.UseBorderColor = false;
      this.xrTableCell214.StylePriority.UseBorders = false;
      this.xrTableCell214.StylePriority.UseFont = false;
      this.xrTableCell214.StylePriority.UseForeColor = false;
      this.xrTableCell214.StylePriority.UseTextAlignment = false;
      this.xrTableCell214.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell214.Weight = 1.333333333333333D;
      // 
      // xrTableCell215
      // 
      this.xrTableCell215.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell215.BorderColor = System.Drawing.Color.White;
      this.xrTableCell215.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell215.Dpi = 254F;
      this.xrTableCell215.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell215.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell215.Name = "xrTableCell215";
      this.xrTableCell215.StylePriority.UseBackColor = false;
      this.xrTableCell215.StylePriority.UseBorderColor = false;
      this.xrTableCell215.StylePriority.UseBorders = false;
      this.xrTableCell215.StylePriority.UseFont = false;
      this.xrTableCell215.StylePriority.UseForeColor = false;
      this.xrTableCell215.StylePriority.UseTextAlignment = false;
      this.xrTableCell215.Text = "Title:";
      this.xrTableCell215.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell215.Weight = 0.66666666666666663D;
      // 
      // xrTableCell216
      // 
      this.xrTableCell216.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell216.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell216.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.TitleDesc")});
      this.xrTableCell216.Dpi = 254F;
      this.xrTableCell216.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell216.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell216.Name = "xrTableCell216";
      this.xrTableCell216.StylePriority.UseBorderColor = false;
      this.xrTableCell216.StylePriority.UseBorders = false;
      this.xrTableCell216.StylePriority.UseFont = false;
      this.xrTableCell216.StylePriority.UseForeColor = false;
      this.xrTableCell216.StylePriority.UseTextAlignment = false;
      this.xrTableCell216.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell216.Weight = 1.333333333333333D;
      // 
      // xrTableRow76
      // 
      this.xrTableRow76.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell217,
            this.xrTableCell218,
            this.xrTableCell219,
            this.xrTableCell220});
      this.xrTableRow76.Dpi = 254F;
      this.xrTableRow76.Name = "xrTableRow76";
      this.xrTableRow76.Weight = 0.94488188976377985D;
      // 
      // xrTableCell217
      // 
      this.xrTableCell217.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell217.BorderColor = System.Drawing.Color.White;
      this.xrTableCell217.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell217.Dpi = 254F;
      this.xrTableCell217.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell217.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell217.Name = "xrTableCell217";
      this.xrTableCell217.StylePriority.UseBackColor = false;
      this.xrTableCell217.StylePriority.UseBorderColor = false;
      this.xrTableCell217.StylePriority.UseBorders = false;
      this.xrTableCell217.StylePriority.UseFont = false;
      this.xrTableCell217.StylePriority.UseForeColor = false;
      this.xrTableCell217.StylePriority.UseTextAlignment = false;
      this.xrTableCell217.Text = "Gender:";
      this.xrTableCell217.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell217.Weight = 0.66666666666666674D;
      // 
      // xrTableCell218
      // 
      this.xrTableCell218.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell218.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell218.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.Gender")});
      this.xrTableCell218.Dpi = 254F;
      this.xrTableCell218.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell218.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell218.Name = "xrTableCell218";
      this.xrTableCell218.StylePriority.UseBorderColor = false;
      this.xrTableCell218.StylePriority.UseBorders = false;
      this.xrTableCell218.StylePriority.UseFont = false;
      this.xrTableCell218.StylePriority.UseForeColor = false;
      this.xrTableCell218.StylePriority.UseTextAlignment = false;
      this.xrTableCell218.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell218.Weight = 1.333333333333333D;
      // 
      // xrTableCell219
      // 
      this.xrTableCell219.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell219.BorderColor = System.Drawing.Color.White;
      this.xrTableCell219.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell219.Dpi = 254F;
      this.xrTableCell219.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell219.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell219.Multiline = true;
      this.xrTableCell219.Name = "xrTableCell219";
      this.xrTableCell219.StylePriority.UseBackColor = false;
      this.xrTableCell219.StylePriority.UseBorderColor = false;
      this.xrTableCell219.StylePriority.UseBorders = false;
      this.xrTableCell219.StylePriority.UseFont = false;
      this.xrTableCell219.StylePriority.UseForeColor = false;
      this.xrTableCell219.StylePriority.UseTextAlignment = false;
      this.xrTableCell219.Text = "Gender:";
      this.xrTableCell219.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell219.Weight = 0.66666666666666663D;
      // 
      // xrTableCell220
      // 
      this.xrTableCell220.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell220.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell220.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.Gender")});
      this.xrTableCell220.Dpi = 254F;
      this.xrTableCell220.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell220.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell220.Name = "xrTableCell220";
      this.xrTableCell220.StylePriority.UseBorderColor = false;
      this.xrTableCell220.StylePriority.UseBorders = false;
      this.xrTableCell220.StylePriority.UseFont = false;
      this.xrTableCell220.StylePriority.UseForeColor = false;
      this.xrTableCell220.StylePriority.UseTextAlignment = false;
      this.xrTableCell220.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell220.Weight = 1.333333333333333D;
      // 
      // xrTableRow77
      // 
      this.xrTableRow77.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell221,
            this.xrTableCell222,
            this.xrTableCell223,
            this.xrTableCell224});
      this.xrTableRow77.Dpi = 254F;
      this.xrTableRow77.Name = "xrTableRow77";
      this.xrTableRow77.Weight = 0.94488188976377985D;
      // 
      // xrTableCell221
      // 
      this.xrTableCell221.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell221.BorderColor = System.Drawing.Color.White;
      this.xrTableCell221.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell221.Dpi = 254F;
      this.xrTableCell221.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell221.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell221.Name = "xrTableCell221";
      this.xrTableCell221.StylePriority.UseBackColor = false;
      this.xrTableCell221.StylePriority.UseBorderColor = false;
      this.xrTableCell221.StylePriority.UseBorders = false;
      this.xrTableCell221.StylePriority.UseFont = false;
      this.xrTableCell221.StylePriority.UseForeColor = false;
      this.xrTableCell221.StylePriority.UseTextAlignment = false;
      this.xrTableCell221.Text = "Date of Birth:";
      this.xrTableCell221.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell221.Weight = 0.66666666666666674D;
      // 
      // xrTableCell222
      // 
      this.xrTableCell222.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell222.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell222.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.BirthDate")});
      this.xrTableCell222.Dpi = 254F;
      this.xrTableCell222.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell222.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell222.Name = "xrTableCell222";
      this.xrTableCell222.Scripts.OnBeforePrint = "xrTableCell222_BeforePrint";
      this.xrTableCell222.StylePriority.UseBorderColor = false;
      this.xrTableCell222.StylePriority.UseBorders = false;
      this.xrTableCell222.StylePriority.UseFont = false;
      this.xrTableCell222.StylePriority.UseForeColor = false;
      this.xrTableCell222.StylePriority.UseTextAlignment = false;
      this.xrTableCell222.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell222.Weight = 1.333333333333333D;
      // 
      // xrTableCell223
      // 
      this.xrTableCell223.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell223.BorderColor = System.Drawing.Color.White;
      this.xrTableCell223.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell223.Dpi = 254F;
      this.xrTableCell223.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell223.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell223.Name = "xrTableCell223";
      this.xrTableCell223.StylePriority.UseBackColor = false;
      this.xrTableCell223.StylePriority.UseBorderColor = false;
      this.xrTableCell223.StylePriority.UseBorders = false;
      this.xrTableCell223.StylePriority.UseFont = false;
      this.xrTableCell223.StylePriority.UseForeColor = false;
      this.xrTableCell223.StylePriority.UseTextAlignment = false;
      this.xrTableCell223.Text = "Date of Birth:";
      this.xrTableCell223.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell223.Weight = 0.66666666666666663D;
      // 
      // xrTableCell224
      // 
      this.xrTableCell224.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell224.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell224.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.BirthDate")});
      this.xrTableCell224.Dpi = 254F;
      this.xrTableCell224.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell224.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell224.Name = "xrTableCell224";
      this.xrTableCell224.Scripts.OnBeforePrint = "xrTableCell224_BeforePrint";
      this.xrTableCell224.StylePriority.UseBorderColor = false;
      this.xrTableCell224.StylePriority.UseBorders = false;
      this.xrTableCell224.StylePriority.UseFont = false;
      this.xrTableCell224.StylePriority.UseForeColor = false;
      this.xrTableCell224.StylePriority.UseTextAlignment = false;
      this.xrTableCell224.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell224.Weight = 1.333333333333333D;
      // 
      // xrTableRow78
      // 
      this.xrTableRow78.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell225,
            this.xrTableCell226,
            this.xrTableCell227,
            this.xrTableCell228});
      this.xrTableRow78.Dpi = 254F;
      this.xrTableRow78.Name = "xrTableRow78";
      this.xrTableRow78.Weight = 0.94488188976377985D;
      // 
      // xrTableCell225
      // 
      this.xrTableCell225.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell225.BorderColor = System.Drawing.Color.White;
      this.xrTableCell225.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell225.Dpi = 254F;
      this.xrTableCell225.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell225.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell225.Name = "xrTableCell225";
      this.xrTableCell225.StylePriority.UseBackColor = false;
      this.xrTableCell225.StylePriority.UseBorderColor = false;
      this.xrTableCell225.StylePriority.UseBorders = false;
      this.xrTableCell225.StylePriority.UseFont = false;
      this.xrTableCell225.StylePriority.UseForeColor = false;
      this.xrTableCell225.StylePriority.UseTextAlignment = false;
      this.xrTableCell225.Text = "Marital Status:";
      this.xrTableCell225.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell225.Weight = 0.66666666666666674D;
      // 
      // xrTableCell226
      // 
      this.xrTableCell226.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell226.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell226.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.MaritalStatusDesc")});
      this.xrTableCell226.Dpi = 254F;
      this.xrTableCell226.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell226.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell226.Name = "xrTableCell226";
      this.xrTableCell226.StylePriority.UseBorderColor = false;
      this.xrTableCell226.StylePriority.UseBorders = false;
      this.xrTableCell226.StylePriority.UseFont = false;
      this.xrTableCell226.StylePriority.UseForeColor = false;
      this.xrTableCell226.StylePriority.UseTextAlignment = false;
      this.xrTableCell226.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell226.Weight = 1.333333333333333D;
      // 
      // xrTableCell227
      // 
      this.xrTableCell227.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell227.BorderColor = System.Drawing.Color.White;
      this.xrTableCell227.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell227.Dpi = 254F;
      this.xrTableCell227.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell227.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell227.Name = "xrTableCell227";
      this.xrTableCell227.StylePriority.UseBackColor = false;
      this.xrTableCell227.StylePriority.UseBorderColor = false;
      this.xrTableCell227.StylePriority.UseBorders = false;
      this.xrTableCell227.StylePriority.UseFont = false;
      this.xrTableCell227.StylePriority.UseForeColor = false;
      this.xrTableCell227.StylePriority.UseTextAlignment = false;
      this.xrTableCell227.Text = "Marital Status:";
      this.xrTableCell227.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell227.Weight = 0.66666666666666663D;
      // 
      // xrTableCell228
      // 
      this.xrTableCell228.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell228.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell228.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.MaritalStatusDesc")});
      this.xrTableCell228.Dpi = 254F;
      this.xrTableCell228.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell228.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell228.Name = "xrTableCell228";
      this.xrTableCell228.StylePriority.UseBorderColor = false;
      this.xrTableCell228.StylePriority.UseBorders = false;
      this.xrTableCell228.StylePriority.UseFont = false;
      this.xrTableCell228.StylePriority.UseForeColor = false;
      this.xrTableCell228.StylePriority.UseTextAlignment = false;
      this.xrTableCell228.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell228.Weight = 1.333333333333333D;
      // 
      // GroupHeader8
      // 
      this.GroupHeader8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable18});
      this.GroupHeader8.Dpi = 254F;
      this.GroupHeader8.HeightF = 185F;
      this.GroupHeader8.Name = "GroupHeader8";
      // 
      // xrTable18
      // 
      this.xrTable18.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTable18.Dpi = 254F;
      this.xrTable18.KeepTogether = true;
      this.xrTable18.LocationFloat = new DevExpress.Utils.PointFloat(0F, 25F);
      this.xrTable18.Name = "xrTable18";
      this.xrTable18.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTable18.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow79,
            this.xrTableRow81});
      this.xrTable18.SizeF = new System.Drawing.SizeF(2058F, 160F);
      this.xrTable18.StylePriority.UseBorders = false;
      this.xrTable18.StylePriority.UsePadding = false;
      // 
      // xrTableRow79
      // 
      this.xrTableRow79.Borders = DevExpress.XtraPrinting.BorderSide.Top;
      this.xrTableRow79.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell229});
      this.xrTableRow79.Dpi = 254F;
      this.xrTableRow79.Name = "xrTableRow79";
      this.xrTableRow79.StylePriority.UseBorders = false;
      this.xrTableRow79.Weight = 1.5748031496062991D;
      // 
      // xrTableCell229
      // 
      this.xrTableCell229.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell229.BorderColor = System.Drawing.Color.Gainsboro;
      this.xrTableCell229.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell229.CanGrow = false;
      this.xrTableCell229.Dpi = 254F;
      this.xrTableCell229.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
      this.xrTableCell229.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
      this.xrTableCell229.Name = "xrTableCell229";
      this.xrTableCell229.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTableCell229.StylePriority.UseBackColor = false;
      this.xrTableCell229.StylePriority.UseBorderColor = false;
      this.xrTableCell229.StylePriority.UseBorders = false;
      this.xrTableCell229.StylePriority.UseFont = false;
      this.xrTableCell229.StylePriority.UseForeColor = false;
      this.xrTableCell229.StylePriority.UsePadding = false;
      this.xrTableCell229.StylePriority.UseTextAlignment = false;
      this.xrTableCell229.Text = "Personal Details Summary";
      this.xrTableCell229.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell229.Weight = 4D;
      // 
      // xrTableRow81
      // 
      this.xrTableRow81.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell233,
            this.xrTableCell234});
      this.xrTableRow81.Dpi = 254F;
      this.xrTableRow81.Name = "xrTableRow81";
      this.xrTableRow81.Weight = 0.94488188976377985D;
      // 
      // xrTableCell233
      // 
      this.xrTableCell233.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(187)))), ((int)(((byte)(186)))));
      this.xrTableCell233.BorderColor = System.Drawing.Color.Black;
      this.xrTableCell233.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell233.BorderWidth = 0.5F;
      this.xrTableCell233.CanGrow = false;
      this.xrTableCell233.Dpi = 254F;
      this.xrTableCell233.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold);
      this.xrTableCell233.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell233.Name = "xrTableCell233";
      this.xrTableCell233.StylePriority.UseBackColor = false;
      this.xrTableCell233.StylePriority.UseBorderColor = false;
      this.xrTableCell233.StylePriority.UseBorders = false;
      this.xrTableCell233.StylePriority.UseBorderWidth = false;
      this.xrTableCell233.StylePriority.UseFont = false;
      this.xrTableCell233.StylePriority.UseForeColor = false;
      this.xrTableCell233.StylePriority.UseTextAlignment = false;
      this.xrTableCell233.Text = "Party A";
      this.xrTableCell233.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell233.Weight = 1.9999999999999998D;
      // 
      // xrTableCell234
      // 
      this.xrTableCell234.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(187)))), ((int)(((byte)(186)))));
      this.xrTableCell234.BorderColor = System.Drawing.Color.Black;
      this.xrTableCell234.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell234.BorderWidth = 0.5F;
      this.xrTableCell234.CanGrow = false;
      this.xrTableCell234.Dpi = 254F;
      this.xrTableCell234.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold);
      this.xrTableCell234.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell234.Name = "xrTableCell234";
      this.xrTableCell234.StylePriority.UseBackColor = false;
      this.xrTableCell234.StylePriority.UseBorderColor = false;
      this.xrTableCell234.StylePriority.UseBorders = false;
      this.xrTableCell234.StylePriority.UseBorderWidth = false;
      this.xrTableCell234.StylePriority.UseFont = false;
      this.xrTableCell234.StylePriority.UseForeColor = false;
      this.xrTableCell234.StylePriority.UseTextAlignment = false;
      this.xrTableCell234.Text = "Party B";
      this.xrTableCell234.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell234.Weight = 1.9999999999999996D;
      // 
      // PersonalDetails_Details
      // 
      this.PersonalDetails_Details.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail13,
            this.ReportHeader1});
      this.PersonalDetails_Details.Dpi = 254F;
      this.PersonalDetails_Details.Level = 5;
      this.PersonalDetails_Details.Name = "PersonalDetails_Details";
      this.PersonalDetails_Details.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand;
      // 
      // Detail13
      // 
      this.Detail13.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable21});
      this.Detail13.Dpi = 254F;
      this.Detail13.HeightF = 1225F;
      this.Detail13.Name = "Detail13";
      // 
      // xrTable21
      // 
      this.xrTable21.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTable21.Dpi = 254F;
      this.xrTable21.KeepTogether = true;
      this.xrTable21.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
      this.xrTable21.Name = "xrTable21";
      this.xrTable21.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTable21.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow82,
            this.xrTableRow83,
            this.xrTableRow84,
            this.xrTableRow85,
            this.xrTableRow86,
            this.xrTableRow87,
            this.xrTableRow88,
            this.xrTableRow89,
            this.xrTableRow90,
            this.xrTableRow102,
            this.xrTableRow2,
            this.xrTableRow96,
            this.xrTableRow100,
            this.xrTableRow99,
            this.xrTableRow98,
            this.xrTableRow97,
            this.xrTableRow95,
            this.xrTableRow94,
            this.xrTableRow93,
            this.xrTableRow101});
      this.xrTable21.SizeF = new System.Drawing.SizeF(2058F, 1225F);
      this.xrTable21.StylePriority.UseBorders = false;
      this.xrTable21.StylePriority.UsePadding = false;
      // 
      // xrTableRow1
      // 
      this.xrTableRow1.Borders = DevExpress.XtraPrinting.BorderSide.Top;
      this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8});
      this.xrTableRow1.Dpi = 254F;
      this.xrTableRow1.Name = "xrTableRow1";
      this.xrTableRow1.StylePriority.UseBorders = false;
      this.xrTableRow1.Weight = 0.94488188976377929D;
      // 
      // xrTableCell8
      // 
      this.xrTableCell8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(187)))), ((int)(((byte)(186)))));
      this.xrTableCell8.BorderColor = System.Drawing.Color.Black;
      this.xrTableCell8.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell8.BorderWidth = 0.5F;
      this.xrTableCell8.CanGrow = false;
      this.xrTableCell8.Dpi = 254F;
      this.xrTableCell8.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold);
      this.xrTableCell8.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell8.Name = "xrTableCell8";
      this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTableCell8.StylePriority.UseBackColor = false;
      this.xrTableCell8.StylePriority.UseBorderColor = false;
      this.xrTableCell8.StylePriority.UseBorders = false;
      this.xrTableCell8.StylePriority.UseBorderWidth = false;
      this.xrTableCell8.StylePriority.UseFont = false;
      this.xrTableCell8.StylePriority.UseForeColor = false;
      this.xrTableCell8.StylePriority.UsePadding = false;
      this.xrTableCell8.StylePriority.UseTextAlignment = false;
      this.xrTableCell8.Text = "Party A";
      this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell8.Weight = 4D;
      // 
      // xrTableRow82
      // 
      this.xrTableRow82.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell232,
            this.xrTableCell235,
            this.xrTableCell236,
            this.xrTableCell237});
      this.xrTableRow82.Dpi = 254F;
      this.xrTableRow82.Name = "xrTableRow82";
      this.xrTableRow82.Weight = 0.94488188976377985D;
      // 
      // xrTableCell232
      // 
      this.xrTableCell232.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell232.BorderColor = System.Drawing.Color.White;
      this.xrTableCell232.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell232.Dpi = 254F;
      this.xrTableCell232.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell232.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell232.Name = "xrTableCell232";
      this.xrTableCell232.StylePriority.UseBackColor = false;
      this.xrTableCell232.StylePriority.UseBorderColor = false;
      this.xrTableCell232.StylePriority.UseBorders = false;
      this.xrTableCell232.StylePriority.UseFont = false;
      this.xrTableCell232.StylePriority.UseForeColor = false;
      this.xrTableCell232.StylePriority.UseTextAlignment = false;
      this.xrTableCell232.Text = "Reference No:";
      this.xrTableCell232.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell232.Weight = 0.66666666666666674D;
      // 
      // xrTableCell235
      // 
      this.xrTableCell235.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell235.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell235.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.ReferenceNo")});
      this.xrTableCell235.Dpi = 254F;
      this.xrTableCell235.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell235.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell235.Name = "xrTableCell235";
      this.xrTableCell235.StylePriority.UseBorderColor = false;
      this.xrTableCell235.StylePriority.UseBorders = false;
      this.xrTableCell235.StylePriority.UseFont = false;
      this.xrTableCell235.StylePriority.UseForeColor = false;
      this.xrTableCell235.StylePriority.UseTextAlignment = false;
      this.xrTableCell235.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell235.Weight = 1.333333333333333D;
      // 
      // xrTableCell236
      // 
      this.xrTableCell236.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell236.BorderColor = System.Drawing.Color.White;
      this.xrTableCell236.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell236.Dpi = 254F;
      this.xrTableCell236.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell236.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell236.Name = "xrTableCell236";
      this.xrTableCell236.StylePriority.UseBackColor = false;
      this.xrTableCell236.StylePriority.UseBorderColor = false;
      this.xrTableCell236.StylePriority.UseBorders = false;
      this.xrTableCell236.StylePriority.UseFont = false;
      this.xrTableCell236.StylePriority.UseForeColor = false;
      this.xrTableCell236.StylePriority.UseTextAlignment = false;
      this.xrTableCell236.Text = "External Reference No:";
      this.xrTableCell236.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell236.Weight = 0.66666666666666663D;
      // 
      // xrTableCell237
      // 
      this.xrTableCell237.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell237.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell237.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.ExternalReference")});
      this.xrTableCell237.Dpi = 254F;
      this.xrTableCell237.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell237.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell237.Name = "xrTableCell237";
      this.xrTableCell237.StylePriority.UseBorderColor = false;
      this.xrTableCell237.StylePriority.UseBorders = false;
      this.xrTableCell237.StylePriority.UseFont = false;
      this.xrTableCell237.StylePriority.UseForeColor = false;
      this.xrTableCell237.StylePriority.UseTextAlignment = false;
      this.xrTableCell237.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell237.Weight = 1.333333333333333D;
      // 
      // xrTableRow83
      // 
      this.xrTableRow83.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell238,
            this.xrTableCell239,
            this.xrTableCell240,
            this.xrTableCell241});
      this.xrTableRow83.Dpi = 254F;
      this.xrTableRow83.Name = "xrTableRow83";
      this.xrTableRow83.Weight = 0.94488188976377985D;
      // 
      // xrTableCell238
      // 
      this.xrTableCell238.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell238.BorderColor = System.Drawing.Color.White;
      this.xrTableCell238.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell238.Dpi = 254F;
      this.xrTableCell238.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell238.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell238.Name = "xrTableCell238";
      this.xrTableCell238.StylePriority.UseBackColor = false;
      this.xrTableCell238.StylePriority.UseBorderColor = false;
      this.xrTableCell238.StylePriority.UseBorders = false;
      this.xrTableCell238.StylePriority.UseFont = false;
      this.xrTableCell238.StylePriority.UseForeColor = false;
      this.xrTableCell238.StylePriority.UseTextAlignment = false;
      this.xrTableCell238.Text = "ID No:";
      this.xrTableCell238.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell238.Weight = 0.66666666666666674D;
      // 
      // xrTableCell239
      // 
      this.xrTableCell239.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell239.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell239.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.IDNo")});
      this.xrTableCell239.Dpi = 254F;
      this.xrTableCell239.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell239.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell239.Name = "xrTableCell239";
      this.xrTableCell239.StylePriority.UseBorderColor = false;
      this.xrTableCell239.StylePriority.UseBorders = false;
      this.xrTableCell239.StylePriority.UseFont = false;
      this.xrTableCell239.StylePriority.UseForeColor = false;
      this.xrTableCell239.StylePriority.UseTextAlignment = false;
      this.xrTableCell239.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell239.Weight = 1.333333333333333D;
      // 
      // xrTableCell240
      // 
      this.xrTableCell240.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell240.BorderColor = System.Drawing.Color.White;
      this.xrTableCell240.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell240.Dpi = 254F;
      this.xrTableCell240.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell240.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell240.Name = "xrTableCell240";
      this.xrTableCell240.StylePriority.UseBackColor = false;
      this.xrTableCell240.StylePriority.UseBorderColor = false;
      this.xrTableCell240.StylePriority.UseBorders = false;
      this.xrTableCell240.StylePriority.UseFont = false;
      this.xrTableCell240.StylePriority.UseForeColor = false;
      this.xrTableCell240.StylePriority.UseTextAlignment = false;
      this.xrTableCell240.Text = "Passport or 2nd ID No:";
      this.xrTableCell240.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell240.Weight = 0.66666666666666663D;
      // 
      // xrTableCell241
      // 
      this.xrTableCell241.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell241.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell241.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.PassportNo")});
      this.xrTableCell241.Dpi = 254F;
      this.xrTableCell241.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell241.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell241.Name = "xrTableCell241";
      this.xrTableCell241.StylePriority.UseBorderColor = false;
      this.xrTableCell241.StylePriority.UseBorders = false;
      this.xrTableCell241.StylePriority.UseFont = false;
      this.xrTableCell241.StylePriority.UseForeColor = false;
      this.xrTableCell241.StylePriority.UseTextAlignment = false;
      this.xrTableCell241.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell241.Weight = 1.333333333333333D;
      // 
      // xrTableRow84
      // 
      this.xrTableRow84.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell242,
            this.xrTableCell243,
            this.xrTableCell244,
            this.xrTableCell245});
      this.xrTableRow84.Dpi = 254F;
      this.xrTableRow84.Name = "xrTableRow84";
      this.xrTableRow84.Weight = 0.94488188976377985D;
      // 
      // xrTableCell242
      // 
      this.xrTableCell242.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell242.BorderColor = System.Drawing.Color.White;
      this.xrTableCell242.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell242.Dpi = 254F;
      this.xrTableCell242.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell242.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell242.Name = "xrTableCell242";
      this.xrTableCell242.StylePriority.UseBackColor = false;
      this.xrTableCell242.StylePriority.UseBorderColor = false;
      this.xrTableCell242.StylePriority.UseBorders = false;
      this.xrTableCell242.StylePriority.UseFont = false;
      this.xrTableCell242.StylePriority.UseForeColor = false;
      this.xrTableCell242.StylePriority.UseTextAlignment = false;
      this.xrTableCell242.Text = "Surname:";
      this.xrTableCell242.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell242.Weight = 0.66666666666666674D;
      // 
      // xrTableCell243
      // 
      this.xrTableCell243.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell243.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell243.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.Surname")});
      this.xrTableCell243.Dpi = 254F;
      this.xrTableCell243.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell243.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell243.Name = "xrTableCell243";
      this.xrTableCell243.StylePriority.UseBorderColor = false;
      this.xrTableCell243.StylePriority.UseBorders = false;
      this.xrTableCell243.StylePriority.UseFont = false;
      this.xrTableCell243.StylePriority.UseForeColor = false;
      this.xrTableCell243.StylePriority.UseTextAlignment = false;
      this.xrTableCell243.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell243.Weight = 1.333333333333333D;
      // 
      // xrTableCell244
      // 
      this.xrTableCell244.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell244.BorderColor = System.Drawing.Color.White;
      this.xrTableCell244.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell244.Dpi = 254F;
      this.xrTableCell244.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell244.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell244.Name = "xrTableCell244";
      this.xrTableCell244.StylePriority.UseBackColor = false;
      this.xrTableCell244.StylePriority.UseBorderColor = false;
      this.xrTableCell244.StylePriority.UseBorders = false;
      this.xrTableCell244.StylePriority.UseFont = false;
      this.xrTableCell244.StylePriority.UseForeColor = false;
      this.xrTableCell244.StylePriority.UseTextAlignment = false;
      this.xrTableCell244.Text = "Residential Address:";
      this.xrTableCell244.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell244.Weight = 0.66666666666666663D;
      // 
      // xrTableCell245
      // 
      this.xrTableCell245.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell245.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell245.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.ResidentialAddress")});
      this.xrTableCell245.Dpi = 254F;
      this.xrTableCell245.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell245.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell245.Name = "xrTableCell245";
      this.xrTableCell245.StylePriority.UseBorderColor = false;
      this.xrTableCell245.StylePriority.UseBorders = false;
      this.xrTableCell245.StylePriority.UseFont = false;
      this.xrTableCell245.StylePriority.UseForeColor = false;
      this.xrTableCell245.StylePriority.UseTextAlignment = false;
      this.xrTableCell245.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell245.Weight = 1.333333333333333D;
      // 
      // xrTableRow85
      // 
      this.xrTableRow85.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell246,
            this.xrTableCell247,
            this.xrTableCell248,
            this.xrTableCell249});
      this.xrTableRow85.Dpi = 254F;
      this.xrTableRow85.Name = "xrTableRow85";
      this.xrTableRow85.Weight = 0.94488188976377985D;
      // 
      // xrTableCell246
      // 
      this.xrTableCell246.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell246.BorderColor = System.Drawing.Color.White;
      this.xrTableCell246.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell246.Dpi = 254F;
      this.xrTableCell246.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell246.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell246.Name = "xrTableCell246";
      this.xrTableCell246.StylePriority.UseBackColor = false;
      this.xrTableCell246.StylePriority.UseBorderColor = false;
      this.xrTableCell246.StylePriority.UseBorders = false;
      this.xrTableCell246.StylePriority.UseFont = false;
      this.xrTableCell246.StylePriority.UseForeColor = false;
      this.xrTableCell246.StylePriority.UseTextAlignment = false;
      this.xrTableCell246.Text = "First Name:";
      this.xrTableCell246.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell246.Weight = 0.66666666666666674D;
      // 
      // xrTableCell247
      // 
      this.xrTableCell247.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell247.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell247.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.FirstName")});
      this.xrTableCell247.Dpi = 254F;
      this.xrTableCell247.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell247.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell247.Name = "xrTableCell247";
      this.xrTableCell247.StylePriority.UseBorderColor = false;
      this.xrTableCell247.StylePriority.UseBorders = false;
      this.xrTableCell247.StylePriority.UseFont = false;
      this.xrTableCell247.StylePriority.UseForeColor = false;
      this.xrTableCell247.StylePriority.UseTextAlignment = false;
      this.xrTableCell247.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell247.Weight = 1.333333333333333D;
      // 
      // xrTableCell248
      // 
      this.xrTableCell248.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell248.BorderColor = System.Drawing.Color.White;
      this.xrTableCell248.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell248.Dpi = 254F;
      this.xrTableCell248.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell248.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell248.Name = "xrTableCell248";
      this.xrTableCell248.StylePriority.UseBackColor = false;
      this.xrTableCell248.StylePriority.UseBorderColor = false;
      this.xrTableCell248.StylePriority.UseBorders = false;
      this.xrTableCell248.StylePriority.UseFont = false;
      this.xrTableCell248.StylePriority.UseForeColor = false;
      this.xrTableCell248.StylePriority.UseTextAlignment = false;
      this.xrTableCell248.Text = "Postal Address:";
      this.xrTableCell248.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell248.Weight = 0.66666666666666663D;
      // 
      // xrTableCell249
      // 
      this.xrTableCell249.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell249.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell249.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.PostalAddress")});
      this.xrTableCell249.Dpi = 254F;
      this.xrTableCell249.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell249.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell249.Name = "xrTableCell249";
      this.xrTableCell249.StylePriority.UseBorderColor = false;
      this.xrTableCell249.StylePriority.UseBorders = false;
      this.xrTableCell249.StylePriority.UseFont = false;
      this.xrTableCell249.StylePriority.UseForeColor = false;
      this.xrTableCell249.StylePriority.UseTextAlignment = false;
      this.xrTableCell249.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell249.Weight = 1.333333333333333D;
      // 
      // xrTableRow86
      // 
      this.xrTableRow86.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell250,
            this.xrTableCell251,
            this.xrTableCell252,
            this.xrTableCell253});
      this.xrTableRow86.Dpi = 254F;
      this.xrTableRow86.Name = "xrTableRow86";
      this.xrTableRow86.Weight = 0.94488188976377985D;
      // 
      // xrTableCell250
      // 
      this.xrTableCell250.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell250.BorderColor = System.Drawing.Color.White;
      this.xrTableCell250.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell250.Dpi = 254F;
      this.xrTableCell250.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell250.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell250.Name = "xrTableCell250";
      this.xrTableCell250.StylePriority.UseBackColor = false;
      this.xrTableCell250.StylePriority.UseBorderColor = false;
      this.xrTableCell250.StylePriority.UseBorders = false;
      this.xrTableCell250.StylePriority.UseFont = false;
      this.xrTableCell250.StylePriority.UseForeColor = false;
      this.xrTableCell250.StylePriority.UseTextAlignment = false;
      this.xrTableCell250.Text = "Second Name:";
      this.xrTableCell250.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell250.Weight = 0.66666666666666674D;
      // 
      // xrTableCell251
      // 
      this.xrTableCell251.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell251.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell251.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.SecondName")});
      this.xrTableCell251.Dpi = 254F;
      this.xrTableCell251.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell251.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell251.Name = "xrTableCell251";
      this.xrTableCell251.StylePriority.UseBorderColor = false;
      this.xrTableCell251.StylePriority.UseBorders = false;
      this.xrTableCell251.StylePriority.UseFont = false;
      this.xrTableCell251.StylePriority.UseForeColor = false;
      this.xrTableCell251.StylePriority.UseTextAlignment = false;
      this.xrTableCell251.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell251.Weight = 1.333333333333333D;
      // 
      // xrTableCell252
      // 
      this.xrTableCell252.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell252.BorderColor = System.Drawing.Color.White;
      this.xrTableCell252.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell252.Dpi = 254F;
      this.xrTableCell252.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell252.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell252.Name = "xrTableCell252";
      this.xrTableCell252.StylePriority.UseBackColor = false;
      this.xrTableCell252.StylePriority.UseBorderColor = false;
      this.xrTableCell252.StylePriority.UseBorders = false;
      this.xrTableCell252.StylePriority.UseFont = false;
      this.xrTableCell252.StylePriority.UseForeColor = false;
      this.xrTableCell252.StylePriority.UseTextAlignment = false;
      this.xrTableCell252.Text = "Telephone No. (H):";
      this.xrTableCell252.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell252.Weight = 0.66666666666666663D;
      // 
      // xrTableCell253
      // 
      this.xrTableCell253.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell253.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell253.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.HomeTelephoneNo")});
      this.xrTableCell253.Dpi = 254F;
      this.xrTableCell253.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell253.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell253.Name = "xrTableCell253";
      this.xrTableCell253.StylePriority.UseBorderColor = false;
      this.xrTableCell253.StylePriority.UseBorders = false;
      this.xrTableCell253.StylePriority.UseFont = false;
      this.xrTableCell253.StylePriority.UseForeColor = false;
      this.xrTableCell253.StylePriority.UseTextAlignment = false;
      this.xrTableCell253.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell253.Weight = 1.333333333333333D;
      // 
      // xrTableRow87
      // 
      this.xrTableRow87.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell254,
            this.xrTableCell255,
            this.xrTableCell256,
            this.xrTableCell257});
      this.xrTableRow87.Dpi = 254F;
      this.xrTableRow87.Name = "xrTableRow87";
      this.xrTableRow87.Weight = 0.94488188976377985D;
      // 
      // xrTableCell254
      // 
      this.xrTableCell254.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell254.BorderColor = System.Drawing.Color.White;
      this.xrTableCell254.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell254.Dpi = 254F;
      this.xrTableCell254.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell254.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell254.Name = "xrTableCell254";
      this.xrTableCell254.StylePriority.UseBackColor = false;
      this.xrTableCell254.StylePriority.UseBorderColor = false;
      this.xrTableCell254.StylePriority.UseBorders = false;
      this.xrTableCell254.StylePriority.UseFont = false;
      this.xrTableCell254.StylePriority.UseForeColor = false;
      this.xrTableCell254.StylePriority.UseTextAlignment = false;
      this.xrTableCell254.Text = "Title:";
      this.xrTableCell254.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell254.Weight = 0.66666666666666674D;
      // 
      // xrTableCell255
      // 
      this.xrTableCell255.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell255.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell255.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.TitleDesc")});
      this.xrTableCell255.Dpi = 254F;
      this.xrTableCell255.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell255.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell255.Name = "xrTableCell255";
      this.xrTableCell255.StylePriority.UseBorderColor = false;
      this.xrTableCell255.StylePriority.UseBorders = false;
      this.xrTableCell255.StylePriority.UseFont = false;
      this.xrTableCell255.StylePriority.UseForeColor = false;
      this.xrTableCell255.StylePriority.UseTextAlignment = false;
      this.xrTableCell255.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell255.Weight = 1.333333333333333D;
      // 
      // xrTableCell256
      // 
      this.xrTableCell256.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell256.BorderColor = System.Drawing.Color.White;
      this.xrTableCell256.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell256.Dpi = 254F;
      this.xrTableCell256.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell256.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell256.Name = "xrTableCell256";
      this.xrTableCell256.StylePriority.UseBackColor = false;
      this.xrTableCell256.StylePriority.UseBorderColor = false;
      this.xrTableCell256.StylePriority.UseBorders = false;
      this.xrTableCell256.StylePriority.UseFont = false;
      this.xrTableCell256.StylePriority.UseForeColor = false;
      this.xrTableCell256.StylePriority.UseTextAlignment = false;
      this.xrTableCell256.Text = "Telephone No. (W):";
      this.xrTableCell256.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell256.Weight = 0.66666666666666663D;
      // 
      // xrTableCell257
      // 
      this.xrTableCell257.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell257.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell257.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.WorkTelephoneNo")});
      this.xrTableCell257.Dpi = 254F;
      this.xrTableCell257.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell257.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell257.Name = "xrTableCell257";
      this.xrTableCell257.StylePriority.UseBorderColor = false;
      this.xrTableCell257.StylePriority.UseBorders = false;
      this.xrTableCell257.StylePriority.UseFont = false;
      this.xrTableCell257.StylePriority.UseForeColor = false;
      this.xrTableCell257.StylePriority.UseTextAlignment = false;
      this.xrTableCell257.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell257.Weight = 1.333333333333333D;
      // 
      // xrTableRow88
      // 
      this.xrTableRow88.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell258,
            this.xrTableCell259,
            this.xrTableCell260,
            this.xrTableCell261});
      this.xrTableRow88.Dpi = 254F;
      this.xrTableRow88.Name = "xrTableRow88";
      this.xrTableRow88.Weight = 0.94488188976377985D;
      // 
      // xrTableCell258
      // 
      this.xrTableCell258.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell258.BorderColor = System.Drawing.Color.White;
      this.xrTableCell258.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell258.Dpi = 254F;
      this.xrTableCell258.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell258.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell258.Name = "xrTableCell258";
      this.xrTableCell258.StylePriority.UseBackColor = false;
      this.xrTableCell258.StylePriority.UseBorderColor = false;
      this.xrTableCell258.StylePriority.UseBorders = false;
      this.xrTableCell258.StylePriority.UseFont = false;
      this.xrTableCell258.StylePriority.UseForeColor = false;
      this.xrTableCell258.StylePriority.UseTextAlignment = false;
      this.xrTableCell258.Text = "Gender:";
      this.xrTableCell258.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell258.Weight = 0.66666666666666674D;
      // 
      // xrTableCell259
      // 
      this.xrTableCell259.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell259.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell259.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.Gender")});
      this.xrTableCell259.Dpi = 254F;
      this.xrTableCell259.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell259.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell259.Name = "xrTableCell259";
      this.xrTableCell259.StylePriority.UseBorderColor = false;
      this.xrTableCell259.StylePriority.UseBorders = false;
      this.xrTableCell259.StylePriority.UseFont = false;
      this.xrTableCell259.StylePriority.UseForeColor = false;
      this.xrTableCell259.StylePriority.UseTextAlignment = false;
      this.xrTableCell259.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell259.Weight = 1.333333333333333D;
      // 
      // xrTableCell260
      // 
      this.xrTableCell260.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell260.BorderColor = System.Drawing.Color.White;
      this.xrTableCell260.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell260.Dpi = 254F;
      this.xrTableCell260.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell260.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell260.Multiline = true;
      this.xrTableCell260.Name = "xrTableCell260";
      this.xrTableCell260.StylePriority.UseBackColor = false;
      this.xrTableCell260.StylePriority.UseBorderColor = false;
      this.xrTableCell260.StylePriority.UseBorders = false;
      this.xrTableCell260.StylePriority.UseFont = false;
      this.xrTableCell260.StylePriority.UseForeColor = false;
      this.xrTableCell260.StylePriority.UseTextAlignment = false;
      this.xrTableCell260.Text = "Cellular/Mobile:\r\n";
      this.xrTableCell260.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell260.Weight = 0.66666666666666663D;
      // 
      // xrTableCell261
      // 
      this.xrTableCell261.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell261.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell261.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.CellularNo")});
      this.xrTableCell261.Dpi = 254F;
      this.xrTableCell261.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell261.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell261.Name = "xrTableCell261";
      this.xrTableCell261.StylePriority.UseBorderColor = false;
      this.xrTableCell261.StylePriority.UseBorders = false;
      this.xrTableCell261.StylePriority.UseFont = false;
      this.xrTableCell261.StylePriority.UseForeColor = false;
      this.xrTableCell261.StylePriority.UseTextAlignment = false;
      this.xrTableCell261.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell261.Weight = 1.333333333333333D;
      // 
      // xrTableRow89
      // 
      this.xrTableRow89.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell262,
            this.xrTableCell263,
            this.xrTableCell264,
            this.xrTableCell265});
      this.xrTableRow89.Dpi = 254F;
      this.xrTableRow89.Name = "xrTableRow89";
      this.xrTableRow89.Weight = 0.94488188976377985D;
      // 
      // xrTableCell262
      // 
      this.xrTableCell262.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell262.BorderColor = System.Drawing.Color.White;
      this.xrTableCell262.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell262.Dpi = 254F;
      this.xrTableCell262.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell262.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell262.Name = "xrTableCell262";
      this.xrTableCell262.StylePriority.UseBackColor = false;
      this.xrTableCell262.StylePriority.UseBorderColor = false;
      this.xrTableCell262.StylePriority.UseBorders = false;
      this.xrTableCell262.StylePriority.UseFont = false;
      this.xrTableCell262.StylePriority.UseForeColor = false;
      this.xrTableCell262.StylePriority.UseTextAlignment = false;
      this.xrTableCell262.Text = "Date of Birth:";
      this.xrTableCell262.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell262.Weight = 0.66666666666666674D;
      // 
      // xrTableCell263
      // 
      this.xrTableCell263.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell263.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell263.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.BirthDate")});
      this.xrTableCell263.Dpi = 254F;
      this.xrTableCell263.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell263.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell263.Name = "xrTableCell263";
      this.xrTableCell263.Scripts.OnBeforePrint = "xrTableCell263_BeforePrint";
      this.xrTableCell263.StylePriority.UseBorderColor = false;
      this.xrTableCell263.StylePriority.UseBorders = false;
      this.xrTableCell263.StylePriority.UseFont = false;
      this.xrTableCell263.StylePriority.UseForeColor = false;
      this.xrTableCell263.StylePriority.UseTextAlignment = false;
      this.xrTableCell263.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell263.Weight = 1.333333333333333D;
      // 
      // xrTableCell264
      // 
      this.xrTableCell264.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell264.BorderColor = System.Drawing.Color.White;
      this.xrTableCell264.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell264.Dpi = 254F;
      this.xrTableCell264.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell264.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell264.Name = "xrTableCell264";
      this.xrTableCell264.StylePriority.UseBackColor = false;
      this.xrTableCell264.StylePriority.UseBorderColor = false;
      this.xrTableCell264.StylePriority.UseBorders = false;
      this.xrTableCell264.StylePriority.UseFont = false;
      this.xrTableCell264.StylePriority.UseForeColor = false;
      this.xrTableCell264.StylePriority.UseTextAlignment = false;
      this.xrTableCell264.Text = "Email Address:";
      this.xrTableCell264.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell264.Weight = 0.66666666666666663D;
      // 
      // xrTableCell265
      // 
      this.xrTableCell265.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell265.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell265.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.EmailAddress")});
      this.xrTableCell265.Dpi = 254F;
      this.xrTableCell265.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell265.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell265.Name = "xrTableCell265";
      this.xrTableCell265.StylePriority.UseBorderColor = false;
      this.xrTableCell265.StylePriority.UseBorders = false;
      this.xrTableCell265.StylePriority.UseFont = false;
      this.xrTableCell265.StylePriority.UseForeColor = false;
      this.xrTableCell265.StylePriority.UseTextAlignment = false;
      this.xrTableCell265.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell265.Weight = 1.333333333333333D;
      // 
      // xrTableRow90
      // 
      this.xrTableRow90.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell266,
            this.xrTableCell267,
            this.xrTableCell268,
            this.xrTableCell269});
      this.xrTableRow90.Dpi = 254F;
      this.xrTableRow90.Name = "xrTableRow90";
      this.xrTableRow90.Weight = 0.94488188976377985D;
      // 
      // xrTableCell266
      // 
      this.xrTableCell266.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell266.BorderColor = System.Drawing.Color.White;
      this.xrTableCell266.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell266.Dpi = 254F;
      this.xrTableCell266.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell266.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell266.Name = "xrTableCell266";
      this.xrTableCell266.StylePriority.UseBackColor = false;
      this.xrTableCell266.StylePriority.UseBorderColor = false;
      this.xrTableCell266.StylePriority.UseBorders = false;
      this.xrTableCell266.StylePriority.UseFont = false;
      this.xrTableCell266.StylePriority.UseForeColor = false;
      this.xrTableCell266.StylePriority.UseTextAlignment = false;
      this.xrTableCell266.Text = "Marital Status:";
      this.xrTableCell266.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell266.Weight = 0.66666666666666674D;
      // 
      // xrTableCell267
      // 
      this.xrTableCell267.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell267.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell267.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.MaritalStatusDesc")});
      this.xrTableCell267.Dpi = 254F;
      this.xrTableCell267.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell267.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell267.Name = "xrTableCell267";
      this.xrTableCell267.StylePriority.UseBorderColor = false;
      this.xrTableCell267.StylePriority.UseBorders = false;
      this.xrTableCell267.StylePriority.UseFont = false;
      this.xrTableCell267.StylePriority.UseForeColor = false;
      this.xrTableCell267.StylePriority.UseTextAlignment = false;
      this.xrTableCell267.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell267.Weight = 1.333333333333333D;
      // 
      // xrTableCell268
      // 
      this.xrTableCell268.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell268.BorderColor = System.Drawing.Color.White;
      this.xrTableCell268.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell268.Dpi = 254F;
      this.xrTableCell268.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell268.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell268.Name = "xrTableCell268";
      this.xrTableCell268.StylePriority.UseBackColor = false;
      this.xrTableCell268.StylePriority.UseBorderColor = false;
      this.xrTableCell268.StylePriority.UseBorders = false;
      this.xrTableCell268.StylePriority.UseFont = false;
      this.xrTableCell268.StylePriority.UseForeColor = false;
      this.xrTableCell268.StylePriority.UseTextAlignment = false;
      this.xrTableCell268.Text = "Current Employer:";
      this.xrTableCell268.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell268.Weight = 0.66666666666666663D;
      // 
      // xrTableCell269
      // 
      this.xrTableCell269.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell269.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell269.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerDetail.EmployerDetail")});
      this.xrTableCell269.Dpi = 254F;
      this.xrTableCell269.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell269.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell269.Name = "xrTableCell269";
      this.xrTableCell269.StylePriority.UseBorderColor = false;
      this.xrTableCell269.StylePriority.UseBorders = false;
      this.xrTableCell269.StylePriority.UseFont = false;
      this.xrTableCell269.StylePriority.UseForeColor = false;
      this.xrTableCell269.StylePriority.UseTextAlignment = false;
      this.xrTableCell269.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell269.Weight = 1.333333333333333D;
      // 
      // xrTableRow102
      // 
      this.xrTableRow102.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell311});
      this.xrTableRow102.Dpi = 254F;
      this.xrTableRow102.Name = "xrTableRow102";
      this.xrTableRow102.Weight = 0.39370078740157477D;
      // 
      // xrTableCell311
      // 
      this.xrTableCell311.BorderColor = System.Drawing.Color.Black;
      this.xrTableCell311.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTableCell311.CanGrow = false;
      this.xrTableCell311.Dpi = 254F;
      this.xrTableCell311.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell311.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell311.Name = "xrTableCell311";
      this.xrTableCell311.StylePriority.UseBorderColor = false;
      this.xrTableCell311.StylePriority.UseBorders = false;
      this.xrTableCell311.StylePriority.UseFont = false;
      this.xrTableCell311.StylePriority.UseForeColor = false;
      this.xrTableCell311.StylePriority.UseTextAlignment = false;
      this.xrTableCell311.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell311.Weight = 3.9999999999999996D;
      // 
      // xrTableRow2
      // 
      this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell274});
      this.xrTableRow2.Dpi = 254F;
      this.xrTableRow2.Name = "xrTableRow2";
      this.xrTableRow2.Weight = 0.94488188976377985D;
      // 
      // xrTableCell274
      // 
      this.xrTableCell274.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(187)))), ((int)(((byte)(186)))));
      this.xrTableCell274.BorderColor = System.Drawing.Color.Black;
      this.xrTableCell274.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell274.BorderWidth = 0.5F;
      this.xrTableCell274.CanGrow = false;
      this.xrTableCell274.Dpi = 254F;
      this.xrTableCell274.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold);
      this.xrTableCell274.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell274.Name = "xrTableCell274";
      this.xrTableCell274.StylePriority.UseBackColor = false;
      this.xrTableCell274.StylePriority.UseBorderColor = false;
      this.xrTableCell274.StylePriority.UseBorders = false;
      this.xrTableCell274.StylePriority.UseBorderWidth = false;
      this.xrTableCell274.StylePriority.UseFont = false;
      this.xrTableCell274.StylePriority.UseForeColor = false;
      this.xrTableCell274.StylePriority.UseTextAlignment = false;
      this.xrTableCell274.Text = "Party B";
      this.xrTableCell274.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell274.Weight = 3.9999999999999996D;
      // 
      // xrTableRow96
      // 
      this.xrTableRow96.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell287,
            this.xrTableCell288,
            this.xrTableCell289,
            this.xrTableCell290});
      this.xrTableRow96.Dpi = 254F;
      this.xrTableRow96.Name = "xrTableRow96";
      this.xrTableRow96.Weight = 0.94488188976377985D;
      // 
      // xrTableCell287
      // 
      this.xrTableCell287.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell287.BorderColor = System.Drawing.Color.White;
      this.xrTableCell287.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell287.Dpi = 254F;
      this.xrTableCell287.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell287.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell287.Name = "xrTableCell287";
      this.xrTableCell287.StylePriority.UseBackColor = false;
      this.xrTableCell287.StylePriority.UseBorderColor = false;
      this.xrTableCell287.StylePriority.UseBorders = false;
      this.xrTableCell287.StylePriority.UseFont = false;
      this.xrTableCell287.StylePriority.UseForeColor = false;
      this.xrTableCell287.StylePriority.UseTextAlignment = false;
      this.xrTableCell287.Text = "Reference No:";
      this.xrTableCell287.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell287.Weight = 0.66666666666666674D;
      // 
      // xrTableCell288
      // 
      this.xrTableCell288.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell288.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell288.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.ReferenceNo")});
      this.xrTableCell288.Dpi = 254F;
      this.xrTableCell288.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell288.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell288.Name = "xrTableCell288";
      this.xrTableCell288.StylePriority.UseBorderColor = false;
      this.xrTableCell288.StylePriority.UseBorders = false;
      this.xrTableCell288.StylePriority.UseFont = false;
      this.xrTableCell288.StylePriority.UseForeColor = false;
      this.xrTableCell288.StylePriority.UseTextAlignment = false;
      this.xrTableCell288.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell288.Weight = 1.333333333333333D;
      // 
      // xrTableCell289
      // 
      this.xrTableCell289.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell289.BorderColor = System.Drawing.Color.White;
      this.xrTableCell289.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell289.Dpi = 254F;
      this.xrTableCell289.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell289.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell289.Name = "xrTableCell289";
      this.xrTableCell289.StylePriority.UseBackColor = false;
      this.xrTableCell289.StylePriority.UseBorderColor = false;
      this.xrTableCell289.StylePriority.UseBorders = false;
      this.xrTableCell289.StylePriority.UseFont = false;
      this.xrTableCell289.StylePriority.UseForeColor = false;
      this.xrTableCell289.StylePriority.UseTextAlignment = false;
      this.xrTableCell289.Text = "External Reference No:";
      this.xrTableCell289.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell289.Weight = 0.66666666666666663D;
      // 
      // xrTableCell290
      // 
      this.xrTableCell290.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell290.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell290.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.ExternalReference")});
      this.xrTableCell290.Dpi = 254F;
      this.xrTableCell290.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell290.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell290.Name = "xrTableCell290";
      this.xrTableCell290.StylePriority.UseBorderColor = false;
      this.xrTableCell290.StylePriority.UseBorders = false;
      this.xrTableCell290.StylePriority.UseFont = false;
      this.xrTableCell290.StylePriority.UseForeColor = false;
      this.xrTableCell290.StylePriority.UseTextAlignment = false;
      this.xrTableCell290.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell290.Weight = 1.333333333333333D;
      // 
      // xrTableRow100
      // 
      this.xrTableRow100.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell303,
            this.xrTableCell304,
            this.xrTableCell305,
            this.xrTableCell306});
      this.xrTableRow100.Dpi = 254F;
      this.xrTableRow100.Name = "xrTableRow100";
      this.xrTableRow100.Weight = 0.94488188976377985D;
      // 
      // xrTableCell303
      // 
      this.xrTableCell303.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell303.BorderColor = System.Drawing.Color.White;
      this.xrTableCell303.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell303.Dpi = 254F;
      this.xrTableCell303.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell303.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell303.Name = "xrTableCell303";
      this.xrTableCell303.StylePriority.UseBackColor = false;
      this.xrTableCell303.StylePriority.UseBorderColor = false;
      this.xrTableCell303.StylePriority.UseBorders = false;
      this.xrTableCell303.StylePriority.UseFont = false;
      this.xrTableCell303.StylePriority.UseForeColor = false;
      this.xrTableCell303.StylePriority.UseTextAlignment = false;
      this.xrTableCell303.Text = "ID No:";
      this.xrTableCell303.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell303.Weight = 0.66666666666666674D;
      // 
      // xrTableCell304
      // 
      this.xrTableCell304.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell304.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell304.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.IDNo")});
      this.xrTableCell304.Dpi = 254F;
      this.xrTableCell304.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell304.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell304.Name = "xrTableCell304";
      this.xrTableCell304.StylePriority.UseBorderColor = false;
      this.xrTableCell304.StylePriority.UseBorders = false;
      this.xrTableCell304.StylePriority.UseFont = false;
      this.xrTableCell304.StylePriority.UseForeColor = false;
      this.xrTableCell304.StylePriority.UseTextAlignment = false;
      this.xrTableCell304.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell304.Weight = 1.333333333333333D;
      // 
      // xrTableCell305
      // 
      this.xrTableCell305.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell305.BorderColor = System.Drawing.Color.White;
      this.xrTableCell305.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell305.Dpi = 254F;
      this.xrTableCell305.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell305.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell305.Name = "xrTableCell305";
      this.xrTableCell305.StylePriority.UseBackColor = false;
      this.xrTableCell305.StylePriority.UseBorderColor = false;
      this.xrTableCell305.StylePriority.UseBorders = false;
      this.xrTableCell305.StylePriority.UseFont = false;
      this.xrTableCell305.StylePriority.UseForeColor = false;
      this.xrTableCell305.StylePriority.UseTextAlignment = false;
      this.xrTableCell305.Text = "Passport or 2nd ID No:";
      this.xrTableCell305.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell305.Weight = 0.66666666666666663D;
      // 
      // xrTableCell306
      // 
      this.xrTableCell306.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell306.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell306.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.PassportNo")});
      this.xrTableCell306.Dpi = 254F;
      this.xrTableCell306.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell306.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell306.Name = "xrTableCell306";
      this.xrTableCell306.StylePriority.UseBorderColor = false;
      this.xrTableCell306.StylePriority.UseBorders = false;
      this.xrTableCell306.StylePriority.UseFont = false;
      this.xrTableCell306.StylePriority.UseForeColor = false;
      this.xrTableCell306.StylePriority.UseTextAlignment = false;
      this.xrTableCell306.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell306.Weight = 1.333333333333333D;
      // 
      // xrTableRow99
      // 
      this.xrTableRow99.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell299,
            this.xrTableCell300,
            this.xrTableCell301,
            this.xrTableCell302});
      this.xrTableRow99.Dpi = 254F;
      this.xrTableRow99.Name = "xrTableRow99";
      this.xrTableRow99.Weight = 0.94488188976377985D;
      // 
      // xrTableCell299
      // 
      this.xrTableCell299.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell299.BorderColor = System.Drawing.Color.White;
      this.xrTableCell299.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell299.Dpi = 254F;
      this.xrTableCell299.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell299.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell299.Name = "xrTableCell299";
      this.xrTableCell299.StylePriority.UseBackColor = false;
      this.xrTableCell299.StylePriority.UseBorderColor = false;
      this.xrTableCell299.StylePriority.UseBorders = false;
      this.xrTableCell299.StylePriority.UseFont = false;
      this.xrTableCell299.StylePriority.UseForeColor = false;
      this.xrTableCell299.StylePriority.UseTextAlignment = false;
      this.xrTableCell299.Text = "Surname:";
      this.xrTableCell299.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell299.Weight = 0.66666666666666674D;
      // 
      // xrTableCell300
      // 
      this.xrTableCell300.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell300.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell300.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.Surname")});
      this.xrTableCell300.Dpi = 254F;
      this.xrTableCell300.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell300.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell300.Name = "xrTableCell300";
      this.xrTableCell300.StylePriority.UseBorderColor = false;
      this.xrTableCell300.StylePriority.UseBorders = false;
      this.xrTableCell300.StylePriority.UseFont = false;
      this.xrTableCell300.StylePriority.UseForeColor = false;
      this.xrTableCell300.StylePriority.UseTextAlignment = false;
      this.xrTableCell300.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell300.Weight = 1.333333333333333D;
      // 
      // xrTableCell301
      // 
      this.xrTableCell301.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell301.BorderColor = System.Drawing.Color.White;
      this.xrTableCell301.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell301.Dpi = 254F;
      this.xrTableCell301.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell301.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell301.Name = "xrTableCell301";
      this.xrTableCell301.StylePriority.UseBackColor = false;
      this.xrTableCell301.StylePriority.UseBorderColor = false;
      this.xrTableCell301.StylePriority.UseBorders = false;
      this.xrTableCell301.StylePriority.UseFont = false;
      this.xrTableCell301.StylePriority.UseForeColor = false;
      this.xrTableCell301.StylePriority.UseTextAlignment = false;
      this.xrTableCell301.Text = "Residential Address:";
      this.xrTableCell301.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell301.Weight = 0.66666666666666663D;
      // 
      // xrTableCell302
      // 
      this.xrTableCell302.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell302.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell302.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.ResidentialAddress")});
      this.xrTableCell302.Dpi = 254F;
      this.xrTableCell302.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell302.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell302.Name = "xrTableCell302";
      this.xrTableCell302.StylePriority.UseBorderColor = false;
      this.xrTableCell302.StylePriority.UseBorders = false;
      this.xrTableCell302.StylePriority.UseFont = false;
      this.xrTableCell302.StylePriority.UseForeColor = false;
      this.xrTableCell302.StylePriority.UseTextAlignment = false;
      this.xrTableCell302.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell302.Weight = 1.333333333333333D;
      // 
      // xrTableRow98
      // 
      this.xrTableRow98.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell295,
            this.xrTableCell296,
            this.xrTableCell297,
            this.xrTableCell298});
      this.xrTableRow98.Dpi = 254F;
      this.xrTableRow98.Name = "xrTableRow98";
      this.xrTableRow98.Weight = 0.94488188976377985D;
      // 
      // xrTableCell295
      // 
      this.xrTableCell295.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell295.BorderColor = System.Drawing.Color.White;
      this.xrTableCell295.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell295.Dpi = 254F;
      this.xrTableCell295.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell295.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell295.Name = "xrTableCell295";
      this.xrTableCell295.StylePriority.UseBackColor = false;
      this.xrTableCell295.StylePriority.UseBorderColor = false;
      this.xrTableCell295.StylePriority.UseBorders = false;
      this.xrTableCell295.StylePriority.UseFont = false;
      this.xrTableCell295.StylePriority.UseForeColor = false;
      this.xrTableCell295.StylePriority.UseTextAlignment = false;
      this.xrTableCell295.Text = "First Name:";
      this.xrTableCell295.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell295.Weight = 0.66666666666666674D;
      // 
      // xrTableCell296
      // 
      this.xrTableCell296.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell296.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell296.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.FirstName")});
      this.xrTableCell296.Dpi = 254F;
      this.xrTableCell296.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell296.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell296.Name = "xrTableCell296";
      this.xrTableCell296.StylePriority.UseBorderColor = false;
      this.xrTableCell296.StylePriority.UseBorders = false;
      this.xrTableCell296.StylePriority.UseFont = false;
      this.xrTableCell296.StylePriority.UseForeColor = false;
      this.xrTableCell296.StylePriority.UseTextAlignment = false;
      this.xrTableCell296.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell296.Weight = 1.333333333333333D;
      // 
      // xrTableCell297
      // 
      this.xrTableCell297.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell297.BorderColor = System.Drawing.Color.White;
      this.xrTableCell297.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell297.Dpi = 254F;
      this.xrTableCell297.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell297.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell297.Name = "xrTableCell297";
      this.xrTableCell297.StylePriority.UseBackColor = false;
      this.xrTableCell297.StylePriority.UseBorderColor = false;
      this.xrTableCell297.StylePriority.UseBorders = false;
      this.xrTableCell297.StylePriority.UseFont = false;
      this.xrTableCell297.StylePriority.UseForeColor = false;
      this.xrTableCell297.StylePriority.UseTextAlignment = false;
      this.xrTableCell297.Text = "Postal Address:";
      this.xrTableCell297.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell297.Weight = 0.66666666666666663D;
      // 
      // xrTableCell298
      // 
      this.xrTableCell298.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell298.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell298.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.PostalAddress")});
      this.xrTableCell298.Dpi = 254F;
      this.xrTableCell298.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell298.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell298.Name = "xrTableCell298";
      this.xrTableCell298.StylePriority.UseBorderColor = false;
      this.xrTableCell298.StylePriority.UseBorders = false;
      this.xrTableCell298.StylePriority.UseFont = false;
      this.xrTableCell298.StylePriority.UseForeColor = false;
      this.xrTableCell298.StylePriority.UseTextAlignment = false;
      this.xrTableCell298.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell298.Weight = 1.333333333333333D;
      // 
      // xrTableRow97
      // 
      this.xrTableRow97.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell291,
            this.xrTableCell292,
            this.xrTableCell293,
            this.xrTableCell294});
      this.xrTableRow97.Dpi = 254F;
      this.xrTableRow97.Name = "xrTableRow97";
      this.xrTableRow97.Weight = 0.94488188976378007D;
      // 
      // xrTableCell291
      // 
      this.xrTableCell291.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell291.BorderColor = System.Drawing.Color.White;
      this.xrTableCell291.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell291.Dpi = 254F;
      this.xrTableCell291.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell291.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell291.Name = "xrTableCell291";
      this.xrTableCell291.StylePriority.UseBackColor = false;
      this.xrTableCell291.StylePriority.UseBorderColor = false;
      this.xrTableCell291.StylePriority.UseBorders = false;
      this.xrTableCell291.StylePriority.UseFont = false;
      this.xrTableCell291.StylePriority.UseForeColor = false;
      this.xrTableCell291.StylePriority.UseTextAlignment = false;
      this.xrTableCell291.Text = "Second Name:";
      this.xrTableCell291.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell291.Weight = 0.66666666666666674D;
      // 
      // xrTableCell292
      // 
      this.xrTableCell292.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell292.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell292.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.SecondName")});
      this.xrTableCell292.Dpi = 254F;
      this.xrTableCell292.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell292.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell292.Name = "xrTableCell292";
      this.xrTableCell292.StylePriority.UseBorderColor = false;
      this.xrTableCell292.StylePriority.UseBorders = false;
      this.xrTableCell292.StylePriority.UseFont = false;
      this.xrTableCell292.StylePriority.UseForeColor = false;
      this.xrTableCell292.StylePriority.UseTextAlignment = false;
      this.xrTableCell292.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell292.Weight = 1.333333333333333D;
      // 
      // xrTableCell293
      // 
      this.xrTableCell293.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell293.BorderColor = System.Drawing.Color.White;
      this.xrTableCell293.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell293.Dpi = 254F;
      this.xrTableCell293.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell293.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell293.Name = "xrTableCell293";
      this.xrTableCell293.StylePriority.UseBackColor = false;
      this.xrTableCell293.StylePriority.UseBorderColor = false;
      this.xrTableCell293.StylePriority.UseBorders = false;
      this.xrTableCell293.StylePriority.UseFont = false;
      this.xrTableCell293.StylePriority.UseForeColor = false;
      this.xrTableCell293.StylePriority.UseTextAlignment = false;
      this.xrTableCell293.Text = "Telephone No. (H):";
      this.xrTableCell293.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell293.Weight = 0.66666666666666663D;
      // 
      // xrTableCell294
      // 
      this.xrTableCell294.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell294.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell294.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.HomeTelephoneNo")});
      this.xrTableCell294.Dpi = 254F;
      this.xrTableCell294.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell294.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell294.Name = "xrTableCell294";
      this.xrTableCell294.StylePriority.UseBorderColor = false;
      this.xrTableCell294.StylePriority.UseBorders = false;
      this.xrTableCell294.StylePriority.UseFont = false;
      this.xrTableCell294.StylePriority.UseForeColor = false;
      this.xrTableCell294.StylePriority.UseTextAlignment = false;
      this.xrTableCell294.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell294.Weight = 1.333333333333333D;
      // 
      // xrTableRow95
      // 
      this.xrTableRow95.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell283,
            this.xrTableCell284,
            this.xrTableCell285,
            this.xrTableCell286});
      this.xrTableRow95.Dpi = 254F;
      this.xrTableRow95.Name = "xrTableRow95";
      this.xrTableRow95.Weight = 0.94488188976377985D;
      // 
      // xrTableCell283
      // 
      this.xrTableCell283.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell283.BorderColor = System.Drawing.Color.White;
      this.xrTableCell283.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell283.Dpi = 254F;
      this.xrTableCell283.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell283.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell283.Name = "xrTableCell283";
      this.xrTableCell283.StylePriority.UseBackColor = false;
      this.xrTableCell283.StylePriority.UseBorderColor = false;
      this.xrTableCell283.StylePriority.UseBorders = false;
      this.xrTableCell283.StylePriority.UseFont = false;
      this.xrTableCell283.StylePriority.UseForeColor = false;
      this.xrTableCell283.StylePriority.UseTextAlignment = false;
      this.xrTableCell283.Text = "Title:";
      this.xrTableCell283.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell283.Weight = 0.66666666666666674D;
      // 
      // xrTableCell284
      // 
      this.xrTableCell284.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell284.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell284.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.TitleDesc")});
      this.xrTableCell284.Dpi = 254F;
      this.xrTableCell284.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell284.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell284.Name = "xrTableCell284";
      this.xrTableCell284.StylePriority.UseBorderColor = false;
      this.xrTableCell284.StylePriority.UseBorders = false;
      this.xrTableCell284.StylePriority.UseFont = false;
      this.xrTableCell284.StylePriority.UseForeColor = false;
      this.xrTableCell284.StylePriority.UseTextAlignment = false;
      this.xrTableCell284.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell284.Weight = 1.333333333333333D;
      // 
      // xrTableCell285
      // 
      this.xrTableCell285.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell285.BorderColor = System.Drawing.Color.White;
      this.xrTableCell285.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell285.Dpi = 254F;
      this.xrTableCell285.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell285.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell285.Name = "xrTableCell285";
      this.xrTableCell285.StylePriority.UseBackColor = false;
      this.xrTableCell285.StylePriority.UseBorderColor = false;
      this.xrTableCell285.StylePriority.UseBorders = false;
      this.xrTableCell285.StylePriority.UseFont = false;
      this.xrTableCell285.StylePriority.UseForeColor = false;
      this.xrTableCell285.StylePriority.UseTextAlignment = false;
      this.xrTableCell285.Text = "Telephone No. (W):";
      this.xrTableCell285.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell285.Weight = 0.66666666666666663D;
      // 
      // xrTableCell286
      // 
      this.xrTableCell286.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell286.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell286.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.WorkTelephoneNo")});
      this.xrTableCell286.Dpi = 254F;
      this.xrTableCell286.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell286.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell286.Name = "xrTableCell286";
      this.xrTableCell286.StylePriority.UseBorderColor = false;
      this.xrTableCell286.StylePriority.UseBorders = false;
      this.xrTableCell286.StylePriority.UseFont = false;
      this.xrTableCell286.StylePriority.UseForeColor = false;
      this.xrTableCell286.StylePriority.UseTextAlignment = false;
      this.xrTableCell286.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell286.Weight = 1.333333333333333D;
      // 
      // xrTableRow94
      // 
      this.xrTableRow94.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell279,
            this.xrTableCell280,
            this.xrTableCell281,
            this.xrTableCell282});
      this.xrTableRow94.Dpi = 254F;
      this.xrTableRow94.Name = "xrTableRow94";
      this.xrTableRow94.Weight = 0.94488188976377985D;
      // 
      // xrTableCell279
      // 
      this.xrTableCell279.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell279.BorderColor = System.Drawing.Color.White;
      this.xrTableCell279.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell279.Dpi = 254F;
      this.xrTableCell279.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell279.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell279.Name = "xrTableCell279";
      this.xrTableCell279.StylePriority.UseBackColor = false;
      this.xrTableCell279.StylePriority.UseBorderColor = false;
      this.xrTableCell279.StylePriority.UseBorders = false;
      this.xrTableCell279.StylePriority.UseFont = false;
      this.xrTableCell279.StylePriority.UseForeColor = false;
      this.xrTableCell279.StylePriority.UseTextAlignment = false;
      this.xrTableCell279.Text = "Gender:";
      this.xrTableCell279.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell279.Weight = 0.66666666666666674D;
      // 
      // xrTableCell280
      // 
      this.xrTableCell280.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell280.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell280.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.Gender")});
      this.xrTableCell280.Dpi = 254F;
      this.xrTableCell280.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell280.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell280.Name = "xrTableCell280";
      this.xrTableCell280.StylePriority.UseBorderColor = false;
      this.xrTableCell280.StylePriority.UseBorders = false;
      this.xrTableCell280.StylePriority.UseFont = false;
      this.xrTableCell280.StylePriority.UseForeColor = false;
      this.xrTableCell280.StylePriority.UseTextAlignment = false;
      this.xrTableCell280.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell280.Weight = 1.333333333333333D;
      // 
      // xrTableCell281
      // 
      this.xrTableCell281.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell281.BorderColor = System.Drawing.Color.White;
      this.xrTableCell281.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell281.Dpi = 254F;
      this.xrTableCell281.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell281.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell281.Multiline = true;
      this.xrTableCell281.Name = "xrTableCell281";
      this.xrTableCell281.StylePriority.UseBackColor = false;
      this.xrTableCell281.StylePriority.UseBorderColor = false;
      this.xrTableCell281.StylePriority.UseBorders = false;
      this.xrTableCell281.StylePriority.UseFont = false;
      this.xrTableCell281.StylePriority.UseForeColor = false;
      this.xrTableCell281.StylePriority.UseTextAlignment = false;
      this.xrTableCell281.Text = "Cellular/Mobile:\r\n";
      this.xrTableCell281.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell281.Weight = 0.66666666666666663D;
      // 
      // xrTableCell282
      // 
      this.xrTableCell282.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell282.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell282.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.CellularNo")});
      this.xrTableCell282.Dpi = 254F;
      this.xrTableCell282.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell282.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell282.Name = "xrTableCell282";
      this.xrTableCell282.StylePriority.UseBorderColor = false;
      this.xrTableCell282.StylePriority.UseBorders = false;
      this.xrTableCell282.StylePriority.UseFont = false;
      this.xrTableCell282.StylePriority.UseForeColor = false;
      this.xrTableCell282.StylePriority.UseTextAlignment = false;
      this.xrTableCell282.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell282.Weight = 1.333333333333333D;
      // 
      // xrTableRow93
      // 
      this.xrTableRow93.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell275,
            this.xrTableCell276,
            this.xrTableCell277,
            this.xrTableCell278});
      this.xrTableRow93.Dpi = 254F;
      this.xrTableRow93.Name = "xrTableRow93";
      this.xrTableRow93.Weight = 0.94488188976377985D;
      // 
      // xrTableCell275
      // 
      this.xrTableCell275.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell275.BorderColor = System.Drawing.Color.White;
      this.xrTableCell275.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell275.Dpi = 254F;
      this.xrTableCell275.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell275.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell275.Name = "xrTableCell275";
      this.xrTableCell275.StylePriority.UseBackColor = false;
      this.xrTableCell275.StylePriority.UseBorderColor = false;
      this.xrTableCell275.StylePriority.UseBorders = false;
      this.xrTableCell275.StylePriority.UseFont = false;
      this.xrTableCell275.StylePriority.UseForeColor = false;
      this.xrTableCell275.StylePriority.UseTextAlignment = false;
      this.xrTableCell275.Text = "Date of Birth:";
      this.xrTableCell275.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell275.Weight = 0.66666666666666674D;
      // 
      // xrTableCell276
      // 
      this.xrTableCell276.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell276.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell276.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.BirthDate")});
      this.xrTableCell276.Dpi = 254F;
      this.xrTableCell276.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell276.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell276.Name = "xrTableCell276";
      this.xrTableCell276.Scripts.OnBeforePrint = "xrTableCell276_BeforePrint";
      this.xrTableCell276.StylePriority.UseBorderColor = false;
      this.xrTableCell276.StylePriority.UseBorders = false;
      this.xrTableCell276.StylePriority.UseFont = false;
      this.xrTableCell276.StylePriority.UseForeColor = false;
      this.xrTableCell276.StylePriority.UseTextAlignment = false;
      this.xrTableCell276.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell276.Weight = 1.333333333333333D;
      // 
      // xrTableCell277
      // 
      this.xrTableCell277.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell277.BorderColor = System.Drawing.Color.White;
      this.xrTableCell277.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell277.Dpi = 254F;
      this.xrTableCell277.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell277.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell277.Name = "xrTableCell277";
      this.xrTableCell277.StylePriority.UseBackColor = false;
      this.xrTableCell277.StylePriority.UseBorderColor = false;
      this.xrTableCell277.StylePriority.UseBorders = false;
      this.xrTableCell277.StylePriority.UseFont = false;
      this.xrTableCell277.StylePriority.UseForeColor = false;
      this.xrTableCell277.StylePriority.UseTextAlignment = false;
      this.xrTableCell277.Text = "Email Address:";
      this.xrTableCell277.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell277.Weight = 0.66666666666666663D;
      // 
      // xrTableCell278
      // 
      this.xrTableCell278.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell278.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell278.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.EmailAddress")});
      this.xrTableCell278.Dpi = 254F;
      this.xrTableCell278.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell278.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell278.Name = "xrTableCell278";
      this.xrTableCell278.StylePriority.UseBorderColor = false;
      this.xrTableCell278.StylePriority.UseBorders = false;
      this.xrTableCell278.StylePriority.UseFont = false;
      this.xrTableCell278.StylePriority.UseForeColor = false;
      this.xrTableCell278.StylePriority.UseTextAlignment = false;
      this.xrTableCell278.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell278.Weight = 1.333333333333333D;
      // 
      // xrTableRow101
      // 
      this.xrTableRow101.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell47,
            this.xrTableCell273,
            this.xrTableCell307});
      this.xrTableRow101.Dpi = 254F;
      this.xrTableRow101.Name = "xrTableRow101";
      this.xrTableRow101.Weight = 0.94488188976377985D;
      // 
      // xrTableCell1
      // 
      this.xrTableCell1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell1.BorderColor = System.Drawing.Color.White;
      this.xrTableCell1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell1.Dpi = 254F;
      this.xrTableCell1.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell1.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell1.Name = "xrTableCell1";
      this.xrTableCell1.StylePriority.UseBackColor = false;
      this.xrTableCell1.StylePriority.UseBorderColor = false;
      this.xrTableCell1.StylePriority.UseBorders = false;
      this.xrTableCell1.StylePriority.UseFont = false;
      this.xrTableCell1.StylePriority.UseForeColor = false;
      this.xrTableCell1.StylePriority.UseTextAlignment = false;
      this.xrTableCell1.Text = "Marital Status:";
      this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell1.Weight = 0.66666666666666674D;
      // 
      // xrTableCell47
      // 
      this.xrTableCell47.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell47.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell47.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.MaritalStatusDesc")});
      this.xrTableCell47.Dpi = 254F;
      this.xrTableCell47.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell47.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell47.Name = "xrTableCell47";
      this.xrTableCell47.StylePriority.UseBorderColor = false;
      this.xrTableCell47.StylePriority.UseBorders = false;
      this.xrTableCell47.StylePriority.UseFont = false;
      this.xrTableCell47.StylePriority.UseForeColor = false;
      this.xrTableCell47.StylePriority.UseTextAlignment = false;
      this.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell47.Weight = 1.333333333333333D;
      // 
      // xrTableCell273
      // 
      this.xrTableCell273.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell273.BorderColor = System.Drawing.Color.White;
      this.xrTableCell273.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell273.Dpi = 254F;
      this.xrTableCell273.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell273.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell273.Name = "xrTableCell273";
      this.xrTableCell273.StylePriority.UseBackColor = false;
      this.xrTableCell273.StylePriority.UseBorderColor = false;
      this.xrTableCell273.StylePriority.UseBorders = false;
      this.xrTableCell273.StylePriority.UseFont = false;
      this.xrTableCell273.StylePriority.UseForeColor = false;
      this.xrTableCell273.StylePriority.UseTextAlignment = false;
      this.xrTableCell273.Text = "Current Employer:";
      this.xrTableCell273.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell273.Weight = 0.66666666666666663D;
      // 
      // xrTableCell307
      // 
      this.xrTableCell307.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell307.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell307.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerOther.ConsumerOther_ConsumerDetail.EmployerDetail")});
      this.xrTableCell307.Dpi = 254F;
      this.xrTableCell307.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell307.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell307.Name = "xrTableCell307";
      this.xrTableCell307.StylePriority.UseBorderColor = false;
      this.xrTableCell307.StylePriority.UseBorders = false;
      this.xrTableCell307.StylePriority.UseFont = false;
      this.xrTableCell307.StylePriority.UseForeColor = false;
      this.xrTableCell307.StylePriority.UseTextAlignment = false;
      this.xrTableCell307.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell307.Weight = 1.333333333333333D;
      // 
      // ReportHeader1
      // 
      this.ReportHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable22});
      this.ReportHeader1.Dpi = 254F;
      this.ReportHeader1.HeightF = 125F;
      this.ReportHeader1.Name = "ReportHeader1";
      // 
      // xrTable22
      // 
      this.xrTable22.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTable22.Dpi = 254F;
      this.xrTable22.KeepTogether = true;
      this.xrTable22.LocationFloat = new DevExpress.Utils.PointFloat(0F, 25F);
      this.xrTable22.Name = "xrTable22";
      this.xrTable22.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTable22.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow91});
      this.xrTable22.SizeF = new System.Drawing.SizeF(2058F, 100F);
      this.xrTable22.StylePriority.UseBorders = false;
      this.xrTable22.StylePriority.UsePadding = false;
      // 
      // xrTableRow91
      // 
      this.xrTableRow91.Borders = DevExpress.XtraPrinting.BorderSide.Top;
      this.xrTableRow91.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell270});
      this.xrTableRow91.Dpi = 254F;
      this.xrTableRow91.Name = "xrTableRow91";
      this.xrTableRow91.StylePriority.UseBorders = false;
      this.xrTableRow91.Weight = 1.5748031496062991D;
      // 
      // xrTableCell270
      // 
      this.xrTableCell270.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell270.BorderColor = System.Drawing.Color.Gainsboro;
      this.xrTableCell270.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell270.CanGrow = false;
      this.xrTableCell270.Dpi = 254F;
      this.xrTableCell270.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
      this.xrTableCell270.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
      this.xrTableCell270.Name = "xrTableCell270";
      this.xrTableCell270.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTableCell270.StylePriority.UseBackColor = false;
      this.xrTableCell270.StylePriority.UseBorderColor = false;
      this.xrTableCell270.StylePriority.UseBorders = false;
      this.xrTableCell270.StylePriority.UseFont = false;
      this.xrTableCell270.StylePriority.UseForeColor = false;
      this.xrTableCell270.StylePriority.UsePadding = false;
      this.xrTableCell270.StylePriority.UseTextAlignment = false;
      this.xrTableCell270.Text = "Personal Details";
      this.xrTableCell270.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell270.Weight = 4D;
      // 
      // DetailReport
      // 
      this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail14,
            this.GroupHeader11});
      this.DetailReport.DataMember = "Consumer.Consumer_ConsumerAddressHistory";
      this.DetailReport.Dpi = 254F;
      this.DetailReport.Level = 6;
      this.DetailReport.Name = "DetailReport";
      // 
      // Detail14
      // 
      this.Detail14.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable24});
      this.Detail14.Dpi = 254F;
      this.Detail14.HeightF = 60F;
      this.Detail14.Name = "Detail14";
      // 
      // xrTable24
      // 
      this.xrTable24.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTable24.Dpi = 254F;
      this.xrTable24.KeepTogether = true;
      this.xrTable24.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
      this.xrTable24.Name = "xrTable24";
      this.xrTable24.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTable24.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow106});
      this.xrTable24.SizeF = new System.Drawing.SizeF(2058F, 60F);
      this.xrTable24.StylePriority.UseBorders = false;
      this.xrTable24.StylePriority.UsePadding = false;
      // 
      // xrTableRow106
      // 
      this.xrTableRow106.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell318,
            this.xrTableCell319,
            this.xrTableCell320,
            this.xrTableCell321,
            this.xrTableCell322,
            this.xrTableCell323,
            this.xrTableCell324});
      this.xrTableRow106.Dpi = 254F;
      this.xrTableRow106.Name = "xrTableRow106";
      this.xrTableRow106.Weight = 0.94488188976377985D;
      // 
      // xrTableCell318
      // 
      this.xrTableCell318.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell318.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell318.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell318.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerAddressHistory.LastUpdatedDate")});
      this.xrTableCell318.Dpi = 254F;
      this.xrTableCell318.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell318.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell318.Name = "xrTableCell318";
      this.xrTableCell318.Scripts.OnBeforePrint = "xrTableCell318_BeforePrint";
      this.xrTableCell318.StylePriority.UseBackColor = false;
      this.xrTableCell318.StylePriority.UseBorderColor = false;
      this.xrTableCell318.StylePriority.UseBorders = false;
      this.xrTableCell318.StylePriority.UseFont = false;
      this.xrTableCell318.StylePriority.UseForeColor = false;
      this.xrTableCell318.StylePriority.UseTextAlignment = false;
      this.xrTableCell318.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell318.Weight = 0.5714285714285714D;
      // 
      // xrTableCell319
      // 
      this.xrTableCell319.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell319.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell319.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell319.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerAddressHistory.AddressType")});
      this.xrTableCell319.Dpi = 254F;
      this.xrTableCell319.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell319.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell319.Name = "xrTableCell319";
      this.xrTableCell319.StylePriority.UseBackColor = false;
      this.xrTableCell319.StylePriority.UseBorderColor = false;
      this.xrTableCell319.StylePriority.UseBorders = false;
      this.xrTableCell319.StylePriority.UseFont = false;
      this.xrTableCell319.StylePriority.UseForeColor = false;
      this.xrTableCell319.StylePriority.UseTextAlignment = false;
      this.xrTableCell319.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell319.Weight = 0.5714285714285714D;
      // 
      // xrTableCell320
      // 
      this.xrTableCell320.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell320.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell320.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell320.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerAddressHistory.Address1")});
      this.xrTableCell320.Dpi = 254F;
      this.xrTableCell320.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell320.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell320.Name = "xrTableCell320";
      this.xrTableCell320.StylePriority.UseBackColor = false;
      this.xrTableCell320.StylePriority.UseBorderColor = false;
      this.xrTableCell320.StylePriority.UseBorders = false;
      this.xrTableCell320.StylePriority.UseFont = false;
      this.xrTableCell320.StylePriority.UseForeColor = false;
      this.xrTableCell320.StylePriority.UseTextAlignment = false;
      this.xrTableCell320.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell320.Weight = 0.57142857142857129D;
      // 
      // xrTableCell321
      // 
      this.xrTableCell321.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell321.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell321.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell321.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerAddressHistory.Address2")});
      this.xrTableCell321.Dpi = 254F;
      this.xrTableCell321.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell321.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell321.Name = "xrTableCell321";
      this.xrTableCell321.StylePriority.UseBackColor = false;
      this.xrTableCell321.StylePriority.UseBorderColor = false;
      this.xrTableCell321.StylePriority.UseBorders = false;
      this.xrTableCell321.StylePriority.UseFont = false;
      this.xrTableCell321.StylePriority.UseForeColor = false;
      this.xrTableCell321.StylePriority.UseTextAlignment = false;
      this.xrTableCell321.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell321.Weight = 0.57142857142857129D;
      // 
      // xrTableCell322
      // 
      this.xrTableCell322.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell322.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell322.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell322.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerAddressHistory.Address3")});
      this.xrTableCell322.Dpi = 254F;
      this.xrTableCell322.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell322.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell322.Name = "xrTableCell322";
      this.xrTableCell322.StylePriority.UseBackColor = false;
      this.xrTableCell322.StylePriority.UseBorderColor = false;
      this.xrTableCell322.StylePriority.UseBorders = false;
      this.xrTableCell322.StylePriority.UseFont = false;
      this.xrTableCell322.StylePriority.UseForeColor = false;
      this.xrTableCell322.StylePriority.UseTextAlignment = false;
      this.xrTableCell322.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell322.Weight = 0.5714285714285714D;
      // 
      // xrTableCell323
      // 
      this.xrTableCell323.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell323.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell323.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell323.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerAddressHistory.Address4")});
      this.xrTableCell323.Dpi = 254F;
      this.xrTableCell323.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell323.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell323.Name = "xrTableCell323";
      this.xrTableCell323.StylePriority.UseBackColor = false;
      this.xrTableCell323.StylePriority.UseBorderColor = false;
      this.xrTableCell323.StylePriority.UseBorders = false;
      this.xrTableCell323.StylePriority.UseFont = false;
      this.xrTableCell323.StylePriority.UseForeColor = false;
      this.xrTableCell323.StylePriority.UseTextAlignment = false;
      this.xrTableCell323.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell323.Weight = 0.5714285714285714D;
      // 
      // xrTableCell324
      // 
      this.xrTableCell324.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell324.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell324.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell324.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerAddressHistory.PostalCode")});
      this.xrTableCell324.Dpi = 254F;
      this.xrTableCell324.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell324.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell324.Name = "xrTableCell324";
      this.xrTableCell324.StylePriority.UseBackColor = false;
      this.xrTableCell324.StylePriority.UseBorderColor = false;
      this.xrTableCell324.StylePriority.UseBorders = false;
      this.xrTableCell324.StylePriority.UseFont = false;
      this.xrTableCell324.StylePriority.UseForeColor = false;
      this.xrTableCell324.StylePriority.UseTextAlignment = false;
      this.xrTableCell324.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell324.Weight = 0.57142857142857117D;
      // 
      // GroupHeader11
      // 
      this.GroupHeader11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable23});
      this.GroupHeader11.Dpi = 254F;
      this.GroupHeader11.HeightF = 245.0001F;
      this.GroupHeader11.Name = "GroupHeader11";
      // 
      // xrTable23
      // 
      this.xrTable23.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTable23.Dpi = 254F;
      this.xrTable23.LocationFloat = new DevExpress.Utils.PointFloat(0F, 25.00009F);
      this.xrTable23.Name = "xrTable23";
      this.xrTable23.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTable23.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow115,
            this.xrTableRow104,
            this.xrTableRow105});
      this.xrTable23.SizeF = new System.Drawing.SizeF(2058F, 220F);
      this.xrTable23.StylePriority.UseBorders = false;
      this.xrTable23.StylePriority.UsePadding = false;
      // 
      // xrTableRow115
      // 
      this.xrTableRow115.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell341});
      this.xrTableRow115.Dpi = 254F;
      this.xrTableRow115.Name = "xrTableRow115";
      this.xrTableRow115.Weight = 0.56761276291635043D;
      // 
      // xrTableCell341
      // 
      this.xrTableCell341.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell341.BorderColor = System.Drawing.Color.Gainsboro;
      this.xrTableCell341.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell341.CanGrow = false;
      this.xrTableCell341.Dpi = 254F;
      this.xrTableCell341.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
      this.xrTableCell341.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
      this.xrTableCell341.Name = "xrTableCell341";
      this.xrTableCell341.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      this.xrTableCell341.StylePriority.UseBackColor = false;
      this.xrTableCell341.StylePriority.UseBorderColor = false;
      this.xrTableCell341.StylePriority.UseBorders = false;
      this.xrTableCell341.StylePriority.UseFont = false;
      this.xrTableCell341.StylePriority.UseForeColor = false;
      this.xrTableCell341.StylePriority.UsePadding = false;
      this.xrTableCell341.StylePriority.UseTextAlignment = false;
      this.xrTableCell341.Text = "Address History";
      this.xrTableCell341.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell341.Weight = 4D;
      // 
      // xrTableRow104
      // 
      this.xrTableRow104.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell309});
      this.xrTableRow104.Dpi = 254F;
      this.xrTableRow104.Name = "xrTableRow104";
      this.xrTableRow104.Weight = 0.3405676669451424D;
      // 
      // xrTableCell309
      // 
      this.xrTableCell309.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(187)))), ((int)(((byte)(186)))));
      this.xrTableCell309.BorderColor = System.Drawing.Color.Black;
      this.xrTableCell309.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell309.BorderWidth = 0.5F;
      this.xrTableCell309.CanGrow = false;
      this.xrTableCell309.Dpi = 254F;
      this.xrTableCell309.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold);
      this.xrTableCell309.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell309.Multiline = true;
      this.xrTableCell309.Name = "xrTableCell309";
      this.xrTableCell309.StylePriority.UseBackColor = false;
      this.xrTableCell309.StylePriority.UseBorderColor = false;
      this.xrTableCell309.StylePriority.UseBorders = false;
      this.xrTableCell309.StylePriority.UseBorderWidth = false;
      this.xrTableCell309.StylePriority.UseFont = false;
      this.xrTableCell309.StylePriority.UseForeColor = false;
      this.xrTableCell309.StylePriority.UseTextAlignment = false;
      this.xrTableCell309.Text = "Party A\r\n";
      this.xrTableCell309.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell309.Weight = 3.9999999999999991D;
      // 
      // xrTableRow105
      // 
      this.xrTableRow105.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell310,
            this.xrTableCell312,
            this.xrTableCell313,
            this.xrTableCell314,
            this.xrTableCell315,
            this.xrTableCell316,
            this.xrTableCell317});
      this.xrTableRow105.Dpi = 254F;
      this.xrTableRow105.Name = "xrTableRow105";
      this.xrTableRow105.Weight = 0.34056760670799885D;
      // 
      // xrTableCell310
      // 
      this.xrTableCell310.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell310.BorderColor = System.Drawing.Color.White;
      this.xrTableCell310.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell310.CanGrow = false;
      this.xrTableCell310.Dpi = 254F;
      this.xrTableCell310.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell310.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell310.Name = "xrTableCell310";
      this.xrTableCell310.StylePriority.UseBackColor = false;
      this.xrTableCell310.StylePriority.UseBorderColor = false;
      this.xrTableCell310.StylePriority.UseBorders = false;
      this.xrTableCell310.StylePriority.UseFont = false;
      this.xrTableCell310.StylePriority.UseForeColor = false;
      this.xrTableCell310.StylePriority.UseTextAlignment = false;
      this.xrTableCell310.Text = "Bureau Update";
      this.xrTableCell310.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell310.Weight = 0.5714285714285714D;
      // 
      // xrTableCell312
      // 
      this.xrTableCell312.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell312.BorderColor = System.Drawing.Color.White;
      this.xrTableCell312.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell312.CanGrow = false;
      this.xrTableCell312.Dpi = 254F;
      this.xrTableCell312.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell312.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell312.Name = "xrTableCell312";
      this.xrTableCell312.StylePriority.UseBackColor = false;
      this.xrTableCell312.StylePriority.UseBorderColor = false;
      this.xrTableCell312.StylePriority.UseBorders = false;
      this.xrTableCell312.StylePriority.UseFont = false;
      this.xrTableCell312.StylePriority.UseForeColor = false;
      this.xrTableCell312.StylePriority.UseTextAlignment = false;
      this.xrTableCell312.Text = "Type";
      this.xrTableCell312.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell312.Weight = 0.5714285714285714D;
      // 
      // xrTableCell313
      // 
      this.xrTableCell313.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell313.BorderColor = System.Drawing.Color.White;
      this.xrTableCell313.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell313.CanGrow = false;
      this.xrTableCell313.Dpi = 254F;
      this.xrTableCell313.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell313.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell313.Name = "xrTableCell313";
      this.xrTableCell313.StylePriority.UseBackColor = false;
      this.xrTableCell313.StylePriority.UseBorderColor = false;
      this.xrTableCell313.StylePriority.UseBorders = false;
      this.xrTableCell313.StylePriority.UseFont = false;
      this.xrTableCell313.StylePriority.UseForeColor = false;
      this.xrTableCell313.StylePriority.UseTextAlignment = false;
      this.xrTableCell313.Text = "Line 1";
      this.xrTableCell313.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell313.Weight = 0.57142857142857129D;
      // 
      // xrTableCell314
      // 
      this.xrTableCell314.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell314.BorderColor = System.Drawing.Color.White;
      this.xrTableCell314.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell314.CanGrow = false;
      this.xrTableCell314.Dpi = 254F;
      this.xrTableCell314.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell314.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell314.Name = "xrTableCell314";
      this.xrTableCell314.StylePriority.UseBackColor = false;
      this.xrTableCell314.StylePriority.UseBorderColor = false;
      this.xrTableCell314.StylePriority.UseBorders = false;
      this.xrTableCell314.StylePriority.UseFont = false;
      this.xrTableCell314.StylePriority.UseForeColor = false;
      this.xrTableCell314.StylePriority.UseTextAlignment = false;
      this.xrTableCell314.Text = "Line 2";
      this.xrTableCell314.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell314.Weight = 0.57142857142857129D;
      // 
      // xrTableCell315
      // 
      this.xrTableCell315.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell315.BorderColor = System.Drawing.Color.White;
      this.xrTableCell315.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell315.CanGrow = false;
      this.xrTableCell315.Dpi = 254F;
      this.xrTableCell315.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell315.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell315.Name = "xrTableCell315";
      this.xrTableCell315.StylePriority.UseBackColor = false;
      this.xrTableCell315.StylePriority.UseBorderColor = false;
      this.xrTableCell315.StylePriority.UseBorders = false;
      this.xrTableCell315.StylePriority.UseFont = false;
      this.xrTableCell315.StylePriority.UseForeColor = false;
      this.xrTableCell315.StylePriority.UseTextAlignment = false;
      this.xrTableCell315.Text = "Line 3";
      this.xrTableCell315.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell315.Weight = 0.5714285714285714D;
      // 
      // xrTableCell316
      // 
      this.xrTableCell316.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell316.BorderColor = System.Drawing.Color.White;
      this.xrTableCell316.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell316.CanGrow = false;
      this.xrTableCell316.Dpi = 254F;
      this.xrTableCell316.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell316.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell316.Name = "xrTableCell316";
      this.xrTableCell316.StylePriority.UseBackColor = false;
      this.xrTableCell316.StylePriority.UseBorderColor = false;
      this.xrTableCell316.StylePriority.UseBorders = false;
      this.xrTableCell316.StylePriority.UseFont = false;
      this.xrTableCell316.StylePriority.UseForeColor = false;
      this.xrTableCell316.StylePriority.UseTextAlignment = false;
      this.xrTableCell316.Text = "Line 4";
      this.xrTableCell316.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell316.Weight = 0.5714285714285714D;
      // 
      // xrTableCell317
      // 
      this.xrTableCell317.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell317.BorderColor = System.Drawing.Color.White;
      this.xrTableCell317.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell317.CanGrow = false;
      this.xrTableCell317.Dpi = 254F;
      this.xrTableCell317.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell317.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell317.Name = "xrTableCell317";
      this.xrTableCell317.StylePriority.UseBackColor = false;
      this.xrTableCell317.StylePriority.UseBorderColor = false;
      this.xrTableCell317.StylePriority.UseBorders = false;
      this.xrTableCell317.StylePriority.UseFont = false;
      this.xrTableCell317.StylePriority.UseForeColor = false;
      this.xrTableCell317.StylePriority.UseTextAlignment = false;
      this.xrTableCell317.Text = "Postal Code";
      this.xrTableCell317.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell317.Weight = 0.57142857142857117D;
      // 
      // DetailReport1
      // 
      this.DetailReport1.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail15,
            this.GroupHeader10});
      this.DetailReport1.DataMember = "Consumer.Consumer_ConsumerTelephoneHistory";
      this.DetailReport1.Dpi = 254F;
      this.DetailReport1.Level = 8;
      this.DetailReport1.Name = "DetailReport1";
      // 
      // Detail15
      // 
      this.Detail15.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable26});
      this.Detail15.Dpi = 254F;
      this.Detail15.HeightF = 60F;
      this.Detail15.Name = "Detail15";
      // 
      // xrTable26
      // 
      this.xrTable26.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTable26.Dpi = 254F;
      this.xrTable26.KeepTogether = true;
      this.xrTable26.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
      this.xrTable26.Name = "xrTable26";
      this.xrTable26.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTable26.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow110});
      this.xrTable26.SizeF = new System.Drawing.SizeF(2058F, 60F);
      this.xrTable26.StylePriority.UseBorders = false;
      this.xrTable26.StylePriority.UsePadding = false;
      // 
      // xrTableRow110
      // 
      this.xrTableRow110.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell330,
            this.xrTableCell331,
            this.xrTableCell332});
      this.xrTableRow110.Dpi = 254F;
      this.xrTableRow110.Name = "xrTableRow110";
      this.xrTableRow110.Weight = 0.94488188976377985D;
      // 
      // xrTableCell330
      // 
      this.xrTableCell330.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell330.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell330.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell330.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerTelephoneHistory.LastUpdatedDate")});
      this.xrTableCell330.Dpi = 254F;
      this.xrTableCell330.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell330.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell330.Name = "xrTableCell330";
      this.xrTableCell330.Scripts.OnBeforePrint = "xrTableCell330_BeforePrint";
      this.xrTableCell330.StylePriority.UseBackColor = false;
      this.xrTableCell330.StylePriority.UseBorderColor = false;
      this.xrTableCell330.StylePriority.UseBorders = false;
      this.xrTableCell330.StylePriority.UseFont = false;
      this.xrTableCell330.StylePriority.UseForeColor = false;
      this.xrTableCell330.StylePriority.UseTextAlignment = false;
      this.xrTableCell330.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell330.Weight = 1.333333333333333D;
      // 
      // xrTableCell331
      // 
      this.xrTableCell331.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell331.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell331.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell331.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerTelephoneHistory.TelephoneType")});
      this.xrTableCell331.Dpi = 254F;
      this.xrTableCell331.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell331.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell331.Name = "xrTableCell331";
      this.xrTableCell331.StylePriority.UseBackColor = false;
      this.xrTableCell331.StylePriority.UseBorderColor = false;
      this.xrTableCell331.StylePriority.UseBorders = false;
      this.xrTableCell331.StylePriority.UseFont = false;
      this.xrTableCell331.StylePriority.UseForeColor = false;
      this.xrTableCell331.StylePriority.UseTextAlignment = false;
      this.xrTableCell331.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell331.Weight = 1.333333333333333D;
      // 
      // xrTableCell332
      // 
      this.xrTableCell332.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell332.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell332.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell332.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerTelephoneHistory.TelephoneNo")});
      this.xrTableCell332.Dpi = 254F;
      this.xrTableCell332.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell332.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell332.Name = "xrTableCell332";
      this.xrTableCell332.StylePriority.UseBackColor = false;
      this.xrTableCell332.StylePriority.UseBorderColor = false;
      this.xrTableCell332.StylePriority.UseBorders = false;
      this.xrTableCell332.StylePriority.UseFont = false;
      this.xrTableCell332.StylePriority.UseForeColor = false;
      this.xrTableCell332.StylePriority.UseTextAlignment = false;
      this.xrTableCell332.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell332.Weight = 1.333333333333333D;
      // 
      // GroupHeader10
      // 
      this.GroupHeader10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable25});
      this.GroupHeader10.Dpi = 254F;
      this.GroupHeader10.HeightF = 245F;
      this.GroupHeader10.Name = "GroupHeader10";
      // 
      // xrTable25
      // 
      this.xrTable25.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTable25.Dpi = 254F;
      this.xrTable25.LocationFloat = new DevExpress.Utils.PointFloat(0F, 25F);
      this.xrTable25.Name = "xrTable25";
      this.xrTable25.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTable25.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow117,
            this.xrTableRow108,
            this.xrTableRow109});
      this.xrTable25.SizeF = new System.Drawing.SizeF(2058F, 220F);
      this.xrTable25.StylePriority.UseBorders = false;
      this.xrTable25.StylePriority.UsePadding = false;
      // 
      // xrTableRow117
      // 
      this.xrTableRow117.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell343});
      this.xrTableRow117.Dpi = 254F;
      this.xrTableRow117.Name = "xrTableRow117";
      this.xrTableRow117.Weight = 0.87793912203047242D;
      // 
      // xrTableCell343
      // 
      this.xrTableCell343.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell343.BorderColor = System.Drawing.Color.Gainsboro;
      this.xrTableCell343.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell343.CanGrow = false;
      this.xrTableCell343.Dpi = 254F;
      this.xrTableCell343.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
      this.xrTableCell343.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
      this.xrTableCell343.Name = "xrTableCell343";
      this.xrTableCell343.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      this.xrTableCell343.StylePriority.UseBackColor = false;
      this.xrTableCell343.StylePriority.UseBorderColor = false;
      this.xrTableCell343.StylePriority.UseBorders = false;
      this.xrTableCell343.StylePriority.UseFont = false;
      this.xrTableCell343.StylePriority.UseForeColor = false;
      this.xrTableCell343.StylePriority.UsePadding = false;
      this.xrTableCell343.StylePriority.UseTextAlignment = false;
      this.xrTableCell343.Text = "Contact Number History";
      this.xrTableCell343.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell343.Weight = 4D;
      // 
      // xrTableRow108
      // 
      this.xrTableRow108.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell326});
      this.xrTableRow108.Dpi = 254F;
      this.xrTableRow108.Name = "xrTableRow108";
      this.xrTableRow108.Weight = 0.52676344800174157D;
      // 
      // xrTableCell326
      // 
      this.xrTableCell326.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(187)))), ((int)(((byte)(186)))));
      this.xrTableCell326.BorderColor = System.Drawing.Color.Black;
      this.xrTableCell326.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell326.BorderWidth = 0.5F;
      this.xrTableCell326.CanGrow = false;
      this.xrTableCell326.Dpi = 254F;
      this.xrTableCell326.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold);
      this.xrTableCell326.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell326.Name = "xrTableCell326";
      this.xrTableCell326.StylePriority.UseBackColor = false;
      this.xrTableCell326.StylePriority.UseBorderColor = false;
      this.xrTableCell326.StylePriority.UseBorders = false;
      this.xrTableCell326.StylePriority.UseBorderWidth = false;
      this.xrTableCell326.StylePriority.UseFont = false;
      this.xrTableCell326.StylePriority.UseForeColor = false;
      this.xrTableCell326.StylePriority.UseTextAlignment = false;
      this.xrTableCell326.Text = "Party A";
      this.xrTableCell326.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell326.Weight = 3.9999999999999991D;
      // 
      // xrTableRow109
      // 
      this.xrTableRow109.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell327,
            this.xrTableCell328,
            this.xrTableCell329});
      this.xrTableRow109.Dpi = 254F;
      this.xrTableRow109.Name = "xrTableRow109";
      this.xrTableRow109.Weight = 0.5267635149831813D;
      // 
      // xrTableCell327
      // 
      this.xrTableCell327.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell327.BorderColor = System.Drawing.Color.White;
      this.xrTableCell327.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell327.CanGrow = false;
      this.xrTableCell327.Dpi = 254F;
      this.xrTableCell327.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell327.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell327.Name = "xrTableCell327";
      this.xrTableCell327.StylePriority.UseBackColor = false;
      this.xrTableCell327.StylePriority.UseBorderColor = false;
      this.xrTableCell327.StylePriority.UseBorders = false;
      this.xrTableCell327.StylePriority.UseFont = false;
      this.xrTableCell327.StylePriority.UseForeColor = false;
      this.xrTableCell327.StylePriority.UseTextAlignment = false;
      this.xrTableCell327.Text = "Bureau Update";
      this.xrTableCell327.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell327.Weight = 1.333333333333333D;
      // 
      // xrTableCell328
      // 
      this.xrTableCell328.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell328.BorderColor = System.Drawing.Color.White;
      this.xrTableCell328.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell328.CanGrow = false;
      this.xrTableCell328.Dpi = 254F;
      this.xrTableCell328.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell328.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell328.Name = "xrTableCell328";
      this.xrTableCell328.StylePriority.UseBackColor = false;
      this.xrTableCell328.StylePriority.UseBorderColor = false;
      this.xrTableCell328.StylePriority.UseBorders = false;
      this.xrTableCell328.StylePriority.UseFont = false;
      this.xrTableCell328.StylePriority.UseForeColor = false;
      this.xrTableCell328.StylePriority.UseTextAlignment = false;
      this.xrTableCell328.Text = "Type";
      this.xrTableCell328.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell328.Weight = 1.333333333333333D;
      // 
      // xrTableCell329
      // 
      this.xrTableCell329.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell329.BorderColor = System.Drawing.Color.White;
      this.xrTableCell329.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell329.CanGrow = false;
      this.xrTableCell329.Dpi = 254F;
      this.xrTableCell329.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell329.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell329.Name = "xrTableCell329";
      this.xrTableCell329.StylePriority.UseBackColor = false;
      this.xrTableCell329.StylePriority.UseBorderColor = false;
      this.xrTableCell329.StylePriority.UseBorders = false;
      this.xrTableCell329.StylePriority.UseFont = false;
      this.xrTableCell329.StylePriority.UseForeColor = false;
      this.xrTableCell329.StylePriority.UseTextAlignment = false;
      this.xrTableCell329.Text = "Telephone No.";
      this.xrTableCell329.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell329.Weight = 1.333333333333333D;
      // 
      // DetailReport2
      // 
      this.DetailReport2.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail16,
            this.GroupHeader9});
      this.DetailReport2.DataMember = "Consumer.Consumer_ConsumerEmploymentHistory";
      this.DetailReport2.Dpi = 254F;
      this.DetailReport2.Level = 10;
      this.DetailReport2.Name = "DetailReport2";
      // 
      // Detail16
      // 
      this.Detail16.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable28});
      this.Detail16.Dpi = 254F;
      this.Detail16.HeightF = 60F;
      this.Detail16.Name = "Detail16";
      // 
      // xrTable28
      // 
      this.xrTable28.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTable28.Dpi = 254F;
      this.xrTable28.KeepTogether = true;
      this.xrTable28.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
      this.xrTable28.Name = "xrTable28";
      this.xrTable28.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTable28.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow114});
      this.xrTable28.SizeF = new System.Drawing.SizeF(2058F, 60F);
      this.xrTable28.StylePriority.UseBorders = false;
      this.xrTable28.StylePriority.UsePadding = false;
      // 
      // xrTableRow114
      // 
      this.xrTableRow114.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell338,
            this.xrTableCell339,
            this.xrTableCell340});
      this.xrTableRow114.Dpi = 254F;
      this.xrTableRow114.Name = "xrTableRow114";
      this.xrTableRow114.Weight = 0.94488188976377985D;
      // 
      // xrTableCell338
      // 
      this.xrTableCell338.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell338.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell338.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell338.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerEmploymentHistory.LastUpdatedDate")});
      this.xrTableCell338.Dpi = 254F;
      this.xrTableCell338.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell338.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell338.Name = "xrTableCell338";
      this.xrTableCell338.Scripts.OnBeforePrint = "xrTableCell338_BeforePrint";
      this.xrTableCell338.StylePriority.UseBackColor = false;
      this.xrTableCell338.StylePriority.UseBorderColor = false;
      this.xrTableCell338.StylePriority.UseBorders = false;
      this.xrTableCell338.StylePriority.UseFont = false;
      this.xrTableCell338.StylePriority.UseForeColor = false;
      this.xrTableCell338.StylePriority.UseTextAlignment = false;
      this.xrTableCell338.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell338.Weight = 1.333333333333333D;
      // 
      // xrTableCell339
      // 
      this.xrTableCell339.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell339.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell339.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell339.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerEmploymentHistory.EmployerDetail")});
      this.xrTableCell339.Dpi = 254F;
      this.xrTableCell339.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell339.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell339.Name = "xrTableCell339";
      this.xrTableCell339.StylePriority.UseBackColor = false;
      this.xrTableCell339.StylePriority.UseBorderColor = false;
      this.xrTableCell339.StylePriority.UseBorders = false;
      this.xrTableCell339.StylePriority.UseFont = false;
      this.xrTableCell339.StylePriority.UseForeColor = false;
      this.xrTableCell339.StylePriority.UseTextAlignment = false;
      this.xrTableCell339.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell339.Weight = 1.333333333333333D;
      // 
      // xrTableCell340
      // 
      this.xrTableCell340.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell340.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell340.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell340.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer.Consumer_ConsumerEmploymentHistory.Designation")});
      this.xrTableCell340.Dpi = 254F;
      this.xrTableCell340.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell340.ForeColor = System.Drawing.Color.DimGray;
      this.xrTableCell340.Name = "xrTableCell340";
      this.xrTableCell340.StylePriority.UseBackColor = false;
      this.xrTableCell340.StylePriority.UseBorderColor = false;
      this.xrTableCell340.StylePriority.UseBorders = false;
      this.xrTableCell340.StylePriority.UseFont = false;
      this.xrTableCell340.StylePriority.UseForeColor = false;
      this.xrTableCell340.StylePriority.UseTextAlignment = false;
      this.xrTableCell340.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell340.Weight = 1.333333333333333D;
      // 
      // GroupHeader9
      // 
      this.GroupHeader9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable27});
      this.GroupHeader9.Dpi = 254F;
      this.GroupHeader9.HeightF = 245F;
      this.GroupHeader9.Name = "GroupHeader9";
      // 
      // xrTable27
      // 
      this.xrTable27.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTable27.Dpi = 254F;
      this.xrTable27.KeepTogether = true;
      this.xrTable27.LocationFloat = new DevExpress.Utils.PointFloat(0F, 25F);
      this.xrTable27.Name = "xrTable27";
      this.xrTable27.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTable27.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow119,
            this.xrTableRow112,
            this.xrTableRow113});
      this.xrTable27.SizeF = new System.Drawing.SizeF(2058F, 220F);
      this.xrTable27.StylePriority.UseBorders = false;
      this.xrTable27.StylePriority.UsePadding = false;
      // 
      // xrTableRow119
      // 
      this.xrTableRow119.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell345});
      this.xrTableRow119.Dpi = 254F;
      this.xrTableRow119.Name = "xrTableRow119";
      this.xrTableRow119.Weight = 0.92005632545374061D;
      // 
      // xrTableCell345
      // 
      this.xrTableCell345.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell345.BorderColor = System.Drawing.Color.Gainsboro;
      this.xrTableCell345.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell345.CanGrow = false;
      this.xrTableCell345.Dpi = 254F;
      this.xrTableCell345.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
      this.xrTableCell345.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(41)))), ((int)(((byte)(111)))));
      this.xrTableCell345.Name = "xrTableCell345";
      this.xrTableCell345.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      this.xrTableCell345.StylePriority.UseBackColor = false;
      this.xrTableCell345.StylePriority.UseBorderColor = false;
      this.xrTableCell345.StylePriority.UseBorders = false;
      this.xrTableCell345.StylePriority.UseFont = false;
      this.xrTableCell345.StylePriority.UseForeColor = false;
      this.xrTableCell345.StylePriority.UsePadding = false;
      this.xrTableCell345.StylePriority.UseTextAlignment = false;
      this.xrTableCell345.Text = "Employment History";
      this.xrTableCell345.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell345.Weight = 4D;
      // 
      // xrTableRow112
      // 
      this.xrTableRow112.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell334});
      this.xrTableRow112.Dpi = 254F;
      this.xrTableRow112.Name = "xrTableRow112";
      this.xrTableRow112.Weight = 0.55203377214927607D;
      // 
      // xrTableCell334
      // 
      this.xrTableCell334.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(187)))), ((int)(((byte)(186)))));
      this.xrTableCell334.BorderColor = System.Drawing.Color.Black;
      this.xrTableCell334.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell334.BorderWidth = 0.5F;
      this.xrTableCell334.CanGrow = false;
      this.xrTableCell334.Dpi = 254F;
      this.xrTableCell334.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold);
      this.xrTableCell334.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell334.Name = "xrTableCell334";
      this.xrTableCell334.StylePriority.UseBackColor = false;
      this.xrTableCell334.StylePriority.UseBorderColor = false;
      this.xrTableCell334.StylePriority.UseBorders = false;
      this.xrTableCell334.StylePriority.UseBorderWidth = false;
      this.xrTableCell334.StylePriority.UseFont = false;
      this.xrTableCell334.StylePriority.UseForeColor = false;
      this.xrTableCell334.StylePriority.UseTextAlignment = false;
      this.xrTableCell334.Text = "Party A";
      this.xrTableCell334.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell334.Weight = 3.9999999999999991D;
      // 
      // xrTableRow113
      // 
      this.xrTableRow113.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell335,
            this.xrTableCell336,
            this.xrTableCell337});
      this.xrTableRow113.Dpi = 254F;
      this.xrTableRow113.Name = "xrTableRow113";
      this.xrTableRow113.Weight = 0.552033772149276D;
      // 
      // xrTableCell335
      // 
      this.xrTableCell335.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell335.BorderColor = System.Drawing.Color.White;
      this.xrTableCell335.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell335.CanGrow = false;
      this.xrTableCell335.Dpi = 254F;
      this.xrTableCell335.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell335.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell335.Name = "xrTableCell335";
      this.xrTableCell335.StylePriority.UseBackColor = false;
      this.xrTableCell335.StylePriority.UseBorderColor = false;
      this.xrTableCell335.StylePriority.UseBorders = false;
      this.xrTableCell335.StylePriority.UseFont = false;
      this.xrTableCell335.StylePriority.UseForeColor = false;
      this.xrTableCell335.StylePriority.UseTextAlignment = false;
      this.xrTableCell335.Text = "Bureau Update";
      this.xrTableCell335.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell335.Weight = 1.333333333333333D;
      // 
      // xrTableCell336
      // 
      this.xrTableCell336.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell336.BorderColor = System.Drawing.Color.White;
      this.xrTableCell336.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell336.CanGrow = false;
      this.xrTableCell336.Dpi = 254F;
      this.xrTableCell336.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell336.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell336.Name = "xrTableCell336";
      this.xrTableCell336.StylePriority.UseBackColor = false;
      this.xrTableCell336.StylePriority.UseBorderColor = false;
      this.xrTableCell336.StylePriority.UseBorders = false;
      this.xrTableCell336.StylePriority.UseFont = false;
      this.xrTableCell336.StylePriority.UseForeColor = false;
      this.xrTableCell336.StylePriority.UseTextAlignment = false;
      this.xrTableCell336.Text = "Employer";
      this.xrTableCell336.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell336.Weight = 1.333333333333333D;
      // 
      // xrTableCell337
      // 
      this.xrTableCell337.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.xrTableCell337.BorderColor = System.Drawing.Color.White;
      this.xrTableCell337.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell337.CanGrow = false;
      this.xrTableCell337.Dpi = 254F;
      this.xrTableCell337.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell337.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell337.Name = "xrTableCell337";
      this.xrTableCell337.StylePriority.UseBackColor = false;
      this.xrTableCell337.StylePriority.UseBorderColor = false;
      this.xrTableCell337.StylePriority.UseBorders = false;
      this.xrTableCell337.StylePriority.UseFont = false;
      this.xrTableCell337.StylePriority.UseForeColor = false;
      this.xrTableCell337.StylePriority.UseTextAlignment = false;
      this.xrTableCell337.Text = "Designation";
      this.xrTableCell337.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell337.Weight = 1.333333333333333D;
      // 
      // LinkageReport_NewLook
      // 
      this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.SubscriberInputDetailsOther,
            this.SubscriberInutDetails,
            this.ConfirmedLinkage,
            this.ConsumerOtherDetail,
            this.ConsumerOtherDetail_Address,
            this.ConsumerOtherDetail_Telephone,
            this.ConsumerOtherDetail_Employment,
            this.ReportHeader,
            this.PageFooter,
            this.PersonalDetail_Summary,
            this.PersonalDetails_Details,
            this.DetailReport,
            this.DetailReport1,
            this.DetailReport2});
      this.Dpi = 254F;
      this.Margins = new System.Drawing.Printing.Margins(20, 20, 41, 91);
      this.PageHeight = 2970;
      this.PageWidth = 2100;
      this.PaperKind = System.Drawing.Printing.PaperKind.A4;
      this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
      this.ScriptsSource = resources.GetString("$this.ScriptsSource");
      this.Version = "14.2";
      this.XmlDataPath = "D:\\Projects\\shbs\\DevExpress\\XML Datasource\\Sample Linkage report xml.xml";
      ((System.ComponentModel.ISupportInitialize)(this.xrTable19)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable20)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable18)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable21)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable22)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable24)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable23)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable26)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable25)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable28)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable27)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

    }

    #endregion

    private DevExpress.XtraReports.UI.DetailBand Detail;
    private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
    private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
    private DevExpress.XtraReports.UI.DetailReportBand SubscriberInputDetailsOther;
    private DevExpress.XtraReports.UI.DetailBand Detail1;
    private DevExpress.XtraReports.UI.DetailReportBand SubscriberInutDetails;
    private DevExpress.XtraReports.UI.DetailBand Detail2;
    private DevExpress.XtraReports.UI.XRTable xrTable2;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
    private DevExpress.XtraReports.UI.DetailReportBand ConfirmedLinkage;
    private DevExpress.XtraReports.UI.DetailBand Detail3;
    private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
    private DevExpress.XtraReports.UI.DetailReportBand ConsumerDetail;
    private DevExpress.XtraReports.UI.DetailBand Detail4;
    private DevExpress.XtraReports.UI.DetailReportBand ConsumerOtherDetail;
    private DevExpress.XtraReports.UI.DetailBand Detail5;
    private DevExpress.XtraReports.UI.DetailReportBand ConsumerDetail_Address;
    private DevExpress.XtraReports.UI.DetailBand Detail6;
    private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader2;
    private DevExpress.XtraReports.UI.DetailReportBand ConsumerOtherDetail_Address;
    private DevExpress.XtraReports.UI.DetailBand Detail7;
    private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader3;
    private DevExpress.XtraReports.UI.DetailReportBand ConsumerDetail_Telephone;
    private DevExpress.XtraReports.UI.DetailBand Detail8;
    private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader4;
    private DevExpress.XtraReports.UI.DetailReportBand ConsumerOtherDetail_Telephone;
    private DevExpress.XtraReports.UI.DetailBand Detail9;
    private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader5;
    private DevExpress.XtraReports.UI.DetailReportBand ConsumerDetail_Employment;
    private DevExpress.XtraReports.UI.DetailBand Detail10;
    private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader7;
    private DevExpress.XtraReports.UI.DetailReportBand ConsumerOtherDetail_Employment;
    private DevExpress.XtraReports.UI.DetailBand Detail11;
    private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader6;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell167;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow45;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell166;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell168;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow46;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell172;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell173;
    private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
    private DevExpress.XtraReports.UI.XRLabel xrLabel21;
    private DevExpress.XtraReports.UI.XRLabel xrLabel22;
    private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
    private DevExpress.XtraReports.UI.XRTable xrTable19;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow47;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow48;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow49;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow50;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell169;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow51;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell170;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell174;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow52;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell175;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell176;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow53;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell177;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell178;
    private DevExpress.XtraReports.UI.XRTable xrTable20;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow55;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell183;
    private DevExpress.XtraReports.UI.XRTable xrTable3;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell180;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell179;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell182;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell181;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
    private DevExpress.XtraReports.UI.XRTable xrTable4;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow54;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow56;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell184;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell185;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell186;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow61;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell203;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell204;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell205;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell206;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow60;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell199;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell200;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell201;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell202;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow59;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell195;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell196;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell197;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell198;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow58;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell191;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell192;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell193;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell194;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow57;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell187;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell188;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell189;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell190;
    private DevExpress.XtraReports.UI.XRTable xrTable5;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell116;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell117;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell120;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell121;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell123;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell124;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell126;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell127;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell128;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell129;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell130;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell132;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell133;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell134;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell135;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell136;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell137;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell139;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell140;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell141;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell143;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell144;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow41;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell145;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell146;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell147;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell148;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow62;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell149;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell150;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell207;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell208;
    private DevExpress.XtraReports.UI.XRTable xrTable7;
    private DevExpress.XtraReports.UI.XRTable xrTable8;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
    private DevExpress.XtraReports.UI.XRTable xrTable11;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow67;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell96;
    private DevExpress.XtraReports.UI.XRTable xrTable12;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell98;
    private DevExpress.XtraReports.UI.XRTable xrTable15;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow74;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell111;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
    private DevExpress.XtraReports.UI.XRTable xrTable13;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow68;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow69;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell99;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow70;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell102;
    private DevExpress.XtraReports.UI.XRTable xrTable16;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow42;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell151;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell152;
    private DevExpress.XtraReports.UI.XRTable xrTable14;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow71;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow72;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow73;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
    private DevExpress.XtraReports.UI.XRTable xrTable6;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
    private DevExpress.XtraReports.UI.XRTable xrTable9;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow63;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
    private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
    private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
    private DevExpress.XtraReports.UI.XRLabel xrLabel95;
    private DevExpress.XtraReports.UI.XRLabel xrLabel16;
    private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo2;
    private DevExpress.XtraReports.UI.XRPanel xrPanel1;
    private DevExpress.XtraReports.UI.DetailReportBand PersonalDetail_Summary;
    private DevExpress.XtraReports.UI.DetailBand Detail12;
    private DevExpress.XtraReports.UI.XRTable xrTable17;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell103;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell153;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell154;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell155;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell156;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell157;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell158;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell159;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell160;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell161;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell162;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell163;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell164;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell165;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow43;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell209;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell210;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell211;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell212;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow75;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell213;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell214;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell215;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell216;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow76;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell217;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell218;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell219;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell220;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow77;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell221;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell222;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell223;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell224;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow78;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell225;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell226;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell227;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell228;
    private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader8;
    private DevExpress.XtraReports.UI.XRTable xrTable18;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow79;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell229;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow81;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell233;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell234;
    private DevExpress.XtraReports.UI.DetailReportBand PersonalDetails_Details;
    private DevExpress.XtraReports.UI.DetailBand Detail13;
    private DevExpress.XtraReports.UI.XRTable xrTable21;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow82;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell232;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell235;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell236;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell237;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow83;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell238;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell239;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell240;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell241;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow84;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell242;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell243;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell244;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell245;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow85;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell246;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell247;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell248;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell249;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow86;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell250;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell251;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell252;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell253;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow87;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell254;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell255;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell256;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell257;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow88;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell258;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell259;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell260;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell261;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow89;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell262;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell263;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell264;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell265;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow90;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell266;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell267;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell268;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell269;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow102;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell311;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell274;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow96;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell287;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell288;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell289;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell290;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow100;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell303;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell304;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell305;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell306;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow99;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell299;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell300;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell301;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell302;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow98;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell295;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell296;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell297;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell298;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow97;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell291;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell292;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell293;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell294;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow95;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell283;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell284;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell285;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell286;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow94;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell279;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell280;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell281;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell282;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow93;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell275;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell276;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell277;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell278;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow101;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell273;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell307;
    private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader1;
    private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
    private DevExpress.XtraReports.UI.DetailBand Detail14;
    private DevExpress.XtraReports.UI.XRTable xrTable24;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow106;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell318;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell319;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell320;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell321;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell322;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell323;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell324;
    private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader11;
    private DevExpress.XtraReports.UI.XRTable xrTable23;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow104;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell309;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow105;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell310;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell312;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell313;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell314;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell315;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell316;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell317;
    private DevExpress.XtraReports.UI.DetailReportBand DetailReport1;
    private DevExpress.XtraReports.UI.DetailBand Detail15;
    private DevExpress.XtraReports.UI.XRTable xrTable26;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow110;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell330;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell331;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell332;
    private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader10;
    private DevExpress.XtraReports.UI.XRTable xrTable25;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow108;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell326;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow109;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell327;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell328;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell329;
    private DevExpress.XtraReports.UI.DetailReportBand DetailReport2;
    private DevExpress.XtraReports.UI.DetailBand Detail16;
    private DevExpress.XtraReports.UI.XRTable xrTable28;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow114;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell338;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell339;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell340;
    private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader9;
    private DevExpress.XtraReports.UI.XRTable xrTable27;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow112;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell334;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow113;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell335;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell336;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell337;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow115;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell341;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
    private DevExpress.XtraReports.UI.XRTable xrTable1;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow116;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell342;
    private DevExpress.XtraReports.UI.XRTable xrTable10;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow66;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow118;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell344;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow117;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell343;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow119;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell345;
    private DevExpress.XtraReports.UI.XRLine xrLine2;
    private DevExpress.XtraReports.UI.XRTable xrTable22;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow91;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell270;
  }
}
