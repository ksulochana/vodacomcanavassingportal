﻿namespace XDS.REPORTS
{
  partial class Test_MVV
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Test_MVV));
      DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
      DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
      DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
      this.Detail = new DevExpress.XtraReports.UI.DetailBand();
      this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
      this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
      this.sqlDataSource1 = new DevExpress.DataAccess.Sql.SqlDataSource();
      this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
      this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
      this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
      this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
      this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
      this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
      this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
      this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
      this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
      this.xrLabel95 = new DevExpress.XtraReports.UI.XRLabel();
      this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
      this.SumamryBlockA = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail12 = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable17 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow100 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell235 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell236 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow66 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow101 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell237 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell238 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow126 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell287 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell288 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow102 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell239 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell240 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow103 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell241 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell242 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow104 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell243 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell244 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow105 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell245 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell246 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow106 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell247 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell248 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow107 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell249 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell250 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow108 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell251 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell252 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow109 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell253 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell254 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow110 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell255 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell256 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow111 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell257 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell258 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow71 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow65 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblTest = new DevExpress.XtraReports.UI.XRTableCell();
      this.SummaryBlockB = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail14 = new DevExpress.XtraReports.UI.DetailBand();
      this.GroupHeader11 = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable12 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow72 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell178 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow48 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell337 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow50 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell207 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell339 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow52 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell281 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell341 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow88 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell214 = new DevExpress.XtraReports.UI.XRTableCell();
      this.SummaryBlockC = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail15 = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable18 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow113 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell261 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell262 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow86 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell209 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow114 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell263 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell264 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow115 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell265 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell266 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow116 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell267 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell268 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow117 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell269 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell270 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow118 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell271 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell272 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow119 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell273 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell274 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow120 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell275 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell276 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow53 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow121 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell277 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell278 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow122 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell279 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell280 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow87 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell212 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTable14 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow73 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow85 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell210 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow75 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell151 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow76 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow77 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell156 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow78 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell157 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell158 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow79 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow80 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell161 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow81 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell163 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow82 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell165 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow83 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow84 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell175 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow74 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell208 = new DevExpress.XtraReports.UI.XRTableCell();
      this.SumamryBlockD = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail16 = new DevExpress.XtraReports.UI.DetailBand();
      this.GroupHeader8 = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable22 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow49 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow89 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow96 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell229 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell230 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell231 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell232 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow95 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell225 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell226 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell227 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell228 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow94 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell221 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell222 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell223 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell224 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow91 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell217 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell218 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell219 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell220 = new DevExpress.XtraReports.UI.XRTableCell();
      this.DetailReport12 = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail8 = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable16 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow64 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
      this.GroupHeader17 = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable27 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow51 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow62 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell139 = new DevExpress.XtraReports.UI.XRTableCell();
      this.CreditAccountInformation_CAS = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail13 = new DevExpress.XtraReports.UI.DetailBand();
      this.DetailReport2 = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail18 = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable23 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow112 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell291 = new DevExpress.XtraReports.UI.XRTableCell();
      this.DetailReport1 = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail17 = new DevExpress.XtraReports.UI.DetailBand();
      this.GroupHeader9 = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.tblMonthlyPaymentH = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow128 = new DevExpress.XtraReports.UI.XRTableRow();
      this.lblcompanyMP = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblM01 = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblM02 = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblM03 = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblM04 = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblM05 = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblM06 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell259 = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblM08 = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblM09 = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblM10 = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblM11 = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblM12 = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblM13 = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblM14 = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblM15 = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblM16 = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblM17 = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblM18 = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblM19 = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblM20 = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblM21 = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblM22 = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblM23 = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblM24 = new DevExpress.XtraReports.UI.XRTableCell();
      this.DetailReport3 = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail19 = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable24 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow134 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell290 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell292 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell293 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell307 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell308 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell309 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell310 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell311 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell312 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell313 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell314 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell318 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell319 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell320 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell321 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell322 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell323 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell324 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell325 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell326 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell327 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell328 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell329 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell331 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell332 = new DevExpress.XtraReports.UI.XRTableCell();
      this.DetailReport4 = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail20 = new DevExpress.XtraReports.UI.DetailBand();
      this.tblConsumerDefinitionC = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow133 = new DevExpress.XtraReports.UI.XRTableRow();
      this.lblDefDesc = new DevExpress.XtraReports.UI.XRTableCell();
      this.lbDefCode = new DevExpress.XtraReports.UI.XRTableCell();
      this.GroupHeader10 = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable25 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow135 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell260 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell333 = new DevExpress.XtraReports.UI.XRTableCell();
      this.DetailReport10 = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail6 = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable51 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow179 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell515 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell516 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell517 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell518 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell519 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell520 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell521 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell522 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell523 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell524 = new DevExpress.XtraReports.UI.XRTableCell();
      this.GroupHeader6 = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable50 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow173 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell491 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell493 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow174 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell495 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow177 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell497 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow178 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell504 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell505 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell506 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell508 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell509 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell510 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell511 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell512 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell513 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell514 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
      this.Detail11 = new DevExpress.XtraReports.UI.DetailBand();
      this.MaritalStatus = new DevExpress.XtraReports.UI.DetailReportBand();
      this.CreditAccountInformation_NLR = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail21 = new DevExpress.XtraReports.UI.DetailBand();
      this.DetailReport5 = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail22 = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable29 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow141 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell358 = new DevExpress.XtraReports.UI.XRTableCell();
      this.DetailReport6 = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail23 = new DevExpress.XtraReports.UI.DetailBand();
      this.GroupHeader12 = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable30 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow143 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell360 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell361 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell362 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell363 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell364 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell365 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell366 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell367 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell368 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell369 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell370 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell371 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell372 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell373 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell374 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell375 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell376 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell377 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell378 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell379 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell380 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell381 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell382 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell383 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell384 = new DevExpress.XtraReports.UI.XRTableCell();
      this.DetailReport7 = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail24 = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable31 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow144 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell385 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell386 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell387 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell388 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell389 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell390 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell391 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell392 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell393 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell394 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell395 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell396 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell397 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell398 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell399 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell400 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell401 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell402 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell403 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell404 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell405 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell406 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell407 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell408 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell409 = new DevExpress.XtraReports.UI.XRTableCell();
      this.DetailReport8 = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail25 = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable33 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow146 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell412 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell413 = new DevExpress.XtraReports.UI.XRTableCell();
      this.GroupHeader13 = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable32 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow145 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell410 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell411 = new DevExpress.XtraReports.UI.XRTableCell();
      this.DetailReport11 = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail7 = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable21 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow154 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell316 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell317 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell507 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell525 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell526 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell527 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell528 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell529 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell530 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell531 = new DevExpress.XtraReports.UI.XRTableCell();
      this.GroupHeader7 = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable19 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow70 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell289 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell294 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow127 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell295 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow129 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell296 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow130 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell297 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow131 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell298 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell299 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell300 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell301 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell302 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell303 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell304 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell305 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell306 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell315 = new DevExpress.XtraReports.UI.XRTableCell();
      this.PropertyInfo = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail26 = new DevExpress.XtraReports.UI.DetailBand();
      this.tblPropInterests = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow148 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell416 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow149 = new DevExpress.XtraReports.UI.XRTableRow();
      this.lblDeedNo = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblDDeedNo = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblsiteno = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblDsiteno = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow150 = new DevExpress.XtraReports.UI.XRTableRow();
      this.lblDeedsoffice = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblDDeedsoffice = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblPhyadd = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblDPhyadd = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow151 = new DevExpress.XtraReports.UI.XRTableRow();
      this.lblPropType = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblDPropType = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblSize = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblDSize = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow152 = new DevExpress.XtraReports.UI.XRTableRow();
      this.lblPurchasedate = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblDPurchasedate = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblPurchaseprice = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblDPurchaseprice = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow153 = new DevExpress.XtraReports.UI.XRTableRow();
      this.lblshareperc = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblDBondNo = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblHolder = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblDHolder = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow155 = new DevExpress.XtraReports.UI.XRTableRow();
      this.lblBondNo = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblDBondAmount = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblBondAmount = new DevExpress.XtraReports.UI.XRTableCell();
      this.lbl50 = new DevExpress.XtraReports.UI.XRTableCell();
      this.GroupHeader14 = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable34 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow147 = new DevExpress.XtraReports.UI.XRTableRow();
      this.lblPropInterestsH = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell414 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow175 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell415 = new DevExpress.XtraReports.UI.XRTableCell();
      this.CIPC = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail27 = new DevExpress.XtraReports.UI.DetailBand();
      this.tblDirInfo = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow157 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell420 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow158 = new DevExpress.XtraReports.UI.XRTableRow();
      this.lblCurrentPost = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblDCurrentPost = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblInceptionDate = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblDAppDate = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow159 = new DevExpress.XtraReports.UI.XRTableRow();
      this.lblCompanyName = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblDCompanyName = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblRegNo = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblDRegNo = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow160 = new DevExpress.XtraReports.UI.XRTableRow();
      this.lblCompanyAdd = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblDPhysicalAdd = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblPhoneNo = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblDPhoneNo = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow161 = new DevExpress.XtraReports.UI.XRTableRow();
      this.lblIndustryCatg = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblDIndCateg = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblDirectorStatus = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblDDirectorStatus = new DevExpress.XtraReports.UI.XRTableCell();
      this.GroupHeader15 = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable35 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow156 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell417 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell418 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow176 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell419 = new DevExpress.XtraReports.UI.XRTableCell();
      this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail28 = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable37 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow164 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell424 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow165 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell425 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell428 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow166 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell429 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell432 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow167 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell433 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell436 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow171 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell434 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell435 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow170 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell430 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell431 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow169 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell426 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell427 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow168 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell437 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell440 = new DevExpress.XtraReports.UI.XRTableCell();
      this.GroupHeader16 = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable36 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow162 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell421 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell422 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow163 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell423 = new DevExpress.XtraReports.UI.XRTableCell();
      this.TelephoneLinkage = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail29 = new DevExpress.XtraReports.UI.DetailBand();
      this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.tblLikagesHC = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
      this.HomeLinkage = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
      this.GroupHeader2 = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
      this.WorkLinkage = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
      this.GroupHeader3 = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
      this.CellularLinkage = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail3 = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
      this.GroupHeader4 = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
      this.Adverse = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail4 = new DevExpress.XtraReports.UI.DetailBand();
      this.PublicDomain_Adverse = new DevExpress.XtraReports.UI.DetailReportBand();
      this.AdverseDetail = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable13 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
      this.AdverseHeader = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
      this.ReportHeader1 = new DevExpress.XtraReports.UI.ReportHeaderBand();
      this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
      this.ReportHeader2 = new DevExpress.XtraReports.UI.ReportHeaderBand();
      this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
      this.PublicDomain_Default = new DevExpress.XtraReports.UI.DetailReportBand();
      this.DefaultDetail = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable20 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
      this.DefaultHeader = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable15 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
      this.PublicDomain_Judgements = new DevExpress.XtraReports.UI.DetailReportBand();
      this.JudgementDetail = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable39 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell168 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell167 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
      this.JudgementHeader = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable38 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell166 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
      this.CourtNotice_Admin = new DevExpress.XtraReports.UI.DetailReportBand();
      this.AdminDetail = new DevExpress.XtraReports.UI.DetailBand();
      this.ReportHeader3 = new DevExpress.XtraReports.UI.ReportHeaderBand();
      this.xrTable40 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell173 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell179 = new DevExpress.XtraReports.UI.XRTableCell();
      this.Sequestration = new DevExpress.XtraReports.UI.DetailReportBand();
      this.SequestrationDetail = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable44 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell444 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell445 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell446 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell447 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell448 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell449 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell450 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell451 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell452 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell453 = new DevExpress.XtraReports.UI.XRTableCell();
      this.SequestrationHeader = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable43 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell191 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell192 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell193 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell194 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell195 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell196 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell197 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell198 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell199 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell442 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell443 = new DevExpress.XtraReports.UI.XRTableCell();
      this.Rehabilitation = new DevExpress.XtraReports.UI.DetailReportBand();
      this.RehabilitationDetail = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable46 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow56 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell465 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell466 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell467 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell468 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell469 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell470 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell471 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell472 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell473 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell474 = new DevExpress.XtraReports.UI.XRTableCell();
      this.RehabilitationHeader = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable45 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow54 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell454 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow55 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell455 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell456 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell457 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell458 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell459 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell460 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell461 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell462 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell463 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell464 = new DevExpress.XtraReports.UI.XRTableCell();
      this.DebtReview = new DevExpress.XtraReports.UI.DetailReportBand();
      this.DebtReviewDetail = new DevExpress.XtraReports.UI.DetailBand();
      this.tblDbtReviewD = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow59 = new DevExpress.XtraReports.UI.XRTableRow();
      this.lblDbtReviewDate = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblDbtReviewDateD = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow60 = new DevExpress.XtraReports.UI.XRTableRow();
      this.lblCounsellorName = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblCounsellorNameD = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow61 = new DevExpress.XtraReports.UI.XRTableRow();
      this.lblCounsellorTelephoneNo = new DevExpress.XtraReports.UI.XRTableCell();
      this.lblCounsellorTelephoneNoD = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow63 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell486 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell487 = new DevExpress.XtraReports.UI.XRTableCell();
      this.DebtReviewHeader = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable47 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow57 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell475 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow58 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell476 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell477 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell478 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell479 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell480 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell481 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell482 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell483 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell484 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell485 = new DevExpress.XtraReports.UI.XRTableCell();
      this.XDSPayment = new DevExpress.XtraReports.UI.DetailReportBand();
      this.XDSPaymentDetail = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable49 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow69 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell499 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell500 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell501 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell502 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell503 = new DevExpress.XtraReports.UI.XRTableCell();
      this.XDSPaymentHeader = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable48 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow67 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell488 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow68 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell490 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell492 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell494 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell496 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell498 = new DevExpress.XtraReports.UI.XRTableCell();
      this.DetailReport9 = new DevExpress.XtraReports.UI.DetailReportBand();
      this.Detail5 = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable42 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell200 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell201 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell202 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell203 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell204 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell205 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell206 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell438 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell439 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell441 = new DevExpress.XtraReports.UI.XRTableCell();
      this.GroupHeader5 = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.xrTable41 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell181 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell182 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell183 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell186 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell188 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell190 = new DevExpress.XtraReports.UI.XRTableCell();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable18)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable22)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable27)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable23)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.tblMonthlyPaymentH)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable24)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.tblConsumerDefinitionC)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable25)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable51)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable50)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable29)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable30)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable31)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable33)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable32)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable21)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable19)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.tblPropInterests)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable34)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.tblDirInfo)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable35)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable37)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable36)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.tblLikagesHC)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable20)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable39)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable38)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable40)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable44)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable43)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable46)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable45)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.tblDbtReviewD)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable47)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable49)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable48)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable42)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable41)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
      // 
      // Detail
      // 
      resources.ApplyResources(this.Detail, "Detail");
      this.Detail.Name = "Detail";
      this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      this.Detail.StylePriority.UseBorderColor = false;
      this.Detail.StylePriority.UseBorders = false;
      this.Detail.StylePriority.UseBorderWidth = false;
      // 
      // TopMargin
      // 
      resources.ApplyResources(this.TopMargin, "TopMargin");
      this.TopMargin.Name = "TopMargin";
      this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      // 
      // BottomMargin
      // 
      resources.ApplyResources(this.BottomMargin, "BottomMargin");
      this.BottomMargin.Name = "BottomMargin";
      this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      // 
      // sqlDataSource1
      // 
      this.sqlDataSource1.Name = "sqlDataSource1";
      // 
      // ReportHeader
      // 
      this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine2,
            this.xrLabel21,
            this.xrLabel22,
            this.xrPictureBox1});
      resources.ApplyResources(this.ReportHeader, "ReportHeader");
      this.ReportHeader.Name = "ReportHeader";
      this.ReportHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      // 
      // xrLine2
      // 
      resources.ApplyResources(this.xrLine2, "xrLine2");
      this.xrLine2.LineWidth = 5;
      this.xrLine2.Name = "xrLine2";
      this.xrLine2.StylePriority.UseForeColor = false;
      // 
      // xrLabel21
      // 
      resources.ApplyResources(this.xrLabel21, "xrLabel21");
      this.xrLabel21.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrLabel21.Name = "xrLabel21";
      this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
      this.xrLabel21.StylePriority.UseBackColor = false;
      this.xrLabel21.StylePriority.UseBorderColor = false;
      this.xrLabel21.StylePriority.UseBorders = false;
      this.xrLabel21.StylePriority.UseFont = false;
      this.xrLabel21.StylePriority.UseForeColor = false;
      this.xrLabel21.StylePriority.UseTextAlignment = false;
      // 
      // xrLabel22
      // 
      resources.ApplyResources(this.xrLabel22, "xrLabel22");
      this.xrLabel22.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrLabel22.Multiline = true;
      this.xrLabel22.Name = "xrLabel22";
      this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrLabel22.StylePriority.UseBackColor = false;
      this.xrLabel22.StylePriority.UseBorderColor = false;
      this.xrLabel22.StylePriority.UseBorders = false;
      this.xrLabel22.StylePriority.UseFont = false;
      this.xrLabel22.StylePriority.UseForeColor = false;
      this.xrLabel22.StylePriority.UsePadding = false;
      this.xrLabel22.StylePriority.UseTextAlignment = false;
      // 
      // xrPictureBox1
      // 
      resources.ApplyResources(this.xrPictureBox1, "xrPictureBox1");
      this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
      this.xrPictureBox1.Name = "xrPictureBox1";
      this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
      this.xrPictureBox1.StylePriority.UsePadding = false;
      // 
      // PageFooter
      // 
      this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel1});
      resources.ApplyResources(this.PageFooter, "PageFooter");
      this.PageFooter.Name = "PageFooter";
      // 
      // xrPanel1
      // 
      resources.ApplyResources(this.xrPanel1, "xrPanel1");
      this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2,
            this.xrLabel16,
            this.xrLabel95,
            this.xrPageInfo1});
      this.xrPanel1.Name = "xrPanel1";
      this.xrPanel1.StylePriority.UseBackColor = false;
      // 
      // xrPageInfo2
      // 
      resources.ApplyResources(this.xrPageInfo2, "xrPageInfo2");
      this.xrPageInfo2.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrPageInfo2.LockedInUserDesigner = true;
      this.xrPageInfo2.Name = "xrPageInfo2";
      this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
      this.xrPageInfo2.StylePriority.UseBackColor = false;
      this.xrPageInfo2.StylePriority.UseBorderColor = false;
      this.xrPageInfo2.StylePriority.UseBorders = false;
      this.xrPageInfo2.StylePriority.UseFont = false;
      this.xrPageInfo2.StylePriority.UseForeColor = false;
      this.xrPageInfo2.StylePriority.UsePadding = false;
      this.xrPageInfo2.StylePriority.UseTextAlignment = false;
      // 
      // xrLabel16
      // 
      resources.ApplyResources(this.xrLabel16, "xrLabel16");
      this.xrLabel16.LockedInUserDesigner = true;
      this.xrLabel16.Name = "xrLabel16";
      this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
      this.xrLabel16.StylePriority.UseBackColor = false;
      this.xrLabel16.StylePriority.UseBorderColor = false;
      this.xrLabel16.StylePriority.UseFont = false;
      this.xrLabel16.StylePriority.UseForeColor = false;
      this.xrLabel16.StylePriority.UseTextAlignment = false;
      // 
      // xrLabel95
      // 
      resources.ApplyResources(this.xrLabel95, "xrLabel95");
      this.xrLabel95.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrLabel95.LockedInUserDesigner = true;
      this.xrLabel95.Name = "xrLabel95";
      this.xrLabel95.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
      this.xrLabel95.StylePriority.UseBackColor = false;
      this.xrLabel95.StylePriority.UseBorderColor = false;
      this.xrLabel95.StylePriority.UseBorders = false;
      this.xrLabel95.StylePriority.UseFont = false;
      this.xrLabel95.StylePriority.UseForeColor = false;
      this.xrLabel95.StylePriority.UseTextAlignment = false;
      // 
      // xrPageInfo1
      // 
      resources.ApplyResources(this.xrPageInfo1, "xrPageInfo1");
      this.xrPageInfo1.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrPageInfo1.LockedInUserDesigner = true;
      this.xrPageInfo1.Name = "xrPageInfo1";
      this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
      this.xrPageInfo1.StylePriority.UseBackColor = false;
      this.xrPageInfo1.StylePriority.UseBorderColor = false;
      this.xrPageInfo1.StylePriority.UseBorders = false;
      this.xrPageInfo1.StylePriority.UseFont = false;
      this.xrPageInfo1.StylePriority.UseForeColor = false;
      this.xrPageInfo1.StylePriority.UseTextAlignment = false;
      // 
      // SumamryBlockA
      // 
      this.SumamryBlockA.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail12});
      resources.ApplyResources(this.SumamryBlockA, "SumamryBlockA");
      this.SumamryBlockA.Level = 0;
      this.SumamryBlockA.Name = "SumamryBlockA";
      // 
      // Detail12
      // 
      resources.ApplyResources(this.Detail12, "Detail12");
      this.Detail12.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable17,
            this.xrTable10});
      this.Detail12.Name = "Detail12";
      this.Detail12.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      this.Detail12.StylePriority.UseBorderColor = false;
      this.Detail12.StylePriority.UsePadding = false;
      // 
      // xrTable17
      // 
      resources.ApplyResources(this.xrTable17, "xrTable17");
      this.xrTable17.KeepTogether = true;
      this.xrTable17.Name = "xrTable17";
      this.xrTable17.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow100,
            this.xrTableRow66,
            this.xrTableRow101,
            this.xrTableRow126,
            this.xrTableRow102,
            this.xrTableRow103,
            this.xrTableRow104,
            this.xrTableRow105,
            this.xrTableRow106,
            this.xrTableRow107,
            this.xrTableRow108,
            this.xrTableRow109,
            this.xrTableRow110,
            this.xrTableRow111,
            this.xrTableRow71});
      // 
      // xrTableRow100
      // 
      this.xrTableRow100.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell235,
            this.xrTableCell236});
      resources.ApplyResources(this.xrTableRow100, "xrTableRow100");
      this.xrTableRow100.Name = "xrTableRow100";
      // 
      // xrTableCell235
      // 
      resources.ApplyResources(this.xrTableCell235, "xrTableCell235");
      this.xrTableCell235.Borders = DevExpress.XtraPrinting.BorderSide.Top;
      this.xrTableCell235.CanGrow = false;
      this.xrTableCell235.Name = "xrTableCell235";
      this.xrTableCell235.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell235.StylePriority.UseBackColor = false;
      this.xrTableCell235.StylePriority.UseBorderColor = false;
      this.xrTableCell235.StylePriority.UseBorders = false;
      this.xrTableCell235.StylePriority.UseFont = false;
      this.xrTableCell235.StylePriority.UseForeColor = false;
      this.xrTableCell235.StylePriority.UsePadding = false;
      this.xrTableCell235.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell236
      // 
      resources.ApplyResources(this.xrTableCell236, "xrTableCell236");
      this.xrTableCell236.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell236.CanGrow = false;
      this.xrTableCell236.Name = "xrTableCell236";
      this.xrTableCell236.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell236.StylePriority.UseBackColor = false;
      this.xrTableCell236.StylePriority.UseBorderColor = false;
      this.xrTableCell236.StylePriority.UseBorders = false;
      this.xrTableCell236.StylePriority.UseFont = false;
      this.xrTableCell236.StylePriority.UseForeColor = false;
      this.xrTableCell236.StylePriority.UsePadding = false;
      this.xrTableCell236.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow66
      // 
      this.xrTableRow66.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell145});
      resources.ApplyResources(this.xrTableRow66, "xrTableRow66");
      this.xrTableRow66.Name = "xrTableRow66";
      // 
      // xrTableCell145
      // 
      resources.ApplyResources(this.xrTableCell145, "xrTableCell145");
      this.xrTableCell145.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTableCell145.CanGrow = false;
      this.xrTableCell145.Name = "xrTableCell145";
      this.xrTableCell145.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell145.StylePriority.UseBackColor = false;
      this.xrTableCell145.StylePriority.UseBorderColor = false;
      this.xrTableCell145.StylePriority.UseBorders = false;
      this.xrTableCell145.StylePriority.UseFont = false;
      this.xrTableCell145.StylePriority.UseForeColor = false;
      this.xrTableCell145.StylePriority.UsePadding = false;
      this.xrTableCell145.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow101
      // 
      this.xrTableRow101.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell237,
            this.xrTableCell238});
      resources.ApplyResources(this.xrTableRow101, "xrTableRow101");
      this.xrTableRow101.Name = "xrTableRow101";
      // 
      // xrTableCell237
      // 
      resources.ApplyResources(this.xrTableCell237, "xrTableCell237");
      this.xrTableCell237.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell237.Name = "xrTableCell237";
      this.xrTableCell237.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell237.StylePriority.UseBackColor = false;
      this.xrTableCell237.StylePriority.UseBorderColor = false;
      this.xrTableCell237.StylePriority.UseBorders = false;
      this.xrTableCell237.StylePriority.UseFont = false;
      this.xrTableCell237.StylePriority.UseForeColor = false;
      this.xrTableCell237.StylePriority.UsePadding = false;
      this.xrTableCell237.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell238
      // 
      resources.ApplyResources(this.xrTableCell238, "xrTableCell238");
      this.xrTableCell238.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell238.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ManufacturerSpecification.BodyStyle")});
      this.xrTableCell238.Name = "xrTableCell238";
      this.xrTableCell238.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell238.StylePriority.UseBackColor = false;
      this.xrTableCell238.StylePriority.UseBorderColor = false;
      this.xrTableCell238.StylePriority.UseBorders = false;
      this.xrTableCell238.StylePriority.UseFont = false;
      this.xrTableCell238.StylePriority.UseForeColor = false;
      this.xrTableCell238.StylePriority.UsePadding = false;
      this.xrTableCell238.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow126
      // 
      this.xrTableRow126.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell287,
            this.xrTableCell288});
      resources.ApplyResources(this.xrTableRow126, "xrTableRow126");
      this.xrTableRow126.Name = "xrTableRow126";
      // 
      // xrTableCell287
      // 
      resources.ApplyResources(this.xrTableCell287, "xrTableCell287");
      this.xrTableCell287.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell287.Name = "xrTableCell287";
      this.xrTableCell287.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell287.StylePriority.UseBackColor = false;
      this.xrTableCell287.StylePriority.UseBorderColor = false;
      this.xrTableCell287.StylePriority.UseBorders = false;
      this.xrTableCell287.StylePriority.UseFont = false;
      this.xrTableCell287.StylePriority.UseForeColor = false;
      this.xrTableCell287.StylePriority.UsePadding = false;
      this.xrTableCell287.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell288
      // 
      resources.ApplyResources(this.xrTableCell288, "xrTableCell288");
      this.xrTableCell288.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell288.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ManufacturerSpecification.TransmissionType")});
      this.xrTableCell288.Name = "xrTableCell288";
      this.xrTableCell288.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell288.StylePriority.UseBackColor = false;
      this.xrTableCell288.StylePriority.UseBorderColor = false;
      this.xrTableCell288.StylePriority.UseBorders = false;
      this.xrTableCell288.StylePriority.UseFont = false;
      this.xrTableCell288.StylePriority.UseForeColor = false;
      this.xrTableCell288.StylePriority.UsePadding = false;
      this.xrTableCell288.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow102
      // 
      this.xrTableRow102.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell239,
            this.xrTableCell240});
      resources.ApplyResources(this.xrTableRow102, "xrTableRow102");
      this.xrTableRow102.Name = "xrTableRow102";
      // 
      // xrTableCell239
      // 
      resources.ApplyResources(this.xrTableCell239, "xrTableCell239");
      this.xrTableCell239.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell239.Name = "xrTableCell239";
      this.xrTableCell239.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell239.StylePriority.UseBackColor = false;
      this.xrTableCell239.StylePriority.UseBorderColor = false;
      this.xrTableCell239.StylePriority.UseBorders = false;
      this.xrTableCell239.StylePriority.UseFont = false;
      this.xrTableCell239.StylePriority.UseForeColor = false;
      this.xrTableCell239.StylePriority.UsePadding = false;
      this.xrTableCell239.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell240
      // 
      resources.ApplyResources(this.xrTableCell240, "xrTableCell240");
      this.xrTableCell240.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell240.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ManufacturerSpecification.EngineCapacity")});
      this.xrTableCell240.Name = "xrTableCell240";
      this.xrTableCell240.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell240.StylePriority.UseBackColor = false;
      this.xrTableCell240.StylePriority.UseBorderColor = false;
      this.xrTableCell240.StylePriority.UseBorders = false;
      this.xrTableCell240.StylePriority.UseFont = false;
      this.xrTableCell240.StylePriority.UseForeColor = false;
      this.xrTableCell240.StylePriority.UsePadding = false;
      this.xrTableCell240.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow103
      // 
      this.xrTableRow103.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell241,
            this.xrTableCell242});
      resources.ApplyResources(this.xrTableRow103, "xrTableRow103");
      this.xrTableRow103.Name = "xrTableRow103";
      // 
      // xrTableCell241
      // 
      resources.ApplyResources(this.xrTableCell241, "xrTableCell241");
      this.xrTableCell241.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell241.Name = "xrTableCell241";
      this.xrTableCell241.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell241.StylePriority.UseBackColor = false;
      this.xrTableCell241.StylePriority.UseBorderColor = false;
      this.xrTableCell241.StylePriority.UseBorders = false;
      this.xrTableCell241.StylePriority.UseFont = false;
      this.xrTableCell241.StylePriority.UseForeColor = false;
      this.xrTableCell241.StylePriority.UsePadding = false;
      this.xrTableCell241.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell242
      // 
      resources.ApplyResources(this.xrTableCell242, "xrTableCell242");
      this.xrTableCell242.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell242.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ManufacturerSpecification.FuelType")});
      this.xrTableCell242.Name = "xrTableCell242";
      this.xrTableCell242.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell242.StylePriority.UseBackColor = false;
      this.xrTableCell242.StylePriority.UseBorderColor = false;
      this.xrTableCell242.StylePriority.UseBorders = false;
      this.xrTableCell242.StylePriority.UseFont = false;
      this.xrTableCell242.StylePriority.UseForeColor = false;
      this.xrTableCell242.StylePriority.UsePadding = false;
      this.xrTableCell242.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow104
      // 
      this.xrTableRow104.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell243,
            this.xrTableCell244});
      resources.ApplyResources(this.xrTableRow104, "xrTableRow104");
      this.xrTableRow104.Name = "xrTableRow104";
      // 
      // xrTableCell243
      // 
      resources.ApplyResources(this.xrTableCell243, "xrTableCell243");
      this.xrTableCell243.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell243.Name = "xrTableCell243";
      this.xrTableCell243.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell243.StylePriority.UseBackColor = false;
      this.xrTableCell243.StylePriority.UseBorderColor = false;
      this.xrTableCell243.StylePriority.UseBorders = false;
      this.xrTableCell243.StylePriority.UseFont = false;
      this.xrTableCell243.StylePriority.UseForeColor = false;
      this.xrTableCell243.StylePriority.UsePadding = false;
      this.xrTableCell243.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell244
      // 
      resources.ApplyResources(this.xrTableCell244, "xrTableCell244");
      this.xrTableCell244.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell244.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ManufacturerSpecification.PowerSteering")});
      this.xrTableCell244.Name = "xrTableCell244";
      this.xrTableCell244.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell244.StylePriority.UseBackColor = false;
      this.xrTableCell244.StylePriority.UseBorderColor = false;
      this.xrTableCell244.StylePriority.UseBorders = false;
      this.xrTableCell244.StylePriority.UseFont = false;
      this.xrTableCell244.StylePriority.UseForeColor = false;
      this.xrTableCell244.StylePriority.UsePadding = false;
      this.xrTableCell244.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow105
      // 
      this.xrTableRow105.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell245,
            this.xrTableCell246});
      resources.ApplyResources(this.xrTableRow105, "xrTableRow105");
      this.xrTableRow105.Name = "xrTableRow105";
      // 
      // xrTableCell245
      // 
      resources.ApplyResources(this.xrTableCell245, "xrTableCell245");
      this.xrTableCell245.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell245.Name = "xrTableCell245";
      this.xrTableCell245.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell245.StylePriority.UseBackColor = false;
      this.xrTableCell245.StylePriority.UseBorderColor = false;
      this.xrTableCell245.StylePriority.UseBorders = false;
      this.xrTableCell245.StylePriority.UseFont = false;
      this.xrTableCell245.StylePriority.UseForeColor = false;
      this.xrTableCell245.StylePriority.UsePadding = false;
      this.xrTableCell245.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell246
      // 
      resources.ApplyResources(this.xrTableCell246, "xrTableCell246");
      this.xrTableCell246.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell246.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ManufacturerSpecification.AirCon")});
      this.xrTableCell246.Name = "xrTableCell246";
      this.xrTableCell246.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell246.StylePriority.UseBackColor = false;
      this.xrTableCell246.StylePriority.UseBorderColor = false;
      this.xrTableCell246.StylePriority.UseBorders = false;
      this.xrTableCell246.StylePriority.UseFont = false;
      this.xrTableCell246.StylePriority.UseForeColor = false;
      this.xrTableCell246.StylePriority.UsePadding = false;
      this.xrTableCell246.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow106
      // 
      this.xrTableRow106.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell247,
            this.xrTableCell248});
      resources.ApplyResources(this.xrTableRow106, "xrTableRow106");
      this.xrTableRow106.Name = "xrTableRow106";
      // 
      // xrTableCell247
      // 
      resources.ApplyResources(this.xrTableCell247, "xrTableCell247");
      this.xrTableCell247.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell247.Name = "xrTableCell247";
      this.xrTableCell247.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell247.StylePriority.UseBackColor = false;
      this.xrTableCell247.StylePriority.UseBorderColor = false;
      this.xrTableCell247.StylePriority.UseBorders = false;
      this.xrTableCell247.StylePriority.UseFont = false;
      this.xrTableCell247.StylePriority.UseForeColor = false;
      this.xrTableCell247.StylePriority.UsePadding = false;
      this.xrTableCell247.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell248
      // 
      resources.ApplyResources(this.xrTableCell248, "xrTableCell248");
      this.xrTableCell248.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell248.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ManufacturerSpecification.CentralLocking")});
      this.xrTableCell248.Name = "xrTableCell248";
      this.xrTableCell248.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell248.StylePriority.UseBackColor = false;
      this.xrTableCell248.StylePriority.UseBorderColor = false;
      this.xrTableCell248.StylePriority.UseBorders = false;
      this.xrTableCell248.StylePriority.UseFont = false;
      this.xrTableCell248.StylePriority.UseForeColor = false;
      this.xrTableCell248.StylePriority.UsePadding = false;
      this.xrTableCell248.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow107
      // 
      this.xrTableRow107.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell249,
            this.xrTableCell250});
      resources.ApplyResources(this.xrTableRow107, "xrTableRow107");
      this.xrTableRow107.Name = "xrTableRow107";
      // 
      // xrTableCell249
      // 
      resources.ApplyResources(this.xrTableCell249, "xrTableCell249");
      this.xrTableCell249.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell249.Name = "xrTableCell249";
      this.xrTableCell249.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell249.StylePriority.UseBackColor = false;
      this.xrTableCell249.StylePriority.UseBorderColor = false;
      this.xrTableCell249.StylePriority.UseBorders = false;
      this.xrTableCell249.StylePriority.UseFont = false;
      this.xrTableCell249.StylePriority.UseForeColor = false;
      this.xrTableCell249.StylePriority.UsePadding = false;
      this.xrTableCell249.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell250
      // 
      resources.ApplyResources(this.xrTableCell250, "xrTableCell250");
      this.xrTableCell250.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell250.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ManufacturerSpecification.LeatherDetail")});
      this.xrTableCell250.Name = "xrTableCell250";
      this.xrTableCell250.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell250.StylePriority.UseBackColor = false;
      this.xrTableCell250.StylePriority.UseBorderColor = false;
      this.xrTableCell250.StylePriority.UseBorders = false;
      this.xrTableCell250.StylePriority.UseFont = false;
      this.xrTableCell250.StylePriority.UseForeColor = false;
      this.xrTableCell250.StylePriority.UsePadding = false;
      this.xrTableCell250.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow108
      // 
      this.xrTableRow108.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell251,
            this.xrTableCell252});
      resources.ApplyResources(this.xrTableRow108, "xrTableRow108");
      this.xrTableRow108.Name = "xrTableRow108";
      // 
      // xrTableCell251
      // 
      resources.ApplyResources(this.xrTableCell251, "xrTableCell251");
      this.xrTableCell251.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell251.Name = "xrTableCell251";
      this.xrTableCell251.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell251.StylePriority.UseBackColor = false;
      this.xrTableCell251.StylePriority.UseBorderColor = false;
      this.xrTableCell251.StylePriority.UseBorders = false;
      this.xrTableCell251.StylePriority.UseFont = false;
      this.xrTableCell251.StylePriority.UseForeColor = false;
      this.xrTableCell251.StylePriority.UsePadding = false;
      this.xrTableCell251.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell252
      // 
      resources.ApplyResources(this.xrTableCell252, "xrTableCell252");
      this.xrTableCell252.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell252.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ManufacturerSpecification.PowerOutput")});
      this.xrTableCell252.Name = "xrTableCell252";
      this.xrTableCell252.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell252.StylePriority.UseBackColor = false;
      this.xrTableCell252.StylePriority.UseBorderColor = false;
      this.xrTableCell252.StylePriority.UseBorders = false;
      this.xrTableCell252.StylePriority.UseFont = false;
      this.xrTableCell252.StylePriority.UseForeColor = false;
      this.xrTableCell252.StylePriority.UsePadding = false;
      this.xrTableCell252.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow109
      // 
      this.xrTableRow109.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell253,
            this.xrTableCell254});
      resources.ApplyResources(this.xrTableRow109, "xrTableRow109");
      this.xrTableRow109.Name = "xrTableRow109";
      // 
      // xrTableCell253
      // 
      resources.ApplyResources(this.xrTableCell253, "xrTableCell253");
      this.xrTableCell253.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell253.Name = "xrTableCell253";
      this.xrTableCell253.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell253.StylePriority.UseBackColor = false;
      this.xrTableCell253.StylePriority.UseBorderColor = false;
      this.xrTableCell253.StylePriority.UseBorders = false;
      this.xrTableCell253.StylePriority.UseFont = false;
      this.xrTableCell253.StylePriority.UseForeColor = false;
      this.xrTableCell253.StylePriority.UsePadding = false;
      this.xrTableCell253.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell254
      // 
      resources.ApplyResources(this.xrTableCell254, "xrTableCell254");
      this.xrTableCell254.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell254.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ManufacturerSpecification.Torque")});
      this.xrTableCell254.Name = "xrTableCell254";
      this.xrTableCell254.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell254.StylePriority.UseBackColor = false;
      this.xrTableCell254.StylePriority.UseBorderColor = false;
      this.xrTableCell254.StylePriority.UseBorders = false;
      this.xrTableCell254.StylePriority.UseFont = false;
      this.xrTableCell254.StylePriority.UseForeColor = false;
      this.xrTableCell254.StylePriority.UsePadding = false;
      this.xrTableCell254.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow110
      // 
      this.xrTableRow110.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell255,
            this.xrTableCell256});
      resources.ApplyResources(this.xrTableRow110, "xrTableRow110");
      this.xrTableRow110.Name = "xrTableRow110";
      // 
      // xrTableCell255
      // 
      resources.ApplyResources(this.xrTableCell255, "xrTableCell255");
      this.xrTableCell255.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell255.Name = "xrTableCell255";
      this.xrTableCell255.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell255.StylePriority.UseBackColor = false;
      this.xrTableCell255.StylePriority.UseBorderColor = false;
      this.xrTableCell255.StylePriority.UseBorders = false;
      this.xrTableCell255.StylePriority.UseFont = false;
      this.xrTableCell255.StylePriority.UseForeColor = false;
      this.xrTableCell255.StylePriority.UsePadding = false;
      this.xrTableCell255.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell256
      // 
      resources.ApplyResources(this.xrTableCell256, "xrTableCell256");
      this.xrTableCell256.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell256.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ManufacturerSpecification.Acceleration0To100")});
      this.xrTableCell256.Name = "xrTableCell256";
      this.xrTableCell256.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell256.StylePriority.UseBackColor = false;
      this.xrTableCell256.StylePriority.UseBorderColor = false;
      this.xrTableCell256.StylePriority.UseBorders = false;
      this.xrTableCell256.StylePriority.UseFont = false;
      this.xrTableCell256.StylePriority.UseForeColor = false;
      this.xrTableCell256.StylePriority.UsePadding = false;
      this.xrTableCell256.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow111
      // 
      this.xrTableRow111.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell257,
            this.xrTableCell258});
      resources.ApplyResources(this.xrTableRow111, "xrTableRow111");
      this.xrTableRow111.Name = "xrTableRow111";
      // 
      // xrTableCell257
      // 
      resources.ApplyResources(this.xrTableCell257, "xrTableCell257");
      this.xrTableCell257.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell257.Name = "xrTableCell257";
      this.xrTableCell257.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell257.StylePriority.UseBackColor = false;
      this.xrTableCell257.StylePriority.UseBorderColor = false;
      this.xrTableCell257.StylePriority.UseBorders = false;
      this.xrTableCell257.StylePriority.UseFont = false;
      this.xrTableCell257.StylePriority.UseForeColor = false;
      this.xrTableCell257.StylePriority.UsePadding = false;
      this.xrTableCell257.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell258
      // 
      resources.ApplyResources(this.xrTableCell258, "xrTableCell258");
      this.xrTableCell258.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell258.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ManufacturerSpecification.TopSpeed")});
      this.xrTableCell258.Name = "xrTableCell258";
      this.xrTableCell258.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell258.StylePriority.UseBackColor = false;
      this.xrTableCell258.StylePriority.UseBorderColor = false;
      this.xrTableCell258.StylePriority.UseBorders = false;
      this.xrTableCell258.StylePriority.UseFont = false;
      this.xrTableCell258.StylePriority.UseForeColor = false;
      this.xrTableCell258.StylePriority.UsePadding = false;
      this.xrTableCell258.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow71
      // 
      this.xrTableRow71.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell148});
      resources.ApplyResources(this.xrTableRow71, "xrTableRow71");
      this.xrTableRow71.Name = "xrTableRow71";
      // 
      // xrTableCell148
      // 
      resources.ApplyResources(this.xrTableCell148, "xrTableCell148");
      this.xrTableCell148.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTableCell148.CanGrow = false;
      this.xrTableCell148.Name = "xrTableCell148";
      this.xrTableCell148.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell148.StylePriority.UseBackColor = false;
      this.xrTableCell148.StylePriority.UseBorderColor = false;
      this.xrTableCell148.StylePriority.UseBorders = false;
      this.xrTableCell148.StylePriority.UseFont = false;
      this.xrTableCell148.StylePriority.UseForeColor = false;
      this.xrTableCell148.StylePriority.UsePadding = false;
      this.xrTableCell148.StylePriority.UseTextAlignment = false;
      // 
      // xrTable10
      // 
      resources.ApplyResources(this.xrTable10, "xrTable10");
      this.xrTable10.KeepTogether = true;
      this.xrTable10.Name = "xrTable10";
      this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow43,
            this.xrTableRow65,
            this.xrTableRow13,
            this.xrTableRow42,
            this.xrTableRow41,
            this.xrTableRow40,
            this.xrTableRow39,
            this.xrTableRow38,
            this.xrTableRow37,
            this.xrTableRow36,
            this.xrTableRow35,
            this.xrTableRow34,
            this.xrTableRow33,
            this.xrTableRow27});
      // 
      // xrTableRow43
      // 
      this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell141,
            this.xrTableCell122});
      resources.ApplyResources(this.xrTableRow43, "xrTableRow43");
      this.xrTableRow43.Name = "xrTableRow43";
      // 
      // xrTableCell141
      // 
      resources.ApplyResources(this.xrTableCell141, "xrTableCell141");
      this.xrTableCell141.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell141.CanGrow = false;
      this.xrTableCell141.Name = "xrTableCell141";
      this.xrTableCell141.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell141.StylePriority.UseBackColor = false;
      this.xrTableCell141.StylePriority.UseBorderColor = false;
      this.xrTableCell141.StylePriority.UseBorders = false;
      this.xrTableCell141.StylePriority.UseFont = false;
      this.xrTableCell141.StylePriority.UseForeColor = false;
      this.xrTableCell141.StylePriority.UsePadding = false;
      this.xrTableCell141.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell122
      // 
      resources.ApplyResources(this.xrTableCell122, "xrTableCell122");
      this.xrTableCell122.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell122.CanGrow = false;
      this.xrTableCell122.Name = "xrTableCell122";
      this.xrTableCell122.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell122.StylePriority.UseBackColor = false;
      this.xrTableCell122.StylePriority.UseBorderColor = false;
      this.xrTableCell122.StylePriority.UseBorderDashStyle = false;
      this.xrTableCell122.StylePriority.UseBorders = false;
      this.xrTableCell122.StylePriority.UseBorderWidth = false;
      this.xrTableCell122.StylePriority.UseFont = false;
      this.xrTableCell122.StylePriority.UseForeColor = false;
      this.xrTableCell122.StylePriority.UsePadding = false;
      this.xrTableCell122.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow65
      // 
      this.xrTableRow65.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell144});
      resources.ApplyResources(this.xrTableRow65, "xrTableRow65");
      this.xrTableRow65.Name = "xrTableRow65";
      this.xrTableRow65.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      this.xrTableRow65.StylePriority.UsePadding = false;
      // 
      // xrTableCell144
      // 
      resources.ApplyResources(this.xrTableCell144, "xrTableCell144");
      this.xrTableCell144.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTableCell144.CanGrow = false;
      this.xrTableCell144.Name = "xrTableCell144";
      this.xrTableCell144.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell144.StylePriority.UseBackColor = false;
      this.xrTableCell144.StylePriority.UseBorderColor = false;
      this.xrTableCell144.StylePriority.UseBorders = false;
      this.xrTableCell144.StylePriority.UseFont = false;
      this.xrTableCell144.StylePriority.UseForeColor = false;
      this.xrTableCell144.StylePriority.UsePadding = false;
      this.xrTableCell144.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow13
      // 
      this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell47});
      resources.ApplyResources(this.xrTableRow13, "xrTableRow13");
      this.xrTableRow13.Name = "xrTableRow13";
      // 
      // xrTableCell7
      // 
      resources.ApplyResources(this.xrTableCell7, "xrTableCell7");
      this.xrTableCell7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell7.Name = "xrTableCell7";
      this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell7.StylePriority.UseBackColor = false;
      this.xrTableCell7.StylePriority.UseBorderColor = false;
      this.xrTableCell7.StylePriority.UseBorders = false;
      this.xrTableCell7.StylePriority.UseFont = false;
      this.xrTableCell7.StylePriority.UseForeColor = false;
      this.xrTableCell7.StylePriority.UsePadding = false;
      this.xrTableCell7.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell47
      // 
      resources.ApplyResources(this.xrTableCell47, "xrTableCell47");
      this.xrTableCell47.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell47.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "VehicleInformation.VIN")});
      this.xrTableCell47.Name = "xrTableCell47";
      this.xrTableCell47.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell47.StylePriority.UseBackColor = false;
      this.xrTableCell47.StylePriority.UseBorderColor = false;
      this.xrTableCell47.StylePriority.UseBorders = false;
      this.xrTableCell47.StylePriority.UseFont = false;
      this.xrTableCell47.StylePriority.UseForeColor = false;
      this.xrTableCell47.StylePriority.UsePadding = false;
      this.xrTableCell47.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow42
      // 
      this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell119,
            this.xrTableCell120});
      resources.ApplyResources(this.xrTableRow42, "xrTableRow42");
      this.xrTableRow42.Name = "xrTableRow42";
      // 
      // xrTableCell119
      // 
      resources.ApplyResources(this.xrTableCell119, "xrTableCell119");
      this.xrTableCell119.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell119.Name = "xrTableCell119";
      this.xrTableCell119.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell119.StylePriority.UseBackColor = false;
      this.xrTableCell119.StylePriority.UseBorderColor = false;
      this.xrTableCell119.StylePriority.UseBorders = false;
      this.xrTableCell119.StylePriority.UseFont = false;
      this.xrTableCell119.StylePriority.UseForeColor = false;
      this.xrTableCell119.StylePriority.UsePadding = false;
      this.xrTableCell119.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell120
      // 
      resources.ApplyResources(this.xrTableCell120, "xrTableCell120");
      this.xrTableCell120.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell120.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "VehicleInformation.EngineNo")});
      this.xrTableCell120.Name = "xrTableCell120";
      this.xrTableCell120.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell120.StylePriority.UseBackColor = false;
      this.xrTableCell120.StylePriority.UseBorderColor = false;
      this.xrTableCell120.StylePriority.UseBorders = false;
      this.xrTableCell120.StylePriority.UseFont = false;
      this.xrTableCell120.StylePriority.UseForeColor = false;
      this.xrTableCell120.StylePriority.UsePadding = false;
      this.xrTableCell120.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow41
      // 
      this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell117,
            this.xrTableCell118});
      resources.ApplyResources(this.xrTableRow41, "xrTableRow41");
      this.xrTableRow41.Name = "xrTableRow41";
      // 
      // xrTableCell117
      // 
      resources.ApplyResources(this.xrTableCell117, "xrTableCell117");
      this.xrTableCell117.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell117.Name = "xrTableCell117";
      this.xrTableCell117.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell117.StylePriority.UseBackColor = false;
      this.xrTableCell117.StylePriority.UseBorderColor = false;
      this.xrTableCell117.StylePriority.UseBorders = false;
      this.xrTableCell117.StylePriority.UseFont = false;
      this.xrTableCell117.StylePriority.UseForeColor = false;
      this.xrTableCell117.StylePriority.UsePadding = false;
      this.xrTableCell117.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell118
      // 
      resources.ApplyResources(this.xrTableCell118, "xrTableCell118");
      this.xrTableCell118.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell118.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "VehicleInformation.RegistrationNo")});
      this.xrTableCell118.Name = "xrTableCell118";
      this.xrTableCell118.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell118.StylePriority.UseBackColor = false;
      this.xrTableCell118.StylePriority.UseBorderColor = false;
      this.xrTableCell118.StylePriority.UseBorders = false;
      this.xrTableCell118.StylePriority.UseFont = false;
      this.xrTableCell118.StylePriority.UseForeColor = false;
      this.xrTableCell118.StylePriority.UsePadding = false;
      this.xrTableCell118.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow40
      // 
      this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell115,
            this.xrTableCell116});
      resources.ApplyResources(this.xrTableRow40, "xrTableRow40");
      this.xrTableRow40.Name = "xrTableRow40";
      // 
      // xrTableCell115
      // 
      resources.ApplyResources(this.xrTableCell115, "xrTableCell115");
      this.xrTableCell115.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell115.Name = "xrTableCell115";
      this.xrTableCell115.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell115.StylePriority.UseBackColor = false;
      this.xrTableCell115.StylePriority.UseBorderColor = false;
      this.xrTableCell115.StylePriority.UseBorders = false;
      this.xrTableCell115.StylePriority.UseFont = false;
      this.xrTableCell115.StylePriority.UseForeColor = false;
      this.xrTableCell115.StylePriority.UsePadding = false;
      this.xrTableCell115.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell116
      // 
      resources.ApplyResources(this.xrTableCell116, "xrTableCell116");
      this.xrTableCell116.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell116.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "VehicleInformation.Manufacturer")});
      this.xrTableCell116.Name = "xrTableCell116";
      this.xrTableCell116.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell116.StylePriority.UseBackColor = false;
      this.xrTableCell116.StylePriority.UseBorderColor = false;
      this.xrTableCell116.StylePriority.UseBorders = false;
      this.xrTableCell116.StylePriority.UseFont = false;
      this.xrTableCell116.StylePriority.UseForeColor = false;
      this.xrTableCell116.StylePriority.UsePadding = false;
      this.xrTableCell116.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow39
      // 
      this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell113,
            this.xrTableCell114});
      resources.ApplyResources(this.xrTableRow39, "xrTableRow39");
      this.xrTableRow39.Name = "xrTableRow39";
      // 
      // xrTableCell113
      // 
      resources.ApplyResources(this.xrTableCell113, "xrTableCell113");
      this.xrTableCell113.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell113.Name = "xrTableCell113";
      this.xrTableCell113.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell113.StylePriority.UseBackColor = false;
      this.xrTableCell113.StylePriority.UseBorderColor = false;
      this.xrTableCell113.StylePriority.UseBorders = false;
      this.xrTableCell113.StylePriority.UseFont = false;
      this.xrTableCell113.StylePriority.UseForeColor = false;
      this.xrTableCell113.StylePriority.UsePadding = false;
      this.xrTableCell113.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell114
      // 
      resources.ApplyResources(this.xrTableCell114, "xrTableCell114");
      this.xrTableCell114.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell114.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "VehicleInformation.Model")});
      this.xrTableCell114.Name = "xrTableCell114";
      this.xrTableCell114.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell114.StylePriority.UseBackColor = false;
      this.xrTableCell114.StylePriority.UseBorderColor = false;
      this.xrTableCell114.StylePriority.UseBorders = false;
      this.xrTableCell114.StylePriority.UseFont = false;
      this.xrTableCell114.StylePriority.UseForeColor = false;
      this.xrTableCell114.StylePriority.UsePadding = false;
      this.xrTableCell114.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow38
      // 
      this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell108,
            this.xrTableCell109});
      resources.ApplyResources(this.xrTableRow38, "xrTableRow38");
      this.xrTableRow38.Name = "xrTableRow38";
      // 
      // xrTableCell108
      // 
      resources.ApplyResources(this.xrTableCell108, "xrTableCell108");
      this.xrTableCell108.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell108.Name = "xrTableCell108";
      this.xrTableCell108.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell108.StylePriority.UseBackColor = false;
      this.xrTableCell108.StylePriority.UseBorderColor = false;
      this.xrTableCell108.StylePriority.UseBorders = false;
      this.xrTableCell108.StylePriority.UseFont = false;
      this.xrTableCell108.StylePriority.UseForeColor = false;
      this.xrTableCell108.StylePriority.UsePadding = false;
      this.xrTableCell108.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell109
      // 
      resources.ApplyResources(this.xrTableCell109, "xrTableCell109");
      this.xrTableCell109.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell109.Name = "xrTableCell109";
      this.xrTableCell109.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell109.StylePriority.UseBackColor = false;
      this.xrTableCell109.StylePriority.UseBorderColor = false;
      this.xrTableCell109.StylePriority.UseBorders = false;
      this.xrTableCell109.StylePriority.UseFont = false;
      this.xrTableCell109.StylePriority.UseForeColor = false;
      this.xrTableCell109.StylePriority.UsePadding = false;
      this.xrTableCell109.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow37
      // 
      this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell106,
            this.xrTableCell107});
      resources.ApplyResources(this.xrTableRow37, "xrTableRow37");
      this.xrTableRow37.Name = "xrTableRow37";
      // 
      // xrTableCell106
      // 
      resources.ApplyResources(this.xrTableCell106, "xrTableCell106");
      this.xrTableCell106.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell106.Name = "xrTableCell106";
      this.xrTableCell106.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell106.StylePriority.UseBackColor = false;
      this.xrTableCell106.StylePriority.UseBorderColor = false;
      this.xrTableCell106.StylePriority.UseBorders = false;
      this.xrTableCell106.StylePriority.UseFont = false;
      this.xrTableCell106.StylePriority.UseForeColor = false;
      this.xrTableCell106.StylePriority.UsePadding = false;
      this.xrTableCell106.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell107
      // 
      resources.ApplyResources(this.xrTableCell107, "xrTableCell107");
      this.xrTableCell107.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell107.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "VehicleInformation.Year")});
      this.xrTableCell107.Name = "xrTableCell107";
      this.xrTableCell107.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell107.StylePriority.UseBackColor = false;
      this.xrTableCell107.StylePriority.UseBorderColor = false;
      this.xrTableCell107.StylePriority.UseBorders = false;
      this.xrTableCell107.StylePriority.UseFont = false;
      this.xrTableCell107.StylePriority.UseForeColor = false;
      this.xrTableCell107.StylePriority.UsePadding = false;
      this.xrTableCell107.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow36
      // 
      this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell104,
            this.xrTableCell105});
      resources.ApplyResources(this.xrTableRow36, "xrTableRow36");
      this.xrTableRow36.Name = "xrTableRow36";
      // 
      // xrTableCell104
      // 
      resources.ApplyResources(this.xrTableCell104, "xrTableCell104");
      this.xrTableCell104.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell104.Name = "xrTableCell104";
      this.xrTableCell104.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell104.StylePriority.UseBackColor = false;
      this.xrTableCell104.StylePriority.UseBorderColor = false;
      this.xrTableCell104.StylePriority.UseBorders = false;
      this.xrTableCell104.StylePriority.UseFont = false;
      this.xrTableCell104.StylePriority.UseForeColor = false;
      this.xrTableCell104.StylePriority.UsePadding = false;
      this.xrTableCell104.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell105
      // 
      resources.ApplyResources(this.xrTableCell105, "xrTableCell105");
      this.xrTableCell105.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell105.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "VehicleInformation.Colour")});
      this.xrTableCell105.Name = "xrTableCell105";
      this.xrTableCell105.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell105.StylePriority.UseBackColor = false;
      this.xrTableCell105.StylePriority.UseBorderColor = false;
      this.xrTableCell105.StylePriority.UseBorders = false;
      this.xrTableCell105.StylePriority.UseFont = false;
      this.xrTableCell105.StylePriority.UseForeColor = false;
      this.xrTableCell105.StylePriority.UsePadding = false;
      this.xrTableCell105.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow35
      // 
      this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell98,
            this.xrTableCell103});
      resources.ApplyResources(this.xrTableRow35, "xrTableRow35");
      this.xrTableRow35.Name = "xrTableRow35";
      // 
      // xrTableCell98
      // 
      resources.ApplyResources(this.xrTableCell98, "xrTableCell98");
      this.xrTableCell98.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell98.Name = "xrTableCell98";
      this.xrTableCell98.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell98.StylePriority.UseBackColor = false;
      this.xrTableCell98.StylePriority.UseBorderColor = false;
      this.xrTableCell98.StylePriority.UseBorders = false;
      this.xrTableCell98.StylePriority.UseFont = false;
      this.xrTableCell98.StylePriority.UseForeColor = false;
      this.xrTableCell98.StylePriority.UsePadding = false;
      this.xrTableCell98.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell103
      // 
      resources.ApplyResources(this.xrTableCell103, "xrTableCell103");
      this.xrTableCell103.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell103.Name = "xrTableCell103";
      this.xrTableCell103.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell103.StylePriority.UseBackColor = false;
      this.xrTableCell103.StylePriority.UseBorderColor = false;
      this.xrTableCell103.StylePriority.UseBorders = false;
      this.xrTableCell103.StylePriority.UseFont = false;
      this.xrTableCell103.StylePriority.UseForeColor = false;
      this.xrTableCell103.StylePriority.UsePadding = false;
      this.xrTableCell103.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow34
      // 
      this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell70});
      resources.ApplyResources(this.xrTableRow34, "xrTableRow34");
      this.xrTableRow34.Name = "xrTableRow34";
      // 
      // xrTableCell70
      // 
      resources.ApplyResources(this.xrTableCell70, "xrTableCell70");
      this.xrTableCell70.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell70.Name = "xrTableCell70";
      this.xrTableCell70.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell70.StylePriority.UseBackColor = false;
      this.xrTableCell70.StylePriority.UseBorderColor = false;
      this.xrTableCell70.StylePriority.UseBorders = false;
      this.xrTableCell70.StylePriority.UseFont = false;
      this.xrTableCell70.StylePriority.UseForeColor = false;
      this.xrTableCell70.StylePriority.UsePadding = false;
      this.xrTableCell70.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow33
      // 
      this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell67,
            this.xrTableCell68});
      resources.ApplyResources(this.xrTableRow33, "xrTableRow33");
      this.xrTableRow33.Name = "xrTableRow33";
      // 
      // xrTableCell67
      // 
      resources.ApplyResources(this.xrTableCell67, "xrTableCell67");
      this.xrTableCell67.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell67.Name = "xrTableCell67";
      this.xrTableCell67.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell67.StylePriority.UseBackColor = false;
      this.xrTableCell67.StylePriority.UseBorderColor = false;
      this.xrTableCell67.StylePriority.UseBorders = false;
      this.xrTableCell67.StylePriority.UseFont = false;
      this.xrTableCell67.StylePriority.UseForeColor = false;
      this.xrTableCell67.StylePriority.UsePadding = false;
      this.xrTableCell67.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell68
      // 
      resources.ApplyResources(this.xrTableCell68, "xrTableCell68");
      this.xrTableCell68.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell68.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SAPInformation.EnquiryDate")});
      this.xrTableCell68.Name = "xrTableCell68";
      this.xrTableCell68.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell68.StylePriority.UseBackColor = false;
      this.xrTableCell68.StylePriority.UseBorderColor = false;
      this.xrTableCell68.StylePriority.UseBorders = false;
      this.xrTableCell68.StylePriority.UseFont = false;
      this.xrTableCell68.StylePriority.UseForeColor = false;
      this.xrTableCell68.StylePriority.UsePadding = false;
      this.xrTableCell68.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow27
      // 
      this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell146,
            this.lblTest});
      resources.ApplyResources(this.xrTableRow27, "xrTableRow27");
      this.xrTableRow27.Name = "xrTableRow27";
      // 
      // xrTableCell146
      // 
      resources.ApplyResources(this.xrTableCell146, "xrTableCell146");
      this.xrTableCell146.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell146.Name = "xrTableCell146";
      this.xrTableCell146.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell146.StylePriority.UseBackColor = false;
      this.xrTableCell146.StylePriority.UseBorderColor = false;
      this.xrTableCell146.StylePriority.UseBorders = false;
      this.xrTableCell146.StylePriority.UseFont = false;
      this.xrTableCell146.StylePriority.UseForeColor = false;
      this.xrTableCell146.StylePriority.UsePadding = false;
      this.xrTableCell146.StylePriority.UseTextAlignment = false;
      // 
      // lblTest
      // 
      resources.ApplyResources(this.lblTest, "lblTest");
      this.lblTest.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.lblTest.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SAPInformation.ResultCode")});
      this.lblTest.Name = "lblTest";
      this.lblTest.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.lblTest.Scripts.OnBeforePrint = "xrTableCell32_BeforePrint";
      this.lblTest.StylePriority.UseBackColor = false;
      this.lblTest.StylePriority.UseBorderColor = false;
      this.lblTest.StylePriority.UseBorders = false;
      this.lblTest.StylePriority.UseFont = false;
      this.lblTest.StylePriority.UseForeColor = false;
      this.lblTest.StylePriority.UsePadding = false;
      this.lblTest.StylePriority.UseTextAlignment = false;
      // 
      // SummaryBlockB
      // 
      this.SummaryBlockB.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail14,
            this.GroupHeader11});
      this.SummaryBlockB.DataMember = "TradeInformation.TradeInformation_MarketPrices";
      resources.ApplyResources(this.SummaryBlockB, "SummaryBlockB");
      this.SummaryBlockB.Level = 1;
      this.SummaryBlockB.Name = "SummaryBlockB";
      // 
      // Detail14
      // 
      resources.ApplyResources(this.Detail14, "Detail14");
      this.Detail14.Expanded = false;
      this.Detail14.Name = "Detail14";
      // 
      // GroupHeader11
      // 
      this.GroupHeader11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable12});
      resources.ApplyResources(this.GroupHeader11, "GroupHeader11");
      this.GroupHeader11.Name = "GroupHeader11";
      // 
      // xrTable12
      // 
      resources.ApplyResources(this.xrTable12, "xrTable12");
      this.xrTable12.Name = "xrTable12";
      this.xrTable12.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow47,
            this.xrTableRow72,
            this.xrTableRow48,
            this.xrTableRow50,
            this.xrTableRow52,
            this.xrTableRow88});
      // 
      // xrTableRow47
      // 
      this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell71,
            this.xrTableCell124});
      resources.ApplyResources(this.xrTableRow47, "xrTableRow47");
      this.xrTableRow47.Name = "xrTableRow47";
      // 
      // xrTableCell71
      // 
      resources.ApplyResources(this.xrTableCell71, "xrTableCell71");
      this.xrTableCell71.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell71.CanGrow = false;
      this.xrTableCell71.Name = "xrTableCell71";
      this.xrTableCell71.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell71.StylePriority.UseBackColor = false;
      this.xrTableCell71.StylePriority.UseBorderColor = false;
      this.xrTableCell71.StylePriority.UseBorders = false;
      this.xrTableCell71.StylePriority.UseFont = false;
      this.xrTableCell71.StylePriority.UseForeColor = false;
      this.xrTableCell71.StylePriority.UsePadding = false;
      this.xrTableCell71.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell124
      // 
      resources.ApplyResources(this.xrTableCell124, "xrTableCell124");
      this.xrTableCell124.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell124.CanGrow = false;
      this.xrTableCell124.Name = "xrTableCell124";
      this.xrTableCell124.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell124.StylePriority.UseBackColor = false;
      this.xrTableCell124.StylePriority.UseBorderColor = false;
      this.xrTableCell124.StylePriority.UseBorders = false;
      this.xrTableCell124.StylePriority.UseFont = false;
      this.xrTableCell124.StylePriority.UseForeColor = false;
      this.xrTableCell124.StylePriority.UsePadding = false;
      this.xrTableCell124.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow72
      // 
      this.xrTableRow72.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell178});
      resources.ApplyResources(this.xrTableRow72, "xrTableRow72");
      this.xrTableRow72.Name = "xrTableRow72";
      // 
      // xrTableCell178
      // 
      resources.ApplyResources(this.xrTableCell178, "xrTableCell178");
      this.xrTableCell178.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTableCell178.CanGrow = false;
      this.xrTableCell178.Name = "xrTableCell178";
      this.xrTableCell178.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell178.StylePriority.UseBackColor = false;
      this.xrTableCell178.StylePriority.UseBorderColor = false;
      this.xrTableCell178.StylePriority.UseBorders = false;
      this.xrTableCell178.StylePriority.UseFont = false;
      this.xrTableCell178.StylePriority.UseForeColor = false;
      this.xrTableCell178.StylePriority.UsePadding = false;
      this.xrTableCell178.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow48
      // 
      this.xrTableRow48.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell125,
            this.xrTableCell177,
            this.xrTableCell337,
            this.xrTableCell126});
      resources.ApplyResources(this.xrTableRow48, "xrTableRow48");
      this.xrTableRow48.Name = "xrTableRow48";
      // 
      // xrTableCell125
      // 
      resources.ApplyResources(this.xrTableCell125, "xrTableCell125");
      this.xrTableCell125.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell125.Name = "xrTableCell125";
      this.xrTableCell125.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell125.StylePriority.UseBackColor = false;
      this.xrTableCell125.StylePriority.UseBorderColor = false;
      this.xrTableCell125.StylePriority.UseBorders = false;
      this.xrTableCell125.StylePriority.UseFont = false;
      this.xrTableCell125.StylePriority.UseForeColor = false;
      this.xrTableCell125.StylePriority.UsePadding = false;
      this.xrTableCell125.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell177
      // 
      resources.ApplyResources(this.xrTableCell177, "xrTableCell177");
      this.xrTableCell177.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell177.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TradeInformation.TradeInformation_MarketPrices.PriceNew", "{0:c2}")});
      this.xrTableCell177.Name = "xrTableCell177";
      this.xrTableCell177.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell177.StylePriority.UseBackColor = false;
      this.xrTableCell177.StylePriority.UseBorderColor = false;
      this.xrTableCell177.StylePriority.UseBorders = false;
      this.xrTableCell177.StylePriority.UseFont = false;
      this.xrTableCell177.StylePriority.UseForeColor = false;
      this.xrTableCell177.StylePriority.UsePadding = false;
      this.xrTableCell177.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell337
      // 
      resources.ApplyResources(this.xrTableCell337, "xrTableCell337");
      this.xrTableCell337.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell337.Name = "xrTableCell337";
      this.xrTableCell337.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell337.StylePriority.UseBackColor = false;
      this.xrTableCell337.StylePriority.UseBorderColor = false;
      this.xrTableCell337.StylePriority.UseBorders = false;
      this.xrTableCell337.StylePriority.UseFont = false;
      this.xrTableCell337.StylePriority.UseForeColor = false;
      this.xrTableCell337.StylePriority.UsePadding = false;
      this.xrTableCell337.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell126
      // 
      resources.ApplyResources(this.xrTableCell126, "xrTableCell126");
      this.xrTableCell126.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell126.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TradeInformation.TradeInformation_MarketPrices.PriceTrade")});
      this.xrTableCell126.Name = "xrTableCell126";
      this.xrTableCell126.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell126.StylePriority.UseBackColor = false;
      this.xrTableCell126.StylePriority.UseBorderColor = false;
      this.xrTableCell126.StylePriority.UseBorders = false;
      this.xrTableCell126.StylePriority.UseFont = false;
      this.xrTableCell126.StylePriority.UseForeColor = false;
      this.xrTableCell126.StylePriority.UsePadding = false;
      this.xrTableCell126.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow50
      // 
      this.xrTableRow50.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell129,
            this.xrTableCell207,
            this.xrTableCell339,
            this.xrTableCell130});
      resources.ApplyResources(this.xrTableRow50, "xrTableRow50");
      this.xrTableRow50.Name = "xrTableRow50";
      // 
      // xrTableCell129
      // 
      resources.ApplyResources(this.xrTableCell129, "xrTableCell129");
      this.xrTableCell129.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell129.Name = "xrTableCell129";
      this.xrTableCell129.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell129.StylePriority.UseBackColor = false;
      this.xrTableCell129.StylePriority.UseBorderColor = false;
      this.xrTableCell129.StylePriority.UseBorders = false;
      this.xrTableCell129.StylePriority.UseFont = false;
      this.xrTableCell129.StylePriority.UseForeColor = false;
      this.xrTableCell129.StylePriority.UsePadding = false;
      this.xrTableCell129.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell207
      // 
      resources.ApplyResources(this.xrTableCell207, "xrTableCell207");
      this.xrTableCell207.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell207.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TradeInformation.TradeInformation_MarketPrices.PriceRetail")});
      this.xrTableCell207.Name = "xrTableCell207";
      this.xrTableCell207.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell207.StylePriority.UseBackColor = false;
      this.xrTableCell207.StylePriority.UseBorderColor = false;
      this.xrTableCell207.StylePriority.UseBorders = false;
      this.xrTableCell207.StylePriority.UseFont = false;
      this.xrTableCell207.StylePriority.UseForeColor = false;
      this.xrTableCell207.StylePriority.UsePadding = false;
      this.xrTableCell207.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell339
      // 
      resources.ApplyResources(this.xrTableCell339, "xrTableCell339");
      this.xrTableCell339.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell339.Name = "xrTableCell339";
      this.xrTableCell339.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell339.StylePriority.UseBackColor = false;
      this.xrTableCell339.StylePriority.UseBorderColor = false;
      this.xrTableCell339.StylePriority.UseBorders = false;
      this.xrTableCell339.StylePriority.UseFont = false;
      this.xrTableCell339.StylePriority.UseForeColor = false;
      this.xrTableCell339.StylePriority.UsePadding = false;
      this.xrTableCell339.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell130
      // 
      resources.ApplyResources(this.xrTableCell130, "xrTableCell130");
      this.xrTableCell130.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell130.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TradeInformation.TradeInformation_MarketPrices.PriceMarket")});
      this.xrTableCell130.Name = "xrTableCell130";
      this.xrTableCell130.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell130.StylePriority.UseBackColor = false;
      this.xrTableCell130.StylePriority.UseBorderColor = false;
      this.xrTableCell130.StylePriority.UseBorders = false;
      this.xrTableCell130.StylePriority.UseFont = false;
      this.xrTableCell130.StylePriority.UseForeColor = false;
      this.xrTableCell130.StylePriority.UsePadding = false;
      this.xrTableCell130.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow52
      // 
      this.xrTableRow52.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell133,
            this.xrTableCell281,
            this.xrTableCell341,
            this.xrTableCell134});
      resources.ApplyResources(this.xrTableRow52, "xrTableRow52");
      this.xrTableRow52.Name = "xrTableRow52";
      // 
      // xrTableCell133
      // 
      resources.ApplyResources(this.xrTableCell133, "xrTableCell133");
      this.xrTableCell133.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell133.Name = "xrTableCell133";
      this.xrTableCell133.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell133.StylePriority.UseBackColor = false;
      this.xrTableCell133.StylePriority.UseBorderColor = false;
      this.xrTableCell133.StylePriority.UseBorders = false;
      this.xrTableCell133.StylePriority.UseFont = false;
      this.xrTableCell133.StylePriority.UseForeColor = false;
      this.xrTableCell133.StylePriority.UsePadding = false;
      this.xrTableCell133.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell281
      // 
      resources.ApplyResources(this.xrTableCell281, "xrTableCell281");
      this.xrTableCell281.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell281.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TradeInformation.TradeInformation_MarketPrices.PriceHigh")});
      this.xrTableCell281.Name = "xrTableCell281";
      this.xrTableCell281.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell281.StylePriority.UseBackColor = false;
      this.xrTableCell281.StylePriority.UseBorderColor = false;
      this.xrTableCell281.StylePriority.UseBorders = false;
      this.xrTableCell281.StylePriority.UseFont = false;
      this.xrTableCell281.StylePriority.UseForeColor = false;
      this.xrTableCell281.StylePriority.UsePadding = false;
      this.xrTableCell281.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell341
      // 
      resources.ApplyResources(this.xrTableCell341, "xrTableCell341");
      this.xrTableCell341.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell341.Name = "xrTableCell341";
      this.xrTableCell341.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell341.StylePriority.UseBackColor = false;
      this.xrTableCell341.StylePriority.UseBorderColor = false;
      this.xrTableCell341.StylePriority.UseBorders = false;
      this.xrTableCell341.StylePriority.UseFont = false;
      this.xrTableCell341.StylePriority.UseForeColor = false;
      this.xrTableCell341.StylePriority.UsePadding = false;
      this.xrTableCell341.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell134
      // 
      resources.ApplyResources(this.xrTableCell134, "xrTableCell134");
      this.xrTableCell134.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell134.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TradeInformation.TradeInformation_MarketPrices.PriceLow")});
      this.xrTableCell134.Name = "xrTableCell134";
      this.xrTableCell134.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell134.StylePriority.UseBackColor = false;
      this.xrTableCell134.StylePriority.UseBorderColor = false;
      this.xrTableCell134.StylePriority.UseBorders = false;
      this.xrTableCell134.StylePriority.UseFont = false;
      this.xrTableCell134.StylePriority.UseForeColor = false;
      this.xrTableCell134.StylePriority.UsePadding = false;
      this.xrTableCell134.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow88
      // 
      this.xrTableRow88.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell214});
      resources.ApplyResources(this.xrTableRow88, "xrTableRow88");
      this.xrTableRow88.Name = "xrTableRow88";
      // 
      // xrTableCell214
      // 
      resources.ApplyResources(this.xrTableCell214, "xrTableCell214");
      this.xrTableCell214.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTableCell214.CanGrow = false;
      this.xrTableCell214.Name = "xrTableCell214";
      this.xrTableCell214.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell214.StylePriority.UseBackColor = false;
      this.xrTableCell214.StylePriority.UseBorderColor = false;
      this.xrTableCell214.StylePriority.UseBorders = false;
      this.xrTableCell214.StylePriority.UseFont = false;
      this.xrTableCell214.StylePriority.UseForeColor = false;
      this.xrTableCell214.StylePriority.UsePadding = false;
      this.xrTableCell214.StylePriority.UseTextAlignment = false;
      // 
      // SummaryBlockC
      // 
      this.SummaryBlockC.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail15});
      resources.ApplyResources(this.SummaryBlockC, "SummaryBlockC");
      this.SummaryBlockC.Level = 2;
      this.SummaryBlockC.Name = "SummaryBlockC";
      // 
      // Detail15
      // 
      this.Detail15.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable18,
            this.xrTable14});
      resources.ApplyResources(this.Detail15, "Detail15");
      this.Detail15.KeepTogether = true;
      this.Detail15.Name = "Detail15";
      // 
      // xrTable18
      // 
      resources.ApplyResources(this.xrTable18, "xrTable18");
      this.xrTable18.KeepTogether = true;
      this.xrTable18.Name = "xrTable18";
      this.xrTable18.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow113,
            this.xrTableRow86,
            this.xrTableRow114,
            this.xrTableRow115,
            this.xrTableRow116,
            this.xrTableRow117,
            this.xrTableRow118,
            this.xrTableRow119,
            this.xrTableRow120,
            this.xrTableRow53,
            this.xrTableRow121,
            this.xrTableRow122,
            this.xrTableRow87});
      // 
      // xrTableRow113
      // 
      this.xrTableRow113.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell261,
            this.xrTableCell262});
      resources.ApplyResources(this.xrTableRow113, "xrTableRow113");
      this.xrTableRow113.Name = "xrTableRow113";
      // 
      // xrTableCell261
      // 
      resources.ApplyResources(this.xrTableCell261, "xrTableCell261");
      this.xrTableCell261.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell261.CanGrow = false;
      this.xrTableCell261.Name = "xrTableCell261";
      this.xrTableCell261.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell261.StylePriority.UseBackColor = false;
      this.xrTableCell261.StylePriority.UseBorderColor = false;
      this.xrTableCell261.StylePriority.UseBorders = false;
      this.xrTableCell261.StylePriority.UseFont = false;
      this.xrTableCell261.StylePriority.UseForeColor = false;
      this.xrTableCell261.StylePriority.UsePadding = false;
      this.xrTableCell261.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell262
      // 
      resources.ApplyResources(this.xrTableCell262, "xrTableCell262");
      this.xrTableCell262.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell262.CanGrow = false;
      this.xrTableCell262.Name = "xrTableCell262";
      this.xrTableCell262.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell262.StylePriority.UseBackColor = false;
      this.xrTableCell262.StylePriority.UseBorderColor = false;
      this.xrTableCell262.StylePriority.UseBorders = false;
      this.xrTableCell262.StylePriority.UseFont = false;
      this.xrTableCell262.StylePriority.UseForeColor = false;
      this.xrTableCell262.StylePriority.UsePadding = false;
      this.xrTableCell262.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow86
      // 
      this.xrTableRow86.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell209});
      resources.ApplyResources(this.xrTableRow86, "xrTableRow86");
      this.xrTableRow86.Name = "xrTableRow86";
      // 
      // xrTableCell209
      // 
      resources.ApplyResources(this.xrTableCell209, "xrTableCell209");
      this.xrTableCell209.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTableCell209.CanGrow = false;
      this.xrTableCell209.Name = "xrTableCell209";
      this.xrTableCell209.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell209.StylePriority.UseBackColor = false;
      this.xrTableCell209.StylePriority.UseBorderColor = false;
      this.xrTableCell209.StylePriority.UseBorders = false;
      this.xrTableCell209.StylePriority.UseFont = false;
      this.xrTableCell209.StylePriority.UseForeColor = false;
      this.xrTableCell209.StylePriority.UsePadding = false;
      this.xrTableCell209.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow114
      // 
      this.xrTableRow114.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell263,
            this.xrTableCell264});
      resources.ApplyResources(this.xrTableRow114, "xrTableRow114");
      this.xrTableRow114.Name = "xrTableRow114";
      // 
      // xrTableCell263
      // 
      resources.ApplyResources(this.xrTableCell263, "xrTableCell263");
      this.xrTableCell263.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell263.Name = "xrTableCell263";
      this.xrTableCell263.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell263.StylePriority.UseBackColor = false;
      this.xrTableCell263.StylePriority.UseBorderColor = false;
      this.xrTableCell263.StylePriority.UseBorders = false;
      this.xrTableCell263.StylePriority.UseFont = false;
      this.xrTableCell263.StylePriority.UseForeColor = false;
      this.xrTableCell263.StylePriority.UsePadding = false;
      this.xrTableCell263.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell264
      // 
      resources.ApplyResources(this.xrTableCell264, "xrTableCell264");
      this.xrTableCell264.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell264.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAffordability.PredictedIncome", "{0:c2}")});
      this.xrTableCell264.Name = "xrTableCell264";
      this.xrTableCell264.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell264.StylePriority.UseBackColor = false;
      this.xrTableCell264.StylePriority.UseBorderColor = false;
      this.xrTableCell264.StylePriority.UseBorders = false;
      this.xrTableCell264.StylePriority.UseFont = false;
      this.xrTableCell264.StylePriority.UseForeColor = false;
      this.xrTableCell264.StylePriority.UsePadding = false;
      this.xrTableCell264.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow115
      // 
      this.xrTableRow115.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell265,
            this.xrTableCell266});
      resources.ApplyResources(this.xrTableRow115, "xrTableRow115");
      this.xrTableRow115.Name = "xrTableRow115";
      // 
      // xrTableCell265
      // 
      resources.ApplyResources(this.xrTableCell265, "xrTableCell265");
      this.xrTableCell265.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell265.Name = "xrTableCell265";
      this.xrTableCell265.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell265.StylePriority.UseBackColor = false;
      this.xrTableCell265.StylePriority.UseBorderColor = false;
      this.xrTableCell265.StylePriority.UseBorders = false;
      this.xrTableCell265.StylePriority.UseFont = false;
      this.xrTableCell265.StylePriority.UseForeColor = false;
      this.xrTableCell265.StylePriority.UsePadding = false;
      this.xrTableCell265.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell266
      // 
      resources.ApplyResources(this.xrTableCell266, "xrTableCell266");
      this.xrTableCell266.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell266.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAffordability.TotalCommitments", "{0:c2}")});
      this.xrTableCell266.Name = "xrTableCell266";
      this.xrTableCell266.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell266.StylePriority.UseBackColor = false;
      this.xrTableCell266.StylePriority.UseBorderColor = false;
      this.xrTableCell266.StylePriority.UseBorders = false;
      this.xrTableCell266.StylePriority.UseFont = false;
      this.xrTableCell266.StylePriority.UseForeColor = false;
      this.xrTableCell266.StylePriority.UsePadding = false;
      this.xrTableCell266.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow116
      // 
      this.xrTableRow116.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell267,
            this.xrTableCell268});
      resources.ApplyResources(this.xrTableRow116, "xrTableRow116");
      this.xrTableRow116.Name = "xrTableRow116";
      // 
      // xrTableCell267
      // 
      resources.ApplyResources(this.xrTableCell267, "xrTableCell267");
      this.xrTableCell267.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell267.Name = "xrTableCell267";
      this.xrTableCell267.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell267.StylePriority.UseBackColor = false;
      this.xrTableCell267.StylePriority.UseBorderColor = false;
      this.xrTableCell267.StylePriority.UseBorders = false;
      this.xrTableCell267.StylePriority.UseFont = false;
      this.xrTableCell267.StylePriority.UseForeColor = false;
      this.xrTableCell267.StylePriority.UsePadding = false;
      this.xrTableCell267.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell268
      // 
      resources.ApplyResources(this.xrTableCell268, "xrTableCell268");
      this.xrTableCell268.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell268.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAffordability.PredAvailableinstalment", "{0:c2}")});
      this.xrTableCell268.Name = "xrTableCell268";
      this.xrTableCell268.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell268.StylePriority.UseBackColor = false;
      this.xrTableCell268.StylePriority.UseBorderColor = false;
      this.xrTableCell268.StylePriority.UseBorders = false;
      this.xrTableCell268.StylePriority.UseFont = false;
      this.xrTableCell268.StylePriority.UseForeColor = false;
      this.xrTableCell268.StylePriority.UsePadding = false;
      this.xrTableCell268.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow117
      // 
      this.xrTableRow117.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell269,
            this.xrTableCell270});
      resources.ApplyResources(this.xrTableRow117, "xrTableRow117");
      this.xrTableRow117.Name = "xrTableRow117";
      // 
      // xrTableCell269
      // 
      resources.ApplyResources(this.xrTableCell269, "xrTableCell269");
      this.xrTableCell269.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell269.Name = "xrTableCell269";
      this.xrTableCell269.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell269.StylePriority.UseBackColor = false;
      this.xrTableCell269.StylePriority.UseBorderColor = false;
      this.xrTableCell269.StylePriority.UseBorders = false;
      this.xrTableCell269.StylePriority.UseFont = false;
      this.xrTableCell269.StylePriority.UseForeColor = false;
      this.xrTableCell269.StylePriority.UsePadding = false;
      this.xrTableCell269.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell270
      // 
      resources.ApplyResources(this.xrTableCell270, "xrTableCell270");
      this.xrTableCell270.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell270.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerCPANLRDebtSummary.JudgementCount")});
      this.xrTableCell270.Name = "xrTableCell270";
      this.xrTableCell270.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell270.Scripts.OnBeforePrint = "xrTableCell270_BeforePrint";
      this.xrTableCell270.StylePriority.UseBackColor = false;
      this.xrTableCell270.StylePriority.UseBorderColor = false;
      this.xrTableCell270.StylePriority.UseBorders = false;
      this.xrTableCell270.StylePriority.UseFont = false;
      this.xrTableCell270.StylePriority.UseForeColor = false;
      this.xrTableCell270.StylePriority.UsePadding = false;
      this.xrTableCell270.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow118
      // 
      this.xrTableRow118.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell271,
            this.xrTableCell272});
      resources.ApplyResources(this.xrTableRow118, "xrTableRow118");
      this.xrTableRow118.Name = "xrTableRow118";
      // 
      // xrTableCell271
      // 
      resources.ApplyResources(this.xrTableCell271, "xrTableCell271");
      this.xrTableCell271.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell271.Name = "xrTableCell271";
      this.xrTableCell271.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell271.StylePriority.UseBackColor = false;
      this.xrTableCell271.StylePriority.UseBorderColor = false;
      this.xrTableCell271.StylePriority.UseBorders = false;
      this.xrTableCell271.StylePriority.UseFont = false;
      this.xrTableCell271.StylePriority.UseForeColor = false;
      this.xrTableCell271.StylePriority.UsePadding = false;
      this.xrTableCell271.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell272
      // 
      resources.ApplyResources(this.xrTableCell272, "xrTableCell272");
      this.xrTableCell272.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell272.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerCPANLRDebtSummary.HighestMonthsinArrearsCPA")});
      this.xrTableCell272.Name = "xrTableCell272";
      this.xrTableCell272.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell272.Scripts.OnBeforePrint = "xrTableCell272_BeforePrint";
      this.xrTableCell272.StylePriority.UseBackColor = false;
      this.xrTableCell272.StylePriority.UseBorderColor = false;
      this.xrTableCell272.StylePriority.UseBorders = false;
      this.xrTableCell272.StylePriority.UseFont = false;
      this.xrTableCell272.StylePriority.UseForeColor = false;
      this.xrTableCell272.StylePriority.UsePadding = false;
      this.xrTableCell272.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow119
      // 
      this.xrTableRow119.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell273,
            this.xrTableCell274});
      resources.ApplyResources(this.xrTableRow119, "xrTableRow119");
      this.xrTableRow119.Name = "xrTableRow119";
      // 
      // xrTableCell273
      // 
      resources.ApplyResources(this.xrTableCell273, "xrTableCell273");
      this.xrTableCell273.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell273.Name = "xrTableCell273";
      this.xrTableCell273.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell273.StylePriority.UseBackColor = false;
      this.xrTableCell273.StylePriority.UseBorderColor = false;
      this.xrTableCell273.StylePriority.UseBorders = false;
      this.xrTableCell273.StylePriority.UseFont = false;
      this.xrTableCell273.StylePriority.UseForeColor = false;
      this.xrTableCell273.StylePriority.UsePadding = false;
      this.xrTableCell273.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell274
      // 
      resources.ApplyResources(this.xrTableCell274, "xrTableCell274");
      this.xrTableCell274.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell274.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.CaseNumber")});
      this.xrTableCell274.Name = "xrTableCell274";
      this.xrTableCell274.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell274.Scripts.OnBeforePrint = "xrTableCell274_BeforePrint";
      this.xrTableCell274.StylePriority.UseBackColor = false;
      this.xrTableCell274.StylePriority.UseBorderColor = false;
      this.xrTableCell274.StylePriority.UseBorders = false;
      this.xrTableCell274.StylePriority.UseFont = false;
      this.xrTableCell274.StylePriority.UseForeColor = false;
      this.xrTableCell274.StylePriority.UsePadding = false;
      this.xrTableCell274.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow120
      // 
      this.xrTableRow120.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell275,
            this.xrTableCell276});
      resources.ApplyResources(this.xrTableRow120, "xrTableRow120");
      this.xrTableRow120.Name = "xrTableRow120";
      // 
      // xrTableCell275
      // 
      resources.ApplyResources(this.xrTableCell275, "xrTableCell275");
      this.xrTableCell275.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell275.Name = "xrTableCell275";
      this.xrTableCell275.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell275.StylePriority.UseBackColor = false;
      this.xrTableCell275.StylePriority.UseBorderColor = false;
      this.xrTableCell275.StylePriority.UseBorders = false;
      this.xrTableCell275.StylePriority.UseFont = false;
      this.xrTableCell275.StylePriority.UseForeColor = false;
      this.xrTableCell275.StylePriority.UsePadding = false;
      this.xrTableCell275.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell276
      // 
      resources.ApplyResources(this.xrTableCell276, "xrTableCell276");
      this.xrTableCell276.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell276.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerCPANLRDebtSummary.CourtNoticeCount")});
      this.xrTableCell276.Name = "xrTableCell276";
      this.xrTableCell276.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell276.Scripts.OnBeforePrint = "xrTableCell276_BeforePrint";
      this.xrTableCell276.StylePriority.UseBackColor = false;
      this.xrTableCell276.StylePriority.UseBorderColor = false;
      this.xrTableCell276.StylePriority.UseBorders = false;
      this.xrTableCell276.StylePriority.UseFont = false;
      this.xrTableCell276.StylePriority.UseForeColor = false;
      this.xrTableCell276.StylePriority.UsePadding = false;
      this.xrTableCell276.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow53
      // 
      this.xrTableRow53.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell32,
            this.xrTableCell57});
      resources.ApplyResources(this.xrTableRow53, "xrTableRow53");
      this.xrTableRow53.Name = "xrTableRow53";
      // 
      // xrTableCell32
      // 
      resources.ApplyResources(this.xrTableCell32, "xrTableCell32");
      this.xrTableCell32.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell32.Name = "xrTableCell32";
      this.xrTableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell32.StylePriority.UseBackColor = false;
      this.xrTableCell32.StylePriority.UseBorderColor = false;
      this.xrTableCell32.StylePriority.UseBorders = false;
      this.xrTableCell32.StylePriority.UseFont = false;
      this.xrTableCell32.StylePriority.UseForeColor = false;
      this.xrTableCell32.StylePriority.UsePadding = false;
      this.xrTableCell32.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell57
      // 
      resources.ApplyResources(this.xrTableCell57, "xrTableCell57");
      this.xrTableCell57.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell57.Name = "xrTableCell57";
      this.xrTableCell57.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell57.StylePriority.UseBackColor = false;
      this.xrTableCell57.StylePriority.UseBorderColor = false;
      this.xrTableCell57.StylePriority.UseBorders = false;
      this.xrTableCell57.StylePriority.UseFont = false;
      this.xrTableCell57.StylePriority.UseForeColor = false;
      this.xrTableCell57.StylePriority.UsePadding = false;
      this.xrTableCell57.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow121
      // 
      this.xrTableRow121.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell277,
            this.xrTableCell278});
      resources.ApplyResources(this.xrTableRow121, "xrTableRow121");
      this.xrTableRow121.Name = "xrTableRow121";
      // 
      // xrTableCell277
      // 
      resources.ApplyResources(this.xrTableCell277, "xrTableCell277");
      this.xrTableCell277.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell277.Name = "xrTableCell277";
      this.xrTableCell277.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell277.StylePriority.UseBackColor = false;
      this.xrTableCell277.StylePriority.UseBorderColor = false;
      this.xrTableCell277.StylePriority.UseBorders = false;
      this.xrTableCell277.StylePriority.UseFont = false;
      this.xrTableCell277.StylePriority.UseForeColor = false;
      this.xrTableCell277.StylePriority.UsePadding = false;
      this.xrTableCell277.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell278
      // 
      resources.ApplyResources(this.xrTableCell278, "xrTableCell278");
      this.xrTableCell278.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell278.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorSummary.NumberOfCompanyDirector")});
      this.xrTableCell278.Name = "xrTableCell278";
      this.xrTableCell278.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell278.Scripts.OnBeforePrint = "xrTableCell278_BeforePrint";
      this.xrTableCell278.StylePriority.UseBackColor = false;
      this.xrTableCell278.StylePriority.UseBorderColor = false;
      this.xrTableCell278.StylePriority.UseBorders = false;
      this.xrTableCell278.StylePriority.UseFont = false;
      this.xrTableCell278.StylePriority.UseForeColor = false;
      this.xrTableCell278.StylePriority.UsePadding = false;
      this.xrTableCell278.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow122
      // 
      this.xrTableRow122.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell279,
            this.xrTableCell280});
      resources.ApplyResources(this.xrTableRow122, "xrTableRow122");
      this.xrTableRow122.Name = "xrTableRow122";
      // 
      // xrTableCell279
      // 
      resources.ApplyResources(this.xrTableCell279, "xrTableCell279");
      this.xrTableCell279.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell279.Name = "xrTableCell279";
      this.xrTableCell279.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell279.StylePriority.UseBackColor = false;
      this.xrTableCell279.StylePriority.UseBorderColor = false;
      this.xrTableCell279.StylePriority.UseBorders = false;
      this.xrTableCell279.StylePriority.UseFont = false;
      this.xrTableCell279.StylePriority.UseForeColor = false;
      this.xrTableCell279.StylePriority.UsePadding = false;
      this.xrTableCell279.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell280
      // 
      resources.ApplyResources(this.xrTableCell280, "xrTableCell280");
      this.xrTableCell280.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell280.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformationSummary.TotalProperty")});
      this.xrTableCell280.Name = "xrTableCell280";
      this.xrTableCell280.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell280.Scripts.OnBeforePrint = "xrTableCell280_BeforePrint";
      this.xrTableCell280.StylePriority.UseBackColor = false;
      this.xrTableCell280.StylePriority.UseBorderColor = false;
      this.xrTableCell280.StylePriority.UseBorders = false;
      this.xrTableCell280.StylePriority.UseFont = false;
      this.xrTableCell280.StylePriority.UseForeColor = false;
      this.xrTableCell280.StylePriority.UsePadding = false;
      this.xrTableCell280.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow87
      // 
      this.xrTableRow87.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell212});
      resources.ApplyResources(this.xrTableRow87, "xrTableRow87");
      this.xrTableRow87.Name = "xrTableRow87";
      // 
      // xrTableCell212
      // 
      resources.ApplyResources(this.xrTableCell212, "xrTableCell212");
      this.xrTableCell212.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTableCell212.CanGrow = false;
      this.xrTableCell212.Name = "xrTableCell212";
      this.xrTableCell212.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell212.StylePriority.UseBackColor = false;
      this.xrTableCell212.StylePriority.UseBorderColor = false;
      this.xrTableCell212.StylePriority.UseBorders = false;
      this.xrTableCell212.StylePriority.UseFont = false;
      this.xrTableCell212.StylePriority.UseForeColor = false;
      this.xrTableCell212.StylePriority.UsePadding = false;
      this.xrTableCell212.StylePriority.UseTextAlignment = false;
      // 
      // xrTable14
      // 
      resources.ApplyResources(this.xrTable14, "xrTable14");
      this.xrTable14.KeepTogether = true;
      this.xrTable14.Name = "xrTable14";
      this.xrTable14.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow73,
            this.xrTableRow85,
            this.xrTableRow75,
            this.xrTableRow76,
            this.xrTableRow77,
            this.xrTableRow78,
            this.xrTableRow79,
            this.xrTableRow80,
            this.xrTableRow81,
            this.xrTableRow82,
            this.xrTableRow83,
            this.xrTableRow84,
            this.xrTableRow74});
      // 
      // xrTableRow73
      // 
      this.xrTableRow73.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell149,
            this.xrTableCell150});
      resources.ApplyResources(this.xrTableRow73, "xrTableRow73");
      this.xrTableRow73.Name = "xrTableRow73";
      // 
      // xrTableCell149
      // 
      resources.ApplyResources(this.xrTableCell149, "xrTableCell149");
      this.xrTableCell149.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell149.CanGrow = false;
      this.xrTableCell149.Name = "xrTableCell149";
      this.xrTableCell149.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell149.StylePriority.UseBackColor = false;
      this.xrTableCell149.StylePriority.UseBorderColor = false;
      this.xrTableCell149.StylePriority.UseBorders = false;
      this.xrTableCell149.StylePriority.UseFont = false;
      this.xrTableCell149.StylePriority.UseForeColor = false;
      this.xrTableCell149.StylePriority.UsePadding = false;
      this.xrTableCell149.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell150
      // 
      resources.ApplyResources(this.xrTableCell150, "xrTableCell150");
      this.xrTableCell150.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell150.CanGrow = false;
      this.xrTableCell150.Name = "xrTableCell150";
      this.xrTableCell150.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell150.StylePriority.UseBackColor = false;
      this.xrTableCell150.StylePriority.UseBorderColor = false;
      this.xrTableCell150.StylePriority.UseBorders = false;
      this.xrTableCell150.StylePriority.UseFont = false;
      this.xrTableCell150.StylePriority.UseForeColor = false;
      this.xrTableCell150.StylePriority.UsePadding = false;
      this.xrTableCell150.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow85
      // 
      this.xrTableRow85.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell210});
      resources.ApplyResources(this.xrTableRow85, "xrTableRow85");
      this.xrTableRow85.Name = "xrTableRow85";
      // 
      // xrTableCell210
      // 
      resources.ApplyResources(this.xrTableCell210, "xrTableCell210");
      this.xrTableCell210.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTableCell210.CanGrow = false;
      this.xrTableCell210.Name = "xrTableCell210";
      this.xrTableCell210.StylePriority.UseBackColor = false;
      this.xrTableCell210.StylePriority.UseBorderColor = false;
      this.xrTableCell210.StylePriority.UseBorders = false;
      this.xrTableCell210.StylePriority.UseFont = false;
      this.xrTableCell210.StylePriority.UseForeColor = false;
      // 
      // xrTableRow75
      // 
      this.xrTableRow75.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell151,
            this.xrTableCell152});
      resources.ApplyResources(this.xrTableRow75, "xrTableRow75");
      this.xrTableRow75.Name = "xrTableRow75";
      // 
      // xrTableCell151
      // 
      resources.ApplyResources(this.xrTableCell151, "xrTableCell151");
      this.xrTableCell151.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell151.Name = "xrTableCell151";
      this.xrTableCell151.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell151.StylePriority.UseBackColor = false;
      this.xrTableCell151.StylePriority.UseBorderColor = false;
      this.xrTableCell151.StylePriority.UseBorders = false;
      this.xrTableCell151.StylePriority.UseFont = false;
      this.xrTableCell151.StylePriority.UseForeColor = false;
      this.xrTableCell151.StylePriority.UsePadding = false;
      this.xrTableCell151.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell152
      // 
      resources.ApplyResources(this.xrTableCell152, "xrTableCell152");
      this.xrTableCell152.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell152.Name = "xrTableCell152";
      this.xrTableCell152.StylePriority.UseBackColor = false;
      this.xrTableCell152.StylePriority.UseBorderColor = false;
      this.xrTableCell152.StylePriority.UseBorders = false;
      this.xrTableCell152.StylePriority.UseFont = false;
      this.xrTableCell152.StylePriority.UseForeColor = false;
      // 
      // xrTableRow76
      // 
      this.xrTableRow76.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell153,
            this.xrTableCell154});
      resources.ApplyResources(this.xrTableRow76, "xrTableRow76");
      this.xrTableRow76.Name = "xrTableRow76";
      // 
      // xrTableCell153
      // 
      resources.ApplyResources(this.xrTableCell153, "xrTableCell153");
      this.xrTableCell153.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell153.Name = "xrTableCell153";
      this.xrTableCell153.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell153.StylePriority.UseBackColor = false;
      this.xrTableCell153.StylePriority.UseBorderColor = false;
      this.xrTableCell153.StylePriority.UseBorders = false;
      this.xrTableCell153.StylePriority.UseFont = false;
      this.xrTableCell153.StylePriority.UseForeColor = false;
      this.xrTableCell153.StylePriority.UsePadding = false;
      this.xrTableCell153.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell154
      // 
      resources.ApplyResources(this.xrTableCell154, "xrTableCell154");
      this.xrTableCell154.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell154.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.IDNo")});
      this.xrTableCell154.Name = "xrTableCell154";
      this.xrTableCell154.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell154.StylePriority.UseBackColor = false;
      this.xrTableCell154.StylePriority.UseBorderColor = false;
      this.xrTableCell154.StylePriority.UseBorders = false;
      this.xrTableCell154.StylePriority.UseFont = false;
      this.xrTableCell154.StylePriority.UseForeColor = false;
      this.xrTableCell154.StylePriority.UsePadding = false;
      this.xrTableCell154.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow77
      // 
      this.xrTableRow77.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell155,
            this.xrTableCell156});
      resources.ApplyResources(this.xrTableRow77, "xrTableRow77");
      this.xrTableRow77.Name = "xrTableRow77";
      // 
      // xrTableCell155
      // 
      resources.ApplyResources(this.xrTableCell155, "xrTableCell155");
      this.xrTableCell155.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell155.Name = "xrTableCell155";
      this.xrTableCell155.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell155.StylePriority.UseBackColor = false;
      this.xrTableCell155.StylePriority.UseBorderColor = false;
      this.xrTableCell155.StylePriority.UseBorders = false;
      this.xrTableCell155.StylePriority.UseFont = false;
      this.xrTableCell155.StylePriority.UseForeColor = false;
      this.xrTableCell155.StylePriority.UsePadding = false;
      this.xrTableCell155.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell156
      // 
      resources.ApplyResources(this.xrTableCell156, "xrTableCell156");
      this.xrTableCell156.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell156.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.BirthDate")});
      this.xrTableCell156.Name = "xrTableCell156";
      this.xrTableCell156.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell156.StylePriority.UseBackColor = false;
      this.xrTableCell156.StylePriority.UseBorderColor = false;
      this.xrTableCell156.StylePriority.UseBorders = false;
      this.xrTableCell156.StylePriority.UseFont = false;
      this.xrTableCell156.StylePriority.UseForeColor = false;
      this.xrTableCell156.StylePriority.UsePadding = false;
      this.xrTableCell156.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow78
      // 
      this.xrTableRow78.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell157,
            this.xrTableCell158});
      resources.ApplyResources(this.xrTableRow78, "xrTableRow78");
      this.xrTableRow78.Name = "xrTableRow78";
      // 
      // xrTableCell157
      // 
      resources.ApplyResources(this.xrTableCell157, "xrTableCell157");
      this.xrTableCell157.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell157.Name = "xrTableCell157";
      this.xrTableCell157.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell157.StylePriority.UseBackColor = false;
      this.xrTableCell157.StylePriority.UseBorderColor = false;
      this.xrTableCell157.StylePriority.UseBorders = false;
      this.xrTableCell157.StylePriority.UseFont = false;
      this.xrTableCell157.StylePriority.UseForeColor = false;
      this.xrTableCell157.StylePriority.UsePadding = false;
      this.xrTableCell157.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell158
      // 
      resources.ApplyResources(this.xrTableCell158, "xrTableCell158");
      this.xrTableCell158.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell158.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.FirstName")});
      this.xrTableCell158.Name = "xrTableCell158";
      this.xrTableCell158.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell158.StylePriority.UseBackColor = false;
      this.xrTableCell158.StylePriority.UseBorderColor = false;
      this.xrTableCell158.StylePriority.UseBorders = false;
      this.xrTableCell158.StylePriority.UseFont = false;
      this.xrTableCell158.StylePriority.UseForeColor = false;
      this.xrTableCell158.StylePriority.UsePadding = false;
      this.xrTableCell158.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow79
      // 
      this.xrTableRow79.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell159,
            this.xrTableCell160});
      resources.ApplyResources(this.xrTableRow79, "xrTableRow79");
      this.xrTableRow79.Name = "xrTableRow79";
      // 
      // xrTableCell159
      // 
      resources.ApplyResources(this.xrTableCell159, "xrTableCell159");
      this.xrTableCell159.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell159.Name = "xrTableCell159";
      this.xrTableCell159.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell159.StylePriority.UseBackColor = false;
      this.xrTableCell159.StylePriority.UseBorderColor = false;
      this.xrTableCell159.StylePriority.UseBorders = false;
      this.xrTableCell159.StylePriority.UseFont = false;
      this.xrTableCell159.StylePriority.UseForeColor = false;
      this.xrTableCell159.StylePriority.UsePadding = false;
      this.xrTableCell159.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell160
      // 
      resources.ApplyResources(this.xrTableCell160, "xrTableCell160");
      this.xrTableCell160.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell160.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.Surname")});
      this.xrTableCell160.Name = "xrTableCell160";
      this.xrTableCell160.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell160.StylePriority.UseBackColor = false;
      this.xrTableCell160.StylePriority.UseBorderColor = false;
      this.xrTableCell160.StylePriority.UseBorders = false;
      this.xrTableCell160.StylePriority.UseFont = false;
      this.xrTableCell160.StylePriority.UseForeColor = false;
      this.xrTableCell160.StylePriority.UsePadding = false;
      this.xrTableCell160.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow80
      // 
      this.xrTableRow80.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell161,
            this.xrTableCell162});
      resources.ApplyResources(this.xrTableRow80, "xrTableRow80");
      this.xrTableRow80.Name = "xrTableRow80";
      // 
      // xrTableCell161
      // 
      resources.ApplyResources(this.xrTableCell161, "xrTableCell161");
      this.xrTableCell161.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell161.Name = "xrTableCell161";
      this.xrTableCell161.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell161.StylePriority.UseBackColor = false;
      this.xrTableCell161.StylePriority.UseBorderColor = false;
      this.xrTableCell161.StylePriority.UseBorders = false;
      this.xrTableCell161.StylePriority.UseFont = false;
      this.xrTableCell161.StylePriority.UseForeColor = false;
      this.xrTableCell161.StylePriority.UsePadding = false;
      this.xrTableCell161.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell162
      // 
      resources.ApplyResources(this.xrTableCell162, "xrTableCell162");
      this.xrTableCell162.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell162.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.ResidentialAddress")});
      this.xrTableCell162.Name = "xrTableCell162";
      this.xrTableCell162.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell162.StylePriority.UseBackColor = false;
      this.xrTableCell162.StylePriority.UseBorderColor = false;
      this.xrTableCell162.StylePriority.UseBorders = false;
      this.xrTableCell162.StylePriority.UseFont = false;
      this.xrTableCell162.StylePriority.UseForeColor = false;
      this.xrTableCell162.StylePriority.UsePadding = false;
      this.xrTableCell162.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow81
      // 
      this.xrTableRow81.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell163,
            this.xrTableCell164});
      resources.ApplyResources(this.xrTableRow81, "xrTableRow81");
      this.xrTableRow81.Name = "xrTableRow81";
      // 
      // xrTableCell163
      // 
      resources.ApplyResources(this.xrTableCell163, "xrTableCell163");
      this.xrTableCell163.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell163.Name = "xrTableCell163";
      this.xrTableCell163.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell163.StylePriority.UseBackColor = false;
      this.xrTableCell163.StylePriority.UseBorderColor = false;
      this.xrTableCell163.StylePriority.UseBorders = false;
      this.xrTableCell163.StylePriority.UseFont = false;
      this.xrTableCell163.StylePriority.UseForeColor = false;
      this.xrTableCell163.StylePriority.UsePadding = false;
      this.xrTableCell163.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell164
      // 
      resources.ApplyResources(this.xrTableCell164, "xrTableCell164");
      this.xrTableCell164.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell164.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.HomeTelephoneNo")});
      this.xrTableCell164.Name = "xrTableCell164";
      this.xrTableCell164.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell164.StylePriority.UseBackColor = false;
      this.xrTableCell164.StylePriority.UseBorderColor = false;
      this.xrTableCell164.StylePriority.UseBorders = false;
      this.xrTableCell164.StylePriority.UseFont = false;
      this.xrTableCell164.StylePriority.UseForeColor = false;
      this.xrTableCell164.StylePriority.UsePadding = false;
      this.xrTableCell164.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow82
      // 
      this.xrTableRow82.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell165,
            this.xrTableCell169});
      resources.ApplyResources(this.xrTableRow82, "xrTableRow82");
      this.xrTableRow82.Name = "xrTableRow82";
      // 
      // xrTableCell165
      // 
      resources.ApplyResources(this.xrTableCell165, "xrTableCell165");
      this.xrTableCell165.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell165.Name = "xrTableCell165";
      this.xrTableCell165.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell165.StylePriority.UseBackColor = false;
      this.xrTableCell165.StylePriority.UseBorderColor = false;
      this.xrTableCell165.StylePriority.UseBorders = false;
      this.xrTableCell165.StylePriority.UseFont = false;
      this.xrTableCell165.StylePriority.UseForeColor = false;
      this.xrTableCell165.StylePriority.UsePadding = false;
      this.xrTableCell165.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell169
      // 
      resources.ApplyResources(this.xrTableCell169, "xrTableCell169");
      this.xrTableCell169.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell169.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.WorkTelephoneNo")});
      this.xrTableCell169.Name = "xrTableCell169";
      this.xrTableCell169.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell169.StylePriority.UseBackColor = false;
      this.xrTableCell169.StylePriority.UseBorderColor = false;
      this.xrTableCell169.StylePriority.UseBorders = false;
      this.xrTableCell169.StylePriority.UseFont = false;
      this.xrTableCell169.StylePriority.UseForeColor = false;
      this.xrTableCell169.StylePriority.UsePadding = false;
      this.xrTableCell169.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow83
      // 
      this.xrTableRow83.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell170,
            this.xrTableCell174});
      resources.ApplyResources(this.xrTableRow83, "xrTableRow83");
      this.xrTableRow83.Name = "xrTableRow83";
      // 
      // xrTableCell170
      // 
      resources.ApplyResources(this.xrTableCell170, "xrTableCell170");
      this.xrTableCell170.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell170.Name = "xrTableCell170";
      this.xrTableCell170.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell170.StylePriority.UseBackColor = false;
      this.xrTableCell170.StylePriority.UseBorderColor = false;
      this.xrTableCell170.StylePriority.UseBorders = false;
      this.xrTableCell170.StylePriority.UseFont = false;
      this.xrTableCell170.StylePriority.UseForeColor = false;
      this.xrTableCell170.StylePriority.UsePadding = false;
      this.xrTableCell170.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell174
      // 
      resources.ApplyResources(this.xrTableCell174, "xrTableCell174");
      this.xrTableCell174.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell174.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.CellularNo")});
      this.xrTableCell174.Name = "xrTableCell174";
      this.xrTableCell174.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell174.StylePriority.UseBackColor = false;
      this.xrTableCell174.StylePriority.UseBorderColor = false;
      this.xrTableCell174.StylePriority.UseBorders = false;
      this.xrTableCell174.StylePriority.UseFont = false;
      this.xrTableCell174.StylePriority.UseForeColor = false;
      this.xrTableCell174.StylePriority.UsePadding = false;
      this.xrTableCell174.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow84
      // 
      this.xrTableRow84.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell175,
            this.xrTableCell176});
      resources.ApplyResources(this.xrTableRow84, "xrTableRow84");
      this.xrTableRow84.Name = "xrTableRow84";
      // 
      // xrTableCell175
      // 
      resources.ApplyResources(this.xrTableCell175, "xrTableCell175");
      this.xrTableCell175.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell175.Name = "xrTableCell175";
      this.xrTableCell175.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell175.StylePriority.UseBackColor = false;
      this.xrTableCell175.StylePriority.UseBorderColor = false;
      this.xrTableCell175.StylePriority.UseBorders = false;
      this.xrTableCell175.StylePriority.UseFont = false;
      this.xrTableCell175.StylePriority.UseForeColor = false;
      this.xrTableCell175.StylePriority.UsePadding = false;
      this.xrTableCell175.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell176
      // 
      resources.ApplyResources(this.xrTableCell176, "xrTableCell176");
      this.xrTableCell176.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell176.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDetail.EmployerDetail")});
      this.xrTableCell176.Name = "xrTableCell176";
      this.xrTableCell176.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell176.StylePriority.UseBackColor = false;
      this.xrTableCell176.StylePriority.UseBorderColor = false;
      this.xrTableCell176.StylePriority.UseBorders = false;
      this.xrTableCell176.StylePriority.UseFont = false;
      this.xrTableCell176.StylePriority.UseForeColor = false;
      this.xrTableCell176.StylePriority.UsePadding = false;
      this.xrTableCell176.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow74
      // 
      this.xrTableRow74.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell208});
      resources.ApplyResources(this.xrTableRow74, "xrTableRow74");
      this.xrTableRow74.Name = "xrTableRow74";
      // 
      // xrTableCell208
      // 
      resources.ApplyResources(this.xrTableCell208, "xrTableCell208");
      this.xrTableCell208.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTableCell208.CanGrow = false;
      this.xrTableCell208.Name = "xrTableCell208";
      this.xrTableCell208.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell208.StylePriority.UseBackColor = false;
      this.xrTableCell208.StylePriority.UseBorderColor = false;
      this.xrTableCell208.StylePriority.UseBorders = false;
      this.xrTableCell208.StylePriority.UseFont = false;
      this.xrTableCell208.StylePriority.UseForeColor = false;
      this.xrTableCell208.StylePriority.UsePadding = false;
      this.xrTableCell208.StylePriority.UseTextAlignment = false;
      // 
      // SumamryBlockD
      // 
      this.SumamryBlockD.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail16,
            this.GroupHeader8,
            this.DetailReport12});
      resources.ApplyResources(this.SumamryBlockD, "SumamryBlockD");
      this.SumamryBlockD.Level = 3;
      this.SumamryBlockD.Name = "SumamryBlockD";
      // 
      // Detail16
      // 
      resources.ApplyResources(this.Detail16, "Detail16");
      this.Detail16.Name = "Detail16";
      // 
      // GroupHeader8
      // 
      this.GroupHeader8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable22});
      resources.ApplyResources(this.GroupHeader8, "GroupHeader8");
      this.GroupHeader8.KeepTogether = true;
      this.GroupHeader8.Name = "GroupHeader8";
      // 
      // xrTable22
      // 
      resources.ApplyResources(this.xrTable22, "xrTable22");
      this.xrTable22.Name = "xrTable22";
      this.xrTable22.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow49,
            this.xrTableRow89,
            this.xrTableRow96,
            this.xrTableRow95,
            this.xrTableRow94,
            this.xrTableRow91});
      // 
      // xrTableRow49
      // 
      this.xrTableRow49.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell123,
            this.xrTableCell127});
      resources.ApplyResources(this.xrTableRow49, "xrTableRow49");
      this.xrTableRow49.Name = "xrTableRow49";
      // 
      // xrTableCell123
      // 
      resources.ApplyResources(this.xrTableCell123, "xrTableCell123");
      this.xrTableCell123.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell123.CanGrow = false;
      this.xrTableCell123.Name = "xrTableCell123";
      this.xrTableCell123.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell123.StylePriority.UseBackColor = false;
      this.xrTableCell123.StylePriority.UseBorderColor = false;
      this.xrTableCell123.StylePriority.UseBorders = false;
      this.xrTableCell123.StylePriority.UseFont = false;
      this.xrTableCell123.StylePriority.UseForeColor = false;
      this.xrTableCell123.StylePriority.UsePadding = false;
      this.xrTableCell123.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell127
      // 
      resources.ApplyResources(this.xrTableCell127, "xrTableCell127");
      this.xrTableCell127.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell127.CanGrow = false;
      this.xrTableCell127.Name = "xrTableCell127";
      this.xrTableCell127.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell127.StylePriority.UseBackColor = false;
      this.xrTableCell127.StylePriority.UseBorderColor = false;
      this.xrTableCell127.StylePriority.UseBorders = false;
      this.xrTableCell127.StylePriority.UseFont = false;
      this.xrTableCell127.StylePriority.UseForeColor = false;
      this.xrTableCell127.StylePriority.UsePadding = false;
      this.xrTableCell127.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow89
      // 
      this.xrTableRow89.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell121});
      resources.ApplyResources(this.xrTableRow89, "xrTableRow89");
      this.xrTableRow89.Name = "xrTableRow89";
      // 
      // xrTableCell121
      // 
      resources.ApplyResources(this.xrTableCell121, "xrTableCell121");
      this.xrTableCell121.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTableCell121.CanGrow = false;
      this.xrTableCell121.Name = "xrTableCell121";
      this.xrTableCell121.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell121.StylePriority.UseBackColor = false;
      this.xrTableCell121.StylePriority.UseBorderColor = false;
      this.xrTableCell121.StylePriority.UseBorders = false;
      this.xrTableCell121.StylePriority.UseFont = false;
      this.xrTableCell121.StylePriority.UseForeColor = false;
      this.xrTableCell121.StylePriority.UsePadding = false;
      this.xrTableCell121.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow96
      // 
      this.xrTableRow96.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell229,
            this.xrTableCell230,
            this.xrTableCell231,
            this.xrTableCell232});
      resources.ApplyResources(this.xrTableRow96, "xrTableRow96");
      this.xrTableRow96.Name = "xrTableRow96";
      // 
      // xrTableCell229
      // 
      resources.ApplyResources(this.xrTableCell229, "xrTableCell229");
      this.xrTableCell229.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell229.Name = "xrTableCell229";
      this.xrTableCell229.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell229.StylePriority.UseBackColor = false;
      this.xrTableCell229.StylePriority.UseBorderColor = false;
      this.xrTableCell229.StylePriority.UseBorders = false;
      this.xrTableCell229.StylePriority.UseFont = false;
      this.xrTableCell229.StylePriority.UseForeColor = false;
      this.xrTableCell229.StylePriority.UsePadding = false;
      this.xrTableCell229.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell230
      // 
      resources.ApplyResources(this.xrTableCell230, "xrTableCell230");
      this.xrTableCell230.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell230.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerScoring.FinalScore")});
      this.xrTableCell230.Name = "xrTableCell230";
      this.xrTableCell230.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell230.StylePriority.UseBackColor = false;
      this.xrTableCell230.StylePriority.UseBorderColor = false;
      this.xrTableCell230.StylePriority.UseBorders = false;
      this.xrTableCell230.StylePriority.UseFont = false;
      this.xrTableCell230.StylePriority.UseForeColor = false;
      this.xrTableCell230.StylePriority.UsePadding = false;
      this.xrTableCell230.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell231
      // 
      resources.ApplyResources(this.xrTableCell231, "xrTableCell231");
      this.xrTableCell231.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell231.Name = "xrTableCell231";
      this.xrTableCell231.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell231.StylePriority.UseBackColor = false;
      this.xrTableCell231.StylePriority.UseBorderColor = false;
      this.xrTableCell231.StylePriority.UseBorders = false;
      this.xrTableCell231.StylePriority.UseFont = false;
      this.xrTableCell231.StylePriority.UseForeColor = false;
      this.xrTableCell231.StylePriority.UsePadding = false;
      this.xrTableCell231.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell232
      // 
      resources.ApplyResources(this.xrTableCell232, "xrTableCell232");
      this.xrTableCell232.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell232.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerCPANLRDebtSummary.TotalJudgmentAmt")});
      this.xrTableCell232.Name = "xrTableCell232";
      this.xrTableCell232.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell232.Scripts.OnBeforePrint = "xrTableCell232_BeforePrint";
      this.xrTableCell232.StylePriority.UseBackColor = false;
      this.xrTableCell232.StylePriority.UseBorderColor = false;
      this.xrTableCell232.StylePriority.UseBorders = false;
      this.xrTableCell232.StylePriority.UseFont = false;
      this.xrTableCell232.StylePriority.UseForeColor = false;
      this.xrTableCell232.StylePriority.UsePadding = false;
      this.xrTableCell232.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow95
      // 
      this.xrTableRow95.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell225,
            this.xrTableCell226,
            this.xrTableCell227,
            this.xrTableCell228});
      resources.ApplyResources(this.xrTableRow95, "xrTableRow95");
      this.xrTableRow95.Name = "xrTableRow95";
      // 
      // xrTableCell225
      // 
      resources.ApplyResources(this.xrTableCell225, "xrTableCell225");
      this.xrTableCell225.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell225.Name = "xrTableCell225";
      this.xrTableCell225.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell225.StylePriority.UseBackColor = false;
      this.xrTableCell225.StylePriority.UseBorderColor = false;
      this.xrTableCell225.StylePriority.UseBorders = false;
      this.xrTableCell225.StylePriority.UseFont = false;
      this.xrTableCell225.StylePriority.UseForeColor = false;
      this.xrTableCell225.StylePriority.UsePadding = false;
      this.xrTableCell225.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell226
      // 
      resources.ApplyResources(this.xrTableCell226, "xrTableCell226");
      this.xrTableCell226.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell226.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerScoring.RiskCategory")});
      this.xrTableCell226.Name = "xrTableCell226";
      this.xrTableCell226.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell226.StylePriority.UseBackColor = false;
      this.xrTableCell226.StylePriority.UseBorderColor = false;
      this.xrTableCell226.StylePriority.UseBorders = false;
      this.xrTableCell226.StylePriority.UseFont = false;
      this.xrTableCell226.StylePriority.UseForeColor = false;
      this.xrTableCell226.StylePriority.UsePadding = false;
      this.xrTableCell226.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell227
      // 
      resources.ApplyResources(this.xrTableCell227, "xrTableCell227");
      this.xrTableCell227.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell227.Name = "xrTableCell227";
      this.xrTableCell227.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell227.StylePriority.UseBackColor = false;
      this.xrTableCell227.StylePriority.UseBorderColor = false;
      this.xrTableCell227.StylePriority.UseBorders = false;
      this.xrTableCell227.StylePriority.UseFont = false;
      this.xrTableCell227.StylePriority.UseForeColor = false;
      this.xrTableCell227.StylePriority.UsePadding = false;
      this.xrTableCell227.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell228
      // 
      resources.ApplyResources(this.xrTableCell228, "xrTableCell228");
      this.xrTableCell228.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell228.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerCPANLRDebtSummary.DefaultListingAmt")});
      this.xrTableCell228.Name = "xrTableCell228";
      this.xrTableCell228.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell228.Scripts.OnBeforePrint = "xrTableCell228_BeforePrint";
      this.xrTableCell228.StylePriority.UseBackColor = false;
      this.xrTableCell228.StylePriority.UseBorderColor = false;
      this.xrTableCell228.StylePriority.UseBorders = false;
      this.xrTableCell228.StylePriority.UseFont = false;
      this.xrTableCell228.StylePriority.UseForeColor = false;
      this.xrTableCell228.StylePriority.UsePadding = false;
      this.xrTableCell228.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow94
      // 
      this.xrTableRow94.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell221,
            this.xrTableCell222,
            this.xrTableCell223,
            this.xrTableCell224});
      resources.ApplyResources(this.xrTableRow94, "xrTableRow94");
      this.xrTableRow94.Name = "xrTableRow94";
      // 
      // xrTableCell221
      // 
      resources.ApplyResources(this.xrTableCell221, "xrTableCell221");
      this.xrTableCell221.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell221.Name = "xrTableCell221";
      this.xrTableCell221.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell221.StylePriority.UseBackColor = false;
      this.xrTableCell221.StylePriority.UseBorderColor = false;
      this.xrTableCell221.StylePriority.UseBorders = false;
      this.xrTableCell221.StylePriority.UseFont = false;
      this.xrTableCell221.StylePriority.UseForeColor = false;
      this.xrTableCell221.StylePriority.UsePadding = false;
      this.xrTableCell221.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell222
      // 
      resources.ApplyResources(this.xrTableCell222, "xrTableCell222");
      this.xrTableCell222.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell222.Name = "xrTableCell222";
      this.xrTableCell222.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell222.StylePriority.UseBackColor = false;
      this.xrTableCell222.StylePriority.UseBorderColor = false;
      this.xrTableCell222.StylePriority.UseBorders = false;
      this.xrTableCell222.StylePriority.UseFont = false;
      this.xrTableCell222.StylePriority.UseForeColor = false;
      this.xrTableCell222.StylePriority.UsePadding = false;
      this.xrTableCell222.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell223
      // 
      resources.ApplyResources(this.xrTableCell223, "xrTableCell223");
      this.xrTableCell223.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell223.Name = "xrTableCell223";
      this.xrTableCell223.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell223.StylePriority.UseBackColor = false;
      this.xrTableCell223.StylePriority.UseBorderColor = false;
      this.xrTableCell223.StylePriority.UseBorders = false;
      this.xrTableCell223.StylePriority.UseFont = false;
      this.xrTableCell223.StylePriority.UseForeColor = false;
      this.xrTableCell223.StylePriority.UsePadding = false;
      this.xrTableCell223.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell224
      // 
      resources.ApplyResources(this.xrTableCell224, "xrTableCell224");
      this.xrTableCell224.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell224.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerCPANLRDebtSummary.TotalCourtNoticeAmt")});
      this.xrTableCell224.Name = "xrTableCell224";
      this.xrTableCell224.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell224.Scripts.OnBeforePrint = "xrTableCell224_BeforePrint";
      this.xrTableCell224.StylePriority.UseBackColor = false;
      this.xrTableCell224.StylePriority.UseBorderColor = false;
      this.xrTableCell224.StylePriority.UseBorders = false;
      this.xrTableCell224.StylePriority.UseFont = false;
      this.xrTableCell224.StylePriority.UseForeColor = false;
      this.xrTableCell224.StylePriority.UsePadding = false;
      this.xrTableCell224.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow91
      // 
      this.xrTableRow91.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell217,
            this.xrTableCell218,
            this.xrTableCell219,
            this.xrTableCell220});
      resources.ApplyResources(this.xrTableRow91, "xrTableRow91");
      this.xrTableRow91.Name = "xrTableRow91";
      // 
      // xrTableCell217
      // 
      resources.ApplyResources(this.xrTableCell217, "xrTableCell217");
      this.xrTableCell217.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell217.Name = "xrTableCell217";
      this.xrTableCell217.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell217.StylePriority.UseBackColor = false;
      this.xrTableCell217.StylePriority.UseBorderColor = false;
      this.xrTableCell217.StylePriority.UseBorders = false;
      this.xrTableCell217.StylePriority.UseFont = false;
      this.xrTableCell217.StylePriority.UseForeColor = false;
      this.xrTableCell217.StylePriority.UsePadding = false;
      this.xrTableCell217.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell218
      // 
      resources.ApplyResources(this.xrTableCell218, "xrTableCell218");
      this.xrTableCell218.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell218.Name = "xrTableCell218";
      this.xrTableCell218.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell218.StylePriority.UseBackColor = false;
      this.xrTableCell218.StylePriority.UseBorderColor = false;
      this.xrTableCell218.StylePriority.UseBorders = false;
      this.xrTableCell218.StylePriority.UseFont = false;
      this.xrTableCell218.StylePriority.UseForeColor = false;
      this.xrTableCell218.StylePriority.UsePadding = false;
      this.xrTableCell218.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell219
      // 
      resources.ApplyResources(this.xrTableCell219, "xrTableCell219");
      this.xrTableCell219.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell219.Name = "xrTableCell219";
      this.xrTableCell219.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell219.StylePriority.UseBackColor = false;
      this.xrTableCell219.StylePriority.UseBorderColor = false;
      this.xrTableCell219.StylePriority.UseBorders = false;
      this.xrTableCell219.StylePriority.UseFont = false;
      this.xrTableCell219.StylePriority.UseForeColor = false;
      this.xrTableCell219.StylePriority.UsePadding = false;
      this.xrTableCell219.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell220
      // 
      resources.ApplyResources(this.xrTableCell220, "xrTableCell220");
      this.xrTableCell220.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell220.Name = "xrTableCell220";
      this.xrTableCell220.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell220.StylePriority.UseBackColor = false;
      this.xrTableCell220.StylePriority.UseBorderColor = false;
      this.xrTableCell220.StylePriority.UseBorders = false;
      this.xrTableCell220.StylePriority.UseFont = false;
      this.xrTableCell220.StylePriority.UseForeColor = false;
      this.xrTableCell220.StylePriority.UsePadding = false;
      this.xrTableCell220.StylePriority.UseTextAlignment = false;
      // 
      // DetailReport12
      // 
      this.DetailReport12.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail8,
            this.GroupHeader17});
      this.DetailReport12.DataMember = "ConsumerAccountStatus";
      resources.ApplyResources(this.DetailReport12, "DetailReport12");
      this.DetailReport12.FilterString = "[AccountType] In (\'S\', \'I\') And [StatusCodeDesc] = \'Active\'";
      this.DetailReport12.Level = 0;
      this.DetailReport12.Name = "DetailReport12";
      // 
      // Detail8
      // 
      this.Detail8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable16});
      resources.ApplyResources(this.Detail8, "Detail8");
      this.Detail8.Name = "Detail8";
      // 
      // xrTable16
      // 
      resources.ApplyResources(this.xrTable16, "xrTable16");
      this.xrTable16.Name = "xrTable16";
      this.xrTable16.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable16.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow64});
      this.xrTable16.StylePriority.UseBackColor = false;
      this.xrTable16.StylePriority.UseFont = false;
      this.xrTable16.StylePriority.UsePadding = false;
      // 
      // xrTableRow64
      // 
      resources.ApplyResources(this.xrTableRow64, "xrTableRow64");
      this.xrTableRow64.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableRow64.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell137,
            this.xrTableCell140,
            this.xrTableCell142,
            this.xrTableCell143});
      this.xrTableRow64.Name = "xrTableRow64";
      this.xrTableRow64.StylePriority.UseBackColor = false;
      this.xrTableRow64.StylePriority.UseBorderColor = false;
      this.xrTableRow64.StylePriority.UseBorders = false;
      this.xrTableRow64.StylePriority.UseFont = false;
      this.xrTableRow64.StylePriority.UseForeColor = false;
      this.xrTableRow64.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell137
      // 
      resources.ApplyResources(this.xrTableCell137, "xrTableCell137");
      this.xrTableCell137.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountStatus.AccountOpenedDate")});
      this.xrTableCell137.Name = "xrTableCell137";
      this.xrTableCell137.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell137.StylePriority.UseBackColor = false;
      this.xrTableCell137.StylePriority.UseBorderColor = false;
      this.xrTableCell137.StylePriority.UseFont = false;
      this.xrTableCell137.StylePriority.UseForeColor = false;
      this.xrTableCell137.StylePriority.UsePadding = false;
      this.xrTableCell137.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell140
      // 
      resources.ApplyResources(this.xrTableCell140, "xrTableCell140");
      this.xrTableCell140.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountStatus.SubscriberName")});
      this.xrTableCell140.Name = "xrTableCell140";
      this.xrTableCell140.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell140.StylePriority.UseBackColor = false;
      this.xrTableCell140.StylePriority.UseBorderColor = false;
      this.xrTableCell140.StylePriority.UseFont = false;
      this.xrTableCell140.StylePriority.UseForeColor = false;
      this.xrTableCell140.StylePriority.UsePadding = false;
      this.xrTableCell140.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell142
      // 
      resources.ApplyResources(this.xrTableCell142, "xrTableCell142");
      this.xrTableCell142.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountStatus.CreditLimitAmt")});
      this.xrTableCell142.Name = "xrTableCell142";
      this.xrTableCell142.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell142.StylePriority.UseBackColor = false;
      this.xrTableCell142.StylePriority.UseBorderColor = false;
      this.xrTableCell142.StylePriority.UseFont = false;
      this.xrTableCell142.StylePriority.UseForeColor = false;
      this.xrTableCell142.StylePriority.UsePadding = false;
      this.xrTableCell142.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell143
      // 
      resources.ApplyResources(this.xrTableCell143, "xrTableCell143");
      this.xrTableCell143.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountStatus.MonthlyInstalmentAmt", "{0:c2}")});
      this.xrTableCell143.Name = "xrTableCell143";
      this.xrTableCell143.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell143.StylePriority.UseBackColor = false;
      this.xrTableCell143.StylePriority.UseBorderColor = false;
      this.xrTableCell143.StylePriority.UseFont = false;
      this.xrTableCell143.StylePriority.UseForeColor = false;
      this.xrTableCell143.StylePriority.UsePadding = false;
      this.xrTableCell143.StylePriority.UseTextAlignment = false;
      // 
      // GroupHeader17
      // 
      this.GroupHeader17.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable27});
      resources.ApplyResources(this.GroupHeader17, "GroupHeader17");
      this.GroupHeader17.Name = "GroupHeader17";
      // 
      // xrTable27
      // 
      this.xrTable27.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
      resources.ApplyResources(this.xrTable27, "xrTable27");
      this.xrTable27.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTable27.Name = "xrTable27";
      this.xrTable27.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable27.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow51,
            this.xrTableRow62});
      this.xrTable27.StylePriority.UseBorderColor = false;
      this.xrTable27.StylePriority.UseBorders = false;
      this.xrTable27.StylePriority.UsePadding = false;
      // 
      // xrTableRow51
      // 
      this.xrTableRow51.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell128});
      resources.ApplyResources(this.xrTableRow51, "xrTableRow51");
      this.xrTableRow51.Name = "xrTableRow51";
      // 
      // xrTableCell128
      // 
      resources.ApplyResources(this.xrTableCell128, "xrTableCell128");
      this.xrTableCell128.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell128.CanGrow = false;
      this.xrTableCell128.Name = "xrTableCell128";
      this.xrTableCell128.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell128.StylePriority.UseBackColor = false;
      this.xrTableCell128.StylePriority.UseBorderColor = false;
      this.xrTableCell128.StylePriority.UseBorders = false;
      this.xrTableCell128.StylePriority.UseFont = false;
      this.xrTableCell128.StylePriority.UseForeColor = false;
      this.xrTableCell128.StylePriority.UsePadding = false;
      this.xrTableCell128.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow62
      // 
      resources.ApplyResources(this.xrTableRow62, "xrTableRow62");
      this.xrTableRow62.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableRow62.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell131,
            this.xrTableCell136,
            this.xrTableCell138,
            this.xrTableCell139});
      this.xrTableRow62.Name = "xrTableRow62";
      this.xrTableRow62.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTableRow62.StylePriority.UseBackColor = false;
      this.xrTableRow62.StylePriority.UseBorderColor = false;
      this.xrTableRow62.StylePriority.UseBorders = false;
      this.xrTableRow62.StylePriority.UseFont = false;
      this.xrTableRow62.StylePriority.UseForeColor = false;
      this.xrTableRow62.StylePriority.UsePadding = false;
      this.xrTableRow62.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell131
      // 
      this.xrTableCell131.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell131.CanGrow = false;
      resources.ApplyResources(this.xrTableCell131, "xrTableCell131");
      this.xrTableCell131.Name = "xrTableCell131";
      this.xrTableCell131.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell131.StylePriority.UseBorders = false;
      this.xrTableCell131.StylePriority.UseFont = false;
      this.xrTableCell131.StylePriority.UseForeColor = false;
      this.xrTableCell131.StylePriority.UsePadding = false;
      // 
      // xrTableCell136
      // 
      this.xrTableCell136.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell136.CanGrow = false;
      resources.ApplyResources(this.xrTableCell136, "xrTableCell136");
      this.xrTableCell136.Name = "xrTableCell136";
      this.xrTableCell136.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell136.StylePriority.UseBorders = false;
      this.xrTableCell136.StylePriority.UseFont = false;
      this.xrTableCell136.StylePriority.UseForeColor = false;
      this.xrTableCell136.StylePriority.UsePadding = false;
      // 
      // xrTableCell138
      // 
      this.xrTableCell138.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell138.CanGrow = false;
      resources.ApplyResources(this.xrTableCell138, "xrTableCell138");
      this.xrTableCell138.Name = "xrTableCell138";
      this.xrTableCell138.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell138.StylePriority.UseBorders = false;
      this.xrTableCell138.StylePriority.UseFont = false;
      this.xrTableCell138.StylePriority.UseForeColor = false;
      this.xrTableCell138.StylePriority.UsePadding = false;
      // 
      // xrTableCell139
      // 
      this.xrTableCell139.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell139.CanGrow = false;
      resources.ApplyResources(this.xrTableCell139, "xrTableCell139");
      this.xrTableCell139.Name = "xrTableCell139";
      this.xrTableCell139.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell139.StylePriority.UseBorders = false;
      this.xrTableCell139.StylePriority.UseFont = false;
      this.xrTableCell139.StylePriority.UseForeColor = false;
      this.xrTableCell139.StylePriority.UsePadding = false;
      // 
      // CreditAccountInformation_CAS
      // 
      this.CreditAccountInformation_CAS.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail13,
            this.DetailReport2,
            this.DetailReport1,
            this.DetailReport3,
            this.DetailReport4,
            this.DetailReport10});
      resources.ApplyResources(this.CreditAccountInformation_CAS, "CreditAccountInformation_CAS");
      this.CreditAccountInformation_CAS.Level = 4;
      this.CreditAccountInformation_CAS.Name = "CreditAccountInformation_CAS";
      this.CreditAccountInformation_CAS.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand;
      // 
      // Detail13
      // 
      resources.ApplyResources(this.Detail13, "Detail13");
      this.Detail13.Name = "Detail13";
      // 
      // DetailReport2
      // 
      this.DetailReport2.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail18});
      this.DetailReport2.DataMember = "AccountTypeLegend";
      resources.ApplyResources(this.DetailReport2, "DetailReport2");
      this.DetailReport2.Level = 1;
      this.DetailReport2.Name = "DetailReport2";
      // 
      // Detail18
      // 
      this.Detail18.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable23});
      resources.ApplyResources(this.Detail18, "Detail18");
      this.Detail18.MultiColumn.ColumnCount = 6;
      this.Detail18.MultiColumn.Layout = DevExpress.XtraPrinting.ColumnLayout.AcrossThenDown;
      this.Detail18.MultiColumn.Mode = DevExpress.XtraReports.UI.MultiColumnMode.UseColumnCount;
      this.Detail18.Name = "Detail18";
      this.Detail18.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      this.Detail18.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("AccountTypeCode", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
      this.Detail18.StylePriority.UsePadding = false;
      // 
      // xrTable23
      // 
      resources.ApplyResources(this.xrTable23, "xrTable23");
      this.xrTable23.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTable23.Name = "xrTable23";
      this.xrTable23.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow112});
      this.xrTable23.StylePriority.UseBackColor = false;
      this.xrTable23.StylePriority.UseBorderColor = false;
      this.xrTable23.StylePriority.UseBorders = false;
      this.xrTable23.StylePriority.UseFont = false;
      // 
      // xrTableRow112
      // 
      this.xrTableRow112.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableRow112.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell291});
      resources.ApplyResources(this.xrTableRow112, "xrTableRow112");
      this.xrTableRow112.Name = "xrTableRow112";
      this.xrTableRow112.StylePriority.UseBorders = false;
      // 
      // xrTableCell291
      // 
      resources.ApplyResources(this.xrTableCell291, "xrTableCell291");
      this.xrTableCell291.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTableCell291.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "AccountTypeLegend.AccountTypeDesc")});
      this.xrTableCell291.Name = "xrTableCell291";
      this.xrTableCell291.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTableCell291.StylePriority.UseBackColor = false;
      this.xrTableCell291.StylePriority.UseBorderColor = false;
      this.xrTableCell291.StylePriority.UseBorders = false;
      this.xrTableCell291.StylePriority.UseFont = false;
      this.xrTableCell291.StylePriority.UseForeColor = false;
      this.xrTableCell291.StylePriority.UsePadding = false;
      this.xrTableCell291.StylePriority.UseTextAlignment = false;
      // 
      // DetailReport1
      // 
      this.DetailReport1.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail17,
            this.GroupHeader9});
      this.DetailReport1.DataMember = "Consumer24MonthlyPaymentHeader";
      resources.ApplyResources(this.DetailReport1, "DetailReport1");
      this.DetailReport1.Level = 2;
      this.DetailReport1.Name = "DetailReport1";
      // 
      // Detail17
      // 
      resources.ApplyResources(this.Detail17, "Detail17");
      this.Detail17.Name = "Detail17";
      // 
      // GroupHeader9
      // 
      this.GroupHeader9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblMonthlyPaymentH});
      resources.ApplyResources(this.GroupHeader9, "GroupHeader9");
      this.GroupHeader9.Name = "GroupHeader9";
      // 
      // tblMonthlyPaymentH
      // 
      resources.ApplyResources(this.tblMonthlyPaymentH, "tblMonthlyPaymentH");
      this.tblMonthlyPaymentH.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.tblMonthlyPaymentH.Name = "tblMonthlyPaymentH";
      this.tblMonthlyPaymentH.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.tblMonthlyPaymentH.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow128});
      this.tblMonthlyPaymentH.StylePriority.UseBorderColor = false;
      this.tblMonthlyPaymentH.StylePriority.UseBorders = false;
      this.tblMonthlyPaymentH.StylePriority.UseFont = false;
      this.tblMonthlyPaymentH.StylePriority.UsePadding = false;
      this.tblMonthlyPaymentH.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow128
      // 
      resources.ApplyResources(this.xrTableRow128, "xrTableRow128");
      this.xrTableRow128.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableRow128.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblcompanyMP,
            this.lblM01,
            this.lblM02,
            this.lblM03,
            this.lblM04,
            this.lblM05,
            this.lblM06,
            this.xrTableCell259,
            this.lblM08,
            this.lblM09,
            this.lblM10,
            this.lblM11,
            this.lblM12,
            this.lblM13,
            this.lblM14,
            this.lblM15,
            this.lblM16,
            this.lblM17,
            this.lblM18,
            this.lblM19,
            this.lblM20,
            this.lblM21,
            this.lblM22,
            this.lblM23,
            this.lblM24});
      this.xrTableRow128.Name = "xrTableRow128";
      this.xrTableRow128.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
      this.xrTableRow128.StylePriority.UseBackColor = false;
      this.xrTableRow128.StylePriority.UseBorderColor = false;
      this.xrTableRow128.StylePriority.UseBorders = false;
      this.xrTableRow128.StylePriority.UseFont = false;
      this.xrTableRow128.StylePriority.UseForeColor = false;
      this.xrTableRow128.StylePriority.UsePadding = false;
      this.xrTableRow128.StylePriority.UseTextAlignment = false;
      // 
      // lblcompanyMP
      // 
      resources.ApplyResources(this.lblcompanyMP, "lblcompanyMP");
      this.lblcompanyMP.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.lblcompanyMP.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.Company")});
      this.lblcompanyMP.Name = "lblcompanyMP";
      this.lblcompanyMP.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
      this.lblcompanyMP.StylePriority.UseBackColor = false;
      this.lblcompanyMP.StylePriority.UseBorderColor = false;
      this.lblcompanyMP.StylePriority.UseBorders = false;
      this.lblcompanyMP.StylePriority.UseFont = false;
      this.lblcompanyMP.StylePriority.UseForeColor = false;
      this.lblcompanyMP.StylePriority.UsePadding = false;
      this.lblcompanyMP.StylePriority.UseTextAlignment = false;
      // 
      // lblM01
      // 
      resources.ApplyResources(this.lblM01, "lblM01");
      this.lblM01.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.lblM01.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M01")});
      this.lblM01.Name = "lblM01";
      this.lblM01.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.lblM01.StylePriority.UseBackColor = false;
      this.lblM01.StylePriority.UseBorderColor = false;
      this.lblM01.StylePriority.UseBorders = false;
      this.lblM01.StylePriority.UseFont = false;
      this.lblM01.StylePriority.UseForeColor = false;
      this.lblM01.StylePriority.UsePadding = false;
      this.lblM01.StylePriority.UseTextAlignment = false;
      // 
      // lblM02
      // 
      resources.ApplyResources(this.lblM02, "lblM02");
      this.lblM02.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.lblM02.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M02")});
      this.lblM02.Name = "lblM02";
      this.lblM02.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.lblM02.StylePriority.UseBackColor = false;
      this.lblM02.StylePriority.UseBorderColor = false;
      this.lblM02.StylePriority.UseBorders = false;
      this.lblM02.StylePriority.UseFont = false;
      this.lblM02.StylePriority.UseForeColor = false;
      this.lblM02.StylePriority.UsePadding = false;
      this.lblM02.StylePriority.UseTextAlignment = false;
      // 
      // lblM03
      // 
      resources.ApplyResources(this.lblM03, "lblM03");
      this.lblM03.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.lblM03.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M03")});
      this.lblM03.Name = "lblM03";
      this.lblM03.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.lblM03.StylePriority.UseBackColor = false;
      this.lblM03.StylePriority.UseBorderColor = false;
      this.lblM03.StylePriority.UseBorders = false;
      this.lblM03.StylePriority.UseFont = false;
      this.lblM03.StylePriority.UseForeColor = false;
      this.lblM03.StylePriority.UsePadding = false;
      this.lblM03.StylePriority.UseTextAlignment = false;
      // 
      // lblM04
      // 
      resources.ApplyResources(this.lblM04, "lblM04");
      this.lblM04.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.lblM04.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M04")});
      this.lblM04.Name = "lblM04";
      this.lblM04.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.lblM04.StylePriority.UseBackColor = false;
      this.lblM04.StylePriority.UseBorderColor = false;
      this.lblM04.StylePriority.UseBorders = false;
      this.lblM04.StylePriority.UseFont = false;
      this.lblM04.StylePriority.UseForeColor = false;
      this.lblM04.StylePriority.UsePadding = false;
      this.lblM04.StylePriority.UseTextAlignment = false;
      // 
      // lblM05
      // 
      resources.ApplyResources(this.lblM05, "lblM05");
      this.lblM05.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.lblM05.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M05")});
      this.lblM05.Name = "lblM05";
      this.lblM05.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.lblM05.StylePriority.UseBackColor = false;
      this.lblM05.StylePriority.UseBorderColor = false;
      this.lblM05.StylePriority.UseBorders = false;
      this.lblM05.StylePriority.UseFont = false;
      this.lblM05.StylePriority.UseForeColor = false;
      this.lblM05.StylePriority.UsePadding = false;
      this.lblM05.StylePriority.UseTextAlignment = false;
      // 
      // lblM06
      // 
      resources.ApplyResources(this.lblM06, "lblM06");
      this.lblM06.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.lblM06.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M06")});
      this.lblM06.Name = "lblM06";
      this.lblM06.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.lblM06.StylePriority.UseBackColor = false;
      this.lblM06.StylePriority.UseBorderColor = false;
      this.lblM06.StylePriority.UseBorders = false;
      this.lblM06.StylePriority.UseFont = false;
      this.lblM06.StylePriority.UseForeColor = false;
      this.lblM06.StylePriority.UsePadding = false;
      this.lblM06.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell259
      // 
      resources.ApplyResources(this.xrTableCell259, "xrTableCell259");
      this.xrTableCell259.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell259.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M07")});
      this.xrTableCell259.Name = "xrTableCell259";
      this.xrTableCell259.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell259.StylePriority.UseBackColor = false;
      this.xrTableCell259.StylePriority.UseBorderColor = false;
      this.xrTableCell259.StylePriority.UseBorders = false;
      this.xrTableCell259.StylePriority.UseFont = false;
      this.xrTableCell259.StylePriority.UseForeColor = false;
      this.xrTableCell259.StylePriority.UsePadding = false;
      this.xrTableCell259.StylePriority.UseTextAlignment = false;
      // 
      // lblM08
      // 
      resources.ApplyResources(this.lblM08, "lblM08");
      this.lblM08.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.lblM08.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M08")});
      this.lblM08.Name = "lblM08";
      this.lblM08.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.lblM08.StylePriority.UseBackColor = false;
      this.lblM08.StylePriority.UseBorderColor = false;
      this.lblM08.StylePriority.UseBorders = false;
      this.lblM08.StylePriority.UseFont = false;
      this.lblM08.StylePriority.UseForeColor = false;
      this.lblM08.StylePriority.UsePadding = false;
      this.lblM08.StylePriority.UseTextAlignment = false;
      // 
      // lblM09
      // 
      resources.ApplyResources(this.lblM09, "lblM09");
      this.lblM09.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.lblM09.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M09")});
      this.lblM09.Name = "lblM09";
      this.lblM09.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.lblM09.StylePriority.UseBackColor = false;
      this.lblM09.StylePriority.UseBorderColor = false;
      this.lblM09.StylePriority.UseBorders = false;
      this.lblM09.StylePriority.UseFont = false;
      this.lblM09.StylePriority.UseForeColor = false;
      this.lblM09.StylePriority.UsePadding = false;
      this.lblM09.StylePriority.UseTextAlignment = false;
      // 
      // lblM10
      // 
      resources.ApplyResources(this.lblM10, "lblM10");
      this.lblM10.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.lblM10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M10")});
      this.lblM10.Name = "lblM10";
      this.lblM10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.lblM10.StylePriority.UseBackColor = false;
      this.lblM10.StylePriority.UseBorderColor = false;
      this.lblM10.StylePriority.UseBorders = false;
      this.lblM10.StylePriority.UseFont = false;
      this.lblM10.StylePriority.UseForeColor = false;
      this.lblM10.StylePriority.UsePadding = false;
      this.lblM10.StylePriority.UseTextAlignment = false;
      // 
      // lblM11
      // 
      resources.ApplyResources(this.lblM11, "lblM11");
      this.lblM11.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.lblM11.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M11")});
      this.lblM11.Name = "lblM11";
      this.lblM11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.lblM11.StylePriority.UseBackColor = false;
      this.lblM11.StylePriority.UseBorderColor = false;
      this.lblM11.StylePriority.UseBorders = false;
      this.lblM11.StylePriority.UseFont = false;
      this.lblM11.StylePriority.UseForeColor = false;
      this.lblM11.StylePriority.UsePadding = false;
      this.lblM11.StylePriority.UseTextAlignment = false;
      // 
      // lblM12
      // 
      resources.ApplyResources(this.lblM12, "lblM12");
      this.lblM12.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.lblM12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M12")});
      this.lblM12.Name = "lblM12";
      this.lblM12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.lblM12.StylePriority.UseBackColor = false;
      this.lblM12.StylePriority.UseBorderColor = false;
      this.lblM12.StylePriority.UseBorders = false;
      this.lblM12.StylePriority.UseFont = false;
      this.lblM12.StylePriority.UseForeColor = false;
      this.lblM12.StylePriority.UsePadding = false;
      this.lblM12.StylePriority.UseTextAlignment = false;
      // 
      // lblM13
      // 
      resources.ApplyResources(this.lblM13, "lblM13");
      this.lblM13.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.lblM13.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M13")});
      this.lblM13.Name = "lblM13";
      this.lblM13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.lblM13.StylePriority.UseBackColor = false;
      this.lblM13.StylePriority.UseBorderColor = false;
      this.lblM13.StylePriority.UseBorders = false;
      this.lblM13.StylePriority.UseFont = false;
      this.lblM13.StylePriority.UseForeColor = false;
      this.lblM13.StylePriority.UsePadding = false;
      this.lblM13.StylePriority.UseTextAlignment = false;
      // 
      // lblM14
      // 
      resources.ApplyResources(this.lblM14, "lblM14");
      this.lblM14.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.lblM14.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M14")});
      this.lblM14.Name = "lblM14";
      this.lblM14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.lblM14.StylePriority.UseBackColor = false;
      this.lblM14.StylePriority.UseBorderColor = false;
      this.lblM14.StylePriority.UseBorders = false;
      this.lblM14.StylePriority.UseFont = false;
      this.lblM14.StylePriority.UseForeColor = false;
      this.lblM14.StylePriority.UsePadding = false;
      this.lblM14.StylePriority.UseTextAlignment = false;
      // 
      // lblM15
      // 
      resources.ApplyResources(this.lblM15, "lblM15");
      this.lblM15.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.lblM15.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M15")});
      this.lblM15.Name = "lblM15";
      this.lblM15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.lblM15.StylePriority.UseBackColor = false;
      this.lblM15.StylePriority.UseBorderColor = false;
      this.lblM15.StylePriority.UseBorders = false;
      this.lblM15.StylePriority.UseFont = false;
      this.lblM15.StylePriority.UseForeColor = false;
      this.lblM15.StylePriority.UsePadding = false;
      this.lblM15.StylePriority.UseTextAlignment = false;
      // 
      // lblM16
      // 
      resources.ApplyResources(this.lblM16, "lblM16");
      this.lblM16.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.lblM16.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M16")});
      this.lblM16.Name = "lblM16";
      this.lblM16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.lblM16.StylePriority.UseBackColor = false;
      this.lblM16.StylePriority.UseBorderColor = false;
      this.lblM16.StylePriority.UseBorders = false;
      this.lblM16.StylePriority.UseFont = false;
      this.lblM16.StylePriority.UseForeColor = false;
      this.lblM16.StylePriority.UsePadding = false;
      this.lblM16.StylePriority.UseTextAlignment = false;
      // 
      // lblM17
      // 
      resources.ApplyResources(this.lblM17, "lblM17");
      this.lblM17.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.lblM17.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M17")});
      this.lblM17.Name = "lblM17";
      this.lblM17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.lblM17.StylePriority.UseBackColor = false;
      this.lblM17.StylePriority.UseBorderColor = false;
      this.lblM17.StylePriority.UseBorders = false;
      this.lblM17.StylePriority.UseFont = false;
      this.lblM17.StylePriority.UseForeColor = false;
      this.lblM17.StylePriority.UsePadding = false;
      this.lblM17.StylePriority.UseTextAlignment = false;
      // 
      // lblM18
      // 
      resources.ApplyResources(this.lblM18, "lblM18");
      this.lblM18.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.lblM18.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M18")});
      this.lblM18.Name = "lblM18";
      this.lblM18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.lblM18.StylePriority.UseBackColor = false;
      this.lblM18.StylePriority.UseBorderColor = false;
      this.lblM18.StylePriority.UseBorders = false;
      this.lblM18.StylePriority.UseFont = false;
      this.lblM18.StylePriority.UseForeColor = false;
      this.lblM18.StylePriority.UsePadding = false;
      this.lblM18.StylePriority.UseTextAlignment = false;
      // 
      // lblM19
      // 
      resources.ApplyResources(this.lblM19, "lblM19");
      this.lblM19.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.lblM19.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M19")});
      this.lblM19.Name = "lblM19";
      this.lblM19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.lblM19.StylePriority.UseBackColor = false;
      this.lblM19.StylePriority.UseBorderColor = false;
      this.lblM19.StylePriority.UseBorders = false;
      this.lblM19.StylePriority.UseFont = false;
      this.lblM19.StylePriority.UseForeColor = false;
      this.lblM19.StylePriority.UsePadding = false;
      this.lblM19.StylePriority.UseTextAlignment = false;
      // 
      // lblM20
      // 
      resources.ApplyResources(this.lblM20, "lblM20");
      this.lblM20.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.lblM20.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M20")});
      this.lblM20.Name = "lblM20";
      this.lblM20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.lblM20.StylePriority.UseBackColor = false;
      this.lblM20.StylePriority.UseBorderColor = false;
      this.lblM20.StylePriority.UseBorders = false;
      this.lblM20.StylePriority.UseFont = false;
      this.lblM20.StylePriority.UseForeColor = false;
      this.lblM20.StylePriority.UsePadding = false;
      this.lblM20.StylePriority.UseTextAlignment = false;
      // 
      // lblM21
      // 
      resources.ApplyResources(this.lblM21, "lblM21");
      this.lblM21.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.lblM21.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M21")});
      this.lblM21.Name = "lblM21";
      this.lblM21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.lblM21.StylePriority.UseBackColor = false;
      this.lblM21.StylePriority.UseBorderColor = false;
      this.lblM21.StylePriority.UseBorders = false;
      this.lblM21.StylePriority.UseFont = false;
      this.lblM21.StylePriority.UseForeColor = false;
      this.lblM21.StylePriority.UsePadding = false;
      this.lblM21.StylePriority.UseTextAlignment = false;
      // 
      // lblM22
      // 
      resources.ApplyResources(this.lblM22, "lblM22");
      this.lblM22.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.lblM22.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M22")});
      this.lblM22.Name = "lblM22";
      this.lblM22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.lblM22.StylePriority.UseBackColor = false;
      this.lblM22.StylePriority.UseBorderColor = false;
      this.lblM22.StylePriority.UseBorders = false;
      this.lblM22.StylePriority.UseFont = false;
      this.lblM22.StylePriority.UseForeColor = false;
      this.lblM22.StylePriority.UsePadding = false;
      this.lblM22.StylePriority.UseTextAlignment = false;
      // 
      // lblM23
      // 
      resources.ApplyResources(this.lblM23, "lblM23");
      this.lblM23.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.lblM23.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M23")});
      this.lblM23.Name = "lblM23";
      this.lblM23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.lblM23.StylePriority.UseBackColor = false;
      this.lblM23.StylePriority.UseBorderColor = false;
      this.lblM23.StylePriority.UseBorders = false;
      this.lblM23.StylePriority.UseFont = false;
      this.lblM23.StylePriority.UseForeColor = false;
      this.lblM23.StylePriority.UsePadding = false;
      this.lblM23.StylePriority.UseTextAlignment = false;
      // 
      // lblM24
      // 
      resources.ApplyResources(this.lblM24, "lblM24");
      this.lblM24.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.lblM24.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPaymentHeader.M24")});
      this.lblM24.Name = "lblM24";
      this.lblM24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.lblM24.StylePriority.UseBackColor = false;
      this.lblM24.StylePriority.UseBorderColor = false;
      this.lblM24.StylePriority.UseBorders = false;
      this.lblM24.StylePriority.UseFont = false;
      this.lblM24.StylePriority.UseForeColor = false;
      this.lblM24.StylePriority.UsePadding = false;
      this.lblM24.StylePriority.UseTextAlignment = false;
      // 
      // DetailReport3
      // 
      this.DetailReport3.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail19});
      this.DetailReport3.DataMember = "Consumer24MonthlyPayment";
      resources.ApplyResources(this.DetailReport3, "DetailReport3");
      this.DetailReport3.Level = 3;
      this.DetailReport3.Name = "DetailReport3";
      // 
      // Detail19
      // 
      this.Detail19.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable24});
      resources.ApplyResources(this.Detail19, "Detail19");
      this.Detail19.Name = "Detail19";
      // 
      // xrTable24
      // 
      resources.ApplyResources(this.xrTable24, "xrTable24");
      this.xrTable24.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTable24.Name = "xrTable24";
      this.xrTable24.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable24.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow134});
      this.xrTable24.StylePriority.UseBorderColor = false;
      this.xrTable24.StylePriority.UseBorders = false;
      this.xrTable24.StylePriority.UseFont = false;
      this.xrTable24.StylePriority.UsePadding = false;
      this.xrTable24.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow134
      // 
      resources.ApplyResources(this.xrTableRow134, "xrTableRow134");
      this.xrTableRow134.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableRow134.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell290,
            this.xrTableCell292,
            this.xrTableCell293,
            this.xrTableCell307,
            this.xrTableCell308,
            this.xrTableCell309,
            this.xrTableCell310,
            this.xrTableCell311,
            this.xrTableCell312,
            this.xrTableCell313,
            this.xrTableCell314,
            this.xrTableCell318,
            this.xrTableCell319,
            this.xrTableCell320,
            this.xrTableCell321,
            this.xrTableCell322,
            this.xrTableCell323,
            this.xrTableCell324,
            this.xrTableCell325,
            this.xrTableCell326,
            this.xrTableCell327,
            this.xrTableCell328,
            this.xrTableCell329,
            this.xrTableCell331,
            this.xrTableCell332});
      this.xrTableRow134.Name = "xrTableRow134";
      this.xrTableRow134.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
      this.xrTableRow134.StylePriority.UseBackColor = false;
      this.xrTableRow134.StylePriority.UseBorderColor = false;
      this.xrTableRow134.StylePriority.UseBorders = false;
      this.xrTableRow134.StylePriority.UseFont = false;
      this.xrTableRow134.StylePriority.UseForeColor = false;
      this.xrTableRow134.StylePriority.UsePadding = false;
      this.xrTableRow134.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell290
      // 
      resources.ApplyResources(this.xrTableCell290, "xrTableCell290");
      this.xrTableCell290.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.SubscriberName")});
      this.xrTableCell290.Name = "xrTableCell290";
      this.xrTableCell290.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell290.StylePriority.UseBackColor = false;
      this.xrTableCell290.StylePriority.UseBorderColor = false;
      this.xrTableCell290.StylePriority.UseFont = false;
      this.xrTableCell290.StylePriority.UseForeColor = false;
      this.xrTableCell290.StylePriority.UsePadding = false;
      this.xrTableCell290.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell292
      // 
      resources.ApplyResources(this.xrTableCell292, "xrTableCell292");
      this.xrTableCell292.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M01")});
      this.xrTableCell292.Name = "xrTableCell292";
      this.xrTableCell292.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell292.StylePriority.UseBackColor = false;
      this.xrTableCell292.StylePriority.UseBorderColor = false;
      this.xrTableCell292.StylePriority.UseFont = false;
      this.xrTableCell292.StylePriority.UseForeColor = false;
      this.xrTableCell292.StylePriority.UsePadding = false;
      this.xrTableCell292.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell293
      // 
      resources.ApplyResources(this.xrTableCell293, "xrTableCell293");
      this.xrTableCell293.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M02")});
      this.xrTableCell293.Name = "xrTableCell293";
      this.xrTableCell293.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell293.StylePriority.UseBackColor = false;
      this.xrTableCell293.StylePriority.UseBorderColor = false;
      this.xrTableCell293.StylePriority.UseFont = false;
      this.xrTableCell293.StylePriority.UseForeColor = false;
      this.xrTableCell293.StylePriority.UsePadding = false;
      this.xrTableCell293.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell307
      // 
      resources.ApplyResources(this.xrTableCell307, "xrTableCell307");
      this.xrTableCell307.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M03")});
      this.xrTableCell307.Name = "xrTableCell307";
      this.xrTableCell307.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell307.StylePriority.UseBackColor = false;
      this.xrTableCell307.StylePriority.UseBorderColor = false;
      this.xrTableCell307.StylePriority.UseFont = false;
      this.xrTableCell307.StylePriority.UseForeColor = false;
      this.xrTableCell307.StylePriority.UsePadding = false;
      this.xrTableCell307.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell308
      // 
      resources.ApplyResources(this.xrTableCell308, "xrTableCell308");
      this.xrTableCell308.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M04")});
      this.xrTableCell308.Name = "xrTableCell308";
      this.xrTableCell308.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell308.StylePriority.UseBackColor = false;
      this.xrTableCell308.StylePriority.UseBorderColor = false;
      this.xrTableCell308.StylePriority.UseFont = false;
      this.xrTableCell308.StylePriority.UseForeColor = false;
      this.xrTableCell308.StylePriority.UsePadding = false;
      this.xrTableCell308.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell309
      // 
      resources.ApplyResources(this.xrTableCell309, "xrTableCell309");
      this.xrTableCell309.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M05")});
      this.xrTableCell309.Name = "xrTableCell309";
      this.xrTableCell309.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell309.StylePriority.UseBackColor = false;
      this.xrTableCell309.StylePriority.UseBorderColor = false;
      this.xrTableCell309.StylePriority.UseFont = false;
      this.xrTableCell309.StylePriority.UseForeColor = false;
      this.xrTableCell309.StylePriority.UsePadding = false;
      this.xrTableCell309.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell310
      // 
      resources.ApplyResources(this.xrTableCell310, "xrTableCell310");
      this.xrTableCell310.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M06")});
      this.xrTableCell310.Name = "xrTableCell310";
      this.xrTableCell310.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell310.StylePriority.UseBackColor = false;
      this.xrTableCell310.StylePriority.UseBorderColor = false;
      this.xrTableCell310.StylePriority.UseFont = false;
      this.xrTableCell310.StylePriority.UseForeColor = false;
      this.xrTableCell310.StylePriority.UsePadding = false;
      this.xrTableCell310.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell311
      // 
      resources.ApplyResources(this.xrTableCell311, "xrTableCell311");
      this.xrTableCell311.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M07")});
      this.xrTableCell311.Name = "xrTableCell311";
      this.xrTableCell311.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell311.StylePriority.UseBackColor = false;
      this.xrTableCell311.StylePriority.UseBorderColor = false;
      this.xrTableCell311.StylePriority.UseFont = false;
      this.xrTableCell311.StylePriority.UseForeColor = false;
      this.xrTableCell311.StylePriority.UsePadding = false;
      this.xrTableCell311.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell312
      // 
      resources.ApplyResources(this.xrTableCell312, "xrTableCell312");
      this.xrTableCell312.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M08")});
      this.xrTableCell312.Name = "xrTableCell312";
      this.xrTableCell312.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell312.StylePriority.UseBackColor = false;
      this.xrTableCell312.StylePriority.UseBorderColor = false;
      this.xrTableCell312.StylePriority.UseFont = false;
      this.xrTableCell312.StylePriority.UseForeColor = false;
      this.xrTableCell312.StylePriority.UsePadding = false;
      this.xrTableCell312.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell313
      // 
      resources.ApplyResources(this.xrTableCell313, "xrTableCell313");
      this.xrTableCell313.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M09")});
      this.xrTableCell313.Name = "xrTableCell313";
      this.xrTableCell313.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell313.StylePriority.UseBackColor = false;
      this.xrTableCell313.StylePriority.UseBorderColor = false;
      this.xrTableCell313.StylePriority.UseFont = false;
      this.xrTableCell313.StylePriority.UseForeColor = false;
      this.xrTableCell313.StylePriority.UsePadding = false;
      this.xrTableCell313.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell314
      // 
      resources.ApplyResources(this.xrTableCell314, "xrTableCell314");
      this.xrTableCell314.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M10")});
      this.xrTableCell314.Name = "xrTableCell314";
      this.xrTableCell314.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell314.StylePriority.UseBackColor = false;
      this.xrTableCell314.StylePriority.UseBorderColor = false;
      this.xrTableCell314.StylePriority.UseFont = false;
      this.xrTableCell314.StylePriority.UseForeColor = false;
      this.xrTableCell314.StylePriority.UsePadding = false;
      this.xrTableCell314.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell318
      // 
      resources.ApplyResources(this.xrTableCell318, "xrTableCell318");
      this.xrTableCell318.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M11")});
      this.xrTableCell318.Name = "xrTableCell318";
      this.xrTableCell318.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell318.StylePriority.UseBackColor = false;
      this.xrTableCell318.StylePriority.UseBorderColor = false;
      this.xrTableCell318.StylePriority.UseFont = false;
      this.xrTableCell318.StylePriority.UseForeColor = false;
      this.xrTableCell318.StylePriority.UsePadding = false;
      this.xrTableCell318.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell319
      // 
      resources.ApplyResources(this.xrTableCell319, "xrTableCell319");
      this.xrTableCell319.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M12")});
      this.xrTableCell319.Name = "xrTableCell319";
      this.xrTableCell319.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell319.StylePriority.UseBackColor = false;
      this.xrTableCell319.StylePriority.UseBorderColor = false;
      this.xrTableCell319.StylePriority.UseFont = false;
      this.xrTableCell319.StylePriority.UseForeColor = false;
      this.xrTableCell319.StylePriority.UsePadding = false;
      this.xrTableCell319.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell320
      // 
      resources.ApplyResources(this.xrTableCell320, "xrTableCell320");
      this.xrTableCell320.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M13")});
      this.xrTableCell320.Name = "xrTableCell320";
      this.xrTableCell320.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell320.StylePriority.UseBackColor = false;
      this.xrTableCell320.StylePriority.UseBorderColor = false;
      this.xrTableCell320.StylePriority.UseFont = false;
      this.xrTableCell320.StylePriority.UseForeColor = false;
      this.xrTableCell320.StylePriority.UsePadding = false;
      this.xrTableCell320.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell321
      // 
      resources.ApplyResources(this.xrTableCell321, "xrTableCell321");
      this.xrTableCell321.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M14")});
      this.xrTableCell321.Name = "xrTableCell321";
      this.xrTableCell321.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell321.StylePriority.UseBackColor = false;
      this.xrTableCell321.StylePriority.UseBorderColor = false;
      this.xrTableCell321.StylePriority.UseFont = false;
      this.xrTableCell321.StylePriority.UseForeColor = false;
      this.xrTableCell321.StylePriority.UsePadding = false;
      this.xrTableCell321.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell322
      // 
      resources.ApplyResources(this.xrTableCell322, "xrTableCell322");
      this.xrTableCell322.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M15")});
      this.xrTableCell322.Name = "xrTableCell322";
      this.xrTableCell322.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell322.StylePriority.UseBackColor = false;
      this.xrTableCell322.StylePriority.UseBorderColor = false;
      this.xrTableCell322.StylePriority.UseFont = false;
      this.xrTableCell322.StylePriority.UseForeColor = false;
      this.xrTableCell322.StylePriority.UsePadding = false;
      this.xrTableCell322.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell323
      // 
      resources.ApplyResources(this.xrTableCell323, "xrTableCell323");
      this.xrTableCell323.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M16")});
      this.xrTableCell323.Name = "xrTableCell323";
      this.xrTableCell323.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell323.StylePriority.UseBackColor = false;
      this.xrTableCell323.StylePriority.UseBorderColor = false;
      this.xrTableCell323.StylePriority.UseFont = false;
      this.xrTableCell323.StylePriority.UseForeColor = false;
      this.xrTableCell323.StylePriority.UsePadding = false;
      this.xrTableCell323.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell324
      // 
      resources.ApplyResources(this.xrTableCell324, "xrTableCell324");
      this.xrTableCell324.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M17")});
      this.xrTableCell324.Name = "xrTableCell324";
      this.xrTableCell324.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell324.StylePriority.UseBackColor = false;
      this.xrTableCell324.StylePriority.UseBorderColor = false;
      this.xrTableCell324.StylePriority.UseFont = false;
      this.xrTableCell324.StylePriority.UseForeColor = false;
      this.xrTableCell324.StylePriority.UsePadding = false;
      this.xrTableCell324.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell325
      // 
      resources.ApplyResources(this.xrTableCell325, "xrTableCell325");
      this.xrTableCell325.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M18")});
      this.xrTableCell325.Name = "xrTableCell325";
      this.xrTableCell325.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell325.StylePriority.UseBackColor = false;
      this.xrTableCell325.StylePriority.UseBorderColor = false;
      this.xrTableCell325.StylePriority.UseFont = false;
      this.xrTableCell325.StylePriority.UseForeColor = false;
      this.xrTableCell325.StylePriority.UsePadding = false;
      this.xrTableCell325.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell326
      // 
      resources.ApplyResources(this.xrTableCell326, "xrTableCell326");
      this.xrTableCell326.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M19")});
      this.xrTableCell326.Name = "xrTableCell326";
      this.xrTableCell326.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell326.StylePriority.UseBackColor = false;
      this.xrTableCell326.StylePriority.UseBorderColor = false;
      this.xrTableCell326.StylePriority.UseFont = false;
      this.xrTableCell326.StylePriority.UseForeColor = false;
      this.xrTableCell326.StylePriority.UsePadding = false;
      this.xrTableCell326.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell327
      // 
      resources.ApplyResources(this.xrTableCell327, "xrTableCell327");
      this.xrTableCell327.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M20")});
      this.xrTableCell327.Name = "xrTableCell327";
      this.xrTableCell327.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell327.StylePriority.UseBackColor = false;
      this.xrTableCell327.StylePriority.UseBorderColor = false;
      this.xrTableCell327.StylePriority.UseFont = false;
      this.xrTableCell327.StylePriority.UseForeColor = false;
      this.xrTableCell327.StylePriority.UsePadding = false;
      this.xrTableCell327.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell328
      // 
      resources.ApplyResources(this.xrTableCell328, "xrTableCell328");
      this.xrTableCell328.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M21")});
      this.xrTableCell328.Name = "xrTableCell328";
      this.xrTableCell328.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell328.StylePriority.UseBackColor = false;
      this.xrTableCell328.StylePriority.UseBorderColor = false;
      this.xrTableCell328.StylePriority.UseFont = false;
      this.xrTableCell328.StylePriority.UseForeColor = false;
      this.xrTableCell328.StylePriority.UsePadding = false;
      this.xrTableCell328.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell329
      // 
      resources.ApplyResources(this.xrTableCell329, "xrTableCell329");
      this.xrTableCell329.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M22")});
      this.xrTableCell329.Name = "xrTableCell329";
      this.xrTableCell329.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell329.StylePriority.UseBackColor = false;
      this.xrTableCell329.StylePriority.UseBorderColor = false;
      this.xrTableCell329.StylePriority.UseFont = false;
      this.xrTableCell329.StylePriority.UseForeColor = false;
      this.xrTableCell329.StylePriority.UsePadding = false;
      this.xrTableCell329.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell331
      // 
      resources.ApplyResources(this.xrTableCell331, "xrTableCell331");
      this.xrTableCell331.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M23")});
      this.xrTableCell331.Name = "xrTableCell331";
      this.xrTableCell331.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell331.StylePriority.UseBackColor = false;
      this.xrTableCell331.StylePriority.UseBorderColor = false;
      this.xrTableCell331.StylePriority.UseFont = false;
      this.xrTableCell331.StylePriority.UseForeColor = false;
      this.xrTableCell331.StylePriority.UsePadding = false;
      this.xrTableCell331.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell332
      // 
      resources.ApplyResources(this.xrTableCell332, "xrTableCell332");
      this.xrTableCell332.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell332.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Consumer24MonthlyPayment.M24")});
      this.xrTableCell332.Name = "xrTableCell332";
      this.xrTableCell332.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell332.StylePriority.UseBackColor = false;
      this.xrTableCell332.StylePriority.UseBorderColor = false;
      this.xrTableCell332.StylePriority.UseBorders = false;
      this.xrTableCell332.StylePriority.UseFont = false;
      this.xrTableCell332.StylePriority.UseForeColor = false;
      this.xrTableCell332.StylePriority.UsePadding = false;
      this.xrTableCell332.StylePriority.UseTextAlignment = false;
      // 
      // DetailReport4
      // 
      this.DetailReport4.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail20,
            this.GroupHeader10});
      this.DetailReport4.DataMember = "ConsumerDefinition";
      resources.ApplyResources(this.DetailReport4, "DetailReport4");
      this.DetailReport4.Level = 4;
      this.DetailReport4.Name = "DetailReport4";
      // 
      // Detail20
      // 
      this.Detail20.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblConsumerDefinitionC});
      resources.ApplyResources(this.Detail20, "Detail20");
      this.Detail20.Name = "Detail20";
      // 
      // tblConsumerDefinitionC
      // 
      this.tblConsumerDefinitionC.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
      resources.ApplyResources(this.tblConsumerDefinitionC, "tblConsumerDefinitionC");
      this.tblConsumerDefinitionC.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.tblConsumerDefinitionC.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Tag", null, "ConsumerNLRDefinition.DefinitionCode")});
      this.tblConsumerDefinitionC.Name = "tblConsumerDefinitionC";
      this.tblConsumerDefinitionC.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.tblConsumerDefinitionC.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow133});
      this.tblConsumerDefinitionC.StylePriority.UseBackColor = false;
      this.tblConsumerDefinitionC.StylePriority.UseBorderColor = false;
      this.tblConsumerDefinitionC.StylePriority.UseBorders = false;
      this.tblConsumerDefinitionC.StylePriority.UseFont = false;
      this.tblConsumerDefinitionC.StylePriority.UsePadding = false;
      // 
      // xrTableRow133
      // 
      resources.ApplyResources(this.xrTableRow133, "xrTableRow133");
      this.xrTableRow133.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableRow133.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDefDesc,
            this.lbDefCode});
      this.xrTableRow133.Name = "xrTableRow133";
      this.xrTableRow133.StylePriority.UseBackColor = false;
      this.xrTableRow133.StylePriority.UseBorderColor = false;
      this.xrTableRow133.StylePriority.UseBorders = false;
      this.xrTableRow133.StylePriority.UseFont = false;
      this.xrTableRow133.StylePriority.UseForeColor = false;
      // 
      // lblDefDesc
      // 
      resources.ApplyResources(this.lblDefDesc, "lblDefDesc");
      this.lblDefDesc.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.lblDefDesc.CanGrow = false;
      this.lblDefDesc.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Tag", null, "ConsumerNLRDefinition.DefinitionCode"),
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDefinition.DefinitionDesc")});
      this.lblDefDesc.Name = "lblDefDesc";
      this.lblDefDesc.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblDefDesc.StylePriority.UseBackColor = false;
      this.lblDefDesc.StylePriority.UseBorderColor = false;
      this.lblDefDesc.StylePriority.UseBorders = false;
      this.lblDefDesc.StylePriority.UseFont = false;
      this.lblDefDesc.StylePriority.UseForeColor = false;
      this.lblDefDesc.StylePriority.UsePadding = false;
      this.lblDefDesc.StylePriority.UseTextAlignment = false;
      // 
      // lbDefCode
      // 
      resources.ApplyResources(this.lbDefCode, "lbDefCode");
      this.lbDefCode.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.lbDefCode.CanGrow = false;
      this.lbDefCode.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Tag", null, "ConsumerNLRDefinition.DefinitionDesc"),
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDefinition.DefinitionCode")});
      this.lbDefCode.Name = "lbDefCode";
      this.lbDefCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lbDefCode.StylePriority.UseBackColor = false;
      this.lbDefCode.StylePriority.UseBorderColor = false;
      this.lbDefCode.StylePriority.UseBorders = false;
      this.lbDefCode.StylePriority.UseFont = false;
      this.lbDefCode.StylePriority.UseForeColor = false;
      this.lbDefCode.StylePriority.UsePadding = false;
      this.lbDefCode.StylePriority.UseTextAlignment = false;
      // 
      // GroupHeader10
      // 
      this.GroupHeader10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable25});
      resources.ApplyResources(this.GroupHeader10, "GroupHeader10");
      this.GroupHeader10.Name = "GroupHeader10";
      // 
      // xrTable25
      // 
      this.xrTable25.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
      resources.ApplyResources(this.xrTable25, "xrTable25");
      this.xrTable25.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTable25.Name = "xrTable25";
      this.xrTable25.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable25.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow135});
      this.xrTable25.StylePriority.UseBackColor = false;
      this.xrTable25.StylePriority.UseBorderColor = false;
      this.xrTable25.StylePriority.UseBorders = false;
      this.xrTable25.StylePriority.UseFont = false;
      this.xrTable25.StylePriority.UsePadding = false;
      // 
      // xrTableRow135
      // 
      this.xrTableRow135.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell260,
            this.xrTableCell333});
      resources.ApplyResources(this.xrTableRow135, "xrTableRow135");
      this.xrTableRow135.Name = "xrTableRow135";
      this.xrTableRow135.StylePriority.UseFont = false;
      this.xrTableRow135.StylePriority.UseForeColor = false;
      // 
      // xrTableCell260
      // 
      resources.ApplyResources(this.xrTableCell260, "xrTableCell260");
      this.xrTableCell260.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell260.CanGrow = false;
      this.xrTableCell260.Name = "xrTableCell260";
      this.xrTableCell260.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell260.StylePriority.UseBorderColor = false;
      this.xrTableCell260.StylePriority.UseBorders = false;
      this.xrTableCell260.StylePriority.UseFont = false;
      this.xrTableCell260.StylePriority.UseForeColor = false;
      this.xrTableCell260.StylePriority.UsePadding = false;
      this.xrTableCell260.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell333
      // 
      resources.ApplyResources(this.xrTableCell333, "xrTableCell333");
      this.xrTableCell333.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell333.CanGrow = false;
      this.xrTableCell333.Name = "xrTableCell333";
      this.xrTableCell333.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell333.StylePriority.UseBorderColor = false;
      this.xrTableCell333.StylePriority.UseBorders = false;
      this.xrTableCell333.StylePriority.UseFont = false;
      this.xrTableCell333.StylePriority.UseForeColor = false;
      this.xrTableCell333.StylePriority.UsePadding = false;
      this.xrTableCell333.StylePriority.UseTextAlignment = false;
      // 
      // DetailReport10
      // 
      this.DetailReport10.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail6,
            this.GroupHeader6});
      this.DetailReport10.DataMember = "ConsumerAccountStatus";
      resources.ApplyResources(this.DetailReport10, "DetailReport10");
      this.DetailReport10.Level = 0;
      this.DetailReport10.Name = "DetailReport10";
      // 
      // Detail6
      // 
      this.Detail6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable51});
      resources.ApplyResources(this.Detail6, "Detail6");
      this.Detail6.Name = "Detail6";
      // 
      // xrTable51
      // 
      resources.ApplyResources(this.xrTable51, "xrTable51");
      this.xrTable51.Name = "xrTable51";
      this.xrTable51.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable51.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow179});
      this.xrTable51.StylePriority.UseBackColor = false;
      this.xrTable51.StylePriority.UseBorderColor = false;
      this.xrTable51.StylePriority.UseFont = false;
      this.xrTable51.StylePriority.UsePadding = false;
      // 
      // xrTableRow179
      // 
      resources.ApplyResources(this.xrTableRow179, "xrTableRow179");
      this.xrTableRow179.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableRow179.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell515,
            this.xrTableCell516,
            this.xrTableCell517,
            this.xrTableCell518,
            this.xrTableCell519,
            this.xrTableCell520,
            this.xrTableCell521,
            this.xrTableCell522,
            this.xrTableCell523,
            this.xrTableCell524});
      this.xrTableRow179.Name = "xrTableRow179";
      this.xrTableRow179.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTableRow179.StylePriority.UseBackColor = false;
      this.xrTableRow179.StylePriority.UseBorderColor = false;
      this.xrTableRow179.StylePriority.UseBorders = false;
      this.xrTableRow179.StylePriority.UseFont = false;
      this.xrTableRow179.StylePriority.UseForeColor = false;
      this.xrTableRow179.StylePriority.UsePadding = false;
      this.xrTableRow179.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell515
      // 
      resources.ApplyResources(this.xrTableCell515, "xrTableCell515");
      this.xrTableCell515.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell515.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountStatus.AccountOpenedDate")});
      this.xrTableCell515.Name = "xrTableCell515";
      this.xrTableCell515.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell515.StylePriority.UseBackColor = false;
      this.xrTableCell515.StylePriority.UseBorderColor = false;
      this.xrTableCell515.StylePriority.UseBorders = false;
      this.xrTableCell515.StylePriority.UseFont = false;
      this.xrTableCell515.StylePriority.UseForeColor = false;
      this.xrTableCell515.StylePriority.UsePadding = false;
      this.xrTableCell515.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell516
      // 
      resources.ApplyResources(this.xrTableCell516, "xrTableCell516");
      this.xrTableCell516.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell516.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountStatus.SubscriberName")});
      this.xrTableCell516.Name = "xrTableCell516";
      this.xrTableCell516.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell516.StylePriority.UseBackColor = false;
      this.xrTableCell516.StylePriority.UseBorderColor = false;
      this.xrTableCell516.StylePriority.UseBorders = false;
      this.xrTableCell516.StylePriority.UseFont = false;
      this.xrTableCell516.StylePriority.UseForeColor = false;
      this.xrTableCell516.StylePriority.UsePadding = false;
      this.xrTableCell516.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell517
      // 
      resources.ApplyResources(this.xrTableCell517, "xrTableCell517");
      this.xrTableCell517.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountStatus.AccountNo")});
      this.xrTableCell517.Name = "xrTableCell517";
      this.xrTableCell517.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell517.StylePriority.UseBackColor = false;
      this.xrTableCell517.StylePriority.UseBorderColor = false;
      this.xrTableCell517.StylePriority.UseFont = false;
      this.xrTableCell517.StylePriority.UseForeColor = false;
      this.xrTableCell517.StylePriority.UsePadding = false;
      this.xrTableCell517.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell518
      // 
      resources.ApplyResources(this.xrTableCell518, "xrTableCell518");
      this.xrTableCell518.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell518.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountStatus.CreditLimitAmt", "{0:c2}")});
      this.xrTableCell518.Name = "xrTableCell518";
      this.xrTableCell518.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell518.StylePriority.UseBackColor = false;
      this.xrTableCell518.StylePriority.UseBorderColor = false;
      this.xrTableCell518.StylePriority.UseBorders = false;
      this.xrTableCell518.StylePriority.UseFont = false;
      this.xrTableCell518.StylePriority.UseForeColor = false;
      this.xrTableCell518.StylePriority.UsePadding = false;
      this.xrTableCell518.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell519
      // 
      resources.ApplyResources(this.xrTableCell519, "xrTableCell519");
      this.xrTableCell519.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell519.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountStatus.CurrentBalanceAmt", "{0:c2}")});
      this.xrTableCell519.Name = "xrTableCell519";
      this.xrTableCell519.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell519.StylePriority.UseBackColor = false;
      this.xrTableCell519.StylePriority.UseBorderColor = false;
      this.xrTableCell519.StylePriority.UseBorders = false;
      this.xrTableCell519.StylePriority.UseFont = false;
      this.xrTableCell519.StylePriority.UseForeColor = false;
      this.xrTableCell519.StylePriority.UsePadding = false;
      this.xrTableCell519.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell520
      // 
      resources.ApplyResources(this.xrTableCell520, "xrTableCell520");
      this.xrTableCell520.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell520.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountStatus.MonthlyInstalmentAmt", "{0:c2}")});
      this.xrTableCell520.Name = "xrTableCell520";
      this.xrTableCell520.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell520.StylePriority.UseBackColor = false;
      this.xrTableCell520.StylePriority.UseBorderColor = false;
      this.xrTableCell520.StylePriority.UseBorders = false;
      this.xrTableCell520.StylePriority.UseFont = false;
      this.xrTableCell520.StylePriority.UseForeColor = false;
      this.xrTableCell520.StylePriority.UsePadding = false;
      this.xrTableCell520.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell521
      // 
      resources.ApplyResources(this.xrTableCell521, "xrTableCell521");
      this.xrTableCell521.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell521.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountStatus.ArrearsAmt", "{0:c2}")});
      this.xrTableCell521.Name = "xrTableCell521";
      this.xrTableCell521.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell521.StylePriority.UseBackColor = false;
      this.xrTableCell521.StylePriority.UseBorderColor = false;
      this.xrTableCell521.StylePriority.UseBorders = false;
      this.xrTableCell521.StylePriority.UseFont = false;
      this.xrTableCell521.StylePriority.UseForeColor = false;
      this.xrTableCell521.StylePriority.UsePadding = false;
      this.xrTableCell521.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell522
      // 
      resources.ApplyResources(this.xrTableCell522, "xrTableCell522");
      this.xrTableCell522.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell522.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountStatus.ArrearsTypeInd")});
      this.xrTableCell522.Name = "xrTableCell522";
      this.xrTableCell522.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell522.StylePriority.UseBackColor = false;
      this.xrTableCell522.StylePriority.UseBorderColor = false;
      this.xrTableCell522.StylePriority.UseBorders = false;
      this.xrTableCell522.StylePriority.UseFont = false;
      this.xrTableCell522.StylePriority.UseForeColor = false;
      this.xrTableCell522.StylePriority.UsePadding = false;
      this.xrTableCell522.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell523
      // 
      resources.ApplyResources(this.xrTableCell523, "xrTableCell523");
      this.xrTableCell523.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountStatus.StatusCodeDesc")});
      this.xrTableCell523.Name = "xrTableCell523";
      this.xrTableCell523.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell523.StylePriority.UseBackColor = false;
      this.xrTableCell523.StylePriority.UseBorderColor = false;
      this.xrTableCell523.StylePriority.UseFont = false;
      this.xrTableCell523.StylePriority.UseForeColor = false;
      this.xrTableCell523.StylePriority.UsePadding = false;
      this.xrTableCell523.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell524
      // 
      resources.ApplyResources(this.xrTableCell524, "xrTableCell524");
      this.xrTableCell524.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAccountStatus.LastPaymentDate")});
      this.xrTableCell524.Name = "xrTableCell524";
      this.xrTableCell524.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell524.StylePriority.UseBackColor = false;
      this.xrTableCell524.StylePriority.UseBorderColor = false;
      this.xrTableCell524.StylePriority.UseFont = false;
      this.xrTableCell524.StylePriority.UseForeColor = false;
      this.xrTableCell524.StylePriority.UsePadding = false;
      this.xrTableCell524.StylePriority.UseTextAlignment = false;
      // 
      // GroupHeader6
      // 
      this.GroupHeader6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable50});
      resources.ApplyResources(this.GroupHeader6, "GroupHeader6");
      this.GroupHeader6.Name = "GroupHeader6";
      // 
      // xrTable50
      // 
      this.xrTable50.Borders = DevExpress.XtraPrinting.BorderSide.None;
      resources.ApplyResources(this.xrTable50, "xrTable50");
      this.xrTable50.KeepTogether = true;
      this.xrTable50.Name = "xrTable50";
      this.xrTable50.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTable50.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow173,
            this.xrTableRow174,
            this.xrTableRow177,
            this.xrTableRow178});
      this.xrTable50.StylePriority.UseBorders = false;
      this.xrTable50.StylePriority.UsePadding = false;
      // 
      // xrTableRow173
      // 
      this.xrTableRow173.Borders = DevExpress.XtraPrinting.BorderSide.Top;
      this.xrTableRow173.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell491,
            this.xrTableCell493});
      resources.ApplyResources(this.xrTableRow173, "xrTableRow173");
      this.xrTableRow173.Name = "xrTableRow173";
      this.xrTableRow173.StylePriority.UseBorders = false;
      // 
      // xrTableCell491
      // 
      resources.ApplyResources(this.xrTableCell491, "xrTableCell491");
      this.xrTableCell491.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell491.Name = "xrTableCell491";
      this.xrTableCell491.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell491.StylePriority.UseBackColor = false;
      this.xrTableCell491.StylePriority.UseBorderColor = false;
      this.xrTableCell491.StylePriority.UseBorders = false;
      this.xrTableCell491.StylePriority.UseFont = false;
      this.xrTableCell491.StylePriority.UseForeColor = false;
      this.xrTableCell491.StylePriority.UsePadding = false;
      this.xrTableCell491.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell493
      // 
      resources.ApplyResources(this.xrTableCell493, "xrTableCell493");
      this.xrTableCell493.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell493.Name = "xrTableCell493";
      this.xrTableCell493.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell493.StylePriority.UseBackColor = false;
      this.xrTableCell493.StylePriority.UseBorderColor = false;
      this.xrTableCell493.StylePriority.UseBorders = false;
      this.xrTableCell493.StylePriority.UseFont = false;
      this.xrTableCell493.StylePriority.UseForeColor = false;
      this.xrTableCell493.StylePriority.UsePadding = false;
      this.xrTableCell493.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow174
      // 
      this.xrTableRow174.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell495});
      resources.ApplyResources(this.xrTableRow174, "xrTableRow174");
      this.xrTableRow174.Name = "xrTableRow174";
      // 
      // xrTableCell495
      // 
      this.xrTableCell495.Borders = DevExpress.XtraPrinting.BorderSide.None;
      resources.ApplyResources(this.xrTableCell495, "xrTableCell495");
      this.xrTableCell495.Name = "xrTableCell495";
      this.xrTableCell495.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      this.xrTableCell495.StylePriority.UseBorders = false;
      this.xrTableCell495.StylePriority.UsePadding = false;
      // 
      // xrTableRow177
      // 
      this.xrTableRow177.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell497});
      resources.ApplyResources(this.xrTableRow177, "xrTableRow177");
      this.xrTableRow177.Name = "xrTableRow177";
      // 
      // xrTableCell497
      // 
      resources.ApplyResources(this.xrTableCell497, "xrTableCell497");
      this.xrTableCell497.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell497.Name = "xrTableCell497";
      this.xrTableCell497.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell497.StylePriority.UseBackColor = false;
      this.xrTableCell497.StylePriority.UseBorderColor = false;
      this.xrTableCell497.StylePriority.UseBorders = false;
      this.xrTableCell497.StylePriority.UseFont = false;
      this.xrTableCell497.StylePriority.UseForeColor = false;
      this.xrTableCell497.StylePriority.UsePadding = false;
      this.xrTableCell497.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow178
      // 
      this.xrTableRow178.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell504,
            this.xrTableCell505,
            this.xrTableCell506,
            this.xrTableCell508,
            this.xrTableCell509,
            this.xrTableCell510,
            this.xrTableCell511,
            this.xrTableCell512,
            this.xrTableCell513,
            this.xrTableCell514});
      resources.ApplyResources(this.xrTableRow178, "xrTableRow178");
      this.xrTableRow178.Name = "xrTableRow178";
      this.xrTableRow178.StylePriority.UseBackColor = false;
      this.xrTableRow178.StylePriority.UseBorderColor = false;
      this.xrTableRow178.StylePriority.UseBorders = false;
      // 
      // xrTableCell504
      // 
      resources.ApplyResources(this.xrTableCell504, "xrTableCell504");
      this.xrTableCell504.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell504.Name = "xrTableCell504";
      this.xrTableCell504.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell504.StylePriority.UseBackColor = false;
      this.xrTableCell504.StylePriority.UseBorderColor = false;
      this.xrTableCell504.StylePriority.UseBorders = false;
      this.xrTableCell504.StylePriority.UseFont = false;
      this.xrTableCell504.StylePriority.UseForeColor = false;
      this.xrTableCell504.StylePriority.UsePadding = false;
      this.xrTableCell504.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell505
      // 
      resources.ApplyResources(this.xrTableCell505, "xrTableCell505");
      this.xrTableCell505.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell505.Name = "xrTableCell505";
      this.xrTableCell505.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell505.StylePriority.UseBackColor = false;
      this.xrTableCell505.StylePriority.UseBorderColor = false;
      this.xrTableCell505.StylePriority.UseBorders = false;
      this.xrTableCell505.StylePriority.UseFont = false;
      this.xrTableCell505.StylePriority.UseForeColor = false;
      this.xrTableCell505.StylePriority.UsePadding = false;
      this.xrTableCell505.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell506
      // 
      resources.ApplyResources(this.xrTableCell506, "xrTableCell506");
      this.xrTableCell506.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell506.Name = "xrTableCell506";
      this.xrTableCell506.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell506.StylePriority.UseBackColor = false;
      this.xrTableCell506.StylePriority.UseBorderColor = false;
      this.xrTableCell506.StylePriority.UseBorders = false;
      this.xrTableCell506.StylePriority.UseFont = false;
      this.xrTableCell506.StylePriority.UseForeColor = false;
      this.xrTableCell506.StylePriority.UsePadding = false;
      this.xrTableCell506.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell508
      // 
      resources.ApplyResources(this.xrTableCell508, "xrTableCell508");
      this.xrTableCell508.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell508.Name = "xrTableCell508";
      this.xrTableCell508.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell508.StylePriority.UseBackColor = false;
      this.xrTableCell508.StylePriority.UseBorderColor = false;
      this.xrTableCell508.StylePriority.UseBorders = false;
      this.xrTableCell508.StylePriority.UseFont = false;
      this.xrTableCell508.StylePriority.UseForeColor = false;
      this.xrTableCell508.StylePriority.UsePadding = false;
      this.xrTableCell508.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell509
      // 
      resources.ApplyResources(this.xrTableCell509, "xrTableCell509");
      this.xrTableCell509.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell509.Name = "xrTableCell509";
      this.xrTableCell509.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell509.StylePriority.UseBackColor = false;
      this.xrTableCell509.StylePriority.UseBorderColor = false;
      this.xrTableCell509.StylePriority.UseBorders = false;
      this.xrTableCell509.StylePriority.UseFont = false;
      this.xrTableCell509.StylePriority.UseForeColor = false;
      this.xrTableCell509.StylePriority.UsePadding = false;
      this.xrTableCell509.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell510
      // 
      resources.ApplyResources(this.xrTableCell510, "xrTableCell510");
      this.xrTableCell510.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell510.Name = "xrTableCell510";
      this.xrTableCell510.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell510.StylePriority.UseBackColor = false;
      this.xrTableCell510.StylePriority.UseBorderColor = false;
      this.xrTableCell510.StylePriority.UseBorders = false;
      this.xrTableCell510.StylePriority.UseFont = false;
      this.xrTableCell510.StylePriority.UseForeColor = false;
      this.xrTableCell510.StylePriority.UsePadding = false;
      this.xrTableCell510.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell511
      // 
      resources.ApplyResources(this.xrTableCell511, "xrTableCell511");
      this.xrTableCell511.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell511.Name = "xrTableCell511";
      this.xrTableCell511.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell511.StylePriority.UseBackColor = false;
      this.xrTableCell511.StylePriority.UseBorderColor = false;
      this.xrTableCell511.StylePriority.UseBorders = false;
      this.xrTableCell511.StylePriority.UseFont = false;
      this.xrTableCell511.StylePriority.UseForeColor = false;
      this.xrTableCell511.StylePriority.UsePadding = false;
      this.xrTableCell511.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell512
      // 
      resources.ApplyResources(this.xrTableCell512, "xrTableCell512");
      this.xrTableCell512.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell512.Name = "xrTableCell512";
      this.xrTableCell512.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell512.StylePriority.UseBackColor = false;
      this.xrTableCell512.StylePriority.UseBorderColor = false;
      this.xrTableCell512.StylePriority.UseBorders = false;
      this.xrTableCell512.StylePriority.UseFont = false;
      this.xrTableCell512.StylePriority.UseForeColor = false;
      this.xrTableCell512.StylePriority.UsePadding = false;
      this.xrTableCell512.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell513
      // 
      resources.ApplyResources(this.xrTableCell513, "xrTableCell513");
      this.xrTableCell513.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell513.Name = "xrTableCell513";
      this.xrTableCell513.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell513.StylePriority.UseBackColor = false;
      this.xrTableCell513.StylePriority.UseBorderColor = false;
      this.xrTableCell513.StylePriority.UseBorders = false;
      this.xrTableCell513.StylePriority.UseFont = false;
      this.xrTableCell513.StylePriority.UseForeColor = false;
      this.xrTableCell513.StylePriority.UsePadding = false;
      this.xrTableCell513.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell514
      // 
      resources.ApplyResources(this.xrTableCell514, "xrTableCell514");
      this.xrTableCell514.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell514.Name = "xrTableCell514";
      this.xrTableCell514.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell514.StylePriority.UseBackColor = false;
      this.xrTableCell514.StylePriority.UseBorderColor = false;
      this.xrTableCell514.StylePriority.UseBorders = false;
      this.xrTableCell514.StylePriority.UseFont = false;
      this.xrTableCell514.StylePriority.UseForeColor = false;
      this.xrTableCell514.StylePriority.UsePadding = false;
      this.xrTableCell514.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell97
      // 
      resources.ApplyResources(this.xrTableCell97, "xrTableCell97");
      this.xrTableCell97.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell97.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerMaritalStatusEnquiry.Surname")});
      this.xrTableCell97.Name = "xrTableCell97";
      this.xrTableCell97.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell97.StylePriority.UseBackColor = false;
      this.xrTableCell97.StylePriority.UseBorderColor = false;
      this.xrTableCell97.StylePriority.UseBorders = false;
      this.xrTableCell97.StylePriority.UseFont = false;
      this.xrTableCell97.StylePriority.UseForeColor = false;
      this.xrTableCell97.StylePriority.UsePadding = false;
      this.xrTableCell97.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell95
      // 
      resources.ApplyResources(this.xrTableCell95, "xrTableCell95");
      this.xrTableCell95.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell95.Name = "xrTableCell95";
      this.xrTableCell95.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell95.StylePriority.UseBackColor = false;
      this.xrTableCell95.StylePriority.UseBorderColor = false;
      this.xrTableCell95.StylePriority.UseBorders = false;
      this.xrTableCell95.StylePriority.UseFont = false;
      this.xrTableCell95.StylePriority.UseForeColor = false;
      this.xrTableCell95.StylePriority.UsePadding = false;
      this.xrTableCell95.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell94
      // 
      resources.ApplyResources(this.xrTableCell94, "xrTableCell94");
      this.xrTableCell94.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell94.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerMaritalStatusEnquiry.Surname")});
      this.xrTableCell94.Name = "xrTableCell94";
      this.xrTableCell94.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell94.StylePriority.UseBackColor = false;
      this.xrTableCell94.StylePriority.UseBorderColor = false;
      this.xrTableCell94.StylePriority.UseBorders = false;
      this.xrTableCell94.StylePriority.UseFont = false;
      this.xrTableCell94.StylePriority.UseForeColor = false;
      this.xrTableCell94.StylePriority.UsePadding = false;
      this.xrTableCell94.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell87
      // 
      resources.ApplyResources(this.xrTableCell87, "xrTableCell87");
      this.xrTableCell87.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell87.Name = "xrTableCell87";
      this.xrTableCell87.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell87.StylePriority.UseBackColor = false;
      this.xrTableCell87.StylePriority.UseBorderColor = false;
      this.xrTableCell87.StylePriority.UseBorders = false;
      this.xrTableCell87.StylePriority.UseFont = false;
      this.xrTableCell87.StylePriority.UseForeColor = false;
      this.xrTableCell87.StylePriority.UsePadding = false;
      this.xrTableCell87.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow32
      // 
      this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell87,
            this.xrTableCell94,
            this.xrTableCell95,
            this.xrTableCell97});
      resources.ApplyResources(this.xrTableRow32, "xrTableRow32");
      this.xrTableRow32.Name = "xrTableRow32";
      // 
      // xrTableCell86
      // 
      resources.ApplyResources(this.xrTableCell86, "xrTableCell86");
      this.xrTableCell86.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell86.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerMaritalStatusEnquiry.SpouseFirstName")});
      this.xrTableCell86.Name = "xrTableCell86";
      this.xrTableCell86.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell86.StylePriority.UseBackColor = false;
      this.xrTableCell86.StylePriority.UseBorderColor = false;
      this.xrTableCell86.StylePriority.UseBorders = false;
      this.xrTableCell86.StylePriority.UseFont = false;
      this.xrTableCell86.StylePriority.UseForeColor = false;
      this.xrTableCell86.StylePriority.UsePadding = false;
      this.xrTableCell86.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell85
      // 
      resources.ApplyResources(this.xrTableCell85, "xrTableCell85");
      this.xrTableCell85.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell85.Name = "xrTableCell85";
      this.xrTableCell85.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell85.StylePriority.UseBackColor = false;
      this.xrTableCell85.StylePriority.UseBorderColor = false;
      this.xrTableCell85.StylePriority.UseBorders = false;
      this.xrTableCell85.StylePriority.UseFont = false;
      this.xrTableCell85.StylePriority.UseForeColor = false;
      this.xrTableCell85.StylePriority.UsePadding = false;
      this.xrTableCell85.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell84
      // 
      resources.ApplyResources(this.xrTableCell84, "xrTableCell84");
      this.xrTableCell84.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell84.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerMaritalStatusEnquiry.FirstName")});
      this.xrTableCell84.Name = "xrTableCell84";
      this.xrTableCell84.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell84.StylePriority.UseBackColor = false;
      this.xrTableCell84.StylePriority.UseBorderColor = false;
      this.xrTableCell84.StylePriority.UseBorders = false;
      this.xrTableCell84.StylePriority.UseFont = false;
      this.xrTableCell84.StylePriority.UseForeColor = false;
      this.xrTableCell84.StylePriority.UsePadding = false;
      this.xrTableCell84.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell75
      // 
      resources.ApplyResources(this.xrTableCell75, "xrTableCell75");
      this.xrTableCell75.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell75.Name = "xrTableCell75";
      this.xrTableCell75.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell75.StylePriority.UseBackColor = false;
      this.xrTableCell75.StylePriority.UseBorderColor = false;
      this.xrTableCell75.StylePriority.UseBorders = false;
      this.xrTableCell75.StylePriority.UseFont = false;
      this.xrTableCell75.StylePriority.UseForeColor = false;
      this.xrTableCell75.StylePriority.UsePadding = false;
      this.xrTableCell75.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow28
      // 
      this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell75,
            this.xrTableCell84,
            this.xrTableCell85,
            this.xrTableCell86});
      resources.ApplyResources(this.xrTableRow28, "xrTableRow28");
      this.xrTableRow28.Name = "xrTableRow28";
      // 
      // xrTableCell65
      // 
      resources.ApplyResources(this.xrTableCell65, "xrTableCell65");
      this.xrTableCell65.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell65.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerMaritalStatusEnquiry.SpouseConsumerID")});
      this.xrTableCell65.Name = "xrTableCell65";
      this.xrTableCell65.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell65.StylePriority.UseBackColor = false;
      this.xrTableCell65.StylePriority.UseBorderColor = false;
      this.xrTableCell65.StylePriority.UseBorders = false;
      this.xrTableCell65.StylePriority.UseFont = false;
      this.xrTableCell65.StylePriority.UseForeColor = false;
      this.xrTableCell65.StylePriority.UsePadding = false;
      this.xrTableCell65.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell64
      // 
      resources.ApplyResources(this.xrTableCell64, "xrTableCell64");
      this.xrTableCell64.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell64.Name = "xrTableCell64";
      this.xrTableCell64.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell64.StylePriority.UseBackColor = false;
      this.xrTableCell64.StylePriority.UseBorderColor = false;
      this.xrTableCell64.StylePriority.UseBorders = false;
      this.xrTableCell64.StylePriority.UseFont = false;
      this.xrTableCell64.StylePriority.UseForeColor = false;
      this.xrTableCell64.StylePriority.UsePadding = false;
      this.xrTableCell64.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell63
      // 
      resources.ApplyResources(this.xrTableCell63, "xrTableCell63");
      this.xrTableCell63.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell63.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerMaritalStatusEnquiry.Idno")});
      this.xrTableCell63.Name = "xrTableCell63";
      this.xrTableCell63.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell63.StylePriority.UseBackColor = false;
      this.xrTableCell63.StylePriority.UseBorderColor = false;
      this.xrTableCell63.StylePriority.UseBorders = false;
      this.xrTableCell63.StylePriority.UseFont = false;
      this.xrTableCell63.StylePriority.UseForeColor = false;
      this.xrTableCell63.StylePriority.UsePadding = false;
      this.xrTableCell63.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell61
      // 
      resources.ApplyResources(this.xrTableCell61, "xrTableCell61");
      this.xrTableCell61.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell61.Name = "xrTableCell61";
      this.xrTableCell61.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell61.StylePriority.UseBackColor = false;
      this.xrTableCell61.StylePriority.UseBorderColor = false;
      this.xrTableCell61.StylePriority.UseBorders = false;
      this.xrTableCell61.StylePriority.UseFont = false;
      this.xrTableCell61.StylePriority.UseForeColor = false;
      this.xrTableCell61.StylePriority.UsePadding = false;
      this.xrTableCell61.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow26
      // 
      this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell61,
            this.xrTableCell63,
            this.xrTableCell64,
            this.xrTableCell65});
      resources.ApplyResources(this.xrTableRow26, "xrTableRow26");
      this.xrTableRow26.Name = "xrTableRow26";
      // 
      // xrTableCell6
      // 
      this.xrTableCell6.Borders = DevExpress.XtraPrinting.BorderSide.None;
      resources.ApplyResources(this.xrTableCell6, "xrTableCell6");
      this.xrTableCell6.Name = "xrTableCell6";
      this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      this.xrTableCell6.StylePriority.UseBorders = false;
      this.xrTableCell6.StylePriority.UsePadding = false;
      // 
      // xrTableRow4
      // 
      this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6});
      resources.ApplyResources(this.xrTableRow4, "xrTableRow4");
      this.xrTableRow4.Name = "xrTableRow4";
      // 
      // xrTableCell5
      // 
      resources.ApplyResources(this.xrTableCell5, "xrTableCell5");
      this.xrTableCell5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell5.Name = "xrTableCell5";
      this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell5.StylePriority.UseBackColor = false;
      this.xrTableCell5.StylePriority.UseBorderColor = false;
      this.xrTableCell5.StylePriority.UseBorders = false;
      this.xrTableCell5.StylePriority.UseFont = false;
      this.xrTableCell5.StylePriority.UseForeColor = false;
      this.xrTableCell5.StylePriority.UsePadding = false;
      this.xrTableCell5.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell4
      // 
      resources.ApplyResources(this.xrTableCell4, "xrTableCell4");
      this.xrTableCell4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell4.Name = "xrTableCell4";
      this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell4.StylePriority.UseBackColor = false;
      this.xrTableCell4.StylePriority.UseBorderColor = false;
      this.xrTableCell4.StylePriority.UseBorders = false;
      this.xrTableCell4.StylePriority.UseFont = false;
      this.xrTableCell4.StylePriority.UseForeColor = false;
      this.xrTableCell4.StylePriority.UsePadding = false;
      this.xrTableCell4.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow2
      // 
      this.xrTableRow2.Borders = DevExpress.XtraPrinting.BorderSide.Top;
      this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell5});
      resources.ApplyResources(this.xrTableRow2, "xrTableRow2");
      this.xrTableRow2.Name = "xrTableRow2";
      this.xrTableRow2.StylePriority.UseBorders = false;
      // 
      // xrTable8
      // 
      this.xrTable8.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTable8.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Tag", null, "ConsumerMaritalStatusEnquiry.ConsumerID")});
      resources.ApplyResources(this.xrTable8, "xrTable8");
      this.xrTable8.KeepTogether = true;
      this.xrTable8.Name = "xrTable8";
      this.xrTable8.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2,
            this.xrTableRow4,
            this.xrTableRow26,
            this.xrTableRow28,
            this.xrTableRow32});
      this.xrTable8.StylePriority.UseBorders = false;
      this.xrTable8.StylePriority.UsePadding = false;
      // 
      // Detail11
      // 
      this.Detail11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable8});
      resources.ApplyResources(this.Detail11, "Detail11");
      this.Detail11.Name = "Detail11";
      // 
      // MaritalStatus
      // 
      this.MaritalStatus.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail11});
      this.MaritalStatus.DataMember = "ConsumerMaritalStatusEnquiry";
      resources.ApplyResources(this.MaritalStatus, "MaritalStatus");
      this.MaritalStatus.Level = 6;
      this.MaritalStatus.Name = "MaritalStatus";
      // 
      // CreditAccountInformation_NLR
      // 
      this.CreditAccountInformation_NLR.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail21,
            this.DetailReport5,
            this.DetailReport6,
            this.DetailReport7,
            this.DetailReport8,
            this.DetailReport11});
      resources.ApplyResources(this.CreditAccountInformation_NLR, "CreditAccountInformation_NLR");
      this.CreditAccountInformation_NLR.Level = 5;
      this.CreditAccountInformation_NLR.Name = "CreditAccountInformation_NLR";
      // 
      // Detail21
      // 
      resources.ApplyResources(this.Detail21, "Detail21");
      this.Detail21.Name = "Detail21";
      // 
      // DetailReport5
      // 
      this.DetailReport5.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail22});
      this.DetailReport5.DataMember = "NLRAccountTypeLegend";
      resources.ApplyResources(this.DetailReport5, "DetailReport5");
      this.DetailReport5.Level = 1;
      this.DetailReport5.Name = "DetailReport5";
      // 
      // Detail22
      // 
      this.Detail22.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable29});
      resources.ApplyResources(this.Detail22, "Detail22");
      this.Detail22.MultiColumn.ColumnCount = 6;
      this.Detail22.MultiColumn.Layout = DevExpress.XtraPrinting.ColumnLayout.AcrossThenDown;
      this.Detail22.MultiColumn.Mode = DevExpress.XtraReports.UI.MultiColumnMode.UseColumnCount;
      this.Detail22.Name = "Detail22";
      this.Detail22.StylePriority.UseFont = false;
      // 
      // xrTable29
      // 
      resources.ApplyResources(this.xrTable29, "xrTable29");
      this.xrTable29.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTable29.Name = "xrTable29";
      this.xrTable29.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow141});
      this.xrTable29.StylePriority.UseBackColor = false;
      this.xrTable29.StylePriority.UseBorderColor = false;
      this.xrTable29.StylePriority.UseBorders = false;
      this.xrTable29.StylePriority.UseFont = false;
      // 
      // xrTableRow141
      // 
      this.xrTableRow141.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableRow141.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell358});
      resources.ApplyResources(this.xrTableRow141, "xrTableRow141");
      this.xrTableRow141.Name = "xrTableRow141";
      this.xrTableRow141.StylePriority.UseBorders = false;
      // 
      // xrTableCell358
      // 
      resources.ApplyResources(this.xrTableCell358, "xrTableCell358");
      this.xrTableCell358.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTableCell358.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "NLRAccountTypeLegend.AccountTypeDesc")});
      this.xrTableCell358.Name = "xrTableCell358";
      this.xrTableCell358.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTableCell358.StylePriority.UseBackColor = false;
      this.xrTableCell358.StylePriority.UseBorderColor = false;
      this.xrTableCell358.StylePriority.UseBorders = false;
      this.xrTableCell358.StylePriority.UseFont = false;
      this.xrTableCell358.StylePriority.UseForeColor = false;
      this.xrTableCell358.StylePriority.UsePadding = false;
      this.xrTableCell358.StylePriority.UseTextAlignment = false;
      // 
      // DetailReport6
      // 
      this.DetailReport6.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail23,
            this.GroupHeader12});
      this.DetailReport6.DataMember = "ConsumerNLR24MonthlyPaymentHeader";
      resources.ApplyResources(this.DetailReport6, "DetailReport6");
      this.DetailReport6.Level = 2;
      this.DetailReport6.Name = "DetailReport6";
      // 
      // Detail23
      // 
      resources.ApplyResources(this.Detail23, "Detail23");
      this.Detail23.Name = "Detail23";
      // 
      // GroupHeader12
      // 
      this.GroupHeader12.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable30});
      resources.ApplyResources(this.GroupHeader12, "GroupHeader12");
      this.GroupHeader12.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("Company", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
      this.GroupHeader12.Name = "GroupHeader12";
      // 
      // xrTable30
      // 
      resources.ApplyResources(this.xrTable30, "xrTable30");
      this.xrTable30.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTable30.Name = "xrTable30";
      this.xrTable30.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable30.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow143});
      this.xrTable30.StylePriority.UseBorderColor = false;
      this.xrTable30.StylePriority.UseBorders = false;
      this.xrTable30.StylePriority.UseFont = false;
      this.xrTable30.StylePriority.UsePadding = false;
      this.xrTable30.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow143
      // 
      resources.ApplyResources(this.xrTableRow143, "xrTableRow143");
      this.xrTableRow143.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableRow143.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell360,
            this.xrTableCell361,
            this.xrTableCell362,
            this.xrTableCell363,
            this.xrTableCell364,
            this.xrTableCell365,
            this.xrTableCell366,
            this.xrTableCell367,
            this.xrTableCell368,
            this.xrTableCell369,
            this.xrTableCell370,
            this.xrTableCell371,
            this.xrTableCell372,
            this.xrTableCell373,
            this.xrTableCell374,
            this.xrTableCell375,
            this.xrTableCell376,
            this.xrTableCell377,
            this.xrTableCell378,
            this.xrTableCell379,
            this.xrTableCell380,
            this.xrTableCell381,
            this.xrTableCell382,
            this.xrTableCell383,
            this.xrTableCell384});
      this.xrTableRow143.Name = "xrTableRow143";
      this.xrTableRow143.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
      this.xrTableRow143.StylePriority.UseBackColor = false;
      this.xrTableRow143.StylePriority.UseBorderColor = false;
      this.xrTableRow143.StylePriority.UseBorders = false;
      this.xrTableRow143.StylePriority.UseFont = false;
      this.xrTableRow143.StylePriority.UseForeColor = false;
      this.xrTableRow143.StylePriority.UsePadding = false;
      this.xrTableRow143.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell360
      // 
      resources.ApplyResources(this.xrTableCell360, "xrTableCell360");
      this.xrTableCell360.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell360.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPaymentHeader.Company")});
      this.xrTableCell360.Name = "xrTableCell360";
      this.xrTableCell360.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell360.StylePriority.UseBackColor = false;
      this.xrTableCell360.StylePriority.UseBorderColor = false;
      this.xrTableCell360.StylePriority.UseBorders = false;
      this.xrTableCell360.StylePriority.UseFont = false;
      this.xrTableCell360.StylePriority.UseForeColor = false;
      this.xrTableCell360.StylePriority.UsePadding = false;
      this.xrTableCell360.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell361
      // 
      resources.ApplyResources(this.xrTableCell361, "xrTableCell361");
      this.xrTableCell361.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell361.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPaymentHeader.M01")});
      this.xrTableCell361.Name = "xrTableCell361";
      this.xrTableCell361.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell361.StylePriority.UseBackColor = false;
      this.xrTableCell361.StylePriority.UseBorderColor = false;
      this.xrTableCell361.StylePriority.UseBorders = false;
      this.xrTableCell361.StylePriority.UseFont = false;
      this.xrTableCell361.StylePriority.UseForeColor = false;
      this.xrTableCell361.StylePriority.UsePadding = false;
      this.xrTableCell361.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell362
      // 
      resources.ApplyResources(this.xrTableCell362, "xrTableCell362");
      this.xrTableCell362.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell362.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPaymentHeader.M02")});
      this.xrTableCell362.Name = "xrTableCell362";
      this.xrTableCell362.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell362.StylePriority.UseBackColor = false;
      this.xrTableCell362.StylePriority.UseBorderColor = false;
      this.xrTableCell362.StylePriority.UseBorders = false;
      this.xrTableCell362.StylePriority.UseFont = false;
      this.xrTableCell362.StylePriority.UseForeColor = false;
      this.xrTableCell362.StylePriority.UsePadding = false;
      this.xrTableCell362.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell363
      // 
      resources.ApplyResources(this.xrTableCell363, "xrTableCell363");
      this.xrTableCell363.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell363.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPaymentHeader.M03")});
      this.xrTableCell363.Name = "xrTableCell363";
      this.xrTableCell363.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell363.StylePriority.UseBackColor = false;
      this.xrTableCell363.StylePriority.UseBorderColor = false;
      this.xrTableCell363.StylePriority.UseBorders = false;
      this.xrTableCell363.StylePriority.UseFont = false;
      this.xrTableCell363.StylePriority.UseForeColor = false;
      this.xrTableCell363.StylePriority.UsePadding = false;
      this.xrTableCell363.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell364
      // 
      resources.ApplyResources(this.xrTableCell364, "xrTableCell364");
      this.xrTableCell364.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell364.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPaymentHeader.M04")});
      this.xrTableCell364.Name = "xrTableCell364";
      this.xrTableCell364.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell364.StylePriority.UseBackColor = false;
      this.xrTableCell364.StylePriority.UseBorderColor = false;
      this.xrTableCell364.StylePriority.UseBorders = false;
      this.xrTableCell364.StylePriority.UseFont = false;
      this.xrTableCell364.StylePriority.UseForeColor = false;
      this.xrTableCell364.StylePriority.UsePadding = false;
      this.xrTableCell364.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell365
      // 
      resources.ApplyResources(this.xrTableCell365, "xrTableCell365");
      this.xrTableCell365.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell365.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPaymentHeader.M05")});
      this.xrTableCell365.Name = "xrTableCell365";
      this.xrTableCell365.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell365.StylePriority.UseBackColor = false;
      this.xrTableCell365.StylePriority.UseBorderColor = false;
      this.xrTableCell365.StylePriority.UseBorders = false;
      this.xrTableCell365.StylePriority.UseFont = false;
      this.xrTableCell365.StylePriority.UseForeColor = false;
      this.xrTableCell365.StylePriority.UsePadding = false;
      this.xrTableCell365.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell366
      // 
      resources.ApplyResources(this.xrTableCell366, "xrTableCell366");
      this.xrTableCell366.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell366.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPaymentHeader.M06")});
      this.xrTableCell366.Name = "xrTableCell366";
      this.xrTableCell366.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell366.StylePriority.UseBackColor = false;
      this.xrTableCell366.StylePriority.UseBorderColor = false;
      this.xrTableCell366.StylePriority.UseBorders = false;
      this.xrTableCell366.StylePriority.UseFont = false;
      this.xrTableCell366.StylePriority.UseForeColor = false;
      this.xrTableCell366.StylePriority.UsePadding = false;
      this.xrTableCell366.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell367
      // 
      resources.ApplyResources(this.xrTableCell367, "xrTableCell367");
      this.xrTableCell367.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell367.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPaymentHeader.M07")});
      this.xrTableCell367.Name = "xrTableCell367";
      this.xrTableCell367.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell367.StylePriority.UseBackColor = false;
      this.xrTableCell367.StylePriority.UseBorderColor = false;
      this.xrTableCell367.StylePriority.UseBorders = false;
      this.xrTableCell367.StylePriority.UseFont = false;
      this.xrTableCell367.StylePriority.UseForeColor = false;
      this.xrTableCell367.StylePriority.UsePadding = false;
      this.xrTableCell367.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell368
      // 
      resources.ApplyResources(this.xrTableCell368, "xrTableCell368");
      this.xrTableCell368.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell368.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPaymentHeader.M08")});
      this.xrTableCell368.Name = "xrTableCell368";
      this.xrTableCell368.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell368.StylePriority.UseBackColor = false;
      this.xrTableCell368.StylePriority.UseBorderColor = false;
      this.xrTableCell368.StylePriority.UseBorders = false;
      this.xrTableCell368.StylePriority.UseFont = false;
      this.xrTableCell368.StylePriority.UseForeColor = false;
      this.xrTableCell368.StylePriority.UsePadding = false;
      this.xrTableCell368.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell369
      // 
      resources.ApplyResources(this.xrTableCell369, "xrTableCell369");
      this.xrTableCell369.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell369.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPaymentHeader.M09")});
      this.xrTableCell369.Name = "xrTableCell369";
      this.xrTableCell369.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell369.StylePriority.UseBackColor = false;
      this.xrTableCell369.StylePriority.UseBorderColor = false;
      this.xrTableCell369.StylePriority.UseBorders = false;
      this.xrTableCell369.StylePriority.UseFont = false;
      this.xrTableCell369.StylePriority.UseForeColor = false;
      this.xrTableCell369.StylePriority.UsePadding = false;
      this.xrTableCell369.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell370
      // 
      resources.ApplyResources(this.xrTableCell370, "xrTableCell370");
      this.xrTableCell370.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell370.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPaymentHeader.M10")});
      this.xrTableCell370.Name = "xrTableCell370";
      this.xrTableCell370.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell370.StylePriority.UseBackColor = false;
      this.xrTableCell370.StylePriority.UseBorderColor = false;
      this.xrTableCell370.StylePriority.UseBorders = false;
      this.xrTableCell370.StylePriority.UseFont = false;
      this.xrTableCell370.StylePriority.UseForeColor = false;
      this.xrTableCell370.StylePriority.UsePadding = false;
      this.xrTableCell370.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell371
      // 
      resources.ApplyResources(this.xrTableCell371, "xrTableCell371");
      this.xrTableCell371.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell371.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPaymentHeader.M11")});
      this.xrTableCell371.Name = "xrTableCell371";
      this.xrTableCell371.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell371.StylePriority.UseBackColor = false;
      this.xrTableCell371.StylePriority.UseBorderColor = false;
      this.xrTableCell371.StylePriority.UseBorders = false;
      this.xrTableCell371.StylePriority.UseFont = false;
      this.xrTableCell371.StylePriority.UseForeColor = false;
      this.xrTableCell371.StylePriority.UsePadding = false;
      this.xrTableCell371.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell372
      // 
      resources.ApplyResources(this.xrTableCell372, "xrTableCell372");
      this.xrTableCell372.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell372.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPaymentHeader.M12")});
      this.xrTableCell372.Name = "xrTableCell372";
      this.xrTableCell372.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell372.StylePriority.UseBackColor = false;
      this.xrTableCell372.StylePriority.UseBorderColor = false;
      this.xrTableCell372.StylePriority.UseBorders = false;
      this.xrTableCell372.StylePriority.UseFont = false;
      this.xrTableCell372.StylePriority.UseForeColor = false;
      this.xrTableCell372.StylePriority.UsePadding = false;
      this.xrTableCell372.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell373
      // 
      resources.ApplyResources(this.xrTableCell373, "xrTableCell373");
      this.xrTableCell373.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell373.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPaymentHeader.M13")});
      this.xrTableCell373.Name = "xrTableCell373";
      this.xrTableCell373.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell373.StylePriority.UseBackColor = false;
      this.xrTableCell373.StylePriority.UseBorderColor = false;
      this.xrTableCell373.StylePriority.UseBorders = false;
      this.xrTableCell373.StylePriority.UseFont = false;
      this.xrTableCell373.StylePriority.UseForeColor = false;
      this.xrTableCell373.StylePriority.UsePadding = false;
      this.xrTableCell373.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell374
      // 
      resources.ApplyResources(this.xrTableCell374, "xrTableCell374");
      this.xrTableCell374.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell374.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPaymentHeader.M14")});
      this.xrTableCell374.Name = "xrTableCell374";
      this.xrTableCell374.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell374.StylePriority.UseBackColor = false;
      this.xrTableCell374.StylePriority.UseBorderColor = false;
      this.xrTableCell374.StylePriority.UseBorders = false;
      this.xrTableCell374.StylePriority.UseFont = false;
      this.xrTableCell374.StylePriority.UseForeColor = false;
      this.xrTableCell374.StylePriority.UsePadding = false;
      this.xrTableCell374.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell375
      // 
      resources.ApplyResources(this.xrTableCell375, "xrTableCell375");
      this.xrTableCell375.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell375.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPaymentHeader.M15")});
      this.xrTableCell375.Name = "xrTableCell375";
      this.xrTableCell375.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell375.StylePriority.UseBackColor = false;
      this.xrTableCell375.StylePriority.UseBorderColor = false;
      this.xrTableCell375.StylePriority.UseBorders = false;
      this.xrTableCell375.StylePriority.UseFont = false;
      this.xrTableCell375.StylePriority.UseForeColor = false;
      this.xrTableCell375.StylePriority.UsePadding = false;
      this.xrTableCell375.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell376
      // 
      resources.ApplyResources(this.xrTableCell376, "xrTableCell376");
      this.xrTableCell376.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell376.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPaymentHeader.M16")});
      this.xrTableCell376.Name = "xrTableCell376";
      this.xrTableCell376.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell376.StylePriority.UseBackColor = false;
      this.xrTableCell376.StylePriority.UseBorderColor = false;
      this.xrTableCell376.StylePriority.UseBorders = false;
      this.xrTableCell376.StylePriority.UseFont = false;
      this.xrTableCell376.StylePriority.UseForeColor = false;
      this.xrTableCell376.StylePriority.UsePadding = false;
      this.xrTableCell376.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell377
      // 
      resources.ApplyResources(this.xrTableCell377, "xrTableCell377");
      this.xrTableCell377.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell377.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPaymentHeader.M17")});
      this.xrTableCell377.Name = "xrTableCell377";
      this.xrTableCell377.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell377.StylePriority.UseBackColor = false;
      this.xrTableCell377.StylePriority.UseBorderColor = false;
      this.xrTableCell377.StylePriority.UseBorders = false;
      this.xrTableCell377.StylePriority.UseFont = false;
      this.xrTableCell377.StylePriority.UseForeColor = false;
      this.xrTableCell377.StylePriority.UsePadding = false;
      this.xrTableCell377.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell378
      // 
      resources.ApplyResources(this.xrTableCell378, "xrTableCell378");
      this.xrTableCell378.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell378.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPaymentHeader.M18")});
      this.xrTableCell378.Name = "xrTableCell378";
      this.xrTableCell378.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell378.StylePriority.UseBackColor = false;
      this.xrTableCell378.StylePriority.UseBorderColor = false;
      this.xrTableCell378.StylePriority.UseBorders = false;
      this.xrTableCell378.StylePriority.UseFont = false;
      this.xrTableCell378.StylePriority.UseForeColor = false;
      this.xrTableCell378.StylePriority.UsePadding = false;
      this.xrTableCell378.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell379
      // 
      resources.ApplyResources(this.xrTableCell379, "xrTableCell379");
      this.xrTableCell379.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell379.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPaymentHeader.M19")});
      this.xrTableCell379.Name = "xrTableCell379";
      this.xrTableCell379.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell379.StylePriority.UseBackColor = false;
      this.xrTableCell379.StylePriority.UseBorderColor = false;
      this.xrTableCell379.StylePriority.UseBorders = false;
      this.xrTableCell379.StylePriority.UseFont = false;
      this.xrTableCell379.StylePriority.UseForeColor = false;
      this.xrTableCell379.StylePriority.UsePadding = false;
      this.xrTableCell379.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell380
      // 
      resources.ApplyResources(this.xrTableCell380, "xrTableCell380");
      this.xrTableCell380.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell380.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPaymentHeader.M20")});
      this.xrTableCell380.Name = "xrTableCell380";
      this.xrTableCell380.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell380.StylePriority.UseBackColor = false;
      this.xrTableCell380.StylePriority.UseBorderColor = false;
      this.xrTableCell380.StylePriority.UseBorders = false;
      this.xrTableCell380.StylePriority.UseFont = false;
      this.xrTableCell380.StylePriority.UseForeColor = false;
      this.xrTableCell380.StylePriority.UsePadding = false;
      this.xrTableCell380.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell381
      // 
      resources.ApplyResources(this.xrTableCell381, "xrTableCell381");
      this.xrTableCell381.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell381.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPaymentHeader.M21")});
      this.xrTableCell381.Name = "xrTableCell381";
      this.xrTableCell381.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell381.StylePriority.UseBackColor = false;
      this.xrTableCell381.StylePriority.UseBorderColor = false;
      this.xrTableCell381.StylePriority.UseBorders = false;
      this.xrTableCell381.StylePriority.UseFont = false;
      this.xrTableCell381.StylePriority.UseForeColor = false;
      this.xrTableCell381.StylePriority.UsePadding = false;
      this.xrTableCell381.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell382
      // 
      resources.ApplyResources(this.xrTableCell382, "xrTableCell382");
      this.xrTableCell382.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell382.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPaymentHeader.M22")});
      this.xrTableCell382.Name = "xrTableCell382";
      this.xrTableCell382.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell382.StylePriority.UseBackColor = false;
      this.xrTableCell382.StylePriority.UseBorderColor = false;
      this.xrTableCell382.StylePriority.UseBorders = false;
      this.xrTableCell382.StylePriority.UseFont = false;
      this.xrTableCell382.StylePriority.UseForeColor = false;
      this.xrTableCell382.StylePriority.UsePadding = false;
      this.xrTableCell382.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell383
      // 
      resources.ApplyResources(this.xrTableCell383, "xrTableCell383");
      this.xrTableCell383.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell383.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPaymentHeader.M23")});
      this.xrTableCell383.Name = "xrTableCell383";
      this.xrTableCell383.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell383.StylePriority.UseBackColor = false;
      this.xrTableCell383.StylePriority.UseBorderColor = false;
      this.xrTableCell383.StylePriority.UseBorders = false;
      this.xrTableCell383.StylePriority.UseFont = false;
      this.xrTableCell383.StylePriority.UseForeColor = false;
      this.xrTableCell383.StylePriority.UsePadding = false;
      this.xrTableCell383.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell384
      // 
      resources.ApplyResources(this.xrTableCell384, "xrTableCell384");
      this.xrTableCell384.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell384.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPaymentHeader.M24")});
      this.xrTableCell384.Name = "xrTableCell384";
      this.xrTableCell384.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell384.StylePriority.UseBackColor = false;
      this.xrTableCell384.StylePriority.UseBorderColor = false;
      this.xrTableCell384.StylePriority.UseBorders = false;
      this.xrTableCell384.StylePriority.UseFont = false;
      this.xrTableCell384.StylePriority.UseForeColor = false;
      this.xrTableCell384.StylePriority.UsePadding = false;
      this.xrTableCell384.StylePriority.UseTextAlignment = false;
      // 
      // DetailReport7
      // 
      this.DetailReport7.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail24});
      this.DetailReport7.DataMember = "ConsumerNLR24MonthlyPayment";
      resources.ApplyResources(this.DetailReport7, "DetailReport7");
      this.DetailReport7.Level = 3;
      this.DetailReport7.Name = "DetailReport7";
      // 
      // Detail24
      // 
      this.Detail24.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable31});
      resources.ApplyResources(this.Detail24, "Detail24");
      this.Detail24.Name = "Detail24";
      // 
      // xrTable31
      // 
      resources.ApplyResources(this.xrTable31, "xrTable31");
      this.xrTable31.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTable31.Name = "xrTable31";
      this.xrTable31.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable31.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow144});
      this.xrTable31.StylePriority.UseBorderColor = false;
      this.xrTable31.StylePriority.UseBorders = false;
      this.xrTable31.StylePriority.UseFont = false;
      this.xrTable31.StylePriority.UsePadding = false;
      this.xrTable31.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow144
      // 
      resources.ApplyResources(this.xrTableRow144, "xrTableRow144");
      this.xrTableRow144.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableRow144.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell385,
            this.xrTableCell386,
            this.xrTableCell387,
            this.xrTableCell388,
            this.xrTableCell389,
            this.xrTableCell390,
            this.xrTableCell391,
            this.xrTableCell392,
            this.xrTableCell393,
            this.xrTableCell394,
            this.xrTableCell395,
            this.xrTableCell396,
            this.xrTableCell397,
            this.xrTableCell398,
            this.xrTableCell399,
            this.xrTableCell400,
            this.xrTableCell401,
            this.xrTableCell402,
            this.xrTableCell403,
            this.xrTableCell404,
            this.xrTableCell405,
            this.xrTableCell406,
            this.xrTableCell407,
            this.xrTableCell408,
            this.xrTableCell409});
      this.xrTableRow144.Name = "xrTableRow144";
      this.xrTableRow144.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
      this.xrTableRow144.StylePriority.UseBackColor = false;
      this.xrTableRow144.StylePriority.UseBorderColor = false;
      this.xrTableRow144.StylePriority.UseBorders = false;
      this.xrTableRow144.StylePriority.UseFont = false;
      this.xrTableRow144.StylePriority.UseForeColor = false;
      this.xrTableRow144.StylePriority.UsePadding = false;
      this.xrTableRow144.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell385
      // 
      resources.ApplyResources(this.xrTableCell385, "xrTableCell385");
      this.xrTableCell385.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPayment.SubscriberName")});
      this.xrTableCell385.Name = "xrTableCell385";
      this.xrTableCell385.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell385.StylePriority.UseBackColor = false;
      this.xrTableCell385.StylePriority.UseBorderColor = false;
      this.xrTableCell385.StylePriority.UseFont = false;
      this.xrTableCell385.StylePriority.UseForeColor = false;
      this.xrTableCell385.StylePriority.UsePadding = false;
      this.xrTableCell385.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell386
      // 
      resources.ApplyResources(this.xrTableCell386, "xrTableCell386");
      this.xrTableCell386.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPayment.M01")});
      this.xrTableCell386.Name = "xrTableCell386";
      this.xrTableCell386.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell386.StylePriority.UseBackColor = false;
      this.xrTableCell386.StylePriority.UseBorderColor = false;
      this.xrTableCell386.StylePriority.UseFont = false;
      this.xrTableCell386.StylePriority.UseForeColor = false;
      this.xrTableCell386.StylePriority.UsePadding = false;
      this.xrTableCell386.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell387
      // 
      resources.ApplyResources(this.xrTableCell387, "xrTableCell387");
      this.xrTableCell387.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPayment.M02")});
      this.xrTableCell387.Name = "xrTableCell387";
      this.xrTableCell387.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell387.StylePriority.UseBackColor = false;
      this.xrTableCell387.StylePriority.UseBorderColor = false;
      this.xrTableCell387.StylePriority.UseFont = false;
      this.xrTableCell387.StylePriority.UseForeColor = false;
      this.xrTableCell387.StylePriority.UsePadding = false;
      this.xrTableCell387.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell388
      // 
      resources.ApplyResources(this.xrTableCell388, "xrTableCell388");
      this.xrTableCell388.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPayment.M03")});
      this.xrTableCell388.Name = "xrTableCell388";
      this.xrTableCell388.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell388.StylePriority.UseBackColor = false;
      this.xrTableCell388.StylePriority.UseBorderColor = false;
      this.xrTableCell388.StylePriority.UseFont = false;
      this.xrTableCell388.StylePriority.UseForeColor = false;
      this.xrTableCell388.StylePriority.UsePadding = false;
      this.xrTableCell388.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell389
      // 
      resources.ApplyResources(this.xrTableCell389, "xrTableCell389");
      this.xrTableCell389.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPayment.M04")});
      this.xrTableCell389.Name = "xrTableCell389";
      this.xrTableCell389.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell389.StylePriority.UseBackColor = false;
      this.xrTableCell389.StylePriority.UseBorderColor = false;
      this.xrTableCell389.StylePriority.UseFont = false;
      this.xrTableCell389.StylePriority.UseForeColor = false;
      this.xrTableCell389.StylePriority.UsePadding = false;
      this.xrTableCell389.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell390
      // 
      resources.ApplyResources(this.xrTableCell390, "xrTableCell390");
      this.xrTableCell390.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPayment.M05")});
      this.xrTableCell390.Name = "xrTableCell390";
      this.xrTableCell390.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell390.StylePriority.UseBackColor = false;
      this.xrTableCell390.StylePriority.UseBorderColor = false;
      this.xrTableCell390.StylePriority.UseFont = false;
      this.xrTableCell390.StylePriority.UseForeColor = false;
      this.xrTableCell390.StylePriority.UsePadding = false;
      this.xrTableCell390.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell391
      // 
      resources.ApplyResources(this.xrTableCell391, "xrTableCell391");
      this.xrTableCell391.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPayment.M06")});
      this.xrTableCell391.Name = "xrTableCell391";
      this.xrTableCell391.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell391.StylePriority.UseBackColor = false;
      this.xrTableCell391.StylePriority.UseBorderColor = false;
      this.xrTableCell391.StylePriority.UseFont = false;
      this.xrTableCell391.StylePriority.UseForeColor = false;
      this.xrTableCell391.StylePriority.UsePadding = false;
      this.xrTableCell391.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell392
      // 
      resources.ApplyResources(this.xrTableCell392, "xrTableCell392");
      this.xrTableCell392.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPayment.M07")});
      this.xrTableCell392.Name = "xrTableCell392";
      this.xrTableCell392.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell392.StylePriority.UseBackColor = false;
      this.xrTableCell392.StylePriority.UseBorderColor = false;
      this.xrTableCell392.StylePriority.UseFont = false;
      this.xrTableCell392.StylePriority.UseForeColor = false;
      this.xrTableCell392.StylePriority.UsePadding = false;
      this.xrTableCell392.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell393
      // 
      resources.ApplyResources(this.xrTableCell393, "xrTableCell393");
      this.xrTableCell393.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPayment.M08")});
      this.xrTableCell393.Name = "xrTableCell393";
      this.xrTableCell393.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell393.StylePriority.UseBackColor = false;
      this.xrTableCell393.StylePriority.UseBorderColor = false;
      this.xrTableCell393.StylePriority.UseFont = false;
      this.xrTableCell393.StylePriority.UseForeColor = false;
      this.xrTableCell393.StylePriority.UsePadding = false;
      this.xrTableCell393.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell394
      // 
      resources.ApplyResources(this.xrTableCell394, "xrTableCell394");
      this.xrTableCell394.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPayment.M09")});
      this.xrTableCell394.Name = "xrTableCell394";
      this.xrTableCell394.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell394.StylePriority.UseBackColor = false;
      this.xrTableCell394.StylePriority.UseBorderColor = false;
      this.xrTableCell394.StylePriority.UseFont = false;
      this.xrTableCell394.StylePriority.UseForeColor = false;
      this.xrTableCell394.StylePriority.UsePadding = false;
      this.xrTableCell394.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell395
      // 
      resources.ApplyResources(this.xrTableCell395, "xrTableCell395");
      this.xrTableCell395.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPayment.M10")});
      this.xrTableCell395.Name = "xrTableCell395";
      this.xrTableCell395.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell395.StylePriority.UseBackColor = false;
      this.xrTableCell395.StylePriority.UseBorderColor = false;
      this.xrTableCell395.StylePriority.UseFont = false;
      this.xrTableCell395.StylePriority.UseForeColor = false;
      this.xrTableCell395.StylePriority.UsePadding = false;
      this.xrTableCell395.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell396
      // 
      resources.ApplyResources(this.xrTableCell396, "xrTableCell396");
      this.xrTableCell396.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPayment.M11")});
      this.xrTableCell396.Name = "xrTableCell396";
      this.xrTableCell396.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell396.StylePriority.UseBackColor = false;
      this.xrTableCell396.StylePriority.UseBorderColor = false;
      this.xrTableCell396.StylePriority.UseFont = false;
      this.xrTableCell396.StylePriority.UseForeColor = false;
      this.xrTableCell396.StylePriority.UsePadding = false;
      this.xrTableCell396.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell397
      // 
      resources.ApplyResources(this.xrTableCell397, "xrTableCell397");
      this.xrTableCell397.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPayment.M12")});
      this.xrTableCell397.Name = "xrTableCell397";
      this.xrTableCell397.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell397.StylePriority.UseBackColor = false;
      this.xrTableCell397.StylePriority.UseBorderColor = false;
      this.xrTableCell397.StylePriority.UseFont = false;
      this.xrTableCell397.StylePriority.UseForeColor = false;
      this.xrTableCell397.StylePriority.UsePadding = false;
      this.xrTableCell397.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell398
      // 
      resources.ApplyResources(this.xrTableCell398, "xrTableCell398");
      this.xrTableCell398.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPayment.M13")});
      this.xrTableCell398.Name = "xrTableCell398";
      this.xrTableCell398.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell398.StylePriority.UseBackColor = false;
      this.xrTableCell398.StylePriority.UseBorderColor = false;
      this.xrTableCell398.StylePriority.UseFont = false;
      this.xrTableCell398.StylePriority.UseForeColor = false;
      this.xrTableCell398.StylePriority.UsePadding = false;
      this.xrTableCell398.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell399
      // 
      resources.ApplyResources(this.xrTableCell399, "xrTableCell399");
      this.xrTableCell399.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPayment.M14")});
      this.xrTableCell399.Name = "xrTableCell399";
      this.xrTableCell399.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell399.StylePriority.UseBackColor = false;
      this.xrTableCell399.StylePriority.UseBorderColor = false;
      this.xrTableCell399.StylePriority.UseFont = false;
      this.xrTableCell399.StylePriority.UseForeColor = false;
      this.xrTableCell399.StylePriority.UsePadding = false;
      this.xrTableCell399.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell400
      // 
      resources.ApplyResources(this.xrTableCell400, "xrTableCell400");
      this.xrTableCell400.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPayment.M15")});
      this.xrTableCell400.Name = "xrTableCell400";
      this.xrTableCell400.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell400.StylePriority.UseBackColor = false;
      this.xrTableCell400.StylePriority.UseBorderColor = false;
      this.xrTableCell400.StylePriority.UseFont = false;
      this.xrTableCell400.StylePriority.UseForeColor = false;
      this.xrTableCell400.StylePriority.UsePadding = false;
      this.xrTableCell400.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell401
      // 
      resources.ApplyResources(this.xrTableCell401, "xrTableCell401");
      this.xrTableCell401.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPayment.M16")});
      this.xrTableCell401.Name = "xrTableCell401";
      this.xrTableCell401.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell401.StylePriority.UseBackColor = false;
      this.xrTableCell401.StylePriority.UseBorderColor = false;
      this.xrTableCell401.StylePriority.UseFont = false;
      this.xrTableCell401.StylePriority.UseForeColor = false;
      this.xrTableCell401.StylePriority.UsePadding = false;
      this.xrTableCell401.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell402
      // 
      resources.ApplyResources(this.xrTableCell402, "xrTableCell402");
      this.xrTableCell402.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPayment.M17")});
      this.xrTableCell402.Name = "xrTableCell402";
      this.xrTableCell402.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell402.StylePriority.UseBackColor = false;
      this.xrTableCell402.StylePriority.UseBorderColor = false;
      this.xrTableCell402.StylePriority.UseFont = false;
      this.xrTableCell402.StylePriority.UseForeColor = false;
      this.xrTableCell402.StylePriority.UsePadding = false;
      this.xrTableCell402.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell403
      // 
      resources.ApplyResources(this.xrTableCell403, "xrTableCell403");
      this.xrTableCell403.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPayment.M18")});
      this.xrTableCell403.Name = "xrTableCell403";
      this.xrTableCell403.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell403.StylePriority.UseBackColor = false;
      this.xrTableCell403.StylePriority.UseBorderColor = false;
      this.xrTableCell403.StylePriority.UseFont = false;
      this.xrTableCell403.StylePriority.UseForeColor = false;
      this.xrTableCell403.StylePriority.UsePadding = false;
      this.xrTableCell403.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell404
      // 
      resources.ApplyResources(this.xrTableCell404, "xrTableCell404");
      this.xrTableCell404.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPayment.M19")});
      this.xrTableCell404.Name = "xrTableCell404";
      this.xrTableCell404.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell404.StylePriority.UseBackColor = false;
      this.xrTableCell404.StylePriority.UseBorderColor = false;
      this.xrTableCell404.StylePriority.UseFont = false;
      this.xrTableCell404.StylePriority.UseForeColor = false;
      this.xrTableCell404.StylePriority.UsePadding = false;
      this.xrTableCell404.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell405
      // 
      resources.ApplyResources(this.xrTableCell405, "xrTableCell405");
      this.xrTableCell405.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPayment.M20")});
      this.xrTableCell405.Name = "xrTableCell405";
      this.xrTableCell405.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell405.StylePriority.UseBackColor = false;
      this.xrTableCell405.StylePriority.UseBorderColor = false;
      this.xrTableCell405.StylePriority.UseFont = false;
      this.xrTableCell405.StylePriority.UseForeColor = false;
      this.xrTableCell405.StylePriority.UsePadding = false;
      this.xrTableCell405.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell406
      // 
      resources.ApplyResources(this.xrTableCell406, "xrTableCell406");
      this.xrTableCell406.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPayment.M21")});
      this.xrTableCell406.Name = "xrTableCell406";
      this.xrTableCell406.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell406.StylePriority.UseBackColor = false;
      this.xrTableCell406.StylePriority.UseBorderColor = false;
      this.xrTableCell406.StylePriority.UseFont = false;
      this.xrTableCell406.StylePriority.UseForeColor = false;
      this.xrTableCell406.StylePriority.UsePadding = false;
      this.xrTableCell406.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell407
      // 
      resources.ApplyResources(this.xrTableCell407, "xrTableCell407");
      this.xrTableCell407.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPayment.M22")});
      this.xrTableCell407.Name = "xrTableCell407";
      this.xrTableCell407.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell407.StylePriority.UseBackColor = false;
      this.xrTableCell407.StylePriority.UseBorderColor = false;
      this.xrTableCell407.StylePriority.UseFont = false;
      this.xrTableCell407.StylePriority.UseForeColor = false;
      this.xrTableCell407.StylePriority.UsePadding = false;
      this.xrTableCell407.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell408
      // 
      resources.ApplyResources(this.xrTableCell408, "xrTableCell408");
      this.xrTableCell408.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPayment.M23")});
      this.xrTableCell408.Name = "xrTableCell408";
      this.xrTableCell408.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell408.StylePriority.UseBackColor = false;
      this.xrTableCell408.StylePriority.UseBorderColor = false;
      this.xrTableCell408.StylePriority.UseFont = false;
      this.xrTableCell408.StylePriority.UseForeColor = false;
      this.xrTableCell408.StylePriority.UsePadding = false;
      this.xrTableCell408.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell409
      // 
      resources.ApplyResources(this.xrTableCell409, "xrTableCell409");
      this.xrTableCell409.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell409.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLR24MonthlyPayment.M24")});
      this.xrTableCell409.Name = "xrTableCell409";
      this.xrTableCell409.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 254F);
      this.xrTableCell409.StylePriority.UseBackColor = false;
      this.xrTableCell409.StylePriority.UseBorderColor = false;
      this.xrTableCell409.StylePriority.UseBorders = false;
      this.xrTableCell409.StylePriority.UseFont = false;
      this.xrTableCell409.StylePriority.UseForeColor = false;
      this.xrTableCell409.StylePriority.UsePadding = false;
      this.xrTableCell409.StylePriority.UseTextAlignment = false;
      // 
      // DetailReport8
      // 
      this.DetailReport8.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail25,
            this.GroupHeader13});
      this.DetailReport8.DataMember = "ConsumerNLRDefinition";
      resources.ApplyResources(this.DetailReport8, "DetailReport8");
      this.DetailReport8.Level = 4;
      this.DetailReport8.Name = "DetailReport8";
      // 
      // Detail25
      // 
      this.Detail25.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable33});
      resources.ApplyResources(this.Detail25, "Detail25");
      this.Detail25.Name = "Detail25";
      // 
      // xrTable33
      // 
      this.xrTable33.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
      resources.ApplyResources(this.xrTable33, "xrTable33");
      this.xrTable33.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTable33.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Tag", null, "ConsumerNLRDefinition.DefinitionCode")});
      this.xrTable33.Name = "xrTable33";
      this.xrTable33.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable33.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow146});
      this.xrTable33.StylePriority.UseBackColor = false;
      this.xrTable33.StylePriority.UseBorderColor = false;
      this.xrTable33.StylePriority.UseBorders = false;
      this.xrTable33.StylePriority.UseFont = false;
      this.xrTable33.StylePriority.UsePadding = false;
      // 
      // xrTableRow146
      // 
      resources.ApplyResources(this.xrTableRow146, "xrTableRow146");
      this.xrTableRow146.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableRow146.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell412,
            this.xrTableCell413});
      this.xrTableRow146.Name = "xrTableRow146";
      this.xrTableRow146.StylePriority.UseBackColor = false;
      this.xrTableRow146.StylePriority.UseBorderColor = false;
      this.xrTableRow146.StylePriority.UseBorders = false;
      this.xrTableRow146.StylePriority.UseFont = false;
      this.xrTableRow146.StylePriority.UseForeColor = false;
      // 
      // xrTableCell412
      // 
      resources.ApplyResources(this.xrTableCell412, "xrTableCell412");
      this.xrTableCell412.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell412.CanGrow = false;
      this.xrTableCell412.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Tag", null, "ConsumerNLRDefinition.DefinitionCode"),
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLRDefinition.DefinitionDesc")});
      this.xrTableCell412.Name = "xrTableCell412";
      this.xrTableCell412.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell412.StylePriority.UseBackColor = false;
      this.xrTableCell412.StylePriority.UseBorderColor = false;
      this.xrTableCell412.StylePriority.UseBorders = false;
      this.xrTableCell412.StylePriority.UseFont = false;
      this.xrTableCell412.StylePriority.UseForeColor = false;
      this.xrTableCell412.StylePriority.UsePadding = false;
      this.xrTableCell412.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell413
      // 
      resources.ApplyResources(this.xrTableCell413, "xrTableCell413");
      this.xrTableCell413.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell413.CanGrow = false;
      this.xrTableCell413.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Tag", null, "ConsumerNLRDefinition.DefinitionDesc"),
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLRDefinition.DefinitionCode")});
      this.xrTableCell413.Name = "xrTableCell413";
      this.xrTableCell413.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell413.StylePriority.UseBackColor = false;
      this.xrTableCell413.StylePriority.UseBorderColor = false;
      this.xrTableCell413.StylePriority.UseBorders = false;
      this.xrTableCell413.StylePriority.UseFont = false;
      this.xrTableCell413.StylePriority.UseForeColor = false;
      this.xrTableCell413.StylePriority.UsePadding = false;
      this.xrTableCell413.StylePriority.UseTextAlignment = false;
      // 
      // GroupHeader13
      // 
      this.GroupHeader13.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable32});
      resources.ApplyResources(this.GroupHeader13, "GroupHeader13");
      this.GroupHeader13.Name = "GroupHeader13";
      // 
      // xrTable32
      // 
      this.xrTable32.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
      resources.ApplyResources(this.xrTable32, "xrTable32");
      this.xrTable32.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTable32.Name = "xrTable32";
      this.xrTable32.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable32.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow145});
      this.xrTable32.StylePriority.UseBackColor = false;
      this.xrTable32.StylePriority.UseBorderColor = false;
      this.xrTable32.StylePriority.UseBorders = false;
      this.xrTable32.StylePriority.UseFont = false;
      this.xrTable32.StylePriority.UsePadding = false;
      // 
      // xrTableRow145
      // 
      this.xrTableRow145.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell410,
            this.xrTableCell411});
      resources.ApplyResources(this.xrTableRow145, "xrTableRow145");
      this.xrTableRow145.Name = "xrTableRow145";
      this.xrTableRow145.StylePriority.UseFont = false;
      this.xrTableRow145.StylePriority.UseForeColor = false;
      // 
      // xrTableCell410
      // 
      resources.ApplyResources(this.xrTableCell410, "xrTableCell410");
      this.xrTableCell410.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell410.CanGrow = false;
      this.xrTableCell410.Name = "xrTableCell410";
      this.xrTableCell410.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell410.StylePriority.UseBorderColor = false;
      this.xrTableCell410.StylePriority.UseBorders = false;
      this.xrTableCell410.StylePriority.UseFont = false;
      this.xrTableCell410.StylePriority.UseForeColor = false;
      this.xrTableCell410.StylePriority.UsePadding = false;
      this.xrTableCell410.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell411
      // 
      resources.ApplyResources(this.xrTableCell411, "xrTableCell411");
      this.xrTableCell411.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell411.CanGrow = false;
      this.xrTableCell411.Name = "xrTableCell411";
      this.xrTableCell411.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell411.StylePriority.UseBorderColor = false;
      this.xrTableCell411.StylePriority.UseBorders = false;
      this.xrTableCell411.StylePriority.UseFont = false;
      this.xrTableCell411.StylePriority.UseForeColor = false;
      this.xrTableCell411.StylePriority.UsePadding = false;
      this.xrTableCell411.StylePriority.UseTextAlignment = false;
      // 
      // DetailReport11
      // 
      this.DetailReport11.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail7,
            this.GroupHeader7});
      this.DetailReport11.DataMember = "ConsumerNLRAccountStatus";
      resources.ApplyResources(this.DetailReport11, "DetailReport11");
      this.DetailReport11.Level = 0;
      this.DetailReport11.Name = "DetailReport11";
      // 
      // Detail7
      // 
      this.Detail7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable21});
      resources.ApplyResources(this.Detail7, "Detail7");
      this.Detail7.Name = "Detail7";
      // 
      // xrTable21
      // 
      resources.ApplyResources(this.xrTable21, "xrTable21");
      this.xrTable21.Name = "xrTable21";
      this.xrTable21.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable21.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow154});
      this.xrTable21.StylePriority.UseBackColor = false;
      this.xrTable21.StylePriority.UseBorderColor = false;
      this.xrTable21.StylePriority.UseFont = false;
      this.xrTable21.StylePriority.UsePadding = false;
      // 
      // xrTableRow154
      // 
      resources.ApplyResources(this.xrTableRow154, "xrTableRow154");
      this.xrTableRow154.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableRow154.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell316,
            this.xrTableCell317,
            this.xrTableCell507,
            this.xrTableCell525,
            this.xrTableCell526,
            this.xrTableCell527,
            this.xrTableCell528,
            this.xrTableCell529,
            this.xrTableCell530,
            this.xrTableCell531});
      this.xrTableRow154.Name = "xrTableRow154";
      this.xrTableRow154.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTableRow154.StylePriority.UseBackColor = false;
      this.xrTableRow154.StylePriority.UseBorderColor = false;
      this.xrTableRow154.StylePriority.UseBorders = false;
      this.xrTableRow154.StylePriority.UseFont = false;
      this.xrTableRow154.StylePriority.UseForeColor = false;
      this.xrTableRow154.StylePriority.UsePadding = false;
      this.xrTableRow154.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell316
      // 
      resources.ApplyResources(this.xrTableCell316, "xrTableCell316");
      this.xrTableCell316.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell316.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLRAccountStatus.AccountOpenedDate")});
      this.xrTableCell316.Name = "xrTableCell316";
      this.xrTableCell316.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell316.StylePriority.UseBackColor = false;
      this.xrTableCell316.StylePriority.UseBorderColor = false;
      this.xrTableCell316.StylePriority.UseBorders = false;
      this.xrTableCell316.StylePriority.UseFont = false;
      this.xrTableCell316.StylePriority.UseForeColor = false;
      this.xrTableCell316.StylePriority.UsePadding = false;
      this.xrTableCell316.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell317
      // 
      resources.ApplyResources(this.xrTableCell317, "xrTableCell317");
      this.xrTableCell317.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell317.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLRAccountStatus.SubscriberName")});
      this.xrTableCell317.Name = "xrTableCell317";
      this.xrTableCell317.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell317.StylePriority.UseBackColor = false;
      this.xrTableCell317.StylePriority.UseBorderColor = false;
      this.xrTableCell317.StylePriority.UseBorders = false;
      this.xrTableCell317.StylePriority.UseFont = false;
      this.xrTableCell317.StylePriority.UseForeColor = false;
      this.xrTableCell317.StylePriority.UsePadding = false;
      this.xrTableCell317.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell507
      // 
      resources.ApplyResources(this.xrTableCell507, "xrTableCell507");
      this.xrTableCell507.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLRAccountStatus.AccountNo")});
      this.xrTableCell507.Name = "xrTableCell507";
      this.xrTableCell507.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell507.StylePriority.UseBackColor = false;
      this.xrTableCell507.StylePriority.UseBorderColor = false;
      this.xrTableCell507.StylePriority.UseFont = false;
      this.xrTableCell507.StylePriority.UseForeColor = false;
      this.xrTableCell507.StylePriority.UsePadding = false;
      this.xrTableCell507.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell525
      // 
      resources.ApplyResources(this.xrTableCell525, "xrTableCell525");
      this.xrTableCell525.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell525.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLRAccountStatus.CreditLimitAmt", "{0:c2}")});
      this.xrTableCell525.Name = "xrTableCell525";
      this.xrTableCell525.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell525.StylePriority.UseBackColor = false;
      this.xrTableCell525.StylePriority.UseBorderColor = false;
      this.xrTableCell525.StylePriority.UseBorders = false;
      this.xrTableCell525.StylePriority.UseFont = false;
      this.xrTableCell525.StylePriority.UseForeColor = false;
      this.xrTableCell525.StylePriority.UsePadding = false;
      this.xrTableCell525.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell526
      // 
      resources.ApplyResources(this.xrTableCell526, "xrTableCell526");
      this.xrTableCell526.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell526.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLRAccountStatus.CurrentBalanceAmt", "{0:c2}")});
      this.xrTableCell526.Name = "xrTableCell526";
      this.xrTableCell526.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell526.StylePriority.UseBackColor = false;
      this.xrTableCell526.StylePriority.UseBorderColor = false;
      this.xrTableCell526.StylePriority.UseBorders = false;
      this.xrTableCell526.StylePriority.UseFont = false;
      this.xrTableCell526.StylePriority.UseForeColor = false;
      this.xrTableCell526.StylePriority.UsePadding = false;
      this.xrTableCell526.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell527
      // 
      resources.ApplyResources(this.xrTableCell527, "xrTableCell527");
      this.xrTableCell527.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell527.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLRAccountStatus.MonthlyInstalmentAmt", "{0:c2}")});
      this.xrTableCell527.Name = "xrTableCell527";
      this.xrTableCell527.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell527.StylePriority.UseBackColor = false;
      this.xrTableCell527.StylePriority.UseBorderColor = false;
      this.xrTableCell527.StylePriority.UseBorders = false;
      this.xrTableCell527.StylePriority.UseFont = false;
      this.xrTableCell527.StylePriority.UseForeColor = false;
      this.xrTableCell527.StylePriority.UsePadding = false;
      this.xrTableCell527.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell528
      // 
      resources.ApplyResources(this.xrTableCell528, "xrTableCell528");
      this.xrTableCell528.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell528.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLRAccountStatus.ArrearsAmt", "{0:c2}")});
      this.xrTableCell528.Name = "xrTableCell528";
      this.xrTableCell528.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell528.StylePriority.UseBackColor = false;
      this.xrTableCell528.StylePriority.UseBorderColor = false;
      this.xrTableCell528.StylePriority.UseBorders = false;
      this.xrTableCell528.StylePriority.UseFont = false;
      this.xrTableCell528.StylePriority.UseForeColor = false;
      this.xrTableCell528.StylePriority.UsePadding = false;
      this.xrTableCell528.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell529
      // 
      resources.ApplyResources(this.xrTableCell529, "xrTableCell529");
      this.xrTableCell529.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell529.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLRAccountStatus.AccountType")});
      this.xrTableCell529.Name = "xrTableCell529";
      this.xrTableCell529.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell529.StylePriority.UseBackColor = false;
      this.xrTableCell529.StylePriority.UseBorderColor = false;
      this.xrTableCell529.StylePriority.UseBorders = false;
      this.xrTableCell529.StylePriority.UseFont = false;
      this.xrTableCell529.StylePriority.UseForeColor = false;
      this.xrTableCell529.StylePriority.UsePadding = false;
      this.xrTableCell529.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell530
      // 
      resources.ApplyResources(this.xrTableCell530, "xrTableCell530");
      this.xrTableCell530.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLRAccountStatus.StatusCodeDesc")});
      this.xrTableCell530.Name = "xrTableCell530";
      this.xrTableCell530.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell530.StylePriority.UseBackColor = false;
      this.xrTableCell530.StylePriority.UseBorderColor = false;
      this.xrTableCell530.StylePriority.UseFont = false;
      this.xrTableCell530.StylePriority.UseForeColor = false;
      this.xrTableCell530.StylePriority.UsePadding = false;
      this.xrTableCell530.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell531
      // 
      resources.ApplyResources(this.xrTableCell531, "xrTableCell531");
      this.xrTableCell531.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerNLRAccountStatus.LastPaymentDate")});
      this.xrTableCell531.Name = "xrTableCell531";
      this.xrTableCell531.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell531.StylePriority.UseBackColor = false;
      this.xrTableCell531.StylePriority.UseBorderColor = false;
      this.xrTableCell531.StylePriority.UseFont = false;
      this.xrTableCell531.StylePriority.UseForeColor = false;
      this.xrTableCell531.StylePriority.UsePadding = false;
      this.xrTableCell531.StylePriority.UseTextAlignment = false;
      // 
      // GroupHeader7
      // 
      this.GroupHeader7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable19});
      resources.ApplyResources(this.GroupHeader7, "GroupHeader7");
      this.GroupHeader7.Name = "GroupHeader7";
      // 
      // xrTable19
      // 
      this.xrTable19.Borders = DevExpress.XtraPrinting.BorderSide.None;
      resources.ApplyResources(this.xrTable19, "xrTable19");
      this.xrTable19.KeepTogether = true;
      this.xrTable19.Name = "xrTable19";
      this.xrTable19.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 0, 0, 0, 254F);
      this.xrTable19.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow70,
            this.xrTableRow127,
            this.xrTableRow129,
            this.xrTableRow130,
            this.xrTableRow131});
      this.xrTable19.StylePriority.UseBorders = false;
      this.xrTable19.StylePriority.UsePadding = false;
      // 
      // xrTableRow70
      // 
      this.xrTableRow70.Borders = DevExpress.XtraPrinting.BorderSide.Top;
      this.xrTableRow70.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell289,
            this.xrTableCell294});
      resources.ApplyResources(this.xrTableRow70, "xrTableRow70");
      this.xrTableRow70.Name = "xrTableRow70";
      this.xrTableRow70.StylePriority.UseBorders = false;
      // 
      // xrTableCell289
      // 
      resources.ApplyResources(this.xrTableCell289, "xrTableCell289");
      this.xrTableCell289.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell289.Name = "xrTableCell289";
      this.xrTableCell289.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell289.StylePriority.UseBackColor = false;
      this.xrTableCell289.StylePriority.UseBorderColor = false;
      this.xrTableCell289.StylePriority.UseBorders = false;
      this.xrTableCell289.StylePriority.UseFont = false;
      this.xrTableCell289.StylePriority.UseForeColor = false;
      this.xrTableCell289.StylePriority.UsePadding = false;
      this.xrTableCell289.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell294
      // 
      resources.ApplyResources(this.xrTableCell294, "xrTableCell294");
      this.xrTableCell294.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell294.Name = "xrTableCell294";
      this.xrTableCell294.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell294.StylePriority.UseBackColor = false;
      this.xrTableCell294.StylePriority.UseBorderColor = false;
      this.xrTableCell294.StylePriority.UseBorders = false;
      this.xrTableCell294.StylePriority.UseFont = false;
      this.xrTableCell294.StylePriority.UseForeColor = false;
      this.xrTableCell294.StylePriority.UsePadding = false;
      this.xrTableCell294.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow127
      // 
      this.xrTableRow127.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell295});
      resources.ApplyResources(this.xrTableRow127, "xrTableRow127");
      this.xrTableRow127.Name = "xrTableRow127";
      // 
      // xrTableCell295
      // 
      resources.ApplyResources(this.xrTableCell295, "xrTableCell295");
      this.xrTableCell295.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTableCell295.Name = "xrTableCell295";
      this.xrTableCell295.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell295.StylePriority.UseBackColor = false;
      this.xrTableCell295.StylePriority.UseBorderColor = false;
      this.xrTableCell295.StylePriority.UseBorders = false;
      this.xrTableCell295.StylePriority.UseFont = false;
      this.xrTableCell295.StylePriority.UseForeColor = false;
      this.xrTableCell295.StylePriority.UsePadding = false;
      this.xrTableCell295.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow129
      // 
      this.xrTableRow129.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell296});
      resources.ApplyResources(this.xrTableRow129, "xrTableRow129");
      this.xrTableRow129.Name = "xrTableRow129";
      // 
      // xrTableCell296
      // 
      resources.ApplyResources(this.xrTableCell296, "xrTableCell296");
      this.xrTableCell296.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell296.Name = "xrTableCell296";
      this.xrTableCell296.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell296.StylePriority.UseBackColor = false;
      this.xrTableCell296.StylePriority.UseBorderColor = false;
      this.xrTableCell296.StylePriority.UseBorders = false;
      this.xrTableCell296.StylePriority.UseFont = false;
      this.xrTableCell296.StylePriority.UseForeColor = false;
      this.xrTableCell296.StylePriority.UsePadding = false;
      this.xrTableCell296.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow130
      // 
      this.xrTableRow130.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell297});
      resources.ApplyResources(this.xrTableRow130, "xrTableRow130");
      this.xrTableRow130.Name = "xrTableRow130";
      // 
      // xrTableCell297
      // 
      this.xrTableCell297.Borders = DevExpress.XtraPrinting.BorderSide.None;
      resources.ApplyResources(this.xrTableCell297, "xrTableCell297");
      this.xrTableCell297.Name = "xrTableCell297";
      this.xrTableCell297.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      this.xrTableCell297.StylePriority.UseBorders = false;
      this.xrTableCell297.StylePriority.UsePadding = false;
      // 
      // xrTableRow131
      // 
      this.xrTableRow131.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell298,
            this.xrTableCell299,
            this.xrTableCell300,
            this.xrTableCell301,
            this.xrTableCell302,
            this.xrTableCell303,
            this.xrTableCell304,
            this.xrTableCell305,
            this.xrTableCell306,
            this.xrTableCell315});
      resources.ApplyResources(this.xrTableRow131, "xrTableRow131");
      this.xrTableRow131.Name = "xrTableRow131";
      this.xrTableRow131.StylePriority.UseBackColor = false;
      this.xrTableRow131.StylePriority.UseBorderColor = false;
      this.xrTableRow131.StylePriority.UseBorders = false;
      // 
      // xrTableCell298
      // 
      resources.ApplyResources(this.xrTableCell298, "xrTableCell298");
      this.xrTableCell298.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell298.Name = "xrTableCell298";
      this.xrTableCell298.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell298.StylePriority.UseBackColor = false;
      this.xrTableCell298.StylePriority.UseBorderColor = false;
      this.xrTableCell298.StylePriority.UseBorders = false;
      this.xrTableCell298.StylePriority.UseFont = false;
      this.xrTableCell298.StylePriority.UseForeColor = false;
      this.xrTableCell298.StylePriority.UsePadding = false;
      this.xrTableCell298.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell299
      // 
      resources.ApplyResources(this.xrTableCell299, "xrTableCell299");
      this.xrTableCell299.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell299.Name = "xrTableCell299";
      this.xrTableCell299.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell299.StylePriority.UseBackColor = false;
      this.xrTableCell299.StylePriority.UseBorderColor = false;
      this.xrTableCell299.StylePriority.UseBorders = false;
      this.xrTableCell299.StylePriority.UseFont = false;
      this.xrTableCell299.StylePriority.UseForeColor = false;
      this.xrTableCell299.StylePriority.UsePadding = false;
      this.xrTableCell299.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell300
      // 
      resources.ApplyResources(this.xrTableCell300, "xrTableCell300");
      this.xrTableCell300.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell300.Name = "xrTableCell300";
      this.xrTableCell300.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell300.StylePriority.UseBackColor = false;
      this.xrTableCell300.StylePriority.UseBorderColor = false;
      this.xrTableCell300.StylePriority.UseBorders = false;
      this.xrTableCell300.StylePriority.UseFont = false;
      this.xrTableCell300.StylePriority.UseForeColor = false;
      this.xrTableCell300.StylePriority.UsePadding = false;
      this.xrTableCell300.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell301
      // 
      resources.ApplyResources(this.xrTableCell301, "xrTableCell301");
      this.xrTableCell301.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell301.Name = "xrTableCell301";
      this.xrTableCell301.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell301.StylePriority.UseBackColor = false;
      this.xrTableCell301.StylePriority.UseBorderColor = false;
      this.xrTableCell301.StylePriority.UseBorders = false;
      this.xrTableCell301.StylePriority.UseFont = false;
      this.xrTableCell301.StylePriority.UseForeColor = false;
      this.xrTableCell301.StylePriority.UsePadding = false;
      this.xrTableCell301.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell302
      // 
      resources.ApplyResources(this.xrTableCell302, "xrTableCell302");
      this.xrTableCell302.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell302.Name = "xrTableCell302";
      this.xrTableCell302.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell302.StylePriority.UseBackColor = false;
      this.xrTableCell302.StylePriority.UseBorderColor = false;
      this.xrTableCell302.StylePriority.UseBorders = false;
      this.xrTableCell302.StylePriority.UseFont = false;
      this.xrTableCell302.StylePriority.UseForeColor = false;
      this.xrTableCell302.StylePriority.UsePadding = false;
      this.xrTableCell302.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell303
      // 
      resources.ApplyResources(this.xrTableCell303, "xrTableCell303");
      this.xrTableCell303.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell303.Name = "xrTableCell303";
      this.xrTableCell303.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell303.StylePriority.UseBackColor = false;
      this.xrTableCell303.StylePriority.UseBorderColor = false;
      this.xrTableCell303.StylePriority.UseBorders = false;
      this.xrTableCell303.StylePriority.UseFont = false;
      this.xrTableCell303.StylePriority.UseForeColor = false;
      this.xrTableCell303.StylePriority.UsePadding = false;
      this.xrTableCell303.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell304
      // 
      resources.ApplyResources(this.xrTableCell304, "xrTableCell304");
      this.xrTableCell304.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell304.Name = "xrTableCell304";
      this.xrTableCell304.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell304.StylePriority.UseBackColor = false;
      this.xrTableCell304.StylePriority.UseBorderColor = false;
      this.xrTableCell304.StylePriority.UseBorders = false;
      this.xrTableCell304.StylePriority.UseFont = false;
      this.xrTableCell304.StylePriority.UseForeColor = false;
      this.xrTableCell304.StylePriority.UsePadding = false;
      this.xrTableCell304.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell305
      // 
      resources.ApplyResources(this.xrTableCell305, "xrTableCell305");
      this.xrTableCell305.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell305.Name = "xrTableCell305";
      this.xrTableCell305.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell305.StylePriority.UseBackColor = false;
      this.xrTableCell305.StylePriority.UseBorderColor = false;
      this.xrTableCell305.StylePriority.UseBorders = false;
      this.xrTableCell305.StylePriority.UseFont = false;
      this.xrTableCell305.StylePriority.UseForeColor = false;
      this.xrTableCell305.StylePriority.UsePadding = false;
      this.xrTableCell305.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell306
      // 
      resources.ApplyResources(this.xrTableCell306, "xrTableCell306");
      this.xrTableCell306.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell306.Name = "xrTableCell306";
      this.xrTableCell306.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell306.StylePriority.UseBackColor = false;
      this.xrTableCell306.StylePriority.UseBorderColor = false;
      this.xrTableCell306.StylePriority.UseBorders = false;
      this.xrTableCell306.StylePriority.UseFont = false;
      this.xrTableCell306.StylePriority.UseForeColor = false;
      this.xrTableCell306.StylePriority.UsePadding = false;
      this.xrTableCell306.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell315
      // 
      resources.ApplyResources(this.xrTableCell315, "xrTableCell315");
      this.xrTableCell315.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell315.Name = "xrTableCell315";
      this.xrTableCell315.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell315.StylePriority.UseBackColor = false;
      this.xrTableCell315.StylePriority.UseBorderColor = false;
      this.xrTableCell315.StylePriority.UseBorders = false;
      this.xrTableCell315.StylePriority.UseFont = false;
      this.xrTableCell315.StylePriority.UseForeColor = false;
      this.xrTableCell315.StylePriority.UsePadding = false;
      this.xrTableCell315.StylePriority.UseTextAlignment = false;
      // 
      // PropertyInfo
      // 
      this.PropertyInfo.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail26,
            this.GroupHeader14});
      this.PropertyInfo.DataMember = "ConsumerPropertyInformation";
      resources.ApplyResources(this.PropertyInfo, "PropertyInfo");
      this.PropertyInfo.Level = 7;
      this.PropertyInfo.Name = "PropertyInfo";
      // 
      // Detail26
      // 
      this.Detail26.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblPropInterests});
      resources.ApplyResources(this.Detail26, "Detail26");
      this.Detail26.Name = "Detail26";
      // 
      // tblPropInterests
      // 
      resources.ApplyResources(this.tblPropInterests, "tblPropInterests");
      this.tblPropInterests.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.tblPropInterests.Name = "tblPropInterests";
      this.tblPropInterests.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow148,
            this.xrTableRow149,
            this.xrTableRow150,
            this.xrTableRow151,
            this.xrTableRow152,
            this.xrTableRow153,
            this.xrTableRow155});
      this.tblPropInterests.StylePriority.UseBorderColor = false;
      this.tblPropInterests.StylePriority.UseBorders = false;
      this.tblPropInterests.StylePriority.UseFont = false;
      // 
      // xrTableRow148
      // 
      this.xrTableRow148.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell416});
      resources.ApplyResources(this.xrTableRow148, "xrTableRow148");
      this.xrTableRow148.Name = "xrTableRow148";
      // 
      // xrTableCell416
      // 
      resources.ApplyResources(this.xrTableCell416, "xrTableCell416");
      this.xrTableCell416.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell416.Name = "xrTableCell416";
      this.xrTableCell416.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTableCell416.StylePriority.UseBackColor = false;
      this.xrTableCell416.StylePriority.UseBorderColor = false;
      this.xrTableCell416.StylePriority.UseBorders = false;
      this.xrTableCell416.StylePriority.UseFont = false;
      this.xrTableCell416.StylePriority.UseForeColor = false;
      this.xrTableCell416.StylePriority.UsePadding = false;
      this.xrTableCell416.StylePriority.UseTextAlignment = false;
      resources.ApplyResources(xrSummary1, "xrSummary1");
      xrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.RecordNumber;
      xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
      this.xrTableCell416.Summary = xrSummary1;
      // 
      // xrTableRow149
      // 
      this.xrTableRow149.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDeedNo,
            this.lblDDeedNo,
            this.lblsiteno,
            this.lblDsiteno});
      resources.ApplyResources(this.xrTableRow149, "xrTableRow149");
      this.xrTableRow149.Name = "xrTableRow149";
      // 
      // lblDeedNo
      // 
      resources.ApplyResources(this.lblDeedNo, "lblDeedNo");
      this.lblDeedNo.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.lblDeedNo.Name = "lblDeedNo";
      this.lblDeedNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblDeedNo.StylePriority.UseBackColor = false;
      this.lblDeedNo.StylePriority.UseBorderColor = false;
      this.lblDeedNo.StylePriority.UseBorders = false;
      this.lblDeedNo.StylePriority.UseFont = false;
      this.lblDeedNo.StylePriority.UseForeColor = false;
      this.lblDeedNo.StylePriority.UsePadding = false;
      this.lblDeedNo.StylePriority.UseTextAlignment = false;
      // 
      // lblDDeedNo
      // 
      resources.ApplyResources(this.lblDDeedNo, "lblDDeedNo");
      this.lblDDeedNo.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.lblDDeedNo.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.TitleDeedNo")});
      this.lblDDeedNo.Name = "lblDDeedNo";
      this.lblDDeedNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblDDeedNo.StylePriority.UseBackColor = false;
      this.lblDDeedNo.StylePriority.UseBorderColor = false;
      this.lblDDeedNo.StylePriority.UseBorders = false;
      this.lblDDeedNo.StylePriority.UseFont = false;
      this.lblDDeedNo.StylePriority.UseForeColor = false;
      this.lblDDeedNo.StylePriority.UsePadding = false;
      this.lblDDeedNo.StylePriority.UseTextAlignment = false;
      // 
      // lblsiteno
      // 
      resources.ApplyResources(this.lblsiteno, "lblsiteno");
      this.lblsiteno.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.lblsiteno.Name = "lblsiteno";
      this.lblsiteno.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblsiteno.StylePriority.UseBackColor = false;
      this.lblsiteno.StylePriority.UseBorderColor = false;
      this.lblsiteno.StylePriority.UseBorders = false;
      this.lblsiteno.StylePriority.UseFont = false;
      this.lblsiteno.StylePriority.UseForeColor = false;
      this.lblsiteno.StylePriority.UsePadding = false;
      this.lblsiteno.StylePriority.UseTextAlignment = false;
      // 
      // lblDsiteno
      // 
      resources.ApplyResources(this.lblDsiteno, "lblDsiteno");
      this.lblDsiteno.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.lblDsiteno.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.StandNo")});
      this.lblDsiteno.Name = "lblDsiteno";
      this.lblDsiteno.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblDsiteno.StylePriority.UseBackColor = false;
      this.lblDsiteno.StylePriority.UseBorderColor = false;
      this.lblDsiteno.StylePriority.UseBorders = false;
      this.lblDsiteno.StylePriority.UseFont = false;
      this.lblDsiteno.StylePriority.UseForeColor = false;
      this.lblDsiteno.StylePriority.UsePadding = false;
      this.lblDsiteno.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow150
      // 
      this.xrTableRow150.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDeedsoffice,
            this.lblDDeedsoffice,
            this.lblPhyadd,
            this.lblDPhyadd});
      resources.ApplyResources(this.xrTableRow150, "xrTableRow150");
      this.xrTableRow150.Name = "xrTableRow150";
      // 
      // lblDeedsoffice
      // 
      resources.ApplyResources(this.lblDeedsoffice, "lblDeedsoffice");
      this.lblDeedsoffice.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.lblDeedsoffice.Name = "lblDeedsoffice";
      this.lblDeedsoffice.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblDeedsoffice.StylePriority.UseBackColor = false;
      this.lblDeedsoffice.StylePriority.UseBorderColor = false;
      this.lblDeedsoffice.StylePriority.UseBorders = false;
      this.lblDeedsoffice.StylePriority.UseFont = false;
      this.lblDeedsoffice.StylePriority.UseForeColor = false;
      this.lblDeedsoffice.StylePriority.UsePadding = false;
      this.lblDeedsoffice.StylePriority.UseTextAlignment = false;
      // 
      // lblDDeedsoffice
      // 
      resources.ApplyResources(this.lblDDeedsoffice, "lblDDeedsoffice");
      this.lblDDeedsoffice.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.lblDDeedsoffice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.DeedsOffice")});
      this.lblDDeedsoffice.Name = "lblDDeedsoffice";
      this.lblDDeedsoffice.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblDDeedsoffice.StylePriority.UseBackColor = false;
      this.lblDDeedsoffice.StylePriority.UseBorderColor = false;
      this.lblDDeedsoffice.StylePriority.UseBorders = false;
      this.lblDDeedsoffice.StylePriority.UseFont = false;
      this.lblDDeedsoffice.StylePriority.UseForeColor = false;
      this.lblDDeedsoffice.StylePriority.UsePadding = false;
      this.lblDDeedsoffice.StylePriority.UseTextAlignment = false;
      // 
      // lblPhyadd
      // 
      resources.ApplyResources(this.lblPhyadd, "lblPhyadd");
      this.lblPhyadd.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.lblPhyadd.Name = "lblPhyadd";
      this.lblPhyadd.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblPhyadd.StylePriority.UseBackColor = false;
      this.lblPhyadd.StylePriority.UseBorderColor = false;
      this.lblPhyadd.StylePriority.UseBorders = false;
      this.lblPhyadd.StylePriority.UseFont = false;
      this.lblPhyadd.StylePriority.UseForeColor = false;
      this.lblPhyadd.StylePriority.UsePadding = false;
      this.lblPhyadd.StylePriority.UseTextAlignment = false;
      // 
      // lblDPhyadd
      // 
      resources.ApplyResources(this.lblDPhyadd, "lblDPhyadd");
      this.lblDPhyadd.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.lblDPhyadd.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.PhysicalAddress")});
      this.lblDPhyadd.Name = "lblDPhyadd";
      this.lblDPhyadd.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblDPhyadd.StylePriority.UseBackColor = false;
      this.lblDPhyadd.StylePriority.UseBorderColor = false;
      this.lblDPhyadd.StylePriority.UseBorders = false;
      this.lblDPhyadd.StylePriority.UseFont = false;
      this.lblDPhyadd.StylePriority.UseForeColor = false;
      this.lblDPhyadd.StylePriority.UsePadding = false;
      this.lblDPhyadd.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow151
      // 
      this.xrTableRow151.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblPropType,
            this.lblDPropType,
            this.lblSize,
            this.lblDSize});
      resources.ApplyResources(this.xrTableRow151, "xrTableRow151");
      this.xrTableRow151.Name = "xrTableRow151";
      // 
      // lblPropType
      // 
      resources.ApplyResources(this.lblPropType, "lblPropType");
      this.lblPropType.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.lblPropType.Name = "lblPropType";
      this.lblPropType.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblPropType.StylePriority.UseBackColor = false;
      this.lblPropType.StylePriority.UseBorderColor = false;
      this.lblPropType.StylePriority.UseBorders = false;
      this.lblPropType.StylePriority.UseFont = false;
      this.lblPropType.StylePriority.UseForeColor = false;
      this.lblPropType.StylePriority.UsePadding = false;
      this.lblPropType.StylePriority.UseTextAlignment = false;
      // 
      // lblDPropType
      // 
      resources.ApplyResources(this.lblDPropType, "lblDPropType");
      this.lblDPropType.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.lblDPropType.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.PropertyTypeDesc")});
      this.lblDPropType.Name = "lblDPropType";
      this.lblDPropType.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblDPropType.StylePriority.UseBackColor = false;
      this.lblDPropType.StylePriority.UseBorderColor = false;
      this.lblDPropType.StylePriority.UseBorders = false;
      this.lblDPropType.StylePriority.UseFont = false;
      this.lblDPropType.StylePriority.UseForeColor = false;
      this.lblDPropType.StylePriority.UsePadding = false;
      this.lblDPropType.StylePriority.UseTextAlignment = false;
      // 
      // lblSize
      // 
      resources.ApplyResources(this.lblSize, "lblSize");
      this.lblSize.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.lblSize.Name = "lblSize";
      this.lblSize.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblSize.StylePriority.UseBackColor = false;
      this.lblSize.StylePriority.UseBorderColor = false;
      this.lblSize.StylePriority.UseBorders = false;
      this.lblSize.StylePriority.UseFont = false;
      this.lblSize.StylePriority.UseForeColor = false;
      this.lblSize.StylePriority.UsePadding = false;
      this.lblSize.StylePriority.UseTextAlignment = false;
      // 
      // lblDSize
      // 
      resources.ApplyResources(this.lblDSize, "lblDSize");
      this.lblDSize.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.lblDSize.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.ErfSize")});
      this.lblDSize.Name = "lblDSize";
      this.lblDSize.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblDSize.StylePriority.UseBackColor = false;
      this.lblDSize.StylePriority.UseBorderColor = false;
      this.lblDSize.StylePriority.UseBorders = false;
      this.lblDSize.StylePriority.UseFont = false;
      this.lblDSize.StylePriority.UseForeColor = false;
      this.lblDSize.StylePriority.UsePadding = false;
      this.lblDSize.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow152
      // 
      this.xrTableRow152.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblPurchasedate,
            this.lblDPurchasedate,
            this.lblPurchaseprice,
            this.lblDPurchaseprice});
      resources.ApplyResources(this.xrTableRow152, "xrTableRow152");
      this.xrTableRow152.Name = "xrTableRow152";
      // 
      // lblPurchasedate
      // 
      resources.ApplyResources(this.lblPurchasedate, "lblPurchasedate");
      this.lblPurchasedate.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.lblPurchasedate.Name = "lblPurchasedate";
      this.lblPurchasedate.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblPurchasedate.StylePriority.UseBackColor = false;
      this.lblPurchasedate.StylePriority.UseBorderColor = false;
      this.lblPurchasedate.StylePriority.UseBorders = false;
      this.lblPurchasedate.StylePriority.UseFont = false;
      this.lblPurchasedate.StylePriority.UseForeColor = false;
      this.lblPurchasedate.StylePriority.UsePadding = false;
      this.lblPurchasedate.StylePriority.UseTextAlignment = false;
      // 
      // lblDPurchasedate
      // 
      resources.ApplyResources(this.lblDPurchasedate, "lblDPurchasedate");
      this.lblDPurchasedate.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.lblDPurchasedate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.PurchaseDate")});
      this.lblDPurchasedate.Name = "lblDPurchasedate";
      this.lblDPurchasedate.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblDPurchasedate.StylePriority.UseBackColor = false;
      this.lblDPurchasedate.StylePriority.UseBorderColor = false;
      this.lblDPurchasedate.StylePriority.UseBorders = false;
      this.lblDPurchasedate.StylePriority.UseFont = false;
      this.lblDPurchasedate.StylePriority.UseForeColor = false;
      this.lblDPurchasedate.StylePriority.UsePadding = false;
      this.lblDPurchasedate.StylePriority.UseTextAlignment = false;
      // 
      // lblPurchaseprice
      // 
      resources.ApplyResources(this.lblPurchaseprice, "lblPurchaseprice");
      this.lblPurchaseprice.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.lblPurchaseprice.Name = "lblPurchaseprice";
      this.lblPurchaseprice.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblPurchaseprice.StylePriority.UseBackColor = false;
      this.lblPurchaseprice.StylePriority.UseBorderColor = false;
      this.lblPurchaseprice.StylePriority.UseBorders = false;
      this.lblPurchaseprice.StylePriority.UseFont = false;
      this.lblPurchaseprice.StylePriority.UseForeColor = false;
      this.lblPurchaseprice.StylePriority.UsePadding = false;
      this.lblPurchaseprice.StylePriority.UseTextAlignment = false;
      // 
      // lblDPurchaseprice
      // 
      resources.ApplyResources(this.lblDPurchaseprice, "lblDPurchaseprice");
      this.lblDPurchaseprice.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.lblDPurchaseprice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.PurchasePriceAmt")});
      this.lblDPurchaseprice.Name = "lblDPurchaseprice";
      this.lblDPurchaseprice.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblDPurchaseprice.StylePriority.UseBackColor = false;
      this.lblDPurchaseprice.StylePriority.UseBorderColor = false;
      this.lblDPurchaseprice.StylePriority.UseBorders = false;
      this.lblDPurchaseprice.StylePriority.UseFont = false;
      this.lblDPurchaseprice.StylePriority.UseForeColor = false;
      this.lblDPurchaseprice.StylePriority.UsePadding = false;
      this.lblDPurchaseprice.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow153
      // 
      this.xrTableRow153.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblshareperc,
            this.lblDBondNo,
            this.lblHolder,
            this.lblDHolder});
      resources.ApplyResources(this.xrTableRow153, "xrTableRow153");
      this.xrTableRow153.Name = "xrTableRow153";
      // 
      // lblshareperc
      // 
      resources.ApplyResources(this.lblshareperc, "lblshareperc");
      this.lblshareperc.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.lblshareperc.Name = "lblshareperc";
      this.lblshareperc.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblshareperc.StylePriority.UseBackColor = false;
      this.lblshareperc.StylePriority.UseBorderColor = false;
      this.lblshareperc.StylePriority.UseBorders = false;
      this.lblshareperc.StylePriority.UseFont = false;
      this.lblshareperc.StylePriority.UseForeColor = false;
      this.lblshareperc.StylePriority.UsePadding = false;
      this.lblshareperc.StylePriority.UseTextAlignment = false;
      // 
      // lblDBondNo
      // 
      resources.ApplyResources(this.lblDBondNo, "lblDBondNo");
      this.lblDBondNo.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.lblDBondNo.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.BondAccountNo")});
      this.lblDBondNo.Name = "lblDBondNo";
      this.lblDBondNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblDBondNo.StylePriority.UseBackColor = false;
      this.lblDBondNo.StylePriority.UseBorderColor = false;
      this.lblDBondNo.StylePriority.UseBorders = false;
      this.lblDBondNo.StylePriority.UseFont = false;
      this.lblDBondNo.StylePriority.UseForeColor = false;
      this.lblDBondNo.StylePriority.UsePadding = false;
      this.lblDBondNo.StylePriority.UseTextAlignment = false;
      // 
      // lblHolder
      // 
      resources.ApplyResources(this.lblHolder, "lblHolder");
      this.lblHolder.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.lblHolder.Name = "lblHolder";
      this.lblHolder.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblHolder.StylePriority.UseBackColor = false;
      this.lblHolder.StylePriority.UseBorderColor = false;
      this.lblHolder.StylePriority.UseBorders = false;
      this.lblHolder.StylePriority.UseFont = false;
      this.lblHolder.StylePriority.UseForeColor = false;
      this.lblHolder.StylePriority.UsePadding = false;
      this.lblHolder.StylePriority.UseTextAlignment = false;
      // 
      // lblDHolder
      // 
      resources.ApplyResources(this.lblDHolder, "lblDHolder");
      this.lblDHolder.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.lblDHolder.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.BondAmt", "{0:c2}")});
      this.lblDHolder.Name = "lblDHolder";
      this.lblDHolder.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblDHolder.StylePriority.UseBackColor = false;
      this.lblDHolder.StylePriority.UseBorderColor = false;
      this.lblDHolder.StylePriority.UseBorders = false;
      this.lblDHolder.StylePriority.UseFont = false;
      this.lblDHolder.StylePriority.UseForeColor = false;
      this.lblDHolder.StylePriority.UsePadding = false;
      this.lblDHolder.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow155
      // 
      this.xrTableRow155.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblBondNo,
            this.lblDBondAmount,
            this.lblBondAmount,
            this.lbl50});
      resources.ApplyResources(this.xrTableRow155, "xrTableRow155");
      this.xrTableRow155.Name = "xrTableRow155";
      // 
      // lblBondNo
      // 
      resources.ApplyResources(this.lblBondNo, "lblBondNo");
      this.lblBondNo.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.lblBondNo.Name = "lblBondNo";
      this.lblBondNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblBondNo.StylePriority.UseBackColor = false;
      this.lblBondNo.StylePriority.UseBorderColor = false;
      this.lblBondNo.StylePriority.UseBorders = false;
      this.lblBondNo.StylePriority.UseFont = false;
      this.lblBondNo.StylePriority.UseForeColor = false;
      this.lblBondNo.StylePriority.UsePadding = false;
      this.lblBondNo.StylePriority.UseTextAlignment = false;
      // 
      // lblDBondAmount
      // 
      resources.ApplyResources(this.lblDBondAmount, "lblDBondAmount");
      this.lblDBondAmount.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.lblDBondAmount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerPropertyInformation.BondHolderName")});
      this.lblDBondAmount.Name = "lblDBondAmount";
      this.lblDBondAmount.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblDBondAmount.StylePriority.UseBackColor = false;
      this.lblDBondAmount.StylePriority.UseBorderColor = false;
      this.lblDBondAmount.StylePriority.UseBorders = false;
      this.lblDBondAmount.StylePriority.UseFont = false;
      this.lblDBondAmount.StylePriority.UseForeColor = false;
      this.lblDBondAmount.StylePriority.UsePadding = false;
      this.lblDBondAmount.StylePriority.UseTextAlignment = false;
      // 
      // lblBondAmount
      // 
      resources.ApplyResources(this.lblBondAmount, "lblBondAmount");
      this.lblBondAmount.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.lblBondAmount.Name = "lblBondAmount";
      this.lblBondAmount.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblBondAmount.StylePriority.UseBackColor = false;
      this.lblBondAmount.StylePriority.UseBorderColor = false;
      this.lblBondAmount.StylePriority.UseBorders = false;
      this.lblBondAmount.StylePriority.UseFont = false;
      this.lblBondAmount.StylePriority.UseForeColor = false;
      this.lblBondAmount.StylePriority.UsePadding = false;
      this.lblBondAmount.StylePriority.UseTextAlignment = false;
      // 
      // lbl50
      // 
      resources.ApplyResources(this.lbl50, "lbl50");
      this.lbl50.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.lbl50.Name = "lbl50";
      this.lbl50.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lbl50.StylePriority.UseBackColor = false;
      this.lbl50.StylePriority.UseBorderColor = false;
      this.lbl50.StylePriority.UseBorders = false;
      this.lbl50.StylePriority.UseFont = false;
      this.lbl50.StylePriority.UseForeColor = false;
      this.lbl50.StylePriority.UsePadding = false;
      this.lbl50.StylePriority.UseTextAlignment = false;
      // 
      // GroupHeader14
      // 
      this.GroupHeader14.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable34});
      resources.ApplyResources(this.GroupHeader14, "GroupHeader14");
      this.GroupHeader14.Name = "GroupHeader14";
      // 
      // xrTable34
      // 
      this.xrTable34.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      resources.ApplyResources(this.xrTable34, "xrTable34");
      this.xrTable34.Name = "xrTable34";
      this.xrTable34.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
      this.xrTable34.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow147,
            this.xrTableRow175});
      this.xrTable34.StylePriority.UseBorders = false;
      this.xrTable34.StylePriority.UseFont = false;
      this.xrTable34.StylePriority.UseForeColor = false;
      this.xrTable34.StylePriority.UsePadding = false;
      this.xrTable34.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow147
      // 
      this.xrTableRow147.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblPropInterestsH,
            this.xrTableCell414});
      resources.ApplyResources(this.xrTableRow147, "xrTableRow147");
      this.xrTableRow147.Name = "xrTableRow147";
      // 
      // lblPropInterestsH
      // 
      resources.ApplyResources(this.lblPropInterestsH, "lblPropInterestsH");
      this.lblPropInterestsH.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
      this.lblPropInterestsH.Name = "lblPropInterestsH";
      this.lblPropInterestsH.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblPropInterestsH.StylePriority.UseBackColor = false;
      this.lblPropInterestsH.StylePriority.UseBorderColor = false;
      this.lblPropInterestsH.StylePriority.UseBorders = false;
      this.lblPropInterestsH.StylePriority.UseFont = false;
      this.lblPropInterestsH.StylePriority.UseForeColor = false;
      this.lblPropInterestsH.StylePriority.UsePadding = false;
      this.lblPropInterestsH.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell414
      // 
      resources.ApplyResources(this.xrTableCell414, "xrTableCell414");
      this.xrTableCell414.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell414.BorderWidth = 1F;
      this.xrTableCell414.Name = "xrTableCell414";
      this.xrTableCell414.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell414.StylePriority.UseBackColor = false;
      this.xrTableCell414.StylePriority.UseBorderColor = false;
      this.xrTableCell414.StylePriority.UseBorders = false;
      this.xrTableCell414.StylePriority.UseBorderWidth = false;
      this.xrTableCell414.StylePriority.UseFont = false;
      this.xrTableCell414.StylePriority.UseForeColor = false;
      this.xrTableCell414.StylePriority.UsePadding = false;
      // 
      // xrTableRow175
      // 
      this.xrTableRow175.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell415});
      resources.ApplyResources(this.xrTableRow175, "xrTableRow175");
      this.xrTableRow175.Name = "xrTableRow175";
      // 
      // xrTableCell415
      // 
      this.xrTableCell415.Borders = DevExpress.XtraPrinting.BorderSide.None;
      resources.ApplyResources(this.xrTableCell415, "xrTableCell415");
      this.xrTableCell415.Multiline = true;
      this.xrTableCell415.Name = "xrTableCell415";
      this.xrTableCell415.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTableCell415.StylePriority.UseBorders = false;
      this.xrTableCell415.StylePriority.UseFont = false;
      this.xrTableCell415.StylePriority.UseForeColor = false;
      this.xrTableCell415.StylePriority.UsePadding = false;
      // 
      // CIPC
      // 
      this.CIPC.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail27,
            this.GroupHeader15});
      this.CIPC.DataMember = "ConsumerDirectorShipLink";
      resources.ApplyResources(this.CIPC, "CIPC");
      this.CIPC.Level = 8;
      this.CIPC.Name = "CIPC";
      // 
      // Detail27
      // 
      this.Detail27.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblDirInfo});
      resources.ApplyResources(this.Detail27, "Detail27");
      this.Detail27.Name = "Detail27";
      // 
      // tblDirInfo
      // 
      resources.ApplyResources(this.tblDirInfo, "tblDirInfo");
      this.tblDirInfo.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.tblDirInfo.Name = "tblDirInfo";
      this.tblDirInfo.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow157,
            this.xrTableRow158,
            this.xrTableRow159,
            this.xrTableRow160,
            this.xrTableRow161});
      this.tblDirInfo.StylePriority.UseBorderColor = false;
      this.tblDirInfo.StylePriority.UseBorders = false;
      // 
      // xrTableRow157
      // 
      this.xrTableRow157.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell420});
      resources.ApplyResources(this.xrTableRow157, "xrTableRow157");
      this.xrTableRow157.Name = "xrTableRow157";
      // 
      // xrTableCell420
      // 
      resources.ApplyResources(this.xrTableCell420, "xrTableCell420");
      this.xrTableCell420.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell420.Name = "xrTableCell420";
      this.xrTableCell420.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTableCell420.StylePriority.UseBackColor = false;
      this.xrTableCell420.StylePriority.UseBorderColor = false;
      this.xrTableCell420.StylePriority.UseBorders = false;
      this.xrTableCell420.StylePriority.UseFont = false;
      this.xrTableCell420.StylePriority.UseForeColor = false;
      this.xrTableCell420.StylePriority.UsePadding = false;
      this.xrTableCell420.StylePriority.UseTextAlignment = false;
      resources.ApplyResources(xrSummary2, "xrSummary2");
      xrSummary2.Func = DevExpress.XtraReports.UI.SummaryFunc.RecordNumber;
      xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
      this.xrTableCell420.Summary = xrSummary2;
      // 
      // xrTableRow158
      // 
      this.xrTableRow158.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCurrentPost,
            this.lblDCurrentPost,
            this.lblInceptionDate,
            this.lblDAppDate});
      resources.ApplyResources(this.xrTableRow158, "xrTableRow158");
      this.xrTableRow158.Name = "xrTableRow158";
      // 
      // lblCurrentPost
      // 
      resources.ApplyResources(this.lblCurrentPost, "lblCurrentPost");
      this.lblCurrentPost.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.lblCurrentPost.Name = "lblCurrentPost";
      this.lblCurrentPost.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblCurrentPost.StylePriority.UseBackColor = false;
      this.lblCurrentPost.StylePriority.UseBorderColor = false;
      this.lblCurrentPost.StylePriority.UseBorders = false;
      this.lblCurrentPost.StylePriority.UseFont = false;
      this.lblCurrentPost.StylePriority.UseForeColor = false;
      this.lblCurrentPost.StylePriority.UsePadding = false;
      this.lblCurrentPost.StylePriority.UseTextAlignment = false;
      // 
      // lblDCurrentPost
      // 
      resources.ApplyResources(this.lblDCurrentPost, "lblDCurrentPost");
      this.lblDCurrentPost.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.lblDCurrentPost.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.DirectorDesignationDesc")});
      this.lblDCurrentPost.Name = "lblDCurrentPost";
      this.lblDCurrentPost.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblDCurrentPost.StylePriority.UseBackColor = false;
      this.lblDCurrentPost.StylePriority.UseBorderColor = false;
      this.lblDCurrentPost.StylePriority.UseBorders = false;
      this.lblDCurrentPost.StylePriority.UseFont = false;
      this.lblDCurrentPost.StylePriority.UseForeColor = false;
      this.lblDCurrentPost.StylePriority.UsePadding = false;
      this.lblDCurrentPost.StylePriority.UseTextAlignment = false;
      // 
      // lblInceptionDate
      // 
      resources.ApplyResources(this.lblInceptionDate, "lblInceptionDate");
      this.lblInceptionDate.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.lblInceptionDate.Name = "lblInceptionDate";
      this.lblInceptionDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblInceptionDate.StylePriority.UseBackColor = false;
      this.lblInceptionDate.StylePriority.UseBorderColor = false;
      this.lblInceptionDate.StylePriority.UseBorders = false;
      this.lblInceptionDate.StylePriority.UseFont = false;
      this.lblInceptionDate.StylePriority.UseForeColor = false;
      this.lblInceptionDate.StylePriority.UsePadding = false;
      this.lblInceptionDate.StylePriority.UseTextAlignment = false;
      // 
      // lblDAppDate
      // 
      resources.ApplyResources(this.lblDAppDate, "lblDAppDate");
      this.lblDAppDate.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.lblDAppDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.AppointmentDate")});
      this.lblDAppDate.Name = "lblDAppDate";
      this.lblDAppDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblDAppDate.StylePriority.UseBackColor = false;
      this.lblDAppDate.StylePriority.UseBorderColor = false;
      this.lblDAppDate.StylePriority.UseBorders = false;
      this.lblDAppDate.StylePriority.UseFont = false;
      this.lblDAppDate.StylePriority.UseForeColor = false;
      this.lblDAppDate.StylePriority.UsePadding = false;
      this.lblDAppDate.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow159
      // 
      this.xrTableRow159.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCompanyName,
            this.lblDCompanyName,
            this.lblRegNo,
            this.lblDRegNo});
      resources.ApplyResources(this.xrTableRow159, "xrTableRow159");
      this.xrTableRow159.Name = "xrTableRow159";
      // 
      // lblCompanyName
      // 
      resources.ApplyResources(this.lblCompanyName, "lblCompanyName");
      this.lblCompanyName.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.lblCompanyName.Name = "lblCompanyName";
      this.lblCompanyName.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblCompanyName.StylePriority.UseBackColor = false;
      this.lblCompanyName.StylePriority.UseBorderColor = false;
      this.lblCompanyName.StylePriority.UseBorders = false;
      this.lblCompanyName.StylePriority.UseFont = false;
      this.lblCompanyName.StylePriority.UseForeColor = false;
      this.lblCompanyName.StylePriority.UsePadding = false;
      this.lblCompanyName.StylePriority.UseTextAlignment = false;
      // 
      // lblDCompanyName
      // 
      resources.ApplyResources(this.lblDCompanyName, "lblDCompanyName");
      this.lblDCompanyName.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.lblDCompanyName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.CommercialName")});
      this.lblDCompanyName.Name = "lblDCompanyName";
      this.lblDCompanyName.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblDCompanyName.StylePriority.UseBackColor = false;
      this.lblDCompanyName.StylePriority.UseBorderColor = false;
      this.lblDCompanyName.StylePriority.UseBorders = false;
      this.lblDCompanyName.StylePriority.UseFont = false;
      this.lblDCompanyName.StylePriority.UseForeColor = false;
      this.lblDCompanyName.StylePriority.UsePadding = false;
      this.lblDCompanyName.StylePriority.UseTextAlignment = false;
      // 
      // lblRegNo
      // 
      resources.ApplyResources(this.lblRegNo, "lblRegNo");
      this.lblRegNo.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.lblRegNo.Name = "lblRegNo";
      this.lblRegNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblRegNo.StylePriority.UseBackColor = false;
      this.lblRegNo.StylePriority.UseBorderColor = false;
      this.lblRegNo.StylePriority.UseBorders = false;
      this.lblRegNo.StylePriority.UseFont = false;
      this.lblRegNo.StylePriority.UseForeColor = false;
      this.lblRegNo.StylePriority.UsePadding = false;
      this.lblRegNo.StylePriority.UseTextAlignment = false;
      // 
      // lblDRegNo
      // 
      resources.ApplyResources(this.lblDRegNo, "lblDRegNo");
      this.lblDRegNo.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.lblDRegNo.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.RegistrationNo")});
      this.lblDRegNo.Name = "lblDRegNo";
      this.lblDRegNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblDRegNo.StylePriority.UseBackColor = false;
      this.lblDRegNo.StylePriority.UseBorderColor = false;
      this.lblDRegNo.StylePriority.UseBorders = false;
      this.lblDRegNo.StylePriority.UseFont = false;
      this.lblDRegNo.StylePriority.UseForeColor = false;
      this.lblDRegNo.StylePriority.UsePadding = false;
      this.lblDRegNo.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow160
      // 
      this.xrTableRow160.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCompanyAdd,
            this.lblDPhysicalAdd,
            this.lblPhoneNo,
            this.lblDPhoneNo});
      resources.ApplyResources(this.xrTableRow160, "xrTableRow160");
      this.xrTableRow160.Name = "xrTableRow160";
      // 
      // lblCompanyAdd
      // 
      resources.ApplyResources(this.lblCompanyAdd, "lblCompanyAdd");
      this.lblCompanyAdd.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.lblCompanyAdd.Name = "lblCompanyAdd";
      this.lblCompanyAdd.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblCompanyAdd.StylePriority.UseBackColor = false;
      this.lblCompanyAdd.StylePriority.UseBorderColor = false;
      this.lblCompanyAdd.StylePriority.UseBorders = false;
      this.lblCompanyAdd.StylePriority.UseFont = false;
      this.lblCompanyAdd.StylePriority.UseForeColor = false;
      this.lblCompanyAdd.StylePriority.UsePadding = false;
      this.lblCompanyAdd.StylePriority.UseTextAlignment = false;
      // 
      // lblDPhysicalAdd
      // 
      resources.ApplyResources(this.lblDPhysicalAdd, "lblDPhysicalAdd");
      this.lblDPhysicalAdd.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.lblDPhysicalAdd.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.PhysicalAddress")});
      this.lblDPhysicalAdd.Name = "lblDPhysicalAdd";
      this.lblDPhysicalAdd.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblDPhysicalAdd.StylePriority.UseBackColor = false;
      this.lblDPhysicalAdd.StylePriority.UseBorderColor = false;
      this.lblDPhysicalAdd.StylePriority.UseBorders = false;
      this.lblDPhysicalAdd.StylePriority.UseFont = false;
      this.lblDPhysicalAdd.StylePriority.UseForeColor = false;
      this.lblDPhysicalAdd.StylePriority.UsePadding = false;
      this.lblDPhysicalAdd.StylePriority.UseTextAlignment = false;
      // 
      // lblPhoneNo
      // 
      resources.ApplyResources(this.lblPhoneNo, "lblPhoneNo");
      this.lblPhoneNo.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.lblPhoneNo.Name = "lblPhoneNo";
      this.lblPhoneNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblPhoneNo.StylePriority.UseBackColor = false;
      this.lblPhoneNo.StylePriority.UseBorderColor = false;
      this.lblPhoneNo.StylePriority.UseBorders = false;
      this.lblPhoneNo.StylePriority.UseFont = false;
      this.lblPhoneNo.StylePriority.UseForeColor = false;
      this.lblPhoneNo.StylePriority.UsePadding = false;
      this.lblPhoneNo.StylePriority.UseTextAlignment = false;
      // 
      // lblDPhoneNo
      // 
      resources.ApplyResources(this.lblDPhoneNo, "lblDPhoneNo");
      this.lblDPhoneNo.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.lblDPhoneNo.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.TelephoneNo")});
      this.lblDPhoneNo.Name = "lblDPhoneNo";
      this.lblDPhoneNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblDPhoneNo.StylePriority.UseBackColor = false;
      this.lblDPhoneNo.StylePriority.UseBorderColor = false;
      this.lblDPhoneNo.StylePriority.UseBorders = false;
      this.lblDPhoneNo.StylePriority.UseFont = false;
      this.lblDPhoneNo.StylePriority.UseForeColor = false;
      this.lblDPhoneNo.StylePriority.UsePadding = false;
      this.lblDPhoneNo.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow161
      // 
      this.xrTableRow161.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblIndustryCatg,
            this.lblDIndCateg,
            this.lblDirectorStatus,
            this.lblDDirectorStatus});
      resources.ApplyResources(this.xrTableRow161, "xrTableRow161");
      this.xrTableRow161.Name = "xrTableRow161";
      // 
      // lblIndustryCatg
      // 
      resources.ApplyResources(this.lblIndustryCatg, "lblIndustryCatg");
      this.lblIndustryCatg.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.lblIndustryCatg.Name = "lblIndustryCatg";
      this.lblIndustryCatg.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblIndustryCatg.StylePriority.UseBackColor = false;
      this.lblIndustryCatg.StylePriority.UseBorderColor = false;
      this.lblIndustryCatg.StylePriority.UseBorders = false;
      this.lblIndustryCatg.StylePriority.UseFont = false;
      this.lblIndustryCatg.StylePriority.UseForeColor = false;
      this.lblIndustryCatg.StylePriority.UsePadding = false;
      this.lblIndustryCatg.StylePriority.UseTextAlignment = false;
      // 
      // lblDIndCateg
      // 
      resources.ApplyResources(this.lblDIndCateg, "lblDIndCateg");
      this.lblDIndCateg.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.lblDIndCateg.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.SICDesc")});
      this.lblDIndCateg.Name = "lblDIndCateg";
      this.lblDIndCateg.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblDIndCateg.StylePriority.UseBackColor = false;
      this.lblDIndCateg.StylePriority.UseBorderColor = false;
      this.lblDIndCateg.StylePriority.UseBorders = false;
      this.lblDIndCateg.StylePriority.UseFont = false;
      this.lblDIndCateg.StylePriority.UseForeColor = false;
      this.lblDIndCateg.StylePriority.UsePadding = false;
      this.lblDIndCateg.StylePriority.UseTextAlignment = false;
      // 
      // lblDirectorStatus
      // 
      resources.ApplyResources(this.lblDirectorStatus, "lblDirectorStatus");
      this.lblDirectorStatus.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.lblDirectorStatus.Name = "lblDirectorStatus";
      this.lblDirectorStatus.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblDirectorStatus.StylePriority.UseBackColor = false;
      this.lblDirectorStatus.StylePriority.UseBorderColor = false;
      this.lblDirectorStatus.StylePriority.UseBorders = false;
      this.lblDirectorStatus.StylePriority.UseFont = false;
      this.lblDirectorStatus.StylePriority.UseForeColor = false;
      this.lblDirectorStatus.StylePriority.UsePadding = false;
      this.lblDirectorStatus.StylePriority.UseTextAlignment = false;
      // 
      // lblDDirectorStatus
      // 
      resources.ApplyResources(this.lblDDirectorStatus, "lblDDirectorStatus");
      this.lblDDirectorStatus.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.lblDDirectorStatus.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDirectorShipLink.DirectorStatus")});
      this.lblDDirectorStatus.Name = "lblDDirectorStatus";
      this.lblDDirectorStatus.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblDDirectorStatus.StylePriority.UseBackColor = false;
      this.lblDDirectorStatus.StylePriority.UseBorderColor = false;
      this.lblDDirectorStatus.StylePriority.UseBorders = false;
      this.lblDDirectorStatus.StylePriority.UseFont = false;
      this.lblDDirectorStatus.StylePriority.UseForeColor = false;
      this.lblDDirectorStatus.StylePriority.UsePadding = false;
      this.lblDDirectorStatus.StylePriority.UseTextAlignment = false;
      // 
      // GroupHeader15
      // 
      this.GroupHeader15.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable35});
      resources.ApplyResources(this.GroupHeader15, "GroupHeader15");
      this.GroupHeader15.Name = "GroupHeader15";
      // 
      // xrTable35
      // 
      this.xrTable35.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      resources.ApplyResources(this.xrTable35, "xrTable35");
      this.xrTable35.Name = "xrTable35";
      this.xrTable35.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
      this.xrTable35.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow156,
            this.xrTableRow176});
      this.xrTable35.StylePriority.UseBorders = false;
      this.xrTable35.StylePriority.UseFont = false;
      this.xrTable35.StylePriority.UseForeColor = false;
      this.xrTable35.StylePriority.UsePadding = false;
      this.xrTable35.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow156
      // 
      this.xrTableRow156.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell417,
            this.xrTableCell418});
      resources.ApplyResources(this.xrTableRow156, "xrTableRow156");
      this.xrTableRow156.Name = "xrTableRow156";
      // 
      // xrTableCell417
      // 
      resources.ApplyResources(this.xrTableCell417, "xrTableCell417");
      this.xrTableCell417.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell417.Name = "xrTableCell417";
      this.xrTableCell417.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell417.StylePriority.UseBackColor = false;
      this.xrTableCell417.StylePriority.UseBorderColor = false;
      this.xrTableCell417.StylePriority.UseBorders = false;
      this.xrTableCell417.StylePriority.UseFont = false;
      this.xrTableCell417.StylePriority.UseForeColor = false;
      this.xrTableCell417.StylePriority.UsePadding = false;
      this.xrTableCell417.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell418
      // 
      resources.ApplyResources(this.xrTableCell418, "xrTableCell418");
      this.xrTableCell418.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell418.BorderWidth = 1F;
      this.xrTableCell418.Name = "xrTableCell418";
      this.xrTableCell418.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell418.StylePriority.UseBackColor = false;
      this.xrTableCell418.StylePriority.UseBorderColor = false;
      this.xrTableCell418.StylePriority.UseBorders = false;
      this.xrTableCell418.StylePriority.UseBorderWidth = false;
      this.xrTableCell418.StylePriority.UseFont = false;
      this.xrTableCell418.StylePriority.UseForeColor = false;
      this.xrTableCell418.StylePriority.UsePadding = false;
      // 
      // xrTableRow176
      // 
      this.xrTableRow176.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell419});
      resources.ApplyResources(this.xrTableRow176, "xrTableRow176");
      this.xrTableRow176.Name = "xrTableRow176";
      // 
      // xrTableCell419
      // 
      this.xrTableCell419.Borders = DevExpress.XtraPrinting.BorderSide.None;
      resources.ApplyResources(this.xrTableCell419, "xrTableCell419");
      this.xrTableCell419.Multiline = true;
      this.xrTableCell419.Name = "xrTableCell419";
      this.xrTableCell419.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTableCell419.StylePriority.UseBorders = false;
      this.xrTableCell419.StylePriority.UseFont = false;
      this.xrTableCell419.StylePriority.UseForeColor = false;
      this.xrTableCell419.StylePriority.UsePadding = false;
      // 
      // DetailReport
      // 
      this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail28,
            this.GroupHeader16});
      this.DetailReport.DataMember = "SAPInformation";
      resources.ApplyResources(this.DetailReport, "DetailReport");
      this.DetailReport.Level = 9;
      this.DetailReport.Name = "DetailReport";
      // 
      // Detail28
      // 
      this.Detail28.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable37});
      resources.ApplyResources(this.Detail28, "Detail28");
      this.Detail28.Name = "Detail28";
      // 
      // xrTable37
      // 
      resources.ApplyResources(this.xrTable37, "xrTable37");
      this.xrTable37.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTable37.Name = "xrTable37";
      this.xrTable37.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow164,
            this.xrTableRow165,
            this.xrTableRow166,
            this.xrTableRow167,
            this.xrTableRow171,
            this.xrTableRow170,
            this.xrTableRow169,
            this.xrTableRow168});
      this.xrTable37.StylePriority.UseBorderColor = false;
      this.xrTable37.StylePriority.UseBorders = false;
      // 
      // xrTableRow164
      // 
      this.xrTableRow164.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell424});
      resources.ApplyResources(this.xrTableRow164, "xrTableRow164");
      this.xrTableRow164.Name = "xrTableRow164";
      // 
      // xrTableCell424
      // 
      resources.ApplyResources(this.xrTableCell424, "xrTableCell424");
      this.xrTableCell424.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell424.Name = "xrTableCell424";
      this.xrTableCell424.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTableCell424.StylePriority.UseBackColor = false;
      this.xrTableCell424.StylePriority.UseBorderColor = false;
      this.xrTableCell424.StylePriority.UseBorders = false;
      this.xrTableCell424.StylePriority.UseFont = false;
      this.xrTableCell424.StylePriority.UseForeColor = false;
      this.xrTableCell424.StylePriority.UsePadding = false;
      this.xrTableCell424.StylePriority.UseTextAlignment = false;
      resources.ApplyResources(xrSummary3, "xrSummary3");
      xrSummary3.Func = DevExpress.XtraReports.UI.SummaryFunc.RecordNumber;
      xrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
      this.xrTableCell424.Summary = xrSummary3;
      // 
      // xrTableRow165
      // 
      this.xrTableRow165.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell425,
            this.xrTableCell428});
      resources.ApplyResources(this.xrTableRow165, "xrTableRow165");
      this.xrTableRow165.Name = "xrTableRow165";
      // 
      // xrTableCell425
      // 
      resources.ApplyResources(this.xrTableCell425, "xrTableCell425");
      this.xrTableCell425.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell425.Name = "xrTableCell425";
      this.xrTableCell425.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell425.StylePriority.UseBackColor = false;
      this.xrTableCell425.StylePriority.UseBorderColor = false;
      this.xrTableCell425.StylePriority.UseBorders = false;
      this.xrTableCell425.StylePriority.UseFont = false;
      this.xrTableCell425.StylePriority.UseForeColor = false;
      this.xrTableCell425.StylePriority.UsePadding = false;
      this.xrTableCell425.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell428
      // 
      resources.ApplyResources(this.xrTableCell428, "xrTableCell428");
      this.xrTableCell428.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell428.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SAPInformation.ChassisNo")});
      this.xrTableCell428.Name = "xrTableCell428";
      this.xrTableCell428.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell428.StylePriority.UseBackColor = false;
      this.xrTableCell428.StylePriority.UseBorderColor = false;
      this.xrTableCell428.StylePriority.UseBorders = false;
      this.xrTableCell428.StylePriority.UseFont = false;
      this.xrTableCell428.StylePriority.UseForeColor = false;
      this.xrTableCell428.StylePriority.UsePadding = false;
      this.xrTableCell428.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow166
      // 
      this.xrTableRow166.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell429,
            this.xrTableCell432});
      resources.ApplyResources(this.xrTableRow166, "xrTableRow166");
      this.xrTableRow166.Name = "xrTableRow166";
      // 
      // xrTableCell429
      // 
      resources.ApplyResources(this.xrTableCell429, "xrTableCell429");
      this.xrTableCell429.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell429.Name = "xrTableCell429";
      this.xrTableCell429.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell429.StylePriority.UseBackColor = false;
      this.xrTableCell429.StylePriority.UseBorderColor = false;
      this.xrTableCell429.StylePriority.UseBorders = false;
      this.xrTableCell429.StylePriority.UseFont = false;
      this.xrTableCell429.StylePriority.UseForeColor = false;
      this.xrTableCell429.StylePriority.UsePadding = false;
      this.xrTableCell429.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell432
      // 
      resources.ApplyResources(this.xrTableCell432, "xrTableCell432");
      this.xrTableCell432.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell432.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SAPInformation.EngineNo")});
      this.xrTableCell432.Name = "xrTableCell432";
      this.xrTableCell432.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell432.StylePriority.UseBackColor = false;
      this.xrTableCell432.StylePriority.UseBorderColor = false;
      this.xrTableCell432.StylePriority.UseBorders = false;
      this.xrTableCell432.StylePriority.UseFont = false;
      this.xrTableCell432.StylePriority.UseForeColor = false;
      this.xrTableCell432.StylePriority.UsePadding = false;
      this.xrTableCell432.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow167
      // 
      this.xrTableRow167.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell433,
            this.xrTableCell436});
      resources.ApplyResources(this.xrTableRow167, "xrTableRow167");
      this.xrTableRow167.Name = "xrTableRow167";
      // 
      // xrTableCell433
      // 
      resources.ApplyResources(this.xrTableCell433, "xrTableCell433");
      this.xrTableCell433.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell433.Name = "xrTableCell433";
      this.xrTableCell433.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell433.StylePriority.UseBackColor = false;
      this.xrTableCell433.StylePriority.UseBorderColor = false;
      this.xrTableCell433.StylePriority.UseBorders = false;
      this.xrTableCell433.StylePriority.UseFont = false;
      this.xrTableCell433.StylePriority.UseForeColor = false;
      this.xrTableCell433.StylePriority.UsePadding = false;
      this.xrTableCell433.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell436
      // 
      resources.ApplyResources(this.xrTableCell436, "xrTableCell436");
      this.xrTableCell436.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell436.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "VehicleInformation.RegistrationNo")});
      this.xrTableCell436.Name = "xrTableCell436";
      this.xrTableCell436.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell436.StylePriority.UseBackColor = false;
      this.xrTableCell436.StylePriority.UseBorderColor = false;
      this.xrTableCell436.StylePriority.UseBorders = false;
      this.xrTableCell436.StylePriority.UseFont = false;
      this.xrTableCell436.StylePriority.UseForeColor = false;
      this.xrTableCell436.StylePriority.UsePadding = false;
      this.xrTableCell436.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow171
      // 
      this.xrTableRow171.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell434,
            this.xrTableCell435});
      resources.ApplyResources(this.xrTableRow171, "xrTableRow171");
      this.xrTableRow171.Name = "xrTableRow171";
      // 
      // xrTableCell434
      // 
      resources.ApplyResources(this.xrTableCell434, "xrTableCell434");
      this.xrTableCell434.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell434.Name = "xrTableCell434";
      this.xrTableCell434.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell434.StylePriority.UseBackColor = false;
      this.xrTableCell434.StylePriority.UseBorderColor = false;
      this.xrTableCell434.StylePriority.UseBorders = false;
      this.xrTableCell434.StylePriority.UseFont = false;
      this.xrTableCell434.StylePriority.UseForeColor = false;
      this.xrTableCell434.StylePriority.UsePadding = false;
      this.xrTableCell434.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell435
      // 
      resources.ApplyResources(this.xrTableCell435, "xrTableCell435");
      this.xrTableCell435.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell435.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SAPInformation.ResultCode", "{0}")});
      this.xrTableCell435.Name = "xrTableCell435";
      this.xrTableCell435.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell435.StylePriority.UseBackColor = false;
      this.xrTableCell435.StylePriority.UseBorderColor = false;
      this.xrTableCell435.StylePriority.UseBorders = false;
      this.xrTableCell435.StylePriority.UseFont = false;
      this.xrTableCell435.StylePriority.UseForeColor = false;
      this.xrTableCell435.StylePriority.UsePadding = false;
      this.xrTableCell435.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow170
      // 
      this.xrTableRow170.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell430,
            this.xrTableCell431});
      resources.ApplyResources(this.xrTableRow170, "xrTableRow170");
      this.xrTableRow170.Name = "xrTableRow170";
      // 
      // xrTableCell430
      // 
      resources.ApplyResources(this.xrTableCell430, "xrTableCell430");
      this.xrTableCell430.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell430.Name = "xrTableCell430";
      this.xrTableCell430.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell430.StylePriority.UseBackColor = false;
      this.xrTableCell430.StylePriority.UseBorderColor = false;
      this.xrTableCell430.StylePriority.UseBorders = false;
      this.xrTableCell430.StylePriority.UseFont = false;
      this.xrTableCell430.StylePriority.UseForeColor = false;
      this.xrTableCell430.StylePriority.UsePadding = false;
      this.xrTableCell430.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell431
      // 
      resources.ApplyResources(this.xrTableCell431, "xrTableCell431");
      this.xrTableCell431.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell431.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SAPInformation.CaseNo")});
      this.xrTableCell431.Name = "xrTableCell431";
      this.xrTableCell431.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell431.StylePriority.UseBackColor = false;
      this.xrTableCell431.StylePriority.UseBorderColor = false;
      this.xrTableCell431.StylePriority.UseBorders = false;
      this.xrTableCell431.StylePriority.UseFont = false;
      this.xrTableCell431.StylePriority.UseForeColor = false;
      this.xrTableCell431.StylePriority.UsePadding = false;
      this.xrTableCell431.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow169
      // 
      this.xrTableRow169.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell426,
            this.xrTableCell427});
      resources.ApplyResources(this.xrTableRow169, "xrTableRow169");
      this.xrTableRow169.Name = "xrTableRow169";
      // 
      // xrTableCell426
      // 
      resources.ApplyResources(this.xrTableCell426, "xrTableCell426");
      this.xrTableCell426.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell426.Name = "xrTableCell426";
      this.xrTableCell426.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell426.StylePriority.UseBackColor = false;
      this.xrTableCell426.StylePriority.UseBorderColor = false;
      this.xrTableCell426.StylePriority.UseBorders = false;
      this.xrTableCell426.StylePriority.UseFont = false;
      this.xrTableCell426.StylePriority.UseForeColor = false;
      this.xrTableCell426.StylePriority.UsePadding = false;
      this.xrTableCell426.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell427
      // 
      resources.ApplyResources(this.xrTableCell427, "xrTableCell427");
      this.xrTableCell427.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell427.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SAPInformation.CirculationNo")});
      this.xrTableCell427.Name = "xrTableCell427";
      this.xrTableCell427.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell427.StylePriority.UseBackColor = false;
      this.xrTableCell427.StylePriority.UseBorderColor = false;
      this.xrTableCell427.StylePriority.UseBorders = false;
      this.xrTableCell427.StylePriority.UseFont = false;
      this.xrTableCell427.StylePriority.UseForeColor = false;
      this.xrTableCell427.StylePriority.UsePadding = false;
      this.xrTableCell427.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow168
      // 
      this.xrTableRow168.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell437,
            this.xrTableCell440});
      resources.ApplyResources(this.xrTableRow168, "xrTableRow168");
      this.xrTableRow168.Name = "xrTableRow168";
      // 
      // xrTableCell437
      // 
      resources.ApplyResources(this.xrTableCell437, "xrTableCell437");
      this.xrTableCell437.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell437.Name = "xrTableCell437";
      this.xrTableCell437.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell437.StylePriority.UseBackColor = false;
      this.xrTableCell437.StylePriority.UseBorderColor = false;
      this.xrTableCell437.StylePriority.UseBorders = false;
      this.xrTableCell437.StylePriority.UseFont = false;
      this.xrTableCell437.StylePriority.UseForeColor = false;
      this.xrTableCell437.StylePriority.UsePadding = false;
      this.xrTableCell437.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell440
      // 
      resources.ApplyResources(this.xrTableCell440, "xrTableCell440");
      this.xrTableCell440.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell440.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SAPInformation.PoliceStation")});
      this.xrTableCell440.Name = "xrTableCell440";
      this.xrTableCell440.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell440.StylePriority.UseBackColor = false;
      this.xrTableCell440.StylePriority.UseBorderColor = false;
      this.xrTableCell440.StylePriority.UseBorders = false;
      this.xrTableCell440.StylePriority.UseFont = false;
      this.xrTableCell440.StylePriority.UseForeColor = false;
      this.xrTableCell440.StylePriority.UsePadding = false;
      this.xrTableCell440.StylePriority.UseTextAlignment = false;
      // 
      // GroupHeader16
      // 
      this.GroupHeader16.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable36});
      resources.ApplyResources(this.GroupHeader16, "GroupHeader16");
      this.GroupHeader16.Name = "GroupHeader16";
      // 
      // xrTable36
      // 
      this.xrTable36.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      resources.ApplyResources(this.xrTable36, "xrTable36");
      this.xrTable36.Name = "xrTable36";
      this.xrTable36.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
      this.xrTable36.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow162,
            this.xrTableRow163});
      this.xrTable36.StylePriority.UseBorders = false;
      this.xrTable36.StylePriority.UseFont = false;
      this.xrTable36.StylePriority.UseForeColor = false;
      this.xrTable36.StylePriority.UsePadding = false;
      this.xrTable36.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow162
      // 
      this.xrTableRow162.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell421,
            this.xrTableCell422});
      resources.ApplyResources(this.xrTableRow162, "xrTableRow162");
      this.xrTableRow162.Name = "xrTableRow162";
      // 
      // xrTableCell421
      // 
      resources.ApplyResources(this.xrTableCell421, "xrTableCell421");
      this.xrTableCell421.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell421.Name = "xrTableCell421";
      this.xrTableCell421.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell421.StylePriority.UseBackColor = false;
      this.xrTableCell421.StylePriority.UseBorderColor = false;
      this.xrTableCell421.StylePriority.UseBorders = false;
      this.xrTableCell421.StylePriority.UseFont = false;
      this.xrTableCell421.StylePriority.UseForeColor = false;
      this.xrTableCell421.StylePriority.UsePadding = false;
      this.xrTableCell421.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell422
      // 
      resources.ApplyResources(this.xrTableCell422, "xrTableCell422");
      this.xrTableCell422.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell422.BorderWidth = 1F;
      this.xrTableCell422.Name = "xrTableCell422";
      this.xrTableCell422.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell422.StylePriority.UseBackColor = false;
      this.xrTableCell422.StylePriority.UseBorderColor = false;
      this.xrTableCell422.StylePriority.UseBorders = false;
      this.xrTableCell422.StylePriority.UseBorderWidth = false;
      this.xrTableCell422.StylePriority.UseFont = false;
      this.xrTableCell422.StylePriority.UseForeColor = false;
      this.xrTableCell422.StylePriority.UsePadding = false;
      // 
      // xrTableRow163
      // 
      this.xrTableRow163.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell423});
      resources.ApplyResources(this.xrTableRow163, "xrTableRow163");
      this.xrTableRow163.Name = "xrTableRow163";
      // 
      // xrTableCell423
      // 
      this.xrTableCell423.Borders = DevExpress.XtraPrinting.BorderSide.None;
      resources.ApplyResources(this.xrTableCell423, "xrTableCell423");
      this.xrTableCell423.Multiline = true;
      this.xrTableCell423.Name = "xrTableCell423";
      this.xrTableCell423.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTableCell423.StylePriority.UseBorders = false;
      this.xrTableCell423.StylePriority.UseFont = false;
      this.xrTableCell423.StylePriority.UseForeColor = false;
      this.xrTableCell423.StylePriority.UsePadding = false;
      // 
      // TelephoneLinkage
      // 
      this.TelephoneLinkage.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail29,
            this.GroupHeader1,
            this.HomeLinkage,
            this.WorkLinkage,
            this.CellularLinkage});
      resources.ApplyResources(this.TelephoneLinkage, "TelephoneLinkage");
      this.TelephoneLinkage.Level = 10;
      this.TelephoneLinkage.Name = "TelephoneLinkage";
      // 
      // Detail29
      // 
      resources.ApplyResources(this.Detail29, "Detail29");
      this.Detail29.Name = "Detail29";
      // 
      // GroupHeader1
      // 
      this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblLikagesHC});
      resources.ApplyResources(this.GroupHeader1, "GroupHeader1");
      this.GroupHeader1.Name = "GroupHeader1";
      // 
      // tblLikagesHC
      // 
      this.tblLikagesHC.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
      resources.ApplyResources(this.tblLikagesHC, "tblLikagesHC");
      this.tblLikagesHC.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.tblLikagesHC.Name = "tblLikagesHC";
      this.tblLikagesHC.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.tblLikagesHC.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9});
      this.tblLikagesHC.StylePriority.UseBorderColor = false;
      this.tblLikagesHC.StylePriority.UseBorders = false;
      this.tblLikagesHC.StylePriority.UsePadding = false;
      // 
      // xrTableRow9
      // 
      this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell24,
            this.xrTableCell28});
      resources.ApplyResources(this.xrTableRow9, "xrTableRow9");
      this.xrTableRow9.Name = "xrTableRow9";
      // 
      // xrTableCell24
      // 
      resources.ApplyResources(this.xrTableCell24, "xrTableCell24");
      this.xrTableCell24.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell24.CanGrow = false;
      this.xrTableCell24.Name = "xrTableCell24";
      this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell24.StylePriority.UseBackColor = false;
      this.xrTableCell24.StylePriority.UseBorderColor = false;
      this.xrTableCell24.StylePriority.UseBorders = false;
      this.xrTableCell24.StylePriority.UseFont = false;
      this.xrTableCell24.StylePriority.UseForeColor = false;
      this.xrTableCell24.StylePriority.UsePadding = false;
      this.xrTableCell24.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell28
      // 
      resources.ApplyResources(this.xrTableCell28, "xrTableCell28");
      this.xrTableCell28.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell28.CanGrow = false;
      this.xrTableCell28.Name = "xrTableCell28";
      this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell28.StylePriority.UseBackColor = false;
      this.xrTableCell28.StylePriority.UseBorderColor = false;
      this.xrTableCell28.StylePriority.UseBorders = false;
      this.xrTableCell28.StylePriority.UseFont = false;
      this.xrTableCell28.StylePriority.UseForeColor = false;
      this.xrTableCell28.StylePriority.UsePadding = false;
      // 
      // HomeLinkage
      // 
      this.HomeLinkage.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.GroupHeader2});
      this.HomeLinkage.DataMember = "ConsumerTelephoneLinkageHome";
      resources.ApplyResources(this.HomeLinkage, "HomeLinkage");
      this.HomeLinkage.Level = 0;
      this.HomeLinkage.Name = "HomeLinkage";
      // 
      // Detail1
      // 
      this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
      resources.ApplyResources(this.Detail1, "Detail1");
      this.Detail1.Name = "Detail1";
      // 
      // xrTable2
      // 
      resources.ApplyResources(this.xrTable2, "xrTable2");
      this.xrTable2.Name = "xrTable2";
      this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
      this.xrTable2.StylePriority.UseBackColor = false;
      this.xrTable2.StylePriority.UseFont = false;
      this.xrTable2.StylePriority.UsePadding = false;
      // 
      // xrTableRow3
      // 
      resources.ApplyResources(this.xrTableRow3, "xrTableRow3");
      this.xrTableRow3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell13,
            this.xrTableCell14,
            this.xrTableCell15});
      this.xrTableRow3.Name = "xrTableRow3";
      this.xrTableRow3.StylePriority.UseBackColor = false;
      this.xrTableRow3.StylePriority.UseBorderColor = false;
      this.xrTableRow3.StylePriority.UseBorders = false;
      this.xrTableRow3.StylePriority.UseFont = false;
      this.xrTableRow3.StylePriority.UseForeColor = false;
      this.xrTableRow3.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell1
      // 
      resources.ApplyResources(this.xrTableCell1, "xrTableCell1");
      this.xrTableCell1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneLinkageWork.ConsumerName")});
      this.xrTableCell1.Name = "xrTableCell1";
      this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell1.StylePriority.UseBackColor = false;
      this.xrTableCell1.StylePriority.UseBorderColor = false;
      this.xrTableCell1.StylePriority.UseFont = false;
      this.xrTableCell1.StylePriority.UseForeColor = false;
      this.xrTableCell1.StylePriority.UsePadding = false;
      this.xrTableCell1.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell2
      // 
      resources.ApplyResources(this.xrTableCell2, "xrTableCell2");
      this.xrTableCell2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneLinkageHome.Surname")});
      this.xrTableCell2.Name = "xrTableCell2";
      this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell2.StylePriority.UseBackColor = false;
      this.xrTableCell2.StylePriority.UseBorderColor = false;
      this.xrTableCell2.StylePriority.UseFont = false;
      this.xrTableCell2.StylePriority.UseForeColor = false;
      this.xrTableCell2.StylePriority.UsePadding = false;
      this.xrTableCell2.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell13
      // 
      resources.ApplyResources(this.xrTableCell13, "xrTableCell13");
      this.xrTableCell13.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneLinkageHome.HomeTelephone")});
      this.xrTableCell13.Name = "xrTableCell13";
      this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell13.StylePriority.UseBackColor = false;
      this.xrTableCell13.StylePriority.UseBorderColor = false;
      this.xrTableCell13.StylePriority.UseFont = false;
      this.xrTableCell13.StylePriority.UseForeColor = false;
      this.xrTableCell13.StylePriority.UsePadding = false;
      this.xrTableCell13.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell14
      // 
      resources.ApplyResources(this.xrTableCell14, "xrTableCell14");
      this.xrTableCell14.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneLinkageHome.WorkTelephone")});
      this.xrTableCell14.Name = "xrTableCell14";
      this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell14.StylePriority.UseBackColor = false;
      this.xrTableCell14.StylePriority.UseBorderColor = false;
      this.xrTableCell14.StylePriority.UseFont = false;
      this.xrTableCell14.StylePriority.UseForeColor = false;
      this.xrTableCell14.StylePriority.UsePadding = false;
      this.xrTableCell14.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell15
      // 
      resources.ApplyResources(this.xrTableCell15, "xrTableCell15");
      this.xrTableCell15.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneLinkageHome.CellularNo")});
      this.xrTableCell15.Name = "xrTableCell15";
      this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell15.StylePriority.UseBackColor = false;
      this.xrTableCell15.StylePriority.UseBorderColor = false;
      this.xrTableCell15.StylePriority.UseFont = false;
      this.xrTableCell15.StylePriority.UseForeColor = false;
      this.xrTableCell15.StylePriority.UsePadding = false;
      this.xrTableCell15.StylePriority.UseTextAlignment = false;
      // 
      // GroupHeader2
      // 
      this.GroupHeader2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
      resources.ApplyResources(this.GroupHeader2, "GroupHeader2");
      this.GroupHeader2.Name = "GroupHeader2";
      // 
      // xrTable1
      // 
      this.xrTable1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
      resources.ApplyResources(this.xrTable1, "xrTable1");
      this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTable1.Name = "xrTable1";
      this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5,
            this.xrTableRow6});
      this.xrTable1.StylePriority.UseBorderColor = false;
      this.xrTable1.StylePriority.UseBorders = false;
      this.xrTable1.StylePriority.UsePadding = false;
      // 
      // xrTableRow5
      // 
      this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3});
      resources.ApplyResources(this.xrTableRow5, "xrTableRow5");
      this.xrTableRow5.Name = "xrTableRow5";
      // 
      // xrTableCell3
      // 
      resources.ApplyResources(this.xrTableCell3, "xrTableCell3");
      this.xrTableCell3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell3.CanGrow = false;
      this.xrTableCell3.Name = "xrTableCell3";
      this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell3.StylePriority.UseBackColor = false;
      this.xrTableCell3.StylePriority.UseBorderColor = false;
      this.xrTableCell3.StylePriority.UseBorders = false;
      this.xrTableCell3.StylePriority.UseFont = false;
      this.xrTableCell3.StylePriority.UseForeColor = false;
      this.xrTableCell3.StylePriority.UsePadding = false;
      this.xrTableCell3.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow6
      // 
      resources.ApplyResources(this.xrTableRow6, "xrTableRow6");
      this.xrTableRow6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8,
            this.xrTableCell9,
            this.xrTableCell10,
            this.xrTableCell11,
            this.xrTableCell12});
      this.xrTableRow6.Name = "xrTableRow6";
      this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTableRow6.StylePriority.UseBackColor = false;
      this.xrTableRow6.StylePriority.UseBorderColor = false;
      this.xrTableRow6.StylePriority.UseBorders = false;
      this.xrTableRow6.StylePriority.UseFont = false;
      this.xrTableRow6.StylePriority.UseForeColor = false;
      this.xrTableRow6.StylePriority.UsePadding = false;
      this.xrTableRow6.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell8
      // 
      this.xrTableCell8.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell8.CanGrow = false;
      resources.ApplyResources(this.xrTableCell8, "xrTableCell8");
      this.xrTableCell8.Name = "xrTableCell8";
      this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell8.StylePriority.UseBorders = false;
      this.xrTableCell8.StylePriority.UseFont = false;
      this.xrTableCell8.StylePriority.UseForeColor = false;
      this.xrTableCell8.StylePriority.UsePadding = false;
      // 
      // xrTableCell9
      // 
      this.xrTableCell9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell9.CanGrow = false;
      resources.ApplyResources(this.xrTableCell9, "xrTableCell9");
      this.xrTableCell9.Name = "xrTableCell9";
      this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell9.StylePriority.UseBorders = false;
      this.xrTableCell9.StylePriority.UseFont = false;
      this.xrTableCell9.StylePriority.UseForeColor = false;
      this.xrTableCell9.StylePriority.UsePadding = false;
      // 
      // xrTableCell10
      // 
      this.xrTableCell10.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell10.CanGrow = false;
      resources.ApplyResources(this.xrTableCell10, "xrTableCell10");
      this.xrTableCell10.Name = "xrTableCell10";
      this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell10.StylePriority.UseBorders = false;
      this.xrTableCell10.StylePriority.UseFont = false;
      this.xrTableCell10.StylePriority.UseForeColor = false;
      this.xrTableCell10.StylePriority.UsePadding = false;
      // 
      // xrTableCell11
      // 
      this.xrTableCell11.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell11.CanGrow = false;
      resources.ApplyResources(this.xrTableCell11, "xrTableCell11");
      this.xrTableCell11.Name = "xrTableCell11";
      this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell11.StylePriority.UseBorders = false;
      this.xrTableCell11.StylePriority.UseFont = false;
      this.xrTableCell11.StylePriority.UseForeColor = false;
      this.xrTableCell11.StylePriority.UsePadding = false;
      // 
      // xrTableCell12
      // 
      this.xrTableCell12.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell12.CanGrow = false;
      resources.ApplyResources(this.xrTableCell12, "xrTableCell12");
      this.xrTableCell12.Name = "xrTableCell12";
      this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell12.StylePriority.UseBorders = false;
      this.xrTableCell12.StylePriority.UseFont = false;
      this.xrTableCell12.StylePriority.UseForeColor = false;
      this.xrTableCell12.StylePriority.UsePadding = false;
      // 
      // WorkLinkage
      // 
      this.WorkLinkage.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail2,
            this.GroupHeader3});
      this.WorkLinkage.DataMember = "ConsumerTelephoneLinkageWork";
      resources.ApplyResources(this.WorkLinkage, "WorkLinkage");
      this.WorkLinkage.Level = 1;
      this.WorkLinkage.Name = "WorkLinkage";
      // 
      // Detail2
      // 
      this.Detail2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
      resources.ApplyResources(this.Detail2, "Detail2");
      this.Detail2.Name = "Detail2";
      // 
      // xrTable5
      // 
      resources.ApplyResources(this.xrTable5, "xrTable5");
      this.xrTable5.Name = "xrTable5";
      this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow11});
      this.xrTable5.StylePriority.UseBackColor = false;
      this.xrTable5.StylePriority.UseFont = false;
      this.xrTable5.StylePriority.UsePadding = false;
      // 
      // xrTableRow11
      // 
      resources.ApplyResources(this.xrTableRow11, "xrTableRow11");
      this.xrTableRow11.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell30,
            this.xrTableCell31,
            this.xrTableCell33,
            this.xrTableCell34,
            this.xrTableCell35});
      this.xrTableRow11.Name = "xrTableRow11";
      this.xrTableRow11.StylePriority.UseBackColor = false;
      this.xrTableRow11.StylePriority.UseBorderColor = false;
      this.xrTableRow11.StylePriority.UseBorders = false;
      this.xrTableRow11.StylePriority.UseFont = false;
      this.xrTableRow11.StylePriority.UseForeColor = false;
      this.xrTableRow11.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell30
      // 
      resources.ApplyResources(this.xrTableCell30, "xrTableCell30");
      this.xrTableCell30.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneLinkageWork.ConsumerName")});
      this.xrTableCell30.Name = "xrTableCell30";
      this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell30.StylePriority.UseBackColor = false;
      this.xrTableCell30.StylePriority.UseBorderColor = false;
      this.xrTableCell30.StylePriority.UseFont = false;
      this.xrTableCell30.StylePriority.UseForeColor = false;
      this.xrTableCell30.StylePriority.UsePadding = false;
      this.xrTableCell30.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell31
      // 
      resources.ApplyResources(this.xrTableCell31, "xrTableCell31");
      this.xrTableCell31.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneLinkageWork.Surname")});
      this.xrTableCell31.Name = "xrTableCell31";
      this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell31.StylePriority.UseBackColor = false;
      this.xrTableCell31.StylePriority.UseBorderColor = false;
      this.xrTableCell31.StylePriority.UseFont = false;
      this.xrTableCell31.StylePriority.UseForeColor = false;
      this.xrTableCell31.StylePriority.UsePadding = false;
      this.xrTableCell31.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell33
      // 
      resources.ApplyResources(this.xrTableCell33, "xrTableCell33");
      this.xrTableCell33.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneLinkageWork.HomeTelephone")});
      this.xrTableCell33.Name = "xrTableCell33";
      this.xrTableCell33.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell33.StylePriority.UseBackColor = false;
      this.xrTableCell33.StylePriority.UseBorderColor = false;
      this.xrTableCell33.StylePriority.UseFont = false;
      this.xrTableCell33.StylePriority.UseForeColor = false;
      this.xrTableCell33.StylePriority.UsePadding = false;
      this.xrTableCell33.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell34
      // 
      resources.ApplyResources(this.xrTableCell34, "xrTableCell34");
      this.xrTableCell34.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneLinkageWork.WorkTelephone")});
      this.xrTableCell34.Name = "xrTableCell34";
      this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell34.StylePriority.UseBackColor = false;
      this.xrTableCell34.StylePriority.UseBorderColor = false;
      this.xrTableCell34.StylePriority.UseFont = false;
      this.xrTableCell34.StylePriority.UseForeColor = false;
      this.xrTableCell34.StylePriority.UsePadding = false;
      this.xrTableCell34.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell35
      // 
      resources.ApplyResources(this.xrTableCell35, "xrTableCell35");
      this.xrTableCell35.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneLinkageWork.CellularNo")});
      this.xrTableCell35.Name = "xrTableCell35";
      this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell35.StylePriority.UseBackColor = false;
      this.xrTableCell35.StylePriority.UseBorderColor = false;
      this.xrTableCell35.StylePriority.UseFont = false;
      this.xrTableCell35.StylePriority.UseForeColor = false;
      this.xrTableCell35.StylePriority.UsePadding = false;
      this.xrTableCell35.StylePriority.UseTextAlignment = false;
      // 
      // GroupHeader3
      // 
      this.GroupHeader3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
      resources.ApplyResources(this.GroupHeader3, "GroupHeader3");
      this.GroupHeader3.Name = "GroupHeader3";
      // 
      // xrTable3
      // 
      this.xrTable3.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
      resources.ApplyResources(this.xrTable3, "xrTable3");
      this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTable3.Name = "xrTable3";
      this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow7});
      this.xrTable3.StylePriority.UseBorderColor = false;
      this.xrTable3.StylePriority.UseBorders = false;
      this.xrTable3.StylePriority.UsePadding = false;
      // 
      // xrTableRow1
      // 
      this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell16});
      resources.ApplyResources(this.xrTableRow1, "xrTableRow1");
      this.xrTableRow1.Name = "xrTableRow1";
      // 
      // xrTableCell16
      // 
      resources.ApplyResources(this.xrTableCell16, "xrTableCell16");
      this.xrTableCell16.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell16.CanGrow = false;
      this.xrTableCell16.Name = "xrTableCell16";
      this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell16.StylePriority.UseBackColor = false;
      this.xrTableCell16.StylePriority.UseBorderColor = false;
      this.xrTableCell16.StylePriority.UseBorders = false;
      this.xrTableCell16.StylePriority.UseFont = false;
      this.xrTableCell16.StylePriority.UseForeColor = false;
      this.xrTableCell16.StylePriority.UsePadding = false;
      this.xrTableCell16.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow7
      // 
      resources.ApplyResources(this.xrTableRow7, "xrTableRow7");
      this.xrTableRow7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell17,
            this.xrTableCell18,
            this.xrTableCell19,
            this.xrTableCell20,
            this.xrTableCell21});
      this.xrTableRow7.Name = "xrTableRow7";
      this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTableRow7.StylePriority.UseBackColor = false;
      this.xrTableRow7.StylePriority.UseBorderColor = false;
      this.xrTableRow7.StylePriority.UseBorders = false;
      this.xrTableRow7.StylePriority.UseFont = false;
      this.xrTableRow7.StylePriority.UseForeColor = false;
      this.xrTableRow7.StylePriority.UsePadding = false;
      this.xrTableRow7.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell17
      // 
      this.xrTableCell17.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell17.CanGrow = false;
      resources.ApplyResources(this.xrTableCell17, "xrTableCell17");
      this.xrTableCell17.Name = "xrTableCell17";
      this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell17.StylePriority.UseBorders = false;
      this.xrTableCell17.StylePriority.UseFont = false;
      this.xrTableCell17.StylePriority.UseForeColor = false;
      this.xrTableCell17.StylePriority.UsePadding = false;
      // 
      // xrTableCell18
      // 
      this.xrTableCell18.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell18.CanGrow = false;
      resources.ApplyResources(this.xrTableCell18, "xrTableCell18");
      this.xrTableCell18.Name = "xrTableCell18";
      this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell18.StylePriority.UseBorders = false;
      this.xrTableCell18.StylePriority.UseFont = false;
      this.xrTableCell18.StylePriority.UseForeColor = false;
      this.xrTableCell18.StylePriority.UsePadding = false;
      // 
      // xrTableCell19
      // 
      this.xrTableCell19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell19.CanGrow = false;
      resources.ApplyResources(this.xrTableCell19, "xrTableCell19");
      this.xrTableCell19.Name = "xrTableCell19";
      this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell19.StylePriority.UseBorders = false;
      this.xrTableCell19.StylePriority.UseFont = false;
      this.xrTableCell19.StylePriority.UseForeColor = false;
      this.xrTableCell19.StylePriority.UsePadding = false;
      // 
      // xrTableCell20
      // 
      this.xrTableCell20.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell20.CanGrow = false;
      resources.ApplyResources(this.xrTableCell20, "xrTableCell20");
      this.xrTableCell20.Name = "xrTableCell20";
      this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell20.StylePriority.UseBorders = false;
      this.xrTableCell20.StylePriority.UseFont = false;
      this.xrTableCell20.StylePriority.UseForeColor = false;
      this.xrTableCell20.StylePriority.UsePadding = false;
      // 
      // xrTableCell21
      // 
      this.xrTableCell21.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell21.CanGrow = false;
      resources.ApplyResources(this.xrTableCell21, "xrTableCell21");
      this.xrTableCell21.Name = "xrTableCell21";
      this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell21.StylePriority.UseBorders = false;
      this.xrTableCell21.StylePriority.UseFont = false;
      this.xrTableCell21.StylePriority.UseForeColor = false;
      this.xrTableCell21.StylePriority.UsePadding = false;
      // 
      // CellularLinkage
      // 
      this.CellularLinkage.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail3,
            this.GroupHeader4});
      this.CellularLinkage.DataMember = "ConsumerTelephoneLinkageCellular";
      resources.ApplyResources(this.CellularLinkage, "CellularLinkage");
      this.CellularLinkage.Level = 2;
      this.CellularLinkage.Name = "CellularLinkage";
      // 
      // Detail3
      // 
      this.Detail3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
      resources.ApplyResources(this.Detail3, "Detail3");
      this.Detail3.Name = "Detail3";
      // 
      // xrTable6
      // 
      resources.ApplyResources(this.xrTable6, "xrTable6");
      this.xrTable6.Name = "xrTable6";
      this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow12});
      this.xrTable6.StylePriority.UseBackColor = false;
      this.xrTable6.StylePriority.UseFont = false;
      this.xrTable6.StylePriority.UsePadding = false;
      // 
      // xrTableRow12
      // 
      resources.ApplyResources(this.xrTableRow12, "xrTableRow12");
      this.xrTableRow12.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell36,
            this.xrTableCell37,
            this.xrTableCell38,
            this.xrTableCell39,
            this.xrTableCell40});
      this.xrTableRow12.Name = "xrTableRow12";
      this.xrTableRow12.StylePriority.UseBackColor = false;
      this.xrTableRow12.StylePriority.UseBorderColor = false;
      this.xrTableRow12.StylePriority.UseBorders = false;
      this.xrTableRow12.StylePriority.UseFont = false;
      this.xrTableRow12.StylePriority.UseForeColor = false;
      this.xrTableRow12.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell36
      // 
      resources.ApplyResources(this.xrTableCell36, "xrTableCell36");
      this.xrTableCell36.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneLinkageCellular.ConsumerName")});
      this.xrTableCell36.Name = "xrTableCell36";
      this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell36.StylePriority.UseBackColor = false;
      this.xrTableCell36.StylePriority.UseBorderColor = false;
      this.xrTableCell36.StylePriority.UseFont = false;
      this.xrTableCell36.StylePriority.UseForeColor = false;
      this.xrTableCell36.StylePriority.UsePadding = false;
      this.xrTableCell36.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell37
      // 
      resources.ApplyResources(this.xrTableCell37, "xrTableCell37");
      this.xrTableCell37.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneLinkageCellular.Surname")});
      this.xrTableCell37.Name = "xrTableCell37";
      this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell37.StylePriority.UseBackColor = false;
      this.xrTableCell37.StylePriority.UseBorderColor = false;
      this.xrTableCell37.StylePriority.UseFont = false;
      this.xrTableCell37.StylePriority.UseForeColor = false;
      this.xrTableCell37.StylePriority.UsePadding = false;
      this.xrTableCell37.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell38
      // 
      resources.ApplyResources(this.xrTableCell38, "xrTableCell38");
      this.xrTableCell38.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneLinkageCellular.HomeTelephone")});
      this.xrTableCell38.Name = "xrTableCell38";
      this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell38.StylePriority.UseBackColor = false;
      this.xrTableCell38.StylePriority.UseBorderColor = false;
      this.xrTableCell38.StylePriority.UseFont = false;
      this.xrTableCell38.StylePriority.UseForeColor = false;
      this.xrTableCell38.StylePriority.UsePadding = false;
      this.xrTableCell38.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell39
      // 
      resources.ApplyResources(this.xrTableCell39, "xrTableCell39");
      this.xrTableCell39.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneLinkageCellular.WorkTelephone")});
      this.xrTableCell39.Name = "xrTableCell39";
      this.xrTableCell39.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell39.StylePriority.UseBackColor = false;
      this.xrTableCell39.StylePriority.UseBorderColor = false;
      this.xrTableCell39.StylePriority.UseFont = false;
      this.xrTableCell39.StylePriority.UseForeColor = false;
      this.xrTableCell39.StylePriority.UsePadding = false;
      this.xrTableCell39.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell40
      // 
      resources.ApplyResources(this.xrTableCell40, "xrTableCell40");
      this.xrTableCell40.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerTelephoneLinkageCellular.CellularNo")});
      this.xrTableCell40.Name = "xrTableCell40";
      this.xrTableCell40.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell40.StylePriority.UseBackColor = false;
      this.xrTableCell40.StylePriority.UseBorderColor = false;
      this.xrTableCell40.StylePriority.UseFont = false;
      this.xrTableCell40.StylePriority.UseForeColor = false;
      this.xrTableCell40.StylePriority.UsePadding = false;
      this.xrTableCell40.StylePriority.UseTextAlignment = false;
      // 
      // GroupHeader4
      // 
      this.GroupHeader4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
      resources.ApplyResources(this.GroupHeader4, "GroupHeader4");
      this.GroupHeader4.Name = "GroupHeader4";
      // 
      // xrTable4
      // 
      this.xrTable4.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
      resources.ApplyResources(this.xrTable4, "xrTable4");
      this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTable4.Name = "xrTable4";
      this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8,
            this.xrTableRow10});
      this.xrTable4.StylePriority.UseBorderColor = false;
      this.xrTable4.StylePriority.UseBorders = false;
      this.xrTable4.StylePriority.UsePadding = false;
      // 
      // xrTableRow8
      // 
      this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell22});
      resources.ApplyResources(this.xrTableRow8, "xrTableRow8");
      this.xrTableRow8.Name = "xrTableRow8";
      // 
      // xrTableCell22
      // 
      resources.ApplyResources(this.xrTableCell22, "xrTableCell22");
      this.xrTableCell22.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell22.CanGrow = false;
      this.xrTableCell22.Name = "xrTableCell22";
      this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell22.StylePriority.UseBackColor = false;
      this.xrTableCell22.StylePriority.UseBorderColor = false;
      this.xrTableCell22.StylePriority.UseBorders = false;
      this.xrTableCell22.StylePriority.UseFont = false;
      this.xrTableCell22.StylePriority.UseForeColor = false;
      this.xrTableCell22.StylePriority.UsePadding = false;
      this.xrTableCell22.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow10
      // 
      resources.ApplyResources(this.xrTableRow10, "xrTableRow10");
      this.xrTableRow10.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell23,
            this.xrTableCell25,
            this.xrTableCell26,
            this.xrTableCell27,
            this.xrTableCell29});
      this.xrTableRow10.Name = "xrTableRow10";
      this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTableRow10.StylePriority.UseBackColor = false;
      this.xrTableRow10.StylePriority.UseBorderColor = false;
      this.xrTableRow10.StylePriority.UseBorders = false;
      this.xrTableRow10.StylePriority.UseFont = false;
      this.xrTableRow10.StylePriority.UseForeColor = false;
      this.xrTableRow10.StylePriority.UsePadding = false;
      this.xrTableRow10.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell23
      // 
      this.xrTableCell23.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell23.CanGrow = false;
      resources.ApplyResources(this.xrTableCell23, "xrTableCell23");
      this.xrTableCell23.Name = "xrTableCell23";
      this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell23.StylePriority.UseBorders = false;
      this.xrTableCell23.StylePriority.UseFont = false;
      this.xrTableCell23.StylePriority.UseForeColor = false;
      this.xrTableCell23.StylePriority.UsePadding = false;
      // 
      // xrTableCell25
      // 
      this.xrTableCell25.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell25.CanGrow = false;
      resources.ApplyResources(this.xrTableCell25, "xrTableCell25");
      this.xrTableCell25.Name = "xrTableCell25";
      this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell25.StylePriority.UseBorders = false;
      this.xrTableCell25.StylePriority.UseFont = false;
      this.xrTableCell25.StylePriority.UseForeColor = false;
      this.xrTableCell25.StylePriority.UsePadding = false;
      // 
      // xrTableCell26
      // 
      this.xrTableCell26.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell26.CanGrow = false;
      resources.ApplyResources(this.xrTableCell26, "xrTableCell26");
      this.xrTableCell26.Name = "xrTableCell26";
      this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell26.StylePriority.UseBorders = false;
      this.xrTableCell26.StylePriority.UseFont = false;
      this.xrTableCell26.StylePriority.UseForeColor = false;
      this.xrTableCell26.StylePriority.UsePadding = false;
      // 
      // xrTableCell27
      // 
      this.xrTableCell27.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell27.CanGrow = false;
      resources.ApplyResources(this.xrTableCell27, "xrTableCell27");
      this.xrTableCell27.Name = "xrTableCell27";
      this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell27.StylePriority.UseBorders = false;
      this.xrTableCell27.StylePriority.UseFont = false;
      this.xrTableCell27.StylePriority.UseForeColor = false;
      this.xrTableCell27.StylePriority.UsePadding = false;
      // 
      // xrTableCell29
      // 
      this.xrTableCell29.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell29.CanGrow = false;
      resources.ApplyResources(this.xrTableCell29, "xrTableCell29");
      this.xrTableCell29.Name = "xrTableCell29";
      this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell29.StylePriority.UseBorders = false;
      this.xrTableCell29.StylePriority.UseFont = false;
      this.xrTableCell29.StylePriority.UseForeColor = false;
      this.xrTableCell29.StylePriority.UsePadding = false;
      // 
      // Adverse
      // 
      this.Adverse.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail4,
            this.PublicDomain_Adverse,
            this.ReportHeader2,
            this.PublicDomain_Default,
            this.PublicDomain_Judgements,
            this.CourtNotice_Admin});
      resources.ApplyResources(this.Adverse, "Adverse");
      this.Adverse.Level = 11;
      this.Adverse.Name = "Adverse";
      // 
      // Detail4
      // 
      resources.ApplyResources(this.Detail4, "Detail4");
      this.Detail4.Name = "Detail4";
      // 
      // PublicDomain_Adverse
      // 
      this.PublicDomain_Adverse.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.AdverseDetail,
            this.AdverseHeader,
            this.ReportHeader1});
      resources.ApplyResources(this.PublicDomain_Adverse, "PublicDomain_Adverse");
      this.PublicDomain_Adverse.Level = 0;
      this.PublicDomain_Adverse.Name = "PublicDomain_Adverse";
      // 
      // AdverseDetail
      // 
      this.AdverseDetail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable13});
      resources.ApplyResources(this.AdverseDetail, "AdverseDetail");
      this.AdverseDetail.Name = "AdverseDetail";
      // 
      // xrTable13
      // 
      resources.ApplyResources(this.xrTable13, "xrTable13");
      this.xrTable13.Name = "xrTable13";
      this.xrTable13.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable13.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow18});
      this.xrTable13.StylePriority.UseBackColor = false;
      this.xrTable13.StylePriority.UseFont = false;
      this.xrTable13.StylePriority.UsePadding = false;
      // 
      // xrTableRow18
      // 
      resources.ApplyResources(this.xrTableRow18, "xrTableRow18");
      this.xrTableRow18.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell53,
            this.xrTableCell59,
            this.xrTableCell54,
            this.xrTableCell55,
            this.xrTableCell56,
            this.xrTableCell58});
      this.xrTableRow18.Name = "xrTableRow18";
      this.xrTableRow18.StylePriority.UseBackColor = false;
      this.xrTableRow18.StylePriority.UseBorderColor = false;
      this.xrTableRow18.StylePriority.UseBorders = false;
      this.xrTableRow18.StylePriority.UseFont = false;
      this.xrTableRow18.StylePriority.UseForeColor = false;
      this.xrTableRow18.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell53
      // 
      resources.ApplyResources(this.xrTableCell53, "xrTableCell53");
      this.xrTableCell53.Name = "xrTableCell53";
      this.xrTableCell53.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell53.StylePriority.UseBackColor = false;
      this.xrTableCell53.StylePriority.UseBorderColor = false;
      this.xrTableCell53.StylePriority.UseFont = false;
      this.xrTableCell53.StylePriority.UseForeColor = false;
      this.xrTableCell53.StylePriority.UsePadding = false;
      this.xrTableCell53.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell59
      // 
      resources.ApplyResources(this.xrTableCell59, "xrTableCell59");
      this.xrTableCell59.Name = "xrTableCell59";
      this.xrTableCell59.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell59.StylePriority.UseBackColor = false;
      this.xrTableCell59.StylePriority.UseBorderColor = false;
      this.xrTableCell59.StylePriority.UseFont = false;
      this.xrTableCell59.StylePriority.UseForeColor = false;
      this.xrTableCell59.StylePriority.UsePadding = false;
      this.xrTableCell59.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell54
      // 
      resources.ApplyResources(this.xrTableCell54, "xrTableCell54");
      this.xrTableCell54.Name = "xrTableCell54";
      this.xrTableCell54.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell54.StylePriority.UseBackColor = false;
      this.xrTableCell54.StylePriority.UseBorderColor = false;
      this.xrTableCell54.StylePriority.UseFont = false;
      this.xrTableCell54.StylePriority.UseForeColor = false;
      this.xrTableCell54.StylePriority.UsePadding = false;
      this.xrTableCell54.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell55
      // 
      resources.ApplyResources(this.xrTableCell55, "xrTableCell55");
      this.xrTableCell55.Name = "xrTableCell55";
      this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell55.StylePriority.UseBackColor = false;
      this.xrTableCell55.StylePriority.UseBorderColor = false;
      this.xrTableCell55.StylePriority.UseFont = false;
      this.xrTableCell55.StylePriority.UseForeColor = false;
      this.xrTableCell55.StylePriority.UsePadding = false;
      this.xrTableCell55.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell56
      // 
      resources.ApplyResources(this.xrTableCell56, "xrTableCell56");
      this.xrTableCell56.Name = "xrTableCell56";
      this.xrTableCell56.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell56.StylePriority.UseBackColor = false;
      this.xrTableCell56.StylePriority.UseBorderColor = false;
      this.xrTableCell56.StylePriority.UseFont = false;
      this.xrTableCell56.StylePriority.UseForeColor = false;
      this.xrTableCell56.StylePriority.UsePadding = false;
      this.xrTableCell56.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell58
      // 
      resources.ApplyResources(this.xrTableCell58, "xrTableCell58");
      this.xrTableCell58.Name = "xrTableCell58";
      this.xrTableCell58.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell58.StylePriority.UseBackColor = false;
      this.xrTableCell58.StylePriority.UseBorderColor = false;
      this.xrTableCell58.StylePriority.UseFont = false;
      this.xrTableCell58.StylePriority.UseForeColor = false;
      this.xrTableCell58.StylePriority.UsePadding = false;
      this.xrTableCell58.StylePriority.UseTextAlignment = false;
      // 
      // AdverseHeader
      // 
      this.AdverseHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable11});
      resources.ApplyResources(this.AdverseHeader, "AdverseHeader");
      this.AdverseHeader.Name = "AdverseHeader";
      // 
      // xrTable11
      // 
      this.xrTable11.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
      resources.ApplyResources(this.xrTable11, "xrTable11");
      this.xrTable11.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTable11.Name = "xrTable11";
      this.xrTable11.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow16,
            this.xrTableRow17});
      this.xrTable11.StylePriority.UseBorderColor = false;
      this.xrTable11.StylePriority.UseBorders = false;
      this.xrTable11.StylePriority.UsePadding = false;
      // 
      // xrTableRow16
      // 
      this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell45});
      resources.ApplyResources(this.xrTableRow16, "xrTableRow16");
      this.xrTableRow16.Name = "xrTableRow16";
      // 
      // xrTableCell45
      // 
      resources.ApplyResources(this.xrTableCell45, "xrTableCell45");
      this.xrTableCell45.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell45.CanGrow = false;
      this.xrTableCell45.Name = "xrTableCell45";
      this.xrTableCell45.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell45.StylePriority.UseBackColor = false;
      this.xrTableCell45.StylePriority.UseBorderColor = false;
      this.xrTableCell45.StylePriority.UseBorders = false;
      this.xrTableCell45.StylePriority.UseFont = false;
      this.xrTableCell45.StylePriority.UseForeColor = false;
      this.xrTableCell45.StylePriority.UsePadding = false;
      this.xrTableCell45.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow17
      // 
      resources.ApplyResources(this.xrTableRow17, "xrTableRow17");
      this.xrTableRow17.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell46,
            this.xrTableCell52,
            this.xrTableCell48,
            this.xrTableCell49,
            this.xrTableCell50,
            this.xrTableCell51});
      this.xrTableRow17.Name = "xrTableRow17";
      this.xrTableRow17.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTableRow17.StylePriority.UseBackColor = false;
      this.xrTableRow17.StylePriority.UseBorderColor = false;
      this.xrTableRow17.StylePriority.UseBorders = false;
      this.xrTableRow17.StylePriority.UseFont = false;
      this.xrTableRow17.StylePriority.UseForeColor = false;
      this.xrTableRow17.StylePriority.UsePadding = false;
      this.xrTableRow17.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell46
      // 
      this.xrTableCell46.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell46.CanGrow = false;
      resources.ApplyResources(this.xrTableCell46, "xrTableCell46");
      this.xrTableCell46.Name = "xrTableCell46";
      this.xrTableCell46.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell46.StylePriority.UseBorders = false;
      this.xrTableCell46.StylePriority.UseFont = false;
      this.xrTableCell46.StylePriority.UseForeColor = false;
      this.xrTableCell46.StylePriority.UsePadding = false;
      // 
      // xrTableCell52
      // 
      this.xrTableCell52.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell52.CanGrow = false;
      resources.ApplyResources(this.xrTableCell52, "xrTableCell52");
      this.xrTableCell52.Name = "xrTableCell52";
      this.xrTableCell52.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell52.StylePriority.UseBorders = false;
      this.xrTableCell52.StylePriority.UseFont = false;
      this.xrTableCell52.StylePriority.UseForeColor = false;
      this.xrTableCell52.StylePriority.UsePadding = false;
      // 
      // xrTableCell48
      // 
      this.xrTableCell48.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell48.CanGrow = false;
      resources.ApplyResources(this.xrTableCell48, "xrTableCell48");
      this.xrTableCell48.Name = "xrTableCell48";
      this.xrTableCell48.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell48.StylePriority.UseBorders = false;
      this.xrTableCell48.StylePriority.UseFont = false;
      this.xrTableCell48.StylePriority.UseForeColor = false;
      this.xrTableCell48.StylePriority.UsePadding = false;
      // 
      // xrTableCell49
      // 
      this.xrTableCell49.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell49.CanGrow = false;
      resources.ApplyResources(this.xrTableCell49, "xrTableCell49");
      this.xrTableCell49.Name = "xrTableCell49";
      this.xrTableCell49.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell49.StylePriority.UseBorders = false;
      this.xrTableCell49.StylePriority.UseFont = false;
      this.xrTableCell49.StylePriority.UseForeColor = false;
      this.xrTableCell49.StylePriority.UsePadding = false;
      // 
      // xrTableCell50
      // 
      this.xrTableCell50.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell50.CanGrow = false;
      resources.ApplyResources(this.xrTableCell50, "xrTableCell50");
      this.xrTableCell50.Name = "xrTableCell50";
      this.xrTableCell50.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell50.StylePriority.UseBorders = false;
      this.xrTableCell50.StylePriority.UseFont = false;
      this.xrTableCell50.StylePriority.UseForeColor = false;
      this.xrTableCell50.StylePriority.UsePadding = false;
      // 
      // xrTableCell51
      // 
      this.xrTableCell51.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell51.CanGrow = false;
      resources.ApplyResources(this.xrTableCell51, "xrTableCell51");
      this.xrTableCell51.Name = "xrTableCell51";
      this.xrTableCell51.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell51.StylePriority.UseBorders = false;
      this.xrTableCell51.StylePriority.UseFont = false;
      this.xrTableCell51.StylePriority.UseForeColor = false;
      this.xrTableCell51.StylePriority.UsePadding = false;
      // 
      // ReportHeader1
      // 
      this.ReportHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable9});
      resources.ApplyResources(this.ReportHeader1, "ReportHeader1");
      this.ReportHeader1.Name = "ReportHeader1";
      // 
      // xrTable9
      // 
      this.xrTable9.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
      resources.ApplyResources(this.xrTable9, "xrTable9");
      this.xrTable9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTable9.Name = "xrTable9";
      this.xrTable9.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow15});
      this.xrTable9.StylePriority.UseBorderColor = false;
      this.xrTable9.StylePriority.UseBorders = false;
      this.xrTable9.StylePriority.UsePadding = false;
      // 
      // xrTableRow15
      // 
      this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell43,
            this.xrTableCell44});
      resources.ApplyResources(this.xrTableRow15, "xrTableRow15");
      this.xrTableRow15.Name = "xrTableRow15";
      // 
      // xrTableCell43
      // 
      resources.ApplyResources(this.xrTableCell43, "xrTableCell43");
      this.xrTableCell43.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell43.CanGrow = false;
      this.xrTableCell43.Name = "xrTableCell43";
      this.xrTableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell43.StylePriority.UseBackColor = false;
      this.xrTableCell43.StylePriority.UseBorderColor = false;
      this.xrTableCell43.StylePriority.UseBorders = false;
      this.xrTableCell43.StylePriority.UseFont = false;
      this.xrTableCell43.StylePriority.UseForeColor = false;
      this.xrTableCell43.StylePriority.UsePadding = false;
      this.xrTableCell43.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell44
      // 
      resources.ApplyResources(this.xrTableCell44, "xrTableCell44");
      this.xrTableCell44.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell44.CanGrow = false;
      this.xrTableCell44.Name = "xrTableCell44";
      this.xrTableCell44.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell44.StylePriority.UseBackColor = false;
      this.xrTableCell44.StylePriority.UseBorderColor = false;
      this.xrTableCell44.StylePriority.UseBorders = false;
      this.xrTableCell44.StylePriority.UseFont = false;
      this.xrTableCell44.StylePriority.UseForeColor = false;
      this.xrTableCell44.StylePriority.UsePadding = false;
      // 
      // ReportHeader2
      // 
      this.ReportHeader2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable7});
      resources.ApplyResources(this.ReportHeader2, "ReportHeader2");
      this.ReportHeader2.Name = "ReportHeader2";
      // 
      // xrTable7
      // 
      this.xrTable7.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
      resources.ApplyResources(this.xrTable7, "xrTable7");
      this.xrTable7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTable7.Name = "xrTable7";
      this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow14});
      this.xrTable7.StylePriority.UseBorderColor = false;
      this.xrTable7.StylePriority.UseBorders = false;
      this.xrTable7.StylePriority.UsePadding = false;
      // 
      // xrTableRow14
      // 
      this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell41,
            this.xrTableCell42});
      resources.ApplyResources(this.xrTableRow14, "xrTableRow14");
      this.xrTableRow14.Name = "xrTableRow14";
      // 
      // xrTableCell41
      // 
      resources.ApplyResources(this.xrTableCell41, "xrTableCell41");
      this.xrTableCell41.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell41.CanGrow = false;
      this.xrTableCell41.Name = "xrTableCell41";
      this.xrTableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell41.StylePriority.UseBackColor = false;
      this.xrTableCell41.StylePriority.UseBorderColor = false;
      this.xrTableCell41.StylePriority.UseBorders = false;
      this.xrTableCell41.StylePriority.UseFont = false;
      this.xrTableCell41.StylePriority.UseForeColor = false;
      this.xrTableCell41.StylePriority.UsePadding = false;
      this.xrTableCell41.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell42
      // 
      resources.ApplyResources(this.xrTableCell42, "xrTableCell42");
      this.xrTableCell42.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell42.CanGrow = false;
      this.xrTableCell42.Name = "xrTableCell42";
      this.xrTableCell42.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell42.StylePriority.UseBackColor = false;
      this.xrTableCell42.StylePriority.UseBorderColor = false;
      this.xrTableCell42.StylePriority.UseBorders = false;
      this.xrTableCell42.StylePriority.UseFont = false;
      this.xrTableCell42.StylePriority.UseForeColor = false;
      this.xrTableCell42.StylePriority.UsePadding = false;
      // 
      // PublicDomain_Default
      // 
      this.PublicDomain_Default.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DefaultDetail,
            this.DefaultHeader});
      this.PublicDomain_Default.DataMember = "SubscriberInputDetails";
      resources.ApplyResources(this.PublicDomain_Default, "PublicDomain_Default");
      this.PublicDomain_Default.Level = 1;
      this.PublicDomain_Default.Name = "PublicDomain_Default";
      // 
      // DefaultDetail
      // 
      this.DefaultDetail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable20});
      resources.ApplyResources(this.DefaultDetail, "DefaultDetail");
      this.DefaultDetail.Name = "DefaultDetail";
      // 
      // xrTable20
      // 
      resources.ApplyResources(this.xrTable20, "xrTable20");
      this.xrTable20.Name = "xrTable20";
      this.xrTable20.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable20.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow21});
      this.xrTable20.StylePriority.UseBackColor = false;
      this.xrTable20.StylePriority.UseFont = false;
      this.xrTable20.StylePriority.UsePadding = false;
      // 
      // xrTableRow21
      // 
      resources.ApplyResources(this.xrTableRow21, "xrTableRow21");
      this.xrTableRow21.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell76,
            this.xrTableCell77,
            this.xrTableCell78,
            this.xrTableCell79,
            this.xrTableCell80,
            this.xrTableCell81});
      this.xrTableRow21.Name = "xrTableRow21";
      this.xrTableRow21.StylePriority.UseBackColor = false;
      this.xrTableRow21.StylePriority.UseBorderColor = false;
      this.xrTableRow21.StylePriority.UseBorders = false;
      this.xrTableRow21.StylePriority.UseFont = false;
      this.xrTableRow21.StylePriority.UseForeColor = false;
      this.xrTableRow21.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell76
      // 
      resources.ApplyResources(this.xrTableCell76, "xrTableCell76");
      this.xrTableCell76.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "XDSPaymentNotification.Company")});
      this.xrTableCell76.Name = "xrTableCell76";
      this.xrTableCell76.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell76.StylePriority.UseBackColor = false;
      this.xrTableCell76.StylePriority.UseBorderColor = false;
      this.xrTableCell76.StylePriority.UseFont = false;
      this.xrTableCell76.StylePriority.UseForeColor = false;
      this.xrTableCell76.StylePriority.UsePadding = false;
      this.xrTableCell76.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell77
      // 
      resources.ApplyResources(this.xrTableCell77, "xrTableCell77");
      this.xrTableCell77.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "XDSPaymentNotification.AccountNo")});
      this.xrTableCell77.Name = "xrTableCell77";
      this.xrTableCell77.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell77.StylePriority.UseBackColor = false;
      this.xrTableCell77.StylePriority.UseBorderColor = false;
      this.xrTableCell77.StylePriority.UseFont = false;
      this.xrTableCell77.StylePriority.UseForeColor = false;
      this.xrTableCell77.StylePriority.UsePadding = false;
      this.xrTableCell77.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell78
      // 
      resources.ApplyResources(this.xrTableCell78, "xrTableCell78");
      this.xrTableCell78.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "XDSPaymentNotification.DateLoaded")});
      this.xrTableCell78.Name = "xrTableCell78";
      this.xrTableCell78.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell78.StylePriority.UseBackColor = false;
      this.xrTableCell78.StylePriority.UseBorderColor = false;
      this.xrTableCell78.StylePriority.UseFont = false;
      this.xrTableCell78.StylePriority.UseForeColor = false;
      this.xrTableCell78.StylePriority.UsePadding = false;
      this.xrTableCell78.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell79
      // 
      resources.ApplyResources(this.xrTableCell79, "xrTableCell79");
      this.xrTableCell79.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "XDSPaymentNotification.Amount", "{0:c2}")});
      this.xrTableCell79.Name = "xrTableCell79";
      this.xrTableCell79.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell79.StylePriority.UseBackColor = false;
      this.xrTableCell79.StylePriority.UseBorderColor = false;
      this.xrTableCell79.StylePriority.UseFont = false;
      this.xrTableCell79.StylePriority.UseForeColor = false;
      this.xrTableCell79.StylePriority.UsePadding = false;
      this.xrTableCell79.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell80
      // 
      resources.ApplyResources(this.xrTableCell80, "xrTableCell80");
      this.xrTableCell80.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "XDSPaymentNotification.Statuscode")});
      this.xrTableCell80.Name = "xrTableCell80";
      this.xrTableCell80.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell80.StylePriority.UseBackColor = false;
      this.xrTableCell80.StylePriority.UseBorderColor = false;
      this.xrTableCell80.StylePriority.UseFont = false;
      this.xrTableCell80.StylePriority.UseForeColor = false;
      this.xrTableCell80.StylePriority.UsePadding = false;
      this.xrTableCell80.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell81
      // 
      resources.ApplyResources(this.xrTableCell81, "xrTableCell81");
      this.xrTableCell81.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "XDSPaymentNotification.Comments")});
      this.xrTableCell81.Name = "xrTableCell81";
      this.xrTableCell81.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell81.StylePriority.UseBackColor = false;
      this.xrTableCell81.StylePriority.UseBorderColor = false;
      this.xrTableCell81.StylePriority.UseFont = false;
      this.xrTableCell81.StylePriority.UseForeColor = false;
      this.xrTableCell81.StylePriority.UsePadding = false;
      this.xrTableCell81.StylePriority.UseTextAlignment = false;
      // 
      // DefaultHeader
      // 
      this.DefaultHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable15});
      resources.ApplyResources(this.DefaultHeader, "DefaultHeader");
      this.DefaultHeader.Name = "DefaultHeader";
      // 
      // xrTable15
      // 
      this.xrTable15.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
      resources.ApplyResources(this.xrTable15, "xrTable15");
      this.xrTable15.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTable15.Name = "xrTable15";
      this.xrTable15.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable15.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow19,
            this.xrTableRow20});
      this.xrTable15.StylePriority.UseBorderColor = false;
      this.xrTable15.StylePriority.UseBorders = false;
      this.xrTable15.StylePriority.UsePadding = false;
      // 
      // xrTableRow19
      // 
      this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell60});
      resources.ApplyResources(this.xrTableRow19, "xrTableRow19");
      this.xrTableRow19.Name = "xrTableRow19";
      // 
      // xrTableCell60
      // 
      resources.ApplyResources(this.xrTableCell60, "xrTableCell60");
      this.xrTableCell60.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell60.CanGrow = false;
      this.xrTableCell60.Name = "xrTableCell60";
      this.xrTableCell60.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell60.StylePriority.UseBackColor = false;
      this.xrTableCell60.StylePriority.UseBorderColor = false;
      this.xrTableCell60.StylePriority.UseBorders = false;
      this.xrTableCell60.StylePriority.UseFont = false;
      this.xrTableCell60.StylePriority.UseForeColor = false;
      this.xrTableCell60.StylePriority.UsePadding = false;
      this.xrTableCell60.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow20
      // 
      resources.ApplyResources(this.xrTableRow20, "xrTableRow20");
      this.xrTableRow20.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell62,
            this.xrTableCell66,
            this.xrTableCell69,
            this.xrTableCell72,
            this.xrTableCell73,
            this.xrTableCell74});
      this.xrTableRow20.Name = "xrTableRow20";
      this.xrTableRow20.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTableRow20.StylePriority.UseBackColor = false;
      this.xrTableRow20.StylePriority.UseBorderColor = false;
      this.xrTableRow20.StylePriority.UseBorders = false;
      this.xrTableRow20.StylePriority.UseFont = false;
      this.xrTableRow20.StylePriority.UseForeColor = false;
      this.xrTableRow20.StylePriority.UsePadding = false;
      this.xrTableRow20.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell62
      // 
      this.xrTableCell62.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell62.CanGrow = false;
      resources.ApplyResources(this.xrTableCell62, "xrTableCell62");
      this.xrTableCell62.Name = "xrTableCell62";
      this.xrTableCell62.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell62.StylePriority.UseBorders = false;
      this.xrTableCell62.StylePriority.UseFont = false;
      this.xrTableCell62.StylePriority.UseForeColor = false;
      this.xrTableCell62.StylePriority.UsePadding = false;
      // 
      // xrTableCell66
      // 
      this.xrTableCell66.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell66.CanGrow = false;
      resources.ApplyResources(this.xrTableCell66, "xrTableCell66");
      this.xrTableCell66.Name = "xrTableCell66";
      this.xrTableCell66.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell66.StylePriority.UseBorders = false;
      this.xrTableCell66.StylePriority.UseFont = false;
      this.xrTableCell66.StylePriority.UseForeColor = false;
      this.xrTableCell66.StylePriority.UsePadding = false;
      // 
      // xrTableCell69
      // 
      this.xrTableCell69.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell69.CanGrow = false;
      resources.ApplyResources(this.xrTableCell69, "xrTableCell69");
      this.xrTableCell69.Name = "xrTableCell69";
      this.xrTableCell69.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell69.StylePriority.UseBorders = false;
      this.xrTableCell69.StylePriority.UseFont = false;
      this.xrTableCell69.StylePriority.UseForeColor = false;
      this.xrTableCell69.StylePriority.UsePadding = false;
      // 
      // xrTableCell72
      // 
      this.xrTableCell72.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell72.CanGrow = false;
      resources.ApplyResources(this.xrTableCell72, "xrTableCell72");
      this.xrTableCell72.Name = "xrTableCell72";
      this.xrTableCell72.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell72.StylePriority.UseBorders = false;
      this.xrTableCell72.StylePriority.UseFont = false;
      this.xrTableCell72.StylePriority.UseForeColor = false;
      this.xrTableCell72.StylePriority.UsePadding = false;
      // 
      // xrTableCell73
      // 
      this.xrTableCell73.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell73.CanGrow = false;
      resources.ApplyResources(this.xrTableCell73, "xrTableCell73");
      this.xrTableCell73.Name = "xrTableCell73";
      this.xrTableCell73.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell73.StylePriority.UseBorders = false;
      this.xrTableCell73.StylePriority.UseFont = false;
      this.xrTableCell73.StylePriority.UseForeColor = false;
      this.xrTableCell73.StylePriority.UsePadding = false;
      // 
      // xrTableCell74
      // 
      this.xrTableCell74.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell74.CanGrow = false;
      resources.ApplyResources(this.xrTableCell74, "xrTableCell74");
      this.xrTableCell74.Name = "xrTableCell74";
      this.xrTableCell74.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell74.StylePriority.UseBorders = false;
      this.xrTableCell74.StylePriority.UseFont = false;
      this.xrTableCell74.StylePriority.UseForeColor = false;
      this.xrTableCell74.StylePriority.UsePadding = false;
      // 
      // PublicDomain_Judgements
      // 
      this.PublicDomain_Judgements.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.JudgementDetail,
            this.JudgementHeader});
      this.PublicDomain_Judgements.DataMember = "ConsumerJudgement";
      resources.ApplyResources(this.PublicDomain_Judgements, "PublicDomain_Judgements");
      this.PublicDomain_Judgements.Level = 2;
      this.PublicDomain_Judgements.Name = "PublicDomain_Judgements";
      // 
      // JudgementDetail
      // 
      this.JudgementDetail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable39});
      resources.ApplyResources(this.JudgementDetail, "JudgementDetail");
      this.JudgementDetail.Name = "JudgementDetail";
      // 
      // xrTable39
      // 
      resources.ApplyResources(this.xrTable39, "xrTable39");
      this.xrTable39.Name = "xrTable39";
      this.xrTable39.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable39.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow24});
      this.xrTable39.StylePriority.UseBackColor = false;
      this.xrTable39.StylePriority.UseFont = false;
      this.xrTable39.StylePriority.UsePadding = false;
      // 
      // xrTableRow24
      // 
      resources.ApplyResources(this.xrTableRow24, "xrTableRow24");
      this.xrTableRow24.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell93,
            this.xrTableCell96,
            this.xrTableCell172,
            this.xrTableCell171,
            this.xrTableCell168,
            this.xrTableCell167,
            this.xrTableCell99,
            this.xrTableCell100,
            this.xrTableCell101,
            this.xrTableCell102});
      this.xrTableRow24.Name = "xrTableRow24";
      this.xrTableRow24.StylePriority.UseBackColor = false;
      this.xrTableRow24.StylePriority.UseBorderColor = false;
      this.xrTableRow24.StylePriority.UseBorders = false;
      this.xrTableRow24.StylePriority.UseFont = false;
      this.xrTableRow24.StylePriority.UseForeColor = false;
      this.xrTableRow24.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell93
      // 
      resources.ApplyResources(this.xrTableCell93, "xrTableCell93");
      this.xrTableCell93.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.CaseNumber")});
      this.xrTableCell93.Name = "xrTableCell93";
      this.xrTableCell93.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell93.StylePriority.UseBackColor = false;
      this.xrTableCell93.StylePriority.UseBorderColor = false;
      this.xrTableCell93.StylePriority.UseFont = false;
      this.xrTableCell93.StylePriority.UseForeColor = false;
      this.xrTableCell93.StylePriority.UsePadding = false;
      this.xrTableCell93.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell96
      // 
      resources.ApplyResources(this.xrTableCell96, "xrTableCell96");
      this.xrTableCell96.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.LastUpdatedDate")});
      this.xrTableCell96.Name = "xrTableCell96";
      this.xrTableCell96.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell96.StylePriority.UseBackColor = false;
      this.xrTableCell96.StylePriority.UseBorderColor = false;
      this.xrTableCell96.StylePriority.UseFont = false;
      this.xrTableCell96.StylePriority.UseForeColor = false;
      this.xrTableCell96.StylePriority.UsePadding = false;
      this.xrTableCell96.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell172
      // 
      resources.ApplyResources(this.xrTableCell172, "xrTableCell172");
      this.xrTableCell172.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.CaseFilingDate")});
      this.xrTableCell172.Name = "xrTableCell172";
      this.xrTableCell172.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell172.StylePriority.UseBackColor = false;
      this.xrTableCell172.StylePriority.UseBorderColor = false;
      this.xrTableCell172.StylePriority.UseFont = false;
      this.xrTableCell172.StylePriority.UseForeColor = false;
      this.xrTableCell172.StylePriority.UsePadding = false;
      this.xrTableCell172.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell171
      // 
      resources.ApplyResources(this.xrTableCell171, "xrTableCell171");
      this.xrTableCell171.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.CaseType")});
      this.xrTableCell171.Name = "xrTableCell171";
      this.xrTableCell171.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell171.StylePriority.UseBackColor = false;
      this.xrTableCell171.StylePriority.UseBorderColor = false;
      this.xrTableCell171.StylePriority.UseFont = false;
      this.xrTableCell171.StylePriority.UseForeColor = false;
      this.xrTableCell171.StylePriority.UsePadding = false;
      this.xrTableCell171.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell168
      // 
      resources.ApplyResources(this.xrTableCell168, "xrTableCell168");
      this.xrTableCell168.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.DisputeAmt", "{0:c2}")});
      this.xrTableCell168.Name = "xrTableCell168";
      this.xrTableCell168.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell168.StylePriority.UseBackColor = false;
      this.xrTableCell168.StylePriority.UseBorderColor = false;
      this.xrTableCell168.StylePriority.UseFont = false;
      this.xrTableCell168.StylePriority.UseForeColor = false;
      this.xrTableCell168.StylePriority.UsePadding = false;
      this.xrTableCell168.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell167
      // 
      resources.ApplyResources(this.xrTableCell167, "xrTableCell167");
      this.xrTableCell167.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.PlaintiffName")});
      this.xrTableCell167.Name = "xrTableCell167";
      this.xrTableCell167.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell167.StylePriority.UseBackColor = false;
      this.xrTableCell167.StylePriority.UseBorderColor = false;
      this.xrTableCell167.StylePriority.UseFont = false;
      this.xrTableCell167.StylePriority.UseForeColor = false;
      this.xrTableCell167.StylePriority.UsePadding = false;
      this.xrTableCell167.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell99
      // 
      resources.ApplyResources(this.xrTableCell99, "xrTableCell99");
      this.xrTableCell99.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.CourtName")});
      this.xrTableCell99.Name = "xrTableCell99";
      this.xrTableCell99.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell99.StylePriority.UseBackColor = false;
      this.xrTableCell99.StylePriority.UseBorderColor = false;
      this.xrTableCell99.StylePriority.UseFont = false;
      this.xrTableCell99.StylePriority.UseForeColor = false;
      this.xrTableCell99.StylePriority.UsePadding = false;
      this.xrTableCell99.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell100
      // 
      resources.ApplyResources(this.xrTableCell100, "xrTableCell100");
      this.xrTableCell100.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.AttorneyName")});
      this.xrTableCell100.Name = "xrTableCell100";
      this.xrTableCell100.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell100.StylePriority.UseBackColor = false;
      this.xrTableCell100.StylePriority.UseBorderColor = false;
      this.xrTableCell100.StylePriority.UseFont = false;
      this.xrTableCell100.StylePriority.UseForeColor = false;
      this.xrTableCell100.StylePriority.UsePadding = false;
      this.xrTableCell100.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell101
      // 
      resources.ApplyResources(this.xrTableCell101, "xrTableCell101");
      this.xrTableCell101.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.TelephoneNo")});
      this.xrTableCell101.Name = "xrTableCell101";
      this.xrTableCell101.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell101.StylePriority.UseBackColor = false;
      this.xrTableCell101.StylePriority.UseBorderColor = false;
      this.xrTableCell101.StylePriority.UseFont = false;
      this.xrTableCell101.StylePriority.UseForeColor = false;
      this.xrTableCell101.StylePriority.UsePadding = false;
      this.xrTableCell101.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell102
      // 
      resources.ApplyResources(this.xrTableCell102, "xrTableCell102");
      this.xrTableCell102.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerJudgement.Comments")});
      this.xrTableCell102.Name = "xrTableCell102";
      this.xrTableCell102.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell102.StylePriority.UseBackColor = false;
      this.xrTableCell102.StylePriority.UseBorderColor = false;
      this.xrTableCell102.StylePriority.UseFont = false;
      this.xrTableCell102.StylePriority.UseForeColor = false;
      this.xrTableCell102.StylePriority.UsePadding = false;
      this.xrTableCell102.StylePriority.UseTextAlignment = false;
      // 
      // JudgementHeader
      // 
      this.JudgementHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable38});
      resources.ApplyResources(this.JudgementHeader, "JudgementHeader");
      this.JudgementHeader.Name = "JudgementHeader";
      // 
      // xrTable38
      // 
      this.xrTable38.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
      resources.ApplyResources(this.xrTable38, "xrTable38");
      this.xrTable38.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTable38.Name = "xrTable38";
      this.xrTable38.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable38.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow22,
            this.xrTableRow23});
      this.xrTable38.StylePriority.UseBorderColor = false;
      this.xrTable38.StylePriority.UseBorders = false;
      this.xrTable38.StylePriority.UsePadding = false;
      // 
      // xrTableRow22
      // 
      this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell82});
      resources.ApplyResources(this.xrTableRow22, "xrTableRow22");
      this.xrTableRow22.Name = "xrTableRow22";
      // 
      // xrTableCell82
      // 
      resources.ApplyResources(this.xrTableCell82, "xrTableCell82");
      this.xrTableCell82.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell82.CanGrow = false;
      this.xrTableCell82.Name = "xrTableCell82";
      this.xrTableCell82.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell82.StylePriority.UseBackColor = false;
      this.xrTableCell82.StylePriority.UseBorderColor = false;
      this.xrTableCell82.StylePriority.UseBorders = false;
      this.xrTableCell82.StylePriority.UseFont = false;
      this.xrTableCell82.StylePriority.UseForeColor = false;
      this.xrTableCell82.StylePriority.UsePadding = false;
      this.xrTableCell82.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow23
      // 
      resources.ApplyResources(this.xrTableRow23, "xrTableRow23");
      this.xrTableRow23.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell83,
            this.xrTableCell110,
            this.xrTableCell88,
            this.xrTableCell111,
            this.xrTableCell112,
            this.xrTableCell89,
            this.xrTableCell90,
            this.xrTableCell166,
            this.xrTableCell91,
            this.xrTableCell92});
      this.xrTableRow23.Name = "xrTableRow23";
      this.xrTableRow23.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTableRow23.StylePriority.UseBackColor = false;
      this.xrTableRow23.StylePriority.UseBorderColor = false;
      this.xrTableRow23.StylePriority.UseBorders = false;
      this.xrTableRow23.StylePriority.UseFont = false;
      this.xrTableRow23.StylePriority.UseForeColor = false;
      this.xrTableRow23.StylePriority.UsePadding = false;
      this.xrTableRow23.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell83
      // 
      this.xrTableCell83.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell83.CanGrow = false;
      resources.ApplyResources(this.xrTableCell83, "xrTableCell83");
      this.xrTableCell83.Name = "xrTableCell83";
      this.xrTableCell83.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell83.StylePriority.UseBorders = false;
      this.xrTableCell83.StylePriority.UseFont = false;
      this.xrTableCell83.StylePriority.UseForeColor = false;
      this.xrTableCell83.StylePriority.UsePadding = false;
      // 
      // xrTableCell110
      // 
      this.xrTableCell110.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell110.CanGrow = false;
      resources.ApplyResources(this.xrTableCell110, "xrTableCell110");
      this.xrTableCell110.Name = "xrTableCell110";
      this.xrTableCell110.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell110.StylePriority.UseBorders = false;
      this.xrTableCell110.StylePriority.UseFont = false;
      this.xrTableCell110.StylePriority.UseForeColor = false;
      this.xrTableCell110.StylePriority.UsePadding = false;
      // 
      // xrTableCell88
      // 
      this.xrTableCell88.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell88.CanGrow = false;
      resources.ApplyResources(this.xrTableCell88, "xrTableCell88");
      this.xrTableCell88.Name = "xrTableCell88";
      this.xrTableCell88.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell88.StylePriority.UseBorders = false;
      this.xrTableCell88.StylePriority.UseFont = false;
      this.xrTableCell88.StylePriority.UseForeColor = false;
      this.xrTableCell88.StylePriority.UsePadding = false;
      // 
      // xrTableCell111
      // 
      this.xrTableCell111.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell111.CanGrow = false;
      resources.ApplyResources(this.xrTableCell111, "xrTableCell111");
      this.xrTableCell111.Name = "xrTableCell111";
      this.xrTableCell111.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell111.StylePriority.UseBorders = false;
      this.xrTableCell111.StylePriority.UseFont = false;
      this.xrTableCell111.StylePriority.UseForeColor = false;
      this.xrTableCell111.StylePriority.UsePadding = false;
      // 
      // xrTableCell112
      // 
      this.xrTableCell112.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell112.CanGrow = false;
      resources.ApplyResources(this.xrTableCell112, "xrTableCell112");
      this.xrTableCell112.Name = "xrTableCell112";
      this.xrTableCell112.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell112.StylePriority.UseBorders = false;
      this.xrTableCell112.StylePriority.UseFont = false;
      this.xrTableCell112.StylePriority.UseForeColor = false;
      this.xrTableCell112.StylePriority.UsePadding = false;
      // 
      // xrTableCell89
      // 
      this.xrTableCell89.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell89.CanGrow = false;
      resources.ApplyResources(this.xrTableCell89, "xrTableCell89");
      this.xrTableCell89.Name = "xrTableCell89";
      this.xrTableCell89.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell89.StylePriority.UseBorders = false;
      this.xrTableCell89.StylePriority.UseFont = false;
      this.xrTableCell89.StylePriority.UseForeColor = false;
      this.xrTableCell89.StylePriority.UsePadding = false;
      // 
      // xrTableCell90
      // 
      this.xrTableCell90.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell90.CanGrow = false;
      resources.ApplyResources(this.xrTableCell90, "xrTableCell90");
      this.xrTableCell90.Name = "xrTableCell90";
      this.xrTableCell90.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell90.StylePriority.UseBorders = false;
      this.xrTableCell90.StylePriority.UseFont = false;
      this.xrTableCell90.StylePriority.UseForeColor = false;
      this.xrTableCell90.StylePriority.UsePadding = false;
      // 
      // xrTableCell166
      // 
      this.xrTableCell166.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell166.CanGrow = false;
      resources.ApplyResources(this.xrTableCell166, "xrTableCell166");
      this.xrTableCell166.Name = "xrTableCell166";
      this.xrTableCell166.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell166.StylePriority.UseBorders = false;
      this.xrTableCell166.StylePriority.UseFont = false;
      this.xrTableCell166.StylePriority.UseForeColor = false;
      this.xrTableCell166.StylePriority.UsePadding = false;
      // 
      // xrTableCell91
      // 
      this.xrTableCell91.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell91.CanGrow = false;
      resources.ApplyResources(this.xrTableCell91, "xrTableCell91");
      this.xrTableCell91.Name = "xrTableCell91";
      this.xrTableCell91.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell91.StylePriority.UseBorders = false;
      this.xrTableCell91.StylePriority.UseFont = false;
      this.xrTableCell91.StylePriority.UseForeColor = false;
      this.xrTableCell91.StylePriority.UsePadding = false;
      // 
      // xrTableCell92
      // 
      this.xrTableCell92.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell92.CanGrow = false;
      resources.ApplyResources(this.xrTableCell92, "xrTableCell92");
      this.xrTableCell92.Name = "xrTableCell92";
      this.xrTableCell92.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell92.StylePriority.UseBorders = false;
      this.xrTableCell92.StylePriority.UseFont = false;
      this.xrTableCell92.StylePriority.UseForeColor = false;
      this.xrTableCell92.StylePriority.UsePadding = false;
      // 
      // CourtNotice_Admin
      // 
      this.CourtNotice_Admin.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.AdminDetail,
            this.ReportHeader3,
            this.Sequestration,
            this.Rehabilitation,
            this.DebtReview,
            this.XDSPayment,
            this.DetailReport9});
      resources.ApplyResources(this.CourtNotice_Admin, "CourtNotice_Admin");
      this.CourtNotice_Admin.Level = 3;
      this.CourtNotice_Admin.Name = "CourtNotice_Admin";
      // 
      // AdminDetail
      // 
      resources.ApplyResources(this.AdminDetail, "AdminDetail");
      this.AdminDetail.Name = "AdminDetail";
      // 
      // ReportHeader3
      // 
      this.ReportHeader3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable40});
      resources.ApplyResources(this.ReportHeader3, "ReportHeader3");
      this.ReportHeader3.Name = "ReportHeader3";
      // 
      // xrTable40
      // 
      this.xrTable40.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
      resources.ApplyResources(this.xrTable40, "xrTable40");
      this.xrTable40.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTable40.Name = "xrTable40";
      this.xrTable40.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable40.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow25});
      this.xrTable40.StylePriority.UseBorderColor = false;
      this.xrTable40.StylePriority.UseBorders = false;
      this.xrTable40.StylePriority.UsePadding = false;
      // 
      // xrTableRow25
      // 
      this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell173,
            this.xrTableCell179});
      resources.ApplyResources(this.xrTableRow25, "xrTableRow25");
      this.xrTableRow25.Name = "xrTableRow25";
      // 
      // xrTableCell173
      // 
      resources.ApplyResources(this.xrTableCell173, "xrTableCell173");
      this.xrTableCell173.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
      this.xrTableCell173.CanGrow = false;
      this.xrTableCell173.Name = "xrTableCell173";
      this.xrTableCell173.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell173.StylePriority.UseBackColor = false;
      this.xrTableCell173.StylePriority.UseBorderColor = false;
      this.xrTableCell173.StylePriority.UseBorders = false;
      this.xrTableCell173.StylePriority.UseFont = false;
      this.xrTableCell173.StylePriority.UseForeColor = false;
      this.xrTableCell173.StylePriority.UsePadding = false;
      this.xrTableCell173.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell179
      // 
      resources.ApplyResources(this.xrTableCell179, "xrTableCell179");
      this.xrTableCell179.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell179.CanGrow = false;
      this.xrTableCell179.Name = "xrTableCell179";
      this.xrTableCell179.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell179.StylePriority.UseBackColor = false;
      this.xrTableCell179.StylePriority.UseBorderColor = false;
      this.xrTableCell179.StylePriority.UseBorders = false;
      this.xrTableCell179.StylePriority.UseFont = false;
      this.xrTableCell179.StylePriority.UseForeColor = false;
      this.xrTableCell179.StylePriority.UsePadding = false;
      // 
      // Sequestration
      // 
      this.Sequestration.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.SequestrationDetail,
            this.SequestrationHeader});
      this.Sequestration.DataMember = "ConsumerSequestration";
      resources.ApplyResources(this.Sequestration, "Sequestration");
      this.Sequestration.Level = 1;
      this.Sequestration.Name = "Sequestration";
      // 
      // SequestrationDetail
      // 
      this.SequestrationDetail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable44});
      resources.ApplyResources(this.SequestrationDetail, "SequestrationDetail");
      this.SequestrationDetail.Name = "SequestrationDetail";
      // 
      // xrTable44
      // 
      resources.ApplyResources(this.xrTable44, "xrTable44");
      this.xrTable44.Name = "xrTable44";
      this.xrTable44.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable44.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow46});
      this.xrTable44.StylePriority.UseBackColor = false;
      this.xrTable44.StylePriority.UseFont = false;
      this.xrTable44.StylePriority.UsePadding = false;
      // 
      // xrTableRow46
      // 
      resources.ApplyResources(this.xrTableRow46, "xrTableRow46");
      this.xrTableRow46.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell444,
            this.xrTableCell445,
            this.xrTableCell446,
            this.xrTableCell447,
            this.xrTableCell448,
            this.xrTableCell449,
            this.xrTableCell450,
            this.xrTableCell451,
            this.xrTableCell452,
            this.xrTableCell453});
      this.xrTableRow46.Name = "xrTableRow46";
      this.xrTableRow46.StylePriority.UseBackColor = false;
      this.xrTableRow46.StylePriority.UseBorderColor = false;
      this.xrTableRow46.StylePriority.UseBorders = false;
      this.xrTableRow46.StylePriority.UseFont = false;
      this.xrTableRow46.StylePriority.UseForeColor = false;
      this.xrTableRow46.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell444
      // 
      resources.ApplyResources(this.xrTableCell444, "xrTableCell444");
      this.xrTableCell444.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.CaseNumber")});
      this.xrTableCell444.Name = "xrTableCell444";
      this.xrTableCell444.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell444.StylePriority.UseBackColor = false;
      this.xrTableCell444.StylePriority.UseBorderColor = false;
      this.xrTableCell444.StylePriority.UseFont = false;
      this.xrTableCell444.StylePriority.UseForeColor = false;
      this.xrTableCell444.StylePriority.UsePadding = false;
      this.xrTableCell444.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell445
      // 
      resources.ApplyResources(this.xrTableCell445, "xrTableCell445");
      this.xrTableCell445.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.LastUpdatedDate")});
      this.xrTableCell445.Name = "xrTableCell445";
      this.xrTableCell445.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell445.StylePriority.UseBackColor = false;
      this.xrTableCell445.StylePriority.UseBorderColor = false;
      this.xrTableCell445.StylePriority.UseFont = false;
      this.xrTableCell445.StylePriority.UseForeColor = false;
      this.xrTableCell445.StylePriority.UsePadding = false;
      this.xrTableCell445.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell446
      // 
      resources.ApplyResources(this.xrTableCell446, "xrTableCell446");
      this.xrTableCell446.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.CaseFilingDate")});
      this.xrTableCell446.Name = "xrTableCell446";
      this.xrTableCell446.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell446.StylePriority.UseBackColor = false;
      this.xrTableCell446.StylePriority.UseBorderColor = false;
      this.xrTableCell446.StylePriority.UseFont = false;
      this.xrTableCell446.StylePriority.UseForeColor = false;
      this.xrTableCell446.StylePriority.UsePadding = false;
      this.xrTableCell446.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell447
      // 
      resources.ApplyResources(this.xrTableCell447, "xrTableCell447");
      this.xrTableCell447.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.CaseType")});
      this.xrTableCell447.Name = "xrTableCell447";
      this.xrTableCell447.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell447.StylePriority.UseBackColor = false;
      this.xrTableCell447.StylePriority.UseBorderColor = false;
      this.xrTableCell447.StylePriority.UseFont = false;
      this.xrTableCell447.StylePriority.UseForeColor = false;
      this.xrTableCell447.StylePriority.UsePadding = false;
      this.xrTableCell447.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell448
      // 
      resources.ApplyResources(this.xrTableCell448, "xrTableCell448");
      this.xrTableCell448.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.DisputeAmt", "{0:c2}")});
      this.xrTableCell448.Name = "xrTableCell448";
      this.xrTableCell448.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell448.StylePriority.UseBackColor = false;
      this.xrTableCell448.StylePriority.UseBorderColor = false;
      this.xrTableCell448.StylePriority.UseFont = false;
      this.xrTableCell448.StylePriority.UseForeColor = false;
      this.xrTableCell448.StylePriority.UsePadding = false;
      this.xrTableCell448.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell449
      // 
      resources.ApplyResources(this.xrTableCell449, "xrTableCell449");
      this.xrTableCell449.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.PlaintiffName")});
      this.xrTableCell449.Name = "xrTableCell449";
      this.xrTableCell449.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell449.StylePriority.UseBackColor = false;
      this.xrTableCell449.StylePriority.UseBorderColor = false;
      this.xrTableCell449.StylePriority.UseFont = false;
      this.xrTableCell449.StylePriority.UseForeColor = false;
      this.xrTableCell449.StylePriority.UsePadding = false;
      this.xrTableCell449.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell450
      // 
      resources.ApplyResources(this.xrTableCell450, "xrTableCell450");
      this.xrTableCell450.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.CourtName")});
      this.xrTableCell450.Name = "xrTableCell450";
      this.xrTableCell450.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell450.StylePriority.UseBackColor = false;
      this.xrTableCell450.StylePriority.UseBorderColor = false;
      this.xrTableCell450.StylePriority.UseFont = false;
      this.xrTableCell450.StylePriority.UseForeColor = false;
      this.xrTableCell450.StylePriority.UsePadding = false;
      this.xrTableCell450.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell451
      // 
      resources.ApplyResources(this.xrTableCell451, "xrTableCell451");
      this.xrTableCell451.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.AttorneyName")});
      this.xrTableCell451.Name = "xrTableCell451";
      this.xrTableCell451.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell451.StylePriority.UseBackColor = false;
      this.xrTableCell451.StylePriority.UseBorderColor = false;
      this.xrTableCell451.StylePriority.UseFont = false;
      this.xrTableCell451.StylePriority.UseForeColor = false;
      this.xrTableCell451.StylePriority.UsePadding = false;
      this.xrTableCell451.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell452
      // 
      resources.ApplyResources(this.xrTableCell452, "xrTableCell452");
      this.xrTableCell452.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.TelephoneNo")});
      this.xrTableCell452.Name = "xrTableCell452";
      this.xrTableCell452.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell452.StylePriority.UseBackColor = false;
      this.xrTableCell452.StylePriority.UseBorderColor = false;
      this.xrTableCell452.StylePriority.UseFont = false;
      this.xrTableCell452.StylePriority.UseForeColor = false;
      this.xrTableCell452.StylePriority.UsePadding = false;
      this.xrTableCell452.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell453
      // 
      resources.ApplyResources(this.xrTableCell453, "xrTableCell453");
      this.xrTableCell453.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerSequestration.Comments")});
      this.xrTableCell453.Name = "xrTableCell453";
      this.xrTableCell453.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell453.StylePriority.UseBackColor = false;
      this.xrTableCell453.StylePriority.UseBorderColor = false;
      this.xrTableCell453.StylePriority.UseFont = false;
      this.xrTableCell453.StylePriority.UseForeColor = false;
      this.xrTableCell453.StylePriority.UsePadding = false;
      this.xrTableCell453.StylePriority.UseTextAlignment = false;
      // 
      // SequestrationHeader
      // 
      this.SequestrationHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable43});
      resources.ApplyResources(this.SequestrationHeader, "SequestrationHeader");
      this.SequestrationHeader.Name = "SequestrationHeader";
      // 
      // xrTable43
      // 
      this.xrTable43.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
      resources.ApplyResources(this.xrTable43, "xrTable43");
      this.xrTable43.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTable43.Name = "xrTable43";
      this.xrTable43.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable43.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow31,
            this.xrTableRow45});
      this.xrTable43.StylePriority.UseBorderColor = false;
      this.xrTable43.StylePriority.UseBorders = false;
      this.xrTable43.StylePriority.UsePadding = false;
      // 
      // xrTableRow31
      // 
      this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell191});
      resources.ApplyResources(this.xrTableRow31, "xrTableRow31");
      this.xrTableRow31.Name = "xrTableRow31";
      // 
      // xrTableCell191
      // 
      resources.ApplyResources(this.xrTableCell191, "xrTableCell191");
      this.xrTableCell191.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell191.CanGrow = false;
      this.xrTableCell191.Name = "xrTableCell191";
      this.xrTableCell191.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell191.StylePriority.UseBackColor = false;
      this.xrTableCell191.StylePriority.UseBorderColor = false;
      this.xrTableCell191.StylePriority.UseBorders = false;
      this.xrTableCell191.StylePriority.UseFont = false;
      this.xrTableCell191.StylePriority.UseForeColor = false;
      this.xrTableCell191.StylePriority.UsePadding = false;
      this.xrTableCell191.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow45
      // 
      resources.ApplyResources(this.xrTableRow45, "xrTableRow45");
      this.xrTableRow45.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell192,
            this.xrTableCell193,
            this.xrTableCell194,
            this.xrTableCell195,
            this.xrTableCell196,
            this.xrTableCell197,
            this.xrTableCell198,
            this.xrTableCell199,
            this.xrTableCell442,
            this.xrTableCell443});
      this.xrTableRow45.Name = "xrTableRow45";
      this.xrTableRow45.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTableRow45.StylePriority.UseBackColor = false;
      this.xrTableRow45.StylePriority.UseBorderColor = false;
      this.xrTableRow45.StylePriority.UseBorders = false;
      this.xrTableRow45.StylePriority.UseFont = false;
      this.xrTableRow45.StylePriority.UseForeColor = false;
      this.xrTableRow45.StylePriority.UsePadding = false;
      this.xrTableRow45.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell192
      // 
      this.xrTableCell192.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell192.CanGrow = false;
      resources.ApplyResources(this.xrTableCell192, "xrTableCell192");
      this.xrTableCell192.Name = "xrTableCell192";
      this.xrTableCell192.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell192.StylePriority.UseBorders = false;
      this.xrTableCell192.StylePriority.UseFont = false;
      this.xrTableCell192.StylePriority.UseForeColor = false;
      this.xrTableCell192.StylePriority.UsePadding = false;
      // 
      // xrTableCell193
      // 
      this.xrTableCell193.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell193.CanGrow = false;
      resources.ApplyResources(this.xrTableCell193, "xrTableCell193");
      this.xrTableCell193.Name = "xrTableCell193";
      this.xrTableCell193.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell193.StylePriority.UseBorders = false;
      this.xrTableCell193.StylePriority.UseFont = false;
      this.xrTableCell193.StylePriority.UseForeColor = false;
      this.xrTableCell193.StylePriority.UsePadding = false;
      // 
      // xrTableCell194
      // 
      this.xrTableCell194.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell194.CanGrow = false;
      resources.ApplyResources(this.xrTableCell194, "xrTableCell194");
      this.xrTableCell194.Name = "xrTableCell194";
      this.xrTableCell194.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell194.StylePriority.UseBorders = false;
      this.xrTableCell194.StylePriority.UseFont = false;
      this.xrTableCell194.StylePriority.UseForeColor = false;
      this.xrTableCell194.StylePriority.UsePadding = false;
      // 
      // xrTableCell195
      // 
      this.xrTableCell195.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell195.CanGrow = false;
      resources.ApplyResources(this.xrTableCell195, "xrTableCell195");
      this.xrTableCell195.Name = "xrTableCell195";
      this.xrTableCell195.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell195.StylePriority.UseBorders = false;
      this.xrTableCell195.StylePriority.UseFont = false;
      this.xrTableCell195.StylePriority.UseForeColor = false;
      this.xrTableCell195.StylePriority.UsePadding = false;
      // 
      // xrTableCell196
      // 
      this.xrTableCell196.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell196.CanGrow = false;
      resources.ApplyResources(this.xrTableCell196, "xrTableCell196");
      this.xrTableCell196.Name = "xrTableCell196";
      this.xrTableCell196.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell196.StylePriority.UseBorders = false;
      this.xrTableCell196.StylePriority.UseFont = false;
      this.xrTableCell196.StylePriority.UseForeColor = false;
      this.xrTableCell196.StylePriority.UsePadding = false;
      // 
      // xrTableCell197
      // 
      this.xrTableCell197.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell197.CanGrow = false;
      resources.ApplyResources(this.xrTableCell197, "xrTableCell197");
      this.xrTableCell197.Name = "xrTableCell197";
      this.xrTableCell197.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell197.StylePriority.UseBorders = false;
      this.xrTableCell197.StylePriority.UseFont = false;
      this.xrTableCell197.StylePriority.UseForeColor = false;
      this.xrTableCell197.StylePriority.UsePadding = false;
      // 
      // xrTableCell198
      // 
      this.xrTableCell198.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell198.CanGrow = false;
      resources.ApplyResources(this.xrTableCell198, "xrTableCell198");
      this.xrTableCell198.Name = "xrTableCell198";
      this.xrTableCell198.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell198.StylePriority.UseBorders = false;
      this.xrTableCell198.StylePriority.UseFont = false;
      this.xrTableCell198.StylePriority.UseForeColor = false;
      this.xrTableCell198.StylePriority.UsePadding = false;
      // 
      // xrTableCell199
      // 
      this.xrTableCell199.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell199.CanGrow = false;
      resources.ApplyResources(this.xrTableCell199, "xrTableCell199");
      this.xrTableCell199.Name = "xrTableCell199";
      this.xrTableCell199.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell199.StylePriority.UseBorders = false;
      this.xrTableCell199.StylePriority.UseFont = false;
      this.xrTableCell199.StylePriority.UseForeColor = false;
      this.xrTableCell199.StylePriority.UsePadding = false;
      // 
      // xrTableCell442
      // 
      this.xrTableCell442.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell442.CanGrow = false;
      resources.ApplyResources(this.xrTableCell442, "xrTableCell442");
      this.xrTableCell442.Name = "xrTableCell442";
      this.xrTableCell442.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell442.StylePriority.UseBorders = false;
      this.xrTableCell442.StylePriority.UseFont = false;
      this.xrTableCell442.StylePriority.UseForeColor = false;
      this.xrTableCell442.StylePriority.UsePadding = false;
      // 
      // xrTableCell443
      // 
      this.xrTableCell443.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell443.CanGrow = false;
      resources.ApplyResources(this.xrTableCell443, "xrTableCell443");
      this.xrTableCell443.Name = "xrTableCell443";
      this.xrTableCell443.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell443.StylePriority.UseBorders = false;
      this.xrTableCell443.StylePriority.UseFont = false;
      this.xrTableCell443.StylePriority.UseForeColor = false;
      this.xrTableCell443.StylePriority.UsePadding = false;
      // 
      // Rehabilitation
      // 
      this.Rehabilitation.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.RehabilitationDetail,
            this.RehabilitationHeader});
      resources.ApplyResources(this.Rehabilitation, "Rehabilitation");
      this.Rehabilitation.Level = 2;
      this.Rehabilitation.Name = "Rehabilitation";
      // 
      // RehabilitationDetail
      // 
      this.RehabilitationDetail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable46});
      resources.ApplyResources(this.RehabilitationDetail, "RehabilitationDetail");
      this.RehabilitationDetail.Name = "RehabilitationDetail";
      // 
      // xrTable46
      // 
      resources.ApplyResources(this.xrTable46, "xrTable46");
      this.xrTable46.Name = "xrTable46";
      this.xrTable46.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable46.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow56});
      this.xrTable46.StylePriority.UseBackColor = false;
      this.xrTable46.StylePriority.UseFont = false;
      this.xrTable46.StylePriority.UsePadding = false;
      // 
      // xrTableRow56
      // 
      resources.ApplyResources(this.xrTableRow56, "xrTableRow56");
      this.xrTableRow56.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableRow56.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell465,
            this.xrTableCell466,
            this.xrTableCell467,
            this.xrTableCell468,
            this.xrTableCell469,
            this.xrTableCell470,
            this.xrTableCell471,
            this.xrTableCell472,
            this.xrTableCell473,
            this.xrTableCell474});
      this.xrTableRow56.Name = "xrTableRow56";
      this.xrTableRow56.StylePriority.UseBackColor = false;
      this.xrTableRow56.StylePriority.UseBorderColor = false;
      this.xrTableRow56.StylePriority.UseBorders = false;
      this.xrTableRow56.StylePriority.UseFont = false;
      this.xrTableRow56.StylePriority.UseForeColor = false;
      this.xrTableRow56.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell465
      // 
      resources.ApplyResources(this.xrTableCell465, "xrTableCell465");
      this.xrTableCell465.Name = "xrTableCell465";
      this.xrTableCell465.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell465.StylePriority.UseBackColor = false;
      this.xrTableCell465.StylePriority.UseBorderColor = false;
      this.xrTableCell465.StylePriority.UseFont = false;
      this.xrTableCell465.StylePriority.UseForeColor = false;
      this.xrTableCell465.StylePriority.UsePadding = false;
      this.xrTableCell465.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell466
      // 
      resources.ApplyResources(this.xrTableCell466, "xrTableCell466");
      this.xrTableCell466.Name = "xrTableCell466";
      this.xrTableCell466.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell466.StylePriority.UseBackColor = false;
      this.xrTableCell466.StylePriority.UseBorderColor = false;
      this.xrTableCell466.StylePriority.UseFont = false;
      this.xrTableCell466.StylePriority.UseForeColor = false;
      this.xrTableCell466.StylePriority.UsePadding = false;
      this.xrTableCell466.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell467
      // 
      resources.ApplyResources(this.xrTableCell467, "xrTableCell467");
      this.xrTableCell467.Name = "xrTableCell467";
      this.xrTableCell467.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell467.StylePriority.UseBackColor = false;
      this.xrTableCell467.StylePriority.UseBorderColor = false;
      this.xrTableCell467.StylePriority.UseFont = false;
      this.xrTableCell467.StylePriority.UseForeColor = false;
      this.xrTableCell467.StylePriority.UsePadding = false;
      this.xrTableCell467.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell468
      // 
      resources.ApplyResources(this.xrTableCell468, "xrTableCell468");
      this.xrTableCell468.Name = "xrTableCell468";
      this.xrTableCell468.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell468.StylePriority.UseBackColor = false;
      this.xrTableCell468.StylePriority.UseBorderColor = false;
      this.xrTableCell468.StylePriority.UseFont = false;
      this.xrTableCell468.StylePriority.UseForeColor = false;
      this.xrTableCell468.StylePriority.UsePadding = false;
      this.xrTableCell468.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell469
      // 
      resources.ApplyResources(this.xrTableCell469, "xrTableCell469");
      this.xrTableCell469.Name = "xrTableCell469";
      this.xrTableCell469.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell469.StylePriority.UseBackColor = false;
      this.xrTableCell469.StylePriority.UseBorderColor = false;
      this.xrTableCell469.StylePriority.UseFont = false;
      this.xrTableCell469.StylePriority.UseForeColor = false;
      this.xrTableCell469.StylePriority.UsePadding = false;
      this.xrTableCell469.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell470
      // 
      resources.ApplyResources(this.xrTableCell470, "xrTableCell470");
      this.xrTableCell470.Name = "xrTableCell470";
      this.xrTableCell470.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell470.StylePriority.UseBackColor = false;
      this.xrTableCell470.StylePriority.UseBorderColor = false;
      this.xrTableCell470.StylePriority.UseFont = false;
      this.xrTableCell470.StylePriority.UseForeColor = false;
      this.xrTableCell470.StylePriority.UsePadding = false;
      this.xrTableCell470.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell471
      // 
      resources.ApplyResources(this.xrTableCell471, "xrTableCell471");
      this.xrTableCell471.Name = "xrTableCell471";
      this.xrTableCell471.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell471.StylePriority.UseBackColor = false;
      this.xrTableCell471.StylePriority.UseBorderColor = false;
      this.xrTableCell471.StylePriority.UseFont = false;
      this.xrTableCell471.StylePriority.UseForeColor = false;
      this.xrTableCell471.StylePriority.UsePadding = false;
      this.xrTableCell471.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell472
      // 
      resources.ApplyResources(this.xrTableCell472, "xrTableCell472");
      this.xrTableCell472.Name = "xrTableCell472";
      this.xrTableCell472.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell472.StylePriority.UseBackColor = false;
      this.xrTableCell472.StylePriority.UseBorderColor = false;
      this.xrTableCell472.StylePriority.UseFont = false;
      this.xrTableCell472.StylePriority.UseForeColor = false;
      this.xrTableCell472.StylePriority.UsePadding = false;
      this.xrTableCell472.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell473
      // 
      resources.ApplyResources(this.xrTableCell473, "xrTableCell473");
      this.xrTableCell473.Name = "xrTableCell473";
      this.xrTableCell473.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell473.StylePriority.UseBackColor = false;
      this.xrTableCell473.StylePriority.UseBorderColor = false;
      this.xrTableCell473.StylePriority.UseFont = false;
      this.xrTableCell473.StylePriority.UseForeColor = false;
      this.xrTableCell473.StylePriority.UsePadding = false;
      this.xrTableCell473.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell474
      // 
      resources.ApplyResources(this.xrTableCell474, "xrTableCell474");
      this.xrTableCell474.Name = "xrTableCell474";
      this.xrTableCell474.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell474.StylePriority.UseBackColor = false;
      this.xrTableCell474.StylePriority.UseBorderColor = false;
      this.xrTableCell474.StylePriority.UseFont = false;
      this.xrTableCell474.StylePriority.UseForeColor = false;
      this.xrTableCell474.StylePriority.UsePadding = false;
      this.xrTableCell474.StylePriority.UseTextAlignment = false;
      // 
      // RehabilitationHeader
      // 
      this.RehabilitationHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable45});
      resources.ApplyResources(this.RehabilitationHeader, "RehabilitationHeader");
      this.RehabilitationHeader.Name = "RehabilitationHeader";
      // 
      // xrTable45
      // 
      this.xrTable45.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
      resources.ApplyResources(this.xrTable45, "xrTable45");
      this.xrTable45.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTable45.Name = "xrTable45";
      this.xrTable45.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable45.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow54,
            this.xrTableRow55});
      this.xrTable45.StylePriority.UseBorderColor = false;
      this.xrTable45.StylePriority.UseBorders = false;
      this.xrTable45.StylePriority.UsePadding = false;
      // 
      // xrTableRow54
      // 
      this.xrTableRow54.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell454});
      resources.ApplyResources(this.xrTableRow54, "xrTableRow54");
      this.xrTableRow54.Name = "xrTableRow54";
      // 
      // xrTableCell454
      // 
      resources.ApplyResources(this.xrTableCell454, "xrTableCell454");
      this.xrTableCell454.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell454.CanGrow = false;
      this.xrTableCell454.Name = "xrTableCell454";
      this.xrTableCell454.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell454.StylePriority.UseBackColor = false;
      this.xrTableCell454.StylePriority.UseBorderColor = false;
      this.xrTableCell454.StylePriority.UseBorders = false;
      this.xrTableCell454.StylePriority.UseFont = false;
      this.xrTableCell454.StylePriority.UseForeColor = false;
      this.xrTableCell454.StylePriority.UsePadding = false;
      this.xrTableCell454.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow55
      // 
      resources.ApplyResources(this.xrTableRow55, "xrTableRow55");
      this.xrTableRow55.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableRow55.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell455,
            this.xrTableCell456,
            this.xrTableCell457,
            this.xrTableCell458,
            this.xrTableCell459,
            this.xrTableCell460,
            this.xrTableCell461,
            this.xrTableCell462,
            this.xrTableCell463,
            this.xrTableCell464});
      this.xrTableRow55.Name = "xrTableRow55";
      this.xrTableRow55.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTableRow55.StylePriority.UseBackColor = false;
      this.xrTableRow55.StylePriority.UseBorderColor = false;
      this.xrTableRow55.StylePriority.UseBorders = false;
      this.xrTableRow55.StylePriority.UseFont = false;
      this.xrTableRow55.StylePriority.UseForeColor = false;
      this.xrTableRow55.StylePriority.UsePadding = false;
      this.xrTableRow55.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell455
      // 
      this.xrTableCell455.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell455.CanGrow = false;
      resources.ApplyResources(this.xrTableCell455, "xrTableCell455");
      this.xrTableCell455.Name = "xrTableCell455";
      this.xrTableCell455.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell455.StylePriority.UseBorders = false;
      this.xrTableCell455.StylePriority.UseFont = false;
      this.xrTableCell455.StylePriority.UseForeColor = false;
      this.xrTableCell455.StylePriority.UsePadding = false;
      // 
      // xrTableCell456
      // 
      this.xrTableCell456.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell456.CanGrow = false;
      resources.ApplyResources(this.xrTableCell456, "xrTableCell456");
      this.xrTableCell456.Name = "xrTableCell456";
      this.xrTableCell456.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell456.StylePriority.UseBorders = false;
      this.xrTableCell456.StylePriority.UseFont = false;
      this.xrTableCell456.StylePriority.UseForeColor = false;
      this.xrTableCell456.StylePriority.UsePadding = false;
      // 
      // xrTableCell457
      // 
      this.xrTableCell457.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell457.CanGrow = false;
      resources.ApplyResources(this.xrTableCell457, "xrTableCell457");
      this.xrTableCell457.Name = "xrTableCell457";
      this.xrTableCell457.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell457.StylePriority.UseBorders = false;
      this.xrTableCell457.StylePriority.UseFont = false;
      this.xrTableCell457.StylePriority.UseForeColor = false;
      this.xrTableCell457.StylePriority.UsePadding = false;
      // 
      // xrTableCell458
      // 
      this.xrTableCell458.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell458.CanGrow = false;
      resources.ApplyResources(this.xrTableCell458, "xrTableCell458");
      this.xrTableCell458.Name = "xrTableCell458";
      this.xrTableCell458.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell458.StylePriority.UseBorders = false;
      this.xrTableCell458.StylePriority.UseFont = false;
      this.xrTableCell458.StylePriority.UseForeColor = false;
      this.xrTableCell458.StylePriority.UsePadding = false;
      // 
      // xrTableCell459
      // 
      this.xrTableCell459.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell459.CanGrow = false;
      resources.ApplyResources(this.xrTableCell459, "xrTableCell459");
      this.xrTableCell459.Name = "xrTableCell459";
      this.xrTableCell459.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell459.StylePriority.UseBorders = false;
      this.xrTableCell459.StylePriority.UseFont = false;
      this.xrTableCell459.StylePriority.UseForeColor = false;
      this.xrTableCell459.StylePriority.UsePadding = false;
      // 
      // xrTableCell460
      // 
      this.xrTableCell460.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell460.CanGrow = false;
      resources.ApplyResources(this.xrTableCell460, "xrTableCell460");
      this.xrTableCell460.Name = "xrTableCell460";
      this.xrTableCell460.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell460.StylePriority.UseBorders = false;
      this.xrTableCell460.StylePriority.UseFont = false;
      this.xrTableCell460.StylePriority.UseForeColor = false;
      this.xrTableCell460.StylePriority.UsePadding = false;
      // 
      // xrTableCell461
      // 
      this.xrTableCell461.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell461.CanGrow = false;
      resources.ApplyResources(this.xrTableCell461, "xrTableCell461");
      this.xrTableCell461.Name = "xrTableCell461";
      this.xrTableCell461.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell461.StylePriority.UseBorders = false;
      this.xrTableCell461.StylePriority.UseFont = false;
      this.xrTableCell461.StylePriority.UseForeColor = false;
      this.xrTableCell461.StylePriority.UsePadding = false;
      // 
      // xrTableCell462
      // 
      this.xrTableCell462.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell462.CanGrow = false;
      resources.ApplyResources(this.xrTableCell462, "xrTableCell462");
      this.xrTableCell462.Name = "xrTableCell462";
      this.xrTableCell462.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell462.StylePriority.UseBorders = false;
      this.xrTableCell462.StylePriority.UseFont = false;
      this.xrTableCell462.StylePriority.UseForeColor = false;
      this.xrTableCell462.StylePriority.UsePadding = false;
      // 
      // xrTableCell463
      // 
      this.xrTableCell463.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell463.CanGrow = false;
      resources.ApplyResources(this.xrTableCell463, "xrTableCell463");
      this.xrTableCell463.Name = "xrTableCell463";
      this.xrTableCell463.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell463.StylePriority.UseBorders = false;
      this.xrTableCell463.StylePriority.UseFont = false;
      this.xrTableCell463.StylePriority.UseForeColor = false;
      this.xrTableCell463.StylePriority.UsePadding = false;
      // 
      // xrTableCell464
      // 
      this.xrTableCell464.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell464.CanGrow = false;
      resources.ApplyResources(this.xrTableCell464, "xrTableCell464");
      this.xrTableCell464.Name = "xrTableCell464";
      this.xrTableCell464.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell464.StylePriority.UseBorders = false;
      this.xrTableCell464.StylePriority.UseFont = false;
      this.xrTableCell464.StylePriority.UseForeColor = false;
      this.xrTableCell464.StylePriority.UsePadding = false;
      // 
      // DebtReview
      // 
      this.DebtReview.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DebtReviewDetail,
            this.DebtReviewHeader});
      this.DebtReview.DataMember = "ConsumerDebtReviewStatus";
      resources.ApplyResources(this.DebtReview, "DebtReview");
      this.DebtReview.Level = 3;
      this.DebtReview.Name = "DebtReview";
      // 
      // DebtReviewDetail
      // 
      this.DebtReviewDetail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.tblDbtReviewD});
      resources.ApplyResources(this.DebtReviewDetail, "DebtReviewDetail");
      this.DebtReviewDetail.Name = "DebtReviewDetail";
      // 
      // tblDbtReviewD
      // 
      resources.ApplyResources(this.tblDbtReviewD, "tblDbtReviewD");
      this.tblDbtReviewD.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.tblDbtReviewD.Name = "tblDbtReviewD";
      this.tblDbtReviewD.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow59,
            this.xrTableRow60,
            this.xrTableRow61,
            this.xrTableRow63});
      this.tblDbtReviewD.StylePriority.UseBorderColor = false;
      this.tblDbtReviewD.StylePriority.UseBorders = false;
      this.tblDbtReviewD.StylePriority.UseFont = false;
      // 
      // xrTableRow59
      // 
      this.xrTableRow59.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDbtReviewDate,
            this.lblDbtReviewDateD});
      resources.ApplyResources(this.xrTableRow59, "xrTableRow59");
      this.xrTableRow59.Name = "xrTableRow59";
      // 
      // lblDbtReviewDate
      // 
      resources.ApplyResources(this.lblDbtReviewDate, "lblDbtReviewDate");
      this.lblDbtReviewDate.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.lblDbtReviewDate.Name = "lblDbtReviewDate";
      this.lblDbtReviewDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblDbtReviewDate.StylePriority.UseBackColor = false;
      this.lblDbtReviewDate.StylePriority.UseBorderColor = false;
      this.lblDbtReviewDate.StylePriority.UseBorders = false;
      this.lblDbtReviewDate.StylePriority.UseFont = false;
      this.lblDbtReviewDate.StylePriority.UseForeColor = false;
      this.lblDbtReviewDate.StylePriority.UsePadding = false;
      this.lblDbtReviewDate.StylePriority.UseTextAlignment = false;
      // 
      // lblDbtReviewDateD
      // 
      resources.ApplyResources(this.lblDbtReviewDateD, "lblDbtReviewDateD");
      this.lblDbtReviewDateD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.lblDbtReviewDateD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtReviewStatus.DebtCounsellorName")});
      this.lblDbtReviewDateD.Name = "lblDbtReviewDateD";
      this.lblDbtReviewDateD.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblDbtReviewDateD.StylePriority.UseBackColor = false;
      this.lblDbtReviewDateD.StylePriority.UseBorderColor = false;
      this.lblDbtReviewDateD.StylePriority.UseBorders = false;
      this.lblDbtReviewDateD.StylePriority.UseFont = false;
      this.lblDbtReviewDateD.StylePriority.UseForeColor = false;
      this.lblDbtReviewDateD.StylePriority.UsePadding = false;
      this.lblDbtReviewDateD.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow60
      // 
      this.xrTableRow60.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCounsellorName,
            this.lblCounsellorNameD});
      resources.ApplyResources(this.xrTableRow60, "xrTableRow60");
      this.xrTableRow60.Name = "xrTableRow60";
      // 
      // lblCounsellorName
      // 
      resources.ApplyResources(this.lblCounsellorName, "lblCounsellorName");
      this.lblCounsellorName.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.lblCounsellorName.Name = "lblCounsellorName";
      this.lblCounsellorName.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblCounsellorName.StylePriority.UseBackColor = false;
      this.lblCounsellorName.StylePriority.UseBorderColor = false;
      this.lblCounsellorName.StylePriority.UseBorders = false;
      this.lblCounsellorName.StylePriority.UseFont = false;
      this.lblCounsellorName.StylePriority.UseForeColor = false;
      this.lblCounsellorName.StylePriority.UsePadding = false;
      this.lblCounsellorName.StylePriority.UseTextAlignment = false;
      // 
      // lblCounsellorNameD
      // 
      resources.ApplyResources(this.lblCounsellorNameD, "lblCounsellorNameD");
      this.lblCounsellorNameD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.lblCounsellorNameD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtReviewStatus.DebtReviewStatusDate")});
      this.lblCounsellorNameD.Name = "lblCounsellorNameD";
      this.lblCounsellorNameD.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblCounsellorNameD.StylePriority.UseBackColor = false;
      this.lblCounsellorNameD.StylePriority.UseBorderColor = false;
      this.lblCounsellorNameD.StylePriority.UseBorders = false;
      this.lblCounsellorNameD.StylePriority.UseFont = false;
      this.lblCounsellorNameD.StylePriority.UseForeColor = false;
      this.lblCounsellorNameD.StylePriority.UsePadding = false;
      this.lblCounsellorNameD.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow61
      // 
      this.xrTableRow61.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCounsellorTelephoneNo,
            this.lblCounsellorTelephoneNoD});
      resources.ApplyResources(this.xrTableRow61, "xrTableRow61");
      this.xrTableRow61.Name = "xrTableRow61";
      // 
      // lblCounsellorTelephoneNo
      // 
      resources.ApplyResources(this.lblCounsellorTelephoneNo, "lblCounsellorTelephoneNo");
      this.lblCounsellorTelephoneNo.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.lblCounsellorTelephoneNo.Name = "lblCounsellorTelephoneNo";
      this.lblCounsellorTelephoneNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblCounsellorTelephoneNo.StylePriority.UseBackColor = false;
      this.lblCounsellorTelephoneNo.StylePriority.UseBorderColor = false;
      this.lblCounsellorTelephoneNo.StylePriority.UseBorders = false;
      this.lblCounsellorTelephoneNo.StylePriority.UseFont = false;
      this.lblCounsellorTelephoneNo.StylePriority.UseForeColor = false;
      this.lblCounsellorTelephoneNo.StylePriority.UsePadding = false;
      this.lblCounsellorTelephoneNo.StylePriority.UseTextAlignment = false;
      // 
      // lblCounsellorTelephoneNoD
      // 
      resources.ApplyResources(this.lblCounsellorTelephoneNoD, "lblCounsellorTelephoneNoD");
      this.lblCounsellorTelephoneNoD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.lblCounsellorTelephoneNoD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtReviewStatus.DebtCounsellorTelephoneNo")});
      this.lblCounsellorTelephoneNoD.Name = "lblCounsellorTelephoneNoD";
      this.lblCounsellorTelephoneNoD.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.lblCounsellorTelephoneNoD.StylePriority.UseBackColor = false;
      this.lblCounsellorTelephoneNoD.StylePriority.UseBorderColor = false;
      this.lblCounsellorTelephoneNoD.StylePriority.UseBorders = false;
      this.lblCounsellorTelephoneNoD.StylePriority.UseFont = false;
      this.lblCounsellorTelephoneNoD.StylePriority.UseForeColor = false;
      this.lblCounsellorTelephoneNoD.StylePriority.UsePadding = false;
      this.lblCounsellorTelephoneNoD.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow63
      // 
      this.xrTableRow63.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell486,
            this.xrTableCell487});
      resources.ApplyResources(this.xrTableRow63, "xrTableRow63");
      this.xrTableRow63.Name = "xrTableRow63";
      // 
      // xrTableCell486
      // 
      resources.ApplyResources(this.xrTableCell486, "xrTableCell486");
      this.xrTableCell486.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell486.Name = "xrTableCell486";
      this.xrTableCell486.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell486.StylePriority.UseBackColor = false;
      this.xrTableCell486.StylePriority.UseBorderColor = false;
      this.xrTableCell486.StylePriority.UseBorders = false;
      this.xrTableCell486.StylePriority.UseFont = false;
      this.xrTableCell486.StylePriority.UseForeColor = false;
      this.xrTableCell486.StylePriority.UsePadding = false;
      this.xrTableCell486.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell487
      // 
      resources.ApplyResources(this.xrTableCell487, "xrTableCell487");
      this.xrTableCell487.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell487.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerDebtReviewStatus.DebtReviewStatus")});
      this.xrTableCell487.Name = "xrTableCell487";
      this.xrTableCell487.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell487.StylePriority.UseBackColor = false;
      this.xrTableCell487.StylePriority.UseBorderColor = false;
      this.xrTableCell487.StylePriority.UseBorders = false;
      this.xrTableCell487.StylePriority.UseFont = false;
      this.xrTableCell487.StylePriority.UseForeColor = false;
      this.xrTableCell487.StylePriority.UsePadding = false;
      this.xrTableCell487.StylePriority.UseTextAlignment = false;
      // 
      // DebtReviewHeader
      // 
      this.DebtReviewHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable47});
      resources.ApplyResources(this.DebtReviewHeader, "DebtReviewHeader");
      this.DebtReviewHeader.Name = "DebtReviewHeader";
      // 
      // xrTable47
      // 
      this.xrTable47.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
      resources.ApplyResources(this.xrTable47, "xrTable47");
      this.xrTable47.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTable47.Name = "xrTable47";
      this.xrTable47.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable47.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow57,
            this.xrTableRow58});
      this.xrTable47.StylePriority.UseBorderColor = false;
      this.xrTable47.StylePriority.UseBorders = false;
      this.xrTable47.StylePriority.UsePadding = false;
      // 
      // xrTableRow57
      // 
      this.xrTableRow57.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell475});
      resources.ApplyResources(this.xrTableRow57, "xrTableRow57");
      this.xrTableRow57.Name = "xrTableRow57";
      // 
      // xrTableCell475
      // 
      resources.ApplyResources(this.xrTableCell475, "xrTableCell475");
      this.xrTableCell475.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell475.CanGrow = false;
      this.xrTableCell475.Name = "xrTableCell475";
      this.xrTableCell475.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell475.StylePriority.UseBackColor = false;
      this.xrTableCell475.StylePriority.UseBorderColor = false;
      this.xrTableCell475.StylePriority.UseBorders = false;
      this.xrTableCell475.StylePriority.UseFont = false;
      this.xrTableCell475.StylePriority.UseForeColor = false;
      this.xrTableCell475.StylePriority.UsePadding = false;
      this.xrTableCell475.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow58
      // 
      resources.ApplyResources(this.xrTableRow58, "xrTableRow58");
      this.xrTableRow58.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableRow58.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell476,
            this.xrTableCell477,
            this.xrTableCell478,
            this.xrTableCell479,
            this.xrTableCell480,
            this.xrTableCell481,
            this.xrTableCell482,
            this.xrTableCell483,
            this.xrTableCell484,
            this.xrTableCell485});
      this.xrTableRow58.Name = "xrTableRow58";
      this.xrTableRow58.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTableRow58.StylePriority.UseBackColor = false;
      this.xrTableRow58.StylePriority.UseBorderColor = false;
      this.xrTableRow58.StylePriority.UseBorders = false;
      this.xrTableRow58.StylePriority.UseFont = false;
      this.xrTableRow58.StylePriority.UseForeColor = false;
      this.xrTableRow58.StylePriority.UsePadding = false;
      this.xrTableRow58.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell476
      // 
      this.xrTableCell476.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell476.CanGrow = false;
      resources.ApplyResources(this.xrTableCell476, "xrTableCell476");
      this.xrTableCell476.Name = "xrTableCell476";
      this.xrTableCell476.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell476.StylePriority.UseBorders = false;
      this.xrTableCell476.StylePriority.UseFont = false;
      this.xrTableCell476.StylePriority.UseForeColor = false;
      this.xrTableCell476.StylePriority.UsePadding = false;
      // 
      // xrTableCell477
      // 
      this.xrTableCell477.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell477.CanGrow = false;
      resources.ApplyResources(this.xrTableCell477, "xrTableCell477");
      this.xrTableCell477.Name = "xrTableCell477";
      this.xrTableCell477.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell477.StylePriority.UseBorders = false;
      this.xrTableCell477.StylePriority.UseFont = false;
      this.xrTableCell477.StylePriority.UseForeColor = false;
      this.xrTableCell477.StylePriority.UsePadding = false;
      // 
      // xrTableCell478
      // 
      this.xrTableCell478.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell478.CanGrow = false;
      resources.ApplyResources(this.xrTableCell478, "xrTableCell478");
      this.xrTableCell478.Name = "xrTableCell478";
      this.xrTableCell478.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell478.StylePriority.UseBorders = false;
      this.xrTableCell478.StylePriority.UseFont = false;
      this.xrTableCell478.StylePriority.UseForeColor = false;
      this.xrTableCell478.StylePriority.UsePadding = false;
      // 
      // xrTableCell479
      // 
      this.xrTableCell479.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell479.CanGrow = false;
      resources.ApplyResources(this.xrTableCell479, "xrTableCell479");
      this.xrTableCell479.Name = "xrTableCell479";
      this.xrTableCell479.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell479.StylePriority.UseBorders = false;
      this.xrTableCell479.StylePriority.UseFont = false;
      this.xrTableCell479.StylePriority.UseForeColor = false;
      this.xrTableCell479.StylePriority.UsePadding = false;
      // 
      // xrTableCell480
      // 
      this.xrTableCell480.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell480.CanGrow = false;
      resources.ApplyResources(this.xrTableCell480, "xrTableCell480");
      this.xrTableCell480.Name = "xrTableCell480";
      this.xrTableCell480.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell480.StylePriority.UseBorders = false;
      this.xrTableCell480.StylePriority.UseFont = false;
      this.xrTableCell480.StylePriority.UseForeColor = false;
      this.xrTableCell480.StylePriority.UsePadding = false;
      // 
      // xrTableCell481
      // 
      this.xrTableCell481.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell481.CanGrow = false;
      resources.ApplyResources(this.xrTableCell481, "xrTableCell481");
      this.xrTableCell481.Name = "xrTableCell481";
      this.xrTableCell481.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell481.StylePriority.UseBorders = false;
      this.xrTableCell481.StylePriority.UseFont = false;
      this.xrTableCell481.StylePriority.UseForeColor = false;
      this.xrTableCell481.StylePriority.UsePadding = false;
      // 
      // xrTableCell482
      // 
      this.xrTableCell482.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell482.CanGrow = false;
      resources.ApplyResources(this.xrTableCell482, "xrTableCell482");
      this.xrTableCell482.Name = "xrTableCell482";
      this.xrTableCell482.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell482.StylePriority.UseBorders = false;
      this.xrTableCell482.StylePriority.UseFont = false;
      this.xrTableCell482.StylePriority.UseForeColor = false;
      this.xrTableCell482.StylePriority.UsePadding = false;
      // 
      // xrTableCell483
      // 
      this.xrTableCell483.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell483.CanGrow = false;
      resources.ApplyResources(this.xrTableCell483, "xrTableCell483");
      this.xrTableCell483.Name = "xrTableCell483";
      this.xrTableCell483.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell483.StylePriority.UseBorders = false;
      this.xrTableCell483.StylePriority.UseFont = false;
      this.xrTableCell483.StylePriority.UseForeColor = false;
      this.xrTableCell483.StylePriority.UsePadding = false;
      // 
      // xrTableCell484
      // 
      this.xrTableCell484.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell484.CanGrow = false;
      resources.ApplyResources(this.xrTableCell484, "xrTableCell484");
      this.xrTableCell484.Name = "xrTableCell484";
      this.xrTableCell484.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell484.StylePriority.UseBorders = false;
      this.xrTableCell484.StylePriority.UseFont = false;
      this.xrTableCell484.StylePriority.UseForeColor = false;
      this.xrTableCell484.StylePriority.UsePadding = false;
      // 
      // xrTableCell485
      // 
      this.xrTableCell485.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell485.CanGrow = false;
      resources.ApplyResources(this.xrTableCell485, "xrTableCell485");
      this.xrTableCell485.Name = "xrTableCell485";
      this.xrTableCell485.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell485.StylePriority.UseBorders = false;
      this.xrTableCell485.StylePriority.UseFont = false;
      this.xrTableCell485.StylePriority.UseForeColor = false;
      this.xrTableCell485.StylePriority.UsePadding = false;
      // 
      // XDSPayment
      // 
      this.XDSPayment.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.XDSPaymentDetail,
            this.XDSPaymentHeader});
      this.XDSPayment.DataMember = "XDSPaymentNotification";
      resources.ApplyResources(this.XDSPayment, "XDSPayment");
      this.XDSPayment.Level = 4;
      this.XDSPayment.Name = "XDSPayment";
      // 
      // XDSPaymentDetail
      // 
      this.XDSPaymentDetail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable49});
      resources.ApplyResources(this.XDSPaymentDetail, "XDSPaymentDetail");
      this.XDSPaymentDetail.Name = "XDSPaymentDetail";
      // 
      // xrTable49
      // 
      resources.ApplyResources(this.xrTable49, "xrTable49");
      this.xrTable49.Name = "xrTable49";
      this.xrTable49.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable49.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow69});
      this.xrTable49.StylePriority.UseBackColor = false;
      this.xrTable49.StylePriority.UseFont = false;
      this.xrTable49.StylePriority.UsePadding = false;
      // 
      // xrTableRow69
      // 
      resources.ApplyResources(this.xrTableRow69, "xrTableRow69");
      this.xrTableRow69.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableRow69.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell499,
            this.xrTableCell500,
            this.xrTableCell501,
            this.xrTableCell502,
            this.xrTableCell503});
      this.xrTableRow69.Name = "xrTableRow69";
      this.xrTableRow69.StylePriority.UseBackColor = false;
      this.xrTableRow69.StylePriority.UseBorderColor = false;
      this.xrTableRow69.StylePriority.UseBorders = false;
      this.xrTableRow69.StylePriority.UseFont = false;
      this.xrTableRow69.StylePriority.UseForeColor = false;
      this.xrTableRow69.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell499
      // 
      resources.ApplyResources(this.xrTableCell499, "xrTableCell499");
      this.xrTableCell499.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "XDSPaymentNotification.Company")});
      this.xrTableCell499.Name = "xrTableCell499";
      this.xrTableCell499.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell499.StylePriority.UseBackColor = false;
      this.xrTableCell499.StylePriority.UseBorderColor = false;
      this.xrTableCell499.StylePriority.UseFont = false;
      this.xrTableCell499.StylePriority.UseForeColor = false;
      this.xrTableCell499.StylePriority.UsePadding = false;
      this.xrTableCell499.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell500
      // 
      resources.ApplyResources(this.xrTableCell500, "xrTableCell500");
      this.xrTableCell500.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "XDSPaymentNotification.AccountNo")});
      this.xrTableCell500.Name = "xrTableCell500";
      this.xrTableCell500.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell500.StylePriority.UseBackColor = false;
      this.xrTableCell500.StylePriority.UseBorderColor = false;
      this.xrTableCell500.StylePriority.UseFont = false;
      this.xrTableCell500.StylePriority.UseForeColor = false;
      this.xrTableCell500.StylePriority.UsePadding = false;
      this.xrTableCell500.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell501
      // 
      resources.ApplyResources(this.xrTableCell501, "xrTableCell501");
      this.xrTableCell501.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "XDSPaymentNotification.DateLoaded")});
      this.xrTableCell501.Name = "xrTableCell501";
      this.xrTableCell501.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell501.StylePriority.UseBackColor = false;
      this.xrTableCell501.StylePriority.UseBorderColor = false;
      this.xrTableCell501.StylePriority.UseFont = false;
      this.xrTableCell501.StylePriority.UseForeColor = false;
      this.xrTableCell501.StylePriority.UsePadding = false;
      this.xrTableCell501.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell502
      // 
      resources.ApplyResources(this.xrTableCell502, "xrTableCell502");
      this.xrTableCell502.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "XDSPaymentNotification.Amount", "{0:c2}")});
      this.xrTableCell502.Name = "xrTableCell502";
      this.xrTableCell502.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell502.StylePriority.UseBackColor = false;
      this.xrTableCell502.StylePriority.UseBorderColor = false;
      this.xrTableCell502.StylePriority.UseFont = false;
      this.xrTableCell502.StylePriority.UseForeColor = false;
      this.xrTableCell502.StylePriority.UsePadding = false;
      this.xrTableCell502.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell503
      // 
      resources.ApplyResources(this.xrTableCell503, "xrTableCell503");
      this.xrTableCell503.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "XDSPaymentNotification.Statuscode")});
      this.xrTableCell503.Name = "xrTableCell503";
      this.xrTableCell503.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell503.StylePriority.UseBackColor = false;
      this.xrTableCell503.StylePriority.UseBorderColor = false;
      this.xrTableCell503.StylePriority.UseFont = false;
      this.xrTableCell503.StylePriority.UseForeColor = false;
      this.xrTableCell503.StylePriority.UsePadding = false;
      this.xrTableCell503.StylePriority.UseTextAlignment = false;
      // 
      // XDSPaymentHeader
      // 
      this.XDSPaymentHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable48});
      resources.ApplyResources(this.XDSPaymentHeader, "XDSPaymentHeader");
      this.XDSPaymentHeader.Name = "XDSPaymentHeader";
      // 
      // xrTable48
      // 
      this.xrTable48.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
      resources.ApplyResources(this.xrTable48, "xrTable48");
      this.xrTable48.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTable48.Name = "xrTable48";
      this.xrTable48.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable48.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow67,
            this.xrTableRow68});
      this.xrTable48.StylePriority.UseBorderColor = false;
      this.xrTable48.StylePriority.UseBorders = false;
      this.xrTable48.StylePriority.UsePadding = false;
      // 
      // xrTableRow67
      // 
      this.xrTableRow67.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell488});
      resources.ApplyResources(this.xrTableRow67, "xrTableRow67");
      this.xrTableRow67.Name = "xrTableRow67";
      // 
      // xrTableCell488
      // 
      resources.ApplyResources(this.xrTableCell488, "xrTableCell488");
      this.xrTableCell488.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell488.CanGrow = false;
      this.xrTableCell488.Name = "xrTableCell488";
      this.xrTableCell488.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell488.StylePriority.UseBackColor = false;
      this.xrTableCell488.StylePriority.UseBorderColor = false;
      this.xrTableCell488.StylePriority.UseBorders = false;
      this.xrTableCell488.StylePriority.UseFont = false;
      this.xrTableCell488.StylePriority.UseForeColor = false;
      this.xrTableCell488.StylePriority.UsePadding = false;
      this.xrTableCell488.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow68
      // 
      resources.ApplyResources(this.xrTableRow68, "xrTableRow68");
      this.xrTableRow68.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableRow68.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell490,
            this.xrTableCell492,
            this.xrTableCell494,
            this.xrTableCell496,
            this.xrTableCell498});
      this.xrTableRow68.Name = "xrTableRow68";
      this.xrTableRow68.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTableRow68.StylePriority.UseBackColor = false;
      this.xrTableRow68.StylePriority.UseBorderColor = false;
      this.xrTableRow68.StylePriority.UseBorders = false;
      this.xrTableRow68.StylePriority.UseFont = false;
      this.xrTableRow68.StylePriority.UseForeColor = false;
      this.xrTableRow68.StylePriority.UsePadding = false;
      this.xrTableRow68.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell490
      // 
      this.xrTableCell490.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell490.CanGrow = false;
      resources.ApplyResources(this.xrTableCell490, "xrTableCell490");
      this.xrTableCell490.Name = "xrTableCell490";
      this.xrTableCell490.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell490.StylePriority.UseBorders = false;
      this.xrTableCell490.StylePriority.UseFont = false;
      this.xrTableCell490.StylePriority.UseForeColor = false;
      this.xrTableCell490.StylePriority.UsePadding = false;
      // 
      // xrTableCell492
      // 
      this.xrTableCell492.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell492.CanGrow = false;
      resources.ApplyResources(this.xrTableCell492, "xrTableCell492");
      this.xrTableCell492.Name = "xrTableCell492";
      this.xrTableCell492.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell492.StylePriority.UseBorders = false;
      this.xrTableCell492.StylePriority.UseFont = false;
      this.xrTableCell492.StylePriority.UseForeColor = false;
      this.xrTableCell492.StylePriority.UsePadding = false;
      // 
      // xrTableCell494
      // 
      this.xrTableCell494.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell494.CanGrow = false;
      resources.ApplyResources(this.xrTableCell494, "xrTableCell494");
      this.xrTableCell494.Name = "xrTableCell494";
      this.xrTableCell494.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell494.StylePriority.UseBorders = false;
      this.xrTableCell494.StylePriority.UseFont = false;
      this.xrTableCell494.StylePriority.UseForeColor = false;
      this.xrTableCell494.StylePriority.UsePadding = false;
      // 
      // xrTableCell496
      // 
      this.xrTableCell496.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell496.CanGrow = false;
      resources.ApplyResources(this.xrTableCell496, "xrTableCell496");
      this.xrTableCell496.Name = "xrTableCell496";
      this.xrTableCell496.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell496.StylePriority.UseBorders = false;
      this.xrTableCell496.StylePriority.UseFont = false;
      this.xrTableCell496.StylePriority.UseForeColor = false;
      this.xrTableCell496.StylePriority.UsePadding = false;
      // 
      // xrTableCell498
      // 
      this.xrTableCell498.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell498.CanGrow = false;
      resources.ApplyResources(this.xrTableCell498, "xrTableCell498");
      this.xrTableCell498.Name = "xrTableCell498";
      this.xrTableCell498.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell498.StylePriority.UseBorders = false;
      this.xrTableCell498.StylePriority.UseFont = false;
      this.xrTableCell498.StylePriority.UseForeColor = false;
      this.xrTableCell498.StylePriority.UsePadding = false;
      // 
      // DetailReport9
      // 
      this.DetailReport9.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail5,
            this.GroupHeader5});
      this.DetailReport9.DataMember = "ConsumerAdminOrder";
      resources.ApplyResources(this.DetailReport9, "DetailReport9");
      this.DetailReport9.Level = 0;
      this.DetailReport9.Name = "DetailReport9";
      // 
      // Detail5
      // 
      this.Detail5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable42});
      resources.ApplyResources(this.Detail5, "Detail5");
      this.Detail5.Name = "Detail5";
      // 
      // xrTable42
      // 
      resources.ApplyResources(this.xrTable42, "xrTable42");
      this.xrTable42.Name = "xrTable42";
      this.xrTable42.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable42.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow44});
      this.xrTable42.StylePriority.UseBackColor = false;
      this.xrTable42.StylePriority.UseFont = false;
      this.xrTable42.StylePriority.UsePadding = false;
      // 
      // xrTableRow44
      // 
      resources.ApplyResources(this.xrTableRow44, "xrTableRow44");
      this.xrTableRow44.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell200,
            this.xrTableCell201,
            this.xrTableCell202,
            this.xrTableCell203,
            this.xrTableCell204,
            this.xrTableCell205,
            this.xrTableCell206,
            this.xrTableCell438,
            this.xrTableCell439,
            this.xrTableCell441});
      this.xrTableRow44.Name = "xrTableRow44";
      this.xrTableRow44.StylePriority.UseBackColor = false;
      this.xrTableRow44.StylePriority.UseBorderColor = false;
      this.xrTableRow44.StylePriority.UseBorders = false;
      this.xrTableRow44.StylePriority.UseFont = false;
      this.xrTableRow44.StylePriority.UseForeColor = false;
      this.xrTableRow44.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell200
      // 
      resources.ApplyResources(this.xrTableCell200, "xrTableCell200");
      this.xrTableCell200.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.CaseNumber")});
      this.xrTableCell200.Name = "xrTableCell200";
      this.xrTableCell200.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell200.StylePriority.UseBackColor = false;
      this.xrTableCell200.StylePriority.UseBorderColor = false;
      this.xrTableCell200.StylePriority.UseFont = false;
      this.xrTableCell200.StylePriority.UseForeColor = false;
      this.xrTableCell200.StylePriority.UsePadding = false;
      this.xrTableCell200.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell201
      // 
      resources.ApplyResources(this.xrTableCell201, "xrTableCell201");
      this.xrTableCell201.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.LastUpdatedDate")});
      this.xrTableCell201.Name = "xrTableCell201";
      this.xrTableCell201.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell201.StylePriority.UseBackColor = false;
      this.xrTableCell201.StylePriority.UseBorderColor = false;
      this.xrTableCell201.StylePriority.UseFont = false;
      this.xrTableCell201.StylePriority.UseForeColor = false;
      this.xrTableCell201.StylePriority.UsePadding = false;
      this.xrTableCell201.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell202
      // 
      resources.ApplyResources(this.xrTableCell202, "xrTableCell202");
      this.xrTableCell202.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.CaseFilingDate")});
      this.xrTableCell202.Name = "xrTableCell202";
      this.xrTableCell202.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell202.StylePriority.UseBackColor = false;
      this.xrTableCell202.StylePriority.UseBorderColor = false;
      this.xrTableCell202.StylePriority.UseFont = false;
      this.xrTableCell202.StylePriority.UseForeColor = false;
      this.xrTableCell202.StylePriority.UsePadding = false;
      this.xrTableCell202.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell203
      // 
      resources.ApplyResources(this.xrTableCell203, "xrTableCell203");
      this.xrTableCell203.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.CaseType")});
      this.xrTableCell203.Name = "xrTableCell203";
      this.xrTableCell203.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell203.StylePriority.UseBackColor = false;
      this.xrTableCell203.StylePriority.UseBorderColor = false;
      this.xrTableCell203.StylePriority.UseFont = false;
      this.xrTableCell203.StylePriority.UseForeColor = false;
      this.xrTableCell203.StylePriority.UsePadding = false;
      this.xrTableCell203.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell204
      // 
      resources.ApplyResources(this.xrTableCell204, "xrTableCell204");
      this.xrTableCell204.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.DisputeAmt", "{0:c2}")});
      this.xrTableCell204.Name = "xrTableCell204";
      this.xrTableCell204.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell204.StylePriority.UseBackColor = false;
      this.xrTableCell204.StylePriority.UseBorderColor = false;
      this.xrTableCell204.StylePriority.UseFont = false;
      this.xrTableCell204.StylePriority.UseForeColor = false;
      this.xrTableCell204.StylePriority.UsePadding = false;
      this.xrTableCell204.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell205
      // 
      resources.ApplyResources(this.xrTableCell205, "xrTableCell205");
      this.xrTableCell205.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.PlaintiffName")});
      this.xrTableCell205.Name = "xrTableCell205";
      this.xrTableCell205.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell205.StylePriority.UseBackColor = false;
      this.xrTableCell205.StylePriority.UseBorderColor = false;
      this.xrTableCell205.StylePriority.UseFont = false;
      this.xrTableCell205.StylePriority.UseForeColor = false;
      this.xrTableCell205.StylePriority.UsePadding = false;
      this.xrTableCell205.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell206
      // 
      resources.ApplyResources(this.xrTableCell206, "xrTableCell206");
      this.xrTableCell206.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.CourtName")});
      this.xrTableCell206.Name = "xrTableCell206";
      this.xrTableCell206.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell206.StylePriority.UseBackColor = false;
      this.xrTableCell206.StylePriority.UseBorderColor = false;
      this.xrTableCell206.StylePriority.UseFont = false;
      this.xrTableCell206.StylePriority.UseForeColor = false;
      this.xrTableCell206.StylePriority.UsePadding = false;
      this.xrTableCell206.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell438
      // 
      resources.ApplyResources(this.xrTableCell438, "xrTableCell438");
      this.xrTableCell438.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.AttorneyName")});
      this.xrTableCell438.Name = "xrTableCell438";
      this.xrTableCell438.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell438.StylePriority.UseBackColor = false;
      this.xrTableCell438.StylePriority.UseBorderColor = false;
      this.xrTableCell438.StylePriority.UseFont = false;
      this.xrTableCell438.StylePriority.UseForeColor = false;
      this.xrTableCell438.StylePriority.UsePadding = false;
      this.xrTableCell438.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell439
      // 
      resources.ApplyResources(this.xrTableCell439, "xrTableCell439");
      this.xrTableCell439.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.TelephoneNo")});
      this.xrTableCell439.Name = "xrTableCell439";
      this.xrTableCell439.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell439.StylePriority.UseBackColor = false;
      this.xrTableCell439.StylePriority.UseBorderColor = false;
      this.xrTableCell439.StylePriority.UseFont = false;
      this.xrTableCell439.StylePriority.UseForeColor = false;
      this.xrTableCell439.StylePriority.UsePadding = false;
      this.xrTableCell439.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell441
      // 
      resources.ApplyResources(this.xrTableCell441, "xrTableCell441");
      this.xrTableCell441.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ConsumerAdminOrder.Comments")});
      this.xrTableCell441.Name = "xrTableCell441";
      this.xrTableCell441.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell441.StylePriority.UseBackColor = false;
      this.xrTableCell441.StylePriority.UseBorderColor = false;
      this.xrTableCell441.StylePriority.UseFont = false;
      this.xrTableCell441.StylePriority.UseForeColor = false;
      this.xrTableCell441.StylePriority.UsePadding = false;
      this.xrTableCell441.StylePriority.UseTextAlignment = false;
      // 
      // GroupHeader5
      // 
      this.GroupHeader5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable41});
      resources.ApplyResources(this.GroupHeader5, "GroupHeader5");
      this.GroupHeader5.Name = "GroupHeader5";
      // 
      // xrTable41
      // 
      this.xrTable41.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
      resources.ApplyResources(this.xrTable41, "xrTable41");
      this.xrTable41.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTable41.Name = "xrTable41";
      this.xrTable41.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTable41.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow29,
            this.xrTableRow30});
      this.xrTable41.StylePriority.UseBorderColor = false;
      this.xrTable41.StylePriority.UseBorders = false;
      this.xrTable41.StylePriority.UsePadding = false;
      // 
      // xrTableRow29
      // 
      this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell180});
      resources.ApplyResources(this.xrTableRow29, "xrTableRow29");
      this.xrTableRow29.Name = "xrTableRow29";
      // 
      // xrTableCell180
      // 
      resources.ApplyResources(this.xrTableCell180, "xrTableCell180");
      this.xrTableCell180.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTableCell180.CanGrow = false;
      this.xrTableCell180.Name = "xrTableCell180";
      this.xrTableCell180.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell180.StylePriority.UseBackColor = false;
      this.xrTableCell180.StylePriority.UseBorderColor = false;
      this.xrTableCell180.StylePriority.UseBorders = false;
      this.xrTableCell180.StylePriority.UseFont = false;
      this.xrTableCell180.StylePriority.UseForeColor = false;
      this.xrTableCell180.StylePriority.UsePadding = false;
      this.xrTableCell180.StylePriority.UseTextAlignment = false;
      // 
      // xrTableRow30
      // 
      resources.ApplyResources(this.xrTableRow30, "xrTableRow30");
      this.xrTableRow30.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell181,
            this.xrTableCell182,
            this.xrTableCell183,
            this.xrTableCell184,
            this.xrTableCell185,
            this.xrTableCell186,
            this.xrTableCell187,
            this.xrTableCell188,
            this.xrTableCell189,
            this.xrTableCell190});
      this.xrTableRow30.Name = "xrTableRow30";
      this.xrTableRow30.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 254F);
      this.xrTableRow30.StylePriority.UseBackColor = false;
      this.xrTableRow30.StylePriority.UseBorderColor = false;
      this.xrTableRow30.StylePriority.UseBorders = false;
      this.xrTableRow30.StylePriority.UseFont = false;
      this.xrTableRow30.StylePriority.UseForeColor = false;
      this.xrTableRow30.StylePriority.UsePadding = false;
      this.xrTableRow30.StylePriority.UseTextAlignment = false;
      // 
      // xrTableCell181
      // 
      this.xrTableCell181.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell181.CanGrow = false;
      resources.ApplyResources(this.xrTableCell181, "xrTableCell181");
      this.xrTableCell181.Name = "xrTableCell181";
      this.xrTableCell181.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell181.StylePriority.UseBorders = false;
      this.xrTableCell181.StylePriority.UseFont = false;
      this.xrTableCell181.StylePriority.UseForeColor = false;
      this.xrTableCell181.StylePriority.UsePadding = false;
      // 
      // xrTableCell182
      // 
      this.xrTableCell182.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell182.CanGrow = false;
      resources.ApplyResources(this.xrTableCell182, "xrTableCell182");
      this.xrTableCell182.Name = "xrTableCell182";
      this.xrTableCell182.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell182.StylePriority.UseBorders = false;
      this.xrTableCell182.StylePriority.UseFont = false;
      this.xrTableCell182.StylePriority.UseForeColor = false;
      this.xrTableCell182.StylePriority.UsePadding = false;
      // 
      // xrTableCell183
      // 
      this.xrTableCell183.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell183.CanGrow = false;
      resources.ApplyResources(this.xrTableCell183, "xrTableCell183");
      this.xrTableCell183.Name = "xrTableCell183";
      this.xrTableCell183.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell183.StylePriority.UseBorders = false;
      this.xrTableCell183.StylePriority.UseFont = false;
      this.xrTableCell183.StylePriority.UseForeColor = false;
      this.xrTableCell183.StylePriority.UsePadding = false;
      // 
      // xrTableCell184
      // 
      this.xrTableCell184.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell184.CanGrow = false;
      resources.ApplyResources(this.xrTableCell184, "xrTableCell184");
      this.xrTableCell184.Name = "xrTableCell184";
      this.xrTableCell184.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell184.StylePriority.UseBorders = false;
      this.xrTableCell184.StylePriority.UseFont = false;
      this.xrTableCell184.StylePriority.UseForeColor = false;
      this.xrTableCell184.StylePriority.UsePadding = false;
      // 
      // xrTableCell185
      // 
      this.xrTableCell185.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell185.CanGrow = false;
      resources.ApplyResources(this.xrTableCell185, "xrTableCell185");
      this.xrTableCell185.Name = "xrTableCell185";
      this.xrTableCell185.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell185.StylePriority.UseBorders = false;
      this.xrTableCell185.StylePriority.UseFont = false;
      this.xrTableCell185.StylePriority.UseForeColor = false;
      this.xrTableCell185.StylePriority.UsePadding = false;
      // 
      // xrTableCell186
      // 
      this.xrTableCell186.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell186.CanGrow = false;
      resources.ApplyResources(this.xrTableCell186, "xrTableCell186");
      this.xrTableCell186.Name = "xrTableCell186";
      this.xrTableCell186.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell186.StylePriority.UseBorders = false;
      this.xrTableCell186.StylePriority.UseFont = false;
      this.xrTableCell186.StylePriority.UseForeColor = false;
      this.xrTableCell186.StylePriority.UsePadding = false;
      // 
      // xrTableCell187
      // 
      this.xrTableCell187.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell187.CanGrow = false;
      resources.ApplyResources(this.xrTableCell187, "xrTableCell187");
      this.xrTableCell187.Name = "xrTableCell187";
      this.xrTableCell187.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell187.StylePriority.UseBorders = false;
      this.xrTableCell187.StylePriority.UseFont = false;
      this.xrTableCell187.StylePriority.UseForeColor = false;
      this.xrTableCell187.StylePriority.UsePadding = false;
      // 
      // xrTableCell188
      // 
      this.xrTableCell188.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell188.CanGrow = false;
      resources.ApplyResources(this.xrTableCell188, "xrTableCell188");
      this.xrTableCell188.Name = "xrTableCell188";
      this.xrTableCell188.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell188.StylePriority.UseBorders = false;
      this.xrTableCell188.StylePriority.UseFont = false;
      this.xrTableCell188.StylePriority.UseForeColor = false;
      this.xrTableCell188.StylePriority.UsePadding = false;
      // 
      // xrTableCell189
      // 
      this.xrTableCell189.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell189.CanGrow = false;
      resources.ApplyResources(this.xrTableCell189, "xrTableCell189");
      this.xrTableCell189.Name = "xrTableCell189";
      this.xrTableCell189.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell189.StylePriority.UseBorders = false;
      this.xrTableCell189.StylePriority.UseFont = false;
      this.xrTableCell189.StylePriority.UseForeColor = false;
      this.xrTableCell189.StylePriority.UsePadding = false;
      // 
      // xrTableCell190
      // 
      this.xrTableCell190.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell190.CanGrow = false;
      resources.ApplyResources(this.xrTableCell190, "xrTableCell190");
      this.xrTableCell190.Name = "xrTableCell190";
      this.xrTableCell190.Padding = new DevExpress.XtraPrinting.PaddingInfo(11, 0, 0, 0, 254F);
      this.xrTableCell190.StylePriority.UseBorders = false;
      this.xrTableCell190.StylePriority.UseFont = false;
      this.xrTableCell190.StylePriority.UseForeColor = false;
      this.xrTableCell190.StylePriority.UsePadding = false;
      // 
      // Test_MVV
      // 
      this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.PageFooter,
            this.MaritalStatus,
            this.SumamryBlockA,
            this.SummaryBlockB,
            this.SummaryBlockC,
            this.SumamryBlockD,
            this.CreditAccountInformation_CAS,
            this.CreditAccountInformation_NLR,
            this.PropertyInfo,
            this.CIPC,
            this.DetailReport,
            this.TelephoneLinkage,
            this.Adverse});
      resources.ApplyResources(this, "$this");
      this.ScriptsSource = resources.GetString("$this.ScriptsSource");
      this.Version = "14.2";
      this.XmlDataPath = "D:\\Projects\\shbs\\DevExpress\\XML Datasource\\MVV.xml";
      ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable18)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable22)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable27)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable23)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.tblMonthlyPaymentH)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable24)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.tblConsumerDefinitionC)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable25)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable51)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable50)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable29)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable30)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable31)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable33)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable32)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable21)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable19)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.tblPropInterests)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable34)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.tblDirInfo)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable35)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable37)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable36)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.tblLikagesHC)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable20)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable39)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable38)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable40)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable44)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable43)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable46)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable45)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.tblDbtReviewD)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable47)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable49)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable48)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable42)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable41)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

    }

    #endregion

    private DevExpress.XtraReports.UI.DetailBand Detail;
    private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
    private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
    private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
    private DevExpress.XtraReports.UI.XRLabel xrLabel21;
    private DevExpress.XtraReports.UI.XRLabel xrLabel22;
    private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
    private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
    private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
    private DevExpress.XtraReports.UI.XRLabel xrLabel95;
    private DevExpress.XtraReports.UI.XRLabel xrLabel16;
    private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo2;
    private DevExpress.XtraReports.UI.XRPanel xrPanel1;
    private DevExpress.XtraReports.UI.DetailReportBand SumamryBlockA;
    private DevExpress.XtraReports.UI.DetailBand Detail12;
    private DevExpress.XtraReports.UI.XRTable xrTable10;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow43;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow42;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell120;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow41;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell117;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell116;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell98;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell103;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
    private DevExpress.XtraReports.UI.XRTableCell lblTest;
    private DevExpress.XtraReports.UI.DetailReportBand SummaryBlockB;
    private DevExpress.XtraReports.UI.DetailBand Detail14;
    private DevExpress.XtraReports.UI.XRTable xrTable12;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow47;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell124;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow48;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell126;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow50;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell129;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell130;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow52;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell133;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell134;
    private DevExpress.XtraReports.UI.DetailReportBand SummaryBlockC;
    private DevExpress.XtraReports.UI.DetailBand Detail15;
    private DevExpress.XtraReports.UI.XRTable xrTable18;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow113;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell261;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell262;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow114;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell263;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell264;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow115;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell265;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell266;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow116;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell267;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell268;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow117;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell269;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell270;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow118;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell271;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell272;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow119;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell273;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell274;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow120;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell275;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell276;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow121;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell277;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell278;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow122;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell279;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell280;
    private DevExpress.XtraReports.UI.XRTable xrTable14;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow73;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell149;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell150;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow75;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell151;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell152;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow76;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell153;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell154;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow77;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell155;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell156;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow78;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell157;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell158;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow79;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell159;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell160;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow80;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell161;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell162;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow81;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell163;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell164;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow82;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell165;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell169;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow83;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell170;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell174;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow84;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell175;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell176;
    private DevExpress.XtraReports.UI.DetailReportBand SumamryBlockD;
    private DevExpress.XtraReports.UI.DetailBand Detail16;
    private DevExpress.XtraReports.UI.DetailReportBand CreditAccountInformation_CAS;
    private DevExpress.XtraReports.UI.DetailBand Detail13;
    private DevExpress.XtraReports.UI.XRTable xrTable17;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow100;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell235;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell236;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow101;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell237;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell238;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow126;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell287;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell288;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow102;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell239;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell240;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow103;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell241;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell242;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow104;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell243;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell244;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow105;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell245;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell246;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow106;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell247;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell248;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow107;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell249;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell250;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow108;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell251;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell252;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow109;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell253;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell254;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow110;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell255;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell256;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow111;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell257;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell258;
    private DevExpress.DataAccess.Sql.SqlDataSource sqlDataSource1;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
    private DevExpress.XtraReports.UI.XRTable xrTable8;
    private DevExpress.XtraReports.UI.DetailBand Detail11;
    private DevExpress.XtraReports.UI.DetailReportBand MaritalStatus;
    private DevExpress.XtraReports.UI.DetailReportBand DetailReport2;
    private DevExpress.XtraReports.UI.DetailBand Detail18;
    private DevExpress.XtraReports.UI.XRTable xrTable23;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow112;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell291;
    private DevExpress.XtraReports.UI.DetailReportBand DetailReport1;
    private DevExpress.XtraReports.UI.DetailBand Detail17;
    private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader9;
    private DevExpress.XtraReports.UI.XRTable tblMonthlyPaymentH;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow128;
    private DevExpress.XtraReports.UI.XRTableCell lblcompanyMP;
    private DevExpress.XtraReports.UI.XRTableCell lblM01;
    private DevExpress.XtraReports.UI.XRTableCell lblM02;
    private DevExpress.XtraReports.UI.XRTableCell lblM03;
    private DevExpress.XtraReports.UI.XRTableCell lblM04;
    private DevExpress.XtraReports.UI.XRTableCell lblM05;
    private DevExpress.XtraReports.UI.XRTableCell lblM06;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell259;
    private DevExpress.XtraReports.UI.XRTableCell lblM08;
    private DevExpress.XtraReports.UI.XRTableCell lblM09;
    private DevExpress.XtraReports.UI.XRTableCell lblM10;
    private DevExpress.XtraReports.UI.XRTableCell lblM11;
    private DevExpress.XtraReports.UI.XRTableCell lblM12;
    private DevExpress.XtraReports.UI.XRTableCell lblM13;
    private DevExpress.XtraReports.UI.XRTableCell lblM14;
    private DevExpress.XtraReports.UI.XRTableCell lblM15;
    private DevExpress.XtraReports.UI.XRTableCell lblM16;
    private DevExpress.XtraReports.UI.XRTableCell lblM17;
    private DevExpress.XtraReports.UI.XRTableCell lblM18;
    private DevExpress.XtraReports.UI.XRTableCell lblM19;
    private DevExpress.XtraReports.UI.XRTableCell lblM20;
    private DevExpress.XtraReports.UI.XRTableCell lblM21;
    private DevExpress.XtraReports.UI.XRTableCell lblM22;
    private DevExpress.XtraReports.UI.XRTableCell lblM23;
    private DevExpress.XtraReports.UI.XRTableCell lblM24;
    private DevExpress.XtraReports.UI.DetailReportBand DetailReport3;
    private DevExpress.XtraReports.UI.DetailBand Detail19;
    private DevExpress.XtraReports.UI.XRTable xrTable24;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow134;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell290;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell292;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell293;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell307;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell308;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell309;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell310;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell311;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell312;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell313;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell314;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell318;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell319;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell320;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell321;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell322;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell323;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell324;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell325;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell326;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell327;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell328;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell329;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell331;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell332;
    private DevExpress.XtraReports.UI.DetailReportBand DetailReport4;
    private DevExpress.XtraReports.UI.DetailBand Detail20;
    private DevExpress.XtraReports.UI.XRTable tblConsumerDefinitionC;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow133;
    private DevExpress.XtraReports.UI.XRTableCell lblDefDesc;
    private DevExpress.XtraReports.UI.XRTableCell lbDefCode;
    private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader10;
    private DevExpress.XtraReports.UI.XRTable xrTable25;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow135;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell260;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell333;
    private DevExpress.XtraReports.UI.DetailReportBand CreditAccountInformation_NLR;
    private DevExpress.XtraReports.UI.DetailBand Detail21;
    private DevExpress.XtraReports.UI.DetailReportBand DetailReport5;
    private DevExpress.XtraReports.UI.DetailBand Detail22;
    private DevExpress.XtraReports.UI.XRTable xrTable29;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow141;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell358;
    private DevExpress.XtraReports.UI.DetailReportBand DetailReport6;
    private DevExpress.XtraReports.UI.DetailBand Detail23;
    private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader12;
    private DevExpress.XtraReports.UI.XRTable xrTable30;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow143;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell360;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell361;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell362;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell363;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell364;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell365;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell366;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell367;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell368;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell369;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell370;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell371;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell372;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell373;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell374;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell375;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell376;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell377;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell378;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell379;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell380;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell381;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell382;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell383;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell384;
    private DevExpress.XtraReports.UI.DetailReportBand DetailReport7;
    private DevExpress.XtraReports.UI.DetailBand Detail24;
    private DevExpress.XtraReports.UI.XRTable xrTable31;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow144;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell385;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell386;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell387;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell388;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell389;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell390;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell391;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell392;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell393;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell394;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell395;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell396;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell397;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell398;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell399;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell400;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell401;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell402;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell403;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell404;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell405;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell406;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell407;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell408;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell409;
    private DevExpress.XtraReports.UI.DetailReportBand DetailReport8;
    private DevExpress.XtraReports.UI.DetailBand Detail25;
    private DevExpress.XtraReports.UI.XRTable xrTable33;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow146;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell412;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell413;
    private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader13;
    private DevExpress.XtraReports.UI.XRTable xrTable32;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow145;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell410;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell411;
    private DevExpress.XtraReports.UI.DetailReportBand PropertyInfo;
    private DevExpress.XtraReports.UI.DetailBand Detail26;
    private DevExpress.XtraReports.UI.XRTable tblPropInterests;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow148;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell416;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow149;
    private DevExpress.XtraReports.UI.XRTableCell lblDeedNo;
    private DevExpress.XtraReports.UI.XRTableCell lblDDeedNo;
    private DevExpress.XtraReports.UI.XRTableCell lblsiteno;
    private DevExpress.XtraReports.UI.XRTableCell lblDsiteno;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow150;
    private DevExpress.XtraReports.UI.XRTableCell lblDeedsoffice;
    private DevExpress.XtraReports.UI.XRTableCell lblDDeedsoffice;
    private DevExpress.XtraReports.UI.XRTableCell lblPhyadd;
    private DevExpress.XtraReports.UI.XRTableCell lblDPhyadd;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow151;
    private DevExpress.XtraReports.UI.XRTableCell lblPropType;
    private DevExpress.XtraReports.UI.XRTableCell lblDPropType;
    private DevExpress.XtraReports.UI.XRTableCell lblSize;
    private DevExpress.XtraReports.UI.XRTableCell lblDSize;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow152;
    private DevExpress.XtraReports.UI.XRTableCell lblPurchasedate;
    private DevExpress.XtraReports.UI.XRTableCell lblDPurchasedate;
    private DevExpress.XtraReports.UI.XRTableCell lblPurchaseprice;
    private DevExpress.XtraReports.UI.XRTableCell lblDPurchaseprice;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow153;
    private DevExpress.XtraReports.UI.XRTableCell lblshareperc;
    private DevExpress.XtraReports.UI.XRTableCell lblDBondNo;
    private DevExpress.XtraReports.UI.XRTableCell lblHolder;
    private DevExpress.XtraReports.UI.XRTableCell lblDHolder;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow155;
    private DevExpress.XtraReports.UI.XRTableCell lblBondNo;
    private DevExpress.XtraReports.UI.XRTableCell lblDBondAmount;
    private DevExpress.XtraReports.UI.XRTableCell lblBondAmount;
    private DevExpress.XtraReports.UI.XRTableCell lbl50;
    private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader14;
    private DevExpress.XtraReports.UI.XRTable xrTable34;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow147;
    private DevExpress.XtraReports.UI.XRTableCell lblPropInterestsH;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell414;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow175;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell415;
    private DevExpress.XtraReports.UI.DetailReportBand CIPC;
    private DevExpress.XtraReports.UI.DetailBand Detail27;
    private DevExpress.XtraReports.UI.XRTable tblDirInfo;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow157;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell420;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow158;
    private DevExpress.XtraReports.UI.XRTableCell lblCurrentPost;
    private DevExpress.XtraReports.UI.XRTableCell lblDCurrentPost;
    private DevExpress.XtraReports.UI.XRTableCell lblInceptionDate;
    private DevExpress.XtraReports.UI.XRTableCell lblDAppDate;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow159;
    private DevExpress.XtraReports.UI.XRTableCell lblCompanyName;
    private DevExpress.XtraReports.UI.XRTableCell lblDCompanyName;
    private DevExpress.XtraReports.UI.XRTableCell lblRegNo;
    private DevExpress.XtraReports.UI.XRTableCell lblDRegNo;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow160;
    private DevExpress.XtraReports.UI.XRTableCell lblCompanyAdd;
    private DevExpress.XtraReports.UI.XRTableCell lblDPhysicalAdd;
    private DevExpress.XtraReports.UI.XRTableCell lblPhoneNo;
    private DevExpress.XtraReports.UI.XRTableCell lblDPhoneNo;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow161;
    private DevExpress.XtraReports.UI.XRTableCell lblIndustryCatg;
    private DevExpress.XtraReports.UI.XRTableCell lblDIndCateg;
    private DevExpress.XtraReports.UI.XRTableCell lblDirectorStatus;
    private DevExpress.XtraReports.UI.XRTableCell lblDDirectorStatus;
    private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader15;
    private DevExpress.XtraReports.UI.XRTable xrTable35;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow156;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell417;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell418;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow176;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell419;
    private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
    private DevExpress.XtraReports.UI.DetailBand Detail28;
    private DevExpress.XtraReports.UI.XRTable xrTable37;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow164;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell424;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow165;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell425;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell428;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow166;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell429;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell432;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow167;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell433;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell436;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow171;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell434;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell435;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow170;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell430;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell431;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow169;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell426;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell427;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow168;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell437;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell440;
    private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader16;
    private DevExpress.XtraReports.UI.XRTable xrTable36;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow162;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell421;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell422;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow163;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell423;
    private DevExpress.XtraReports.UI.DetailReportBand TelephoneLinkage;
    private DevExpress.XtraReports.UI.DetailBand Detail29;
    private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
    private DevExpress.XtraReports.UI.XRTable tblLikagesHC;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
    private DevExpress.XtraReports.UI.DetailReportBand HomeLinkage;
    private DevExpress.XtraReports.UI.DetailBand Detail1;
    private DevExpress.XtraReports.UI.XRTable xrTable2;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
    private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader2;
    private DevExpress.XtraReports.UI.XRTable xrTable1;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
    private DevExpress.XtraReports.UI.DetailReportBand WorkLinkage;
    private DevExpress.XtraReports.UI.DetailBand Detail2;
    private DevExpress.XtraReports.UI.XRTable xrTable5;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
    private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader3;
    private DevExpress.XtraReports.UI.XRTable xrTable3;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
    private DevExpress.XtraReports.UI.DetailReportBand CellularLinkage;
    private DevExpress.XtraReports.UI.DetailBand Detail3;
    private DevExpress.XtraReports.UI.XRTable xrTable6;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
    private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader4;
    private DevExpress.XtraReports.UI.XRTable xrTable4;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
    private DevExpress.XtraReports.UI.DetailReportBand Adverse;
    private DevExpress.XtraReports.UI.DetailBand Detail4;
    private DevExpress.XtraReports.UI.DetailReportBand PublicDomain_Adverse;
    private DevExpress.XtraReports.UI.DetailBand AdverseDetail;
    private DevExpress.XtraReports.UI.GroupHeaderBand AdverseHeader;
    private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader1;
    private DevExpress.XtraReports.UI.XRTable xrTable9;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
    private DevExpress.XtraReports.UI.XRTable xrTable13;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
    private DevExpress.XtraReports.UI.XRTable xrTable11;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
    private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader2;
    private DevExpress.XtraReports.UI.XRTable xrTable7;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
    private DevExpress.XtraReports.UI.DetailReportBand PublicDomain_Default;
    private DevExpress.XtraReports.UI.DetailBand DefaultDetail;
    private DevExpress.XtraReports.UI.XRTable xrTable20;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
    private DevExpress.XtraReports.UI.GroupHeaderBand DefaultHeader;
    private DevExpress.XtraReports.UI.XRTable xrTable15;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
    private DevExpress.XtraReports.UI.DetailReportBand PublicDomain_Judgements;
    private DevExpress.XtraReports.UI.DetailBand JudgementDetail;
    private DevExpress.XtraReports.UI.XRTable xrTable39;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell96;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell172;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell171;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell168;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell167;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell99;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell102;
    private DevExpress.XtraReports.UI.GroupHeaderBand JudgementHeader;
    private DevExpress.XtraReports.UI.XRTable xrTable38;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell111;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell166;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
    private DevExpress.XtraReports.UI.DetailReportBand CourtNotice_Admin;
    private DevExpress.XtraReports.UI.DetailBand AdminDetail;
    private DevExpress.XtraReports.UI.XRTable xrTable42;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow44;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell200;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell201;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell202;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell203;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell204;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell205;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell206;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell438;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell439;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell441;
    private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader3;
    private DevExpress.XtraReports.UI.XRTable xrTable40;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell173;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell179;
    private DevExpress.XtraReports.UI.XRTable xrTable41;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell180;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell181;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell182;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell183;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell184;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell185;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell186;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell187;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell188;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell189;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell190;
    private DevExpress.XtraReports.UI.DetailReportBand Sequestration;
    private DevExpress.XtraReports.UI.DetailBand SequestrationDetail;
    private DevExpress.XtraReports.UI.XRTable xrTable44;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow46;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell444;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell445;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell446;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell447;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell448;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell449;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell450;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell451;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell452;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell453;
    private DevExpress.XtraReports.UI.GroupHeaderBand SequestrationHeader;
    private DevExpress.XtraReports.UI.XRTable xrTable43;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell191;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow45;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell192;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell193;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell194;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell195;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell196;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell197;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell198;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell199;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell442;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell443;
    private DevExpress.XtraReports.UI.DetailReportBand Rehabilitation;
    private DevExpress.XtraReports.UI.DetailBand RehabilitationDetail;
    private DevExpress.XtraReports.UI.XRTable xrTable46;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow56;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell465;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell466;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell467;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell468;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell469;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell470;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell471;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell472;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell473;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell474;
    private DevExpress.XtraReports.UI.GroupHeaderBand RehabilitationHeader;
    private DevExpress.XtraReports.UI.XRTable xrTable45;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow54;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell454;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow55;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell455;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell456;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell457;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell458;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell459;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell460;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell461;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell462;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell463;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell464;
    private DevExpress.XtraReports.UI.DetailReportBand DebtReview;
    private DevExpress.XtraReports.UI.DetailBand DebtReviewDetail;
    private DevExpress.XtraReports.UI.XRTable tblDbtReviewD;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow59;
    private DevExpress.XtraReports.UI.XRTableCell lblDbtReviewDate;
    private DevExpress.XtraReports.UI.XRTableCell lblDbtReviewDateD;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow60;
    private DevExpress.XtraReports.UI.XRTableCell lblCounsellorName;
    private DevExpress.XtraReports.UI.XRTableCell lblCounsellorNameD;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow61;
    private DevExpress.XtraReports.UI.XRTableCell lblCounsellorTelephoneNo;
    private DevExpress.XtraReports.UI.XRTableCell lblCounsellorTelephoneNoD;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow63;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell486;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell487;
    private DevExpress.XtraReports.UI.GroupHeaderBand DebtReviewHeader;
    private DevExpress.XtraReports.UI.XRTable xrTable47;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow57;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell475;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow58;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell476;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell477;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell478;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell479;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell480;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell481;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell482;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell483;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell484;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell485;
    private DevExpress.XtraReports.UI.DetailReportBand XDSPayment;
    private DevExpress.XtraReports.UI.DetailBand XDSPaymentDetail;
    private DevExpress.XtraReports.UI.XRTable xrTable49;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow69;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell499;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell500;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell501;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell502;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell503;
    private DevExpress.XtraReports.UI.GroupHeaderBand XDSPaymentHeader;
    private DevExpress.XtraReports.UI.XRTable xrTable48;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow67;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell488;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow68;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell490;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell492;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell494;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell496;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell498;
    private DevExpress.XtraReports.UI.DetailReportBand DetailReport9;
    private DevExpress.XtraReports.UI.DetailBand Detail5;
    private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader5;
    private DevExpress.XtraReports.UI.DetailReportBand DetailReport10;
    private DevExpress.XtraReports.UI.DetailBand Detail6;
    private DevExpress.XtraReports.UI.XRTable xrTable51;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow179;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell515;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell516;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell517;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell518;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell519;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell520;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell521;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell522;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell523;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell524;
    private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader6;
    private DevExpress.XtraReports.UI.XRTable xrTable50;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow173;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell491;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell493;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow174;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell495;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow177;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell497;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow178;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell504;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell505;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell506;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell508;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell509;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell510;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell511;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell512;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell513;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell514;
    private DevExpress.XtraReports.UI.DetailReportBand DetailReport11;
    private DevExpress.XtraReports.UI.DetailBand Detail7;
    private DevExpress.XtraReports.UI.XRTable xrTable21;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow154;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell316;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell317;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell507;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell525;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell526;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell527;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell528;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell529;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell530;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell531;
    private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader7;
    private DevExpress.XtraReports.UI.XRTable xrTable19;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow70;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell289;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell294;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow127;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell295;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow129;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell296;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow130;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell297;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow131;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell298;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell299;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell300;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell301;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell302;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell303;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell304;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell305;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell306;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell315;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell177;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell337;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell207;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell339;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell281;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell341;
    private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader8;
    private DevExpress.XtraReports.UI.XRTable xrTable22;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow49;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell123;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell127;
    private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader11;
    private DevExpress.XtraReports.UI.DetailReportBand DetailReport12;
    private DevExpress.XtraReports.UI.DetailBand Detail8;
    private DevExpress.XtraReports.UI.XRTable xrTable16;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow64;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell137;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell140;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell143;
    private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader17;
    private DevExpress.XtraReports.UI.XRTable xrTable27;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow51;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell128;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow62;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell136;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell139;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow66;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell145;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow65;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell144;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow72;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell178;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell141;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow71;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell148;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell146;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow88;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell214;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow86;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell209;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow87;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell212;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow85;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell210;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow74;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell208;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow89;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell121;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow53;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow96;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell229;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell230;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell231;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell232;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow95;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell225;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell226;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell227;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell228;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow94;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell221;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell222;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell223;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell224;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow91;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell217;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell218;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell219;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell220;
    private DevExpress.XtraReports.UI.XRLine xrLine2;
  }
}
