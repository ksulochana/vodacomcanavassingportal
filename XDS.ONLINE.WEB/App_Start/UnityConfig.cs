using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc5;
using XDS.ONLINE.ENGINE;
using XDS.ONLINE.ENGINE.REPORTGENERATOR;
using XDS.ONLINE.REPOSITORY;

namespace XDS.ONLINE.WEB
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            
            // register all your components with the container here
            // it is NOT necessary to register your controllers
            
            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<IXDSServiceEngine, XDSServiceEngine>();
            container.RegisterType<IXDSRepository, XDSRepository>();
            container.RegisterType<ISHBSRepository, SHBSRepository>();
            container.RegisterType<IXDSReportExporter, XDSReportExporter>();
            container.RegisterType<IXDSAuthRepository, XDSAuthRepository>();
            container.RegisterType<IXDSCustomVettingRepository, CustomVettingRepository>();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}