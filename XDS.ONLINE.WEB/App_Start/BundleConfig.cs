﻿using System.Web;
using System.Web.Optimization;

namespace XDS.ONLINE.WEB
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/expressive.annotations.validate.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/paging").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/expressive.annotations.validate.js",
                        "~/Scripts/jquery-simple-pagination-plugin.js"));

            
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/bootstrap-session-timeout.js"));

            bundles.Add(new ScriptBundle("~/bundles/applicationjs").Include(
                    "~/Scripts/application.js",
                        "~/Scripts/metisMenu.min.js",
                        "~/Scripts/raphael-min.js",
                        "~/Scripts/morris.min.js",
                      "~/Scripts/geoPosition.js",
                      "~/Scripts/modernizr-custom.js",
                      "~/Scripts/chosen.jquery.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                        "~/Content/bootstrap-responsive.min.css",
                        "~/Content/metisMenu.min.css",
                        "~/Content/morris.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/datepicker").Include(
                       "~/Scripts/moment-with-locales.js",
                      "~/Scripts/bootstrap-datetimepicker.js"));


            bundles.Add(new ScriptBundle("~/bundles/linkage").Include(
                       "~/Scripts/linkage.js"));

            bundles.Add(new ScriptBundle("~/bundles/TBbootstrap").Include(
                     "~/Scripts/jquery-1.10.2.js",
                     "~/Scripts/jquery.rateyo.js"));

            bundles.Add(new ScriptBundle("~/Content/TBbootstrapCSS").Include(
                      "~/Content/jquery.rateyo.css"));
        }
    }
}
