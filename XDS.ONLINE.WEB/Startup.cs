﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(XDS.ONLINE.WEB.Startup))]
namespace XDS.ONLINE.WEB
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
