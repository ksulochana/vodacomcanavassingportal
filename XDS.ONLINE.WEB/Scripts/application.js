var BonusXmlUrl = '';
var ReportUrl = '';
var contextUrl = ''; 


$(function () {

  

    $('#side-menu').metisMenu();

    $.sessionTimeout({
        keepAliveUrl: contextUrl + 'Home/KeepAlive',
        logoutUrl: contextUrl + 'Account/LogOff',
        redirUrl: contextUrl + 'Account/LogOff',
        warnAfter: 1800000, // 900000 = 15 minutes, 3000 = 3 seconds
        redirAfter:1920000, // 40 minutes
        countdownMessage: 'Redirecting in {timer} seconds.',
        ignoreUserActivity: true,
        keepAlive: false
    }); 
});
  
//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function () {
    $(window).bind("load resize", function () {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    var element = $('ul.nav a').filter(function () {
        return this.href == url;
    }).addClass('active').parent().parent().addClass('in').parent();
    if (element.is('li')) {
        element.addClass('active');
    }
});

function InitializeCustomVettingReport()
{
    $('#reportsection').hide();

   // $('#searchbutton').on("click", function () {
   // CustomVettingMatch();
     //   return false;
   // });

    //$("#clearbutton").on("click", function () {
    //    $('.form-control').val('');
    //    $('#ConsumerResults').html(''); 
    //});
}


function CustomVettingClear() {
    $('#DealerValue').val('');
    $('#PromotionValue').val('');
    $('#PromoCodeValue').val('');
    $('#IdNumber').val('');
    $('#Surname').val('');
    $('#FirstName').val('');
    $('#DateOfBirth').val('');
    $('#Income').val('');
    $('#MobileNo').val('');
    $('#AlternateMobileNo').val('');
    $('#Email').val('');
}

function CustomVettingMatch() { 

    var form = $("#searchform");

    $.validator.unobtrusive.parse(form);
    form.validate();

    if (!form.valid()) { return false; }

    $.ajax({
        type: "POST",
        url: form.attr('action'),
        data: form.serialize(),
        dataType: "html"
    })
        .success(function (data) {  
             
            $('#searchsection').html(data);
            $('#searchsection').show();

            if (data.indexOf('Reference') != -1) {
                $('#reportsection').show();
            }
            else {
                $('#reportsection').val();
                $('#reportsection').hide();
            }
        })
        .error(function () {
            unexpectedError();
            ShowSideBar();
        });
}

function DownloadBulletin(bulletinID)
{
    var form = $("#reportform"); 
    $("#BulletinID").val(bulletinID);
     
    if ($("#BulletinID").val() != '') {        
        form.target = '_report';
        form.submit();
    }
}

function GetPromotionDealByPortfolio(portfolio) {

    if (portfolio.indexOf('Apply') != -1)
        return;

    var form = $("#portfolioform");
    $("#Portfolio").val(portfolio);

    form.target = '_report';
    form.submit();
}

function GetPromotionDeal(promotionDealID) {
    var form = $("#reportform");
    $("#PromotionDealID").val(promotionDealID);

    if ($("#PromotionDealID").val() != '') {

        //Get Deals
        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: form.serialize(),
            dataType: "html"
        })
         .success(function (data) {
             if (data.length > 0) {
                 $("#gPromotionDealBody").html(data); 

                 $("#gPromotionDeal").modal({
                     "show": true,
                     // "backdrop": "static",
                     "keyboard": false
                 });
             }
             else {
                 alert("Error retrieving Deal");
             }
         })
         .error(function () {
             unexpectedError();
             ShowSideBar();
         });
    }
}
  
$(document).ajaxStart(function () {
    $("#gloading").modal({
        "show": true,
        "backdrop": "static",
        "keyboard": false
    });
});

$(document).ajaxStop(function () {
    $("#gloading").modal('hide');
});
  
function exportReport(format) {
    //return false;
    //todo:
    var form = $("#reportform"); 
    if ($("#EnquiryID").val() != '') {
        $("#Format").val(format);
        form.target = '_report';
        form.submit();
    }
}

function exportReportAUTHEN(format) {
    //return false;
    //todo:  
    var form = $("#reportformAUTHEN");
    $("#Format1").val(format);
    $("#EnquiryResultID1").val($("#EnquiryResultID").val());
    if ($("#EnquiryID").val() != '') {

        form.target = '_report';
        form.submit();
    }
}
  
function fetchReport() {
    var form = $("#reportform");
    $.ajax({
        type: "POST",
        url: ReportUrl,
        data: form.serialize(),
        dataType: "html"
    })
                      .success(function (data) {
                          $('#reportdetail').html(data);
                          $('#Viewed_' + $("#EnquiryResultID").val()).text('Yes');
                          $('#searchsection').hide();
                          $('#reportsection').show();

                          $('#tabAll').click(function () {
                              $('#tabAll').addClass('active');
                              $('.tab-pane').each(function (i, t) {
                                  $('#myTabs li').removeClass('active');
                                  $(this).addClass('active');
                              });
                          });

                      })
             .error(function () {
                 unexpectedError();
             });

    return false;
}
 
function InitializeReportSearchWithBusinessBonus() {

 

    $('#reportsection').hide();
    $('#searchbutton').on("click", function () {

        var form = $("#searchform");

        $.validator.unobtrusive.parse(form);
        form.validate();
        if (!form.valid()) { return false; }

        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: form.serialize(),
            dataType: "html"
        })
            .success(function (data) {
                $('#ConsumerResults').html(data);
                $('#ConsumerResults').show();
                $(".clickableRow").click(function () {
                    $("#ConsumerID").val($(this).attr("ConsumerID"));
                    $("#EnquiryID").val($(this).attr("EnquiryID"));
                    $("#EnquiryResultID").val($(this).attr("EnquiryResultID"));
                    if (BonusXmlUrl == '')
                    { fetchBusinessReport(); }
                    else
                    {
                        fetchBusinessBonusSegments();
                    }
                    HideSideBar();
                });
            })
            .error(function () {
                unexpectedError();
                ShowSideBar();
            });
        return false;
    });

    $("#clearbutton").on("click", function () {
        $('.form-control').val('');
        $('#ConsumerResults').html('');
    });
}
 
function InitializeHistoryReportSearch() { 
    $('#reportsection').hide();

    $('#searchbutton').on("click", function () {

        var form = $("#searchform");

        $.validator.unobtrusive.parse(form);
        form.validate();
        if (!form.valid()) { return false; }
        
        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: form.serialize(),
            dataType: "html"
        })
            .success(function (data) {

                $('#ConsumerResults').html(data);
                $('#ConsumerResults').show();
                $(".clickableRow").click(function () {

                    ReportUrl = "../CustomVetting/CustomVettingHistoryReport"

                    $("#EnquiryID").val($(this).attr("EnquiryID"));

                    fetchCustomVettingReport();

                    HideSideBar();
                });
            })
            .error(function () {
                unexpectedError();
                ShowSideBar();
            });
        return false;
    });

    $("#clearbutton").on("click", function () {
        $('.form-control').val('');
        $('#ConsumerResults').html('');
    });

    $("#clearbutton").on("click", function () {
        $('.form-control').val(''); 
    });

    DoSearch();
}

function DoSearch() {

    var form = $("#searchform");
    $.validator.unobtrusive.parse(form);
    form.validate();
    if (!form.valid()) { return false; }

    $.ajax({
        type: "POST",
        url: form.attr('action'),
        data: form.serialize(),
        dataType: "html"
    })
        .success(function (data) {
            $('#ConsumerResults').html(data);
            $('#ConsumerResults').show();
            $(".clickableRow").click(function () {

                ReportUrl = "../CustomVetting/CustomVettingHistoryReport"

                $("#EnquiryID").val($(this).attr("EnquiryID"));

                fetchCustomVettingReport();

               // HideSideBar();
            });
        })
        .error(function () {
            unexpectedError();
            ShowSideBar();
        });
    return false;
}
 
function fetchCustomVettingReport() {

    var form = $("#reportform");
    $.ajax({
        type: "POST",
        url: ReportUrl,
        data: form.serialize(),
        dataType: "html"
    })
                      .success(function (data) {
                          $('#reportdetail').html(data);
                          $('#searchsection').hide();
                          $('#reportsection').show();
                      })
             .error(function () {
                 unexpectedError();
             });

    return false;
}
 
function generalPopup(title, message) {
    if (typeof title === 'undefined') { title = 'Message from site'; }
    if (typeof message === 'undefined') { message = ''; }
    
    var content = '<div class="modal-dialog"> \
            <div class="modal-content"> \
                <div class="modal-header"> \
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> \
                <h4 class="modal-title">' + title + '</h4> \
                </div> \
                <div class="modal-body"> \
                <p>' + message + '</p> \
                </div> \
                <div class="modal-footer"> \
                <button id="session-timeout-dialog-keepalive" type="button" class="btn btn-primary" data-dismiss="modal">Close</button> \
                </div> \
            </div> \
          </div>';

    if ($("#general-dialog").length > 0) {
        // update error warning content 
        $('#general-dialog').html(content);
    }
    else {
        // Create error warning dialog
        $('body').append('<div class="modal fade" id="general-dialog">' + content + '</div>');
    }
    $('#general-dialog').modal('show');
}

function unexpectedError() {
    generalPopup('<i class="glyphicon glyphicon-exclamation-sign"></i> Warning', 'The request did not complete successfully. Please try again or contact us if the problem persists.');
}

$('#toggleSidebar').click(function () {
   
    HideShowSideBar(); 
    //$('.navbar-collapse').collapse('toggle');
});

function HideShowSideBar()  {  // used when user clicks glyphicon-chevron
    if ($('#toggleSidebar i').hasClass('glyphicon-chevron-right')) {
        ShowSideBar(); 
    } else {
        HideSideBar();
    }
}

function HideSideBar()  {  // used to hide only. eg. when user click a report
    $('#toggleSidebar i').removeClass('glyphicon-chevron-left');
    $('#toggleSidebar i').addClass('glyphicon-chevron-right');
    $('#sidebar-main').addClass('collapse');
    $('#page-wrapper').addClass('main-no-sidebar');
    $('#toggleSidebar').addClass('toggleSidebarClosed');
    $('#btnHideShowSideBar').val("Show Menu");
    $('#btnHideShowSideBar').text("Show Menu");
    $('#btnHideShowSideBar2').val("Show Menu");
    $('#btnHideShowSideBar2').text("Show Menu");
}

function ShowSideBar() {    // used to Show only. eg. when user click back
    $('#toggleSidebar i').addClass('glyphicon-chevron-left');
    $('#toggleSidebar i').removeClass('glyphicon-chevron-right');
    $('#sidebar-main').removeClass('collapse');
    $('#page-wrapper').removeClass('main-no-sidebar');
    $('#toggleSidebar').removeClass('toggleSidebarClosed');
    $('#btnHideShowSideBar').val("Hide Menu");
    $('#btnHideShowSideBar').text("Hide Menu");
    $('#btnHideShowSideBar2').val("Hide Menu");
    $('#btnHideShowSideBar2').text("Hide Menu");

}

function closeReport() {
    $('#reportsection').hide(); 
    $('#searchsection').show();

    $('.form-control').val('');
    $('#ConsumerResults').html('');

    $('#searchinput').show();

    ShowSideBar();
}
 

$('#toggle-btn').click(function () {
        $('#sidebar-main').removeClass('collapse');
        $('#page-wrapper').removeClass('main-no-sidebar');
});

function DownloadTrainingDocument(trainingID) {
    var form = $("#reportform");
    $("#TrainingID").val(trainingID);

    if ($("#TrainingID").val() !== '') {
        $.ajax({
            type: "POST",
            url: '../VideoTraining/DownloadTrainingDoc',
            data: form.serialize(),
            datatype: "html"
        })
            .success(function (data) {
                $("#gTrainingDocPopupBody").html(data);
                $("#gTrainingDocPopup").modal({
                    "show": true,
                    "backdrop": "static",
                    "keyboard": false
                });
            })
            .error(function () {
                unexpectedError();
                ShowSideBar();
            });

        $('#TrainingDocclosebutton').on("click", function () {
            
            var form1 = $("#reportform");

            var data = $("#Videoabsolutepath").val();
            $.ajax({
                type: "POST",
                url: '../VideoTraining/ClearMedia',
                data: form1.serialize(),
                datatype: "html"
            })
                .success(function (data) {
                    $("#gTrainingDocPopup").modal('hide');
                })
                .error(function () {
                    unexpectedError();
                    ShowSideBar();
                });
        });
    }
}
