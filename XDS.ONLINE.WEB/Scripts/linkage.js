﻿$(function () {

        $(".idnolink").each(function () {
            $(this).click(function () {

                var idno = $(this).text();
                $('#selectedVal').val(idno);

                //jquery call 
                $.ajax({
                    type: "POST",
                    url: "../TraceEnquiry/GetIDLinkage",
                    data: "",
                dataType: "html"
            })
                     .success(function (data) {
                         $("#modalLinkDataSubmitBody").html(data);
                         if ($("#myLinksModal").length > 0) {
                             $("#myLinksModal").modal({
                                 "show": true,
                                 "backdrop": "static",
                                 "keyboard": false
                             });
                         }
                         else {
                             alert("Error retrieving Linkage List");
                         }

                     })
            .error(function () {
                alert("Error retrieving Linkage List");
            });

            return false;


            //$("#myLinksModal").modal({
            //    "show": true,
            //    "backdrop": "static",
            //    "keyboard": false
            //});

        });
    });

        $(".passportlink").each(function () {
            $(this).click(function () {

                var passportNo = $(this).text();
                $('#selectedVal').val(passportNo);

                //jquery call 
                $.ajax({
                    type: "POST",
                    url: "../TraceEnquiry/GetPassportLinkage",
                    data: "",
                    dataType: "html"
                })
                     .success(function (data) {
                         $("#modalLinkDataSubmitBody").html(data);
                         if ($("#myLinksModal").length > 0) {
                             $("#myLinksModal").modal({
                                 "show": true,
                                 "backdrop": "static",
                                 "keyboard": false
                             });
                         }
                         else {
                             alert("Error retrieving Linkage List");
                         }

                     })
            .error(function () {
                alert("Error retrieving Linkage List");
            });

                return false;


                //$("#myLinksModal").modal({
                //    "show": true,
                //    "backdrop": "static",
                //    "keyboard": false
                //});

            });
        });

        $(".companylink").each(function () {
            $(this).click(function () {

                var companyName = $(this).text();
                $('#selectedVal').val(companyName);

                //jquery call 
                $.ajax({
                    type: "POST",
                    url: "../TraceEnquiry/GetCompanyLinkage",
                    data: "",
                    dataType: "html"
                })
                     .success(function (data) {
                         $("#modalLinkDataSubmitBody").html(data);
                         if ($("#myLinksModal").length > 0) {
                             $("#myLinksModal").modal({
                                 "show": true,
                                 "backdrop": "static",
                                 "keyboard": false
                             });
                         }
                         else {
                             alert("Error retrieving Linkage List");
                         }

                     })
            .error(function () {
                alert("Error retrieving Linkage List");
            });

                return false;


                //$("#myLinksModal").modal({
                //    "show": true,
                //    "backdrop": "static",
                //    "keyboard": false
                //});

            });
        });

        $(".idnoprincipallink").each(function () {
            $(this).click(function () {

                var passportNo = $(this).text();
                $('#selectedVal').val(passportNo);

                //jquery call 
                $.ajax({
                    type: "POST",
                    url: "../TraceEnquiry/GetPrincipalIDLinkage",
                    data: "",
                    dataType: "html"
                })
                     .success(function (data) {
                         $("#modalLinkDataSubmitBody").html(data);
                         if ($("#myLinksModal").length > 0) {
                             $("#myLinksModal").modal({
                                 "show": true,
                                 "backdrop": "static",
                                 "keyboard": false
                             });
                         }
                         else {
                             alert("Error retrieving Linkage List");
                         }

                     })
            .error(function () {
                alert("Error retrieving Linkage List");
            });

                return false;

            });
        });

});
