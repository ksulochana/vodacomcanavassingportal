﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XDS.ONLINE.ENGINE;
using XDS.ONLINE.MODELS;
using System.Threading.Tasks;
using System.Web.Routing;
using System.Xml.Linq;
using System.Xml;

namespace XDS.ONLINE.WEB.Controllers
{
    public class PromotionDealController : BaseXDSEngineController
    {

        public PromotionDealController()
        {
        }

        public PromotionDealController(IXDSServiceEngine serviceEngine)
        {
            _serviceEngine = serviceEngine;
        }

        //
        // GET: /CustomVetting/
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> PromotionDealMatch()
        {
            ViewBag.Message = "";
            ViewBag.ProductID = "110";

            ListofPromotionDeals em = new ListofPromotionDeals();

            em = await _serviceEngine.GetPromotionDealsAsync(Session["AuthToken"].ToString(), 0);

            return View(em);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PromotionDealMatch(string Portfolio)
        {
            ListofPromotionDeals em = null;

            em = _serviceEngine.GetPromotionDealsByPortfolio(Session["AuthToken"].ToString(), Portfolio);

            return View(em);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPromotionDeal(long PromotionDealID)
        {
            ListofPromotionDeals em = null;
            ListofPortfolios emPortfolio = null;

            PromotionDealItem oPromotionDealItem = null;

            em = _serviceEngine.GetPromotionDeals(Session["AuthToken"].ToString(), PromotionDealID);
            emPortfolio = _serviceEngine.GetPortfoliosByPromotionDeal(Session["AuthToken"].ToString(), PromotionDealID);

            if (em != null && em.PromotionDealList.Count() > 0)
            {
                oPromotionDealItem = em.PromotionDealList[0];

                oPromotionDealItem.PromotionDealPortfolios = emPortfolio;

                if (oPromotionDealItem.Doc != null)
                    oPromotionDealItem.Image = string.Format("data:{0};base64,{1}", oPromotionDealItem.DocContentType, Convert.ToBase64String(oPromotionDealItem.Doc));
            }

            return PartialView("_PromotionDealPartial", oPromotionDealItem);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPortfoliosByPromotionDeal(long PromotionDealID)
        {
            ListofPortfolios em = null;

            em = _serviceEngine.GetPortfoliosByPromotionDeal(Session["AuthToken"].ToString(), PromotionDealID);

            return PartialView("_PortfolioPartial", em);
        }
    }
}