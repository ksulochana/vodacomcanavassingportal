﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XDS.ONLINE.ENGINE;
using XDS.ONLINE.MODELS;
using System.Threading.Tasks;
using System.Web.Routing;
using System.Xml.Linq;
using System.Xml;
using System.Globalization;

namespace XDS.ONLINE.WEB.Controllers
{
    public class CustomVettingController : BaseXDSEngineController
    {

        public CustomVettingController()
        {
        }

        public CustomVettingController(IXDSServiceEngine serviceEngine)
        {
            _serviceEngine = serviceEngine;
        }

        //
        // GET: /CustomVetting/
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> CustomVettingMatch()
        {
            ViewBag.Message = "Your create enquiry page.";
            ViewBag.ProductID = "110";

            string transactionDetails = _serviceEngine.CustomVettingGetTransactionDetails(Session["AuthToken"].ToString());

            CustomVettingMatchViewModel em = new CustomVettingMatchViewModel(transactionDetails);
            ViewData["PromotionDealList"] = em.PromotionDealList;
            return View(em);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CustomVettingMatch(CustomVettingMatchViewModel model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Message = "Your create enquiry page.";
                ViewBag.ProductID = "110";

                string transactionDetails = _serviceEngine.CustomVettingGetTransactionDetails(Session["AuthToken"].ToString());
                CustomVettingMatchViewModel em = new CustomVettingMatchViewModel(transactionDetails);

                model.DealerList = em.DealerList;
                model.PromotionDealList = em.PromotionDealList;
                model.PromoCodeList = em.PromoCodeList;
                model.ProductID = 110;
                // return View(model);
                return PartialViewHelper("CustomVettingMatch", model);
            }

            string transactionDetailstemp = _serviceEngine.CustomVettingGetTransactionDetails(Session["AuthToken"].ToString());
            string promotionDealValue = string.Empty;

            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.LoadXml(transactionDetailstemp);
            System.Xml.XmlNodeList nodeList = doc.SelectNodes("//TransactionDetails/Dealer");
            System.Xml.XmlNodeList nodeList2 = doc.SelectNodes("//TransactionDetails/PromotionDeal");
            System.Xml.XmlNodeList nodeList3 = doc.SelectNodes("//TransactionDetails/PromoCode");

            foreach (System.Xml.XmlNode node2 in nodeList2)
                if (node2["PromotionDealID"].InnerText == model.PromotionValue)
                {
                    promotionDealValue = node2["DealSheetNo"].InnerText.ToString();
                }

            


            string strUsername = Session["UserName"] != null ? Session["UserName"].ToString() : string.Empty;

            CustomVetting rpt = null;
            ListofHistory listOfHistory = null;

            listOfHistory = await _serviceEngine.CustomVettingGetTransactionHistoryAsync(Session["AuthToken"].ToString(), Session["UserName"].ToString(), model.IdNumber == null ? "" : model.IdNumber,
                  "", "", DateTime.Now.AddDays(-30), DateTime.Now, true);

            if (listOfHistory != null && listOfHistory.SearchHistory.Count() > 0)
                return PartialViewHelper("_CustomVettingReportPartial", "Customer has been submitted within the last 30 days");
            else
            {
                rpt = await _serviceEngine.ConnectCustomVettingNAsync(Session["XDSWSAuthToken"].ToString(), model.ProductID, model.IdNumber, model.FirstName, model.Surname, model.DateOfBirth, model.Income, model.MobileNo,
                    model.AlternateMobileNo, model.Email, model.DealerValue, model.PromotionValue, model.PromoCodeValue, strUsername, DateTime.Now,promotionDealValue);

                Session["ReportXML"] = _serviceEngine.ReportXML();

                if (rpt != null && rpt.CustomVettingResult.Count() > 0)
                {
                    CustomVettingResult oCustomVettingResult = rpt.CustomVettingResult[0];

                    #region[ Log Transaction ] 
                    string strResult = string.Empty;

                    if (string.IsNullOrEmpty(model.City))
                    {
                        model.City = string.Empty;
                    }

                    float Latitude = 0;
                    float Longitude = 0;

                    float.TryParse(model.Latitude,NumberStyles.Float, CultureInfo.InvariantCulture.NumberFormat,out Latitude);
                    float.TryParse(model.Longitude, NumberStyles.Float, CultureInfo.InvariantCulture.NumberFormat, out Longitude);


                    strResult = await _serviceEngine.LogTransactionAsync(Session["AuthToken"].ToString(),
                        oCustomVettingResult.ApplicationPromotionDeal, oCustomVettingResult.ApplicationDealerID, oCustomVettingResult.PromoCode, oCustomVettingResult.FirstName,
                        oCustomVettingResult.Surname, General.DisplayDateWithTime(oCustomVettingResult.BirthDate),
                        oCustomVettingResult.IDNo, oCustomVettingResult.Income, oCustomVettingResult.CellularNo,
                        oCustomVettingResult.AlternateContactNo, oCustomVettingResult.EmailAddress, oCustomVettingResult.ReferenceNo, oCustomVettingResult.Score,
                        oCustomVettingResult.ReasonCode, oCustomVettingResult.ApplicationStatus, oCustomVettingResult.Portfolio,
                        General.DisplayDateWithTime(oCustomVettingResult.CreatedOnDate),
                        oCustomVettingResult.ReasonDescription, oCustomVettingResult.CapturedBy, oCustomVettingResult.ConsumerID.ToString(),
                        oCustomVettingResult.PredictedIncome, oCustomVettingResult.PredictedAvailableInstalment,
                        oCustomVettingResult.AccountNo, oCustomVettingResult.SourceInd, oCustomVettingResult.VodacomResponse, oCustomVettingResult.VodacomResponseType, oCustomVettingResult.EmailStatus, Latitude, Longitude, model.City);
                    #endregion

                    #region[ Send SMS ]
                    if (!oCustomVettingResult.ApplicationStatus.ToLower().Trim().StartsWith("apply") && !oCustomVettingResult.Portfolio.ToLower().Trim().StartsWith("apply"))
                    {
                        ListofSMSConfig ListofSMSConfig = null;
                        SMSResult oSMSResult = null;

                        string strSMSMessage = string.Empty;
                        int EnquiryID = 0;

                        ListofSMSConfig = await _serviceEngine.GetSMSConfigAsync(Session["AuthToken"].ToString());

                        if (ListofSMSConfig != null && ListofSMSConfig.SMSConfigList.Count() > 0)
                        {
                            strSMSMessage = ListofSMSConfig.SMSConfigList[0].SMSMessage.Replace("##RefNo##", oCustomVettingResult.IDNo);
                            int.TryParse(oCustomVettingResult.EnquiryID.ToString(), out EnquiryID);

                            oSMSResult = await _serviceEngine.SMSAlertAsync(Session["XDSWSAuthToken"].ToString(),
                                !string.IsNullOrEmpty(model.MobileNo) ? model.MobileNo : model.AlternateMobileNo, strSMSMessage, Session["SMSAlertType"].ToString(), EnquiryID, int.Parse(Session["SMSProductID"].ToString()));
                        }
                    }
                    #endregion
                }
            }

            return PartialViewHelper("_CustomVettingReportPartial", rpt);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CustomVettingHistoryReport(HistoryMatchViewModel model)
        {
            //ViewBag.Message = "Your create enquiry page.";
            if (ModelState.IsValid)
            {
                CustomVetting rpt = null;

                rpt = await _serviceEngine.CustomVettingGetTransactionEnquiryAsync(Session["AuthToken"].ToString(), model.EnquiryID);

                Session["ReportXML"] = _serviceEngine.ReportXML();

                return PartialViewHelper("_CustomVettingReportPartial", rpt);

            }
            return null;
        }
    }
}