﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using XDS.ONLINE.WEB.Models;

namespace XDS.ONLINE.WEB.Controllers
{
    public class ErrorController : BaseController
    {
        // GET: Error
        public ActionResult Index(ErrorViewModel errorViewModel)
        {
            return View(errorViewModel);
        }

        // GET: Error
        public ActionResult AjaxIndex(ErrorViewModel errorViewModel)
        {
            return View(errorViewModel);
        }
    }
}