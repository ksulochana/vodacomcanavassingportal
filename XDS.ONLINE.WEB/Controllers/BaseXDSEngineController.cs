﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XDS.ONLINE.ENGINE;
using XDS.ONLINE.MODELS;
using System.Threading.Tasks;
using System.Web.Routing;
using LogRouter;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace XDS.ONLINE.WEB.Controllers
{
    public class BaseXDSEngineController : BaseController
    {
        protected IXDSServiceEngine _serviceEngine;

        public BaseXDSEngineController()
        { }


        protected override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            // call the base method first
            base.OnActionExecuting(actionContext);
            if (Session["AuthToken"] == null)
            {
                actionContext.Result = RedirectToAction("Login", "Account");
                return;
            }
            else
            {
                // TODO: log user action
                // application : actionContext.ActionDescriptor.ControllerDescriptor.ControllerType.Assembly.ManifestModule.Name
                // controller : actionContext.ActionDescriptor.ControllerDescriptor.ControllerName 
                // action : actionContext.ActionDescriptor.ActionName
                // httpmethod : actionContext.HttpContext.Request.HttpMethod.ToUpper()
                // user : Session["LoginName"]
                // Login Audit ID : Session["LoginAuditID"]


                string sControllerName = actionContext.RouteData.Values["controller"].ToString();
                string sActionName = actionContext.RouteData.Values["action"].ToString();
                string sMethod = "";
                Int64 LoginAuditID = 0;

                sMethod = actionContext.HttpContext.Request.HttpMethod;
                if (actionContext.RequestContext.HttpContext.Session["LoginAuditID"] != null)
                {
                    LoginAuditID = (Int64)actionContext.RequestContext.HttpContext.Session["LoginAuditID"];
                }

                if (System.Configuration.ConfigurationManager.AppSettings["ActivityLoggingInd"] == "1")
                {
                    //Logging ErrLog = new Logging(Logging.PreferredLog.WindowsEventLog);
                    Logging ErrLog = new Logging();
                    ActivityInfo oActivityInfo = new ActivityInfo();
                    oActivityInfo.LoginId = System.Web.HttpContext.Current.Session["UserName"].ToString();
                    oActivityInfo.Action = sActionName;
                    oActivityInfo.Application = "XDS";
                    oActivityInfo.LoginSource = "XDSONLINE";
                    oActivityInfo.Classification = "ACTIVITY";
                    oActivityInfo.Message = "Activity detected";
                    oActivityInfo.Method = sControllerName + "/" + sMethod; ;
                    oActivityInfo.Parameters = "ABC";
                    oActivityInfo.Process = "80";

                    ErrLog.LogActivity(oActivityInfo);
                }

                //Task<LogOutcome> LogError = ErrLog.LogError(oErrorInfo);
                //LogOutcome oLogOutcome = await LogError;


                if (actionContext.HttpContext.Request.HttpMethod.ToUpper() == "POST")
                {
                    if (!_serviceEngine.CustomVettingIsTicketValid(Session["AuthToken"].ToString()))
                    {
                        // TODO: log off with reason invalid token

                        actionContext.Result = RedirectToAction("Login", "Account");
                        return;
                    }
                }
            }
        }

        public PartialViewResult PartialViewHelper(string ViewName, object model)
        {
            if (model == null)
            {
                if (_serviceEngine.HasError())
                {

                    XDS.ONLINE.WEB.Models.ErrorViewModel errormodel = new XDS.ONLINE.WEB.Models.ErrorViewModel();
                    errormodel.Message = _serviceEngine.ErrorMessage();

                    ViewName = "~/Views/Error/Index.cshtml";
                    model = errormodel;
                }
            }
            else
            {

                if (Request.IsAjaxRequest())
                {
                    //something specific for ajax?
                }
            }

            return PartialView(ViewName, model);

        }

        public PartialViewResult PartialViewHelper(string ViewName, string errorMessage)
        {

            XDS.ONLINE.WEB.Models.ErrorViewModel errormodel = new XDS.ONLINE.WEB.Models.ErrorViewModel();
            errormodel.Message = errorMessage;

            ViewName = "~/Views/Error/Index.cshtml"; 

            return PartialView(ViewName, errormodel);
        }

        public String EncryptString(string text, bool encode = true)
        {
            string encrypted = Encrypt(text, "uj9HBiDgFCroksjW");           
                return HttpUtility.UrlEncode(encrypted, System.Text.Encoding.UTF8);
        }

        public string Encrypt(string plainText, string passPhrase)
        {
            
            int keysize = 256;
            byte[] initVectorBytes = Encoding.ASCII.GetBytes("plK0VV4WCaA8VhZi");
            string defaultPassPhrase = "uj9HBiDgFCroksjW";
            if (String.IsNullOrEmpty(passPhrase)) { passPhrase = defaultPassPhrase; }
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            using (PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null))
            {
                byte[] keyBytes = password.GetBytes(keysize / 8);
                using (RijndaelManaged symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.Mode = CipherMode.CBC;
                    using (ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes))
                    {
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            using (CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                            {
                                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                                cryptoStream.FlushFinalBlock();
                                byte[] cipherTextBytes = memoryStream.ToArray();

                                //if (String.IsNullOrEmpty(plainText)) { plainText = String.Format("AccountId={0};QuestionnaireId={1};SupplierId={2}", 1, 1, 1); }
                                var EncodedData = Convert.ToBase64String(cipherTextBytes);
                                return Base64UrlEncode(EncodedData);
                            }
                        }
                    }
                }
            }
        }

        public string Base64UrlEncode(string value)
        {
            var bytes = Encoding.UTF8.GetBytes(value);
            var s = Convert.ToBase64String(bytes); // Regular base64 encoder
            s = s.Split('=')[0]; // Remove any trailing '='s
            s = s.Replace('+', '-'); // 62nd char of encoding
            s = s.Replace('/', '_'); // 63rd char of encoding
            return s;
        }

        public byte[] Base64UrlDecode(string arg)
        {
            string s = arg;
            s = s.Replace('-', '+'); // 62nd char of encoding
            s = s.Replace('_', '/'); // 63rd char of encoding
            switch (s.Length % 4) // Pad with trailing '='s
            {
                case 0: break; // No pad chars in this case
                case 2: s += "=="; break; // Two pad chars
                case 3: s += "="; break; // One pad char
                default:
                    throw new System.Exception(
             "Illegal base64url string!");
            }
            return Convert.FromBase64String(s); // Standard base64 decoder
        }

        //public string Base64UrlDecode(string str)
        //{
        //    //            byte[] decbuff = HttpServerUtility.UrlTokenDecode(str);
        //    byte[] decbuff = Convert.FromBase64String(str);
        //    return Encoding.UTF8.GetString(decbuff);
        //}

        public String DecryptString( String encrypted, bool decode = true)
        {
            String passphrase = "uj9HBiDgFCroksjW";


                string decodedString = HttpUtility.UrlDecode(encrypted);
                return Decrypt(decodedString, passphrase);
           
        }

        public string Decrypt(string cipherText, string passPhrase)
        {
            int keysize = 256;
            var decodedBytes = Base64UrlDecode(cipherText);
            cipherText =  Encoding.ASCII.GetString(decodedBytes);
            byte[] initVectorBytes = Encoding.ASCII.GetBytes("plK0VV4WCaA8VhZi");
            string defaultPassPhrase = "uj9HBiDgFCroksjW";
            if (String.IsNullOrEmpty(passPhrase)) { passPhrase = defaultPassPhrase; }
            //Ensure the encrypted string has the correct padding at the end as it might of been removed for transportation of a URL
            //Determine how many characters to pad (Base64 needs to have a length of a multiple of 4)
            int remainderValue = 0;
            //If the length isn't already a multiple of 4, determine the number of characters needed to make it so
            if (0 != (cipherText.Length % 4))
                remainderValue = 4 - (cipherText.Length % 4);
            //Make sure the string is at least 8 characters)
            cipherText = cipherText.PadRight(cipherText.Length + remainderValue, '=');
            byte[] cipherTextBytes = Convert.FromBase64String(cipherText);
            using (PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null))
            {
                byte[] keyBytes = password.GetBytes(keysize / 8);
                using (RijndaelManaged symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.Mode = CipherMode.CBC;
                    using (ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes))
                    {
                        using (MemoryStream memoryStream = new MemoryStream(cipherTextBytes))
                        {
                            using (CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                            {
                                byte[] plainTextBytes = new byte[cipherTextBytes.Length];
                                int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                                return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                            }
                        }
                    }
                }
            }
        }

    }
}