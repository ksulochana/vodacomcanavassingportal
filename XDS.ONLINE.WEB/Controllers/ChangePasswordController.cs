﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XDS.ONLINE.ENGINE;
using XDS.ONLINE.MODELS;
//using XDS.ONLINE.MODELS.Report;
using System.Threading.Tasks;
using System.Web.Routing;
using System.Web.Script.Serialization;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace XDS.ONLINE.WEB.Controllers
{
    public class ChangePasswordController : BaseXDSEngineController
    {
         public ChangePasswordController()
        { }

         public ChangePasswordController(IXDSServiceEngine serviceEngine)
        {
            _serviceEngine = serviceEngine;
        }

         [HttpGet]
         //[ValidateAntiForgeryToken]
         public ActionResult ChangePassword()
         {
             ChangePasswordViewModel cp = new ChangePasswordViewModel();

             return View(cp);
         }

         [HttpPost]
         //[ValidateAntiForgeryToken]
         public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
         {
             if (ModelState.IsValid)
             {

                 General general = new General();
                 string CurrentPassword = model.CurrentPassword;
                 string NewPassword = model.NewPassword;
                 string ConfirmPassword = model.ConfirmPassword;
                 try
                 {
                     model.Result = GetResult(await _serviceEngine.AdminChangePasswordAsync(Session["AuthToken"].ToString(), CurrentPassword, NewPassword, ConfirmPassword));
                     //reset values
                 }
                 catch (Exception)
                 {
                     throw;
                 }
                 finally
                 {
                     model.CurrentPassword = "";
                     model.NewPassword = "";
                     model.ConfirmPassword = "";
                 }
                 return View(model);
             }
             return null;
         }

         private String GetResult(string XMLString)
         {
             XmlDocument xml = new XmlDocument();
             xml.LoadXml(XMLString);
             if (XMLString.IndexOf("NoResult") >= 0)
             {

                 return xml.DocumentElement.FirstChild.InnerText; 
             }
             else
             {
                 return xml.DocumentElement.ParentNode.InnerText; 
             }
         }


         [HttpGet]
         //[ValidateAntiForgeryToken]
         public ActionResult ChangePasswordPartial()
         {
             Session["DonotRenderchangepassword"] = "1";
             ChangePasswordViewModel cp = new ChangePasswordViewModel();

             return PartialView("~//Views//ChangePassword/_ChangePassword.cshtml", cp);
         }

         //[HttpPost]
         ////[ValidateAntiForgeryToken]
         //public async Task<ActionResult> ChangePasswordPartial(ChangePasswordViewModel model)
         //{
         //    if (ModelState.IsValid)
         //    {

         //        General general = new General();
         //        string CurrentPassword = model.CurrentPassword;
         //        string NewPassword = model.NewPassword;
         //        string ConfirmPassword = model.ConfirmPassword;
         //        try
         //        {
         //            model.Result = GetResult(await _serviceEngine.AdminChangePasswordFirstLoginAsync(Session["AuthToken"].ToString(), CurrentPassword, NewPassword, ConfirmPassword));
         //            XDS.ONLINE.MODELS.SystemUser usr = await _serviceEngine.AdminSystemUserPasswordResetAsync(Session["AuthToken"].ToString());



         //            Session["PasswordResetStatus"] = usr.SystemUser1[0].PasswordResetStatus.ToString();
         //            //reset values
         //        }
         //        catch (Exception)
         //        {
         //            throw;
         //        }
         //        finally
         //        {
         //            model.CurrentPassword = "";
         //            model.NewPassword = "";
         //            model.ConfirmPassword = "";
         //        }
         //        return Content(model.Result);
         //    }
         //    return null;
         //}
    } 
}