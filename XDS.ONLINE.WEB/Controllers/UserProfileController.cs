﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XDS.ONLINE.ENGINE;
using XDS.ONLINE.MODELS;
//using XDS.ONLINE.MODELS.Report;
using System.Threading.Tasks;
using System.Web.Routing;
using System.Web.Script.Serialization;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace XDS.ONLINE.WEB.Controllers
{
    public class UserProfileController : BaseXDSEngineController
    {
         public UserProfileController()
        { }

         public UserProfileController(IXDSServiceEngine serviceEngine)
        {
            _serviceEngine = serviceEngine;
        }

         [HttpGet]
         //[ValidateAntiForgeryToken]
         public async Task<ActionResult> UserProfile()
         {
             XDS.ONLINE.MODELS.SystemUser usr = await _serviceEngine.CustomVettingAdminSystemUserAsync(Session["AuthToken"].ToString());
             XDS.ONLINE.MODELS.SystemUserSystemUser loggedInUser = usr.SystemUser1.Single(s => s.UserName.ToUpper() == Session["UserName"].ToString().ToUpper());
             
             return View(loggedInUser);
         }

         [HttpPost]
         //[ValidateAntiForgeryToken]
         public async Task<ActionResult> UserProfile(SystemUserSystemUser model)
         {
             if (ModelState.IsValid)
             {

                 string FirstName = model.FirstName;
                 string Surname = model.Surname;
                // string JobPosition = model.JobPosition;
                 XDS.ONLINE.REPOSITORY.XDSCustomVetting.UserRole tempRole =  REPOSITORY.XDSCustomVetting.UserRole.SUBSADMIN.ToString() == model.SystemUserRole ? REPOSITORY.XDSCustomVetting.UserRole.SUBSADMIN : REPOSITORY.XDSCustomVetting.UserRole.SUBSUSERS;

                 try
                 {
                     var x = await _serviceEngine.AdminChangeUserDetailsAsync(Session["AuthToken"].ToString(), FirstName, Surname,model.EmailAddress, "", "",tempRole, model.ActiveYN);
                     model.Result = GetResult(x);
                    if (model.Result.Equals("User details successfully updated"))
                     {
                         //XDS.ONLINE.MODELS.SystemUser usr = await _serviceEngine.AdminSystemUserAsync(Session["AuthToken"].ToString(), true);
                         //model = usr.SystemUser1.Single(s => s.UserName.ToUpper() == Session["UserName"].ToString());
                         Session["DisplayName"] = model.FirstName + " " + model.Surname;
                     }
                     //reset values
                 }
                 catch (Exception)
                 {
                     throw;
                 }
               
                 return View(model);

             }
             return null;
         }

         private String GetResult(string XMLString)
         {
             XmlDocument xml = new XmlDocument();
             xml.LoadXml(XMLString);
             if (XMLString.IndexOf("NoResult") >= 0)
             {

                 return xml.DocumentElement.FirstChild.InnerText; 
             }
             else
             {
                 return xml.DocumentElement.ParentNode.InnerText; 
             }
         }
    } 
}