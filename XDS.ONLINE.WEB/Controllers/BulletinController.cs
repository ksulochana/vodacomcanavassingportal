﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XDS.ONLINE.ENGINE;
using XDS.ONLINE.MODELS;
using System.Threading.Tasks;
using System.Web.Routing;
using System.Xml.Linq;
using System.Xml;

namespace XDS.ONLINE.WEB.Controllers
{
    public class BulletinController : BaseXDSEngineController
    {

        public BulletinController()
        {
        }

        public BulletinController(IXDSServiceEngine serviceEngine)
        {
            _serviceEngine = serviceEngine;
        }

        //
        // GET: /CustomVetting/
        public ActionResult Index()
        {
            return View();
        } 

        [HttpGet]
        public async Task<ActionResult> BulletinMatch()
        {
            ViewBag.Message = "";
            ViewBag.ProductID = "110";

            ListofBulletins em = new ListofBulletins();

            em = await _serviceEngine.CustomVettingGetBulletinsAsync(Session["AuthToken"].ToString()); 

            return View(em);
        } 

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DownloadBulletin(long BulletinID)
        {
            ListofBulletins em = null;

            em = _serviceEngine.CustomVettingGetBulletinFile(Session["AuthToken"].ToString(), BulletinID);

            if (em != null && em.BulletinList.Count() > 0)
            {
                ViewBag.ContentType = "application/pdf";
                ViewBag.ReportName = em.BulletinList[0].DocFileName;
                ViewBag.ReportBinary = em.BulletinList[0].Doc;
            }

            return View();
        }
    }
}