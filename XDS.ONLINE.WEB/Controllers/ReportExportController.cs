﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XDS.ONLINE.ENGINE;
using XDS.ONLINE.ENGINE.REPORTGENERATOR;
using XDS.ONLINE.MODELS;

namespace XDS.ONLINE.WEB.Controllers
{
    public class ReportExportController : BaseController
    {
        private readonly IXDSReportExporter _reportExporter;

        public ReportExportController(IXDSReportExporter XDSReportExporter)
        {
            _reportExporter = XDSReportExporter;
        }

        // GET: ReportExport
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetReport(string EnquiryID, string EnquiryResultID, string Format, int ProductID, string Format1, string EnquiryResultID1)
        {
            Format = Format == null ? Format1 : Format;
            EnquiryResultID = EnquiryResultID == null ? EnquiryResultID1 : EnquiryResultID; 
            string XmlData = "";
            string Reference = "";
            string ContentType = "";
            string FileName = "";

            XmlData = Session["ReportXML"] != null ? Session["ReportXML"].ToString() : "";
            Reference = Guid.NewGuid().ToString();

            byte[] ReportBinary = null;
            Types.ReportFormat fmt = Types.ReportFormat.pdf;

            switch (Format.ToLower())
            {
                case "pdf":
                    fmt = Types.ReportFormat.pdf;
                    FileName = string.Concat(Reference.Replace('-', '_'), ".pdf");
                    ContentType = "application/pdf";
                    break;

                case "html":
                    fmt = Types.ReportFormat.html;
                    FileName = string.Concat(Reference.Replace('-', '_'), ".mht");
                    ContentType = "text/html";
                    break;

                case "image":
                    fmt = Types.ReportFormat.image;
                    FileName = string.Concat(Reference.Replace('-', '_'), ".jpg");
                    ContentType = "image/jpeg";
                    break;
            }

            ReportBinary = _reportExporter.GenerateReport(XmlData, fmt, ProductID);

            ViewBag.ContentType = ContentType;
            ViewBag.ReportName = FileName;
            ViewBag.ReportBinary = ReportBinary;

            return View();
        }


        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EmailReport(string EnquiryID, string EnquiryResultID, int ProductID)
        {
            EmailReportViewModel model = new EmailReportViewModel();
            model.FromName = Session["DisplayName"].ToString() ;
            return View(model);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EmailReportSend(EmailReportViewModel model)
        {
            string XmlData = "";
            string Reference = "";
            string response = "";

            if (ModelState.IsValid)
            { 
            // todo: generate binary and send email
                 if (Session["EnquiryResultID"].ToString() == model.EnquiryResultID.ToString())
                  {
                      XmlData = Session["ReportXML"].ToString();
                      Reference = Guid.NewGuid().ToString();
                  }
                 else
                 {
                     throw new Exception("Report data has been refreshed.");
                 }

                if (_reportExporter.EmailReport(Session["DisplayName"].ToString(), model.ReplyToEmailAddress, model.ToEmailAddress, model.EmailBody, XmlData, model.ProductID))
                {
                    response = "The email has been sent.";
                }
                else
                {
                    response = "There has been a problem with your request. Please check your input and try again.";
                }
            }else{
                response="There has been a problem with your request. Please check your input and try again.";
            }
            if (Request.IsAjaxRequest())
            {
                return Content(response , "text/html");
            }
            return View();
        }
    
    }
}