﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using XDS.ONLINE.WEB.Models;
using XDS.ONLINE.ENGINE;
using LogRouter;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Hosting;

namespace XDS.ONLINE.WEB.Controllers
{
    [Authorize]
    public class AccountController : BaseController
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        private readonly IXDSServiceEngine _serviceEngine;

        public AccountController(IXDSServiceEngine serviceEngine)
        {
            _serviceEngine = serviceEngine;

            //TODO: check token and log off if invalid
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {

            //do login check here?

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            string AuthResponseString = await _serviceEngine.CustomVettingLoginAsync(model.UserName, model.Password);

            #region[ XDS WS ]
            string strXDSUser = System.Configuration.ConfigurationManager.AppSettings["XDSWSUSER"].ToString();
            string XDSPwd = System.Configuration.ConfigurationManager.AppSettings["XDSWSPWD"].ToString(); 
            string strXDSWSTicket = await _serviceEngine.LoginAsync(strXDSUser, XDSPwd); 
            #endregion

            if (AuthResponseString == "NotAuthenticated" || strXDSWSTicket == "NotAuthenticated")
            { }
            else
            {
                if (await _serviceEngine.CustomVettingIsTicketValidAsync(AuthResponseString))
                {
                    Session["AuthToken"] = AuthResponseString;
                    Session["XDSWSAuthToken"] = strXDSWSTicket; 

                    Session["SMSAlertType"] = System.Configuration.ConfigurationManager.AppSettings["SMSAlertType"].ToString();
                    Session["SMSProductID"] = System.Configuration.ConfigurationManager.AppSettings["SMSProductID"].ToString();

                    //FormsAuthentication.SetAuthCookie(model.UserName, false);
                    //var user = new ApplicationUser { UserName = model.UserName };
                    //await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                    XDS.ONLINE.MODELS.SystemUser usr = await _serviceEngine.CustomVettingAdminSystemUserAsync(AuthResponseString);

                    string DisplayName = usr.SystemUser1.Single(s => s.UserName == model.UserName).FirstName + " " + usr.SystemUser1.Single(s => s.UserName == model.UserName).Surname;
                    string UserName = usr.SystemUser1.Single(s => s.UserName == model.UserName).UserName;

                    Session["DisplayName"] = DisplayName;
                    Session["UserName"] = UserName;

                    var identity = new ClaimsIdentity(new[] {
                            new Claim(ClaimTypes.Name, model.UserName),
                        },
                        DefaultAuthenticationTypes.ApplicationCookie,
                        ClaimTypes.Name, ClaimTypes.Role);

                    // if you want roles, just add as many as you want here (for loop maybe?)
                    identity.AddClaim(new Claim(ClaimTypes.Role, "user"));
                    // identity.AddClaim(new Claim(ClaimTypes.NameIdentifier , model.UserName ));

                    // tell OWIN the identity provider, optional
                    // identity.AddClaim(new Claim(IdentityProvider, "Simplest Auth"));

                    AuthenticationManager.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = model.RememberMe
                    }, identity);

                    // obj.Dispose();

                    if (System.Configuration.ConfigurationManager.AppSettings["LoginLogOffLogging"] == "1")
                    {
                        //detect duplicate login
                        //Logging checkErrLog = new Logging(Logging.PreferredLog.WindowsEventLog);
                        Logging checkErrLog = new Logging();
                        //Logging ErrLog = new Logging();
                        Task<LogOutcome> CheckAccess = checkErrLog.CheckAccess(model.UserName.ToString());
                        LogOutcome checkoLogOutcome = await CheckAccess;

                        //Logging ErrLog = new Logging(Logging.PreferredLog.WindowsEventLog);
                        Logging ErrLog = new Logging();
                        AccessInfo oAccessInfo = new AccessInfo();
                        oAccessInfo.LoginId = model.UserName.ToString();
                        oAccessInfo.LoginSource = "XDSONLINE";
                        oAccessInfo.LogggedInInd = true;
                        oAccessInfo.Reason = "LOGIN";
                        oAccessInfo.ServerHostName = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"];
                        oAccessInfo.ServerIPAddress = System.Web.HttpContext.Current.Request.ServerVariables["LOCAL_ADDR"];
                        oAccessInfo.BrowserUseragent = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"];
                        oAccessInfo.ClientHostName = System.Web.HttpContext.Current.Request.UserHostName;
                        oAccessInfo.ClientIPAddress = System.Web.HttpContext.Current.Request.UserHostAddress;
                        oAccessInfo.ClientIPAddressPort = 80;

                        //await ErrLog.LogAccess(oAccessInfo);
                        Task<LogOutcome> LogAccess = ErrLog.LogAccess(oAccessInfo);
                        LogOutcome oLogOutcome = await LogAccess; 
                    }

                    return RedirectToLocal(returnUrl);
                }
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "The user name or password provided is incorrect.");
            return View(model);

        }

        //
        // POST: /Account/LogOff
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> LogOff()
        {

            if (System.Configuration.ConfigurationManager.AppSettings["LoginLogOffLogging"] == "1")
            {
                //Logging ErrLog = new Logging(Logging.PreferredLog.WindowsEventLog);
                Logging ErrLog = new Logging();
                AccessInfo oAccessInfo = new AccessInfo();
                oAccessInfo.LoginId = Session["UserName"].ToString();
                oAccessInfo.LoginSource = "XDSONLINE";
                oAccessInfo.LogggedInInd = true;
                oAccessInfo.Reason = "LOGOFF";
                oAccessInfo.ServerHostName = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"];
                oAccessInfo.ServerIPAddress = System.Web.HttpContext.Current.Request.ServerVariables["LOCAL_ADDR"];
                oAccessInfo.BrowserUseragent = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"];
                oAccessInfo.ClientHostName = System.Web.HttpContext.Current.Request.UserHostName;
                oAccessInfo.ClientIPAddress = System.Web.HttpContext.Current.Request.UserHostAddress;
                oAccessInfo.ClientIPAddressPort = 80;

                //ErrLog.LogAccess(oAccessInfo);
                Task<LogOutcome> LogAccess = ErrLog.LogAccess(oAccessInfo);
                LogOutcome oLogOutcome = await LogAccess;
            }

            AuthenticationManager.SignOut();
            Session["Survey"] = null;
            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            System.IO.File.AppendAllText(@"C:\Log\Log.txt", "Enter Forgot password\n");
            if (ModelState.IsValid)
            {
                System.IO.File.AppendAllText(@"C:\Log\Log.txt", "valid model\n");
                string AuthResponseString = await _serviceEngine.CustomVettingLoginAsync(ConfigurationManager.AppSettings["resetadmin"].ToString() , ConfigurationManager.AppSettings["resetadminpassword"].ToString());
                System.IO.File.AppendAllText(@"C:\Log\Log.txt", "login\n");
                Session["AuthResponseString"] = AuthResponseString;
                ResetPasswordViewModel m1 = new ResetPasswordViewModel();
                m1.ResetCellNumber = model.CellphoneNumber;
                m1.ResetUsername = model.Username;
                if (_serviceEngine.CustomVettingIsTicketValid(AuthResponseString))
                {
                    System.IO.File.AppendAllText(@"C:\Log\Log.txt", "Step1\n");
                    if (_serviceEngine.CheckifUserExists(AuthResponseString, model.Username, model.CellphoneNumber))
                    {
                        System.IO.File.AppendAllText(@"C:\Log\Log.txt", "user exists\n");
                        Session["UserName"] = model.Username;
                        //string code = string.Empty;
                        //string resetlink=  BuildPasswordresetLink(model.CellphoneNumber, out code);

                        //store the link
                        //_serviceEngine.InsertPasswordResetLink(AuthResponseString, model.Username, model.CellphoneNumber, code, ConfigurationManager.AppSettings["resetadmin"].ToString());

                        // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                        // Send an email with this link
                        // string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                        // var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                        //await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");


                        //Generate Code
                        string OTP = _serviceEngine.GenerateResetPasswordCode(AuthResponseString, model.Username, model.CellphoneNumber, ConfigurationManager.AppSettings["resetadmin"].ToString());
                        //m1.ResetPIN = OTP;
                        long code = 0;

                        //Send an SMS
                        if (long.TryParse(OTP, out code))
                        {
                            string strXDSUser = System.Configuration.ConfigurationManager.AppSettings["XDSWSUSER"].ToString();
                            string XDSPwd = System.Configuration.ConfigurationManager.AppSettings["XDSWSPWD"].ToString();
                            string strXDSWSTicket = await _serviceEngine.LoginAsync(strXDSUser, XDSPwd);
                            string Message = System.Configuration.ConfigurationManager.AppSettings["ResetPasswordSMSText"].ToString().Replace("'username'", model.Username).Replace("'OTP'", OTP);
                            _serviceEngine.SMSAlert(strXDSWSTicket, model.CellphoneNumber, Message, System.Configuration.ConfigurationManager.AppSettings["SMSAlertType"].ToString(), 1, int.Parse(System.Configuration.ConfigurationManager.AppSettings["SMSProductID"].ToString()));
                            m1.status = "2";
                            return RedirectToAction("ResetPasswordGet", m1);
                        }
                        else
                        {
                            return PartialView("Error");
                        }
                        //                    return RedirectToAction("ForgotPasswordConfirmation", model);

                    }
                    else
                    {
                        m1.status = "2";
                        // Don't reveal that the user does not exist or is not confirmed
                        return RedirectToAction("ResetPasswordGet", m1);
                    }
                }
                else
                {
                    return PartialView("Error");
                }

            }

            // If we got this far, something failed, redisplay form
            return PartialView();
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation(ForgotPasswordViewModel model)
        {
            return PartialView(model);
        }

        //
        // GET: /Account/ResetPassword
        //[AllowAnonymous]
        //public ActionResult ResetPassword(string code)
        //{
        //    if (code == null)
        //    {
        //        return View("Error");
        //    }
        //    else
        //    {
        //        BaseXDSEngineController oXDSEngine = new BaseXDSEngineController();
        //        var decodedData = oXDSEngine.DecryptString(code);
        //        var dict = HttpUtility.ParseQueryString(decodedData);
        //        string json = Newtonsoft.Json.JsonConvert.SerializeObject(dict.Cast<string>().ToDictionary(k => k, v => dict[v]));                
        //        Models.ResetPasswordViewModel model = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.ResetPasswordViewModel>(json);
        //        model.Code = code;
        //        string AuthResponseString = _serviceEngine.CustomVettingLogin(ConfigurationManager.AppSettings["resetadmin"].ToString(), ConfigurationManager.AppSettings["resetadminpassword"].ToString());

        //        Session["AuthResponseString"] = AuthResponseString;

        //        model.status = _serviceEngine.GetPasswordResetCodeStatus(AuthResponseString, model.Username, model.CellphoneNumber, code);


        //        //if (model.status == "3" || model.status == "1")
        //        //{
        //        //    return RedirectToAction("ResetPasswordConfirmation", model);
        //        //}
        //        //else
        //        //{
        //            return View(model);
        //        //}

        //    }
        //}

        [AllowAnonymous]
        public ActionResult ResetPasswordGet(ResetPasswordViewModel model)
        {
            //long code = 0;
            
            
            //if (!long.TryParse(model.ResetPIN, out code))
            //{
            //    return PartialView("Error");
            //}
            //else
            //{
                
               // model.ResetPIN = string.Empty;
                return PartialView("ResetPassword",model);

            //}
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (Session["AuthResponseString"] == null)
            {
                Session["AuthResponseString"] = _serviceEngine.CustomVettingLogin(ConfigurationManager.AppSettings["resetadmin"].ToString(), ConfigurationManager.AppSettings["resetadminpassword"].ToString());
            }
            if (!ModelState.IsValid)
            {
              
              //  model.status = _serviceEngine.GetPasswordResetCodeStatus(Session["AuthResponseString"].ToString(), model.Username, model.CellphoneNumber, model.Code);

                return View(model);
            }
            string AuthResponseString = Session["AuthResponseString"].ToString();

            model.status = _serviceEngine.GetPasswordResetCodeStatus(AuthResponseString, model.ResetUsername, model.ResetCellNumber, model.ResetPIN);

            if (model.status == "2")
            {
                string result = await _serviceEngine.AdminResetPasswordAsync(AuthResponseString, model.ResetUsername, model.ResetCellNumber, model.Password, model.ConfirmPassword, model.ResetPIN);
                model.status = "3";
            }
            else
            {
                model.status = "2";
                ModelState.AddModelError("ResetPIN","Incorrect one time pin");
                return PartialView("ResetPassword", model);
            }

            return RedirectToAction("ResetPasswordConfirmation", model);


        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation(ResetPasswordViewModel model)
        {
            return PartialView(model);
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }


        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        private string BuildPasswordresetLink(string cellphonenumber, out string code)
        {
           string controller = "Account";
            string action = "ResetPassword";

            //string url = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;
            //// http://localhost:1302/TESTERS/Default6.aspx

            //string path = System.Web.HttpContext.Current.Request.Url.AbsolutePath;

            string baseUrl = string.Format("{0}://{1}{2}/{3}/{4}","https", System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"], System.Web.HttpContext.Current.Request.ApplicationPath, controller, action);

            string key = String.Format("Username={0}", Session["UserName"].ToString());
            string cell = String.Format("CellphoneNumber={0}",cellphonenumber);
            string uniqueref = String.Format("Uniqueref={0}", Guid.NewGuid());
            BaseXDSEngineController oXDSEngine = new BaseXDSEngineController();
            string text = String.Format("{0}&{1}&{2}", key, cell,uniqueref);
            string encoded = oXDSEngine.EncryptString(text);
            code = encoded;
            string ahref = string.Format("{0}?code={1}", baseUrl, encoded);
           

            //Dictionary<string, string> values = new Dictionary<string, string>();
            //values.Add("[REGISTERURL]", ahref);

            ////If FULLREG, get the representative
            //if (vInvite.InviteType == 2)
            //{
            //    var supplier = _iSupplier.FindSupplierIncRep(vInvite.SupplierKey);
            //    values.Add("[REPNAME]", supplier.Representatives[0].FullName);
            //}

            //var email = new MailMessage
            //{
            //    From = new System.Net.Mail.MailAddress(emailFromAddressSetting.Setting, emailFromNameSetting.Setting),
            //    Body = template.FormatContent(values),
            //    Subject = subjectSetting.Setting,
            //    IsBodyHtml = true
            //};
            //email.To.Add(new System.Net.Mail.MailAddress(vInvite.EmailAddress, vInvite.ContactName));
            //return SupplierManager.Support.XmlSerializationUtilities.WriteMessageXml(email);

            return ahref;
        }

       

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}