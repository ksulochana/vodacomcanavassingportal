﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XDS.ONLINE.ENGINE;
using XDS.ONLINE.MODELS;
using System.Threading.Tasks;
using System.Web.Routing;
using System.Xml.Linq;
using System.Xml;

namespace XDS.ONLINE.WEB.Controllers
{
    public class VideoTrainingController : BaseXDSEngineController
    {

        public VideoTrainingController()
        {
        }

        public VideoTrainingController(IXDSServiceEngine serviceEngine)
        {
            _serviceEngine = serviceEngine;
        }

        //
        // GET: /CustomVetting/
        public ActionResult Index()
        {
            return View();
        } 

        [HttpGet]
        public async Task<ActionResult> VideoTrainingMatch()
        {
            ViewBag.Message = "";
            ViewBag.ProductID = "110";

            ListofTrainingDoc em = new ListofTrainingDoc();

            em = await _serviceEngine.CustomVettingGetTrainingDocAsync(Session["AuthToken"].ToString()); 

            return View(em);
        } 

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DownloadTrainingDoc(long TrainingID)
        {
            ListofTrainingDoc em = null;

            em = _serviceEngine.CustomVettingGetTrainingDocFile(Session["AuthToken"].ToString(), TrainingID);

            if (em != null && em.TrainingDocList.Count() > 0)
            {
                string Applicatuionpath = HttpRuntime.AppDomainAppPath;
                string file =  "/TrainingVideo/";

                if (!System.IO.Directory.Exists(Applicatuionpath + "/" + file))
                {
                    System.IO.Directory.CreateDirectory(Applicatuionpath + "/" + file);
                }

                file = file + DateTime.Now.ToString("yyyyMMddhhmmss")+"_Training.mp4";
                em.TrainingDocList[0].VideofileLocation = System.Web.HttpContext.Current.Request.ApplicationPath+file;
                

                System.IO.File.WriteAllBytes(Applicatuionpath +"/"+ file, em.TrainingDocList[0].TrainingDoc);
            }

            return View("../VideoTraining/ViewTrainingContent", em);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public void ClearMedia(string Videoabsolutepath)
        {
            Videoabsolutepath = Videoabsolutepath.Replace(System.Web.HttpContext.Current.Request.ApplicationPath, string.Empty);
            if (System.IO.File.Exists(HttpRuntime.AppDomainAppPath + "/"+ Videoabsolutepath))
            {
                System.IO.File.Delete(HttpRuntime.AppDomainAppPath + "/" + Videoabsolutepath);
            }
           
        }
    }
}