﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XDS.ONLINE.ENGINE;
using XDS.ONLINE.MODELS;
using System.Threading.Tasks;
using System.Web.Routing;
using System.Web.Script.Serialization;
namespace XDS.ONLINE.WEB.Controllers
{
    public class HistoryController : BaseXDSEngineController
    {

        public HistoryController()
        { }

        public HistoryController(IXDSServiceEngine serviceEngine)
        {
            _serviceEngine = serviceEngine;
        }

        #region History Search 
        [HttpGet]
        public async Task<ActionResult> HistoryMatch()
        {
            string connectTicket = Session["AuthToken"].ToString();
            ViewBag.Message = "History";
            ViewBag.ProductID = "110";

            HistoryMatchViewModel bm = new HistoryMatchViewModel();
            bm.StartDate = DateTime.Now.AddDays(-7).ToString("yyyy/mm/dd");
            bm.EndDate = DateTime.Now.ToString("yyyy/mm/dd");
            bm.SystemUserName = Session["UserName"] != null ? Session["UserName"].ToString() : "";

            return View(bm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("HistoryModelSearch")]
        public async Task<ActionResult> HistoryMatch(HistoryMatchViewModel model)
        {
            ViewBag.Message = "Your create enquiry page.";
            if (ModelState.IsValid)
            {
                ListofHistory listOfHistory = null;

                if (model.StartDate == null)
                    model.StartDate = DateTime.Now.AddDays(-7).ToString();
                if (model.EndDate == null)
                    model.EndDate = DateTime.Now.ToString();

                DateTime dtStartDate = new DateTime();
                DateTime dtEndDate = new DateTime();

                DateTime.TryParse(model.StartDate, out dtStartDate);
                DateTime.TryParse(model.EndDate, out dtEndDate);

                listOfHistory = await _serviceEngine.CustomVettingGetTransactionHistoryAsync(Session["AuthToken"].ToString(), Session["UserName"].ToString(), model.IDNo == null? "" : model.IDNo, 
                    model.ReferenceNo == null ? "" : model.ReferenceNo, model.StatusValue == null ? "" : model.StatusValue, dtStartDate, dtEndDate, false);

                Session["ReportXML"] = _serviceEngine.ReportXML();

                return PartialViewHelper("_ListOfHistoryPartial", listOfHistory);

            }
            return null;
        } 
        #endregion
    }
}