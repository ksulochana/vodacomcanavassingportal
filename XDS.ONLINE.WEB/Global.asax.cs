﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using LogRouter;

namespace XDS.ONLINE.WEB
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            UnityConfig.RegisterComponents();
            System.Web.Helpers.AntiForgeryConfig.UniqueClaimTypeIdentifier = System.Security.Claims.ClaimTypes.Name;
            AnnotationsConfig.RegisterAnnotations();
            //AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.NameIdentifier;
           // Shandon.Utils.Mvc.Startup.RegisterValidationAttributes();
        // necessary to enable client side validation
        //DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(Shandon.Utils.Attributes.PersonNameAttribute ), typeof(RegularExpressionAttributeAdapter));

        }

        protected void Session_End(Object sender, EventArgs e)
        {
            //Logging ErrLog = new Logging(Logging.PreferredLog.WindowsEventLog);
            //AccessInfo oAccessInfo = new AccessInfo();
            //oAccessInfo.LoginId = Session["UserName"].ToString();
            //oAccessInfo.LogggedInInd = true;
            //oAccessInfo.Reason = "LOGOFF";
            //oAccessInfo.ServerHostName = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"];
            //oAccessInfo.ServerIPAddress = System.Web.HttpContext.Current.Request.ServerVariables["LOCAL_ADDR"];
            //oAccessInfo.BrowserUseragent = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"];
            //oAccessInfo.ClientHostName = System.Web.HttpContext.Current.Request.UserHostName;
            //oAccessInfo.ClientIPAddress = System.Web.HttpContext.Current.Request.UserHostAddress;
            //oAccessInfo.ClientIPAddressPort = 80;

            //ErrLog.LogAccess(oAccessInfo);

            //Task<LogOutcome> LogAccess = ErrLog.LogAccess(oAccessInfo);
            //LogOutcome oLogOutcome = await LogAccess;
        }
    }
}
