﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XDS.ONLINE.WEB.Models
{
    public class ErrorViewModel
    {
        public Int64 ErrorID { get; set; }
        public string Message { get; set; }

    }
}