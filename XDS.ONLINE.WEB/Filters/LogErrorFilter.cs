﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

using XDS.ONLINE.WEB.Models;
using Shandon.Logging;
using LogRouter;
using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace XDS.ONLINE.WEB.Filters
{
    public class LogErrorFilter : FilterAttribute, IExceptionFilter {
    #region Implements IExceptionFilter
    void IExceptionFilter.OnException(ExceptionContext filterContext) {
      if (filterContext != null && filterContext.Exception != null) {   
        string sControllerName = filterContext.RouteData.Values["controller"].ToString();
        string sActionName = filterContext.RouteData.Values["action"].ToString();
        string sMethod = "";
        Int64  LoginAuditID = 0;

        HttpRequest oHttpRequest = HttpContext.Current.Request;
        

        if (oHttpRequest != null) {
            sMethod = oHttpRequest.HttpMethod;
            if (filterContext.RequestContext.HttpContext.Session["LoginAuditID"]!=null)
            {
                LoginAuditID = (Int64)filterContext.RequestContext.HttpContext.Session["LoginAuditID"];
            }
        }

          ExceptionLogger logger = new ExceptionLogger();
          logger.LogError(filterContext.Exception, "XDS.ONLINE", sControllerName, sActionName, sMethod, "", "", LoginAuditID);

          if (System.Configuration.ConfigurationManager.AppSettings["ErrorLoggingInd"] == "1")
          {
              Logging ErrLog = new Logging();
             
              ErrorInfo oErrorInfo = new ErrorInfo();

              if (System.Web.HttpContext.Current.Session["UserName"] == null)
              {
                  oErrorInfo.LoginId = string.Empty;
              }
              else
              {
                  oErrorInfo.LoginId = System.Web.HttpContext.Current.Session["UserName"].ToString();
              }
              oErrorInfo.Action = sActionName;
              oErrorInfo.LoginSource = "XDSONLINE";
              oErrorInfo.ErrorMessage = filterContext.Exception.Message;
              oErrorInfo.Application = "XDS";
              oErrorInfo.Classification = "ERROR";
              oErrorInfo.ClientHostName = HttpContext.Current.Request.UserHostName;
              oErrorInfo.Method = sControllerName + "/" + sMethod;
              oErrorInfo.ClientIPAddress = HttpContext.Current.Request.UserHostAddress;
              oErrorInfo.Process = "80";
              oErrorInfo.ServerHostName = HttpContext.Current.Request.ServerVariables["SERVER_NAME"];
              oErrorInfo.ServerIPAddress = HttpContext.Current.Request.ServerVariables["LOCAL_ADDR"];
              oErrorInfo.Severity = 1;
              oErrorInfo.StackTrace = filterContext.Exception.StackTrace; ;



              Task<LogOutcome> LogError = ErrLog.LogError(oErrorInfo);
              //LogOutcome oLogOutcome = await LogError;
             // LogOutcome oLogOutcome = await LogError;


          }

        RouteValueDictionary oValues;

                if (sControllerName == "Error")
                {
                    oValues = new RouteValueDictionary(new { action = "Login", controller = "Account" });
                }
                else
                {
                    if (filterContext.HttpContext.Request.IsAjaxRequest())
                    {
                       // add status code for ajax call in case of error. error handler on ajax calls to change to cater for this and show message
                        // filterContext.HttpContext.Response.StatusCode = 500;

                        oValues = new RouteValueDictionary(new { action = "AjaxIndex", controller = "Error" });
                    }
                    else
                    {
                        oValues = new RouteValueDictionary(new { action = "Index", controller = "Error" });
                    }
                }

        


          //cater for customs error messages but still log for investigation



        if (filterContext.Exception is TimeoutException)
        {
            oValues.Add("errorViewModel.Message", "The request has timed out. Please refine your search and try again.");
        }
        else
        {         
          oValues.Add("errorViewModel.Message" , logger.ErrorMessage );
        }





      
        oValues.Add("errorViewModel.ErrorID", logger.ErrorID);



        logger = null;

        filterContext.Result = new RedirectToRouteResult(oValues);
        filterContext.ExceptionHandled = true;
      }
    }
    #endregion

    #region Private Static Properties
   // private static ErrorEngine moErrorEngine = new ErrorEngine();
    #endregion

    #region Public Static Methods


    
    #endregion
  }
}