﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using XDS.ONLINE.MODELS;

namespace XDS.ONLINE.VERIFICATION
{
    public class LinkageDetector
    {

        public LinkageDetector()
        {
        }

        public string ConsumerLinkageDetector(string PartyAXml, string PartyBXml)
        {
            Boolean addLinkage = false;
            PartyBXml = PartyBXml.Replace("ConsumerOther", "Consumer");
            StringBuilder LinkageData = new StringBuilder("<ConfirmedLinkages>");

            ConsumerTraceReportModel PartyADetails = new ConsumerTraceReportModel();
            ConsumerTraceReportModel PartyBDetails = new ConsumerTraceReportModel();

            PartyADetails = (ConsumerTraceReportModel)CreateObject(PartyAXml, PartyADetails);
            PartyBDetails = (ConsumerTraceReportModel)CreateObject(PartyBXml, PartyBDetails);

            ConsumerConsumerAddressHistory[] PartyAAddressHistory = PartyADetails.ConsumerAddressHistory;
            ConsumerConsumerTelephoneHistory[] PartyAContactNumberHistory = PartyADetails.ConsumerTelephoneHistory;
            ConsumerConsumerEmploymentHistory[] PartyAEmploymentHistory = PartyADetails.ConsumerEmploymentHistory;

            ConsumerConsumerAddressHistory[] PartyBAddressHistory = PartyBDetails.ConsumerAddressHistory;
            ConsumerConsumerTelephoneHistory[] PartyBContactNumberHistory = PartyBDetails.ConsumerTelephoneHistory;
            ConsumerConsumerEmploymentHistory[] PartyBEmploymentHistory = PartyBDetails.ConsumerEmploymentHistory;

            if (PartyAContactNumberHistory != null && PartyBContactNumberHistory != null)
            {
                var PartyA_inPartyB_PhoneNumber = (from PartyAContact in PartyAContactNumberHistory
                                                   join PartyBContact in PartyBContactNumberHistory
                                            on ReplaceContactNumCharacters(PartyAContact.TelephoneNo) equals ReplaceContactNumCharacters(PartyBContact.TelephoneNo)
                                            into matches
                                                   where matches.Any()
                                                   select PartyAContact).ToList();
                var distinctList = PartyA_inPartyB_PhoneNumber.GroupBy(x => x.TelephoneNo)
                        .Select(g => g.First())
                        .ToList();

                foreach (var linkedPhoneNumber in distinctList)
                {
                    LinkageData = LinkageData.Append("<ConfirmedLinkage>");
                    LinkageData = LinkageData.Append("<LinkageCategory>Contact No</LinkageCategory>");
                    LinkageData = LinkageData.Append("<ConsumerRecordDate>" + linkedPhoneNumber.LastUpdatedDate + "</ConsumerRecordDate>");
                    LinkageData = LinkageData.Append("<ConsumerRecordType>" + linkedPhoneNumber.TelephoneType + "</ConsumerRecordType>");
                    LinkageData = LinkageData.Append("<OtherRecordDate>" + linkedPhoneNumber.LastUpdatedDate + "</OtherRecordDate>");
                    LinkageData = LinkageData.Append("<OtherRecordType>" + linkedPhoneNumber.TelephoneType + "</OtherRecordType>");
                    LinkageData = LinkageData.Append("<MatchedData>" + linkedPhoneNumber.TelephoneNo + "</MatchedData>");
                    LinkageData = LinkageData.Append("</ConfirmedLinkage>");

                    addLinkage = true;
                }

            }

            if (PartyAEmploymentHistory != null && PartyBEmploymentHistory != null)
            {
                var PartyA_inPartyB_Employment = (from PartyAEmployment in PartyAEmploymentHistory
                                                  from PartyBEmployment in PartyBEmploymentHistory
                                                  where XDS.ONLINE.VERIFICATION.LevenshteinDistance.Compute(CompareNames(PartyAEmployment.EmployerDetail), CompareNames(PartyBEmployment.EmployerDetail)) <= 2
                                                  select PartyAEmployment).ToList();
                var distinctList = PartyA_inPartyB_Employment.GroupBy(x => x.EmployerDetail)
                        .Select(g => g.First())
                        .ToList();

                foreach (var linkedEmployment in distinctList)
                {
                    LinkageData = LinkageData.Append("<ConfirmedLinkage>");
                    LinkageData = LinkageData.Append("<LinkageCategory>Employment</LinkageCategory>");
                    LinkageData = LinkageData.Append("<ConsumerRecordDate>" + linkedEmployment.LastUpdatedDate + "</ConsumerRecordDate>");
                    LinkageData = LinkageData.Append("<ConsumerRecordType>Work</ConsumerRecordType>");
                    LinkageData = LinkageData.Append("<OtherRecordDate>" + linkedEmployment.LastUpdatedDate + "</OtherRecordDate>");
                    LinkageData = LinkageData.Append("<OtherRecordType>Work</OtherRecordType>");
                    LinkageData = LinkageData.Append("<MatchedData>" + linkedEmployment.EmployerDetail + "</MatchedData>");
                    LinkageData = LinkageData.Append("</ConfirmedLinkage>");

                    addLinkage = true;
                }

            }

            //Address Line 1........1

            if (PartyAAddressHistory != null && PartyBAddressHistory != null)
            {
                var PartyA_inPartyB_Address = (from PartyAAddress in PartyAAddressHistory
                                               from PartyBAddress in PartyBAddressHistory
                                               where XDS.ONLINE.VERIFICATION.LevenshteinDistance.Compute(ReplaceAddressCharacters(PartyAAddress.Address1), ReplaceAddressCharacters(PartyBAddress.Address1)) <= 2 && PartyAAddress.PostalCode == PartyBAddress.PostalCode && PartyAAddress.Address1.Trim() != ""
                                               select PartyAAddress).ToList();

                var distinctList = PartyA_inPartyB_Address.GroupBy(x => x.Address1)
                         .Select(g => g.First())
                         .ToList();

                foreach (var linkedAddress in distinctList)
                {
                    LinkageData = LinkageData.Append("<ConfirmedLinkage>");
                    LinkageData = LinkageData.Append("<LinkageCategory>Address</LinkageCategory>");
                    LinkageData = LinkageData.Append("<ConsumerRecordDate>" + linkedAddress.LastUpdatedDate + "</ConsumerRecordDate>");
                    LinkageData = LinkageData.Append("<ConsumerRecordType>" + linkedAddress.AddressType + "</ConsumerRecordType>");
                    LinkageData = LinkageData.Append("<OtherRecordDate>" + linkedAddress.LastUpdatedDate + "</OtherRecordDate>");
                    LinkageData = LinkageData.Append("<OtherRecordType>" + linkedAddress.AddressType + "</OtherRecordType>");
                    LinkageData = LinkageData.Append("<MatchedData>" + linkedAddress.Address + "</MatchedData>");
                    LinkageData = LinkageData.Append("</ConfirmedLinkage>");

                    addLinkage = true;
                }
           
                //Address Line 1........2
                PartyA_inPartyB_Address = null;
                distinctList = null;

                  PartyA_inPartyB_Address = (from PartyAAddress in PartyAAddressHistory
                                               from PartyBAddress in PartyBAddressHistory
                                             where XDS.ONLINE.VERIFICATION.LevenshteinDistance.Compute(ReplaceAddressCharacters(PartyAAddress.Address1), ReplaceAddressCharacters(PartyBAddress.Address2)) <= 2 && PartyAAddress.PostalCode == PartyBAddress.PostalCode && PartyAAddress.Address1.Trim() != ""
                                               select PartyAAddress).ToList();

                  distinctList = PartyA_inPartyB_Address.GroupBy(x => x.Address1)
                         .Select(g => g.First())
                         .ToList();

                  foreach (var linkedAddress in distinctList)
                {
                    LinkageData = LinkageData.Append("<ConfirmedLinkage>");
                    LinkageData = LinkageData.Append("<LinkageCategory>Address</LinkageCategory>");
                    LinkageData = LinkageData.Append("<ConsumerRecordDate>" + linkedAddress.FirstReportedDate + "</ConsumerRecordDate>");
                    LinkageData = LinkageData.Append("<ConsumerRecordType>" + linkedAddress.AddressType + "</ConsumerRecordType>");
                    LinkageData = LinkageData.Append("<OtherRecordDate>" + linkedAddress.FirstReportedDate + "</OtherRecordDate>");
                    LinkageData = LinkageData.Append("<OtherRecordType>" + linkedAddress.AddressType + "</OtherRecordType>");
                    LinkageData = LinkageData.Append("<MatchedData>" + linkedAddress.Address + "</MatchedData>");
                    LinkageData = LinkageData.Append("</ConfirmedLinkage>");

                    addLinkage = true;
                }
            
                 //Address Line 2........1

                PartyA_inPartyB_Address = null;
                distinctList = null;

                 PartyA_inPartyB_Address = (from PartyAAddress in PartyAAddressHistory
                                               from PartyBAddress in PartyBAddressHistory
                                            where XDS.ONLINE.VERIFICATION.LevenshteinDistance.Compute(ReplaceAddressCharacters(PartyAAddress.Address2), ReplaceAddressCharacters(PartyBAddress.Address1)) <= 2 && PartyAAddress.PostalCode == PartyBAddress.PostalCode && PartyAAddress.Address2.Trim() != ""
                                            select PartyAAddress).ToList();
                 
                distinctList = PartyA_inPartyB_Address.GroupBy(x => x.Address1)
                         .Select(g => g.First())
                         .ToList();

                foreach (var linkedAddress in distinctList)
                {
                    LinkageData = LinkageData.Append("<ConfirmedLinkage>");
                    LinkageData = LinkageData.Append("<LinkageCategory>Address</LinkageCategory>");
                    LinkageData = LinkageData.Append("<ConsumerRecordDate>" + linkedAddress.FirstReportedDate + "</ConsumerRecordDate>");
                    LinkageData = LinkageData.Append("<ConsumerRecordType>" + linkedAddress.AddressType + "</ConsumerRecordType>");
                    LinkageData = LinkageData.Append("<OtherRecordDate>" + linkedAddress.FirstReportedDate + "</OtherRecordDate>");
                    LinkageData = LinkageData.Append("<OtherRecordType>" + linkedAddress.AddressType + "</OtherRecordType>");
                    LinkageData = LinkageData.Append("<MatchedData>" + linkedAddress.Address + "</MatchedData>");
                    LinkageData = LinkageData.Append("</ConfirmedLinkage>");

                    addLinkage = true;
                }

                //Address Line 2........2

                PartyA_inPartyB_Address = null;
                distinctList = null;

                PartyA_inPartyB_Address = (from PartyAAddress in PartyAAddressHistory
                                           from PartyBAddress in PartyBAddressHistory
                                           where XDS.ONLINE.VERIFICATION.LevenshteinDistance.Compute(ReplaceAddressCharacters(PartyAAddress.Address2), ReplaceAddressCharacters(PartyBAddress.Address2)) <= 2 && PartyAAddress.PostalCode == PartyBAddress.PostalCode && PartyAAddress.Address2.Trim() != ""
                                           select PartyAAddress).ToList();
                distinctList = PartyA_inPartyB_Address.GroupBy(x => x.Address1)
                        .Select(g => g.First())
                        .ToList();

                foreach (var linkedAddress in distinctList)
                {
                    LinkageData = LinkageData.Append("<ConfirmedLinkage>");
                    LinkageData = LinkageData.Append("<LinkageCategory>Address</LinkageCategory>");
                    LinkageData = LinkageData.Append("<ConsumerRecordDate>" + linkedAddress.FirstReportedDate + "</ConsumerRecordDate>");
                    LinkageData = LinkageData.Append("<ConsumerRecordType>" + linkedAddress.AddressType + "</ConsumerRecordType>");
                    LinkageData = LinkageData.Append("<OtherRecordDate>" + linkedAddress.FirstReportedDate + "</OtherRecordDate>");
                    LinkageData = LinkageData.Append("<OtherRecordType>" + linkedAddress.AddressType + "</OtherRecordType>");
                    LinkageData = LinkageData.Append("<MatchedData>" + linkedAddress.Address + "</MatchedData>");
                    LinkageData = LinkageData.Append("</ConfirmedLinkage>");

                    addLinkage = true;
                }

            }
            LinkageData = LinkageData.Append("</ConfirmedLinkages>");

            if (!addLinkage)
            {
                LinkageData = new StringBuilder("");
            }
            return LinkageData.ToString();
        }


        private Object CreateObject(string XMLString, Object YourClassObject)
        {
            if (XMLString.Length >= 0)
            {
                try
                {
                    XmlSerializer oXmlSerializer = new XmlSerializer(YourClassObject.GetType());
                    //The StringReader will be the stream holder for the existing XML file 
                    YourClassObject = oXmlSerializer.Deserialize(new StringReader(XMLString));
                    //initially deserialized, the data is represented by an object without a defined type 
                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }
            return YourClassObject;

        }

        private string CompareNames(string CapturedName)
        {
            CapturedName = CapturedName.ToUpper().Trim().Replace(" BK", "").Replace(" CC", "").Replace(" & ", " AND ").Replace("&amp;", "AND").Replace(" EN ", " AND ").Replace("P/L", "PTY LTD").Replace("F/T", "FAMILY TRUST");
            CapturedName = CapturedName.Replace(".", "").Replace(" ", "").Replace("'", "").Replace("(", "").Replace(")", "").Replace("-", "");
            CapturedName = CapturedName.Replace("EIENDOMS", "PTY").Replace("EDMS", "PTY").Replace("PROPRIETARY", "PTY").Replace("INCORPORATED", "INC");
            CapturedName = CapturedName.Replace("BEPERK", "LTD").Replace("BPK", "LTD").Replace("LIMITED", "LTD").Replace("FAMILIE", "FAMILY");
            CapturedName = CapturedName.Replace("PTY", "").Replace("LTD", "").Replace("INC", "");

            return CapturedName;
        }

        private string ReplaceContactNumCharacters(string ContactNumber)
        {
            return ContactNumber.Replace("-", "").Replace("(", "").Replace(")", "").Replace(" ", "");
        }

        private string ReplaceAddressCharacters(string AddressLine)
        {
            string sReturnString;
            sReturnString = AddressLine.ToUpper().Replace("-", "").Replace("(", "").Replace(")", "").Replace(".", "");
            sReturnString = sReturnString.Replace("STREET", "").Replace("ROAD", "").Replace("DRIVE", "").Replace("STRAAT", "").Replace("LAAN", "").Replace("LANE", "").Replace("AVENUE", "").Replace("PLACE", "").Replace("CRESCENT", "").Replace("COURT", "").Replace("WEG", "");
            sReturnString = sReturnString.Replace("STR", "").Replace(" RD", "").Replace(" DR", "").Replace(" ST", "").Replace(" LN", "").Replace(" AVE", "").Replace(" PL", "").Replace(" CRES", "").Replace(" CRT", "");
            sReturnString = sReturnString.Replace(" ", "");
            sReturnString = sReturnString.Replace("POSBUS", "").Replace("POBOX", "").Replace("PRIVATEBAG", "").Replace("PRIVAATSAK", "");
            return sReturnString;
        }

    }

}
