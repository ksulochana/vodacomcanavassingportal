﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shandon.Logging
{
    public class ExceptionLogger : Shandon.Logging.IExceptionLogger
    {
        //intention: To log application errors

        private Int64 _ErrorID;
        private string _ErrorMessage;

        public Int64 ErrorID {
            get
            {
                return _ErrorID;
            }
        }

        public string ErrorMessage {
            get {

                string message = "There has been a problem with your request. Please try again. Your reference is: {0}";
                if (_ErrorID != null)
                {
                    message = string.Format(message, _ErrorID);
                }
                else
                {
                    message = "There has been a problem with your request. Please try again.";
                }
                return message;

            }
        }

        public void LogError(Exception exception, string ApplicationName, string Process, string Action, string Method, string ExtraInfo = "",  string ServerIPAddress ="", Int64 LoginAuditID = 0)
        {
        // TODO: log it somewhere
            _ErrorID = DateTime.Now.Second;
        }

        #region other
      //   StringBuilder oString = new StringBuilder();

      //if (sessionModel != null) {
      //  oString.AppendLine(sessionModel.ToString());
      //}
      //if (supportServicesSessionModel != null) {
      //  oString.AppendLine(supportServicesSessionModel.ToString());
      //}
      //oString.AppendLine("<Error>");
      //oString.AppendLine(String.Format("<Message>{0}</Message>", exception.Message));
      //oString.AppendLine(String.Format("<StackTrace>{0}</StackTrace>", exception.StackTrace));
      //oString.AppendLine("</Error>");
      //DBException oDBException = exception as DBException;
      //if (oDBException != null) {
      //  oString.AppendLine("<DatabaseError>");
      //  oString.AppendLine(String.Format("<StoredProcedure>{0}</StoredProcedure>", oDBException.StoredProcedureDetails));
      //  oString.AppendLine(String.Format("<DataSet>{0}</DataSet>", oDBException.DataSetXml));
      //  oString.AppendLine("</DatabaseError>");
      //}
      //oString.AppendLine("<Environment>");
      //oString.AppendLine(String.Format("<MachineName>{0}</MachineName>", System.Environment.MachineName));
      //oString.AppendLine(String.Format("<WorkingSet>{0}</WorkingSet>", System.Environment.WorkingSet));
      //oString.AppendLine(String.Format("<Version>{0}</Version>", System.Environment.Version));
      //oString.AppendLine("</Environment>");

      //oString.AppendLine("<HttpRequest>");
      //oString.AppendLine(String.Format("<UserAgent>{0}</UserAgent>", userAgent));
      //oString.AppendLine(String.Format("<UserHostAddress>{0}</UserHostAddress>", userHostAddress));
      //oString.AppendLine(String.Format("<UserHostName>{0}</UserHostName>", userHostName));
      //oString.AppendLine("</HttpRequest>");

      //return ErrorRepository.InsertErrorLog("Mobile Website", processCode, actionCode, oString.ToString());

#endregion
    }
}
