﻿using System;
namespace Shandon.Logging
{
    interface IExceptionLogger
    {
        long ErrorID { get; }
        string ErrorMessage{get;}
        void LogError(Exception exception, string ApplicationName, string Process, string Action, string Method, string ExtraInfo = "", string ServerIPAddress = "", long LoginAuditID = 0);
    }
}
