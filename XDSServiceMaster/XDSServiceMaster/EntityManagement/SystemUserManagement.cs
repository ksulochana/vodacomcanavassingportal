﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace XDSServiceMaster.EntityManagement
{
    public class SystemUserManagement
    {
           public SystemUserManagement()
        {

        }
        public EntityDataModels.SystemUser GetSystemUserRecord(SqlConnection conString, int SystemUserID)
        {
            if (conString.State == System.Data.ConnectionState.Closed)
                conString.Open();

            EntityDataModels.SystemUser objSystemUser = new EntityDataModels.SystemUser();

            //SqlConnection ObjConstring = new SqlConnection(conString);
            //ObjConstring.Open();

            string sqlSelect = "SELECT [SystemUserID],isnull([SystemUserTypeInd],'') AS SystemUserTypeInd ,isnull([FirstName],'') AS FirstName,isnull([Surname],'') AS Surname,isnull([IDNo],'') AS IDNo,isnull([JobPosition],'') AS JobPosition,isnull([CellularCode], '') AS CellularCode,isnull([CellularNo], '')  AS CellularNo,isnull([EmailAddress], '') AS EmailAddress,isnull([ActiveYN],0) AS ActiveYN,isnull([Username],'') AS Username,isnull([SubscriberID],0) AS SubscriberID,isnull([CreatedByUser], '') AS CreatedByUser,isnull([CreatedOnDate], '') AS CreatedOnDate,isnull([ChangedByUser], '') AS ChangedByUser,isnull([ChangedOnDate], '') AS ChangedOnDate,isnull([SystemUserRole], '') AS SystemUserRole from SystemUser (NOLOCK) where SystemUserID = " + SystemUserID;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, conString);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSystemUser.SubscriberID = Convert.ToInt32(Dr["SubscriberID"]);
                objSystemUser.ActiveYN = bool.Parse(Dr["ActiveYN"].ToString());
                objSystemUser.CellularCode = Dr["CellularCode"].ToString();
                objSystemUser.CellularNo = Dr["CellularNo"].ToString();
                objSystemUser.ChangedByUser = Dr["ChangedByUser"].ToString();
                objSystemUser.ChangedOnDate = DateTime.Parse(Dr["ChangedOnDate"].ToString());
                objSystemUser.CreatedByUser = Dr["CreatedByUser"].ToString();
                objSystemUser.CreatedOnDate = DateTime.Parse(Dr["CreatedOnDate"].ToString());
                objSystemUser.EmailAddress = Dr["EmailAddress"].ToString();
                objSystemUser.FirstName = Dr["FirstName"].ToString();
                objSystemUser.IDNo = Dr["IDNo"].ToString();
                objSystemUser.JobPosition = Dr["JobPosition"].ToString();
                objSystemUser.Surname = Dr["Surname"].ToString();

                objSystemUser.SystemUserID = int.Parse(Dr["SystemUserID"].ToString());
                objSystemUser.SystemUserRole = Dr["SystemUserRole"].ToString();
                objSystemUser.SystemUserTypeInd = Dr["SystemUserTypeInd"].ToString();
                objSystemUser.Username = Dr["Username"].ToString();
               
            }

            objsqladp.Dispose();
            Objsqlcom.Dispose();
            ds.Dispose();
            conString.Close();

           return objSystemUser;
        }

        public EntityDataModels.SystemUser GetSystemUser(SqlConnection conString, string strUserName)
        {
            if (conString.State == ConnectionState.Closed)
                conString.Open();

            EntityDataModels.SystemUser objSystemUser = new EntityDataModels.SystemUser();

            //SqlConnection ObjConstring = new SqlConnection(conString);
            //ObjConstring.Open();

            string sqlSelect = string.Format("SELECT [SystemUserID],isnull([SystemUserTypeInd],'') AS SystemUserTypeInd ,isnull([FirstName],'') AS FirstName,isnull([Surname],'') AS Surname,isnull([IDNo],'') AS IDNo,isnull([JobPosition],'') AS JobPosition,isnull([CellularCode], '') AS CellularCode,isnull([CellularNo], '')  AS CellularNo,isnull([EmailAddress], '') AS EmailAddress,isnull([ActiveYN],0) AS ActiveYN,isnull([Username],'') AS Username,isnull([SubscriberID],0) AS SubscriberID,isnull([CreatedByUser], '') AS CreatedByUser,isnull([CreatedOnDate], '') AS CreatedOnDate,isnull([ChangedByUser], '') AS ChangedByUser,isnull([ChangedOnDate], '') AS ChangedOnDate,isnull([SystemUserRole], '') AS SystemUserRole from SystemUser (NOLOCK) where UserName ='{0}' ", strUserName);

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, conString);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSystemUser.SubscriberID = Convert.ToInt32(Dr["SubscriberID"]);
                objSystemUser.ActiveYN = bool.Parse(Dr["ActiveYN"].ToString());
                objSystemUser.CellularCode = Dr["CellularCode"].ToString();
                objSystemUser.CellularNo = Dr["CellularNo"].ToString();
                objSystemUser.ChangedByUser = Dr["ChangedByUser"].ToString();
                objSystemUser.ChangedOnDate = DateTime.Parse(Dr["ChangedOnDate"].ToString());
                objSystemUser.CreatedByUser = Dr["CreatedByUser"].ToString();
                objSystemUser.CreatedOnDate = DateTime.Parse(Dr["CreatedOnDate"].ToString());
                objSystemUser.EmailAddress = Dr["EmailAddress"].ToString();
                objSystemUser.FirstName = Dr["FirstName"].ToString();
                objSystemUser.IDNo = Dr["IDNo"].ToString();
                objSystemUser.JobPosition = Dr["JobPosition"].ToString();
                objSystemUser.Surname = Dr["Surname"].ToString();

                objSystemUser.SystemUserID = int.Parse(Dr["SystemUserID"].ToString());
                objSystemUser.SystemUserRole = Dr["SystemUserRole"].ToString();
                objSystemUser.SystemUserTypeInd = Dr["SystemUserTypeInd"].ToString();
                objSystemUser.Username = Dr["Username"].ToString();

            }
            objsqladp.Dispose();
            Objsqlcom.Dispose();
            ds.Dispose();
            conString.Close();

            return objSystemUser;
        }

        public EntityDataModels.SystemUser GetSystemUser(string conString, string strUserName)
        {
            EntityDataModels.SystemUser objSystemUser = new EntityDataModels.SystemUser();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = string.Format("SELECT [SystemUserID],isnull([SystemUserTypeInd],'') AS SystemUserTypeInd ,isnull([FirstName],'') AS FirstName,isnull([Surname],'') AS Surname,isnull([IDNo],'') AS IDNo,isnull([JobPosition],'') AS JobPosition,isnull([CellularCode], '') AS CellularCode,isnull([CellularNo], '')  AS CellularNo,isnull([EmailAddress], '') AS EmailAddress,isnull([ActiveYN],0) AS ActiveYN,isnull([Username],'') AS Username,isnull([SubscriberID],0) AS SubscriberID,isnull([CreatedByUser], '') AS CreatedByUser,isnull([CreatedOnDate], '') AS CreatedOnDate,isnull([ChangedByUser], '') AS ChangedByUser,isnull([ChangedOnDate], '') AS ChangedOnDate,isnull([SystemUserRole], '') AS SystemUserRole from SystemUser (NOLOCK) where UserName ='{0}' ", strUserName);

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSystemUser.SubscriberID = Convert.ToInt32(Dr["SubscriberID"]);
                objSystemUser.ActiveYN = bool.Parse(Dr["ActiveYN"].ToString());
                objSystemUser.CellularCode = Dr["CellularCode"].ToString();
                objSystemUser.CellularNo = Dr["CellularNo"].ToString();
                objSystemUser.ChangedByUser = Dr["ChangedByUser"].ToString();
                objSystemUser.ChangedOnDate = DateTime.Parse(Dr["ChangedOnDate"].ToString());
                objSystemUser.CreatedByUser = Dr["CreatedByUser"].ToString();
                objSystemUser.CreatedOnDate = DateTime.Parse(Dr["CreatedOnDate"].ToString());
                objSystemUser.EmailAddress = Dr["EmailAddress"].ToString();
                objSystemUser.FirstName = Dr["FirstName"].ToString();
                objSystemUser.IDNo = Dr["IDNo"].ToString();
                objSystemUser.JobPosition = Dr["JobPosition"].ToString();
                objSystemUser.Surname = Dr["Surname"].ToString();

                objSystemUser.SystemUserID = int.Parse(Dr["SystemUserID"].ToString());
                objSystemUser.SystemUserRole = Dr["SystemUserRole"].ToString();
                objSystemUser.SystemUserTypeInd = Dr["SystemUserTypeInd"].ToString();
                objSystemUser.Username = Dr["Username"].ToString();

            }
            objsqladp.Dispose();
            Objsqlcom.Dispose();
            ds.Dispose();
            ObjConstring.Close();
            return objSystemUser;
        }

        public EntityDataModels.SystemUser GetSystemUserRecord(string conString, int SystemUserID)
        {
            EntityDataModels.SystemUser objSystemUser = new EntityDataModels.SystemUser();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "SELECT [SystemUserID],isnull([SystemUserTypeInd],'') AS SystemUserTypeInd ,isnull([FirstName],'') AS FirstName,isnull([Surname],'') AS Surname,isnull([IDNo],'') AS IDNo,isnull([JobPosition],'') AS JobPosition,isnull([CellularCode], '') AS CellularCode,isnull([CellularNo], '')  AS CellularNo,isnull([EmailAddress], '') AS EmailAddress,isnull([ActiveYN],0) AS ActiveYN,isnull([Username],'') AS Username,isnull([SubscriberID],0) AS SubscriberID,isnull([CreatedByUser], '') AS CreatedByUser,isnull([CreatedOnDate], '') AS CreatedOnDate,isnull([ChangedByUser], '') AS ChangedByUser,isnull([ChangedOnDate], '') AS ChangedOnDate,isnull([SystemUserRole], '') AS SystemUserRole from SystemUser (NOLOCK) where SystemUserID = " + SystemUserID;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSystemUser.SubscriberID = Convert.ToInt32(Dr["SubscriberID"]);
                objSystemUser.ActiveYN = bool.Parse(Dr["ActiveYN"].ToString());
                objSystemUser.CellularCode = Dr["CellularCode"].ToString();
                objSystemUser.CellularNo = Dr["CellularNo"].ToString();
                objSystemUser.ChangedByUser = Dr["ChangedByUser"].ToString();
                objSystemUser.ChangedOnDate = DateTime.Parse(Dr["ChangedOnDate"].ToString());
                objSystemUser.CreatedByUser = Dr["CreatedByUser"].ToString();
                objSystemUser.CreatedOnDate = DateTime.Parse(Dr["CreatedOnDate"].ToString());
                objSystemUser.EmailAddress = Dr["EmailAddress"].ToString();
                objSystemUser.FirstName = Dr["FirstName"].ToString();
                objSystemUser.IDNo = Dr["IDNo"].ToString();
                objSystemUser.JobPosition = Dr["JobPosition"].ToString();
                objSystemUser.Surname = Dr["Surname"].ToString();

                objSystemUser.SystemUserID = int.Parse(Dr["SystemUserID"].ToString());
                objSystemUser.SystemUserRole = Dr["SystemUserRole"].ToString();
                objSystemUser.SystemUserTypeInd = Dr["SystemUserTypeInd"].ToString();
                objSystemUser.Username = Dr["Username"].ToString();

            }
            objsqladp.Dispose();
            Objsqlcom.Dispose();
            ds.Dispose();
            ObjConstring.Close();
            return objSystemUser;
        }

        public DataSet GetSystemUserList(string conString, int SubscriberID)
        {
            EntityDataModels.SystemUser objSystemUser = new EntityDataModels.SystemUser();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "SELECT [SystemUserID],isnull([SystemUserTypeInd],'') AS SystemUserTypeInd ,isnull([FirstName],'') AS FirstName,isnull([Surname],'') AS Surname,isnull([IDNo],'') AS IDNo,isnull([JobPosition],'') AS JobPosition,isnull([CellularCode], '') AS CellularCode,isnull([CellularNo], '')  AS CellularNo,isnull([EmailAddress], '') AS EmailAddress,isnull([ActiveYN],0) AS ActiveYN,isnull([Username],'') AS Username,isnull([SubscriberID],0) AS SubscriberID,isnull([CreatedByUser], '') AS CreatedByUser,isnull([CreatedOnDate], '') AS CreatedOnDate,isnull([ChangedByUser], '') AS ChangedByUser,isnull([ChangedOnDate], '') AS ChangedOnDate,isnull([SystemUserRole], '') AS SystemUserRole from SystemUser (NOLOCK) where SubscriberID = " + SubscriberID;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);
            ObjConstring.Close();
            return ds;
        }

        public List<EntityDataModels.SystemUser> GetSystemUserRecord(string conString, string whereclause)
        {
            List<EntityDataModels.SystemUser> objSystemUserList = new List<EntityDataModels.SystemUser>();
            EntityDataModels.SystemUser objSystemUser = new EntityDataModels.SystemUser();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "select * from Subscriber " + whereclause;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSystemUser.SubscriberID = Convert.ToInt32(Dr["SubscriberID"]);
                objSystemUser.ActiveYN = bool.Parse(Dr["ActiveYN"].ToString());
                objSystemUser.CellularCode = Dr["CellularCode"].ToString();
                objSystemUser.CellularNo = Dr["CellularNo"].ToString();
                objSystemUser.ChangedByUser = Dr["ChangedByUser"].ToString();
                objSystemUser.ChangedOnDate = DateTime.Parse(Dr["ChangedOnDate"].ToString());
                objSystemUser.CreatedByUser = Dr["CreatedByUser"].ToString();
                objSystemUser.CreatedOnDate = DateTime.Parse(Dr["CreatedOnDate"].ToString());
                objSystemUser.EmailAddress = Dr["EmailAddress"].ToString();
                objSystemUser.FirstName = Dr["FirstName"].ToString();
                objSystemUser.IDNo = Dr["IDNo"].ToString();
                objSystemUser.JobPosition = Dr["JobPosition"].ToString();
                objSystemUser.Surname = Dr["Surname"].ToString();

                objSystemUser.SystemUserID = int.Parse(Dr["SystemUserID"].ToString());
                objSystemUser.SystemUserRole = Dr["SystemUserRole"].ToString();
                objSystemUser.SystemUserTypeInd = Dr["SystemUserTypeInd"].ToString();
                objSystemUser.Username = Dr["Username"].ToString();
                objSystemUserList.Add(objSystemUser);
            }
            objsqladp.Dispose();
            Objsqlcom.Dispose();
            ds.Dispose();
            ObjConstring.Close();
            return objSystemUserList;
        }
    }
}
