﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace XDSServiceMaster.DataSources
{
    public static class DBConnections
    {
        public static SqlConnection AdminConnectString
        {
            get
            {
                return new SqlConnection(Properties.Settings.Default.AdminConnectString);
            }
        }
    }
}
