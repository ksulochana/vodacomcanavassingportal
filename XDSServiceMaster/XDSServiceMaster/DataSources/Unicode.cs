﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.InteropServices;


namespace XDSServiceMaster.DataSources
{
    public class Unicode
    {
        [DllImport("ZUTruTrade.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        static extern IntPtr casequery([MarshalAs(UnmanagedType.LPWStr)] string refer,
                                       [MarshalAs(UnmanagedType.LPWStr)] string make,
                                       [MarshalAs(UnmanagedType.LPWStr)] string vin,
                                       [MarshalAs(UnmanagedType.LPWStr)] string eng,
                                       [MarshalAs(UnmanagedType.LPWStr)] string reg,
                                       [MarshalAs(UnmanagedType.LPWStr)] string enqtype);

        public Output GetVehicleData(Input oInput)
        {
            try
            {
                IntPtr result = casequery(oInput.Reference, oInput.Make, oInput.VinNumber, oInput.EngineNumber, oInput.RegistrationNumber, oInput.EnquiryType);
                string outstring = Marshal.PtrToStringUni(result);

                Output oOutput = new Output();
                oOutput.Result = outstring;
                oOutput.IsSuccess = true;
                oOutput.ReasonPhrase = "Successful";

                return oOutput;
            }
            catch (Exception exp)
            {
                //Console.WriteLine(exp.Message);
         
                  Output oOutput = new Output();
                  oOutput.Result = exp.Message;
                  oOutput.IsSuccess = false;
                  oOutput.ReasonPhrase = "Exception";

                  return oOutput;
            }
        }

       public partial class Input
        {
            
            public string Reference { get; set; }
            
            public string Make { get; set; }
            
            public string VinNumber { get; set; }
            
            public string EngineNumber { get; set; }
            
            public string RegistrationNumber { get; set; }
            
            public string EnquiryType { get; set; }            
           
        }

       public partial class Output
       {
           private bool sIsSuccess = false;
           private string sResult;
           private string sReason;

           public bool IsSuccess
           {
               get { return sIsSuccess; }
               set { sIsSuccess = value; }
           }

           public string Result
           {
               get { return sResult; }
               set { sResult = value; }
           }

           public string ReasonPhrase
           {
               get { return sReason; }
               set { sReason = value; }
           }
       }
       
    }
}
