﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Xml.Serialization;
using XDSServiceMaster.Utilities;


namespace XDSServiceMaster.DataSources
{
    public class TransUnion
    {
        public async Task<Output> GetVehicleData(Input oInput)
        {
            try
            {
                using (var client = new HttpClient())
                {

                    //client.BaseAddress = new Uri("https://secureuat.decisionsystems.co.za/ReportApi/apireport/hpireports?format=json"); //ReportApi/
                    client.BaseAddress = new Uri("https://secureuat.decisionsystems.co.za/ReportApi/soap12");

                    string ClientHashValue = HashOperations.ComputeHashValue(oInput, "Cli3ntH@sah");

                    //Make sure request headers are used with HttpRequestMessage, response headers with HttpResponseMessage, and content headers with HttpContent objects.
                    client.DefaultRequestHeaders.Add("request-hash", ClientHashValue);
                    client.DefaultRequestHeaders.Add("Host", "secureuat.decisionsystems.co.za");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/soap+xml"));
                    //client.DefaultRequestHeaders.Add("Content-Type", "application/json");
                    //client.DefaultRequestHeaders.Add("apikey", "Cli3ntH@sah");
                    //client.DefaultRequestHeaders.Add("reportcode", "Cli3ntH@sah");

                    //Seialize Input to JSON
                    //MemoryStream stream1 = new MemoryStream();
                    //DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Input));
                    //ser.WriteObject(stream1, oInput);
                    //stream1.Position = 0;
                    //StreamReader sr = new StreamReader(stream1);
                    //string sInput = sr.ReadToEnd();

                    //Seialize Input to XML
                    MemoryStream stream1 = new MemoryStream();
                    XmlSerializer ser = new XmlSerializer(typeof(Input));
                    ser.Serialize(stream1, oInput);
                    stream1.Position = 0;
                    StreamReader sr = new StreamReader(stream1);
                    string sInput = sr.ReadToEnd();

                    
                    //var postData = new List<KeyValuePair<string, string>>();
                    //postData.Add(new KeyValuePair<string, string>("ApiKey", "C4EA646D-7C82-48D5-8D29-3F4518A4F805"));
                    //postData.Add(new KeyValuePair<string, string>("ReportCode", "9849A63B-FC07-4BBD-B1D9-3BFAF6B2EE21"));
                    //postData.Add(new KeyValuePair<string, string>("Input", sInput));

                    //HttpContent content = new FormUrlEncodedContent(postData);
                     
                    var content = new StringContent(HashOperations.BuildRequestBody(sInput), Encoding.UTF8, "application/soap+xml");

                    HttpResponseMessage response = await client.PostAsync(client.BaseAddress, content);

                    Output oOutput = new Output();
                    oOutput.IsSuccess = response.IsSuccessStatusCode;
                    oOutput.Result = response.Content.ReadAsStringAsync().Result;
                    oOutput.ReasonPhrase = response.ReasonPhrase;

                    return oOutput;

                    //    //ListOfProducts = await jsonText..ReadAsAsync<ListOfProducts>();

                }
            }
            catch (Exception ex)
            {
                Output oOutput = new Output();
                oOutput.Result = ex.Message;
                oOutput.ReasonPhrase = "Exception";
                return oOutput;
            }
        }

        [DataContract(Namespace = "")]
        public partial class Input
        {
            [DataMember(EmitDefaultValue = false)]
            public string SubscriptionUsername { get; set; }
            [DataMember(EmitDefaultValue = false)]
            public string SubscriptionPassword { get; set; }
            [DataMember(EmitDefaultValue = false)]
            public string VehicleVinNumber { get; set; }
            [DataMember(EmitDefaultValue = false)]
            public string VehicleColour { get; set; }
            [DataMember(EmitDefaultValue = false)]
            public string VehicleMMCode { get; set; }
            [DataMember(EmitDefaultValue = false)]
            public string VehicleEngineNumber { get; set; }
            [DataMember(EmitDefaultValue = false)]
            public string VehicleManufactureYear { get; set; }
            [DataMember(EmitDefaultValue = false)]
            public string ClientReference { get; set; }
            [DataMember(EmitDefaultValue = false)]
            public string ConsumerCellularNumber { get; set; }
            [DataMember(EmitDefaultValue = false)]
            public string RequestorPerson { get; set; }
            [DataMember(EmitDefaultValue = false)]
            public string VehicleRegistrationNumber { get; set; }
            [DataMember(EmitDefaultValue = false)]
            public string ManufactureCode { get; set; }
            [DataMember(EmitDefaultValue = false)]
            public int GuideYear { get; set; }
            [DataMember(EmitDefaultValue = false)]
            public int GuideMonth { get; set; }
            [DataMember(EmitDefaultValue = false)]
            public string Condition { get; set; }
            [DataMember(EmitDefaultValue = false)]
            public int VehicleMileage { get; set; }
            [DataMember(EmitDefaultValue = false)]
            public string OptionCodeUsed { get; set; }
            [DataMember(EmitDefaultValue = false)]
            public string OptionCodeNew { get; set; }
        }

        public partial class Output
        {
            private bool sIsSuccess = false;
            private string sResult;
            private string sReason;

            public bool IsSuccess
            {
                get { return sIsSuccess; }
                set { sIsSuccess = value; }
            }

            public string Result
            {
                get { return sResult; }
                set { sResult = value; }
            }

            public string ReasonPhrase
            {
                get { return sReason; }
                set { sReason = value; }
            }
        }
       

    }

    
}
