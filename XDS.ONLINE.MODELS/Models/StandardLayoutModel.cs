﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XDS.ONLINE.MODELS
{
    public class StandardLayoutModel
    {
        public SortedList<string,string> FinalList;

        public string sHeading;

        public StandardLayoutModel BuildLayout(string Heading, SortedList<string, string> Fieldlist)
        {
            this.sHeading = Heading;
            this.FinalList = new SortedList<string,string>();
            foreach (var item in Fieldlist)
            {
                FinalList.Add(item.Key, item.Value);
            }
            return this;
        }
    }
}