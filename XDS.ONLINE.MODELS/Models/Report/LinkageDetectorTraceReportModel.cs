﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace XDS.ONLINE.MODELS
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class Linkage
    {

        private LinkageReportInformation reportInformationField;

        private ConsumerSubscriberInputDetails subscriberInputDetailsField;

        private ConsumerSubscriberInputDetails subscriberInputDetailsOtherField;

        private ConsumerConsumerConfirmedLinkage[] confirmedLinkagesField;

        private LinkageConsumer consumerField;

        private LinkageConsumerOther consumerOtherField;

        /// <remarks/>
        public LinkageReportInformation ReportInformation
        {
            get
            {
                return this.reportInformationField;
            }
            set
            {
                this.reportInformationField = value;
            }
        }

        /// <remarks/>
        public ConsumerSubscriberInputDetails SubscriberInputDetails
        {
            get
            {
                return this.subscriberInputDetailsField;
            }
            set
            {
                this.subscriberInputDetailsField = value;
            }
        }

        /// <remarks/>
        public ConsumerSubscriberInputDetails SubscriberInputDetailsOther
        {
            get
            {
                return this.subscriberInputDetailsOtherField;
            }
            set
            {
                this.subscriberInputDetailsOtherField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("ConfirmedLinkage", IsNullable = false)]
        public ConsumerConsumerConfirmedLinkage[] ConfirmedLinkages
        {
            get
            {
                return this.confirmedLinkagesField;
            }
            set
            {
                this.confirmedLinkagesField = value;
            }
        }

        /// <remarks/>
        public LinkageConsumer Consumer
        {
            get
            {
                return this.consumerField;
            }
            set
            {
                this.consumerField = value;
            }
        }

        /// <remarks/>
        public LinkageConsumerOther ConsumerOther
        {
            get
            {
                return this.consumerOtherField;
            }
            set
            {
                this.consumerOtherField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class LinkageReportInformation
    {

        private string reportIDField;

        private string reportNameField;

        /// <remarks/>
        public string ReportID
        {
            get
            {
                return this.reportIDField;
            }
            set
            {
                this.reportIDField = value;
            }
        }

        /// <remarks/>
        public string ReportName
        {
            get
            {
                return this.reportNameField;
            }
            set
            {
                this.reportNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class LinkageSubscriberInputDetails
    {

        private string enquiryDateField;

        private string enquiryTypeField;

        private string subscriberNameField;

        private string subscriberUserNameField;

        private string enquiryInputField;

        private string enquiryReasonField;

        /// <remarks/>
        public string EnquiryDate
        {
            get
            {
                return this.enquiryDateField;
            }
            set
            {
                this.enquiryDateField = value;
            }
        }

        /// <remarks/>
        public string EnquiryType
        {
            get
            {
                return this.enquiryTypeField;
            }
            set
            {
                this.enquiryTypeField = value;
            }
        }

        /// <remarks/>
        public string SubscriberName
        {
            get
            {
                return this.subscriberNameField;
            }
            set
            {
                this.subscriberNameField = value;
            }
        }

        /// <remarks/>
        public string SubscriberUserName
        {
            get
            {
                return this.subscriberUserNameField;
            }
            set
            {
                this.subscriberUserNameField = value;
            }
        }

        /// <remarks/>
        public string EnquiryInput
        {
            get
            {
                return this.enquiryInputField;
            }
            set
            {
                this.enquiryInputField = value;
            }
        }

        /// <remarks/>
        public string EnquiryReason
        {
            get
            {
                return this.enquiryReasonField;
            }
            set
            {
                this.enquiryReasonField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class LinkageSubscriberInputDetailsOther
    {

        private string enquiryDateField;

        private string enquiryTypeField;

        private string subscriberNameField;

        private string subscriberUserNameField;

        private string enquiryInputField;

        private string enquiryReasonField;

        /// <remarks/>
        public string EnquiryDate
        {
            get
            {
                return this.enquiryDateField;
            }
            set
            {
                this.enquiryDateField = value;
            }
        }

        /// <remarks/>
        public string EnquiryType
        {
            get
            {
                return this.enquiryTypeField;
            }
            set
            {
                this.enquiryTypeField = value;
            }
        }

        /// <remarks/>
        public string SubscriberName
        {
            get
            {
                return this.subscriberNameField;
            }
            set
            {
                this.subscriberNameField = value;
            }
        }

        /// <remarks/>
        public string SubscriberUserName
        {
            get
            {
                return this.subscriberUserNameField;
            }
            set
            {
                this.subscriberUserNameField = value;
            }
        }

        /// <remarks/>
        public string EnquiryInput
        {
            get
            {
                return this.enquiryInputField;
            }
            set
            {
                this.enquiryInputField = value;
            }
        }

        /// <remarks/>
        public string EnquiryReason
        {
            get
            {
                return this.enquiryReasonField;
            }
            set
            {
                this.enquiryReasonField = value;
            }
        }
    }

      /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class LinkageConsumer
    {

        private ConsumerConsumerDetail consumerDetailField;

        private ConsumerConsumerAddressHistory[] consumerAddressHistoryField;

        private ConsumerConsumerTelephoneHistory[] consumerTelephoneHistoryField;

        private ConsumerConsumerEmploymentHistory[] consumerEmploymentHistoryField;

        /// <remarks/>
        public ConsumerConsumerDetail ConsumerDetail
        {
            get
            {
                return this.consumerDetailField;
            }
            set
            {
                this.consumerDetailField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ConsumerAddressHistory")]
        public ConsumerConsumerAddressHistory[] ConsumerAddressHistory
        {
            get
            {
                return this.consumerAddressHistoryField;
            }
            set
            {
                this.consumerAddressHistoryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ConsumerTelephoneHistory")]
        public ConsumerConsumerTelephoneHistory[] ConsumerTelephoneHistory
        {
            get
            {
                return this.consumerTelephoneHistoryField;
            }
            set
            {
                this.consumerTelephoneHistoryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ConsumerEmploymentHistory")]
        public ConsumerConsumerEmploymentHistory[] ConsumerEmploymentHistory
        {
            get
            {
                return this.consumerEmploymentHistoryField;
            }
            set
            {
                this.consumerEmploymentHistoryField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class LinkageConsumerConsumerDetail
    {

        private string displayTextField;

        private string consumerIDField;

        private string initialsField;

        private string firstNameField;

        private string secondNameField;

        private string thirdNameField;

        private string surnameField;

        private string iDNoField;

        private string passportNoField;

        private string birthDateField;

        private string genderField;

        private string titleDescField;

        private string maritalStatusDescField;

        private string privacyStatusField;

        private string residentialAddressField;

        private string postalAddressField;

        private string homeTelephoneNoField;

        private string workTelephoneNoField;

        private string cellularNoField;

        private string emailAddressField;

        private string employerDetailField;

        private string referenceNoField;

        private string externalReferenceField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string ConsumerID
        {
            get
            {
                return this.consumerIDField;
            }
            set
            {
                this.consumerIDField = value;
            }
        }

        /// <remarks/>
        public string Initials
        {
            get
            {
                return this.initialsField;
            }
            set
            {
                this.initialsField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string SecondName
        {
            get
            {
                return this.secondNameField;
            }
            set
            {
                this.secondNameField = value;
            }
        }

        /// <remarks/>
        public string ThirdName
        {
            get
            {
                return this.thirdNameField;
            }
            set
            {
                this.thirdNameField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string IDNo
        {
            get
            {
                return this.iDNoField;
            }
            set
            {
                this.iDNoField = value;
            }
        }

        /// <remarks/>
        public string PassportNo
        {
            get
            {
                return this.passportNoField;
            }
            set
            {
                this.passportNoField = value;
            }
        }

        /// <remarks/>
        public string BirthDate
        {
            get
            {
                return this.birthDateField;
            }
            set
            {
                this.birthDateField = value;
            }
        }

        /// <remarks/>
        public string Gender
        {
            get
            {
                return this.genderField;
            }
            set
            {
                this.genderField = value;
            }
        }

        /// <remarks/>
        public string TitleDesc
        {
            get
            {
                return this.titleDescField;
            }
            set
            {
                this.titleDescField = value;
            }
        }

        /// <remarks/>
        public string MaritalStatusDesc
        {
            get
            {
                return this.maritalStatusDescField;
            }
            set
            {
                this.maritalStatusDescField = value;
            }
        }

        /// <remarks/>
        public string PrivacyStatus
        {
            get
            {
                return this.privacyStatusField;
            }
            set
            {
                this.privacyStatusField = value;
            }
        }

        /// <remarks/>
        public string ResidentialAddress
        {
            get
            {
                return this.residentialAddressField;
            }
            set
            {
                this.residentialAddressField = value;
            }
        }

        /// <remarks/>
        public string PostalAddress
        {
            get
            {
                return this.postalAddressField;
            }
            set
            {
                this.postalAddressField = value;
            }
        }

        /// <remarks/>
        public string HomeTelephoneNo
        {
            get
            {
                return this.homeTelephoneNoField;
            }
            set
            {
                this.homeTelephoneNoField = value;
            }
        }

        /// <remarks/>
        public string WorkTelephoneNo
        {
            get
            {
                return this.workTelephoneNoField;
            }
            set
            {
                this.workTelephoneNoField = value;
            }
        }

        /// <remarks/>
        public string CellularNo
        {
            get
            {
                return this.cellularNoField;
            }
            set
            {
                this.cellularNoField = value;
            }
        }

        /// <remarks/>
        public string EmailAddress
        {
            get
            {
                return this.emailAddressField;
            }
            set
            {
                this.emailAddressField = value;
            }
        }

        /// <remarks/>
        public string EmployerDetail
        {
            get
            {
                return this.employerDetailField;
            }
            set
            {
                this.employerDetailField = value;
            }
        }

        /// <remarks/>
        public string ReferenceNo
        {
            get
            {
                return this.referenceNoField;
            }
            set
            {
                this.referenceNoField = value;
            }
        }

        /// <remarks/>
        public string ExternalReference
        {
            get
            {
                return this.externalReferenceField;
            }
            set
            {
                this.externalReferenceField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class LinkageConsumerConsumerAddressHistory
    {

        private string displayTextField;

        private string consumerAddressIDField;

        private string addressTypeField;

        private string addressTypeIndField;

        private string address1Field;

        private string address2Field;

        private string address3Field;

        private string address4Field;

        private string postalCodeField;

        private string addressField;

        private string lastUpdatedDateField;

        private string firstReportedDateField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string ConsumerAddressID
        {
            get
            {
                return this.consumerAddressIDField;
            }
            set
            {
                this.consumerAddressIDField = value;
            }
        }

        /// <remarks/>
        public string AddressType
        {
            get
            {
                return this.addressTypeField;
            }
            set
            {
                this.addressTypeField = value;
            }
        }

        /// <remarks/>
        public string AddressTypeInd
        {
            get
            {
                return this.addressTypeIndField;
            }
            set
            {
                this.addressTypeIndField = value;
            }
        }

        /// <remarks/>
        public string Address1
        {
            get
            {
                return this.address1Field;
            }
            set
            {
                this.address1Field = value;
            }
        }

        /// <remarks/>
        public string Address2
        {
            get
            {
                return this.address2Field;
            }
            set
            {
                this.address2Field = value;
            }
        }

        /// <remarks/>
        public string Address3
        {
            get
            {
                return this.address3Field;
            }
            set
            {
                this.address3Field = value;
            }
        }

        /// <remarks/>
        public string Address4
        {
            get
            {
                return this.address4Field;
            }
            set
            {
                this.address4Field = value;
            }
        }

        /// <remarks/>
        public string PostalCode
        {
            get
            {
                return this.postalCodeField;
            }
            set
            {
                this.postalCodeField = value;
            }
        }

        /// <remarks/>
        public string Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>

        public string LastUpdatedDate
        {
            get
            {
                return this.lastUpdatedDateField;
            }
            set
            {
                this.lastUpdatedDateField = value;
            }
        }

        /// <remarks/>

        public string FirstReportedDate
        {
            get
            {
                return this.firstReportedDateField;
            }
            set
            {
                this.firstReportedDateField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class LinkageConsumerConsumerTelephoneHistory
    {

        private string displayTextField;

        private string consumerTelephoneIDField;

        private string telephoneTypeField;

        private string telephoneTypeIndField;

        private string telCodeField;

        private string telNoField;

        private string telephoneNoField;

        private string emailAddressField;

        private string lastUpdatedDateField;

        private string firstReportedDateField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string ConsumerTelephoneID
        {
            get
            {
                return this.consumerTelephoneIDField;
            }
            set
            {
                this.consumerTelephoneIDField = value;
            }
        }

        /// <remarks/>
        public string TelephoneType
        {
            get
            {
                return this.telephoneTypeField;
            }
            set
            {
                this.telephoneTypeField = value;
            }
        }

        /// <remarks/>
        public string TelephoneTypeInd
        {
            get
            {
                return this.telephoneTypeIndField;
            }
            set
            {
                this.telephoneTypeIndField = value;
            }
        }

        /// <remarks/>
        public string TelCode
        {
            get
            {
                return this.telCodeField;
            }
            set
            {
                this.telCodeField = value;
            }
        }

        /// <remarks/>
        public string TelNo
        {
            get
            {
                return this.telNoField;
            }
            set
            {
                this.telNoField = value;
            }
        }

        /// <remarks/>
        public string TelephoneNo
        {
            get
            {
                return this.telephoneNoField;
            }
            set
            {
                this.telephoneNoField = value;
            }
        }

        /// <remarks/>
        public string EmailAddress
        {
            get
            {
                return this.emailAddressField;
            }
            set
            {
                this.emailAddressField = value;
            }
        }

        /// <remarks/>

        public string LastUpdatedDate
        {
            get
            {
                return this.lastUpdatedDateField;
            }
            set
            {
                this.lastUpdatedDateField = value;
            }
        }

        /// <remarks/>

        public string FirstReportedDate
        {
            get
            {
                return this.firstReportedDateField;
            }
            set
            {
                this.firstReportedDateField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class LinkageConsumerConsumerEmploymentHistory
    {

        private string displayTextField;

        private string employerDetailField;

        private string designationField;

        private string lastUpdatedDateField;

        private string firstReportedDateField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string EmployerDetail
        {
            get
            {
                return this.employerDetailField;
            }
            set
            {
                this.employerDetailField = value;
            }
        }

        /// <remarks/>
        public string Designation
        {
            get
            {
                return this.designationField;
            }
            set
            {
                this.designationField = value;
            }
        }

        /// <remarks/>

        public string LastUpdatedDate
        {
            get
            {
                return this.lastUpdatedDateField;
            }
            set
            {
                this.lastUpdatedDateField = value;
            }
        }

        /// <remarks/>

        public string FirstReportedDate
        {
            get
            {
                return this.firstReportedDateField;
            }
            set
            {
                this.firstReportedDateField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class LinkageConsumerOther
    {

        private ConsumerConsumerDetail consumerDetailField;

        private ConsumerConsumerAddressHistory[] consumerAddressHistoryField;

        private ConsumerConsumerTelephoneHistory[] consumerTelephoneHistoryField;

        private ConsumerConsumerEmploymentHistory[] consumerEmploymentHistoryField;

        /// <remarks/>
        public ConsumerConsumerDetail ConsumerDetail
        {
            get
            {
                return this.consumerDetailField;
            }
            set
            {
                this.consumerDetailField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ConsumerAddressHistory")]
        public ConsumerConsumerAddressHistory[] ConsumerAddressHistory
        {
            get
            {
                return this.consumerAddressHistoryField;
            }
            set
            {
                this.consumerAddressHistoryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ConsumerTelephoneHistory")]
        public ConsumerConsumerTelephoneHistory[] ConsumerTelephoneHistory
        {
            get
            {
                return this.consumerTelephoneHistoryField;
            }
            set
            {
                this.consumerTelephoneHistoryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ConsumerEmploymentHistory")]
        public ConsumerConsumerEmploymentHistory[] ConsumerEmploymentHistory
        {
            get
            {
                return this.consumerEmploymentHistoryField;
            }
            set
            {
                this.consumerEmploymentHistoryField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class LinkageConsumerOtherConsumerDetail
    {

        private string displayTextField;

        private string consumerIDField;

        private string initialsField;

        private string firstNameField;

        private string secondNameField;

        private string thirdNameField;

        private string surnameField;

        private string iDNoField;

        private string passportNoField;

        private string birthDateField;

        private string genderField;

        private string titleDescField;

        private string maritalStatusDescField;

        private string privacyStatusField;

        private string residentialAddressField;

        private string postalAddressField;

        private string homeTelephoneNoField;

        private string workTelephoneNoField;

        private string cellularNoField;

        private string emailAddressField;

        private string employerDetailField;

        private string referenceNoField;

        private string externalReferenceField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string ConsumerID
        {
            get
            {
                return this.consumerIDField;
            }
            set
            {
                this.consumerIDField = value;
            }
        }

        /// <remarks/>
        public string Initials
        {
            get
            {
                return this.initialsField;
            }
            set
            {
                this.initialsField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string SecondName
        {
            get
            {
                return this.secondNameField;
            }
            set
            {
                this.secondNameField = value;
            }
        }

        /// <remarks/>
        public string ThirdName
        {
            get
            {
                return this.thirdNameField;
            }
            set
            {
                this.thirdNameField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string IDNo
        {
            get
            {
                return this.iDNoField;
            }
            set
            {
                this.iDNoField = value;
            }
        }

        /// <remarks/>
        public string PassportNo
        {
            get
            {
                return this.passportNoField;
            }
            set
            {
                this.passportNoField = value;
            }
        }

        /// <remarks/>

        public string BirthDate
        {
            get
            {
                return this.birthDateField;
            }
            set
            {
                this.birthDateField = value;
            }
        }

        /// <remarks/>
        public string Gender
        {
            get
            {
                return this.genderField;
            }
            set
            {
                this.genderField = value;
            }
        }

        /// <remarks/>
        public string TitleDesc
        {
            get
            {
                return this.titleDescField;
            }
            set
            {
                this.titleDescField = value;
            }
        }

        /// <remarks/>
        public string MaritalStatusDesc
        {
            get
            {
                return this.maritalStatusDescField;
            }
            set
            {
                this.maritalStatusDescField = value;
            }
        }

        /// <remarks/>
        public string PrivacyStatus
        {
            get
            {
                return this.privacyStatusField;
            }
            set
            {
                this.privacyStatusField = value;
            }
        }

        /// <remarks/>
        public string ResidentialAddress
        {
            get
            {
                return this.residentialAddressField;
            }
            set
            {
                this.residentialAddressField = value;
            }
        }

        /// <remarks/>
        public string PostalAddress
        {
            get
            {
                return this.postalAddressField;
            }
            set
            {
                this.postalAddressField = value;
            }
        }

        /// <remarks/>
        public string HomeTelephoneNo
        {
            get
            {
                return this.homeTelephoneNoField;
            }
            set
            {
                this.homeTelephoneNoField = value;
            }
        }

        /// <remarks/>
        public string WorkTelephoneNo
        {
            get
            {
                return this.workTelephoneNoField;
            }
            set
            {
                this.workTelephoneNoField = value;
            }
        }

        /// <remarks/>
        public string CellularNo
        {
            get
            {
                return this.cellularNoField;
            }
            set
            {
                this.cellularNoField = value;
            }
        }

        /// <remarks/>
        public string EmailAddress
        {
            get
            {
                return this.emailAddressField;
            }
            set
            {
                this.emailAddressField = value;
            }
        }

        /// <remarks/>
        public string EmployerDetail
        {
            get
            {
                return this.employerDetailField;
            }
            set
            {
                this.employerDetailField = value;
            }
        }

        /// <remarks/>
        public string ReferenceNo
        {
            get
            {
                return this.referenceNoField;
            }
            set
            {
                this.referenceNoField = value;
            }
        }

        /// <remarks/>
        public string ExternalReference
        {
            get
            {
                return this.externalReferenceField;
            }
            set
            {
                this.externalReferenceField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class LinkageConsumerOtherConsumerAddressHistory
    {

        private string displayTextField;

        private string consumerAddressIDField;

        private string addressTypeField;

        private string addressTypeIndField;

        private string address1Field;

        private string address2Field;

        private string address3Field;

        private string address4Field;

        private string postalCodeField;

        private string addressField;

        private string lastUpdatedDateField;

        private string firstReportedDateField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string ConsumerAddressID
        {
            get
            {
                return this.consumerAddressIDField;
            }
            set
            {
                this.consumerAddressIDField = value;
            }
        }

        /// <remarks/>
        public string AddressType
        {
            get
            {
                return this.addressTypeField;
            }
            set
            {
                this.addressTypeField = value;
            }
        }

        /// <remarks/>
        public string AddressTypeInd
        {
            get
            {
                return this.addressTypeIndField;
            }
            set
            {
                this.addressTypeIndField = value;
            }
        }

        /// <remarks/>
        public string Address1
        {
            get
            {
                return this.address1Field;
            }
            set
            {
                this.address1Field = value;
            }
        }

        /// <remarks/>
        public string Address2
        {
            get
            {
                return this.address2Field;
            }
            set
            {
                this.address2Field = value;
            }
        }

        /// <remarks/>
        public string Address3
        {
            get
            {
                return this.address3Field;
            }
            set
            {
                this.address3Field = value;
            }
        }

        /// <remarks/>
        public string Address4
        {
            get
            {
                return this.address4Field;
            }
            set
            {
                this.address4Field = value;
            }
        }

        /// <remarks/>
        public string PostalCode
        {
            get
            {
                return this.postalCodeField;
            }
            set
            {
                this.postalCodeField = value;
            }
        }

        /// <remarks/>
        public string Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>

        public string LastUpdatedDate
        {
            get
            {
                return this.lastUpdatedDateField;
            }
            set
            {
                this.lastUpdatedDateField = value;
            }
        }

        /// <remarks/>

        public string FirstReportedDate
        {
            get
            {
                return this.firstReportedDateField;
            }
            set
            {
                this.firstReportedDateField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class LinkageConsumerOtherConsumerTelephoneHistory
    {

        private string displayTextField;

        private string consumerTelephoneIDField;

        private string telephoneTypeField;

        private string telephoneTypeIndField;

        private string telCodeField;

        private string telNoField;

        private string telephoneNoField;

        private string emailAddressField;

        private string lastUpdatedDateField;

        private string firstReportedDateField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string ConsumerTelephoneID
        {
            get
            {
                return this.consumerTelephoneIDField;
            }
            set
            {
                this.consumerTelephoneIDField = value;
            }
        }

        /// <remarks/>
        public string TelephoneType
        {
            get
            {
                return this.telephoneTypeField;
            }
            set
            {
                this.telephoneTypeField = value;
            }
        }

        /// <remarks/>
        public string TelephoneTypeInd
        {
            get
            {
                return this.telephoneTypeIndField;
            }
            set
            {
                this.telephoneTypeIndField = value;
            }
        }

        /// <remarks/>
        public string TelCode
        {
            get
            {
                return this.telCodeField;
            }
            set
            {
                this.telCodeField = value;
            }
        }

        /// <remarks/>
        public string TelNo
        {
            get
            {
                return this.telNoField;
            }
            set
            {
                this.telNoField = value;
            }
        }

        /// <remarks/>
        public string TelephoneNo
        {
            get
            {
                return this.telephoneNoField;
            }
            set
            {
                this.telephoneNoField = value;
            }
        }

        /// <remarks/>
        public string EmailAddress
        {
            get
            {
                return this.emailAddressField;
            }
            set
            {
                this.emailAddressField = value;
            }
        }

        /// <remarks/>

        public string LastUpdatedDate
        {
            get
            {
                return this.lastUpdatedDateField;
            }
            set
            {
                this.lastUpdatedDateField = value;
            }
        }

        /// <remarks/>

        public string FirstReportedDate
        {
            get
            {
                return this.firstReportedDateField;
            }
            set
            {
                this.firstReportedDateField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class LinkageConsumerOtherConsumerEmploymentHistory
    {

        private string displayTextField;

        private string employerDetailField;

        private string designationField;

        private string lastUpdatedDateField;

        private string firstReportedDateField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string EmployerDetail
        {
            get
            {
                return this.employerDetailField;
            }
            set
            {
                this.employerDetailField = value;
            }
        }

        /// <remarks/>
        public string Designation
        {
            get
            {
                return this.designationField;
            }
            set
            {
                this.designationField = value;
            }
        }

        /// <remarks/>

        public string LastUpdatedDate
        {
            get
            {
                return this.lastUpdatedDateField;
            }
            set
            {
                this.lastUpdatedDateField = value;
            }
        }

        /// <remarks/>

        public string FirstReportedDate
        {
            get
            {
                return this.firstReportedDateField;
            }
            set
            {
                this.firstReportedDateField = value;
            }
        }
    }

}