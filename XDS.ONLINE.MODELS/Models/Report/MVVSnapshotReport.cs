﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace XDS.ONLINE.MODELS
{
    [System.Xml.Serialization.XmlRoot("Consumer")]
    public partial class MVVSnapshotReport
    {
            private ConsumerReportInformation reportInformationField;

        private ConsumerConsumerDetail consumerDetailField;

        private ConsumerConsumerScoring consumerScoringField;

        private ConsumerConsumerMaritalStatusEnquiry consumerMaritalStatusEnquiryField;

        private ConsumerConsumerSequestration[] consumerSequestrationField;

        private ConsumerConsumerDefaultAlert[] consumerDefaultAlertField;

        private ConsumerConsumerDebtReviewStatus consumerDebtReviewStatusField;

        private ConsumerConsumerDirectorShipLink[] consumerDirectorShipLinkField;

        private ConsumerConsumerPropertyInformation[] consumerPropertyInformationField;

        private ConsumerConsumerAddressHistory[] consumerAddressHistoryField;

        private ConsumerConsumerTelephoneHistory[] consumerTelephoneHistoryField;

        private ConsumerConsumerEmploymentHistory[] consumerEmploymentHistoryField;

        private ConsumerConsumerTelephoneLinkageHome[] consumerTelephoneLinkageHomeField;

        private ConsumerConsumerTelephoneLinkageWork[] consumerTelephoneLinkageWorkField;

        private ConsumerConsumerTelephoneLinkageCellular[] consumerTelephoneLinkageCellularField;

        private ConsumerConsumerEmailHistory[] consumerEmailHistoryField;

        private ConsumerConsumerAffordability consumerAffordabilityField;

        private ConsumerCollectionConsumerSummary collectionConsumerSummaryField;

        private ConsumerAccountTypeLegend[] accountTypeLegendField;

        private ConsumerConsumerAccountStatus[] consumerAccountStatusField;

        private ConsumerConsumerAccountStatus[] ConsumerVehicleAccountStatusField;

        private ConsumerConsumer24MonthlyPaymentHeader consumer24MonthlyPaymentHeaderField;

        private ConsumerConsumer24MonthlyPayment[] consumer24MonthlyPaymentField;

        private ConsumerConsumerDefinition[] consumerDefinitionField;

        private ConsumerNLRAccountTypeLegend[] nLRAccountTypeLegendField;

        private ConsumerConsumerNLRAccountStatus[] consumerNLRAccountStatusField;

        private ConsumerConsumerNLR24MonthlyPaymentHeader consumerNLR24MonthlyPaymentHeaderField;

        private ConsumerConsumerNLR24MonthlyPayment[] consumerNLR24MonthlyPaymentField;

        private ConsumerConsumerNLRDefinition[] consumerNLRDefinitionField;

        private ConsumerConsumerCPANLRDebtSummary consumerCPANLRDebtSummaryField;

        private ConsumerCreditActiveNess[] creditActiveNessField;

        private ConsumerIndustryPayments[] industryPaymentsField;

        private ConsumerPaymentGAP paymentGAPField;

        private ConsumerSubscriberInputDetails subscriberInputDetailsField;

        private ConsumerConsumerAdverseInformation[] consumerAdverseInformationField;
        private ConsumerConsumerJudgement[] consumerJudgementField;
        private ConsumerConsumerAdminOrder[] consumerAdminOrderField;
        private ConsumerConsumerRehabilitationOrder[] consumerRehabilitationOrderField;
        private ConsumerXDSPaymentNotification[] xDSPaymentNotificationField;

        private ConsumerConsumerAddressConfirmation[] consumerAddressConfirmationField;
        private ConsumerConsumerContactConfirmation[] consumerContactConfirmationField;

            private ConsumerVehicleInformation vehicleInformationField;

            private ConsumerSAPInformation sAPInformationField;

            private ConsumerTradeInformation tradeInformationField;

            /// <remarks/>
            public ConsumerReportInformation ReportInformation
            {
                get
                {
                    return this.reportInformationField;
                }
                set
                {
                    this.reportInformationField = value;
                }
            }

            /// <remarks/>
            public ConsumerConsumerDetail ConsumerDetail
            {
                get
                {
                    return this.consumerDetailField;
                }
                set
                {
                    this.consumerDetailField = value;
                }
            }

            /// <remarks/>
            public ConsumerConsumerMaritalStatusEnquiry ConsumerMaritalStatusEnquiry
             {
                get
                {
                    return this.consumerMaritalStatusEnquiryField;
                }
                set
                {
                    this.consumerMaritalStatusEnquiryField = value;
                }
            }

        
         /// <remarks/>
            public ConsumerConsumerScoring ConsumerScoring
            {
                get
                {
                    return this.consumerScoringField;
                }
                set
                {
                    this.consumerScoringField = value;
                }
            }
        

            /// <remarks/>
            public ConsumerConsumerSequestration[] ConsumerSequestration
            {
                get
                {
                    return this.consumerSequestrationField;
                }
                set
                {
                    this.consumerSequestrationField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("ConsumerDefaultAlert")]
            public ConsumerConsumerDefaultAlert[] ConsumerDefaultAlert
            {
                get
                {
                    return this.consumerDefaultAlertField;
                }
                set
                {
                    this.consumerDefaultAlertField = value;
                }
            }

            /// <remarks/>
            public ConsumerConsumerDebtReviewStatus ConsumerDebtReviewStatus
            {
                get
                {
                    return this.consumerDebtReviewStatusField;
                }
                set
                {
                    this.consumerDebtReviewStatusField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("ConsumerDirectorShipLink")]
            public ConsumerConsumerDirectorShipLink[] ConsumerDirectorShipLink
            {
                get
                {
                    return this.consumerDirectorShipLinkField;
                }
                set
                {
                    this.consumerDirectorShipLinkField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("ConsumerPropertyInformation")]
            public ConsumerConsumerPropertyInformation[] ConsumerPropertyInformation
            {
                get
                {
                    return this.consumerPropertyInformationField;
                }
                set
                {
                    this.consumerPropertyInformationField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("ConsumerAddressHistory")]
            public ConsumerConsumerAddressHistory[] ConsumerAddressHistory
            {
                get
                {
                    return this.consumerAddressHistoryField;
                }
                set
                {
                    this.consumerAddressHistoryField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("ConsumerTelephoneHistory")]
            public ConsumerConsumerTelephoneHistory[] ConsumerTelephoneHistory
            {
                get
                {
                    return this.consumerTelephoneHistoryField;
                }
                set
                {
                    this.consumerTelephoneHistoryField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("ConsumerEmploymentHistory")]
            public ConsumerConsumerEmploymentHistory[] ConsumerEmploymentHistory
            {
                get
                {
                    return this.consumerEmploymentHistoryField;
                }
                set
                {
                    this.consumerEmploymentHistoryField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("ConsumerTelephoneLinkageHome")]
            public ConsumerConsumerTelephoneLinkageHome[] ConsumerTelephoneLinkageHome
            {
                get
                {
                    return this.consumerTelephoneLinkageHomeField;
                }
                set
                {
                    this.consumerTelephoneLinkageHomeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("ConsumerTelephoneLinkageWork")]
            public ConsumerConsumerTelephoneLinkageWork[] ConsumerTelephoneLinkageWork
            {
                get
                {
                    return this.consumerTelephoneLinkageWorkField;
                }
                set
                {
                    this.consumerTelephoneLinkageWorkField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("ConsumerTelephoneLinkageCellular")]
            public ConsumerConsumerTelephoneLinkageCellular[] ConsumerTelephoneLinkageCellular
            {
                get
                {
                    return this.consumerTelephoneLinkageCellularField;
                }
                set
                {
                    this.consumerTelephoneLinkageCellularField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("ConsumerEmailHistory")]
            public ConsumerConsumerEmailHistory[] ConsumerEmailHistory
            {
                get
                {
                    return this.consumerEmailHistoryField;
                }
                set
                {
                    this.consumerEmailHistoryField = value;
                }
            }

            /// <remarks/>
            public ConsumerConsumerAffordability ConsumerAffordability
            {
                get
                {
                    return this.consumerAffordabilityField;
                }
                set
                {
                    this.consumerAffordabilityField = value;
                }
            }

            /// <remarks/>
            public ConsumerCollectionConsumerSummary CollectionConsumerSummary
            {
                get
                {
                    return this.collectionConsumerSummaryField;
                }
                set
                {
                    this.collectionConsumerSummaryField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("AccountTypeLegend")]
            public ConsumerAccountTypeLegend[] AccountTypeLegend
            {
                get
                {
                    return this.accountTypeLegendField;
                }
                set
                {
                    this.accountTypeLegendField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("ConsumerAccountStatus")]
            public ConsumerConsumerAccountStatus[] ConsumerAccountStatus
            {
                get
                {
                    return this.consumerAccountStatusField;
                }
                set
                {
                    this.consumerAccountStatusField = value;
                }
            }

            /// <remarks/>
            public ConsumerConsumer24MonthlyPaymentHeader Consumer24MonthlyPaymentHeader
            {
                get
                {
                    return this.consumer24MonthlyPaymentHeaderField;
                }
                set
                {
                    this.consumer24MonthlyPaymentHeaderField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("Consumer24MonthlyPayment")]
            public ConsumerConsumer24MonthlyPayment[] Consumer24MonthlyPayment
            {
                get
                {
                    return this.consumer24MonthlyPaymentField;
                }
                set
                {
                    this.consumer24MonthlyPaymentField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("ConsumerDefinition")]
            public ConsumerConsumerDefinition[] ConsumerDefinition
            {
                get
                {
                    return this.consumerDefinitionField;
                }
                set
                {
                    this.consumerDefinitionField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("NLRAccountTypeLegend")]
            public ConsumerNLRAccountTypeLegend[] NLRAccountTypeLegend
            {
                get
                {
                    return this.nLRAccountTypeLegendField;
                }
                set
                {
                    this.nLRAccountTypeLegendField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("ConsumerNLRAccountStatus")]
            public ConsumerConsumerNLRAccountStatus[] ConsumerNLRAccountStatus
            {
                get
                {
                    return this.consumerNLRAccountStatusField;
                }
                set
                {
                    this.consumerNLRAccountStatusField = value;
                }
            }

            /// <remarks/>
            public ConsumerConsumerNLR24MonthlyPaymentHeader ConsumerNLR24MonthlyPaymentHeader
            {
                get
                {
                    return this.consumerNLR24MonthlyPaymentHeaderField;
                }
                set
                {
                    this.consumerNLR24MonthlyPaymentHeaderField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("ConsumerNLR24MonthlyPayment")]
            public ConsumerConsumerNLR24MonthlyPayment[] ConsumerNLR24MonthlyPayment
            {
                get
                {
                    return this.consumerNLR24MonthlyPaymentField;
                }
                set
                {
                    this.consumerNLR24MonthlyPaymentField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("ConsumerNLRDefinition")]
            public ConsumerConsumerNLRDefinition[] ConsumerNLRDefinition
            {
                get
                {
                    return this.consumerNLRDefinitionField;
                }
                set
                {
                    this.consumerNLRDefinitionField = value;
                }
            }

            /// <remarks/>
            public ConsumerConsumerCPANLRDebtSummary ConsumerCPANLRDebtSummary
            {
                get
                {
                    return this.consumerCPANLRDebtSummaryField;
                }
                set
                {
                    this.consumerCPANLRDebtSummaryField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("CreditActiveNess")]
            public ConsumerCreditActiveNess[] CreditActiveNess
            {
                get
                {
                    return this.creditActiveNessField;
                }
                set
                {
                    this.creditActiveNessField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("IndustryPayments")]
            public ConsumerIndustryPayments[] IndustryPayments
            {
                get
                {
                    return this.industryPaymentsField;
                }
                set
                {
                    this.industryPaymentsField = value;
                }
            }

            /// <remarks/>
            public ConsumerPaymentGAP PaymentGAP
            {
                get
                {
                    return this.paymentGAPField;
                }
                set
                {
                    this.paymentGAPField = value;
                }
            }

            /// <remarks/>
            public ConsumerSubscriberInputDetails SubscriberInputDetails
            {
                get
                {
                    return this.subscriberInputDetailsField;
                }
                set
                {
                    this.subscriberInputDetailsField = value;
                }
            }

            [XmlElement("ConsumerAdverseInfo")]
            public ConsumerConsumerAdverseInformation[] ConsumerAdverseInformation
            {
                get
                {
                    return this.consumerAdverseInformationField;
                }
                set
                {
                    this.consumerAdverseInformationField = value;
                }
            }

            [XmlElement("ConsumerJudgement")]
            public ConsumerConsumerJudgement[] ConsumerJudgement
            {
                get
                {
                    return this.consumerJudgementField;
                }
                set
                {
                    this.consumerJudgementField = value;
                }
            }

            [XmlElement("ConsumerAdminOrder")]
            public ConsumerConsumerAdminOrder[] ConsumerAdminOrder
            {
                get
                {
                    return this.consumerAdminOrderField;
                }
                set
                {
                    this.consumerAdminOrderField = value;
                }
            }


            [XmlElement("ConsumerRehabilitationOrder")]
            public ConsumerConsumerRehabilitationOrder[] ConsumerRehabilitationOrder
            {
                get
                {
                    return this.consumerRehabilitationOrderField;
                }
                set
                {
                    this.consumerRehabilitationOrderField = value;
                }
            }

            [System.Xml.Serialization.XmlElement("XDSPaymentNotification")]
            public ConsumerXDSPaymentNotification[] XDSPaymentNotification
            {
                get
                {
                    return this.xDSPaymentNotificationField;
                }
                set
                {
                    this.xDSPaymentNotificationField = value;
                }
            }

            /// <remarks/>
            [XmlElement("ConsumerAddressConfirmation")]
            public ConsumerConsumerAddressConfirmation[] ConsumerAddressConfirmation
            {
                get
                {
                    return this.consumerAddressConfirmationField;
                }
                set
                {
                    this.consumerAddressConfirmationField = value;
                }
            }

            /// <remarks/>
            [XmlElement("ConsumerContactConfirmation")]
            public ConsumerConsumerContactConfirmation[] ConsumerContactConfirmation
            {
                get
                {
                    return this.consumerContactConfirmationField;
                }
                set
                {
                    this.consumerContactConfirmationField = value;
                }
            }

            /// <remarks/>
            [XmlElement("VehicleInformation")]
            public ConsumerVehicleInformation VehicleInformation
            {
                get
                {
                    return this.vehicleInformationField;
                }
                set
                {
                    this.vehicleInformationField = value;
                }
            }

            /// <remarks/>
            [XmlElement("SAPInformation")]
            public ConsumerSAPInformation SAPInformation
            {
                get
                {
                    return this.sAPInformationField;
                }
                set
                {
                    this.sAPInformationField = value;
                }
            }

            /// <remarks/>
              [XmlElement("TradeInformation")]
            public ConsumerTradeInformation TradeInformation
            {
                get
                {
                    return this.tradeInformationField;
                }
                set
                {
                    this.tradeInformationField = value;
                }
            }

             [XmlElement("ConsumerVehicleAccountStatus")]
            public ConsumerConsumerAccountStatus[] ConsumerVehicleAccountStatus
            {
                get
                {
                    return this.ConsumerVehicleAccountStatusField;
                }
                set
                {
                    this.ConsumerVehicleAccountStatusField = value;
                }
            }
        }
 
     }

