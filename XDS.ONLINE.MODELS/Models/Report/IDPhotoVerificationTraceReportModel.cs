﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDS.ONLINE.MODELS
{
    [System.Xml.Serialization.XmlRoot("HomeAffairs")]
    public class IDPhotoVerificationTraceReportModel
    {
        private ConsumerReportInformation reportInformationField;

        private ConsumerConsumerDetail consumerDetailField;

        private ConsumerConsumerIdentityVerification consumerIdentityVerificationField;

        private ConsumerSubscriberInputDetails subscriberInputDetailsField;

        private HomeAffairsIDPhotoDetails iDPhotoDetailsField;









        /// <remarks/>
        public ConsumerReportInformation ReportInformation
        {
            get
            {
                return this.reportInformationField;
            }
            set
            {
                this.reportInformationField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerDetail ConsumerDetail
        {
            get
            {
                return this.consumerDetailField;
            }
            set
            {
                this.consumerDetailField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerIdentityVerification ConsumerIdentityVerification
        {
            get
            {
                return this.consumerIdentityVerificationField;
            }
            set
            {
                this.consumerIdentityVerificationField = value;
            }
        }

        /// <remarks/>
        public ConsumerSubscriberInputDetails SubscriberInputDetails
        {
            get
            {
                return this.subscriberInputDetailsField;
            }
            set
            {
                this.subscriberInputDetailsField = value;
            }
        }


        public HomeAffairsIDPhotoDetails IDPhotoDetails
        {
            get
            {
                return this.iDPhotoDetailsField;
            }
            set
            {
                this.iDPhotoDetailsField = value;
            }
        }



    }

    ///// <remarks/>
    //public partial class ConsumerReportInformation : Consumer
    //{ }

    ///// <remarks/>
    //public  partial class ConsumerConsumerDetail : Consumer
    //{ }

    ///// <remarks/>
    //public  partial class ConsumerConsumerIdentityVerification : Consumer
    //{ }

    ///// <remarks/>
    //public  partial class ConsumerSubscriberInputDetails : Consumer
    //{ }

}
