﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDS.ONLINE.MODELS
{
    [System.Xml.Serialization.XmlRoot("Commercial")]
    public class BusinessEnquiryTraceReportModel
    {

        private int productID = 0;
        private ConsumerReportInformation reportInformationField;
        private ConsumerCommercialSubscriberInputDetails subscriberInputDetailsField;
        private ConsumerXDSPaymentNotification[] xDSPaymentNotificationField;
        private ConsumerCommercialPossibleJudgment[] commercialPossibleJudgmentField;
        
        private ConsumerConsumerDirectorInformation[] commercialDirectorInformationField;
        private ConsumerConsumerAuditorInformation commercialAuditorInformationField;
        private ConsumerXDSConsumerAddressHistory[] xDSconsumerAddressHistoryField;
        private ConsumerXDSConsumerContactHistory[] xDSconsumerContactHistoryField;
        private ConsumerConsumerAddressInformation[] commercialAddressInformationField;
        private ConsumerConsumerBusinessInformation commercialBusinessInformationField;
        private ConsumerConsumerCompanyInformation commercialCompanyInformationField;
        private ConsumerActiveDirectorJudgments[] activeDirectorJudgmentsField;
        private ConsumerConsumerPrincipalInformation[] commercialPrincipalInformationField;
        private ConsumerConsumerActivePrincipalInfoSummary commercialActivePrincipalInfoSummaryField;
        private ConsumerConsumerInActivePrincipalInfoSummary commercialInActivePrincipalInfoSummaryField;
        private ConsumerConsumerActivePrincipalInformation[] commercialActivePrincipalInformationField;
        private ConsumerConsumerInactivePrincipalInformation[] commercialInactivePrincipalInformationField;
        private ConsumerConsumerInactiveDirectorJudgments inactiveDirectorJudgmentsField;

        private CommercialCommercialScoring commercialScoringField;

        //private CommercialVATInformation[] commercialVATInformationField;

        //public CommercialVATInformation[] CommercialVATInformationField
        //{
        //    get { return commercialVATInformationField; }
        //    set { commercialVATInformationField = value; }
        //}

        private CommercialChangeHistoryInformation[] changeHistoryInformationField;

        private CommercialCommercialVATInformation[] businessCommercialVATInformationField;

        private CommercialCommercialPropertyInformation[] commercialPropertyInformationField;

        private CommercialCommercialTradeReferencesInfo[] commercialTradeReferencesInfoField;
        private CommercialCommercialBankCodeHistory[] commercialBankCodeHistoryField;
        
        private CommercialCommercialJudgment[] commercialJudgmentField;


       private CommercialCommercialVATInformation commercialVATInformationField;



       private CommercialDirectorPropertyInterests[] directorPropertyInterestsField;

       private CommercialDirectorCurrentBusinessinterests[] directorCurrentBusinessinterestsField;

       private CommercialDirectorPreviousBusinessInterests[] directorPreviousBusinessInterestsField;



       public int ProductID
       {
           get { return productID; }
           set { productID = value; }
       } 

       [System.Xml.Serialization.XmlElementAttribute("DirectorCurrentBusinessinterests")]
       public CommercialDirectorCurrentBusinessinterests[] DirectorCurrentBusinessinterests
       {
           get
           {
               return this.directorCurrentBusinessinterestsField;
           }
           set
           {
               this.directorCurrentBusinessinterestsField = value;
           }
       }

       /// <remarks/>
       [System.Xml.Serialization.XmlElementAttribute("DirectorPreviousBusinessInterests")]
       public CommercialDirectorPreviousBusinessInterests[] DirectorPreviousBusinessInterests
       {
           get
           {
               return this.directorPreviousBusinessInterestsField;
           }
           set
           {
               this.directorPreviousBusinessInterestsField = value;
           }
       }

        /// <remarks/>
        public ConsumerReportInformation ReportInformation
        {
            get
            {
                return this.reportInformationField;
            }
            set
            {
                this.reportInformationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CommercialJudgment")]
        public CommercialCommercialJudgment[] CommercialJudgment
        {
            get
            {
                return this.commercialJudgmentField;
            }
            set
            {
                this.commercialJudgmentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CommercialDirectorInformation")]
        public ConsumerConsumerDirectorInformation[] CommercialDirectorInformation
        {
            get
            {
                return this.commercialDirectorInformationField;
            }
            set
            {
                this.commercialDirectorInformationField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerAuditorInformation CommercialAuditorInformation
        {
            get
            {
                return this.commercialAuditorInformationField;
            }
            set
            {
                this.commercialAuditorInformationField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute("XDSCommercialAddressHistory")]
        public ConsumerXDSConsumerAddressHistory[] XDSConsumerAddressHistory
        {
            get
            {
                return this.xDSconsumerAddressHistoryField;
            }
            set
            {
                this.xDSconsumerAddressHistoryField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute("XDSCommercialContactHistory")]
        public ConsumerXDSConsumerContactHistory[] XDSConsumerContactHistory
        {
            get
            {
                return this.xDSconsumerContactHistoryField;
            }
            set
            {
                this.xDSconsumerContactHistoryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CommercialAddressInformation")]
        public ConsumerConsumerAddressInformation[] CommercialAddressInformation
        {
            get
            {
                return this.commercialAddressInformationField;
            }
            set
            {
                this.commercialAddressInformationField = value;
            }
        }

       

        /// <remarks/>
        public ConsumerConsumerBusinessInformation CommercialBusinessInformation
        {
            get
            {
                return this.commercialBusinessInformationField;
            }
            set
            {
                this.commercialBusinessInformationField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerCompanyInformation CommercialCompanyInformation
        {
            get
            {
                return this.commercialCompanyInformationField;
            }
            set
            {
                this.commercialCompanyInformationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ActiveDirectorJudgments")]
        public ConsumerActiveDirectorJudgments[] ActiveDirectorJudgments
        {
            get
            {
                return this.activeDirectorJudgmentsField;
            }
            set
            {
                this.activeDirectorJudgmentsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CommercialPrincipalInformation")]
        public ConsumerConsumerPrincipalInformation[] CommercialPrincipalInformation
        {
            get
            {
                return this.commercialPrincipalInformationField;
            }
            set
            {
                this.commercialPrincipalInformationField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerActivePrincipalInfoSummary CommercialActivePrincipalInfoSummary
        {
            get
            {
                return this.commercialActivePrincipalInfoSummaryField;
            }
            set
            {
                this.commercialActivePrincipalInfoSummaryField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerInActivePrincipalInfoSummary CommercialInActivePrincipalInfoSummary
        {
            get
            {
                return this.commercialInActivePrincipalInfoSummaryField;
            }
            set
            {
                this.commercialInActivePrincipalInfoSummaryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CommercialActivePrincipalInformation")]
        public ConsumerConsumerActivePrincipalInformation[] CommercialActivePrincipalInformation
        {
            get
            {
                return this.commercialActivePrincipalInformationField;
            }
            set
            {
                this.commercialActivePrincipalInformationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CommercialInactivePrincipalInformation")]
        public ConsumerConsumerInactivePrincipalInformation[] CommercialInactivePrincipalInformation
        {
            get
            {
                return this.commercialInactivePrincipalInformationField;
            }
            set
            {
                this.commercialInactivePrincipalInformationField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerInactiveDirectorJudgments InactiveDirectorJudgments
        {
            get
            {
                return this.inactiveDirectorJudgmentsField;
            }
            set
            {
                this.inactiveDirectorJudgmentsField = value;
            }
        }

        public ConsumerCommercialSubscriberInputDetails SubscriberInputDetails
        {
            get
            {
                return this.subscriberInputDetailsField;
            }
            set
            {
                this.subscriberInputDetailsField = value;
            }
        }

        [System.Xml.Serialization.XmlElement("XDSPaymentNotification")]
        public ConsumerXDSPaymentNotification[] XDSPaymentNotification
        {
            get
            {
                return this.xDSPaymentNotificationField;
            }
            set
            {
                this.xDSPaymentNotificationField = value;
            }
        }

        [System.Xml.Serialization.XmlElement("CommercialPossibleJudgment")]
        public ConsumerCommercialPossibleJudgment[] CommercialPossibleJudgment
        {
            get
            {
                return this.commercialPossibleJudgmentField;
            }
            set
            {
                this.commercialPossibleJudgmentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ChangeHistoryInformation")]
        public CommercialChangeHistoryInformation[] ChangeHistoryInformation
        {
            get
            {
                return this.changeHistoryInformationField;
            }
            set
            {
                this.changeHistoryInformationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CommercialVATInformation")]
        public CommercialCommercialVATInformation[] BusinessCommercialVATInformation
        {
            get
            {
                return this.businessCommercialVATInformationField;
            }
            set
            {
                this.businessCommercialVATInformationField = value;
            }
        }

        /// <remarks/>
        public CommercialCommercialScoring CommercialScoring
        {
            get
            {
                return this.commercialScoringField;
            }
            set
            {
                this.commercialScoringField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CommercialPropertyInformation")]
        public CommercialCommercialPropertyInformation[] CommercialPropertyInformation
        {
            get
            {
                return this.commercialPropertyInformationField;
            }
            set
            {
                this.commercialPropertyInformationField = value;
            }
        }


        [System.Xml.Serialization.XmlElementAttribute("CommercialTradeReferencesInformation")]
        public CommercialCommercialTradeReferencesInfo[] CommercialCommercialTradeReferencesInfo
        {
            get
            {
                return this.commercialTradeReferencesInfoField;
            }
            set
            {
                this.commercialTradeReferencesInfoField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute("DirectorPropertyInterests")]
        public CommercialDirectorPropertyInterests[] DirectorPropertyInterests
        {
            get
            {
                return this.directorPropertyInterestsField;
            }
            set
            {
                this.directorPropertyInterestsField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute("CommercialBankCodeHistory")]
        public CommercialCommercialBankCodeHistory[] CommercialCommercialBankCodeHistory
        {
            get
            {
                return this.commercialBankCodeHistoryField;
            }
            set
            {
                this.commercialBankCodeHistoryField = value;
            }
        }



    }



    //public partial class ConsumerReportInformation : Consumer
    //{ }

    //public partial class ConsumerConsumerDirectorInformation : Consumer
    //{ }

    //public partial class ConsumerConsumerAuditorInformation : Consumer
    //{ }

    //public partial class ConsumerConsumerAddressInformation : Consumer 
    //{ }

    ///// <summary>
    ///// May not be needed
    ///// </summary>
    //public partial class ConsumerConsumerAddressInformationAddress3 : Consumer
    //{ }

    ///// <summary>
    ///// May not be needed
    ///// </summary>
    //public partial class ConsumerConsumerAddressInformationAddress4 : Consumer
    //{ }

    //public partial class ConsumerConsumerBusinessInformation : Consumer 
    //{ }

    //public partial class ConsumerConsumerBusinessInformationTradeName : Consumer 
    //{ }

    //public partial class ConsumerConsumerCompanyInformation : Consumer 
    //{ }

    //public partial class ConsumerConsumerCompanyInformationTradeName : Consumer
    //{ }

    //public partial class ConsumerConsumerDirectorJudgments : Consumer
    //{ }

    //public partial class ConsumerConsumerPrincipalInformation : Consumer
    //{ }

    //public partial class ConsumerConsumerPrincipalInformationMemberControlType : Consumer
    //{ }

    //public partial class ConsumerConsumerActivePrincipalInfoSummary : Consumer
    //{ }

    //public partial class ConsumerConsumerInActivePrincipalInfoSummary : Consumer
    //{ }

    //public partial class ConsumerConsumerActivePrincipalInformation : Consumer
    //{ }

    //public partial class ConsumerConsumerInactivePrincipalInformation : Consumer 
    //{ }

    //public partial class ConsumerConsumerInactiveDirectorJudgments : Consumer 
    //{ }

    //public partial class ConsumerCommercialSubscriberInputDetails : Consumer
    //{ }

    //public partial class ConsumerXDSPaymentNotification : Consumer
    //{ }

    //public partial class CommercialVATInformation : Consumer
    //{ }
}
