﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace XDS.ONLINE.MODELS
{
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false, ElementName = "Consumer")]
    [System.Xml.Serialization.XmlRoot("Consumer")]
    public class ConsumerTraceReportModel
    {
        private ConsumerReportInformation reportInformationField;
        private ConsumerSubscriberInputDetails subscriberInputDetailsField;
        private ConsumerConsumerCreditAccountSummary consumerCreditAccountSummaryField;
        private ConsumerConsumerPropertyInformationSummary consumerPropertyInformationSummaryField;
        private ConsumerConsumerFraudIndicatorsSummary consumerFraudIndicatorsSummaryField;
        private ConsumerConsumerScoring consumerScoringField;
        private ConsumerConsumerDirectorSummary consumerDirectorSummaryField;
        private ConsumerConsumerDetail consumerDetailField;
        private ConsumerConsumerDebtReviewStatus consumerDebtReviewStatusField;
        private ConsumerConsumerFraudIndicators consumerFraudIndicatorsField;
        private ConsumerConsumerAccountStatus[] consumerAccountStatusField;
        private ConsumerAccountTypeLegend[] accountTypeLegendField;
        private ConsumerConsumer24MonthlyPaymentHeader consumer24MonthlyPaymentHeaderField;
        private ConsumerConsumer24MonthlyPayment[] consumer24MonthlyPaymentField;
        private ConsumerConsumerDefinition[] consumerDefinitionField;
        private ConsumerNLRAccountTypeLegend[] nLRAccountTypeLegendField;
        private ConsumerConsumerNLRAccountStatus[] consumerNLRAccountStatusField;
        private ConsumerConsumerNLR24MonthlyPaymentHeader consumerNLR24MonthlyPaymentHeaderField;
        private ConsumerConsumerNLR24MonthlyPayment[] consumerNLR24MonthlyPaymentField;
        private ConsumerConsumerNLRDefinition[] consumerNLRDefinitionField;
        private ConsumerConsumerAddressHistory[] consumerAddressHistoryField;
        private ConsumerConsumerTelephoneHistory[] consumerTelephoneHistoryField;
        private ConsumerConsumerEmailHistory[] consumerEmailHistoryField;
        private ConsumerConsumerEmploymentHistory[] consumerEmploymentHistoryField;
        private ConsumerConsumerTelephoneLinkageHome[] consumerTelephoneLinkageHomeField;
        private ConsumerConsumerTelephoneLinkageWork[] consumerTelephoneLinkageWorkField;
        private ConsumerConsumerTelephoneLinkageCellular[] consumerTelephoneLinkageCellularField;
        private ConsumerConsumerMaritalStatusEnquiry consumerMaritalStatusEnquiryField;
        private ConsumerConsumerAdverseInformation[] consumerAdverseInformationField;
        private ConsumerConsumerJudgement[] consumerJudgementField;
        private ConsumerConsumerAdminOrder[] consumerAdminOrderField;
        private ConsumerConsumerSequestration[] consumerSequestrationField;
        private ConsumerConsumerRehabilitationOrder[] consumerRehabilitationOrderField;
        private ConsumerConsumerDefaultAlert[] consumerDefaultAlertField;        
        private ConsumerConsumerEnquiryHistory[] consumerEnquiryHistoryField;
        private ConsumerConsumerCPANLRDebtSummary consumerCPANLRDebtSummaryField;
        private ConsumerConsumerAccountGoodBadSummary consumerAccountGoodBadSummaryField;
        private ConsumerXDSPaymentNotification[] xDSPaymentNotificationField;
        private ConsumerConsumerAddressConfirmation[] consumerAddressConfirmationField;
        private ConsumerConsumerContactConfirmation[] consumerContactConfirmationField;

        private ConsumerConsumerOtherContactInfoAddress[] consumerOtherContactInfoAddressField;
        private ConsumerConsumerOtherContactInfoTelephone[] consumerOtherContactInfoTelephoneField;

        private ConsumerConsumerPropertyInformation[] consumerPropertyInformationField;
        private ConsumerConsumerDirectorShipLink[] consumerDirectorShipLinkField;

        private ConsumerConsumerEmploymentConfirmation[] consumerEmploymentConfirmationField;

        public ConsumerReportInformation ReportInformation
        {
            get
            {
                return this.reportInformationField;
            }
            set
            {
                this.reportInformationField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute("SubscriberInputDetails")]
        public ConsumerSubscriberInputDetails SubscriberInputDetails
        {
            get
            {
                return this.subscriberInputDetailsField;
            }
            set
            {
                this.subscriberInputDetailsField = value;
            }
        }

        public ConsumerConsumerDetail ConsumerDetail
        {
            get
            {
                return this.consumerDetailField;
            }
            set
            {
                this.consumerDetailField = value;
            }
        }

        public ConsumerConsumerCreditAccountSummary ConsumerCreditAccountSummary
        {
            get
            {
                return this.consumerCreditAccountSummaryField;
            }
            set
            {
                this.consumerCreditAccountSummaryField = value;
            }
        }

        public ConsumerConsumerPropertyInformationSummary ConsumerPropertyInformationSummary
        {
            get
            {
                return this.consumerPropertyInformationSummaryField;
            }
            set
            {
                this.consumerPropertyInformationSummaryField = value;
            }
        }

        public ConsumerConsumerFraudIndicatorsSummary ConsumerFraudIndicatorsSummary
        {
            get
            {
                return this.consumerFraudIndicatorsSummaryField;
            }
            set
            {
                this.consumerFraudIndicatorsSummaryField = value;
            }
        }

        public ConsumerConsumerScoring ConsumerScoring
        {
            get
            {
                return this.consumerScoringField;
            }
            set
            {
                this.consumerScoringField = value;
            }
        }

        public ConsumerConsumerDirectorSummary ConsumerDirectorSummary
        {
            get
            {
                return this.consumerDirectorSummaryField;
            }
            set
            {
                this.consumerDirectorSummaryField = value;
            }
        }
        
        public ConsumerConsumerDebtReviewStatus ConsumerDebtReviewStatus
        {
            get
            {
                return this.consumerDebtReviewStatusField;
            }
            set
            {
                this.consumerDebtReviewStatusField = value;
            }
        }

        public ConsumerConsumerFraudIndicators ConsumerFraudIndicators
        {
            get
            {
                return this.consumerFraudIndicatorsField;
            }
            set
            {
                this.consumerFraudIndicatorsField = value;
            }
        }

        [XmlElement("ConsumerAccountStatus")]
        public ConsumerConsumerAccountStatus[] ConsumerAccountStatus
        {
            get
            {
                return this.consumerAccountStatusField;
            }
            set
            {
                this.consumerAccountStatusField = value;
            }
        }

        [XmlElement("AccountTypeLegend")]
        public ConsumerAccountTypeLegend[] AccountTypeLegend
        {
            get
            {
                return this.accountTypeLegendField;
            }
            set
            {
                this.accountTypeLegendField = value;
            }
        }

        [XmlElement("Consumer24MonthlyPaymentHeader")]
        public ConsumerConsumer24MonthlyPaymentHeader Consumer24MonthlyPaymentHeader
        {
            get
            {
                return this.consumer24MonthlyPaymentHeaderField;
            }
            set
            {
                this.consumer24MonthlyPaymentHeaderField = value;
            }
        }

        [XmlElement("Consumer24MonthlyPayment")]
        public ConsumerConsumer24MonthlyPayment[] Consumer24MonthlyPayment
        {
            get
            {
                return this.consumer24MonthlyPaymentField;
            }
            set
            {
                this.consumer24MonthlyPaymentField = value;
            }
        }

        [XmlElement("ConsumerDefinition")]
        public ConsumerConsumerDefinition[] ConsumerDefinition
        {
            get
            {
                return this.consumerDefinitionField;
            }
            set
            {
                this.consumerDefinitionField = value;
            }
        }

        [XmlElement("NLRAccountTypeLegend")]
        public ConsumerNLRAccountTypeLegend[] NLRAccountTypeLegend
        {
            get
            {
                return this.nLRAccountTypeLegendField;
            }
            set
            {
                this.nLRAccountTypeLegendField = value;
            }
        }

        [XmlElement("ConsumerNLRAccountStatus")]
        public ConsumerConsumerNLRAccountStatus[] ConsumerNLRAccountStatus
        {
            get
            {
                return this.consumerNLRAccountStatusField;
            }
            set
            {
                this.consumerNLRAccountStatusField = value;
            }
        }

        [XmlElement("ConsumerNLR24MonthlyPaymentHeader")]
        public ConsumerConsumerNLR24MonthlyPaymentHeader ConsumerNLR24MonthlyPaymentHeader
        {
            get
            {
                return this.consumerNLR24MonthlyPaymentHeaderField;
            }
            set
            {
                this.consumerNLR24MonthlyPaymentHeaderField = value;
            }
        }

        [XmlElement("ConsumerNLR24MonthlyPayment")]
        public ConsumerConsumerNLR24MonthlyPayment[] ConsumerNLR24MonthlyPayment
        {
            get
            {
                return this.consumerNLR24MonthlyPaymentField;
            }
            set
            {
                this.consumerNLR24MonthlyPaymentField = value;
            }
        }

        [XmlElement("ConsumerNLRDefinition")]
        public ConsumerConsumerNLRDefinition[] ConsumerNLRDefinition
        {
            get
            {
                return this.consumerNLRDefinitionField;
            }
            set
            {
                this.consumerNLRDefinitionField = value;
            }
        }

        [XmlElement("ConsumerAddressHistory")]
        public ConsumerConsumerAddressHistory[] ConsumerAddressHistory
        {
            get
            {
                return this.consumerAddressHistoryField;
            }
            set
            {
                this.consumerAddressHistoryField = value;
            }
        }

        [XmlElement("ConsumerTelephoneHistory")]
        public ConsumerConsumerTelephoneHistory[] ConsumerTelephoneHistory
        {
            get
            {
                return this.consumerTelephoneHistoryField;
            }
            set
            {
                this.consumerTelephoneHistoryField = value;
            }
        }

        [XmlElement("ConsumerEmailHistory")]
        public ConsumerConsumerEmailHistory[] ConsumerEmailHistory
        {
            get
            {
                return this.consumerEmailHistoryField;
            }
            set
            {
                this.consumerEmailHistoryField = value;
            }
        }

        [XmlElement("ConsumerEmploymentHistory")]
        public ConsumerConsumerEmploymentHistory[] ConsumerEmploymentHistory
        {
            get
            {
                return this.consumerEmploymentHistoryField;
            }
            set
            {
                this.consumerEmploymentHistoryField = value;
            }
        }

        [XmlElement("ConsumerTelephoneLinkageHome")]
        public ConsumerConsumerTelephoneLinkageHome[] ConsumerTelephoneLinkageHome
        {
            get
            {
                return this.consumerTelephoneLinkageHomeField;
            }
            set
            {
                this.consumerTelephoneLinkageHomeField = value;
            }
        }

        [XmlElement("ConsumerTelephoneLinkageWork")]
        public ConsumerConsumerTelephoneLinkageWork[] ConsumerTelephoneLinkageWork
        {
            get
            {
                return this.consumerTelephoneLinkageWorkField;
            }
            set
            {
                this.consumerTelephoneLinkageWorkField = value;
            }
        }

        [XmlElement("ConsumerTelephoneLinkageCellular")]
        public ConsumerConsumerTelephoneLinkageCellular[] ConsumerTelephoneLinkageCellular
        {
            get
            {
                return this.consumerTelephoneLinkageCellularField;
            }
            set
            {
                this.consumerTelephoneLinkageCellularField = value;
            }
        }

        public ConsumerConsumerMaritalStatusEnquiry ConsumerMaritalStatusEnquiry
        {
            get
            {
                return this.consumerMaritalStatusEnquiryField;
            }
            set
            {
                this.consumerMaritalStatusEnquiryField = value;
            }
        }

        [XmlElement("ConsumerAdverseInfo")]
        public ConsumerConsumerAdverseInformation[] ConsumerAdverseInformation
        {
            get
            {
                return this.consumerAdverseInformationField;
            }
            set
            {
                this.consumerAdverseInformationField = value;
            }
        }

        [XmlElement("ConsumerJudgement")]
        public ConsumerConsumerJudgement[] ConsumerJudgement
        {
            get
            {
                return this.consumerJudgementField;
            }
            set
            {
                this.consumerJudgementField = value;
            }
        }
         [XmlElement("ConsumerAdminOrder")]
        public ConsumerConsumerAdminOrder[] ConsumerAdminOrder
        {
            get
            {
                return this.consumerAdminOrderField;
            }
            set
            {
                this.consumerAdminOrderField = value;
            }
        }

        [XmlElement("ConsumerSequestration")]
        public ConsumerConsumerSequestration[] ConsumerSequestration
        {
            get
            {
                return this.consumerSequestrationField;
            }
            set
            {
                this.consumerSequestrationField = value;
            }
        }
          [XmlElement("ConsumerRehabilitationOrder")]
        public ConsumerConsumerRehabilitationOrder[] ConsumerRehabilitationOrder
        {
            get
            {
                return this.consumerRehabilitationOrderField;
            }
            set
            {
                this.consumerRehabilitationOrderField = value;
            }
        }

        [System.Xml.Serialization.XmlElement("ConsumerDefaultAlert")]
        public ConsumerConsumerDefaultAlert[] ConsumerDefaultAlert
        {
            get
            {
                return this.consumerDefaultAlertField;
            }
            set
            {
                this.consumerDefaultAlertField = value;
            }
        }

        [XmlElement("ConsumerEnquiryHistory")]
        public ConsumerConsumerEnquiryHistory[] ConsumerEnquiryHistory
        {
            get
            {
                return this.consumerEnquiryHistoryField;
            }
            set
            {
                this.consumerEnquiryHistoryField = value;
            }
        }

        public ConsumerConsumerCPANLRDebtSummary ConsumerCPANLRDebtSummary
        {
            get
            {
                return this.consumerCPANLRDebtSummaryField;
            }
            set
            {
                this.consumerCPANLRDebtSummaryField = value;
            }
        }

        public ConsumerConsumerAccountGoodBadSummary ConsumerAccountGoodBadSummary
        {
            get
            {
                return this.consumerAccountGoodBadSummaryField;
            }
            set
            {
                this.consumerAccountGoodBadSummaryField = value;
            }
        }

        [System.Xml.Serialization.XmlElement("XDSPaymentNotification")]
        public ConsumerXDSPaymentNotification[] XDSPaymentNotification
        {
            get
            {
                return this.xDSPaymentNotificationField;
            }
            set
            {
                this.xDSPaymentNotificationField = value;
            }
        }

        /// <remarks/>
        [XmlElement("ConsumerAddressConfirmation")]
        public ConsumerConsumerAddressConfirmation[] ConsumerAddressConfirmation
        {
            get
            {
                return this.consumerAddressConfirmationField;
    }
            set
            {
                this.consumerAddressConfirmationField = value;
            }
        }

        /// <remarks/>
        [XmlElement("ConsumerContactConfirmation")]
        public ConsumerConsumerContactConfirmation[] ConsumerContactConfirmation
        {
            get
            {
                return this.consumerContactConfirmationField;
            }
            set
            {
                this.consumerContactConfirmationField = value;
            }
        }

        [XmlElement("ConsumerPropertyInformation")]
        public ConsumerConsumerPropertyInformation[] ConsumerPropertyInformation
        {
            get
            {
                return this.consumerPropertyInformationField;
            }
            set
            {
                this.consumerPropertyInformationField = value;
            }
        }

        public ConsumerConsumerOtherContactInfoAddress[] ConsumerOtherContactInfoAddress
        {
            get
            {
                return this.consumerOtherContactInfoAddressField;
            }
            set
            {
                this.consumerOtherContactInfoAddressField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute("ConsumerOtherContactInfoTelephone")]
        public ConsumerConsumerOtherContactInfoTelephone[] ConsumerOtherContactInfoTelephone
        {
            get
            {
                return this.consumerOtherContactInfoTelephoneField;
            }
            set
            {
                this.consumerOtherContactInfoTelephoneField = value;
            }
        }
        
        //[System.Xml.Serialization.XmlElementAttribute("ConsumerDirectorShipLink")]
        [XmlElement("ConsumerDirectorShipLink")]
        public ConsumerConsumerDirectorShipLink[] ConsumerDirectorShipLink
        {
            get
            {
                return this.consumerDirectorShipLinkField;
            }
            set
            {
                this.consumerDirectorShipLinkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ConsumerEmploymentConfirmation")]
        public ConsumerConsumerEmploymentConfirmation[] ConsumerEmploymentConfirmation
        {
            get
            {
                return this.consumerEmploymentConfirmationField;
            }
            set
            {
                this.consumerEmploymentConfirmationField = value;
            }
        }

    }

       
}
