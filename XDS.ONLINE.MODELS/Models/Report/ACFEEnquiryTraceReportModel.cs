﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace XDS.ONLINE.MODELS
{
    [System.Xml.Serialization.XmlRoot("Consumer")]
    public class ACFEEnquiryTraceReportModel
    {

        private ConsumerReportInformation reportInformationField;

        private ConsumerConsumerDetail consumerDetailField;

        private ConsumerConsumerFraudIndicatorsSummary consumerFraudIndicatorsSummaryField;

        private ConsumerConsumerScoring consumerScoringField;

        private ConsumerConsumerPropertyInformationSummary consumerPropertyInformationSummaryField;

        private ConsumerConsumerDirectorSummary consumerDirectorSummaryField;

        private ConsumerConsumerEnquiryHistory[] consumerEnquiryHistoryField;

        private ConsumerConsumerDirectorShipLink[] consumerDirectorShipLinkField;

        private ConsumerConsumerAddressHistory[] consumerAddressHistoryField;

        private ConsumerConsumerIdentityVerification consumerIdentityVerificationField;

        private ConsumerConsumerMaritalStatusEnquiry consumerMaritalStatusEnquiryField;

        private ConsumerVirginMobileCustomizedScoring virginMobileCustomizedScoringField;

        private ConsumerConsumerAffordability consumerAffordabilityField;

        private ConsumerCollectionConsumerSummary collectionConsumerSummaryField;

        private ConsumerConsumerCPANLRDebtSummary consumerCPANLRDebtSummaryField;

        private ConsumerCreditActiveNess[] creditActiveNessField;

        private ConsumerPaymentGAP paymentGAPField;

        private ConsumerSubscriberInputDetails subscriberInputDetailsField;

        private ConsumerAuthenticationHistory[] authenticationHistoryField;

        private ConsumerConsumerAccountStatus[] consumerAccountStatusField;

        private ConsumerAccountTypeLegend[] accountTypeLegendField;
        private ConsumerXDSPaymentNotification[] xDSPaymentNotificationField;
        private ConsumerSAFPSStatus sAFPSStatusField;
        private ConsumerConsumerDefaultAlert[] consumerDefaultAlertField;   
        private ConsumerConsumer24MonthlyPaymentHeader consumer24MonthlyPaymentHeaderField;
        private ConsumerConsumer24MonthlyPayment[] consumer24MonthlyPaymentField;
        private ConsumerConsumerDefinition[] consumerDefinitionField;
        private ConsumerConsumerNLRAccountStatus[] consumerNLRAccountStatusField;
        private ConsumerNLRAccountTypeLegend[] nLRAccountTypeLegendField;
        private ConsumerConsumerNLR24MonthlyPayment[] consumerNLR24MonthlyPaymentField;
        private ConsumerConsumerNLRDefinition[] consumerNLRDefinitionField;
        private ConsumerConsumerNLR24MonthlyPaymentHeader consumerNLR24MonthlyPaymentHeaderField;
        private ConsumerConsumerAdverseInformation[] consumerAdverseInformationField;
        private ConsumerConsumerJudgement[] consumerJudgementField;
        private ConsumerConsumerAdminOrder[] consumerAdminOrderField;
        private ConsumerConsumerTelephoneHistory[] consumerTelephoneHistoryField;
        private ConsumerConsumerEmailHistory[] consumerEmailHistoryField;
        private ConsumerConsumerEmploymentHistory[] consumerEmploymentHistoryField;
        private ConsumerConsumerTelephoneLinkageHome[] consumerTelephoneLinkageHomeField;
        private ConsumerConsumerTelephoneLinkageWork[] consumerTelephoneLinkageWorkField;
        private ConsumerConsumerTelephoneLinkageCellular[] consumerTelephoneLinkageCellularField;

        private ConsumerConsumerAddressConfirmation[] consumerAddressConfirmationField;
        private ConsumerConsumerContactConfirmation[] consumerContactConfirmationField;

        private ConsumerConsumerOtherContactInfoAddress[] consumerOtherContactInfoAddressField;
        private ConsumerConsumerOtherContactInfoTelephone[] consumerOtherContactInfoTelephoneField;
        private ConsumerConsumerEmploymentConfirmation[] consumerEmploymentConfirmationField;
        private ConsumerConsumerPropertyInformation[] consumerPropertyInformationField;
        /// <remarks/>
        /// 
       private ConsumerIndustryPayments[] industryPaymentsField;
        private ConsumerConsumerDebtReviewStatus consumerDebtReviewStatusField;
   private ConsumerConsumerRehabilitationOrder[] consumerRehabilitationOrderField;
        public ConsumerReportInformation ReportInformation
        {
            get
            {
                return this.reportInformationField;
            }
            set
            {
                this.reportInformationField = value;
            }
        }
        private ConsumerConsumerSequestration[] consumerSequestrationField;
        /// <remarks/>
        public ConsumerConsumerDetail ConsumerDetail
        {
            get
            {
                return this.consumerDetailField;
            }
            set
            {
                this.consumerDetailField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerFraudIndicatorsSummary ConsumerFraudIndicatorsSummary
        {
            get
            {
                return this.consumerFraudIndicatorsSummaryField;
            }
            set
            {
                this.consumerFraudIndicatorsSummaryField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerScoring ConsumerScoring
        {
            get
            {
                return this.consumerScoringField;
            }
            set
            {
                this.consumerScoringField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerPropertyInformationSummary ConsumerPropertyInformationSummary
        {
            get
            {
                return this.consumerPropertyInformationSummaryField;
            }
            set
            {
                this.consumerPropertyInformationSummaryField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerDirectorSummary ConsumerDirectorSummary
        {
            get
            {
                return this.consumerDirectorSummaryField;
            }
            set
            {
                this.consumerDirectorSummaryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ConsumerEnquiryHistory")]
        public ConsumerConsumerEnquiryHistory[] ConsumerEnquiryHistory
        {
            get
            {
                return this.consumerEnquiryHistoryField;
            }
            set
            {
                this.consumerEnquiryHistoryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ConsumerDirectorShipLink")]
        public ConsumerConsumerDirectorShipLink[] ConsumerDirectorShipLink
        {
            get
            {
                return this.consumerDirectorShipLinkField;
            }
            set
            {
                this.consumerDirectorShipLinkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ConsumerAddressHistory")]
        public ConsumerConsumerAddressHistory[] ConsumerAddressHistory
        {
            get
            {
                return this.consumerAddressHistoryField;
            }
            set
            {
                this.consumerAddressHistoryField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerIdentityVerification ConsumerIdentityVerification
        {
            get
            {
                return this.consumerIdentityVerificationField;
            }
            set
            {
                this.consumerIdentityVerificationField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerMaritalStatusEnquiry ConsumerMaritalStatusEnquiry
        {
            get
            {
                return this.consumerMaritalStatusEnquiryField;
            }
            set
            {
                this.consumerMaritalStatusEnquiryField = value;
            }
        }

        /// <remarks/>
        public ConsumerVirginMobileCustomizedScoring VirginMobileCustomizedScoring
        {
            get
            {
                return this.virginMobileCustomizedScoringField;
            }
            set
            {
                this.virginMobileCustomizedScoringField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerAffordability ConsumerAffordability
        {
            get
            {
                return this.consumerAffordabilityField;
            }
            set
            {
                this.consumerAffordabilityField = value;
            }
        }

        /// <remarks/>
        public ConsumerCollectionConsumerSummary CollectionConsumerSummary
        {
            get
            {
                return this.collectionConsumerSummaryField;
            }
            set
            {
                this.collectionConsumerSummaryField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerCPANLRDebtSummary ConsumerCPANLRDebtSummary
        {
            get
            {
                return this.consumerCPANLRDebtSummaryField;
            }
            set
            {
                this.consumerCPANLRDebtSummaryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CreditActiveNess")]
        public ConsumerCreditActiveNess[] CreditActiveNess
        {
            get
            {
                return this.creditActiveNessField;
            }
            set
            {
                this.creditActiveNessField = value;
            }
        }

        /// <remarks/>
        public ConsumerPaymentGAP PaymentGAP
        {
            get
            {
                return this.paymentGAPField;
            }
            set
            {
                this.paymentGAPField = value;
            }
        }

        /// <remarks/>
        public ConsumerSubscriberInputDetails SubscriberInputDetails
        {
            get
            {
                return this.subscriberInputDetailsField;
            }
            set
            {
                this.subscriberInputDetailsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("AuthenticationHistory")]
        public ConsumerAuthenticationHistory[] AuthenticationHistory
        {
            get
            {
                return this.authenticationHistoryField;
            }
            set
            {
                this.authenticationHistoryField = value;
            }
        }

        /// <remarks/>
        [XmlElement("SAFPSStatus")]
        public ConsumerSAFPSStatus SAFPSStatus
        {
            get
            {
                return this.sAFPSStatusField;
            }
            set
            {
                this.sAFPSStatusField = value;
            }
        }


        [XmlElement("ConsumerAccountStatus")]
        public ConsumerConsumerAccountStatus[] ConsumerAccountStatus
        {
            get
            {
                return this.consumerAccountStatusField;
            }
            set
            {
                this.consumerAccountStatusField = value;
            }
        }

        [XmlElement("AccountTypeLegend")]
        public ConsumerAccountTypeLegend[] AccountTypeLegend
        {
            get
            {
                return this.accountTypeLegendField;
            }
            set
            {
                this.accountTypeLegendField = value;
            }
        }


        [XmlElement("Consumer24MonthlyPayment")]
        public ConsumerConsumer24MonthlyPayment[] Consumer24MonthlyPayment
        {
            get
            {
                return this.consumer24MonthlyPaymentField;
            }
            set
            {
                this.consumer24MonthlyPaymentField = value;
            }
        }



        [XmlElement("Consumer24MonthlyPaymentHeader")]
        public ConsumerConsumer24MonthlyPaymentHeader Consumer24MonthlyPaymentHeader
        {
            get
            {
                return this.consumer24MonthlyPaymentHeaderField;
            }
            set
            {
                this.consumer24MonthlyPaymentHeaderField = value;
            }
        }


        [XmlElement("ConsumerDefinition")]
        public ConsumerConsumerDefinition[] ConsumerDefinition
        {
            get
            {
                return this.consumerDefinitionField;
            }
            set
            {
                this.consumerDefinitionField = value;
            }
        }


        [XmlElement("ConsumerNLRAccountStatus")]
        public ConsumerConsumerNLRAccountStatus[] ConsumerNLRAccountStatus
        {
            get
            {
                return this.consumerNLRAccountStatusField;
            }
            set
            {
                this.consumerNLRAccountStatusField = value;
            }
        }

        [XmlElement("NLRAccountTypeLegend")]
        public ConsumerNLRAccountTypeLegend[] NLRAccountTypeLegend
        {
            get
            {
                return this.nLRAccountTypeLegendField;
            }
            set
            {
                this.nLRAccountTypeLegendField = value;
            }
        }


        [XmlElement("ConsumerNLR24MonthlyPayment")]
        public ConsumerConsumerNLR24MonthlyPayment[] ConsumerNLR24MonthlyPayment
        {
            get
            {
                return this.consumerNLR24MonthlyPaymentField;
            }
            set
            {
                this.consumerNLR24MonthlyPaymentField = value;
            }
        }




        [XmlElement("ConsumerNLR24MonthlyPaymentHeader")]
        public ConsumerConsumerNLR24MonthlyPaymentHeader ConsumerNLR24MonthlyPaymentHeader
        {
            get
            {
                return this.consumerNLR24MonthlyPaymentHeaderField;
            }
            set
            {
                this.consumerNLR24MonthlyPaymentHeaderField = value;
            }
        }



        [XmlElement("ConsumerNLRDefinition")]
        public ConsumerConsumerNLRDefinition[] ConsumerNLRDefinition
        {
            get
            {
                return this.consumerNLRDefinitionField;
            }
            set
            {
                this.consumerNLRDefinitionField = value;
            }
        }



        [XmlElement("ConsumerAdverseInfo")]
        public ConsumerConsumerAdverseInformation[] ConsumerAdverseInformation
        {
            get
            {
                return this.consumerAdverseInformationField;
            }
            set
            {
                this.consumerAdverseInformationField = value;
            }
        }

        [System.Xml.Serialization.XmlElement("ConsumerDefaultAlert")]
        public ConsumerConsumerDefaultAlert[] ConsumerDefaultAlert
        {
            get
            {
                return this.consumerDefaultAlertField;
            }
            set
            {
                this.consumerDefaultAlertField = value;
            }
        }

        [XmlElement("ConsumerJudgement")]
        public ConsumerConsumerJudgement[] ConsumerJudgement
        {
            get
            {
                return this.consumerJudgementField;
            }
            set
            {
                this.consumerJudgementField = value;
            }
        }



        public ConsumerConsumerAdminOrder[] ConsumerAdminOrder
        {
            get
            {
                return this.consumerAdminOrderField;
            }
            set
            {
                this.consumerAdminOrderField = value;
            }
        }




        [XmlElement("ConsumerSequestration")]
        public ConsumerConsumerSequestration[] ConsumerSequestration
        {
            get
            {
                return this.consumerSequestrationField;
            }
            set
            {
                this.consumerSequestrationField = value;
            }
        }


        public ConsumerConsumerRehabilitationOrder[] ConsumerRehabilitationOrder
        {
            get
            {
                return this.consumerRehabilitationOrderField;
            }
            set
            {
                this.consumerRehabilitationOrderField = value;
            }
        }




        public ConsumerConsumerDebtReviewStatus ConsumerDebtReviewStatus
        {
            get
            {
                return this.consumerDebtReviewStatusField;
            }
            set
            {
                this.consumerDebtReviewStatusField = value;
            }
        }



        [System.Xml.Serialization.XmlElement("XDSPaymentNotification")]
        public ConsumerXDSPaymentNotification[] XDSPaymentNotification
        {
            get
            {
                return this.xDSPaymentNotificationField;
            }
            set
            {
                this.xDSPaymentNotificationField = value;
            }
        }


        [XmlElement("ConsumerTelephoneHistory")]
        public ConsumerConsumerTelephoneHistory[] ConsumerTelephoneHistory
        {
            get
            {
                return this.consumerTelephoneHistoryField;
            }
            set
            {
                this.consumerTelephoneHistoryField = value;
            }
        }

        [XmlElement("ConsumerEmailHistory")]
        public ConsumerConsumerEmailHistory[] ConsumerEmailHistory
        {
            get
            {
                return this.consumerEmailHistoryField;
            }
            set
            {
                this.consumerEmailHistoryField = value;
            }
        }

        [XmlElement("ConsumerEmploymentHistory")]
        public ConsumerConsumerEmploymentHistory[] ConsumerEmploymentHistory
        {
            get
            {
                return this.consumerEmploymentHistoryField;
            }
            set
            {
                this.consumerEmploymentHistoryField = value;
            }
        }

        [XmlElement("ConsumerTelephoneLinkageHome")]
        public ConsumerConsumerTelephoneLinkageHome[] ConsumerTelephoneLinkageHome
        {
            get
            {
                return this.consumerTelephoneLinkageHomeField;
            }
            set
            {
                this.consumerTelephoneLinkageHomeField = value;
            }
        }

        [XmlElement("ConsumerTelephoneLinkageWork")]
        public ConsumerConsumerTelephoneLinkageWork[] ConsumerTelephoneLinkageWork
        {
            get
            {
                return this.consumerTelephoneLinkageWorkField;
            }
            set
            {
                this.consumerTelephoneLinkageWorkField = value;
            }
        }

        [XmlElement("ConsumerTelephoneLinkageCellular")]
        public ConsumerConsumerTelephoneLinkageCellular[] ConsumerTelephoneLinkageCellular
        {
            get
            {
                return this.consumerTelephoneLinkageCellularField;
            }
            set
            {
                this.consumerTelephoneLinkageCellularField = value;
            }
        }


        /// <remarks/>
        [XmlElement("ConsumerAddressConfirmation")]
        public ConsumerConsumerAddressConfirmation[] ConsumerAddressConfirmation
        {
            get
            {
                return this.consumerAddressConfirmationField;
            }
            set
            {
                this.consumerAddressConfirmationField = value;
            }
        }

        /// <remarks/>
        [XmlElement("ConsumerContactConfirmation")]
        public ConsumerConsumerContactConfirmation[] ConsumerContactConfirmation
        {
            get
            {
                return this.consumerContactConfirmationField;
            }
            set
            {
                this.consumerContactConfirmationField = value;
            }
        }

        [XmlElement("ConsumerPropertyInformation")]
        public ConsumerConsumerPropertyInformation[] ConsumerPropertyInformation
        {
            get
            {
                return this.consumerPropertyInformationField;
            }
            set
            {
                this.consumerPropertyInformationField = value;
            }
        }

        public ConsumerConsumerOtherContactInfoAddress[] ConsumerOtherContactInfoAddress
        {
            get
            {
                return this.consumerOtherContactInfoAddressField;
            }
            set
            {
                this.consumerOtherContactInfoAddressField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute("ConsumerOtherContactInfoTelephone")]
        public ConsumerConsumerOtherContactInfoTelephone[] ConsumerOtherContactInfoTelephone
        {
            get
            {
                return this.consumerOtherContactInfoTelephoneField;
            }
            set
            {
                this.consumerOtherContactInfoTelephoneField = value;
            }
        }


        [System.Xml.Serialization.XmlElementAttribute("ConsumerEmploymentConfirmation")]
        public ConsumerConsumerEmploymentConfirmation[] ConsumerEmploymentConfirmation
        {
            get
            {
                return this.consumerEmploymentConfirmationField;
            }
            set
            {
                this.consumerEmploymentConfirmationField = value;
            }
        }


        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("IndustryPayments")]
        public ConsumerIndustryPayments[] IndustryPayments
        {
            get
            {
                return this.industryPaymentsField;
            }
            set
            {
                this.industryPaymentsField = value;
            }
        }



    }

 




}

