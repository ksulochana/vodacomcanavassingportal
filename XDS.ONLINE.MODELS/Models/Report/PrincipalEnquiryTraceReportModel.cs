﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDS.ONLINE.MODELS
{
    [System.Xml.Serialization.XmlRoot("Director")]
    public class PrincipalEnquiryTraceReportModel
    {
        private ConsumerReportInformation reportInformationField;
        //private ConsumerConsumerDetail consumerDetailField;
        public DirectorConsumerDetail directorConsumerDetail;
        private ConsumerConsumerFraudIndicatorsSummary consumerFraudIndicatorsSummaryField;
        private ConsumerConsumerDirectorSummary consumerDirectorSummaryField;
        private ConsumerConsumerDefaultAlert[] consumerDefaultAlertField;
        private ConsumerConsumerEnquiryHistory[] consumerEnquiryHistoryField;
        private ConsumerConsumerAddressHistory[] consumerAddressHistoryField;
        private ConsumerConsumerMaritalStatusEnquiry consumerMaritalStatusEnquiryField;
        private ConsumerSubscriberInputDetails subscriberInputDetailsField;
        private ConsumerConsumerTelephoneHistory[] consumerTelephoneHistoryField;
        private ConsumerConsumerEmailHistory[] consumerEmailHistoryField;
        private ConsumerConsumerEmploymentHistory[] consumerEmploymentHistoryField;

        private ConsumerConsumerAdverseInformation[] consumerAdverseInformationField;
        private ConsumerConsumerJudgement[] consumerJudgementField;
        private ConsumerConsumerAdminOrder[] consumerAdminOrderField;
        private ConsumerConsumerSequestration[] consumerSequestrationField;
        private ConsumerConsumerRehabilitationOrder[] consumerRehabilitationOrderField;
        private ConsumerConsumerDebtReviewStatus[] consumerDebtReviewStatusField;
        private ConsumerXDSPaymentNotification[] xDSPaymentNotificationField;
        private ConsumerConsumerPropertyInformationSummary[] consumerPropertyInformationSummaryField;
        private ConsumerConsumerPropertyInformation[] consumerPropertyInformationField;
        private ConsumerConsumerDirectorShipLink[] consumerDirectorShipLinkField;

        private DirectorEnquiryHistory[] directorEnquiryHistory;

        /// <remarks/>
        public ConsumerReportInformation ReportInformation
        {
            get
            {
                return this.reportInformationField;
            }
            set
            {
                this.reportInformationField = value;
            }
        }

        /// <remarks/>
        //public ConsumerConsumerDetail ConsumerDetail
        //{
        //    get
        //    {
        //        return this.consumerDetailField;
        //    }
        //    set
        //    {
        //        this.consumerDetailField = value;
        //    }
        //}

        [System.Xml.Serialization.XmlElement("ConsumerDetail")]
        public DirectorConsumerDetail ConsumerDetail
        {
            get
            {
                return this.directorConsumerDetail;
            }
            set
            {
                this.directorConsumerDetail = value;
            }
        }


        /// <remarks/>
        public ConsumerConsumerFraudIndicatorsSummary ConsumerFraudIndicatorsSummary
        {
            get
            {
                return this.consumerFraudIndicatorsSummaryField;
            }
            set
            {
                this.consumerFraudIndicatorsSummaryField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerDirectorSummary ConsumerDirectorSummary
        {
            get
            {
                return this.consumerDirectorSummaryField;
            }
            set
            {
                this.consumerDirectorSummaryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ConsumerDefaultAlert")]
        public ConsumerConsumerDefaultAlert[] ConsumerDefaultAlert
        {
            get
            {
                return this.consumerDefaultAlertField;
            }
            set
            {
                this.consumerDefaultAlertField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ConsumerEnquiryHistory")]
        public ConsumerConsumerEnquiryHistory[] ConsumerEnquiryHistory
        {
            get
            {
                return this.consumerEnquiryHistoryField;
            }
            set
            {
                this.consumerEnquiryHistoryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElement("ConsumerAddressHistory")]
        public ConsumerConsumerAddressHistory[] ConsumerAddressHistory
        {
            get
            {
                return this.consumerAddressHistoryField;
            }
            set
            {
                this.consumerAddressHistoryField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerMaritalStatusEnquiry ConsumerMaritalStatusEnquiry
        {
            get
            {
                return this.consumerMaritalStatusEnquiryField;
            }
            set
            {
                this.consumerMaritalStatusEnquiryField = value;
            }
        }

        /// <remarks/>
        public ConsumerSubscriberInputDetails SubscriberInputDetails
        {
            get
            {
                return this.subscriberInputDetailsField;
            }
            set
            {
                this.subscriberInputDetailsField = value;
            }
        }

        [System.Xml.Serialization.XmlElement("ConsumerTelephoneHistory")]
        public ConsumerConsumerTelephoneHistory[] ConsumerTelephoneHistory
        {
            get
            {
                return this.consumerTelephoneHistoryField;
            }
            set
            {
                this.consumerTelephoneHistoryField = value;
            }
        }

        [System.Xml.Serialization.XmlElement("ConsumerEmailHistory")]
        public ConsumerConsumerEmailHistory[] ConsumerEmailHistory
        {
            get
            {
                return this.consumerEmailHistoryField;
            }
            set
            {
                this.consumerEmailHistoryField = value;
            }
        }

        [System.Xml.Serialization.XmlElement("ConsumerEmploymentHistory")]
        public ConsumerConsumerEmploymentHistory[] ConsumerEmploymentHistory
        {
            get
            {
                return this.consumerEmploymentHistoryField;
            }
            set
            {
                this.consumerEmploymentHistoryField = value;
            }
        }

        [System.Xml.Serialization.XmlElement("ConsumerAdverseInformation")]
        public ConsumerConsumerAdverseInformation[] ConsumerAdverseInformation
        {
            get
            {
                return this.consumerAdverseInformationField;
            }
            set
            {
                this.consumerAdverseInformationField = value;
            }
        }

        [System.Xml.Serialization.XmlElement("ConsumerJudgement")]
        public ConsumerConsumerJudgement[] ConsumerJudgement
        {
            get
            {
                return this.consumerJudgementField;
            }
            set
            {
                this.consumerJudgementField = value;
            }
        }

        public ConsumerConsumerAdminOrder[] ConsumerAdminOrder
        {
            get
            {
                return this.consumerAdminOrderField;
            }
            set
            {
                this.consumerAdminOrderField = value;
            }
        }

        public ConsumerConsumerSequestration[] ConsumerSequestration
        {
            get
            {
                return this.consumerSequestrationField;
            }
            set
            {
                this.consumerSequestrationField = value;
            }
        }

        public ConsumerConsumerRehabilitationOrder[] ConsumerRehabilitationOrder
        {
            get
            {
                return this.consumerRehabilitationOrderField;
            }
            set
            {
                this.consumerRehabilitationOrderField = value;
            }
        }

        [System.Xml.Serialization.XmlElement("ConsumerDebtReviewStatus")]
        public ConsumerConsumerDebtReviewStatus[] ConsumerDebtReviewStatus
        {
            get
            {
                return this.consumerDebtReviewStatusField;
            }
            set
            {
                this.consumerDebtReviewStatusField = value;
            }
        }

        [System.Xml.Serialization.XmlElement("XDSPaymentNotification")]
        public ConsumerXDSPaymentNotification[] XDSPaymentNotification
        {
            get
            {
                return this.xDSPaymentNotificationField;
            }
            set
            {
                this.xDSPaymentNotificationField = value;
            }
        }

        [System.Xml.Serialization.XmlElement("ConsumerPropertyInformationSummary")]
        public ConsumerConsumerPropertyInformationSummary[] ConsumerPropertyInformationSummary
        {
            get
            {
                return this.consumerPropertyInformationSummaryField;
            }
            set
            {
                this.consumerPropertyInformationSummaryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElement("ConsumerPropertyInformation")]
        public ConsumerConsumerPropertyInformation[] ConsumerPropertyInformation
        {
            get
            {
                return this.consumerPropertyInformationField;
            }
            set
            {
                this.consumerPropertyInformationField = value;
            }
        }

        [System.Xml.Serialization.XmlElement("ConsumerDirectorShipLink")]
        public ConsumerConsumerDirectorShipLink[] ConsumerDirectorShipLink
        {
            get
            {
                return this.consumerDirectorShipLinkField;
            }
            set
            {
                this.consumerDirectorShipLinkField = value;
            }
        }

        [System.Xml.Serialization.XmlElement("DirectorEnquiryHistory")]
        public DirectorEnquiryHistory[] DirectorEnquiryHistory
        {
            get
            {
                return this.directorEnquiryHistory;
            }
            set
            {
                this.directorEnquiryHistory = value;
            }
        }

    }

    //public partial class ConsumerReportInformation : Consumer
    //{

    //}

    ////public partial class ConsumerConsumerDetail : Consumer
    ////{

    ////}

    //public partial class DirectorConsumerDetail : Consumer
    //{

    //}


    //public partial class ConsumerConsumerFraudIndicatorsSummary : Consumer
    //{

    //}

    //public partial class ConsumerConsumerDirectorSummary : Consumer
    //{

    //}

    //public partial class ConsumerConsumerDefaultAlert : Consumer
    //{

    //}

    //public partial class ConsumerConsumerEnquiryHistory : Consumer
    //{

    //}

    //public partial class ConsumerConsumerAddressHistory : Consumer
    //{

    //}

    //public partial class ConsumerConsumerMaritalStatusEnquiry : Consumer
    //{

    //}

    //public partial class ConsumerSubscriberInputDetails : Consumer
    //{

    //}

    //public partial class ConsumerConsumerTelephoneHistory : Consumer
    //{ }

    //public partial class ConsumerConsumerEmailHistory : Consumer
    //{ }

    //public partial class ConsumerConsumerEmploymentHistory : Consumer
    //{ }

    //public partial class ConsumerConsumerAdverseInformation : Consumer
    //{ }

    //public partial class ConsumerConsumerJudgement : Consumer
    //{ }

    //public partial class ConsumerConsumerAdminOrder : Consumer
    //{ }

    //public partial class ConsumerConsumerSequestration : Consumer
    //{ }

    //public partial class ConsumerConsumerRehabilitationOrder : Consumer
    //{ }

    //public partial class ConsumerConsumerDebtReviewStatus : Consumer
    //{ }

    //public partial class ConsumerXDSPaymentNotification : Consumer
    //{ }

    //public partial class ConsumerConsumerPropertyInformationSummary : Consumer
    //{ }

    //public partial class ConsumerConsumerPropertyInformation : Consumer
    //{ }

    //public partial class ConsumerConsumerDirectorShipLink : Consumer
    //{ }
}
