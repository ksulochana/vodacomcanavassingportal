﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace XDS.ONLINE.MODELS
{

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class AuthenticationProcess
    {

        private CurrentObjectState currentObjectStateField;

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.web.xds.co.za/XDSConnectWS")]
        public CurrentObjectState CurrentObjectState
        {
            get
            {
                return this.currentObjectStateField;
            }
            set
            {
                this.currentObjectStateField = value;
            }
        }
    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.web.xds.co.za/XDSConnectWS")]
    //[System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.web.xds.co.za/XDSConnectWS", IsNullable = false)]
    public partial class CurrentObjectState
    {

        private CurrentObjectStateAuthenticationDocument authenticationDocumentField;

        private string currentQuestionNumField;

        /// <remarks/>
        public CurrentObjectStateAuthenticationDocument AuthenticationDocument
        {
            get
            {
                return this.authenticationDocumentField;
            }
            set
            {
                this.authenticationDocumentField = value;
            }
        }

        /// <remarks/>
        public string CurrentQuestionNum
        {
            get
            {
                return this.currentQuestionNumField;
            }
            set
            {
                this.currentQuestionNumField = value;
            }
        }
    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.web.xds.co.za/XDSConnectWS")]
    public partial class CurrentObjectStateAuthenticationDocument
    {

        private string subscriberIDField;

        private string subscriberAuthenticationIDField;

        private string authenticationDateField;

        private string authenticationStatusIndField;

        private string authenticationTypeIndField;

        private string referenceNoField;

        private string totalQuestionPointValueField;

        private string requiredAuthenticatedPercField;

        private string authenticatedPercField;

        private string repeatAuthenticationMessageField;

        private string actionedBySystemUserIDField;

        private string firstNameField;

        private string secondNameField;

        private string surnameField;

        private string iDNoField;

        private string passportNoField;

        private string birthDateField;

        private string genderField;

        private string ha_FirstNameField;

        private string ha_SecondNameField;

        private string ha_SurnameField;

        private string ha_IDNoField;

        private string ha_DeaseasedStatusField;

        private string ha_DeaseasedDateField;

        private string ha_CauseOfDeathField;

        private string ha_IDIssuedDateField;

        private string consumerAccountAgeMessageField;

        private string encryptedReferenceNoField;

        private string authenticationCommentField;

        private string sAFPSIndicatorField;

        private string isUserBlockedField;

        private string personalQuestionsenabledField;

        private string blockingEnabledFlagField;

        private string referToFraudField;

        private string blockidField;

        private string blockingReasonField;

        private string questionTimeoutField;

        private string errorMessageField;

        private string voidorFraudReasonField;

        private string noofAttemptsRemainingField;

        private string oTPStatusField;

        private string oTPEnabledField;

        private string oTPNotGeneratedReasonField;

        private string oTPValueField;

        private string authenticationStatusReasonField;

        private string sAFPSMessageField;

        private CurrentObjectStateAuthenticationDocumentQuestionDocument[] questionsField;

        private CurrentObjectStateAuthenticationDocumentAuthenticationHistoryDocument[] authenticationHistoryField;

        private CurrentObjectStateAuthenticationDocumentVoidReasonsDocument[] voidReasonsField;

        private CurrentObjectStateAuthenticationDocumentFraudReasonsDocument[] fraudReasonsField;

        private PersonalQuestionsDocument[] PersonalQuestionsField;

        private OTPNumbers otpNumbers;

        /// <remarks/>
        public string SubscriberID
        {
            get
            {
                return this.subscriberIDField;
            }
            set
            {
                this.subscriberIDField = value;
            }
        }

        /// <remarks/>
        public string SubscriberAuthenticationID
        {
            get
            {
                return this.subscriberAuthenticationIDField;
            }
            set
            {
                this.subscriberAuthenticationIDField = value;
            }
        }

        /// <remarks/>
        public string AuthenticationDate
        {
            get
            {
                return this.authenticationDateField;
            }
            set
            {
                this.authenticationDateField = value;
            }
        }

        /// <remarks/>
        public string AuthenticationStatusInd
        {
            get
            {
                return this.authenticationStatusIndField;
            }
            set
            {
                this.authenticationStatusIndField = value;
            }
        }

        /// <remarks/>
        public string AuthenticationTypeInd
        {
            get
            {
                return this.authenticationTypeIndField;
            }
            set
            {
                this.authenticationTypeIndField = value;
            }
        }

        /// <remarks/>
        public string ReferenceNo
        {
            get
            {
                return this.referenceNoField;
            }
            set
            {
                this.referenceNoField = value;
            }
        }

        /// <remarks/>
        public string TotalQuestionPointValue
        {
            get
            {
                return this.totalQuestionPointValueField;
            }
            set
            {
                this.totalQuestionPointValueField = value;
            }
        }

        /// <remarks/>
        public string RequiredAuthenticatedPerc
        {
            get
            {
                return XDS.ONLINE.MODELS.General.TrimDecimalToTwoDecimalPoint(this.requiredAuthenticatedPercField); ;
            }
            set
            {
                this.requiredAuthenticatedPercField = value;
            }
        }

        /// <remarks/>
        public string AuthenticatedPerc
        {
            get
            {
                return XDS.ONLINE.MODELS.General.TrimDecimalToTwoDecimalPoint(this.authenticatedPercField);
            }
            set
            {
                this.authenticatedPercField = value;
            }
        }

        /// <remarks/>
        public string RepeatAuthenticationMessage
        {
            get
            {
                return this.repeatAuthenticationMessageField;
            }
            set
            {
                this.repeatAuthenticationMessageField = value;
            }
        }

        /// <remarks/>
        public string ActionedBySystemUserID
        {
            get
            {
                return this.actionedBySystemUserIDField;
            }
            set
            {
                this.actionedBySystemUserIDField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        public string SecondName
        {
            get
            {
                return this.secondNameField;
            }
            set
            {
                this.secondNameField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string IDNo
        {
            get
            {
                return this.iDNoField;
            }
            set
            {
                this.iDNoField = value;
            }
        }

        /// <remarks/>
        public string PassportNo
        {
            get
            {
                return this.passportNoField;
            }
            set
            {
                this.passportNoField = value;
            }
        }

        /// <remarks/>
        public string BirthDate
        {
            get
            {
                return this.birthDateField;
            }
            set
            {
                this.birthDateField = value;
            }
        }

        /// <remarks/>
        public string Gender
        {
            get
            {
                return this.genderField;
            }
            set
            {
                this.genderField = value;
            }
        }

        public string Ha_FirstName
        {
            get
            {
                return this.ha_FirstNameField;
            }
            set
            {
                this.ha_FirstNameField = value;
            }
        }

        public string Ha_SecondName
        {
            get
            {
                return this.ha_SecondNameField;
            }
            set
            {
                this.ha_SecondNameField = value;
            }
        }

        public string Ha_Surname
        {
            get
            {
                return this.ha_SurnameField;
            }
            set
            {
                this.ha_SurnameField = value;
            }
        }

        public string Ha_IDNo
        {
            get
            {
                return this.ha_IDNoField;
            }
            set
            {
                this.ha_IDNoField = value;
            }
        }

        public string Ha_DeaseasedStatus
        {
            get
            {
                return this.ha_DeaseasedStatusField;
            }
            set
            {
                this.ha_DeaseasedStatusField = value;
            }
        }

        public string Ha_DeaseasedDate
        {
            get
            {
                return this.ha_DeaseasedDateField;
            }
            set
            {
                this.ha_DeaseasedDateField = value;
            }
        }

        public string Ha_CauseOfDeath
        {
            get
            {
                return this.ha_CauseOfDeathField;
            }
            set
            {
                this.ha_CauseOfDeathField = value;
            }
        }

        public string Ha_IDIssuedDate
        {
            get
            {
                return this.ha_IDIssuedDateField;
            }
            set
            {
                this.ha_IDIssuedDateField = value;
            }
        }

        /// <remarks/>
        public string ConsumerAccountAgeMessage
        {
            get
            {
                return this.consumerAccountAgeMessageField;
            }
            set
            {
                this.consumerAccountAgeMessageField = value;
            }
        }

        /// <remarks/>
        public string EncryptedReferenceNo
        {
            get
            {
                return this.encryptedReferenceNoField;
            }
            set
            {
                this.encryptedReferenceNoField = value;
            }
        }

        /// <remarks/>
        public string AuthenticationComment
        {
            get
            {
                return this.authenticationCommentField;
            }
            set
            {
                this.authenticationCommentField = value;
            }
        }

        /// <remarks/>
        public string SAFPSIndicator
        {
            get
            {
                return this.sAFPSIndicatorField;
            }
            set
            {
                this.sAFPSIndicatorField = value;
            }
        }

        /// <remarks/>
        public string IsUserBlocked
        {
            get
            {
                return this.isUserBlockedField;
            }
            set
            {
                this.isUserBlockedField = value;
            }
        }

        /// <remarks/>
        public string PersonalQuestionsenabled
        {
            get
            {
                return this.personalQuestionsenabledField;
            }
            set
            {
                this.personalQuestionsenabledField = value;
            }
        }

        /// <remarks/>
        public string BlockingEnabledFlag
        {
            get
            {
                return this.blockingEnabledFlagField;
            }
            set
            {
                this.blockingEnabledFlagField = value;
            }
        }

        /// <remarks/>
        public string ReferToFraud
        {
            get
            {
                return this.referToFraudField;
            }
            set
            {
                this.referToFraudField = value;
            }
        }

        /// <remarks/>
        public string blockid
        {
            get
            {
                return this.blockidField;
            }
            set
            {
                this.blockidField = value;
            }
        }

        /// <remarks/>
        public string BlockingReason
        {
            get
            {
                return this.blockingReasonField;
            }
            set
            {
                this.blockingReasonField = value;
            }
        }

        /// <remarks/>
        public string QuestionTimeout
        {
            get
            {
                return this.questionTimeoutField;
            }
            set
            {
                this.questionTimeoutField = value;
            }
        }

        /// <remarks/>
        public string ErrorMessage
        {
            get
            {
                return this.errorMessageField;
            }
            set
            {
                this.errorMessageField = value;
            }
        }

        /// <remarks/>
        public string VoidorFraudReason
        {
            get
            {
                return this.voidorFraudReasonField;
            }
            set
            {
                this.voidorFraudReasonField = value;
            }
        }

        /// <remarks/>
        public string NoofAttemptsRemaining
        {
            get
            {
                return this.noofAttemptsRemainingField;
            }
            set
            {
                this.noofAttemptsRemainingField = value;
            }
        }

        /// <remarks/>
        public string OTPStatus
        {
            get
            {
                return this.oTPStatusField;
            }
            set
            {
                this.oTPStatusField = value;
            }
        }

        /// <remarks/>
        public string OTPEnabled
        {
            get
            {
                return this.oTPEnabledField;
            }
            set
            {
                this.oTPEnabledField = value;
            }
        }

        /// <remarks/>
        public string OTPNotGeneratedReason
        {
            get
            {
                return this.oTPNotGeneratedReasonField;
            }
            set
            {
                this.oTPNotGeneratedReasonField = value;
            }
        }

        /// <remarks/>
        public string OTPValue
        {
            get
            {
                return this.oTPValueField;
            }
            set
            {
                this.oTPValueField = value;
            }
        }

        /// <remarks/>
        public string AuthenticationStatusReason
        {
            get
            {
                return this.authenticationStatusReasonField;
            }
            set
            {
                this.authenticationStatusReasonField = value;
            }
        }

        /// <remarks/>
        public string SAFPSMessage
        {
            get
            {
                return this.sAFPSMessageField;
            }
            set
            {
                this.sAFPSMessageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("QuestionDocument", IsNullable = false)]
        public CurrentObjectStateAuthenticationDocumentQuestionDocument[] Questions
        {
            get
            {
                return this.questionsField;
            }
            set
            {
                this.questionsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("AuthenticationHistoryDocument", IsNullable = false)]
        public CurrentObjectStateAuthenticationDocumentAuthenticationHistoryDocument[] AuthenticationHistory
        {
            get
            {
                return this.authenticationHistoryField;
            }
            set
            {
                this.authenticationHistoryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("VoidReasonsDocument", IsNullable = false)]
        public CurrentObjectStateAuthenticationDocumentVoidReasonsDocument[] VoidReasons
        {
            get
            {
                return this.voidReasonsField;
            }
            set
            {
                this.voidReasonsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("FraudReasonsDocument", IsNullable = false)]
        public CurrentObjectStateAuthenticationDocumentFraudReasonsDocument[] FraudReasons
        {
            get
            {
                return this.fraudReasonsField;
            }
            set
            {
                this.fraudReasonsField = value;
            }
        }

        public PersonalQuestionsDocument[] PersonalQuestions
        {
            get
            {
                return this.PersonalQuestionsField;
            }
            set
            {
                this.PersonalQuestionsField = value;
            }
        }

        public OTPNumbers OTPNumbers
        {
            get
            {
                return this.otpNumbers;
            }
            set
            {
                this.otpNumbers = value;
            }
        }
    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.web.xds.co.za/XDSConnectWS")]
    public partial class CurrentObjectStateAuthenticationDocumentQuestionDocument
    {

        private string productAuthenticationQuestionIDField;

        private string questionField;

        private string answerStatusIndField;

        private string questionPointValueField;

        private string requiredNoOfAnswersField;

        private CurrentObjectStateAuthenticationDocumentQuestionDocumentAnswerDocument[] answersField;

        /// <remarks/>
        public string ProductAuthenticationQuestionID
        {
            get
            {
                return this.productAuthenticationQuestionIDField;
            }
            set
            {
                this.productAuthenticationQuestionIDField = value;
            }
        }

        /// <remarks/>
        public string Question
        {
            get
            {
                return this.questionField;
            }
            set
            {
                this.questionField = value;
            }
        }

        /// <remarks/>
        public string AnswerStatusInd
        {
            get
            {
                return this.answerStatusIndField;
            }
            set
            {
                this.answerStatusIndField = value;
            }
        }

        /// <remarks/>
        public string QuestionPointValue
        {
            get
            {
                return this.questionPointValueField;
            }
            set
            {
                this.questionPointValueField = value;
            }
        }

        /// <remarks/>
        public string RequiredNoOfAnswers
        {
            get
            {
                return this.requiredNoOfAnswersField;
            }
            set
            {
                this.requiredNoOfAnswersField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("AnswerDocument", IsNullable = false)]
        public CurrentObjectStateAuthenticationDocumentQuestionDocumentAnswerDocument[] Answers
        {
            get
            {
                return this.answersField;
            }
            set
            {
                this.answersField = value;
            }
        }
    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.web.xds.co.za/XDSConnectWS")]
    public partial class CurrentObjectStateAuthenticationDocumentQuestionDocumentAnswerDocument
    {

        private string answerIDField;

        private string answerField;

        private string isEnteredAnswerYNField;

        /// <remarks/>
        public string AnswerID
        {
            get
            {
                return this.answerIDField;
            }
            set
            {
                this.answerIDField = value;
            }
        }

        /// <remarks/>
        public string Answer
        {
            get
            {
                return this.answerField;
            }
            set
            {
                this.answerField = value;
            }
        }

        /// <remarks/>
        public string IsEnteredAnswerYN
        {
            get
            {
                return this.isEnteredAnswerYNField;
            }
            set
            {
                this.isEnteredAnswerYNField = value;
            }
        }
    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.web.xds.co.za/XDSConnectWS")]
    public partial class CurrentObjectStateAuthenticationDocumentAuthenticationHistoryDocument
    {

        private string bureauDateField;

        private string subscriberNameField;

        private string cellularNumberField;

        private string authenticationStatusField;

        private string authenticationPercField;

        private string emailAddressField;

        /// <remarks/>
        public string BureauDate
        {
            get
            {
                return this.bureauDateField;
            }
            set
            {
                this.bureauDateField = value;
            }
        }

        /// <remarks/>
        public string SubscriberName
        {
            get
            {
                return this.subscriberNameField;
            }
            set
            {
                this.subscriberNameField = value;
            }
        }

        /// <remarks/>
        public string cellularNumber
        {
            get
            {
                return this.cellularNumberField;
            }
            set
            {
                this.cellularNumberField = value;
            }
        }

        /// <remarks/>
        public string AuthenticationStatus
        {
            get
            {
                return this.authenticationStatusField;
            }
            set
            {
                this.authenticationStatusField = value;
            }
        }

        /// <remarks/>
        public string AuthenticationPerc
        {
            get
            {
                return this.authenticationPercField;
            }
            set
            {
                this.authenticationPercField = value;
            }
        }

        /// <remarks/>
        public string EmailAddress
        {
            get
            {
                return this.emailAddressField;
            }
            set
            {
                this.emailAddressField = value;
            }
        }
    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.web.xds.co.za/XDSConnectWS")]
    public partial class CurrentObjectStateAuthenticationDocumentVoidReasonsDocument
    {

        private string voidReasonIDField;

        private string voidReasonField;

        private string isEnteredReasonYNField;

        /// <remarks/>
        public string VoidReasonID
        {
            get
            {
                return this.voidReasonIDField;
            }
            set
            {
                this.voidReasonIDField = value;
            }
        }

        /// <remarks/>
        public string VoidReason
        {
            get
            {
                return this.voidReasonField;
            }
            set
            {
                this.voidReasonField = value;
            }
        }

        /// <remarks/>
        public string IsEnteredReasonYN
        {
            get
            {
                return this.isEnteredReasonYNField;
            }
            set
            {
                this.isEnteredReasonYNField = value;
            }
        }
    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.web.xds.co.za/XDSConnectWS")]
    public partial class CurrentObjectStateAuthenticationDocumentFraudReasonsDocument
    {

        private string fraudReasonIDField;

        private string fraudReasonField;

        private string isEnteredReasonYNField;

        /// <remarks/>
        public string FraudReasonID
        {
            get
            {
                return this.fraudReasonIDField;
            }
            set
            {
                this.fraudReasonIDField = value;
            }
        }

        /// <remarks/>
        public string FraudReason
        {
            get
            {
                return this.fraudReasonField;
            }
            set
            {
                this.fraudReasonField = value;
            }
        }

        /// <remarks/>
        public string IsEnteredReasonYN
        {
            get
            {
                return this.isEnteredReasonYNField;
            }
            set
            {
                this.isEnteredReasonYNField = value;
            }
        }
    }

    //[System.Diagnostics.DebuggerStepThroughAttribute()]
    //[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    //[System.Runtime.Serialization.DataContractAttribute(Name = "PersonalQuestionsDocument", Namespace = "http://www.web.xds.co.za/XDSConnectAuthWS")]
    //[System.SerializableAttribute()]
    public partial class PersonalQuestionsDocument
    {

        //[System.NonSerializedAttribute()]
        //private System.Runtime.Serialization.ExtensionDataObject extensionDataField;

        private string PQuestionIDField;

        //[System.Runtime.Serialization.OptionalFieldAttribute()]
        private string PQuestionField;

        //[System.Runtime.Serialization.OptionalFieldAttribute()]
        private string PAnswerField;

        //[global::System.ComponentModel.BrowsableAttribute(false)]
        //public System.Runtime.Serialization.ExtensionDataObject ExtensionData
        //{
        //    get
        //    {
        //        return this.extensionDataField;
        //    }
        //    set
        //    {
        //        this.extensionDataField = value;
        //    }
        //}

        //[System.Runtime.Serialization.DataMemberAttribute(IsRequired = true)]
        public string PQuestionID
        {
            get
            {
                return this.PQuestionIDField;
            }
            set
            {
                //if ((this.PQuestionIDField.Equals(value) != true))
                //{
                this.PQuestionIDField = value;
                //this.RaisePropertyChanged("PQuestionID");
                //}
            }
        }

        //[System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue = false, Order = 1)]
        public string PQuestion
        {
            get
            {
                return this.PQuestionField;
            }
            set
            {
                //if ((object.ReferenceEquals(this.PQuestionField, value) != true))
                //{
                this.PQuestionField = value;
                // this.RaisePropertyChanged("PQuestion");
                //}
            }
        }

        //[System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue = false, Order = 2)]
        public string PAnswer
        {
            get
            {
                return this.PAnswerField;
            }
            set
            {
                //if ((object.ReferenceEquals(this.PAnswerField, value) != true))
                //{
                this.PAnswerField = value;
                //this.RaisePropertyChanged("PAnswer");
                //}
            }
        }

        //public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        //protected void RaisePropertyChanged(string propertyName)
        //{
        //    System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
        //    if ((propertyChanged != null))
        //    {
        //        propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
        //    }
        //}
    }

}
