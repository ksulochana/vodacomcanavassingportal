﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDS.ONLINE.MODELS
{

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class AuthenticationProcessModel
    {

        private CurrentObjectState currentObjectStateField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.web.xds.co.za/XDSConnectWS")]
        public CurrentObjectState CurrentObjectState
        {
            get
            {
                return this.currentObjectStateField;
            }
            set
            {
                this.currentObjectStateField = value;
            }
        }

        public string QuestionAnswersValue { get; set; }

        public string Comment { get; set; }

        public string ProcessAction { get; set; }

        public string EnteredOTPValue { get; set; }

        public string ReferToFraudReason { get; set; }

        public string ReferToFraudOther { get; set; }

        public string VoidReason { get; set; }

        public string VoidReasonOther { get; set; }

        public string EnquiryResultID { get; set; }

        

    }
}
