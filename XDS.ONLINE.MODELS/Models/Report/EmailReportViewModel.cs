﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;

namespace XDS.ONLINE.MODELS
{
    public class EmailReportViewModel
    {
        [Display(Name = "From:")]
        public string FromName { get; set; }

        [Display(Name = "Reply-To (Email)")]
        [StringLength(250, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(EmailAddress.Pattern, ErrorMessage = EmailAddress.Message)]
        [Required(ErrorMessage = "Reply-To (Email) is required.")]
        public string ReplyToEmailAddress { get; set; }

        [Display(Name = "To")]
        [StringLength(250, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(EmailAddresses.Pattern, ErrorMessage = EmailAddresses.Message)]
        [Required(ErrorMessage = "To email address is required.")]
        public string ToEmailAddress { get; set; }

        [Display(Name = "Body")]
        [StringLength(1000, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(GeneralText.Pattern, ErrorMessage = GeneralText.Message)]
        public string EmailBody { get; set; }

        public int EnquiryID { get; set; }
        public int EnquiryResultID { get; set; }
        public int ProductID { get; set; }
    }
}
