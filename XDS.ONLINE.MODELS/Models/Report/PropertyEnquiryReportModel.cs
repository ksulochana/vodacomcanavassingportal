﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace XDS.ONLINE.MODELS
{
    [System.Xml.Serialization.XmlRoot("Property")]
    public class PropertyEnquiryReportModel
    {
            
            private PropertyReportInformation reportInformationField;

            private PropertyPropertyInformation propertyInformationField;

            private PropertyPropertyRegistrationInformation propertyRegistrationInformationField;

            private PropertyBuyerandSellerInformation buyerandSellerInformationField;

            private PropertyBondInformation bondInformationField;

            private PropertyAttorneyInformation attorneyInformationField;

            private PropertyPropertyHistory[] propertyHistoryField;

            private PropertyBuyerHistory[] buyerHistoryField;

            private PropertySellerHistory[] sellerHistoryField;

            private ConsumerSubscriberInputDetails subscriberInputDetailsField;

            /// <remarks/>
            public PropertyReportInformation ReportInformation
            {
                get
                {
                    return this.reportInformationField;
                }
                set
                {
                    this.reportInformationField = value;
                }
            }

            /// <remarks/>
            public PropertyPropertyInformation PropertyInformation
            {
                get
                {
                    return this.propertyInformationField;
                }
                set
                {
                    this.propertyInformationField = value;
                }
            }

            /// <remarks/>
            public PropertyPropertyRegistrationInformation PropertyRegistrationInformation
            {
                get
                {
                    return this.propertyRegistrationInformationField;
                }
                set
                {
                    this.propertyRegistrationInformationField = value;
                }
            }

            /// <remarks/>
            public PropertyBuyerandSellerInformation BuyerandSellerInformation
            {
                get
                {
                    return this.buyerandSellerInformationField;
                }
                set
                {
                    this.buyerandSellerInformationField = value;
                }
            }

            /// <remarks/>
            public PropertyBondInformation BondInformation
            {
                get
                {
                    return this.bondInformationField;
                }
                set
                {
                    this.bondInformationField = value;
                }
            }

            /// <remarks/>
            public PropertyAttorneyInformation AttorneyInformation
            {
                get
                {
                    return this.attorneyInformationField;
                }
                set
                {
                    this.attorneyInformationField = value;
                }
            }

            /// <remarks/>
           [System.Xml.Serialization.XmlElementAttribute("PropertyHistory")]
            public PropertyPropertyHistory[] PropertyHistory
            {
                get
                {
                    return this.propertyHistoryField;
                }
                set
                {
                    this.propertyHistoryField = value;
                }
            }

           [System.Xml.Serialization.XmlElementAttribute("BuyerHistory")]
           public PropertyBuyerHistory[] BuyerHistory
           {
               get
               {
                   return this.buyerHistoryField;
               }
               set
               {
                   this.buyerHistoryField = value;
               }
           }



            /// <remarks/>
             [System.Xml.Serialization.XmlElementAttribute("SellerHistory")]
            public PropertySellerHistory[] SellerHistory
            {
                get
                {
                    return this.sellerHistoryField;
                }
                set
                {
                    this.sellerHistoryField = value;
                }
            }

            /// <remarks/>
             public ConsumerSubscriberInputDetails SubscriberInputDetails
            {
                get
                {
                    return this.subscriberInputDetailsField;
                }
                set
                {
                    this.subscriberInputDetailsField = value;
                }
            }
        }

        ///// <remarks/>
        //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        //public partial class PropertyReportInformation
        //{

        //    private byte reportIDField;

        //    private string reportNameField;

        //    /// <remarks/>
        //    public byte ReportID
        //    {
        //        get
        //        {
        //            return this.reportIDField;
        //        }
        //        set
        //        {
        //            this.reportIDField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string ReportName
        //    {
        //        get
        //        {
        //            return this.reportNameField;
        //        }
        //        set
        //        {
        //            this.reportNameField = value;
        //        }
        //    }
        //}

        ///// <remarks/>
        //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        //public partial class PropertyPropertyInformation
        //{

        //    private string displayTextField;

        //    private string referenceNoField;

        //    private object externalReferenceNoField;

        //    private string propertyTypeField;

        //    private ushort standNoField;

        //    private byte streetNumberField;

        //    private string streetNameField;

        //    private string suburbField;

        //    private string cityField;

        //    private string townshipNameField;

        //    private string provinceField;

        //    /// <remarks/>
        //    public string DisplayText
        //    {
        //        get
        //        {
        //            return this.displayTextField;
        //        }
        //        set
        //        {
        //            this.displayTextField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string ReferenceNo
        //    {
        //        get
        //        {
        //            return this.referenceNoField;
        //        }
        //        set
        //        {
        //            this.referenceNoField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public object ExternalReferenceNo
        //    {
        //        get
        //        {
        //            return this.externalReferenceNoField;
        //        }
        //        set
        //        {
        //            this.externalReferenceNoField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string PropertyType
        //    {
        //        get
        //        {
        //            return this.propertyTypeField;
        //        }
        //        set
        //        {
        //            this.propertyTypeField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public ushort StandNo
        //    {
        //        get
        //        {
        //            return this.standNoField;
        //        }
        //        set
        //        {
        //            this.standNoField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public byte StreetNumber
        //    {
        //        get
        //        {
        //            return this.streetNumberField;
        //        }
        //        set
        //        {
        //            this.streetNumberField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string StreetName
        //    {
        //        get
        //        {
        //            return this.streetNameField;
        //        }
        //        set
        //        {
        //            this.streetNameField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string Suburb
        //    {
        //        get
        //        {
        //            return this.suburbField;
        //        }
        //        set
        //        {
        //            this.suburbField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string City
        //    {
        //        get
        //        {
        //            return this.cityField;
        //        }
        //        set
        //        {
        //            this.cityField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string TownshipName
        //    {
        //        get
        //        {
        //            return this.townshipNameField;
        //        }
        //        set
        //        {
        //            this.townshipNameField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string Province
        //    {
        //        get
        //        {
        //            return this.provinceField;
        //        }
        //        set
        //        {
        //            this.provinceField = value;
        //        }
        //    }
        //}

        ///// <remarks/>
  
        //public partial class PropertyPropertyRegistrationInformation
        //{

        //    private string displayTextField;

        //    private string transferIDField;

        //    private string titleDeedNoField;

        //    private string oldtitleDeednoField;

        //    private System.DateTime transferDateField;

        //    private System.DateTime purchaseDateField;

        //    private decimal soldPriceField;

        //    private string erfSizeField;

        //    private string deedsOfficeField;

        //    /// <remarks/>
        //    public string DisplayText
        //    {
        //        get
        //        {
        //            return this.displayTextField;
        //        }
        //        set
        //        {
        //            this.displayTextField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string TransferID
        //    {
        //        get
        //        {
        //            return this.transferIDField;
        //        }
        //        set
        //        {
        //            this.transferIDField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string TitleDeedNo
        //    {
        //        get
        //        {
        //            return this.titleDeedNoField;
        //        }
        //        set
        //        {
        //            this.titleDeedNoField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string OldtitleDeedno
        //    {
        //        get
        //        {
        //            return this.oldtitleDeednoField;
        //        }
        //        set
        //        {
        //            this.oldtitleDeednoField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        //    public System.DateTime TransferDate
        //    {
        //        get
        //        {
        //            return this.transferDateField;
        //        }
        //        set
        //        {
        //            this.transferDateField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        //    public System.DateTime PurchaseDate
        //    {
        //        get
        //        {
        //            return this.purchaseDateField;
        //        }
        //        set
        //        {
        //            this.purchaseDateField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public decimal SoldPrice
        //    {
        //        get
        //        {
        //            return this.soldPriceField;
        //        }
        //        set
        //        {
        //            this.soldPriceField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string ErfSize
        //    {
        //        get
        //        {
        //            return this.erfSizeField;
        //        }
        //        set
        //        {
        //            this.erfSizeField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string DeedsOffice
        //    {
        //        get
        //        {
        //            return this.deedsOfficeField;
        //        }
        //        set
        //        {
        //            this.deedsOfficeField = value;
        //        }
        //    }
        //}

        ///// <remarks/>
        //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        //public partial class PropertyBuyerandSellerInformation
        //{

        //    private string displayTextField;

        //    private string buyerTypeCodeField;

        //    private uint buyerIDNoField;

        //    private string buyerNameField;

        //    private string buyerMaritalStatusField;

        //    private string sellerTypeCodeField;

        //    private uint sellerIDNoField;

        //    private string sellerNameField;

        //    private object sellerMaritalStatusField;

        //    /// <remarks/>
        //    public string DisplayText
        //    {
        //        get
        //        {
        //            return this.displayTextField;
        //        }
        //        set
        //        {
        //            this.displayTextField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string BuyerTypeCode
        //    {
        //        get
        //        {
        //            return this.buyerTypeCodeField;
        //        }
        //        set
        //        {
        //            this.buyerTypeCodeField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public uint BuyerIDNo
        //    {
        //        get
        //        {
        //            return this.buyerIDNoField;
        //        }
        //        set
        //        {
        //            this.buyerIDNoField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string BuyerName
        //    {
        //        get
        //        {
        //            return this.buyerNameField;
        //        }
        //        set
        //        {
        //            this.buyerNameField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string BuyerMaritalStatus
        //    {
        //        get
        //        {
        //            return this.buyerMaritalStatusField;
        //        }
        //        set
        //        {
        //            this.buyerMaritalStatusField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string SellerTypeCode
        //    {
        //        get
        //        {
        //            return this.sellerTypeCodeField;
        //        }
        //        set
        //        {
        //            this.sellerTypeCodeField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public uint SellerIDNo
        //    {
        //        get
        //        {
        //            return this.sellerIDNoField;
        //        }
        //        set
        //        {
        //            this.sellerIDNoField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string SellerName
        //    {
        //        get
        //        {
        //            return this.sellerNameField;
        //        }
        //        set
        //        {
        //            this.sellerNameField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public object SellerMaritalStatus
        //    {
        //        get
        //        {
        //            return this.sellerMaritalStatusField;
        //        }
        //        set
        //        {
        //            this.sellerMaritalStatusField = value;
        //        }
        //    }
        //}

        ///// <remarks/>
        //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        //public partial class PropertyBondInformation
        //{

        //    private string displayTextField;

        //    private string bondHoldernameField;

        //    private decimal bondAmountField;

        //    private string bondAccountNoField;

        //    /// <remarks/>
        //    public string DisplayText
        //    {
        //        get
        //        {
        //            return this.displayTextField;
        //        }
        //        set
        //        {
        //            this.displayTextField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string BondHoldername
        //    {
        //        get
        //        {
        //            return this.bondHoldernameField;
        //        }
        //        set
        //        {
        //            this.bondHoldernameField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public decimal BondAmount
        //    {
        //        get
        //        {
        //            return this.bondAmountField;
        //        }
        //        set
        //        {
        //            this.bondAmountField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string BondAccountNo
        //    {
        //        get
        //        {
        //            return this.bondAccountNoField;
        //        }
        //        set
        //        {
        //            this.bondAccountNoField = value;
        //        }
        //    }
        //}

        ///// <remarks/>
        //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        //public partial class PropertyAttorneyInformation
        //{

        //    private string displayTextField;

        //    private byte attorneyFirmNoField;

        //    private string attorneyFileNoField;

        //    private string externalSourceUpdatedDateField;

        //    private decimal titleDeedFeeField;

        //    /// <remarks/>
        //    public string DisplayText
        //    {
        //        get
        //        {
        //            return this.displayTextField;
        //        }
        //        set
        //        {
        //            this.displayTextField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public byte AttorneyFirmNo
        //    {
        //        get
        //        {
        //            return this.attorneyFirmNoField;
        //        }
        //        set
        //        {
        //            this.attorneyFirmNoField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string AttorneyFileNo
        //    {
        //        get
        //        {
        //            return this.attorneyFileNoField;
        //        }
        //        set
        //        {
        //            this.attorneyFileNoField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string ExternalSourceUpdatedDate
        //    {
        //        get
        //        {
        //            return this.externalSourceUpdatedDateField;
        //        }
        //        set
        //        {
        //            this.externalSourceUpdatedDateField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public decimal TitleDeedFee
        //    {
        //        get
        //        {
        //            return this.titleDeedFeeField;
        //        }
        //        set
        //        {
        //            this.titleDeedFeeField = value;
        //        }
        //    }
        //}

        ///// <remarks/>
        //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        //public partial class PropertyPropertyHistory
        //{

        //    private string displayTextField;

        //    private string propertyTypeField;

        //    private string titleDeedNoField;

        //    private string buyerNameField;

        //    private string sellerNameField;

        //    private decimal soldPriceField;

        //    private System.DateTime purchaseDateField;

        //    /// <remarks/>
        //    public string DisplayText
        //    {
        //        get
        //        {
        //            return this.displayTextField;
        //        }
        //        set
        //        {
        //            this.displayTextField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string PropertyType
        //    {
        //        get
        //        {
        //            return this.propertyTypeField;
        //        }
        //        set
        //        {
        //            this.propertyTypeField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string TitleDeedNo
        //    {
        //        get
        //        {
        //            return this.titleDeedNoField;
        //        }
        //        set
        //        {
        //            this.titleDeedNoField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string BuyerName
        //    {
        //        get
        //        {
        //            return this.buyerNameField;
        //        }
        //        set
        //        {
        //            this.buyerNameField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string SellerName
        //    {
        //        get
        //        {
        //            return this.sellerNameField;
        //        }
        //        set
        //        {
        //            this.sellerNameField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public decimal SoldPrice
        //    {
        //        get
        //        {
        //            return this.soldPriceField;
        //        }
        //        set
        //        {
        //            this.soldPriceField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        //    public System.DateTime PurchaseDate
        //    {
        //        get
        //        {
        //            return this.purchaseDateField;
        //        }
        //        set
        //        {
        //            this.purchaseDateField = value;
        //        }
        //    }
        //}

        ///// <remarks/>
        //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        //public partial class PropertySellerHistory
        //{

        //    private string displayTextField;

        //    private string propertyTypeField;

        //    private string provinceField;

        //    private string titleDeedNoField;

        //    private string buyerNameField;

        //    private object sellerNameField;

        //    private object standNoField;

        //    private byte portionNoField;

        //    /// <remarks/>
        //    public string DisplayText
        //    {
        //        get
        //        {
        //            return this.displayTextField;
        //        }
        //        set
        //        {
        //            this.displayTextField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string PropertyType
        //    {
        //        get
        //        {
        //            return this.propertyTypeField;
        //        }
        //        set
        //        {
        //            this.propertyTypeField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string Province
        //    {
        //        get
        //        {
        //            return this.provinceField;
        //        }
        //        set
        //        {
        //            this.provinceField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string TitleDeedNo
        //    {
        //        get
        //        {
        //            return this.titleDeedNoField;
        //        }
        //        set
        //        {
        //            this.titleDeedNoField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string BuyerName
        //    {
        //        get
        //        {
        //            return this.buyerNameField;
        //        }
        //        set
        //        {
        //            this.buyerNameField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public object SellerName
        //    {
        //        get
        //        {
        //            return this.sellerNameField;
        //        }
        //        set
        //        {
        //            this.sellerNameField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public object StandNo
        //    {
        //        get
        //        {
        //            return this.standNoField;
        //        }
        //        set
        //        {
        //            this.standNoField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public byte PortionNo
        //    {
        //        get
        //        {
        //            return this.portionNoField;
        //        }
        //        set
        //        {
        //            this.portionNoField = value;
        //        }
        //    }
        //}

        ///// <remarks/>
        //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        //public partial class PropertySubscriberInputDetails
        //{

        //    private System.DateTime enquiryDateField;

        //    private string enquiryTypeField;

        //    private string subscriberNameField;

        //    private string subscriberUserNameField;

        //    private string enquiryInputField;

        //    /// <remarks/>
        //    public System.DateTime EnquiryDate
        //    {
        //        get
        //        {
        //            return this.enquiryDateField;
        //        }
        //        set
        //        {
        //            this.enquiryDateField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string EnquiryType
        //    {
        //        get
        //        {
        //            return this.enquiryTypeField;
        //        }
        //        set
        //        {
        //            this.enquiryTypeField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string SubscriberName
        //    {
        //        get
        //        {
        //            return this.subscriberNameField;
        //        }
        //        set
        //        {
        //            this.subscriberNameField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string SubscriberUserName
        //    {
        //        get
        //        {
        //            return this.subscriberUserNameField;
        //        }
        //        set
        //        {
        //            this.subscriberUserNameField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string EnquiryInput
        //    {
        //        get
        //        {
        //            return this.enquiryInputField;
        //        }
        //        set
        //        {
        //            this.enquiryInputField = value;
        //        }
        //    }
        //}
  
}
