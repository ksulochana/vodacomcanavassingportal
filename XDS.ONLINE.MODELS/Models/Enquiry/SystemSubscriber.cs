﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDS.ONLINE.MODELS
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class Subscriber
    {

        private SubscriberSubscriber subscriber1Field;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Subscriber")]
        public SubscriberSubscriber Subscriber1
        {
            get
            {
                return this.subscriber1Field;
            }
            set
            {
                this.subscriber1Field = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class SubscriberSubscriber
    {

        private string _subscriberIDField;

        private string _subscriberNameField;

        private string _statusIndField;

        private string _subscriberAssociationCodeField;

        private string _companyTelephoneCodeField;

        private string _companyTelephoneNoField;

        private string _associationTypeCodeField;

        private string _payAsYouGoField;

        private string _payAsyouGoEnquiryLimitField;

        private string _marketingContentField;

        private string _stayAliveField;

        /// <remarks/>
        public string SubscriberID
        {
            get
            {
                return this._subscriberIDField;
            }
            set
            {
                this._subscriberIDField = value;
            }
        }

        /// <remarks/>
        public string SubscriberName
        {
            get
            {
                return this._subscriberNameField;
            }
            set
            {
                this._subscriberNameField = value;
            }
        }

        /// <remarks/>
        public string StatusInd
        {
            get
            {
                return this._statusIndField;
            }
            set
            {
                this._statusIndField = value;
            }
        }

        /// <remarks/>
        public string SubscriberAssociationCode
        {
            get
            {
                return this._subscriberAssociationCodeField;
            }
            set
            {
                this._subscriberAssociationCodeField = value;
            }
        }

        /// <remarks/>
        public string CompanyTelephoneCode
        {
            get
            {
                return this._companyTelephoneCodeField;
            }
            set
            {
                this._companyTelephoneCodeField = value;
            }
        }

        /// <remarks/>
        public string CompanyTelephoneNo
        {
            get
            {
                return this._companyTelephoneNoField;
            }
            set
            {
                this._companyTelephoneNoField = value;
            }
        }

        /// <remarks/>
        public string AssociationTypeCode
        {
            get
            {
                return this._associationTypeCodeField;
            }
            set
            {
                this._associationTypeCodeField = value;
            }
        }

        /// <remarks/>
        public string PayAsYouGo
        {
            get
            {
                return this._payAsYouGoField;
            }
            set
            {
                this._payAsYouGoField = value;
            }
        }

        /// <remarks/>
        public string PayAsyouGoEnquiryLimit
        {
            get
            {
                return this._payAsyouGoEnquiryLimitField;
            }
            set
            {
                this._payAsyouGoEnquiryLimitField = value;
            }
        }

        /// <remarks/>
        public string MarketingContent
        {
            get
            {
                return this._marketingContentField;
            }
            set
            {
                this._marketingContentField = value;
            }
        }

        /// <remarks/>
        public string StayAlive
        {
            get
            {
                return this._stayAliveField;
            }
            set
            {
                this._stayAliveField = value;
            }
        }
    }

}