﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDS.ONLINE.MODELS
{

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class ListOfDirectors
    {

        private ListOfDirectorsDirectorDetails[] directorDetailsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("DirectorDetails")]
        public ListOfDirectorsDirectorDetails[] DirectorDetails
        {
            get
            {
                return this.directorDetailsField;
            }
            set
            {
                this.directorDetailsField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ListOfDirectorsDirectorDetails
    {

        private string directorIDField;

        private string consumerIDField;

        private string idnoField;

        private string passportnumberField;

        private string firstInitialField;

        private string firstnameField;

        private string surnameField;

        //private System.DateTime birthDateField;
        private string birthDateField;

        private bool birthDateFieldSpecified;

        private string genderField;

        private string bonusXMLField;

        private string tempReferenceField;

        private string enquiryIDField;

        private string enquiryResultIDField;

        private string referenceField;

        /// <remarks/>
        public string DirectorID
        {
            get
            {
                return this.directorIDField;
            }
            set
            {
                this.directorIDField = value;
            }
        }

        /// <remarks/>
        public string ConsumerID
        {
            get
            {
                return this.consumerIDField;
            }
            set
            {
                this.consumerIDField = value;
            }
        }

        /// <remarks/>
        public string Idno
        {
            get
            {
                return this.idnoField;
            }
            set
            {
                this.idnoField = value;
            }
        }

        /// <remarks/>
        public string Passportnumber
        {
            get
            {
                return this.passportnumberField;
            }
            set
            {
                this.passportnumberField = value;
            }
        }

        /// <remarks/>
        public string FirstInitial
        {
            get
            {
                return this.firstInitialField;
            }
            set
            {
                this.firstInitialField = value;
            }
        }

        /// <remarks/>
        public string Firstname
        {
            get
            {
                return this.firstnameField;
            }
            set
            {
                this.firstnameField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string BirthDate
        {
            get
            {
                return this.birthDateField;
            }
            set
            {
                this.birthDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool BirthDateSpecified
        {
            get
            {
                return this.birthDateFieldSpecified;
            }
            set
            {
                this.birthDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Gender
        {
            get
            {
                return this.genderField;
            }
            set
            {
                this.genderField = value;
            }
        }

        /// <remarks/>
        public string BonusXML
        {
            get
            {
                return this.bonusXMLField;
            }
            set
            {
                this.bonusXMLField = value;
            }
        }

        /// <remarks/>
        public string TempReference
        {
            get
            {
                return this.tempReferenceField;
            }
            set
            {
                this.tempReferenceField = value;
            }
        }

        /// <remarks/>
        public string EnquiryID
        {
            get
            {
                return this.enquiryIDField;
            }
            set
            {
                this.enquiryIDField = value;
            }
        }

        /// <remarks/>
        public string EnquiryResultID
        {
            get
            {
                return this.enquiryResultIDField;
            }
            set
            {
                this.enquiryResultIDField = value;
            }
        }

        /// <remarks/>
        public string Reference
        {
            get
            {
                return this.referenceField;
            }
            set
            {
                this.referenceField = value;
            }
        }
    }


}
