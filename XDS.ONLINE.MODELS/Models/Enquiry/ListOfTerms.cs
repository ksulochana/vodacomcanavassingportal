﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace XDS.ONLINE.MODELS
{
    [System.Xml.Serialization.XmlRoot("Terms")]
    public class ListOfTerms
    {
        private string[] termField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Term")]
        public string[] Term
        {
            get
            {
                return this.termField;
            }
            set
            {
                this.termField = value;
            }
        }
    }

}
