﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDS.ONLINE.MODELS
{
    [System.Xml.Serialization.XmlRoot("DsCommercialMatch")]
    public class ListOfBusiness
    {
        private DsCommercialMatchCommercialDetails[] commercialDetailsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CommercialDetails")]
        public DsCommercialMatchCommercialDetails[] CommercialDetails
        {
            get
            {
                return this.commercialDetailsField;
            }
            set
            {
                this.commercialDetailsField = value;
            }
        }
    }


    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    //public partial class DsCommercialMatch
    //{

    //    private DsCommercialMatchCommercialDetails[] commercialDetailsField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlElementAttribute("CommercialDetails")]
    //    public DsCommercialMatchCommercialDetails[] CommercialDetails
    //    {
    //        get
    //        {
    //            return this.commercialDetailsField;
    //        }
    //        set
    //        {
    //            this.commercialDetailsField = value;
    //        }
    //    }
    //}

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class DsCommercialMatchCommercialDetails
    {

        private uint commercialIDField;

        private string registrationNoField;

        private string businessnameField;

        private object bonusXMLField;

        private object tempReferenceField;

        private uint enquiryIDField;

        private uint enquiryResultIDField;

        private string referenceField;

        /// <remarks/>
        public uint CommercialID
        {
            get
            {
                return this.commercialIDField;
            }
            set
            {
                this.commercialIDField = value;
            }
        }

        /// <remarks/>
        public string RegistrationNo
        {
            get
            {
                return this.registrationNoField;
            }
            set
            {
                this.registrationNoField = value;
            }
        }

        /// <remarks/>
        public string Businessname
        {
            get
            {
                return this.businessnameField;
            }
            set
            {
                this.businessnameField = value;
            }
        }

        /// <remarks/>
        public object BonusXML
        {
            get
            {
                return this.bonusXMLField;
            }
            set
            {
                this.bonusXMLField = value;
            }
        }

        /// <remarks/>
        public object TempReference
        {
            get
            {
                return this.tempReferenceField;
            }
            set
            {
                this.tempReferenceField = value;
            }
        }

        /// <remarks/>
        public uint EnquiryID
        {
            get
            {
                return this.enquiryIDField;
            }
            set
            {
                this.enquiryIDField = value;
            }
        }

        /// <remarks/>
        public uint EnquiryResultID
        {
            get
            {
                return this.enquiryResultIDField;
            }
            set
            {
                this.enquiryResultIDField = value;
            }
        }

        /// <remarks/>
        public string Reference
        {
            get
            {
                return this.referenceField;
            }
            set
            {
                this.referenceField = value;
            }
        }
    }

}
