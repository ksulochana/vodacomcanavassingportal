﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDS.ONLINE.MODELS
{       
    
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]       
    [System.Xml.Serialization.XmlRoot("Result")]
    public partial class ListOfProducts
    {

            private ResultProductItem[] productsField;

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("ProductItem", IsNullable = false)]
            public ResultProductItem[] Products
            {
                get
                {
                    return this.productsField;
                }
                set
                {
                    this.productsField = value;
                }
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class ResultProductItem
        {

            private byte productIDField;

            private byte reportIDField;

            private string productDescriptionField;

            /// <remarks/>
            public byte ProductID
            {
                get
                {
                    return this.productIDField;
                }
                set
                {
                    this.productIDField = value;
                }
            }

            /// <remarks/>
            public byte ReportID
            {
                get
                {
                    return this.reportIDField;
                }
                set
                {
                    this.reportIDField = value;
                }
            }

            /// <remarks/>
            public string ProductDescription
            {
                get
                {
                    return this.productDescriptionField;
                }
                set
                {
                    this.productDescriptionField = value;
                }
            }
        }




    }





