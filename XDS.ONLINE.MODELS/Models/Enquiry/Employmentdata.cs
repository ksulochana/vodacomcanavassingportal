﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDS.ONLINE.MODELS
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class Employmentdata
    {

        private string _resultField;

        /// <remarks/>
        public string Result
        {
            get
            {
                return this._resultField;
            }
            set
            {
                this._resultField = value;
            }
        }

       
    }
}

