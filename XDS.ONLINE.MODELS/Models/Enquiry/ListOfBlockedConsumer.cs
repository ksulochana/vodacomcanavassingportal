﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace XDS.ONLINE.MODELS
{

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    [System.Xml.Serialization.XmlRoot("BlockedConsumer")]
    public partial class BlockedConsumer
    {

        private BlockedConsumerConsumer[] consumerField;

        private bool blockingAllowed;

        public bool BlockingAllowed
        {
            get { return blockingAllowed; }
            set { blockingAllowed = value; }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Consumer")]
        public BlockedConsumerConsumer[] Consumer
        {
            get
            {
                return this.consumerField;
            }
            set
            {
                this.consumerField = value;
            }
        }

        [Display(Name = "Please select a reason for unblocking")]
        public UnblockReasons UnblockReasons { get; set; }

        [Required(ErrorMessage = "Please select a reason for unblocking")]
        public string UnblockReasonValue { get; set; }


    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class BlockedConsumerConsumer
    {

        private string blockIDField;

        private string consumerIDField;

        private string iDNoField;

        private string passportNoField;

        private string surnameField;

        private string firstNameField;

        private object secondNameField;

        private string birthDateField;

        private string blockingReasonField;

        /// <remarks/>
        public string BlockID
        {
            get
            {
                return this.blockIDField;
            }
            set
            {
                this.blockIDField = value;
            }
        }

        /// <remarks/>
        public string ConsumerID
        {
            get
            {
                return this.consumerIDField;
            }
            set
            {
                this.consumerIDField = value;
            }
        }

        /// <remarks/>
        public string IDNo
        {
            get
            {
                return this.iDNoField;
            }
            set
            {
                this.iDNoField = value;
            }
        }

        /// <remarks/>
        public string PassportNo
        {
            get
            {
                return this.passportNoField;
            }
            set
            {
                this.passportNoField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public object SecondName
        {
            get
            {
                return this.secondNameField;
            }
            set
            {
                this.secondNameField = value;
            }
        }

        /// <remarks/>
        public string BirthDate
        {
            get
            {
                return this.birthDateField;
            }
            set
            {
                this.birthDateField = value;
            }
        }

        /// <remarks/>
        public string BlockingReason
        {
            get
            {
                return this.blockingReasonField;
            }
            set
            {
                this.blockingReasonField = value;
            }
        }
    }






}
