﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDS.ONLINE.MODELS
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class SystemUser
    {

        private SystemUserSystemUser[] systemUser1Field;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SystemUser")]
        public SystemUserSystemUser[] SystemUser1
        {
            get
            {
                return this.systemUser1Field;
            }
            set
            {
                this.systemUser1Field = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class SystemUserSystemUser
    {

        private string userNameField;

        private string firstNameField;

        private string surnameField;

        private string systemUserRoleField;
        private bool activeYNField;
        private string result;
        private string emailaddressField;

        /// <remarks/>
        public string UserName
        {
            get
            {
                return this.userNameField;
            }
            set
            {
                this.userNameField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string SystemUserRole
        {
            get
            {
                return this.systemUserRoleField;
            }
            set
            {
                this.systemUserRoleField = value;
            }
        }

        
        public bool ActiveYN
        {
            get
            {
                return this.activeYNField;
            }
            set
            {
                this.activeYNField = value;
            }
        }

        public string EmailAddress
        {
            get
            {
                return this.emailaddressField;
            }
            set
            {
                this.emailaddressField = value;
            }
        }
        public string Result
        {
            get
            {
                return this.result;
            }
            set
            {
                this.result = value;
            }
        }
    }


}
