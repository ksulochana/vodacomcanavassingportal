﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDS.ONLINE.MODELS
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class ListOfConsumers
    {

        private ListOfConsumersConsumerDetails[] consumerDetailsField;
        private ListOfMVVConsumersVehicleIdentification vehicleIdentificationField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ConsumerDetails")]
        public ListOfConsumersConsumerDetails[] ConsumerDetails
        {
            get
            {
                return this.consumerDetailsField;
            }
            set
            {
                this.consumerDetailsField = value;
            }
        }

        /// <remarks/>
        public ListOfMVVConsumersVehicleIdentification VehicleIdentification
        {
            get
            {
                return this.vehicleIdentificationField;
            }
            set
            {
                this.vehicleIdentificationField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ListOfConsumersConsumerDetails
    {
        private string consumerIDField;
        private string homeAffairsIDField;
        private string idnoField;
        private string passportNoField;
        private string firstNameField;
        private string secondNameField;
        private string surnameField;
        private string deceasedStatusField;
        private System.DateTime iDIssuedDateField;
        private string causeOfDeathField;
        private string bonusXMLField;
        private string tempReferenceField;
        private string enquiryIDField;
        private string enquiryResultIDField;
        private string referenceField;

        /// <remarks/>
        public string ConsumerID
        {
            get
            {
                return this.consumerIDField;
            }
            set
            {
                this.consumerIDField = value;
            }
        }

        /// <remarks/>
        public string HomeAffairsID
        {
            get
            {
                return this.homeAffairsIDField;
            }
            set
            {
                this.homeAffairsIDField = value;
            }
        }

        /// <remarks/>
        public string IDNo
        {
            get
            {
                return this.idnoField;
            }
            set
            {
                this.idnoField = value;
            }
        }

        // added to cater for inconsistency in xml
        public string Idno
        {
            get
            {
                return this.idnoField;
            }
            set
            {
                this.idnoField = value;
            }
        }

        // added to cater for inconsistency in xml
        public string IDno
        {
            get
            {
                return this.idnoField;
            }
            set
            {
                this.idnoField = value;
            }
        }

        public string PassportNo
        {
            get
            {
                return this.passportNoField;
            }
            set
            {
                this.passportNoField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string SecondName
        {
            get
            {
                return this.secondNameField;
            }
            set
            {
                this.secondNameField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        // added to cater for inconsistency in xml
        public string SurName
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string DeceasedStatus
        {
            get
            {
                return this.deceasedStatusField;
            }
            set
            {
                this.deceasedStatusField = value;
            }
        }

        /// <remarks/>
        public System.DateTime IDIssuedDate
        {
            get
            {
                return this.iDIssuedDateField;
            }
            set
            {
                this.iDIssuedDateField = value;
            }
        }

        /// <remarks/>
        public string CauseOfDeath
        {
            get
            {
                return this.causeOfDeathField;
            }
            set
            {
                this.causeOfDeathField = value;
            }
        }

        /// <remarks/>
        public string BonusXML
        {
            get
            {
                return this.bonusXMLField;
            }
            set
            {
                this.bonusXMLField = value;
            }
        }

        /// <remarks/>
        public string TempReference
        {
            get
            {
                return this.tempReferenceField;
            }
            set
            {
                this.tempReferenceField = value;
            }
        }

        /// <remarks/>
        public string EnquiryID
        {
            get
            {
                return this.enquiryIDField;
            }
            set
            {
                this.enquiryIDField = value;
            }
        }

        /// <remarks/>
        public string EnquiryResultID
        {
            get
            {
                return this.enquiryResultIDField;
            }
            set
            {
                this.enquiryResultIDField = value;
            }
        }

        /// <remarks/>
        public string Reference
        {
            get
            {
                return this.referenceField;
            }
            set
            {
                this.referenceField = value;
            }
        }
    }


    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ListOfMVVConsumersVehicleIdentification
    {

        private string manufacturerField;

        private string yearField;

        private string DescriptionField;

        /// <remarks/>
        public string Manufacturer
        {
            get
            {
                return this.manufacturerField;
            }
            set
            {
                this.manufacturerField = value;
            }
        }

        /// <remarks/>
        public string Year
        {
            get
            {
                return this.yearField;
            }
            set
            {
                this.yearField = value;
            }
        }

        /// <remarks/>
        public string Description
        {
            get
            {
                return this.DescriptionField;
            }
            set
            {
                this.DescriptionField = value;
            }
        }
    }
}
