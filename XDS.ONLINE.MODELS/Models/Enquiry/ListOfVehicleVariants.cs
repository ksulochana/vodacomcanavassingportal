﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDS.ONLINE.MODELS
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class VehicleVariants
    {

        private VehicleVariantsVariantInfo[] variantInfoField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("VariantInfo")]
        public VehicleVariantsVariantInfo[]     VariantInfo
        {
            get
            {
                return this.variantInfoField;
            }
            set
            {
                this.variantInfoField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class VehicleVariantsVariantInfo
    {

        private string newCodeField;

        private string variantField;

        /// <remarks/>
        public string NewCode
        {
            get
            {
                return this.newCodeField;
            }
            set
            {
                this.newCodeField = value;
            }
        }

        /// <remarks/>
        public string Variant
        {
            get
            {
                return this.variantField;
            }
            set
            {
                this.variantField = value;
            }
        }
    }
}

