﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDS.ONLINE.MODELS
{

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.web.xds.co.za/XDSConnectAuthWS")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.web.xds.co.za/XDSConnectAuthWS", IsNullable = false)]
    public class ConnectFraudSavePersonalQuestionsResponse
    {

        private string connectFraudSavePersonalQuestionsResultField;

        /// <remarks/>
        public string ConnectFraudSavePersonalQuestionsResult
        {
            get
            {
                return this.connectFraudSavePersonalQuestionsResultField;
            }
            set
            {
                this.connectFraudSavePersonalQuestionsResultField = value;
            }
        }
    }


}
