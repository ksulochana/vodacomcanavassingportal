﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDS.ONLINE.MODELS
{ 
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)] 
        [System.Xml.Serialization.XmlRoot("Result")]
        public partial class ListofHistory
        {

            private ResultHistoryItem[] searchHistoryField;

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("HistoryItem", IsNullable = false)]
            public ResultHistoryItem[] SearchHistory
            {
                get
                {
                    return this.searchHistoryField;
                }
                set
                {
                    this.searchHistoryField = value;
                }
            }
        }
     
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class ResultHistoryItem
        {

        private string referenceNoField;
        private string enquiryIDField;
        private string dealerIDField;
        private string promotionDealField;
        private string promoCodeField;
        private string firstnameField;
        private string surnameField;
        private string iDNoField;
        private string birthDateField;
        private string incomeField;
        private string cellularNoField;
        private string otherNoField;
        private string emailField;
        private string decisionField;
        private string portfolioField;
        private string createdByUserField;
        private string createdOnDateField;
          

        public string ReferenceNo
        {
            get
            {
                return referenceNoField;
            }

            set
            {
                referenceNoField = value;
            }
        }

        public string EnquiryID
        {
            get
            {
                return enquiryIDField;
            }

            set
            {
                enquiryIDField = value;
            }
        }

        public string DealerID
        {
            get
            {
                return dealerIDField;
            }

            set
            {
                dealerIDField = value;
            }
        }

        public string PromotionDeal
        {
            get
            {
                return promotionDealField;
            }

            set
            {
                promotionDealField = value;
            }
        }

        public string PromoCode
        {
            get
            {
                return promoCodeField;
            }

            set
            {
                promoCodeField = value;
            }
        }

        public string Firstname
        {
            get
            {
                return firstnameField;
            }

            set
            {
                firstnameField = value;
            }
        }

        public string Surname
        {
            get
            {
                return surnameField;
            }

            set
            {
                surnameField = value;
            }
        }

        public string IDNo
        {
            get
            {
                return iDNoField;
            }

            set
            {
                iDNoField = value;
            }
        }

        public string BirthDate
        {
            get
            {
                return birthDateField;
            }

            set
            {
                birthDateField = value;
            }
        }

        public string Income
        {
            get
            {
                return incomeField;
            }

            set
            {
                incomeField = value;
            }
        }

        public string CellularNo
        {
            get
            {
                return cellularNoField;
            }

            set
            {
                cellularNoField = value;
            }
        }

        public string OtherNo
        {
            get
            {
                return otherNoField;
            }

            set
            {
                otherNoField = value;
            }
        }

        public string Email
        {
            get
            {
                return emailField;
            }

            set
            {
                emailField = value;
            }
        }

        public string Decision
        {
            get
            {
                return decisionField;
            }

            set
            {
                decisionField = value;
            }
        }

        public string Portfolio
        {
            get
            {
                return portfolioField;
            }

            set
            {
                portfolioField = value;
            }
        }

        public string CreatedByUser
        {
            get
            {
                return createdByUserField;
            }

            set
            {
                createdByUserField = value;
            }
        }

        public string CreatedOnDate
        {
            get
            {
                return createdOnDateField;
            }

            set
            {
                createdOnDateField = value;
            }
        }
    }
    }

