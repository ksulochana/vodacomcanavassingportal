﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDS.ONLINE.MODELS
{

    /// <remarks/>
    [System.Xml.Serialization.XmlRoot("ConnectAccTypes")]
    public partial class ListofBankAccountTypes
    {

        private ConnectAccTypesAcctypes[] acctypesField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Acctypes")]
        public ConnectAccTypesAcctypes[] Acctypes
        {
            get
            {
                return this.acctypesField;
            }
            set
            {
                this.acctypesField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConnectAccTypesAcctypes
    {

        private string acctypeField;

        private string codeField;

        /// <remarks/>
        public string Acctype
        {
            get
            {
                return this.acctypeField;
            }
            set
            {
                this.acctypeField = value;
            }
        }

        /// <remarks/>
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
    }

}
