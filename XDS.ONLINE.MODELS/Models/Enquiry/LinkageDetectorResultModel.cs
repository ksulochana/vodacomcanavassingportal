﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDS.ONLINE.MODELS
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class LinkageSearchResult
    {

        private LinkageSearchResultConsumerDetails[] listOfConsumersField;

        private LinkageSearchResultThirdPartyDetails[] listOfThirdPartiesField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("ConsumerDetails", IsNullable = false)]
        public LinkageSearchResultConsumerDetails[] ListOfConsumers
        {
            get
            {
                return this.listOfConsumersField;
            }
            set
            {
                this.listOfConsumersField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("ThirdPartyDetails", IsNullable = false)]
        public LinkageSearchResultThirdPartyDetails[] ListOfThirdParties
        {
            get
            {
                return this.listOfThirdPartiesField;
            }
            set
            {
                this.listOfThirdPartiesField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class LinkageSearchResultConsumerDetails
    {

        private string consumerIDField;

        private string firstNameField;

        private string secondNameField;

        private string thirdNameField;

        private string surnameField;

        private string iDNoField;

        private string passportNoField;

        private string birthDateField;

        private string genderIndField;

        private string bonusXMLField;

        private string tempReferenceField;

        private string enquiryIDField;

        private string enquiryResultIDField;

        private string referenceField;

        /// <remarks/>
        public string ConsumerID
        {
            get
            {
                return this.consumerIDField;
            }
            set
            {
                this.consumerIDField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string SecondName
        {
            get
            {
                return this.secondNameField;
            }
            set
            {
                this.secondNameField = value;
            }
        }

        /// <remarks/>
        public string ThirdName
        {
            get
            {
                return this.thirdNameField;
            }
            set
            {
                this.thirdNameField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string IDNo
        {
            get
            {
                return this.iDNoField;
            }
            set
            {
                this.iDNoField = value;
            }
        }

        /// <remarks/>
        public string PassportNo
        {
            get
            {
                return this.passportNoField;
            }
            set
            {
                this.passportNoField = value;
            }
        }

        /// <remarks/>
        public string BirthDate
        {
            get
            {
                return this.birthDateField;
            }
            set
            {
                this.birthDateField = value;
            }
        }

        /// <remarks/>
        public string GenderInd
        {
            get
            {
                return this.genderIndField;
            }
            set
            {
                this.genderIndField = value;
            }
        }

        /// <remarks/>
        public string BonusXML
        {
            get
            {
                return this.bonusXMLField;
            }
            set
            {
                this.bonusXMLField = value;
            }
        }

        /// <remarks/>
        public string TempReference
        {
            get
            {
                return this.tempReferenceField;
            }
            set
            {
                this.tempReferenceField = value;
            }
        }

        /// <remarks/>
        public string EnquiryID
        {
            get
            {
                return this.enquiryIDField;
            }
            set
            {
                this.enquiryIDField = value;
            }
        }

        /// <remarks/>
        public string EnquiryResultID
        {
            get
            {
                return this.enquiryResultIDField;
            }
            set
            {
                this.enquiryResultIDField = value;
            }
        }

        /// <remarks/>
        public string Reference
        {
            get
            {
                return this.referenceField;
            }
            set
            {
                this.referenceField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class LinkageSearchResultThirdPartyDetails
    {

        private string consumerIDField;

        private string firstNameField;

        private string secondNameField;

        private string thirdNameField;

        private string surnameField;

        private string iDNoField;

        private string passportNoField;

        private string birthDateField;

        private string genderIndField;

        private string bonusXMLField;

        private string tempReferenceField;

        private string enquiryIDField;

        private string enquiryResultIDField;

        private string referenceField;

        /// <remarks/>
        public string ConsumerID
        {
            get
            {
                return this.consumerIDField;
            }
            set
            {
                this.consumerIDField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string SecondName
        {
            get
            {
                return this.secondNameField;
            }
            set
            {
                this.secondNameField = value;
            }
        }

        /// <remarks/>
        public string ThirdName
        {
            get
            {
                return this.thirdNameField;
            }
            set
            {
                this.thirdNameField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string IDNo
        {
            get
            {
                return this.iDNoField;
            }
            set
            {
                this.iDNoField = value;
            }
        }

        /// <remarks/>
        public string PassportNo
        {
            get
            {
                return this.passportNoField;
            }
            set
            {
                this.passportNoField = value;
            }
        }

        /// <remarks/>
        public string BirthDate
        {
            get
            {
                return this.birthDateField;
            }
            set
            {
                this.birthDateField = value;
            }
        }

        /// <remarks/>
        public string GenderInd
        {
            get
            {
                return this.genderIndField;
            }
            set
            {
                this.genderIndField = value;
            }
        }

        /// <remarks/>
        public string BonusXML
        {
            get
            {
                return this.bonusXMLField;
            }
            set
            {
                this.bonusXMLField = value;
            }
        }

        /// <remarks/>
        public string TempReference
        {
            get
            {
                return this.tempReferenceField;
            }
            set
            {
                this.tempReferenceField = value;
            }
        }

        /// <remarks/>
        public string EnquiryID
        {
            get
            {
                return this.enquiryIDField;
            }
            set
            {
                this.enquiryIDField = value;
            }
        }

        /// <remarks/>
        public string EnquiryResultID
        {
            get
            {
                return this.enquiryResultIDField;
            }
            set
            {
                this.enquiryResultIDField = value;
            }
        }

        /// <remarks/>
        public string Reference
        {
            get
            {
                return this.referenceField;
            }
            set
            {
                this.referenceField = value;
            }
        }
    }



}