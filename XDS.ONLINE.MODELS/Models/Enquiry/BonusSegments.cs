﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDS.ONLINE.MODELS
{

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class BonusSegments
    {

        private BonusSegmentsSegments[] segmentsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Segments")]
        public BonusSegmentsSegments[] Segments
        {
            get
            {
                return this.segmentsField;
            }
            set
            {
                this.segmentsField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class BonusSegmentsSegments
    {

        private byte dataSegmentIDField;

        private string dataSegmentNameField;

        private string dataSegmentDisplayTextField;

        private string bonusViewedField;

        private decimal bonusPriceField;

        /// <remarks/>
        public byte DataSegmentID
        {
            get
            {
                return this.dataSegmentIDField;
            }
            set
            {
                this.dataSegmentIDField = value;
            }
        }

        /// <remarks/>
        public string DataSegmentName
        {
            get
            {
                return this.dataSegmentNameField;
            }
            set
            {
                this.dataSegmentNameField = value;
            }
        }

        /// <remarks/>
        public string DataSegmentDisplayText
        {
            get
            {
                return this.dataSegmentDisplayTextField;
            }
            set
            {
                this.dataSegmentDisplayTextField = value;
            }
        }

        /// <remarks/>
        public string BonusViewed
        {
            get
            {
                return this.bonusViewedField;
            }
            set
            {
                this.bonusViewedField = value;
            }
        }

        /// <remarks/>
        public decimal BonusPrice
        {
            get
            {
                return this.bonusPriceField;
            }
            set
            {
                this.bonusPriceField = value;
            }
        }
    }


}
