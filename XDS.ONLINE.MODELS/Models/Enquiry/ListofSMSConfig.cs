﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Xml.Serialization;

using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;

namespace XDS.ONLINE.MODELS
{

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRoot("Result")]
    public partial class ListofSMSConfig
    {

        private SMSConfigItem[] smsConfigField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("SMSConfigItem", IsNullable = false)]
        public SMSConfigItem[] SMSConfigList
        {
            get
            {
                return this.smsConfigField;
            }
            set
            {
                this.smsConfigField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class SMSConfigItem
    {
        private string smsConfigID;
        private string smsMessage;
        private bool activeYN;
        private string createdByUserField;
        private string createdOnDateField;

        public string SMSConfigID
        {
            get
            {
                return smsConfigID;
            }

            set
            {
                smsConfigID = value;
            }
        }

        public string SMSMessage
        {
            get
            {
                return smsMessage;
            }

            set
            {
                smsMessage = value;
            }
        }

        public bool ActiveYN
        {
            get
            {
                return activeYN;
            }

            set
            {
                activeYN = value;
            }
        }
        public string CreatedByUser
        {
            get
            {
                return createdByUserField;
            }

            set
            {
                createdByUserField = value;
            }
        }
        public string CreatedOnDate
        {
            get
            {
                return createdOnDateField;
            }

            set
            {
                createdOnDateField = value;
            }
        }
    }
}