﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Xml.Serialization;

using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;

namespace XDS.ONLINE.MODELS
{

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRoot("Result")]
    public partial class ListofPromotionDeals
    {

        private PromotionDealItem[] promotionDeal;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("PromotionDealItem", IsNullable = false)]
        public PromotionDealItem[] PromotionDealList
        {
            get
            {
                return this.promotionDeal;
            }
            set
            {
                this.promotionDeal = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class PromotionDealItem
    { 
        private string promotionDealID;
        private string dealerID;
        private string promotionDealerID;
        private string dealSheetNo;
        private string description;
        private string validFrom;
        private string validTo;
        private string comments;
        private string docName;
        private byte[] doc;
        private string docFileName;
        private string docContentType;
        private string docLength;
        private string createdByUser;
        private string createdOnDate;
        private string changedByUser;
        private string changedOnDate;
        private string image;
        private ListofPortfolios promotionDealPortfolios;

        public string PromotionDealID
        {
            get
            {
                return promotionDealID;
            }

            set
            {
                promotionDealID = value;
            }
        }

        public string DealerID
        {
            get
            {
                return dealerID;
            }

            set
            {
                dealerID = value;
            }
        }

        public string PromotionDealerID
        {
            get
            {
                return promotionDealerID;
            }

            set
            {
                promotionDealerID = value;
            }
        }

        public string DealSheetNo
        {
            get
            {
                return dealSheetNo;
            }

            set
            {
                dealSheetNo = value;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }

            set
            {
                description = value;
            }
        }

        public string ValidFrom
        {
            get
            {
                return validFrom;
            }

            set
            {
                validFrom = value;
            }
        }

        public string ValidTo
        {
            get
            {
                return validTo;
            }

            set
            {
                validTo = value;
            }
        }

        public string Comments
        {
            get
            {
                return comments;
            }

            set
            {
                comments = value;
            }
        }

        public string DocName
        {
            get
            {
                return docName;
            }

            set
            {
                docName = value;
            }
        }

        public byte[] Doc
        {
            get
            {
                return doc;
            }

            set
            {
                doc = value;
            }
        }

        public string DocFileName
        {
            get
            {
                return docFileName;
            }

            set
            {
                docFileName = value;
            }
        }
        public string DocContentType
        {
            get
            {
                return docContentType;
            }

            set
            {
                docContentType = value;
            }
        }
        public string DocLength
        {
            get
            {
                return docLength;
            }

            set
            {
                docLength = value;
            }
        }
        public string CreatedByUser
        {
            get
            {
                return createdByUser;
            }

            set
            {
                createdByUser = value;
            }
        }
        public string CreatedOnDate
        {
            get
            {
                return createdOnDate;
            }

            set
            {
                createdOnDate = value;
            }
        }

        public string ChangedByUser
        {
            get
            {
                return changedByUser;
            }

            set
            {
                changedByUser = value;
            }
        }

        public string ChangedOnDate
        {
            get
            {
                return changedOnDate;
            }

            set
            {
                changedOnDate = value;
            }
        }

        public string Image
        {
            get
            {
                return image;
            }

            set
            {
                image = value;
            }
        }

        public ListofPortfolios PromotionDealPortfolios
        {
            get
            {
                return promotionDealPortfolios;
            }

            set
            {
                promotionDealPortfolios = value;
            }
        }
    }
}