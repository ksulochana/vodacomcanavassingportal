﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDS.ONLINE.MODELS
{
    /// <remarks/>
    [System.Xml.Serialization.XmlRoot("BICountryCodes")]
    public partial class CountryList
    {

        private BICountryCodesCountry[] countryField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Country")]
        public BICountryCodesCountry[] Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class BICountryCodesCountry
    {

        private byte countryIDField;

        private string countryDescriptionField;

        /// <remarks/>
        public byte CountryID
        {
            get
            {
                return this.countryIDField;
            }
            set
            {
                this.countryIDField = value;
            }
        }

        /// <remarks/>
        public string CountryDescription
        {
            get
            {
                return this.countryDescriptionField;
            }
            set
            {
                this.countryDescriptionField = value;
            }
        }
    }
}
