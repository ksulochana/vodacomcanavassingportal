﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace XDS.ONLINE.MODELS
{
    [XmlRoot("BankCodes")]
    public class BankCodesEnquiryRequest
    {
        private string statusField;

        private uint referenceNoField;

        /// <remarks/>
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        public uint ReferenceNo
        {
            get
            {
                return this.referenceNoField;
            }
            set
            {
                this.referenceNoField = value;
            }
        }
    }


    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    //public partial class BankCodes
    //{

    //    private string statusField;

    //    private uint referenceNoField;

    //    /// <remarks/>
    //    public string Status
    //    {
    //        get
    //        {
    //            return this.statusField;
    //        }
    //        set
    //        {
    //            this.statusField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public uint ReferenceNo
    //    {
    //        get
    //        {
    //            return this.referenceNoField;
    //        }
    //        set
    //        {
    //            this.referenceNoField = value;
    //        }
    //    }
    //}


}
