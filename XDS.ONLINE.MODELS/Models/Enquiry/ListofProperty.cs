﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDS.ONLINE.MODELS
{
   
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class ListOfProperties
        {

            private ListOfPropertiesPropertyDetails[] propertyDetailsField;

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("PropertyDetails")]
            public ListOfPropertiesPropertyDetails[] PropertyDetails
            {
                get
                {
                    return this.propertyDetailsField;
                }
                set
                {
                    this.propertyDetailsField = value;
                }
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class ListOfPropertiesPropertyDetails
        {

            private uint propertyDeedIDField;

            private string titleDeedNoField;

            private string propertyTypeDescField;

            private string buyerNameField;

            private string sellerNameField;

            private ushort standNoField;

            private object provinceField;

            private object bonusXMLField;

            private object tempReferenceField;

            private uint enquiryIDField;

            private uint enquiryResultIDField;

            private string referenceField;

            /// <remarks/>
            public uint PropertyDeedID
            {
                get
                {
                    return this.propertyDeedIDField;
                }
                set
                {
                    this.propertyDeedIDField = value;
                }
            }

            /// <remarks/>
            public string TitleDeedNo
            {
                get
                {
                    return this.titleDeedNoField;
                }
                set
                {
                    this.titleDeedNoField = value;
                }
            }

            /// <remarks/>
            public string PropertyTypeDesc
            {
                get
                {
                    return this.propertyTypeDescField;
                }
                set
                {
                    this.propertyTypeDescField = value;
                }
            }

            /// <remarks/>
            public string BuyerName
            {
                get
                {
                    return this.buyerNameField;
                }
                set
                {
                    this.buyerNameField = value;
                }
            }

            /// <remarks/>
            public string SellerName
            {
                get
                {
                    return this.sellerNameField;
                }
                set
                {
                    this.sellerNameField = value;
                }
            }

            /// <remarks/>
            public ushort StandNo
            {
                get
                {
                    return this.standNoField;
                }
                set
                {
                    this.standNoField = value;
                }
            }

            /// <remarks/>
            public object Province
            {
                get
                {
                    return this.provinceField;
                }
                set
                {
                    this.provinceField = value;
                }
            }

            /// <remarks/>
            public object BonusXML
            {
                get
                {
                    return this.bonusXMLField;
                }
                set
                {
                    this.bonusXMLField = value;
                }
            }

            /// <remarks/>
            public object TempReference
            {
                get
                {
                    return this.tempReferenceField;
                }
                set
                {
                    this.tempReferenceField = value;
                }
            }

            /// <remarks/>
            public uint EnquiryID
            {
                get
                {
                    return this.enquiryIDField;
                }
                set
                {
                    this.enquiryIDField = value;
                }
            }

            /// <remarks/>
            public uint EnquiryResultID
            {
                get
                {
                    return this.enquiryResultIDField;
                }
                set
                {
                    this.enquiryResultIDField = value;
                }
            }

            /// <remarks/>
            public string Reference
            {
                get
                {
                    return this.referenceField;
                }
                set
                {
                    this.referenceField = value;
                }
            }
        }

   
}
