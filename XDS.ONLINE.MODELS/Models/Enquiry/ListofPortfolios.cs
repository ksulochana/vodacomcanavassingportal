﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Xml.Serialization;

using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;

namespace XDS.ONLINE.MODELS
{

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRoot("Result")]
    public partial class ListofPortfolios
    {
        private PortfolioItem[] portfolio;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("PortfolioItem", IsNullable = false)]
        public PortfolioItem[] PortfolioList
        {
            get
            {
                return this.portfolio;
            }
            set
            {
                this.portfolio = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class PortfolioItem
    {
        private string portfolioID;
        private string portfolioName; 
        private string description; 
        private string comments; 
        private string createdByUser;
        private string createdOnDate;
        private string changedByUser;
        private string changedOnDate;

        public string PortfolioID
        {
            get
            {
                return portfolioID;
            }

            set
            {
                portfolioID = value;
            }
        }

        public string PortfolioName
        {
            get
            {
                return portfolioName;
            }

            set
            {
                portfolioName = value;
            }
        }
        public string Description
        {
            get
            {
                return description;
            }

            set
            {
                description = value;
            }
        } 

        public string Comments
        {
            get
            {
                return comments;
            }

            set
            {
                comments = value;
            }
        }
         
        public string CreatedByUser
        {
            get
            {
                return createdByUser;
            }

            set
            {
                createdByUser = value;
            }
        }
        public string CreatedOnDate
        {
            get
            {
                return createdOnDate;
            }

            set
            {
                createdOnDate = value;
            }
        }

        public string ChangedByUser
        {
            get
            {
                return changedByUser;
            }

            set
            {
                changedByUser = value;
            }
        }

        public string ChangedOnDate
        {
            get
            {
                return changedOnDate;
            }

            set
            {
                changedOnDate = value;
            }
        }       
    }
}