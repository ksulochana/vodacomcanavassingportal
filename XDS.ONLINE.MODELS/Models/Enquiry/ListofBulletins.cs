﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Xml.Serialization;

using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;

namespace XDS.ONLINE.MODELS
{

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRoot("Result")]
    public partial class ListofBulletins
    {

        private BulletinItem[] bulletinField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("BulletinItem", IsNullable = false)]
        public BulletinItem[] BulletinList
        {
            get
            {
                return this.bulletinField;
            }
            set
            {
                this.bulletinField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class BulletinItem
    {
        private string bulletinIDField;
        private string bulletinNameField;
        private byte[] bulletinField;
        private string docFileNameField;
        private string docContentTypeField;
        private string docLengthField;
        private string createdByUserField;
        private string createdOnDateField;
        public string BulletinID
        {
            get
            {
                return bulletinIDField;
            }

            set
            {
                bulletinIDField = value;
            }
        }

        public string DocName
        {
            get
            {
                return bulletinNameField;
            }

            set
            {
                bulletinNameField = value;
            }
        }

        public byte[] Doc
        {
            get
            {
                return bulletinField;
            }

            set
            {
                bulletinField = value;
            }
        }

        public string DocFileName
        {
            get
            {
                return docFileNameField;
            }

            set
            {
                docFileNameField = value;
            }
        }
        public string DocContentType
        {
            get
            {
                return docContentTypeField;
            }

            set
            {
                docContentTypeField = value;
            }
        }
        public string DocLength
        {
            get
            {
                return docLengthField;
            }

            set
            {
                docLengthField = value;
            }
        }
        public string CreatedByUser
        {
            get
            {
                return createdByUserField;
            }

            set
            {
                createdByUserField = value;
            }
        }
        public string CreatedOnDate
        {
            get
            {
                return createdOnDateField;
            }

            set
            {
                createdOnDateField = value;
            }
        }
    }
}