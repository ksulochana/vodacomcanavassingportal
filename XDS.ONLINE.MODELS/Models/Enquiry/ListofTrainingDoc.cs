﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Xml.Serialization;

using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;

namespace XDS.ONLINE.MODELS
{

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRoot("Result")]
    public partial class ListofTrainingDoc
    {

        private TrainingDocItem[] trainingDocField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("TrainingDocItem", IsNullable = false)]
        public TrainingDocItem[] TrainingDocList
        {
            get
            {
                return this.trainingDocField;
            }
            set
            {
                this.trainingDocField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class TrainingDocItem
    {
        private string trainingDocIDField;
        private string trainingDocNameField;
        private byte[] trainingDocField;
        private string docFileNameField;
        private string docContentTypeField;
        private string docLengthField;
        private string createdByUserField;
        private string createdOnDateField;
        private string videopathField;
        
        public string TrainingID
        {
            get
            {
                return trainingDocIDField;
            }

            set
            {
                trainingDocIDField = value;
            }
        }

        public string TrainingDocName
        {
            get
            {
                return trainingDocNameField;
            }

            set
            {
                trainingDocNameField = value;
            }
        }

        public byte[] TrainingDoc
        {
            get
            {
                return trainingDocField;
            }

            set
            {
                trainingDocField = value;
            }
        }

        public string TrainingDocFileName
        {
            get
            {
                return docFileNameField;
            }

            set
            {
                docFileNameField = value;
            }
        }
        public string TrainingDocContentType
        {
            get
            {
                return docContentTypeField;
            }

            set
            {
                docContentTypeField = value;
            }
        }
        public string TrainingDocLength
        {
            get
            {
                return docLengthField;
            }

            set
            {
                docLengthField = value;
            }
        }
        public string CreatedByUser
        {
            get
            {
                return createdByUserField;
            }

            set
            {
                createdByUserField = value;
            }
        }
        public string CreatedOnDate
        {
            get
            {
                return createdOnDateField;
            }

            set
            {
                createdOnDateField = value;
            }
        }
        public string VideofileLocation
        {
            get
            {
                return videopathField;
            }

            set
            {
                videopathField = value;
            }
        }
       
    }
}