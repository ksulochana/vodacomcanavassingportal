﻿using System.Collections.Generic;

namespace XDS.ONLINE.MODELS.ListOfProduct
    {
   
public class ClientInfo
{
    public string Successind { get; set; }
    public string ClientSessionID { get; set; }
    public string ClientMessage { get; set; }
}

public class ProductInfo
{
    public string ProductID { get; set; }
    public string Description { get; set; }
    public string TriesLeft { get; set; }
}

public class AnswerResponce
{
    public string SuccessInd { get; set; }
    public string ClientMessage { get; set; }
    public string ProductPath { get; set; }
    public string ProductReviewID { get; set; }
}

public class RootObject
{
    public List<ClientInfo> ClientInfo { get; set; }
    public List<ProductInfo> ProductInfo { get; set; }
}
}
