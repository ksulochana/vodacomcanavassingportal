﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace XDS.ONLINE.MODELS {
  public class SurveyQuestionViewModel {
    public string QuestionID { get; set; }
    public string Question { get; set; }
    public string AnswerTypeInd { get; set; }
    public bool PhoneNotCompatibleWithDropdown { get; set; }
    
    [StringLength(500)]
    public string Answer { get; set; }

    public IEnumerable<SelectListItem> AnswerOptionsList { get; set; }
  }

  public class SurveyViewModel {
    public List<SurveyQuestionViewModel> Questions { get; set; }

    [Required]
    //[CustomRegularExpression(RegularExpressionTypes.NumericOnly)]
    public string ProductID { get; set; }

    [Required]
    //[CustomRegularExpression(RegularExpressionTypes.FreeText)]
    public string ClientSessionID { get; set; }

    [Required]
    //[CustomRegularExpression(RegularExpressionTypes.FreeText)]
    public string TemplateTypeCode { get; set; }
       
     [Required]
    //[CustomRegularExpression(RegularExpressionTypes.FreeText)]
    public string ProductReviewID { get; set; }

     [Required]
     //[CustomRegularExpression(RegularExpressionTypes.FreeText)]
     public string TemplateID { get; set; }
  }
}
