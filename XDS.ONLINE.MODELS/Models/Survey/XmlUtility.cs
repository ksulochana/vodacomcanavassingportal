﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Xml;

namespace XDS.ONLINE.MODELS.MobileSurvey
{
    public class XmlUtility
    {
        #region Public Static Methods
        public static string CleanSQLValue(string xml)
        {
            xml = xml.Replace("'", "''");
            xml = xml.Replace("\n", " ");
            xml = xml.Replace("\r", " ");

            return xml;
        }

        public static XmlDocument LoadXmlDocument(string xml)
        {
            XmlDocument oXmlDocument = new XmlDocument();

            oXmlDocument.LoadXml(xml);

            return oXmlDocument;
        }

        public static XmlNode AddNode(XmlDocument xmlDocument, XmlNode node, string nodeName, string nodeValue)
        {
            XmlNode oNewNode = xmlDocument.CreateNode(XmlNodeType.Element, node.Prefix, nodeName, node.NamespaceURI);

            oNewNode.InnerText = nodeValue == null ? "" : nodeValue;
            node.AppendChild(oNewNode);

            return oNewNode;
        }

        public static XmlNode AddNode(XmlDocument xmlDocument, string nodeName, string nodeValue)
        {
            XmlNode oNewNode = xmlDocument.CreateNode(XmlNodeType.Element, xmlDocument.Prefix, nodeName, xmlDocument.NamespaceURI);

            oNewNode.InnerText = nodeValue;
            xmlDocument.AppendChild(oNewNode);

            return oNewNode;
        }

        public static void RemoveNode(XmlNode parentNode, string nodeName)
        {
            XmlNode oXmlNode = GetNode(parentNode, nodeName);

            if (oXmlNode != null)
            {
                parentNode.RemoveChild(oXmlNode);
            }
        }

        public static string GetElementTag(XmlDocument xmlDocument, string tag, bool valid, string status, string errors, bool mandatory)
        {
            // Return Element string
            // If bMandatory is true then if Tag is missing or Empty then:
            // append Error, set status to FAILED and set vbValid = False
            string sElementValue = string.Empty;

            if (xmlDocument.GetElementsByTagName(tag).Item(0) != null)
            {
                sElementValue = xmlDocument.GetElementsByTagName(tag).Item(0).InnerText;
            }

            if (mandatory && sElementValue.Trim() == string.Empty)
            {
                errors = string.Format("{0}<Error>{1} is Missing or Empty</Error>", errors, "<Error>", tag);
                status = "FAILED";
                valid = false;
            }
            return sElementValue;
        }

        public static XmlNode GetNode(XmlNode xmlNode, string name)
        {
            if (xmlNode[name, xmlNode.NamespaceURI] != null)
            {
                return xmlNode[name, xmlNode.NamespaceURI];
            }
            return null;
        }

        public static string GetValue(XmlNode xmlNode)
        {
            return GetValue(xmlNode, string.Empty);
        }

        public static string GetValue(XmlNode xmlNode, string defaultValue)
        {
            if (xmlNode != null)
            {
                return xmlNode.InnerText;
            }
            return defaultValue;
        }

        public static string GetNodeValue(XmlNode xmlNode, string name)
        {
            return GetNodeValue(xmlNode, name, string.Empty);
        }

        public static string GetNodeValue(XmlNode xmlNode, string name, string defaultValue)
        {
            XmlNode oXmlNode = GetNode(xmlNode, name);

            if (oXmlNode != null)
            {
                return oXmlNode.InnerText;
            }
            return defaultValue;
        }

        public static void UpdateNodeValue(XmlNode xmlNode, string name, string value)
        {
            XmlNode oXmlNode = GetNode(xmlNode, name);

            if (oXmlNode != null)
            {
                if (!String.IsNullOrEmpty(value))
                {
                    oXmlNode.InnerText = value;
                }
                else
                {
                    RemoveNode(xmlNode, name);
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(value))
                {
                    AddNode(xmlNode.OwnerDocument, xmlNode, name, value);
                }
            }
        }
        #endregion
    }
}
