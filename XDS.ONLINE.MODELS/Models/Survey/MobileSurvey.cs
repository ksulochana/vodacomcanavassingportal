﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using XDS.ONLINE.MODELS;
using XDS.ONLINE.MODELS.MobileSurvey;

namespace XDS.ONLINE.MODELS.MobileSurvey
{
  public class MobileSurvey {
    #region Private Properties
    private XmlDocument moXmlDocument;
    #endregion

    #region Public Properties
    public SurveyModel Survey {
      get {
        SurveyModel oSurveyModel = new SurveyModel();

        oSurveyModel.Questions = this.QuestionList;

        return oSurveyModel;
      }
    }

    public List<SurveyModel.QuestionModel> QuestionList {
      get {
        List<SurveyModel.QuestionModel> oList = new List<SurveyModel.QuestionModel>();
        XmlNode oXmlNode = GetSurvey_SurveyQuestionsXmlNode(moXmlDocument);

        if (oXmlNode != null) {
          foreach (XmlNode oSurveyQuestionXmlNode in oXmlNode.ChildNodes) {
            SurveyModel.QuestionModel oQuestionModel = new SurveyModel.QuestionModel {
              QuestionID = XmlUtility.GetNodeValue(oSurveyQuestionXmlNode, "QuestionID"),
              Question = XmlUtility.GetNodeValue(oSurveyQuestionXmlNode, "Question"),
              AnswerTypeInd = XmlUtility.GetNodeValue(oSurveyQuestionXmlNode, "AnswerTypeInd"),
              AnswerOptions = new List<string>()
            };
            XmlNode oAnswerOptionsXmlNode = GetSurvey_SurveyQuestions_AnswerOptionsXmlNode(oSurveyQuestionXmlNode);

            if (oAnswerOptionsXmlNode != null) {
              foreach (XmlNode oAnswerOptionXmlNode in oAnswerOptionsXmlNode.ChildNodes) {
                if (oAnswerOptionXmlNode.LocalName == "AnswerOption") {
                  oQuestionModel.AnswerOptions.Add(XmlUtility.GetValue(oAnswerOptionXmlNode));
                }
              }
            }
            oList.Add(oQuestionModel);
          }
        }
        return oList;
      }
      set {
        XmlNode oXmlNode = GetSurvey_SurveyQuestionsXmlNode(moXmlDocument);

        foreach (XmlNode oSurveyQuestionXmlNode in oXmlNode.ChildNodes) {
          SurveyModel.QuestionModel oQuestionModel = value.Where(qm => qm.QuestionID == XmlUtility.GetNodeValue(oSurveyQuestionXmlNode, "QuestionID")).SingleOrDefault();

          if (oQuestionModel != null) {
            XmlUtility.UpdateNodeValue(oSurveyQuestionXmlNode, "Answer", oQuestionModel.Answer);
          }
        }
      }
    }

    #region Helper Functions
    public static XmlNode GetSurveyXmlNode(XmlDocument xmlDocument) {
      return xmlDocument.SelectSingleNode("/*[local-name()='Survey']");
    }

    public static XmlNode GetSurvey_SurveyQuestionsXmlNode(XmlDocument xmlDocument) {
      XmlNode oXmlNode = xmlDocument.SelectSingleNode("/*[local-name()='Survey']/*[local-name()='SurveyQuestions']");

      if (oXmlNode == null) {
        oXmlNode = XmlUtility.AddNode(xmlDocument, GetSurveyXmlNode(xmlDocument), "SurveyQuestions", string.Empty);
      }

      return oXmlNode;
    }

    public static XmlNode GetSurvey_SurveyQuestions_AnswerOptionsXmlNode(XmlNode xmlNode) {
      XmlNode oXmlNode = XmlUtility.GetNode(xmlNode, "AnswerOptions");

      if (oXmlNode == null) {
        oXmlNode = XmlUtility.AddNode(xmlNode.OwnerDocument, xmlNode, "AnswerOptions", string.Empty);
      }

      return oXmlNode;
    }
    #endregion
    #endregion

    #region Constructor
    public MobileSurvey(string surveyXml) {
      moXmlDocument = XmlUtility.LoadXmlDocument(surveyXml);
    }
    #endregion

    #region Base Overrides
    public override string ToString() {
      return moXmlDocument.OuterXml;
    }
    #endregion

    #region Sample Xml
    //<Survey>
    //  <SurveyQuestions>
    //    <SurveyQuestion>
    //      <Question />
    //      <AnswerTypeInd />
    //      <AnswerOptions>
    //        <AnswerOption/>
    //      <AnswerOptions/>
    //      <Answer />
    //    </SurveyQuestion>
    //  </SurveyQuestions>
    //</Survey>
    #endregion
  }
}
