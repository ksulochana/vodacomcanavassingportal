﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XDS.ONLINE.MODELS
{
    #region Class SurveyModel
    public class SurveyModel
    {
        #region Class QuestionModel
        public class QuestionModel
        {
            #region Public Properties
            public string QuestionID;
            public string Question;
            public string AnswerTypeInd;
            public List<string> AnswerOptions;
            public string Answer;
            #endregion
        }
        #endregion

        #region Public Properties
        public int MobiTemplateID;
        public string TemplateXml;
        public bool PhoneNotCompatibleWithDropdown;
        public List<QuestionModel> Questions;
        #endregion
    }
    #endregion
}
