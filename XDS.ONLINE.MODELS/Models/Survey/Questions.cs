﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDS.ONLINE.MODELS.Questions
{

    public class QuestionsTempalte
    {
        public string SuccessInd { get; set; }
        public string ClientMessage { get; set; }
        public string TemplateID { get; set; }
        public string TemplateXML { get; set; }
        public string ProductReviewID { get; set; }
    }

}
