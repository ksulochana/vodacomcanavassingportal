﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDS.ONLINE.MODELS
{
    public class ConnectGetResultUnblockModel
    {
        //public string ConnectTicket { get; set; }
        public int BlockID { get; set; }
        public string UnblockReason { get; set; }
    }
}
