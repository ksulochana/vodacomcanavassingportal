﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;

namespace XDS.ONLINE.MODELS
{
    public class ErfEnquiryMatchViewModel
    {

 

        [Display(Name = "Deeds Office")]
        public List<DeedOfficeTypes> DeedsOffice { get; set; }


        [Display(Name = "Erf Number")]
        [Required(ErrorMessage = "Please enter an Erf Number.")]
        [StringLength(6, ErrorMessage = "The Erf Number entered is invalid. Please enter a valid Erf Number.", MinimumLength = 1)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        public string ErfNumber { get; set; }

        [Display(Name = "Township")]
        [Required(ErrorMessage = "Please enter township.")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string Township { get; set; }

        [Display(Name = "Portion Number")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string PortionNumber { get; set; }


        [Display(Name = "Voucher Code")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string VoucherCode { get; set; }

        [Display(Name = "Your Reference")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(ReferenceChars.Pattern, ErrorMessage = ReferenceChars.Message)]
        public string YourReference { get; set; }

        public int ProductID { get; set; }

        [Required(ErrorMessage = "Please select a Deeds Office.")]
        [RequiredIf("DeedsOfficeValue == 'Please select'", ErrorMessage = "Please select Deeds Office.")]
        public string DeedsOfficeValue { get; set; }

     }




    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class Result
    {

        private string[] deedsOfficesField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Office", IsNullable = false)]
        public string[] DeedsOffices
        {
            get
            {
                return this.deedsOfficesField;
            }
            set
            {
                this.deedsOfficesField = value;
            }
        }
    }


    public class DeedOfficeTypes
    {

        public string Display { get; set; }

        public string Value { get; set; }

    }



    
}








