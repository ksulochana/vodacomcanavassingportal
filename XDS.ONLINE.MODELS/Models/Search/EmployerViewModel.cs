﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;


namespace XDS.ONLINE.MODELS
{
    public class EmployerViewModel
    {
        [Display(Name = "Employment Date")]
        [RegularExpression(DateDMY.Pattern, ErrorMessage = "The Employement Date supplied is not in the correct format. Please enter the date in the format yyyy/mm/dd")]
        [Required]
        public string EmployementDate { get; set; }

        [Display(Name = "Employer Name")]
        [Required]
        public string EmployerName { get; set; }

        [Display(Name = "Employer Telephone Number")]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        public string EmployerTelephone { get; set; }

        public int EnquiryID { get; set; }
        public int ProductID { get; set; }
        public int EnquiryResultID { get; set; }
        private string _externalReference;

        public string ExternalReference
        {
            get { return _externalReference == null ? "" : _externalReference; }
            set { _externalReference = value; }
        }
        
    }

  
}
