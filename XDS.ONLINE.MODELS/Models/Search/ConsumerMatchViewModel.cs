﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;

namespace XDS.ONLINE.MODELS
{

    public class ConsumerMatchViewModel
    {
        [Display(Name = "ID Number")]
        [RequiredIf("IsNullOrWhiteSpace(FirstName) && IsNullOrWhiteSpace(Surname) && IsNullOrWhiteSpace(DateOfBirth) && IsNullOrWhiteSpace(Passport)", ErrorMessage = "Either search by ID/Passport or Name, Surname and Date of Birth.")]
        [StringLength(13, ErrorMessage = "The ID Number entered is invalid. Please enter a valid 13 digit ID Number.", MinimumLength = 13)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        public string IdNumber { get; set; }

        [Display(Name = "Passport Number/Other ID")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RequiredIf("(IsNullOrWhiteSpace(FirstName) && IsNullOrWhiteSpace(Surname) && IsNullOrWhiteSpace(DateOfBirth) && IsNullOrWhiteSpace(IdNumber))", ErrorMessage = "Either search by ID/Passport or Name, Surname and Date of Birth.")]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string Passport { get; set; }

        [Display(Name = "First Name")]
        [RequiredIf("(IsNullOrWhiteSpace(IdNumber) && IsNullOrWhiteSpace(Passport))", ErrorMessage = "Either search by ID/Passport or Name, Surname and Date of Birth.")]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        public string FirstName { get; set; }

        [Display(Name = "Surname")]
        [RequiredIf("(IsNullOrWhiteSpace(IdNumber) && IsNullOrWhiteSpace(Passport))", ErrorMessage = "Either search by ID/Passport or Name, Surname and Date of Birth.")]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        public string Surname { get; set; }

        [Display(Name = "Date of Birth")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [RequiredIf("(IsNullOrWhiteSpace(IdNumber) && IsNullOrWhiteSpace(Passport))", ErrorMessage = "Either search by ID/Passport or Name, Surname and Date of Birth.")]
        //[RegularExpression(DateDMY.Pattern, ErrorMessage = "The date of birth supplied is not in the correct format. Please enter the date in the format dd/mm/yyyy.")]
        //[AssertThat("DateOfBirth <= Today()", ErrorMessage = "The date of birth cannot be a future date. Please enter a valid date of birth.")]
        //public DateTime? DateOfBirth { get; set; }
        public string DateOfBirth { get; set; }

        [Display(Name = "Your Reference")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(ReferenceChars.Pattern, ErrorMessage = ReferenceChars.Message)]
        public string YourReference { get; set; }

        [Display(Name = "Voucher Code")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(AlphanumericDash.Pattern, ErrorMessage = AlphanumericDash.Message)]
        public string VoucherCode { get; set; }

        public int ProductID { get; set; }
    }
}
