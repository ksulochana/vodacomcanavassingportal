﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;

namespace XDS.ONLINE.MODELS
{
    public class ChangePasswordViewModel
    {
        [Display(Name = "Current Password")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Current Password is a required field.")]
        public string CurrentPassword { get; set; }

        [Display(Name = "New Password")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "New Password is a required field.")]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?])[A-Za-z\d$@$!%*?]{6,10}", ErrorMessage = "The password is in the incorrect format. Min 6, Max 10 characters, at least one uppercase, at least one lower case, at least one special character and a number.Special characters allowed are @,$,!,%,*,?")]
        public string NewPassword { get; set; }

        [Display(Name = "Confirm Password")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Confirm Password is a required field.")]
        public string ConfirmPassword { get; set; }

        public string Result { get; set; }


    }
}
