﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;

namespace XDS.ONLINE.MODELS
{
    public class AddressMatchViewModel 
    {

        public List<Province> _items = new List<Province>();

        public AddressMatchViewModel()
        {
            PostalMatch = false;

            _items.Add(new Province { Text = "Eastern Cape", Value = "Eastern Cape" });
            _items.Add(new Province { Text = "Free State", Value = "Free State" });
            _items.Add(new Province { Text = "Gauteng", Value = "Gauteng" });
            _items.Add(new Province { Text = "Kwazulu-Natal", Value = "Kwazulu-Natal" });
            _items.Add(new Province { Text = "Limpopo", Value = "Limpopo" });
            _items.Add(new Province { Text = "Mpumalanga", Value = "Mpumalanga" });
            _items.Add(new Province { Text = "North West", Value = "North West" });
            _items.Add(new Province { Text = "Northern Cape", Value = "Northern Cape" });
            _items.Add(new Province { Text = "Western Cape", Value = "Western Cape" });
        }

        [Display(Name = "Province")]
        public List<Province> Province
        {
            get
            {
                return _items;
            }
            set
            {
                _items = value;

            }
        }

        [Display(Name = "City")]
        [Required(ErrorMessage = "City is a required field.")]
        [StringLength(50, ErrorMessage = "The {0} must not be more than {2} characters long.")]
        //[RegularExpression(Alphabetic.Pattern, ErrorMessage = Alphabetic.Message)]
        [RegularExpression(NameOrganisation.Pattern, ErrorMessage = NameOrganisation.Message)]
        public string City { get; set; }

        //[Display(Name = "Search On")]
        //public string SearchOn { get; set; }

        [Display(Name = "Suburb")]
        [RequiredIf("PostalMatch == false", ErrorMessage = "Suburb is a required field")]
        [StringLength(50, ErrorMessage = "The {0} must not be more than {2} characters long.")]
        //[RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        [RegularExpression(NameOrganisation.Pattern, ErrorMessage = NameOrganisation.Message)]
        public string Suburb { get; set; }

        [Display(Name = "Street Number")]
        [StringLength(6, ErrorMessage = "The {0} must not be more than {2} characters long.")]
        //[RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string StreetNo { get; set; }

        [Display(Name = "Street Name")]
        [RequiredIf("PostalMatch == false", ErrorMessage = "Street Name is a required field.")]
        [StringLength(50, ErrorMessage = "The {0} must not be more than {2} characters long.")]
        [RegularExpression(NameOrganisation.Pattern, ErrorMessage = NameOrganisation.Message)]
        public string StreetName { get; set; }

        [Display(Name = "Postal Code")]
        [RequiredIf("PostalMatch == false", ErrorMessage = "Postal Code is a required field.")]
        [StringLength(4, ErrorMessage = "The {0} must not be more than {2} characters long.")]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        public string PostalCode { get; set; }

        [Display(Name = "Suburb")]
        [RequiredIf("PostalMatch == true", ErrorMessage = "Postal Suburb is a required field")]
        [StringLength(50, ErrorMessage = "The {0} must not be more than {2} characters long.")]
        [RegularExpression(NameOrganisation.Pattern, ErrorMessage = NameOrganisation.Message)]
        public string SuburbPostal { get; set; }

        [Display(Name = "Postal Number")]
        [RequiredIf("PostalMatch == true", ErrorMessage = "Postal Number is a required field.")]
        [StringLength(15, ErrorMessage = "The {0} must be at least {2} characters long.")]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string PostalNumber { get; set; }

        [Display(Name = "Postal Code")]
        [RequiredIf("PostalMatch == true", ErrorMessage = "Postal Code is a required field.")]
        [StringLength(4, ErrorMessage = "The {0} must not be more than {2} characters long.")]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        public string PostalCodePostal { get; set; }

        [Display(Name = "Surname")]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        [StringLength(50, ErrorMessage = "The {0} must not be more than {2} characters long.")]
        public string Surname { get; set; }

        [Display(Name = "Voucher Code")]
        [StringLength(50, ErrorMessage = "The {0} must not be more than {2} characters long.")]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string VoucherCode { get; set; }

        [Display(Name = "Your Reference")]
        [StringLength(50, ErrorMessage = "The {0} must not be more than {2} characters long.")]
        [RegularExpression(ReferenceChars.Pattern, ErrorMessage = ReferenceChars.Message)]
        public string YourReference { get; set; }

        public bool PostalMatch { get; set; }

        public string ProvinceValue { get; set; }

    }

    public class Province
    {
        string _text;
        string _value;

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }
}
