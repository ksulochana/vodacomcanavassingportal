﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;


namespace XDS.ONLINE.MODELS
{
   public class CompanyEnquiryMatchViewModel
    {
        
        [Display(Name = "Deeds Office")]
        public List<DeedOfficeTypes> DeedsOffice { get; set; }


        [Display(Name = "Registration Number")]
        [StringLength(4, ErrorMessage = "Please enter a valid 4 digit Number.", MinimumLength = 4)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [RequiredIf("IsNullOrWhiteSpace(BusinessName)  || !IsNullOrWhiteSpace(Reg2) || !IsNullOrWhiteSpace(Reg3)", ErrorMessage = "Required for Registration Number.")]
        public string Reg1 { get; set; }

        [StringLength(6, ErrorMessage = "Please enter a valid 6 digit Number.", MinimumLength = 6)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [RequiredIf("!IsNullOrWhiteSpace(Reg1) || !IsNullOrWhiteSpace(Reg3)", ErrorMessage = "Required for Registration Number.")]
        public string Reg2 { get; set; }

        [StringLength(2, ErrorMessage = "Please enter a valid 4 digit Number.", MinimumLength = 2)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [RequiredIf("!IsNullOrWhiteSpace(Reg1) || !IsNullOrWhiteSpace(Reg2)", ErrorMessage = "Required for Registration Number.")]
        public string Reg3 { get; set; }

              
        [Display(Name = "Business Name")]        
        [RegularExpression(NameOrganisation.Pattern, ErrorMessage = NameOrganisation.Message)]       
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        [RequiredIf("IsNullOrWhiteSpace(Reg1)", ErrorMessage = "Please enter business name.")]
        public string BusinessName { get; set; }

             
        [Display(Name = "Voucher Code")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string VoucherCode { get; set; }

        [Display(Name = "Your Reference")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(ReferenceChars.Pattern, ErrorMessage = ReferenceChars.Message)]
        public string YourReference { get; set; }

        public int ProductID { get; set; }

        [Required(ErrorMessage = "Please select a Deeds Office.")]
        [RequiredIf("DeedsOfficeValue == 'Please select'", ErrorMessage = "Please select Deeds Office.")]
        public string DeedsOfficeValue { get; set; }

        
    }

  
}
