﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;

namespace XDS.ONLINE.MODELS
{
    public class BankingAVSMatchViewModel
    {
        public List<TypeOfVerificationAVS> _typeOfVerificationItems = new List<TypeOfVerificationAVS>();
        public List<IdentificationType> _identityTypeItems = new List<IdentificationType>();

        public BankingAVSMatchViewModel()
        {
            _typeOfVerificationItems.Add(new TypeOfVerificationAVS { Text = "Individual", Value = "Individual" });
            _typeOfVerificationItems.Add(new TypeOfVerificationAVS { Text = "Company", Value = "Company" });

            _identityTypeItems.Add(new IdentificationType { Text = "SA ID Number", Value = "SA ID Number" });
            _identityTypeItems.Add(new IdentificationType { Text = "Passport", Value = "Passport" });
        }

        [Display(Name = "Type of Verification:")]
        public List<TypeOfVerificationAVS> TypeOfVerification
        {
            get
            {
                return _typeOfVerificationItems;
            }
            set
            {
                _typeOfVerificationItems = value;
            }
        }

        [Display(Name = "Identification Type:")]
        public List<IdentificationType> IdentificationType
        {
            get
            {
                return _identityTypeItems;
            }
            set
            {
                _identityTypeItems = value;
            }
        }

        [Display(Name = "Initials")]
        [StringLength(5, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        [RequiredIf("(TypeOfVerificationValue == 'Individual')", ErrorMessage = "Initials is a required field.")]
        public string Initials { get; set; }

        [Display(Name = "Surname")]
        [StringLength(30, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 3)]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        [RequiredIf("(TypeOfVerificationValue == 'Individual')", ErrorMessage = "Surname is a required field.")]
        public string RequestSurname { get; set; }

        [Display(Name = "ID Number")]
        [StringLength(13, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 13)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [RequiredIf("(TypeOfVerificationValue == 'Individual' && IdentificationTypeValue == 'SA ID Number')", ErrorMessage = "ID Number is a required field.")]
        public string IdNumber { get; set; }

        [Display(Name = "Passport")]
        [StringLength(30, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 5)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        [RequiredIf("(TypeOfVerificationValue == 'Individual' && IdentificationTypeValue == 'Passport')", ErrorMessage = "Passport is a required field.")]
        public string Passport { get; set; }

        [Display(Name = "Company Name")]
        [StringLength(30, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 3)]
        [RegularExpression(NameOrganisation.Pattern, ErrorMessage = Alphanumeric.Message)]
        [RequiredIf("(TypeOfVerificationValue == 'Company')", ErrorMessage = "Company Name is a required field.")]
        public string CompanyName { get; set; }

        [Display(Name = "Registration Number")]
        [StringLength(5, ErrorMessage = "Please enter a valid 4 digit Number.", MinimumLength = 4)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        [RequiredIf("(TypeOfVerificationValue == 'Company')", ErrorMessage = "Registration Number is a required field.")]
        public string RegistrationNumber1 { get; set; }

        [StringLength(6, ErrorMessage = "Please enter a valid 6 digit Number.", MinimumLength = 6)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [RequiredIf("(TypeOfVerificationValue == 'Company' && !IsNullOrWhiteSpace(RegistrationNumber1)  )", ErrorMessage = "Registration Number is a required field.")]
        public string RegistrationNumber2 { get; set; }

        [StringLength(2, ErrorMessage = "Please enter a valid 2 digit Number.", MinimumLength = 2)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [RequiredIf("(TypeOfVerificationValue == 'Company' && !IsNullOrWhiteSpace(RegistrationNumber2) )", ErrorMessage = "Registration Number is a required field.")]
        public string RegistrationNumber3 { get; set; }



        [Display(Name = "Account Number")]
        [StringLength(13, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [Required(ErrorMessage = "Account Number is a required field.")]
        public string AccountNumber { get; set; }

        [Display(Name = "Branch Code")]
        [StringLength(6, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 4)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [Required(ErrorMessage = "Branch Code is a required field.")]
        public string BranchCode { get; set; }

        [Display(Name = "Account Type")]
        public ListofBankAccountTypes BankAccountType { get; set; }

        [Display(Name = "Bank Name")]
        public ListOfBanks BankName { get; set; }


        [Display(Name = "Voucher Code")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string VoucherCode { get; set; }

        [Display(Name = "Your Reference")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(ReferenceChars.Pattern, ErrorMessage = ReferenceChars.Message)]
        public string YourReference { get; set; }

        public string IdentificationTypeValue { get; set; }
        public string TypeOfVerificationValue { get; set; }

        [Display(Name = "Account Type")]
        [Required]
        public string BankAccountTypeValue { get; set; }
        
        [Display(Name = "Bank Name")]
        [Required]
        public string BankNameValue { get; set; }
        //public string TermsValue { get; set; }

    }

    public class IdentificationType
    {
        string _text;
        string _value;

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }

    public class TypeOfVerificationAVS
    {
        string _text;
        string _value;

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }

}
