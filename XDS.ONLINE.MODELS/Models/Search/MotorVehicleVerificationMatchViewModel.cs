﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;

namespace XDS.ONLINE.MODELS
{
    public class MotorVehicleVerificationMatchViewModel
    {
        [Display(Name = "ID Number")]
        [RequiredIf("IsNullOrWhiteSpace(FirstName) && IsNullOrWhiteSpace(Surname) && IsNullOrWhiteSpace(DateOfBirth) && IsNullOrWhiteSpace(Passport)", ErrorMessage = "Either search by ID/Passport or Name, Surname and Date of Birth.")]
        [StringLength(13, ErrorMessage = "The ID Number entered is invalid. Please enter a valid 13 digit ID Number.", MinimumLength = 13)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        public string IdNumber { get; set; }

        [Display(Name = "Passport Number/Other ID")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RequiredIf("(IsNullOrWhiteSpace(FirstName) && IsNullOrWhiteSpace(Surname) && IsNullOrWhiteSpace(DateOfBirth) && IsNullOrWhiteSpace(IdNumber))", ErrorMessage = "Either search by ID/Passport or Name, Surname and Date of Birth.")]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string Passport { get; set; }

        [Display(Name = "First Name")]
        [RequiredIf("(IsNullOrWhiteSpace(IdNumber) && IsNullOrWhiteSpace(Passport))", ErrorMessage = "Either search by ID/Passport or Name, Surname and Date of Birth.")]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        public string FirstName { get; set; }

        [Display(Name = "Surname")]
        [RequiredIf("(IsNullOrWhiteSpace(IdNumber) && IsNullOrWhiteSpace(Passport))", ErrorMessage = "Either search by ID/Passport or Name, Surname and Date of Birth.")]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        public string Surname { get; set; }

        [Display(Name = "Date of Birth")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [RequiredIf("(IsNullOrWhiteSpace(IdNumber) && IsNullOrWhiteSpace(Passport))", ErrorMessage = "Either search by ID/Passport or Name, Surname and Date of Birth.")]
        public string DateOfBirth { get; set; }

        [Display(Name = "Vehicle Identification Number (VIN)")]
        [Required(ErrorMessage = "VIN Number is a required field.")]
        [StringLength(30, ErrorMessage = "The VIN Number entered is invalid. Please enter a valid 30 digit VIN Number.", MinimumLength = 1)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string VINNumber { get; set; }

        [Display(Name = "Registration Number")]
        [StringLength(9, ErrorMessage = "The Registration Number entered is invalid. Please enter a valid  Registration Number.", MinimumLength = 2)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string RegistrationNumber { get; set; }

        [Display(Name = "Your Reference")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(ReferenceChars.Pattern, ErrorMessage = ReferenceChars.Message)]
        public string YourReference { get; set; }

        [Display(Name = "Voucher Code")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(AlphanumericDash.Pattern, ErrorMessage = AlphanumericDash.Message)]
        public string VoucherCode { get; set; }

        public int ProductID { get; set; }
        public string Manufacturer { get; set; }
        public string Year { get; set; }
        public string VehicleModel { get; set; }
        public string NewCode { get; set; }
    }
}
