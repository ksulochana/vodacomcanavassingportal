﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Xml.Serialization;

using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;

namespace XDS.ONLINE.MODELS
{
    public class MaritalStatusMatchViewModel
    {
        [Display(Name = "ID Number")]
        [StringLength(13, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 13)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        //[RequiredIf("(Passport == '' && Surname == '' && FirstName == '' && DateOfBirth == '')", ErrorMessage = "Either search by ID Number or Passport Number.")]
        [RequiredIf("(IsNullOrWhiteSpace(Passport) && IsNullOrWhiteSpace(Surname) && IsNullOrWhiteSpace(FirstName) && IsNullOrWhiteSpace(DateOfBirth))", ErrorMessage = "Either search by ID Number or Passport Number.")]
        public string IdNumber { get; set; }

        //[Required(ErrorMessage="Testing")]
        [Display(Name = "Passport Number/Other ID")]
        [StringLength(30, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        //[RequiredIf("IdNumber == '' && Surname == '' && FirstName == '' && DateOfBirth == ''", ErrorMessage = "Either search by ID Number or Passport Number.")]
        [RequiredIf("(IsNullOrWhiteSpace(IdNumber) && IsNullOrWhiteSpace(Surname) && IsNullOrWhiteSpace(FirstName) && IsNullOrWhiteSpace(DateOfBirth))", ErrorMessage = "Either search by ID Number or Passport Number.")]
        public string Passport { get; set; }

        [Display(Name = "First Name")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        [RequiredIf("(IsNullOrWhiteSpace(IdNumber) && IsNullOrWhiteSpace(Passport))", ErrorMessage = "First Name is required if not searching by ID Number or Passport Number.")]
        public string FirstName { get; set; }

        [Display(Name = "Surname")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        [RequiredIf("(IsNullOrWhiteSpace(IdNumber) && IsNullOrWhiteSpace(Passport))", ErrorMessage = "Surname is required if not searching by ID Number or Passport Number.")]
        public string Surname { get; set; }

        [Display(Name = "Date of Birth")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [RequiredIf("(IsNullOrWhiteSpace(IdNumber) && IsNullOrWhiteSpace(Passport))", ErrorMessage = "Date of Birth is required if not searching by ID Number or Passport Number.")]
        public string DateOfBirth { get; set; }

        [Display(Name = "Voucher Code")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string VoucherCode { get; set; }

        [Display(Name = "Your Reference")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(ReferenceChars.Pattern, ErrorMessage = ReferenceChars.Message)]
        public string YourReference { get; set; }

        public int ProductID { get; set; }
    }
}
