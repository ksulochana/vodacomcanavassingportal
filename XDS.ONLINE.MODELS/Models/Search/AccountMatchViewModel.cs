﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using Shandon.Utils.RegExPatterns;

namespace XDS.ONLINE.MODELS
{

    public class AccountMatchViewModel
    {
        [Display(Name = "Account Number")]
        [StringLength(25, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [Required(ErrorMessage="Account Number is a required field.")]
        //[NumericFormat]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string AccountNumber { get; set; }

        [Display(Name = "Surname")]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        public string Surname { get; set; }

        [Display(Name = "Sub Account Number")]
        [StringLength(20, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string SubAccountNumber { get; set; }

        [Display(Name = "Voucher Code")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(AlphanumericDash.Pattern, ErrorMessage = AlphanumericDash.Message)]
        public string VoucherCode { get; set; }

        [Display(Name = "Your Reference")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(ReferenceChars.Pattern, ErrorMessage = ReferenceChars.Message)]
        public string YourReference { get; set; }

        [Required]
        public int ProductID { get; set; }
    }
}
