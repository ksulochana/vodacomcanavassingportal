﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDS.ONLINE.MODELS
{
    public class ConnectGetResultModel
    {
        public string ConnectTicket { get; set; }
        public int EnquiryID { get; set; }
        public int EnquiryResultID { get; set; }
        public int ProductID { get; set; }
        public string BonusXML { get; set; }
        public int Enquiry2ResultID { get; set; }
        public string NewCode { get; set; }
        public long ConsumerID { get; set; }
    }
}
