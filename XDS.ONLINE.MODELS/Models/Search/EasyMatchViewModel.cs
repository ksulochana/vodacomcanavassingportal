﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;

namespace XDS.ONLINE.MODELS
{

    public class EasyMatchViewModel
    {
        public List<TypeOfSearch> _typeOfSearchitems = new List<TypeOfSearch>();
        public List<DeviationOfAge> _deviationIitems = new List<DeviationOfAge>();

        public EasyMatchViewModel()
        {
            _typeOfSearchitems.Add(new TypeOfSearch { Text = "Exact Search", Value = "Exact Search" });
            _typeOfSearchitems.Add(new TypeOfSearch { Text = "Deviation", Value = "Deviation" });

            _deviationIitems.Add(new DeviationOfAge { Text = "0", Value = "0" });
            _deviationIitems.Add(new DeviationOfAge { Text = "1", Value = "1" });
            _deviationIitems.Add(new DeviationOfAge { Text = "2", Value = "2" });
            _deviationIitems.Add(new DeviationOfAge { Text = "3", Value = "3" });
            _deviationIitems.Add(new DeviationOfAge { Text = "4", Value = "4" });
            _deviationIitems.Add(new DeviationOfAge { Text = "5", Value = "5" });

        }

        [Display(Name = "Type of Search")]
        public List<TypeOfSearch> TypeOfSearch 
        {
            get 
            {
                return _typeOfSearchitems; 
            }
            set 
            {
                _typeOfSearchitems = value;

            }
        }

        [StringLength(20, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        public string SearchType { get; set; }

        
        [Display(Name = "Surname")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        [Required(ErrorMessage = "Surname is a required field.")]
        public string Surname { get; set; }

        [Display(Name = "First Name")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        [Required(ErrorMessage = "First Name is a required field.")]
        public string FirstName { get; set; }

        [Display(Name = "Age")]
        [AssertThat("Age > 17", ErrorMessage = "The minimum age allowed is 18.")]
        //[StringLength(3, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [Required(ErrorMessage = "Age is a required field.")]
        public int Age { get; set; }

        
        [Display(Name = "Year (YYYY)")]
        //[StringLength(4, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 4)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [Required(ErrorMessage = "Year is a required field.")]
        public int Year { get; set; }

        [Display(Name = "Deviation of Age (+-)")]
        [Required(ErrorMessage = "Deviation of Age is a required field.")]
        public List<DeviationOfAge> AgeDeviation
        {
            get
            {
                return _deviationIitems;
            }
            set
            {
                _deviationIitems = value;

            }
        }

        [Display(Name = "Voucher Code")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string VoucherCode { get; set; }

        [Display(Name = "Your Reference")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(ReferenceChars.Pattern, ErrorMessage = ReferenceChars.Message)]
        public string YourReference { get; set; }

        public int ProductID { get; set; }

        public string TypeOfSearchValue { get; set; }

        public int AgeDeviationValue { get; set; }
    }

    public class TypeOfSearch
    {
        string _text;
        string _value;

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }

    public class DeviationOfAge
    {
        string _text;
        string _value;

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }
}
