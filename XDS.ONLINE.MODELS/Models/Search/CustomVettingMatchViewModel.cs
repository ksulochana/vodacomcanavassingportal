﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Xml.Serialization;

using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;

namespace XDS.ONLINE.MODELS
{
    public class CustomVettingMatchViewModel
    {
        private List<Dealer> _dealerList = new List<Dealer>();
        private List<PromotionDeal> _promotionDealList = new List<PromotionDeal>();
        private List<PromoCode> _promoCodeList = new List<PromoCode>();

        public string TransactionDetails { get; set; }

        public CustomVettingMatchViewModel()
        {
        }

        public CustomVettingMatchViewModel(string transactionDetails)
        {
            try
            {
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                TransactionDetails = transactionDetails;
                doc.LoadXml(TransactionDetails);
                System.Xml.XmlNodeList nodeList = doc.SelectNodes("//TransactionDetails/Dealer");
                System.Xml.XmlNodeList nodeList2 = doc.SelectNodes("//TransactionDetails/PromotionDeal");
                System.Xml.XmlNodeList nodeList3 = doc.SelectNodes("//TransactionDetails/PromoCode");

                foreach (System.Xml.XmlNode node in nodeList)
                    _dealerList.Add(new Dealer { DealerID = node["DealerID"].InnerText, DealerName = node["DealerName"].InnerText });

                foreach (System.Xml.XmlNode node2 in nodeList2)
                    _promotionDealList.Add(new PromotionDeal { PromotionDealID = node2["PromotionDealID"].InnerText, Description = node2["Description"].InnerText });

                foreach (System.Xml.XmlNode node3 in nodeList3)
                    _promoCodeList.Add(new PromoCode { PromoCodeID = node3["PromoCodeID"].InnerText, Description = node3["Description"].InnerText });

                
            }
            catch (Exception ex) { }
        }


        [Required]
        public List<Dealer> DealerList
        {
            get
            {
                return _dealerList;
            }
            set
            {
                _dealerList = value;

            }
        }

        [Required]
        public List<PromotionDeal> PromotionDealList
        {
            get
            {
                return _promotionDealList;
            }
            set
            {
                _promotionDealList = value;

            }
        }

        [Required]
        public List<PromoCode> PromoCodeList
        {
            get
            {
                return _promoCodeList;
            }
            set
            {
                _promoCodeList = value;

            }
        }

        [Required]
        [Display(Name = "ID Number")]
        [StringLength(13, ErrorMessage = "The ID Number entered is invalid. Please enter a valid 13 digit ID Number. ", MinimumLength = 13)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [HomeAffairsIDNoValidation(ErrorMessage = "Invalid ID Number")]
        [IDNoIsMinorValidation(ErrorMessage = "ID Number belongs to a minor")]
        public string IdNumber { get; set; }

        [Display(Name = "First Name")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        public string FirstName { get; set; }

        [Display(Name = "Surname")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [Required]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        public string Surname { get; set; }

        [Display(Name = "Date of Birth")]
        [RegularExpression(DateDMY.Pattern, ErrorMessage = "The date of birth supplied is not in the correct format. Please enter the date in the format dd/mm/yyyy")]
        public string DateOfBirth { get; set; }

        [Display(Name = "Gross Income")]
        [Required]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        public string Income { get; set; }

        [Display(Name = "Mobile Number")]
        [RequiredIf("IsNullOrWhiteSpace(MobileNo) && IsNullOrWhiteSpace(AlternateMobileNo)", ErrorMessage = "Either Mobile Number or Alternate Contact Number is required.")]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        public string MobileNo { get; set; }

        [Display(Name = "Alternate Contact Number")]
        [RequiredIf("IsNullOrWhiteSpace(MobileNo) && IsNullOrWhiteSpace(AlternateMobileNo)", ErrorMessage = "Either Mobile Number or Alternate Contact Number is required.")]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        public string AlternateMobileNo { get; set; }

        [Display(Name = "Email Address")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Email Address is invalid")]
        [RegularExpression(EmailAddress.Pattern, ErrorMessage = EmailAddress.Message)]
        public string Email { get; set; }

        [Display(Name = "I declare that I have received customer permission")]
        [Range(typeof(bool), "true", "true", ErrorMessage = "Declaration consent is required")]
        public bool Consent { get; set; }

        public int ProductID { get; set; }

        [Required]
        [Display(Name = "Dealer")]
        public string DealerValue { get; set; }

        [Required]
        [Display(Name = "Promotion Deal")]
        public string PromotionValue { get; set; }

        [Required]
        [Display(Name = "Promo Code")]
        public string PromoCodeValue { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string City { get; set; }

    }

    public class Dealer
    {

        string _dealerID;
        string _dealerName;

        public string DealerID
        {
            get { return _dealerID; }
            set { _dealerID = value; }
        }

        public string DealerName
        {
            get { return _dealerName; }
            set { _dealerName = value; }
        }
    }

    public class PromotionDeal
    {

        string _promotionDealID;
        string _description;

        public string PromotionDealID
        {
            get { return _promotionDealID; }
            set { _promotionDealID = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
    }

    public class PromoCode
    {

        string _promoCodeID;
        string _description;

        public string PromoCodeID
        {
            get { return _promoCodeID; }
            set { _promoCodeID = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
    }

    [System.Xml.Serialization.XmlRoot("Result")]
    public class SMSResult
    {
        ulong _referenceNo;
        string _message;

        public ulong ReferenceNo
        {
            get { return _referenceNo; }
            set { _referenceNo = value; }
        }

        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class CustomVetting
    {
        [System.Xml.Serialization.XmlElementAttribute("CustomVettingResult")]
        public CustomVettingResult[] CustomVettingResult { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CustomVettingResult
    {
        private UInt64 enquiryIDField;
        private uint consumerIDField;
        private string firstNameField;
        private string surnameField;
        private string iDNoField;
        private string birthDateField;
        private string capturedByField;
        private string incomeField;
        private string alternateContactNoField;
        private string cellularNoField;
        private string emailAddressField;
        private string referenceNoField;
        private string applicationStatusField;
        private string portfolioField;
        private string dealerIDField;
        private string promoCodeField;
        private string createdOnDateField; 
        private string applicationDealerID;
        private string score;
        private string predictedIncome;
        private string predictedAvailableInstalment;
        private string reasonDescription;
        private string applicationPromotionDeal;
        private string reasonCode;
        private string accountNo;
        private string sourceInd;
        private string vodacomResponse;
        private string vodacomResponseType;
        private string emailStatus;
        private string regionField;

        /// <remarks/>
        public UInt64 EnquiryID
        {
            get
            {
                return this.enquiryIDField;
            }
            set
            {
                this.enquiryIDField = value;
            }
        }


        /// <remarks/>
        public uint ConsumerID
        {
            get
            {
                return this.consumerIDField;
            }
            set
            {
                this.consumerIDField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string IDNo
        {
            get
            {
                return this.iDNoField;
            }
            set
            {
                this.iDNoField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        //public System.DateTime? BirthDate
        public string BirthDate
        {
            get
            {
                return this.birthDateField;

                //return BirthDate.HasValue ? BirthDate.Value : new DateTime(9999, 12, 31);
                //return string.IsNullOrEmpty(BirthDate.Value.ToString()) ? new DateTime(9999, 12, 31) : BirthDate.Value;
                //return BirthDate.HasValue ? BirthDate.Value : null;

            }
            set
            {
                this.birthDateField = value;
                //this.birthDateField = string.IsNullOrEmpty(BirthDate.Value.ToString()) ? new DateTime(9999, 12, 31) : value;
            }
        }


        /// <remarks/> 
        public string CellularNo
        {
            get
            {
                return this.cellularNoField;
            }
            set
            {
                this.cellularNoField = value;
            }
        }

        /// <remarks/>
        public string EmailAddress
        {
            get
            {
                return this.emailAddressField;
            }
            set
            {
                this.emailAddressField = value;
            }
        }

        /// <remarks/>
        public string CapturedBy
        {
            get
            {
                return this.capturedByField;
            }
            set
            {
                this.capturedByField = value;
            }
        }

        /// <remarks/>
        public string Income
        {
            get
            {
                return this.incomeField;
            }
            set
            {
                this.incomeField = value;
            }
        }

        /// <remarks/>
        public string AlternateContactNo
        {
            get
            {
                return this.alternateContactNoField;
            }
            set
            {
                this.alternateContactNoField = value;
            }
        }

        /// <remarks/>
        public string ApplicationStatus
        {
            get
            {
                return this.applicationStatusField;
            }
            set
            {
                this.applicationStatusField = value;
            }
        }

        public string Portfolio
        {
            get
            {
                return this.portfolioField; ;
            }
            set
            {
                this.portfolioField = value;
            }
        }

        public string DealerID
        {
            get
            {
                return this.dealerIDField;
            }
            set
            {
                this.dealerIDField = value;
            }
        }

        public string PromoCode
        {
            get
            {
                return this.promoCodeField;
            }
            set
            {
                this.promoCodeField = value;
            }
        }

        public string ReferenceNo
        {
            get
            {
                return this.referenceNoField;
            }
            set
            {
                this.referenceNoField = value;
            }
        }

        public string CreatedOnDate
        {
            get
            {
                return createdOnDateField;
            }

            set
            {
                createdOnDateField = value;
            }
        }

        public string ApplicationDealerID
        {
            get
            {
                return applicationDealerID;
            }

            set
            {
                applicationDealerID = value;
            }
        }

        public string Score
        {
            get
            {
                return score;
            }

            set
            {
                score = value;
            }
        }

        public string PredictedIncome
        {
            get
            {
                return predictedIncome;
            }

            set
            {
                predictedIncome = value;
            }
        }

        public string PredictedAvailableInstalment
        {
            get
            {
                return predictedAvailableInstalment;
            }

            set
            {
                predictedAvailableInstalment = value;
            }
        }

        public string ReasonDescription
        {
            get
            {
                return reasonDescription;
            }

            set
            {
                reasonDescription = value;
            }
        }

        public string ApplicationPromotionDeal
        {
            get
            {
                return applicationPromotionDeal;
            }

            set
            {
                applicationPromotionDeal = value;
            }
        }

        public string ReasonCode
        {
            get
            {
                return reasonCode;
            }

            set
            {
                reasonCode = value;
            }
        }

        public string AccountNo
        {
            get
            {
                return accountNo;
            }

            set
            {
                accountNo = value;
            }
        }

        public string SourceInd
        {
            get
            {
                return sourceInd;
            }

            set
            {
                sourceInd = value;
            }
        }

        public string VodacomResponse
        {
            get
            {
                return vodacomResponse;
            }

            set
            {
                vodacomResponse = value;
            }
        }

        public string VodacomResponseType
        {
            get
            {
                return vodacomResponseType;
            }

            set
            {
                vodacomResponseType = value;
            }
        }
        public string EmailStatus
        {
            get
            {
                return emailStatus;
            }

            set
            {
                emailStatus = value;
            }
        }
        /// <remarks/>
        public string Region
        {
            get
            {
                return this.regionField;
            }
            set
            {
                this.regionField = value;
            }
        }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class HomeAffairsIDNoValidationAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            /*
                ID Number 800101 5009 087 as an example:

                •Add all the digits in the odd positions (excluding last digit).
                   8 + 0 + 0 + 5 + 0 + 0 = 13...................[1] 
                •Move the even positions into a field and multiply the number by 2.
                   011098 x 2 = 22196 
                •Add the digits of the result in b).
                   2 + 2 + 1 + 9 + 6 = 20.........................[2] 
                •Add the answer in [2] to the answer in [1].
                   13 + 20 = 33 
                •Subtract the second digit (i.e. 3) from 10.  The number must tally with the last number in the ID Number. 
                If the result is 2 digits, the last digit is used to compare against the last number in the ID Number.  If the answer differs, the ID number is invalid.

            */

            if (value == null || value.ToString().Length != 13)
                return false;

            try
            {
                string iD = value.ToString();
                string iDExcludingLastChar = iD.Substring(0, iD.Length - 1);

                int evenSum = 0, oddSum = 0, finalSum = 0, secondDigit = 0, lastDigit = 0, finalDigit = 0;
                string evenChars = string.Empty, oddChars = string.Empty;

                for (int i = 1; i <= iDExcludingLastChar.Length; i++)
                {
                    string strValue = string.Empty;

                    if (i == iDExcludingLastChar.Length)
                        strValue = iDExcludingLastChar.Substring(iDExcludingLastChar.Length - 1, 1);
                    else
                        strValue = iDExcludingLastChar.Substring(i - 1, 1);

                    if (i % 2 != 0)
                        oddChars += strValue;
                    else
                        evenChars += strValue;
                }

                oddSum = AddIntegers(int.Parse(oddChars));
                evenSum = AddIntegers(int.Parse(evenChars) * 2);

                finalSum = oddSum + evenSum;

                if (finalSum.ToString().Length > 1)
                    secondDigit = int.Parse(finalSum.ToString().Substring(1, 1));

                lastDigit = int.Parse(iD.Substring(iD.Length - 1, 1));

                finalDigit = 10 - secondDigit;

                if (finalDigit.ToString().Length > 1)
                    finalDigit = int.Parse(finalDigit.ToString().Substring(1, 1));

                return finalDigit == lastDigit;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public int AddIntegers(int num)
        {
            int sum = 0, r;

            while (num != 0)
            {
                r = num % 10;
                num = num / 10;
                sum = sum + r;
            }

            return sum;
        }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)] 
    public class IDNoIsMinorValidationAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            bool result = false;

            try
            {
                string iD = value.ToString();

                HomeAffairsIDNoValidationAttribute idValidate = new HomeAffairsIDNoValidationAttribute();

                if (idValidate.IsValid(iD))
                {
                    if (iD.Length > 5)
                    {
                        DateTime dobIDNo = new DateTime();

                        DateTime.TryParseExact("19" + iD.Substring(0, 6), "yyyyMMdd",
                            System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dobIDNo);

                        if ((DateTime.Now.Year - dobIDNo.Year) < 18)
                            result = true;
                    }
                }
                else
                {
                    if (iD.Length > 6)
                    {
                        if (IsDate(iD.Substring(1, 6)))
                        {
                            DateTime dobIDNo;

                            DateTime.TryParseExact("19" + iD.Substring(1, 6), "yyyyMMdd",
                                System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dobIDNo);

                            if ((DateTime.Now.Year - dobIDNo.Year) < 18)
                                result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
            }

            return !result;
        }

        public bool IsDate(string input)
        {
            bool result = false;
            DateTime dtParse = new DateTime();
            if (DateTime.TryParseExact(input, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtParse))
            {
                if (dtParse.Year >= 1753) { result = true; } else { result = false; }
            }
            return result;
        }
    } 
}
