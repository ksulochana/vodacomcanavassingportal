﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;

namespace XDS.ONLINE.MODELS
{
    public class AuthenticationEnquiryMatchViewModel
    {
        private List<ListOfOTPReasons> _otpReasonList = new List<ListOfOTPReasons>();

        public AuthenticationEnquiryMatchViewModel()
        {
            _otpReasonList.Add(new ListOfOTPReasons { Text = "", Value = "" });
            _otpReasonList.Add(new ListOfOTPReasons { Text = "Customer does not have a cellular", Value = "Customer does not have a cellular" });
            _otpReasonList.Add(new ListOfOTPReasons { Text = "Customer only has a landline as a contact number", Value = "Customer only has a landline as a contact number" });
            _otpReasonList.Add(new ListOfOTPReasons { Text = "Customer does not have any contact numbers", Value = "Customer does not have any contact numbers" });
            _otpReasonList.Add(new ListOfOTPReasons { Text = "Other", Value = "Other" });
        }

        
        [Display(Name = "Branch")]
        public ProfileBranches[] EnquiryBranch { get; set; }

        [Display(Name = "Purpose")]
        public ProfilePurposes[] EnquiryPurpose { get; set; }

        [Display(Name = "ID Number")]
        [StringLength(13, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 13)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [RequiredIf("IsNullOrWhiteSpace(Passport)", ErrorMessage = "Your search input must either be an ID Number or a Passport Number.")]
        public string IdNumber { get; set; }

        [Display(Name = "Passport Number/Other ID")]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        [RequiredIf("IsNullOrWhiteSpace(IdNumber)", ErrorMessage = "Your search input must either be an ID Number or a Passport Number.")]
        public string Passport { get; set; }

        [Display(Name = "Cellular Code")]
        [StringLength(3, ErrorMessage = "The {0} must be at least {2} characters long.")]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        public string CellularCode { get; set; }

        [Display(Name = "Cellular No.")]
        [StringLength(7, ErrorMessage = "The {0} must be at least {2} characters long.")]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        //[Required(ErrorMessage = "Contact no is mandatory, please supply a valid value.")]
        public string CellularNo { get; set; }

        [Display(Name = "Cellular No.")]
        [StringLength(10, ErrorMessage = "The {0} must be at least {2} characters long.")]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        //[Required(ErrorMessage = "Contact no is mandatory, please supply a valid value.")]
        public string TelephoneNo { get; set; }

        [Display(Name = "Email Address")]
        [RegularExpression(Shandon.Utils.RegExPatterns.EmailAddress.Pattern, ErrorMessage = Shandon.Utils.RegExPatterns.EmailAddress.Message)]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string EmailAddress { get; set; }

        [Display(Name = "Override OTP")]
        public bool OverrideOTP { get; set; }

        [Display(Name = "Reason")]
        public List<ListOfOTPReasons> OverrideOTPReason
        {
            get
            {
                return _otpReasonList;
            }
            set
            {
                _otpReasonList = value;

            }
        }

        [Display(Name = "Voucher Code")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string VoucherCode { get; set; }

        [Display(Name = "Your Reference")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(ReferenceChars.Pattern, ErrorMessage = ReferenceChars.Message)]
        public string YourReference { get; set; }

        [Required(ErrorMessage = "Enquiry Branch is a required field.")]
        public string EnquiryBranchValue { get; set; }

        [Required(ErrorMessage = "Enquiry Purpose is a required field.")]
        public string EnquiryPurposeValue { get; set; }

        [RequiredIf("UserOverrideOTP == true && IsNullOrWhiteSpace(OverrideOTPReasonValue)", ErrorMessage = "Override OTP Reason is a required field.")]
        public string OverrideOTPReasonValue { get; set; }

        public XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.AuthenticationSearchType SearchType { get; set; }

        public string AccountNo { get; set; }

        public string SubAccountNo { get; set; }

        [Display(Name = "Override OTP")]
        public bool UserOverrideOTP { get; set; }

        [Display(Name = "Comment")]
        [RequiredIf("UserOverrideOTP == true && OverrideOTPReasonValue == 'Other'", ErrorMessage = "Comment is a required field.")]
        public string OverrideOTPComment { get; set; }

        //public bool ReferToFraud { get; set; }
        //public bool Splitscreen { get; set; }
        //public bool PersonalQuestionsCheck { get; set; }

    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class Profile
    {

        private ProfileBranches[] branchesField;

        private ProfileSubscriberProfile subscriberProfileField;

        private SubscriberProfileSettings subscriberProfileSettingsField;

        private ProfilePurposes[] purposesField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Branches")]
        public ProfileBranches[] Branches
        {
            get
            {
                return this.branchesField;
            }
            set
            {
                this.branchesField = value;
            }
        }

        /// <remarks/>
        public ProfileSubscriberProfile SubscriberProfile
        {
            get
            {
                return this.subscriberProfileField;
            }
            set
            {
                this.subscriberProfileField = value;
            }
        }

        public SubscriberProfileSettings SubscriberProfileSettings
        {
            get
            {
                return this.subscriberProfileSettingsField;
            }
            set
            {
                this.subscriberProfileSettingsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Purposes")]
        public ProfilePurposes[] Purposes
        {
            get
            {
                return this.purposesField;
            }
            set
            {
                this.purposesField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ProfileBranches
    {

        private string subscriberIDField;

        private string branchCodeField;

        private string branchNameField;

        /// <remarks/>
        public string SubscriberID
        {
            get
            {
                return this.subscriberIDField;
            }
            set
            {
                this.subscriberIDField = value;
            }
        }

        /// <remarks/>
        public string BranchCode
        {
            get
            {
                return this.branchCodeField;
            }
            set
            {
                this.branchCodeField = value;
            }
        }

        /// <remarks/>
        public string BranchName
        {
            get
            {
                return this.branchNameField;
            }
            set
            {
                this.branchNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ProfileSubscriberProfile
    {

        private string subscriberIDField;

        private bool authenticationSearchOnIDNoYNField;

        private bool authenticationSearchOnCellPhoneNoYNField;

        private bool authenticationSearchOnAccountNoYNField;

        /// <remarks/>
        public string SubscriberID
        {
            get
            {
                return this.subscriberIDField;
            }
            set
            {
                this.subscriberIDField = value;
            }
        }

        /// <remarks/>
        public bool AuthenticationSearchOnIDNoYN
        {
            get
            {
                return this.authenticationSearchOnIDNoYNField;
            }
            set
            {
                this.authenticationSearchOnIDNoYNField = value;
            }
        }

        /// <remarks/>
        public bool AuthenticationSearchOnCellPhoneNoYN
        {
            get
            {
                return this.authenticationSearchOnCellPhoneNoYNField;
            }
            set
            {
                this.authenticationSearchOnCellPhoneNoYNField = value;
            }
        }

        /// <remarks/>
        public bool AuthenticationSearchOnAccountNoYN
        {
            get
            {
                return this.authenticationSearchOnAccountNoYNField;
            }
            set
            {
                this.authenticationSearchOnAccountNoYNField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class SubscriberProfileSettings
    {

        private string profileIDField;

        private string subscriberIDField;

        private bool blockUserField;

        private string totalNoofQuestionsField;

        private bool personalQuestionsCheckField;

        private bool unblockAccessField;

        private bool referToFraudField;

        private bool splitscreenField;

        private string questionTimeOutField;

        private bool useVoidDefaultReasonsField;

        private bool useFraudDefaultReasonsField;

        private bool cellNoMandatoryField;

        private string statusIndField;

        private string createdbyUserField;

        private string createdonDateField;

        private string changedByUserField;

        private string changedonDateField;

        private bool excludeInputcellNoField;

        private bool excludeLatestDBcellNoField;

        private bool overrideOTPField;

        private string blockedMessageField;

        private bool excludeEmailAddressField;

        private bool emailMandatoryField;

        private int authVersionField;

        private bool useDefaultUnblockReasonsField;

        private bool fraudScoreField;

        private bool accountVerificationField;

        private bool iDPhotoField;

        private bool creditReportField;

        private bool resendOTPField;

        private int noOfOTPRetriesField;
        /// <remarks/>
        public string ProfileID
        {
            get
            {
                return this.profileIDField;
            }
            set
            {
                this.profileIDField = value;
            }
        }

        /// <remarks/>
        public string SubscriberID
        {
            get
            {
                return this.subscriberIDField;
            }
            set
            {
                this.subscriberIDField = value;
            }
        }

        /// <remarks/>
        public bool BlockUser
        {
            get
            {
                return this.blockUserField;
            }
            set
            {
                this.blockUserField = value;
            }
        }

        /// <remarks/>
        public string TotalNoofQuestions
        {
            get
            {
                return this.totalNoofQuestionsField;
            }
            set
            {
                this.totalNoofQuestionsField = value;
            }
        }

        /// <remarks/>
        public bool PersonalQuestionsCheck
        {
            get
            {
                return this.personalQuestionsCheckField;
            }
            set
            {
                this.personalQuestionsCheckField = value;
            }
        }

        /// <remarks/>
        public bool UnblockAccess
        {
            get
            {
                return this.unblockAccessField;
            }
            set
            {
                this.unblockAccessField = value;
            }
        }

        /// <remarks/>
        public bool ReferToFraud
        {
            get
            {
                return this.referToFraudField;
            }
            set
            {
                this.referToFraudField = value;
            }
        }

        /// <remarks/>
        public bool SplitScreen
        {
            get
            {
                return this.splitscreenField;
            }
            set
            {
                this.splitscreenField = value;
            }
        }

        /// <remarks/>
        public string QuestionTimeOut
        {
            get
            {
                return this.questionTimeOutField;
            }
            set
            {
                this.questionTimeOutField = value;
            }
        }

        /// <remarks/>
        public bool UseVoidDefaultReasons
        {
            get
            {
                return this.useVoidDefaultReasonsField;
            }
            set
            {
                this.useVoidDefaultReasonsField = value;
            }
        }

        /// <remarks/>
        public bool UseFraudDefaultReasons
        {
            get
            {
                return this.useFraudDefaultReasonsField;
            }
            set
            {
                this.useFraudDefaultReasonsField = value;
            }
        }

        /// <remarks/>
        public bool CellNoMandatory
        {
            get
            {
                return this.cellNoMandatoryField;
            }
            set
            {
                this.cellNoMandatoryField = value;
            }
        }

        /// <remarks/>
        public string StatusInd
        {
            get
            {
                return this.statusIndField;
            }
            set
            {
                this.statusIndField = value;
            }
        }

        /// <remarks/>
        public string CreatedbyUser
        {
            get
            {
                return this.createdbyUserField;
            }
            set
            {
                this.createdbyUserField = value;
            }
        }

        /// <remarks/>
        public string CreatedonDate
        {
            get
            {
                return this.createdonDateField;
            }
            set
            {
                this.createdonDateField = value;
            }
        }

        /// <remarks/>
        public string ChangedByUser
        {
            get
            {
                return this.changedByUserField;
            }
            set
            {
                this.changedByUserField = value;
            }
        }

        /// <remarks/>
        public string ChangedonDate
        {
            get
            {
                return this.changedonDateField;
            }
            set
            {
                this.changedonDateField = value;
            }
        }

        /// <remarks/>
        public bool ExcludeInputcellNo
        {
            get
            {
                return this.excludeInputcellNoField;
            }
            set
            {
                this.excludeInputcellNoField = value;
            }
        }

        /// <remarks/>
        public bool ExcludeLatestDBcellNo
        {
            get
            {
                return this.excludeLatestDBcellNoField;
            }
            set
            {
                this.excludeLatestDBcellNoField = value;
            }
        }

        /// <remarks/>
        public bool OverrideOTP
        {
            get
            {
                return this.overrideOTPField;
            }
            set
            {
                this.overrideOTPField = value;
            }
        }

        /// <remarks/>
        public string BlockedMessage
        {
            get
            {
                return this.blockedMessageField;
            }
            set
            {
                this.blockedMessageField = value;
            }
        }

        /// <remarks/>
        public bool ExcludeEmailAddress
        {
            get
            {
                return this.excludeEmailAddressField;
            }
            set
            {
                this.excludeEmailAddressField = value;
            }
        }

        /// <remarks/>
        public bool EmailMandatory
        {
            get
            {
                return this.emailMandatoryField;
            }
            set
            {
                this.emailMandatoryField = value;
            }
        }


        //Authentication V2.2
        public int AuthVersion
        {
            get
            {
                return this.authVersionField;
            }
            set
            {
                this.authVersionField = value;
            }
        }

        public bool UseDefaultUnblockReasons
        {
            get
            {
                return this.useDefaultUnblockReasonsField;
            }
            set
            {
                this.useDefaultUnblockReasonsField = value;
            }
        }

        public bool FraudScore
        {
            get
            {
                return this.fraudScoreField;
            }
            set
            {
                this.fraudScoreField = value;
            }
        }

        public bool AccountVerification
        {
            get
            {
                return this.accountVerificationField;
            }
            set
            {
                this.accountVerificationField = value;
            }
        }

        public bool IDPhoto
        {
            get
            {
                return this.iDPhotoField;
            }
            set
            {
                this.iDPhotoField = value;
            }
        }

        public bool CreditReport
        {
            get
            {
                return this.creditReportField;
            }
            set
            {
                this.creditReportField = value;
            }
        }

        public bool ResendOTP
        {
            get
            {
                return this.resendOTPField;
            }
            set
            {
                this.resendOTPField = value;
            }
        }

        public int NoOfOTPRetries
        {
            get
            {
                return this.noOfOTPRetriesField;
            }
            set
            {
                this.noOfOTPRetriesField = value;
            }
        }
    }



    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ProfilePurposes
    {

        private string subscriberIDField;

        private string purposeIDField;

        private string purposeField;

        /// <remarks/>
        public string SubscriberID
        {
            get
            {
                return this.subscriberIDField;
            }
            set
            {
                this.subscriberIDField = value;
            }
        }

        /// <remarks/>
        public string PurposeID
        {
            get
            {
                return this.purposeIDField;
            }
            set
            {
                this.purposeIDField = value;
            }
        }

        /// <remarks/>
        public string Purpose
        {
            get
            {
                return this.purposeField;
            }
            set
            {
                this.purposeField = value;
            }
        }
    }


    public partial class ListOfOTPReasons
    {
        string _text;
        string _value;

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }

}
