﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;

namespace XDS.ONLINE.MODELS
{
    //public class ConnectGetResultModel
    //{
    //    public string ConnectTicket { get; set; }
    //    public int EnquiryID { get; set; }
    //    public int EnquiryResultID { get; set; }
    //    public int ProductID { get; set; }
    //    public string BonusXML { get; set; }
    //}

    public class TelephoneMatchViewModel
    {

        public List<SearchOn> _items = new List<SearchOn>();

        public TelephoneMatchViewModel()
        {
            _items.Add(new SearchOn { Text = "Landline", Value = "Landline" });
            _items.Add(new SearchOn { Text = "Cellphone", Value = "Cellphone" });
        }

        [Display(Name = "Search On")]
        public List<SearchOn> SearchOn 
        {
            get 
            {
                return _items; 
            }
            set 
            {
                _items = value;

            }
        }

        [Display(Name = "Code")]
        [StringLength(3, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 3)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        public string Code { get; set; }

        [Display(Name = "Number")]
        [StringLength(7, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 7)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [Required(ErrorMessage = "Telephone Number is a required field.")]
        public string Number { get; set; }

        [Display(Name = "Surname")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        public string Surname { get; set; }

        [Display(Name = "Voucher Code")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string VoucherCode { get; set; }

        [Display(Name = "Your Reference")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(ReferenceChars.Pattern, ErrorMessage = ReferenceChars.Message)]
        public string YourReference { get; set; }

        public string SearchOnValue { get; set; }

    }

    public class SearchOn
    {
        string _text;
        string _value;

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }
}
