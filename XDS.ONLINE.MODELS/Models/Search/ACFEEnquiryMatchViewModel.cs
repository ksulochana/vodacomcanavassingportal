﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;

namespace XDS.ONLINE.MODELS
{
    public class ACFEEnquiryMatchViewModel
    {

        [Display(Name = "ID Number")]
        [StringLength(13, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 13)]
        [RequiredIf("IsNullOrWhiteSpace(FirstName) && IsNullOrWhiteSpace(Surname) && IsNullOrWhiteSpace(DateOfBirth) && IsNullOrWhiteSpace(Passport)", ErrorMessage = "Either search by ID Number or Passport Number.")]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        public string IdNumber { get; set; }

        [Display(Name = "Passport Number/Other ID")]
        [RequiredIf("(IsNullOrWhiteSpace(FirstName) && IsNullOrWhiteSpace(Surname) && IsNullOrWhiteSpace(DateOfBirth) && IsNullOrWhiteSpace(IdNumber))", ErrorMessage = "Either search by ID Number or Passport Number.")]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string Passport { get; set; }

        
        [Display(Name = "Surname")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RequiredIf("(IsNullOrWhiteSpace(IdNumber) && IsNullOrWhiteSpace(Passport))", ErrorMessage = "Either search by ID/Passport or Name, Surname and Date of Birth.")]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        public string Surname { get; set; }

        [Display(Name = "First Name")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RequiredIf("(IsNullOrWhiteSpace(IdNumber) && IsNullOrWhiteSpace(Passport))", ErrorMessage = "Either search by ID/Passport or Name, Surname and Date of Birth.")]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        public string FirstName { get; set; }


        [Display(Name = "Date of Birth")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [RequiredIf("(IsNullOrWhiteSpace(IdNumber) && IsNullOrWhiteSpace(Passport))", ErrorMessage = "Either search by ID/Passport or Name, Surname and Date of Birth.")]
        public string DateOfBirth { get; set; }

        
        [Display(Name = "Voucher Code")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string VoucherCode { get; set; }

        [Display(Name = "Your Reference")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(ReferenceChars.Pattern, ErrorMessage = ReferenceChars.Message)]
        public string YourReference { get; set; }

        public int ProductID { get; set; }


        

    }
}
