﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;

namespace XDS.ONLINE.MODELS
{
    public class BankingAccountVerificationMatchViewModel
    {
        public List<TypeOfVerification> _typeOfVerificationItems = new List<TypeOfVerification>();
        public List<BankingEntityType> _bankingEntityTypeItems = new List<BankingEntityType>();

        public BankingAccountVerificationMatchViewModel()
        {
            _typeOfVerificationItems.Add(new TypeOfVerification { Text = "", Value = "" });
            _typeOfVerificationItems.Add(new TypeOfVerification { Text = "Individual", Value = "Individual" });
            _typeOfVerificationItems.Add(new TypeOfVerification { Text = "Company", Value = "Company" });


            _bankingEntityTypeItems.Add(new BankingEntityType { Text = "", Value = "" });
            _bankingEntityTypeItems.Add(new BankingEntityType { Text = "Registered Company", Value = "Registered Company" });
            _bankingEntityTypeItems.Add(new BankingEntityType { Text = "Sole Proprieter", Value = "Sole Proprieter" });
            _bankingEntityTypeItems.Add(new BankingEntityType { Text = "Trust", Value = "Trust" });
        }

        [Display(Name = "Type of Verification:")]
        public List<TypeOfVerification> TypeOfVerification
        {
            get
            {
                return _typeOfVerificationItems;
            }
            set
            {
                _typeOfVerificationItems = value;
            }
        }

        [Display(Name = "Type of Entity:")]
        public List<BankingEntityType> EntityType 
        {
            get
            {
                return _bankingEntityTypeItems;
            }
            set
            {
                _bankingEntityTypeItems = value;
            }
        }  

        [Display(Name = "Initials")]
        [StringLength(5, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        [RequiredIf("TypeOfVerificationValue == 'Individual'", ErrorMessage="Initials Required")]
        public string Initials { get; set; }

        [Display(Name = "Surname")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 3)]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        [RequiredIf("TypeOfVerificationValue == 'Individual'", ErrorMessage="Surname Required")]
        public string RequestSurname { get; set; }

        [Display(Name = "ID NO/Other ID")]
        [StringLength(13, ErrorMessage = "ID No must be at least 13 characters long. Other ID must at least be 4 characters long", MinimumLength = 4)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        [RequiredIf("TypeOfVerificationValue == 'Individual' || (BankingEntityTypeValue == 'Sole Proprieter' && TypeOfVerificationValue == 'Company')", ErrorMessage = "ID Number is a required field.")]
        public string IdNumber { get; set; }

        //[RequiredIf("IsNullOrWhiteSpace(RegistrationNumber1) && IsNullOrWhiteSpace(RegistrationNumber2)  && IsNullOrWhiteSpace(RegistrationNumber3) && BankingEntityTypeValue == 'Registered Company' && TypeOfVerificationValue == 'Company'", ErrorMessage = "Either search by Business Name or Registration Number.")]
        [Display(Name = "Company Name")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 3)]
        [RegularExpression(NameOrganisation.Pattern, ErrorMessage = NameOrganisation.Message)]
        [RequiredIf("TypeOfVerificationValue == 'Company'", ErrorMessage = "Please enter Company Name")]
        public string CompanyName { get; set; }

        //[RequiredIf("RegistrationNumber2 != '' || RegistrationNumber3 != '' || BankingEntityTypeValue == 'Registered Company'", ErrorMessage = "Either search by Business Name or Registration Number.")]
        //[RequiredIf("!IsNullOrWhiteSpace(RegistrationNumber2) || !IsNullOrWhiteSpace(RegistrationNumber3) && BankingEntityTypeValue == 'Registered Company' && TypeOfVerificationValue == 'Company'", ErrorMessage = "Either search by Business Name or Registration Number.")]
        [Display(Name = "Registration Number")]
        [StringLength(4, ErrorMessage = "Please enter a valid 4 digit Number.", MinimumLength = 4)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [RequiredIf("BankingEntityTypeValue == 'Registered Company' && TypeOfVerificationValue == 'Company'", ErrorMessage = "Please enter Registration Number.")]
        public string RegistrationNumber1 { get; set; }

        //[RequiredIf("RegistrationNumber1 != '' || RegistrationNumber3 != '' || BankingEntityTypeValue == 'Registered Company' ", ErrorMessage = "Either search by Business Name or Registration Number.")]
        //[RequiredIf("!IsNullOrWhiteSpace(RegistrationNumber1) || !IsNullOrWhiteSpace(RegistrationNumber3) && BankingEntityTypeValue == 'Registered Company' && TypeOfVerificationValue == 'Company'", ErrorMessage = "Either search by Business Name or Registration Number.")]
        [StringLength(6, ErrorMessage = "Please enter a valid 6 digit Number.", MinimumLength = 6)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [RequiredIf("BankingEntityTypeValue == 'Registered Company' && TypeOfVerificationValue == 'Company'", ErrorMessage = " ")]
        public string RegistrationNumber2 { get; set; }

        //[RequiredIf("RegistrationNumber1 != '' || RegistrationNumber2 != '' || BankingEntityTypeValue == 'Registered Company'", ErrorMessage = "Either search by Business Name or Registration Number.")]
        //[RequiredIf("!IsNullOrWhiteSpace(RegistrationNumber1) || !IsNullOrWhiteSpace(RegistrationNumber2) && BankingEntityTypeValue == 'Registered Company' && TypeOfVerificationValue == 'Company'", ErrorMessage = "Either search by Business Name or Registration Number.")]
        [StringLength(2, ErrorMessage = "Please enter a valid 2 digit Number.", MinimumLength = 2)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [RequiredIf("BankingEntityTypeValue == 'Registered Company' && TypeOfVerificationValue == 'Company'", ErrorMessage = " ")]
        public string RegistrationNumber3 { get; set; }

        [Display(Name = "Trust Number")]
        [StringLength(30, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        [RequiredIf("BankingEntityTypeValue == 'Trust' && TypeOfVerificationValue == 'Company'", ErrorMessage = "Trust Number is a required field.")]
        public string TrustNumber { get; set; }

        [Display(Name = "Account Number")]
        [StringLength(25, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [Required(ErrorMessage = "Account Number is a required field.")]
        public string AccountNumber { get; set; }

        [Display(Name = "Branch Code")]
        [StringLength(6, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 4)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [Required(ErrorMessage = "Branch Code is a required field.")]
        public string BranchCode { get; set; }

        [Display(Name = "Account Type")]
        public ListofBankAccountTypes BankAccountType { get; set; }

        [Display(Name = "Bank Name")]
        public ListOfBanks BankName { get; set; }


        [Display(Name = "First Name")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        [Required(ErrorMessage = "First Name is a required field.")]
        public string FirstName { get; set; }

        [Display(Name = "Surname")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        [Required(ErrorMessage = "Surname is a required field.")]
        public string Surname { get; set; }

        [Display(Name = "Email Address")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(Shandon.Utils.RegExPatterns.EmailAddress.Pattern, ErrorMessage = Shandon.Utils.RegExPatterns.EmailAddress.Message)]
        [Required(ErrorMessage = "Email Address is a required field.")]
        public string EmailAddress { get; set; }


        [Display(Name = "Voucher Code")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string VoucherCode { get; set; }

        [Display(Name = "Your Reference")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(ReferenceChars.Pattern, ErrorMessage = ReferenceChars.Message)]
        public string YourReference { get; set; }




        [RequiredIf("IsNullOrWhiteSpace(TypeOfVerificationValue)", ErrorMessage = "Please select Type of Verification")]
        public string TypeOfVerificationValue { get; set; }

        [RequiredIf("TypeOfVerificationValue == 'Company'", ErrorMessage = "Please select type of Entity.")]
        public string BankingEntityTypeValue { get; set; }

        [Display(Name = "Account Type")]
        [Required]
        public string BankAccountTypeValue { get; set; }

        [Display(Name = "Bank Name")]
        [Required]
        public string BankNameValue { get; set; }
        //public string TermsValue { get; set; }

    }

    public class BankingEntityType
    {
        string _text;
        string _value;

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }

    public class TypeOfVerification
    {
        string _text;
        string _value;

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }

}
