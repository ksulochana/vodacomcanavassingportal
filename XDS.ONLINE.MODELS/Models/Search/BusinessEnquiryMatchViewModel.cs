﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;

namespace XDS.ONLINE.MODELS
{
    public class BusinessEnquiryMatchViewModel
    {
        public List<EntityType> _entityTypeIitems = new List<EntityType>();

        public BusinessEnquiryMatchViewModel()
        {
            _entityTypeIitems.Add(new EntityType { Text = "Registered Company", Value = "Company" });
            _entityTypeIitems.Add(new EntityType { Text = "Vat Number", Value = "Vat Number" });
            _entityTypeIitems.Add(new EntityType { Text = "Sole Proprietor", Value = "Sole Proprietor" });
        }

        [Display(Name = "Legal Entity Type")]
        public List<EntityType> EntityType
        {
            get
            {
                return _entityTypeIitems;
            }
            set
            {
                _entityTypeIitems = value;
            }
        }

        [Display(Name = "Registration Number")]
        [StringLength(5, ErrorMessage = " ", MinimumLength = 4)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        [RequiredIf("EntityTypeValue == 'Company' && IsNullOrWhiteSpace(BusinessName)", ErrorMessage = "Either search by Business Name or Registration Number.")]
        public string RegistrationNumber1 { get; set; }

        [StringLength(6, ErrorMessage = "Please enter a valid 6 digit ID Number.", MinimumLength = 6)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        public string RegistrationNumber2 { get; set; }

        [StringLength(2, ErrorMessage = "Please enter a valid 2 digit ID Number.", MinimumLength = 2)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        public string RegistrationNumber3 { get; set; }

        [Display(Name = "Business Name")]
        [StringLength(150, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        [RequiredIf("EntityTypeValue == 'Company' && (IsNullOrWhiteSpace(RegistrationNumber1) || IsNullOrWhiteSpace(RegistrationNumber2) || IsNullOrWhiteSpace(RegistrationNumber3))", ErrorMessage = "Either search by Business Name or Registration Number.")]
        public string BusinessName { get; set; }

        [Display(Name = "ID Number")]
        [RequiredIf("(EntityTypeValue == 'Sole Proprietor')", ErrorMessage = "Please enter an ID Number.")]
        [StringLength(13, ErrorMessage = "The ID Number entered is invalid. Please enter a valid 13 digit ID Number.", MinimumLength = 13)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        public string IdNumber { get; set; }

        [Display(Name = "VAT Number")]
        [StringLength(10, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 10)]
        [RequiredIf("(EntityTypeValue == 'Vat Number')", ErrorMessage = "Please enter a VAT Number.")]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        public string VATNumber { get; set; }

        [Display(Name = "Voucher Code")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string VoucherCode { get; set; }

        [Display(Name = "Your Reference")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(ReferenceChars.Pattern, ErrorMessage = ReferenceChars.Message)]
        public string YourReference { get; set; }

        public int ProductID { get; set; }

        public string EntityTypeValue { get; set; }
    }

    public class EntityType
    {
        string _text;
        string _value;

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }
}
