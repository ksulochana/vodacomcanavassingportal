﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Xml.Serialization;
using Shandon.Utils.RegExPatterns;

using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;

namespace XDS.ONLINE.MODELS
{
    public class BankCodeRequestMatchViewModel
    {
        public List<TypeOfVerification> _typeOfVerificationItems = new List<TypeOfVerification>();
        public List<BankingEntityType> _bankingEntityTypeItems = new List<BankingEntityType>();

        public BankCodeRequestMatchViewModel()
        {
            _typeOfVerificationItems.Add(new TypeOfVerification { Text = "Individual", Value = "Individual" });
            _typeOfVerificationItems.Add(new TypeOfVerification { Text = "Company", Value = "Company" });

            _bankingEntityTypeItems.Add(new BankingEntityType { Text = "Registered Company", Value = "Registered Company" });
            _bankingEntityTypeItems.Add(new BankingEntityType { Text = "Sole Proprieter", Value = "Sole Proprieter" });
            _bankingEntityTypeItems.Add(new BankingEntityType { Text = "Trust", Value = "Trust" });
        }

        [Display(Name="Bank Code Request Type")]
        public ListOfRequestTypes RequestTypes { get; set; }

        [Display(Name = "Bank Name")]
        public ListOfBanks BankNames { get; set; }
        
        [Display(Name = "Terms")]
        public ListOfTerms Terms { get; set; }

        //[Display(Name="Type of Verification")]
        //public XDS.ONLINE.REPOSITORY.XDSReportServiceRef.TypeofVerificationenum TypeOfVerification { get; set; }

        //[Display(Name="Entity Type")]
        //public XDS.ONLINE.REPOSITORY.XDSReportServiceRef.Entity EntityType { get; set; }

        [Display(Name = "Type of Verification:")]
        public List<TypeOfVerification> TypeOfVerification
        {
            get
            {
                return _typeOfVerificationItems;
            }
            set
            {
                _typeOfVerificationItems = value;
            }
        }

        [Display(Name = "Type of Entity:")]
        public List<BankingEntityType> EntityType
        {
            get
            {
                return _bankingEntityTypeItems;
            }
            set
            {
                _bankingEntityTypeItems = value;
            }
        }  


        //NOTE: These 2 Values seem to be passed into the ConnectBankCodesRequestAsync, but are not on the form??
        public XDS.ONLINE.REPOSITORY.XDSReportServiceRef.Region BankCodeRegion { get; set; }
        public XDS.ONLINE.REPOSITORY.XDSReportServiceRef.CountryCode BankCodeCountryCode { get; set; }

        public bool SendEmail { get; set; }

        [Display(Name = "ID Number")]        
        [StringLength(13, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RequiredIf("TypeOfVerificationValue == 'Individual' || BankingEntityTypeValue == 'Sole Proprieter'", ErrorMessage = "Please enter your ID Number")]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        public string IDNumber { get; set; }

        //The Reg1 - 3 is Registration number but on the form it is 3 smaller textboxes which make up the whole number
        //[RequiredIf("(Reg2 != '' || Reg3 != '') && (BankingEntityTypeValue == 'Registered Company' && TypeOfVerificationValue == 'Company')", ErrorMessage = "Required for Registration Number.")]
        //[RequiredIf("!IsNullOrWhiteSpace(Reg2) || !IsNullOrWhiteSpace(Reg3) && BankingEntityTypeValue == 'Registered Company' && TypeOfVerificationValue == 'Company'", ErrorMessage = "Either search by Business Name or Registration Number.")]
        [Display(Name = "Registration Number")]
        [StringLength(4, ErrorMessage = "Please enter a valid 4 digit Number.", MinimumLength = 4)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [RequiredIf("BankingEntityTypeValue == 'Registered Company' && TypeOfVerificationValue == 'Company'", ErrorMessage = "Registration Number is required.")]
        public string Reg1 { get; set; }

        //[RequiredIf("Reg1 != '' || Reg3 != ''", ErrorMessage = "Required for Registration Number.")]
        //[RequiredIf("!IsNullOrWhiteSpace(Reg1) || !IsNullOrWhiteSpace(Reg3) && BankingEntityTypeValue == 'Registered Company' && TypeOfVerificationValue == 'Company'", ErrorMessage = "Either search by Business Name or Registration Number.")]
        [StringLength(6, ErrorMessage = "Please enter a valid 4 digit Number.", MinimumLength = 6)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [RequiredIf("BankingEntityTypeValue == 'Registered Company' && TypeOfVerificationValue == 'Company'", ErrorMessage = "Registration Number is required.")]
        public string Reg2 { get; set; }

        //[RequiredIf("Reg1 != '' || Reg2 != ''", ErrorMessage = "Required for Registration Number.")]
        //[RequiredIf("!IsNullOrWhiteSpace(Reg1) || !IsNullOrWhiteSpace(Reg2) && BankingEntityTypeValue == 'Registered Company' && TypeOfVerificationValue == 'Company'", ErrorMessage = "Either search by Business Name or Registration Number.")]
        [StringLength(2, ErrorMessage = "Please enter a valid 4 digit Number.", MinimumLength = 2)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [RequiredIf("BankingEntityTypeValue == 'Registered Company' && TypeOfVerificationValue == 'Company'", ErrorMessage = "Registration Number is required.")]
        public string Reg3 { get; set; }

        [Display(Name = "Trust Number")]
        [StringLength(30, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        [RequiredIf("TypeOfVerificationValue == 'Company' && BankingEntityTypeValue == 'Trust'", ErrorMessage = "Trust number is required")]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string TrustNumber { get; set; }

        [Display(Name = "Branch Name")]
        [StringLength(30, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        [RegularExpression(NameOrganisation.Pattern, ErrorMessage = NameOrganisation.Pattern)]
        [Required(ErrorMessage = "Branch name is required")]
        public string BranchName { get; set; }

        [Display(Name = "Branch Code")]
        [StringLength(6, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 4)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [Required(ErrorMessage = "Branch Code is a required field.")]
        public string BranchCode { get; set; }

        [Display(Name = "Account Number")]
        [StringLength(25, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [Required(ErrorMessage = "Account Number is a required field.")]
        public string AccountNumber { get; set; }

        [Display(Name = "Account Holder")]
        [StringLength(30, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Pattern)]
        [Required(ErrorMessage = "Account holder is required")]
        public string AccountHolder { get; set; }


        //Is DecimalValue the correct RegEx pattern for this?
        [Display(Name = "Amount")]
        [StringLength(30, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        [Required(ErrorMessage = "Amount is required")]
        [RegularExpression(DecimalValue.Pattern, ErrorMessage = DecimalValue.Message)]
        public string Amount { get; set; }

        [Display(Name = "Additional Information")]
        [RegularExpression(GeneralText.Pattern, ErrorMessage = GeneralText.Message)]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string AdditionalInformation { get; set; }

        [Display(Name = "First Name")]
        [Required(ErrorMessage = "Please enter your first name")]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string FirstName { get; set; }

        [Display(Name = "Surname")]
        [Required(ErrorMessage = "Please enter your surname")]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string Surname { get; set; }

        [Display(Name = "Company")]
        [Required(ErrorMessage = "Company name is required")]
        [RegularExpression(NameOrganisation.Pattern, ErrorMessage = NameOrganisation.Message)]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string Company { get; set; }

        [Display(Name = "Contact Number")]
        [Required(ErrorMessage = "Contact number is required")]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [StringLength(10, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string ContactNumber { get; set; }

        [Display(Name = "Email Address")]
        [Required(ErrorMessage = "Email address is required")]
        [RegularExpression(Shandon.Utils.RegExPatterns.EmailAddress.Pattern, ErrorMessage = Shandon.Utils.RegExPatterns.EmailAddress.Message)]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string EmailAddress { get; set; }

        [Display(Name = "Voucher Code")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string VoucherCode { get; set; }

        [Display(Name = "Your Reference")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(ReferenceChars.Pattern, ErrorMessage = ReferenceChars.Message)]
        public string YourReference { get; set; }

        public string BankCodeRequestTypeValue { get; set; }

        [Required(ErrorMessage = "Bank name is a required field.")]
        public string BankNameValue { get; set; }

        [Required(ErrorMessage = "Term is a required field.")]
        public string TermValue { get; set; }

        public string BankingEntityTypeValue { get; set; }
        public string TypeOfVerificationValue { get; set; }

    }

    [XmlRoot(ElementName = "BankCodeRequestTypes"), XmlType("BankCodeRequestTypes")]
    public class ListOfRequestTypes
    {
        [XmlElement("RequestType")]
        public List<BankCodeRequestTypes> RequestTypes { get; set; }
        public ListOfRequestTypes()
        {
            RequestTypes = new List<BankCodeRequestTypes>();
        }
    }

    [XmlRoot(ElementName = "ConnectBankNames"), XmlType("ConnectBankNames")]
    public class ListOfBanks
    {
        [XmlElement("AccBankNames")]
        public List<BankNames> BankNames { get; set; }
        public ListOfBanks()
        {
            BankNames = new List<BankNames>();
        }
    }

    [XmlType("RequestType")]
    public class BankCodeRequestTypes
    {

        public string Display { get; set; }

        public string Value { get; set; }
        
    }

    [XmlType("AccBankNames")]
    public class BankNames
    {
        public string BankName { get; set; }
        public string Code { get; set; }
    }

}
