﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;

namespace XDS.ONLINE.MODELS
{
    public class BusinessInvestigateMatchViewModel
    {
        //public List<CountryList> _countryListIitems = new List<CountryList>();
        //public List<ReportType> _reportTypeIitems = new List<ReportType>();
        public List<ReportTimeFrame> _reportTimeFrameIitems = new List<ReportTimeFrame>();
        //public List<Terms> _termIitems = new List<Terms>();

        public BusinessInvestigateMatchViewModel()
        {
            //_countryListIitems.Add(new CountryList { Text = "South Africa", Value = "South Africa" });
            //_countryListIitems.Add(new CountryList { Text = "Namibia", Value = "Namibia" });
            //_countryListIitems.Add(new CountryList { Text = "Mozambique", Value = "Mozambique" });
            //_countryListIitems.Add(new CountryList { Text = "Botswana", Value = "Botswana" });
            //_countryListIitems.Add(new CountryList { Text = "Lesotho", Value = "Lesotho" });
            //_countryListIitems.Add(new CountryList { Text = "Zimbabwe", Value = "Zimbabwe" });
            //_countryListIitems.Add(new CountryList { Text = "Malawi", Value = "Malawi" });
            //_countryListIitems.Add(new CountryList { Text = "Swaziland", Value = "Swaziland" });
            //_countryListIitems.Add(new CountryList { Text = "Ghana", Value = "Ghana" });
            //_countryListIitems.Add(new CountryList { Text = "Nigeria", Value = "Nigeria" });

            //_reportTypeIitems.Add(new ReportType { Text = "Xpert Comprehensive", Value = "Xpert Comprehensive" });
            //_reportTypeIitems.Add(new ReportType { Text = "Xpert Detailed", Value = "Xpert Detailed" });

            _reportTimeFrameIitems.Add(new ReportTimeFrame { Text = "Normal (3 to 5 Working Days)", Value = "Normal (3 to 5 Working Days)" });
            _reportTimeFrameIitems.Add(new ReportTimeFrame { Text = "Express (2 to 3 Working Days)", Value = "Express (2 to 3 Working Days)" });

            //_termIitems.Add(new Terms { Text = "7 Days", Value = "7 Days" });
            //_termIitems.Add(new Terms { Text = "14 Days", Value = "14 Days" });
            //_termIitems.Add(new Terms { Text = "30 Days", Value = "30 Days" });
            //_termIitems.Add(new Terms { Text = "60 Days", Value = "60 Days" });
            //_termIitems.Add(new Terms { Text = "90 Days", Value = "90 Days" });
            //_termIitems.Add(new Terms { Text = "120 Days", Value = "120 Days" });
            //_termIitems.Add(new Terms { Text = "150 Days", Value = "150 Days" });
            //_termIitems.Add(new Terms { Text = "180 Days +", Value = "180 Days +" });
        }

        [Display(Name = "Country")]
        public CountryList Country { get; set; }

        [Display(Name = "Report Type")]
        public ListReportTypes ReportType { get; set; }

        [Display(Name = "Report Time Frame")]
        public List<ReportTimeFrame> ReportTimeFrame
        {
            get
            {
                return _reportTimeFrameIitems;
            }
            set
            {
                _reportTimeFrameIitems = value;
            }
        }

        [Display(Name = "Terms")]
        public ListOfTerms Terms { get; set; }

        [Display(Name = "Registration Number")]
        [StringLength(5, ErrorMessage = " ", MinimumLength = 4)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        [RequiredIf("IsNullOrWhiteSpace(BusinessName)", ErrorMessage = "The Registration Number field is required")]
        public string RegistrationNumber1 { get; set; }

        [StringLength(6, ErrorMessage = "Please enter a valid 6 digit Number.", MinimumLength = 6)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [RequiredIf("(RegistrationNumber1 != '' || RegistrationNumber3 != '') && IsNullOrWhiteSpace(BusinessName)", ErrorMessage = " ")]
        public string RegistrationNumber2 { get; set; }

        [StringLength(2, ErrorMessage = "Please enter a valid 2 digit Number.", MinimumLength = 2)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [RequiredIf("(RegistrationNumber1 != '' || RegistrationNumber2 != '') && IsNullOrWhiteSpace(BusinessName)", ErrorMessage = " ")]
        public string RegistrationNumber3 { get; set; }

        [Display(Name = "Business Name")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        [RegularExpression(NameOrganisation.Pattern, ErrorMessage = NameOrganisation.Pattern)]
        [Required()]
        public string BusinessName { get; set; }

        [Display(Name = "ID Number")]
        [StringLength(13, ErrorMessage = "The ID Number entered is invalid. Please enter a valid 13 digit ID Number.", MinimumLength = 13)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        public string IdNumber { get; set; }

        [Display(Name = "Other")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string OtherNumber { get; set; }

        [Display(Name = "Contact Number")]
        [StringLength(10, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 10)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        //[Required(ErrorMessage = "Telephone Number is a required field.")]
        public string ContactNumberRequest { get; set; }

        [Display(Name = "Amount")]
        [StringLength(10, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [Required(ErrorMessage = "Amount is a required field.")]
        public string Amount { get; set; }

        [Display(Name = "First Name")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RequiredIf("IsNullOrWhiteSpace(IdNumber)", ErrorMessage = "Either search by ID or Name, Surname and Date of Birth.")]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        public string FirstName { get; set; }

        [Display(Name = "Surname")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RequiredIf("IsNullOrWhiteSpace(IdNumber)", ErrorMessage = "Either search by ID or Name, Surname and Date of Birth.")]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        public string Surname { get; set; }

        [Display(Name = "Company Name")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        [RegularExpression(NameOrganisation.Pattern, ErrorMessage = NameOrganisation.Message)]
        [Required(ErrorMessage = "Company Name is a required field.")]
        public string CompanyName { get; set; }

        [Display(Name = "Contact Number")]
        [StringLength(10, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 10)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [Required(ErrorMessage = "Telephone Number is a required field.")]
        public string ContactNumber { get; set; }

        [Display(Name = "Email Address")]
        [Required(ErrorMessage = "Email address is required")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(Shandon.Utils.RegExPatterns.EmailAddress.Pattern, ErrorMessage = Shandon.Utils.RegExPatterns.EmailAddress.Message)]
        public string EmailAddress { get; set; }

        [Display(Name = "Voucher Code")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string VoucherCode { get; set; }

        [Display(Name = "Your Reference")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(ReferenceChars.Pattern, ErrorMessage = ReferenceChars.Message)]
        public string YourReference { get; set; }

        public string CountryListValue { get; set; }
        public string ReportTypeValue { get; set; }
        public string ReportTimeFrameValue { get; set; }
        public string TermsValue { get; set; }
    }

    //public class CountryList
    //{
    //    string _text;
    //    string _value;

    //    public string Text
    //    {
    //        get { return _text; }
    //        set { _text = value; }
    //    }

    //    public string Value
    //    {
    //        get { return _value; }
    //        set { _value = value; }
    //    }
    //}

    //public class ReportType
    //{
    //    string _text;
    //    string _value;

    //    public string Text
    //    {
    //        get { return _text; }
    //        set { _text = value; }
    //    }

    //    public string Value
    //    {
    //        get { return _value; }
    //        set { _value = value; }
    //    }
    //}

    public class ReportTimeFrame
    {
        string _text;
        string _value;

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }

    //public class Terms
    //{
    //    string _text;
    //    string _value;

    //    public string Text
    //    {
    //        get { return _text; }
    //        set { _text = value; }
    //    }

    //    public string Value
    //    {
    //        get { return _value; }
    //        set { _value = value; }
    //    }
    //}

}
