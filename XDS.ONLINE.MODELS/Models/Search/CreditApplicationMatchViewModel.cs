﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Xml.Serialization;

using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;

namespace XDS.ONLINE.MODELS
{
    public class CreditApplicationMatchViewModel
    {

        private List<EnquiryReason> _enquiryReasonList = new List<EnquiryReason>();

        [Display(Name="Marital Status")]
        public List<SelectListItem> MaritalItems = new List<SelectListItem>();

        public CreditApplicationMatchViewModel()
        {
            _enquiryReasonList.Add(new EnquiryReason { Text = "Credit Assessment", Value = "Credit Assessment" });
            _enquiryReasonList.Add(new EnquiryReason { Text = "SAPS or State Agency", Value = "SAPS or State Agency" });
            _enquiryReasonList.Add(new EnquiryReason { Text = "Employment Screening", Value = "Employment Screening" });
            _enquiryReasonList.Add(new EnquiryReason { Text = "Account limit determination", Value = "Account limit determination" });
            _enquiryReasonList.Add(new EnquiryReason { Text = "Application for insurance", Value = "Application for insurance" });
            _enquiryReasonList.Add(new EnquiryReason { Text = "Employment Verification", Value = "Employment Verification" });
            _enquiryReasonList.Add(new EnquiryReason { Text = "Affordability Assessment", Value = "Affordability Assessment" });
            _enquiryReasonList.Add(new EnquiryReason { Text = "Debt Review applications", Value = "Debt Review applications" });
            MaritalItems.Add(new SelectListItem { Text = "Single", Value = "Single" });
            MaritalItems.Add(new SelectListItem { Text = "Married", Value = "Married" });
        }

        [Display(Name = "Enquiry Reason")]
        public List<EnquiryReason> EnquiryReasonList
        {
            get
            {
                return _enquiryReasonList;
            }
            set
            {
                _enquiryReasonList = value;

            }
        }

        [Display(Name = "ID Number")]
        [StringLength(13, ErrorMessage = "The ID Number entered is invalid. Please enter a valid 13 digit ID Number. ", MinimumLength = 13)]
        [RequiredIf("(FirstName == '' && Surname == '' && DateOfBirth == '')", ErrorMessage = "Either search by ID or Name, Surname and Date of Birth.")]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        public string IdNumber { get; set; }

        [Display(Name = "Passport Number/Other ID")]
        //[StringLength(13, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 13)]
        [RequiredIf("IdNumber == '' && FirstName == '' && Surname == '' && DateOfBirth == ''", ErrorMessage = "Either search by ID or Name, Surname and Date of Birth.")]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        //[RequiredIf("(TypeOfVerificationValue == 'Individual' && IdentificationTypeValue == 'Passport')", ErrorMessage = "Either search by Business Name or Registration Number.")]
        public string Passport { get; set; }

        [Display(Name = "First Name")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RequiredIf("IsNullOrWhiteSpace(IdNumber) && IsNullOrWhiteSpace(Passport)", ErrorMessage = "Either search by ID or Name, Surname and Date of Birth.")]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        public string FirstName { get; set; }

        [Display(Name = "Surname")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RequiredIf("IsNullOrWhiteSpace(IdNumber) && IsNullOrWhiteSpace(Passport)", ErrorMessage = "Either search by ID or Name, Surname and Date of Birth.")]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        public string Surname { get; set; }

        [Display(Name = "Date of Birth")]
        [RegularExpression(DateDMY.Pattern, ErrorMessage = "The date of birth supplied is not in the correct format. Please enter the date in the format yyyy/mm/dd")]
        [RequiredIf("IsNullOrWhiteSpace(IdNumber) && IsNullOrWhiteSpace(Passport)", ErrorMessage = "Either search by ID or Name, Surname and Date of Birth.")]
        public string DateOfBirth { get; set; }

        //[Display(Name = "Marital Status")]
        //public List<string> MaritalStatus
        //{
        //    get;
        //    set;
        //}

        [Display(Name = "Voucher Code")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string VoucherCode { get; set; }

        [Display(Name = "Your Reference")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(ReferenceChars.Pattern, ErrorMessage = ReferenceChars.Message)]
        public string YourReference { get; set; }

        public int ProductID { get; set; }

        [Required(ErrorMessage = "Enquiry Reason is a required field.")]
        public string EnquiryReasonValue { get; set; }

        public string MaritalStatusValue { get; set; }

        [Display(Name = "Spouse First Name")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        //[RequiredIf("IsNullOrWhiteSpace(IdNumber)", ErrorMessage = "Either search by ID or Name, Surname and Date of Birth.")]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        public string SpouseFirstName { get; set; }

        [Display(Name = "Spouse Surname")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        //[RequiredIf("IsNullOrWhiteSpace(IdNumber)", ErrorMessage = "Either search by ID or Name, Surname and Date of Birth.")]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        public string SpouseSurname { get; set; }

        [Display(Name = "Line 1")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 0)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string PhysicalAddressLine1 { get; set; }

        [Display(Name = "Line 2")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 0)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string PhysicalAddressLine2 { get; set; }

        [Display(Name = "Line 3")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 0)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string PhysicalAddressLine3 { get; set; }

        [Display(Name = "Line 4")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 0)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string PhysicalAddressLine4 { get; set; }

        [Display(Name = "Code")]
        //[RequiredIf("PostalMatch == false", ErrorMessage = "Postal Code is a required field.")]
        [StringLength(4, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        public string PhysicalAddressCode { get; set; }

        [Display(Name = "Line 1")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 0)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string PostalAddressLine1 { get; set; }

        [Display(Name = "Line 2")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 0)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string PostalAddressLine2 { get; set; }

        [Display(Name = "Line 3")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 0)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string PostalAddressLine3 { get; set; }

        [Display(Name = "Line 4")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 0)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string PostalAddressLine4 { get; set; }

        [Display(Name = "Code")]
        //[RequiredIf("PostalMatch == false", ErrorMessage = "Postal Code is a required field.")]
        [StringLength(4, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        public string PostalAddressCode { get; set; }

        [Display(Name = "Home Telephone Code")]
        [StringLength(3, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 3)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        public string HomeTelephoneCode { get; set; }

        [Display(Name = "Home Telephone No.")]
        [StringLength(7, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 7)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        //[Required(ErrorMessage = "Telephone Number is a required field.")]
        public string HomeTelephoneNo { get; set; }

        [Display(Name = "Work Telephone Code")]
        [StringLength(3, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 3)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        public string WorkTelephoneCode { get; set; }

        [Display(Name = "Work Telephone No.")]
        [StringLength(7, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 7)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        //[Required(ErrorMessage = "Telephone Number is a required field.")]
        public string WorkTelephoneNo { get; set; }

        [Display(Name = "Cellular Code")]
        [StringLength(3, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 3)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        public string CellularCode { get; set; }

        [Display(Name = "Cellular No.")]
        [StringLength(7, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 7)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
       // [Required(ErrorMessage = "Telephone Number is a required field.")]
        public string CellularNo { get; set; }

        [Display(Name = "Email Address")]
        //[DataType(DataType.EmailAddress)]
        [RegularExpression(Shandon.Utils.RegExPatterns.EmailAddress.Pattern, ErrorMessage = Shandon.Utils.RegExPatterns.EmailAddress.Message)]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string EmailAddress { get; set; }

        [Display(Name = "Total Net Monthly Income")]
        [StringLength(10, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        public string TotalNetMonthlyIncome { get; set; }

        [Display(Name = "Employer")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        [RegularExpression(NameOrganisation.Pattern, ErrorMessage = NameOrganisation.Message)]
        public string Employer { get; set; }

        [Display(Name = "Job Title")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        [RegularExpression(NameOrganisation.Pattern, ErrorMessage = NameOrganisation.Message)]
        public string JobTitle { get; set; }
    }    
}
