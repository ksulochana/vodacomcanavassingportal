﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;

namespace XDS.ONLINE.MODELS
{
    public class IndividualEnquiryMatchViewModel
    {

        [Display(Name = "Deeds Office")]
        public List<DeedOfficeTypes> DeedsOffice { get; set; }


        [Display(Name = "ID Number/Other")]
        [StringLength(13, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [Required(ErrorMessage = "Please enter your ID Number")]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string IDNumber { get; set; }


        [Display(Name = "Surname")]      
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string Surname { get; set; }


        [Display(Name = "First Name")]      
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string FirstName { get; set; }

         
        [Display(Name = "Voucher Code")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string VoucherCode { get; set; }

        [Display(Name = "Your Reference")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(ReferenceChars.Pattern, ErrorMessage = ReferenceChars.Message)]
        public string YourReference { get; set; }

        public int ProductID { get; set; }

        [Required(ErrorMessage = "Please select a Deeds Office.")]
        [RequiredIf("DeedsOfficeValue == 'Please select'", ErrorMessage = "Please select Deeds Office.")]
        public string DeedsOfficeValue { get; set; }

    }


}
