﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;

namespace XDS.ONLINE.MODELS
{
    public class HistoryMatchViewModel
    {
        private List<string> _statusList = new List<string>();

        public HistoryMatchViewModel()
        {
            _statusList.Add("All");
            _statusList.Add("Passed");
            _statusList.Add("Apply In Store");
        }

        [Display(Name = "Start Date")] 
        [RegularExpression(DateDMY.Pattern, ErrorMessage = "The date supplied is not in the correct format. Please enter the date in the format dd/mm/yyyy")]
        [RequiredIf("StatusValue != null", ErrorMessage = "Start Date and End Date are required")]
        public string StartDate { get; set; }

        [Display(Name = "End Date")] 
        [RegularExpression(DateDMY.Pattern, ErrorMessage = "The date supplied is not in the correct format. Please enter the date in the format dd/mm/yyyy")]
        [RequiredIf("StatusValue != null", ErrorMessage = "Start Date and End Date are required")]
        public string EndDate { get; set; }

        [Display(Name = "ID Number")]
        [StringLength(13, ErrorMessage = "The ID Number entered is invalid. Please enter a valid 13 digit ID Number. ", MinimumLength = 13)]
        public string IDNo { get; set; }

        [Display(Name = "Reference Number")] 
        public string ReferenceNo { get; set; }

        [Required]
        [Display(Name = "Status")] 
        public List<string> StatusList
        {
            get
            {
                return _statusList;
            }
            set
            {
                _statusList = value;
            }
        }
         
        public string StatusValue { get; set; }

        public string SystemUserName { get; set; }

        public long EnquiryID { get; set; }

    }
}