﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Xml.Serialization;

using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;

namespace XDS.ONLINE.MODELS
{
    public class CreditEnquiryMatchViewModel
    {

        private List<EnquiryReason> _enquiryReasonList = new List<EnquiryReason>();

        public string EnquiryReasons { get; set; }

        //public IEnumerable<SelectListItem> EnquiryReasons { get; set; }

        public CreditEnquiryMatchViewModel()
        { 
        }

        /// <summary>
        /// Use this to populate the dropdown with the XML string passed in here...
        /// </summary>
        /// <param name="enquiryReasons">XML string of all the Credit Enquiry Reasons</param>
        public CreditEnquiryMatchViewModel(string enquiryReasons)
        {

            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            EnquiryReasons = enquiryReasons;
            doc.LoadXml(EnquiryReasons);
            System.Xml.XmlNodeList nodeList = doc.SelectNodes("//CreditEnquiryReasons/EnquiryReason");

            foreach (System.Xml.XmlNode node in nodeList)
            {
                _enquiryReasonList.Add(new EnquiryReason { Text = node.InnerText, Value = node.InnerText });
            }            
        }

        /// <summary>
        /// Maybe to use if we want to use the async call to get the CreditEnquiry Reasons from the WebService
        /// </summary>
        public ListOfEnquiryReasons ListOfReasons
        {
            get;
            set;
        }

        [Display(Name = "Enquiry Reason")]
        //[RequiredIf("IsNullOrWhiteSpace(EnquiryReasonValue)", ErrorMessage = "Please select a Credit Reason.")]
        public List<EnquiryReason> EnquiryReasonList
        {
            get
            {
                return _enquiryReasonList;
            }
            set
            {
                _enquiryReasonList = value;

            }
        }

        [Display(Name = "ID Number")]
        [StringLength(13, ErrorMessage = "The ID Number entered is invalid. Please enter a valid 13 digit ID Number. ", MinimumLength = 13)]
        [RequiredIf("(FirstName == '' && Surname == '' && DateOfBirth == '')", ErrorMessage = "Either search by ID/Passport Number or Name, Surname and Date of Birth.")]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        public string IdNumber { get; set; }

        [Display(Name = "Passport Number/Other ID")]
        //[StringLength(13, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 13)]
        [RequiredIf("(IdNumber == '' && FirstName == '' && Surname == '' && DateOfBirth == '' && IdNumber == '')", ErrorMessage = "Either search by ID/Passport Number or Name, Surname and Date of Birth.")]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        //[RequiredIf("(TypeOfVerificationValue == 'Individual' && IdentificationTypeValue == 'Passport')", ErrorMessage = "Either search by Business Name or Registration Number.")]
        public string Passport { get; set; }

        [Display(Name = "First Name")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RequiredIf("IsNullOrWhiteSpace(IdNumber) && IsNullOrWhiteSpace(Passport)", ErrorMessage = "Either search by ID/Passport Number or Name, Surname and Date of Birth.")]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        public string FirstName { get; set; }

        [Display(Name = "Surname")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RequiredIf("IsNullOrWhiteSpace(IdNumber) && IsNullOrWhiteSpace(Passport)", ErrorMessage = "Either search by ID/Passport Number or Name, Surname and Date of Birth.")]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        public string Surname { get; set; }

        [Display(Name = "Date of Birth")]
        //[RegularExpression(@"(((0|1)[1-9]|2[1-9]|3[0-1])\/(0[1-9]|1[1-2])\/((19|20)\d\d))$", ErrorMessage = "The date of birth supplied is not in the correct format. Please enter the date in the format dd/mm/yyyy")]
        [RegularExpression(DateDMY.Pattern, ErrorMessage = "The date of birth supplied is not in the correct format. Please enter the date in the format dd/mm/yyyy")]
        [RequiredIf("IsNullOrWhiteSpace(IdNumber) && IsNullOrWhiteSpace(Passport)", ErrorMessage = "Either search by ID/Passport Number or Name, Surname and Date of Birth.")]
        public string DateOfBirth { get; set; }

        [Display(Name = "Voucher Code")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string VoucherCode { get; set; }

        [Display(Name = "Your Reference")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(ReferenceChars.Pattern, ErrorMessage = ReferenceChars.Message)]
        public string YourReference { get; set; }

        public int ProductID { get; set; }

        //[RequiredIf("IsNullOrWhiteSpace(EnquiryReasonValue)", ErrorMessage = "Please select a Credit Reason.")]
        [Required(ErrorMessage = "Please select a Credit Reason.")]
        public string EnquiryReasonValue { get; set; }

    }

    [XmlRoot(ElementName="CreditEnquiryReasons"), XmlType("CreditEnquiryReasons")]
    public class ListOfEnquiryReasons
    {
        //private EnquiryReason[] enquiryReasonField;

        //[System.Xml.Serialization.XmlElementAttribute("CreditEnquiryReasons")]
        //[System.Xml.Serialization.XmlElementAttribute("EnquiryReason")]
        //public EnquiryReason[] EnquiryReason
        //{
        //    get
        //    {
        //        return this.enquiryReasonField;
        //    }
        //    set
        //    {
        //        this.enquiryReasonField = value;
        //    }
        //}


        public ListOfEnquiryReasons()
        {
            Reasons = new List<CreditEnquiryReasons>();
        }

        [XmlElement("EnquiryReason")]
        public List<CreditEnquiryReasons> Reasons { get; set; }

        //private CreditEnquiryReasons[] enquiryReasonField;

        //[System.Xml.Serialization.XmlElementAttribute("EnquiryReason")]
        //public CreditEnquiryReasons[] EnquiryReasons
        //{
        //    get
        //    {
        //        return this.enquiryReasonField;
        //    }
        //    set
        //    {
        //        this.enquiryReasonField = value;
        //    }
        //}

    }

    public class EnquiryReason
    {

        string _text;
        string _value;
        
        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }


    [XmlType("EnquiryReason")]
    public class CreditEnquiryReasons
    {
        [XmlText]
        public string enquiryReason { get; set;}

    }
}
