﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDS.ONLINE.MODELS.Models.Search
{
    public class ConnectGetBankCodeResultModel
    {
        string ConnectTicket { get; set; }
        int EnquiryLogID { get; set; }
    }
}
