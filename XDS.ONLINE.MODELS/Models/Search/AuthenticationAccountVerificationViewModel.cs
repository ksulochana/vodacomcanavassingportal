﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;

namespace XDS.ONLINE.MODELS
{
    public class AuthenticationAccountVerificationViewModel
    {
        public AuthenticationAccountVerificationViewModel()
        { }

        [Display(Name = "Initials")]
        [StringLength(5, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        [Required(ErrorMessage = "Initials Required")]
        public string Initials { get; set; }

        [Display(Name = "Surname")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        [Required(ErrorMessage = "Surname is a required field.")]
        public string Surname { get; set; }

        [Display(Name = "Account Number")]
        [StringLength(25, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [Required(ErrorMessage = "Account Number is a required field.")]
        public string AccountNumber { get; set; }

        [Display(Name = "Branch Code")]
        [StringLength(6, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 4)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [Required(ErrorMessage = "Branch Code is a required field.")]
        public string BranchCode { get; set; }

        [Display(Name = "Account Type")]
        public ListofBankAccountTypes BankAccountType { get; set; }

        [Display(Name = "Bank Name")]
        public ListOfBanks BankName { get; set; }

        [Display(Name = "Account Type")]
        [Required]
        public string BankAccountTypeValue { get; set; }

        [Display(Name = "Bank Name")]
        [Required]
        public string BankNameValue { get; set; }

        public long ConsumerID { get; set; }

        public int EnquiryID { get; set; }

        public int EnquiryResultID { get; set; }
    }
}