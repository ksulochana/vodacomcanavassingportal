﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;

namespace XDS.ONLINE.MODELS
{
    public class AuthenticationEnquiryV22MatchViewModel
    {
        private List<ListOfOTPReasons> _otpReasonList = new List<ListOfOTPReasons>();

        public AuthenticationEnquiryV22MatchViewModel()
        {
            _otpReasonList.Add(new ListOfOTPReasons { Text = "", Value = "" });
            _otpReasonList.Add(new ListOfOTPReasons { Text = "Customer does not have a cellular", Value = "Customer does not have a cellular" });
            _otpReasonList.Add(new ListOfOTPReasons { Text = "Customer only has a landline as a contact number", Value = "Customer only has a landline as a contact number" });
            _otpReasonList.Add(new ListOfOTPReasons { Text = "Customer does not have any contact numbers", Value = "Customer does not have any contact numbers" });
            _otpReasonList.Add(new ListOfOTPReasons { Text = "Other", Value = "Other" });
        }


        [Display(Name = "Branch")]
        public ProfileBranches[] EnquiryBranch { get; set; }

        [Display(Name = "Purpose")]
        public ProfilePurposes[] EnquiryPurpose { get; set; }

        [Display(Name = "ID Number")]
        [StringLength(13, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 13)]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [RequiredIf("IsNullOrWhiteSpace(Passport)", ErrorMessage = "Your search input must either be an ID Number or a Passport Number.")]
        public string IdNumber { get; set; }

        [Display(Name = "Passport Number/Other ID")]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        [RequiredIf("IsNullOrWhiteSpace(IdNumber)", ErrorMessage = "Your search input must either be an ID Number or a Passport Number.")]
        public string Passport { get; set; }

        [Display(Name = "Cellular Code")]
        [StringLength(3, ErrorMessage = "The {0} must be at least {2} characters long.")]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        public string CellularCode { get; set; }

        [Display(Name = "Cellular No.")]
        [StringLength(7, ErrorMessage = "The {0} must be at least {2} characters long.")]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        //[Required(ErrorMessage = "Contact no is mandatory, please supply a valid value.")]
        public string CellularNo { get; set; }

        [Display(Name = "Cellular No.")]
        [StringLength(10, ErrorMessage = "The {0} must be at least {2} characters long.")]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        //[Required(ErrorMessage = "Contact no is mandatory, please supply a valid value.")]
        public string TelephoneNo { get; set; }

        [Display(Name = "Email Address")]
        [RegularExpression(Shandon.Utils.RegExPatterns.EmailAddress.Pattern, ErrorMessage = Shandon.Utils.RegExPatterns.EmailAddress.Message)]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string EmailAddress { get; set; }

        [Display(Name = "Override OTP")]
        public bool OverrideOTP { get; set; }

        [Display(Name = "Reason")]
        public List<ListOfOTPReasons> OverrideOTPReason
        {
            get
            {
                return _otpReasonList;
            }
            set
            {
                _otpReasonList = value;

            }
        }

        [Display(Name = "Voucher Code")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string VoucherCode { get; set; }

        [Display(Name = "Your Reference")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(ReferenceChars.Pattern, ErrorMessage = ReferenceChars.Message)]
        public string YourReference { get; set; }

        [Required(ErrorMessage = "Enquiry Branch is a required field.")]
        public string EnquiryBranchValue { get; set; }

        [Required(ErrorMessage = "Enquiry Purpose is a required field.")]
        public string EnquiryPurposeValue { get; set; }

        [RequiredIf("UserOverrideOTP == true && IsNullOrWhiteSpace(OverrideOTPReasonValue)", ErrorMessage = "Override OTP Reason is a required field.")]
        public string OverrideOTPReasonValue { get; set; }

        public XDS.ONLINE.REPOSITORY.XDSConnectAuthWS.AuthenticationSearchType SearchType { get; set; }

        public string AccountNo { get; set; }

        public string SubAccountNo { get; set; }

        [Display(Name = "Override OTP")]
        public bool UserOverrideOTP { get; set; }

        [Display(Name = "Comment")]
        [RequiredIf("UserOverrideOTP == true && OverrideOTPReasonValue == 'Other'", ErrorMessage = "Comment is a required field.")]
        public string OverrideOTPComment { get; set; }

        public bool FraudScore { get; set; }
        public bool AccountVerification { get; set; }
        public bool IDPhoto { get; set; }
        public bool CreditReport { get; set; }
        public bool ResendOTP { get; set; }
        public int NoOfOTPRetries { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class ConsumerInfoDocument
    {
        public long ConsumerID { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string PostalCode { get; set; }
        public string ErrorMessage { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class UpdateConsumerInfo
    {
        public string Result { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class UnblockReasons
    {
        [System.Xml.Serialization.XmlElementAttribute("Reason")]
        public Reason[] Reason { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class OTPNumbers
    {
        public string ProvidedContactNumber { get; set; }
        public string LatestContactNumber { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class FraudScore
    { 
        [System.Xml.Serialization.XmlElementAttribute("ScoreDetails")]
        public ScoreDetails[] ScoreDetails { get; set; } 
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class ScoreDetails
    {
        public int ConsumerID { get; set; }
        public float XDSFraudScore { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class AccountVerificationResult
    {
        [System.Xml.Serialization.XmlElementAttribute("ResultFile")]
        public ResultFile[] ResultFile { get; set; } 
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class ResultFile
    {
        public string RECORDINDICATOR { get; set; }
        public string SEQUENCENUMBER { get; set; }
        public string BRANCHNUMBER { get; set; }
        public string ACCOUNTNUMBER { get; set; }
        public string ACCOUNTTYPE { get; set; }
        public string IDNUMBER { get; set; }
        public string IDTYPE { get; set; }
        public string INITIALS { get; set; }
        public string SURNAME { get; set; }
        public string TAXREFERENCENUMBER { get; set; }
        public string CLIENTUSERREFERENCE { get; set; }
        public string SUBBILLINGID { get; set; }
        public string ERRORCONDITIONNUMBER { get; set; }
        public string ACCOUNTFOUND { get; set; }
        public string IDNUMBERMATCH { get; set; }
        public string INITIALSMATCH { get; set; }
        public string SURNAMEMATCH { get; set; }
        public string ACCOUNTOPEN { get; set; }
        public string ACCOUNTDORMANT { get; set; }
        public string ACCOUNTOPENFORATLEASTTHREEMONTHS { get; set; }
        public string ACCOUNTACCEPTSDEBITS { get; set; }
        public string ACCOUNTACCEPTSCREDITS { get; set; }
        public string TAXREFERENCEMATCH { get; set; }
        public string ACCOUNTISSUER { get; set; }
        public string ACCOUNTTYPERETURN { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class Reason
    { 
        public int ID { get; set; }
        public string UnblockReason { get; set; }
    }

}
     