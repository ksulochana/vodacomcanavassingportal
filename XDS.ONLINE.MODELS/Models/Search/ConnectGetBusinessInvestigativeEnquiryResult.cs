﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDS.ONLINE.MODELS
{
    public class ConnectGetBusinessInvestigativeEnquiryResult
    {
        public string ConnectTicket { get; set; }
        public int EnquiryID { get; set; }
        public int EnquiryResultID { get; set; }
        public int ProductID { get; set; }
        public int ReferenceNo { get; set; } 
        public string BonusXML { get; set; }
    }
}
