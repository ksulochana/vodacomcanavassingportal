﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;

namespace XDS.ONLINE.MODELS
{
  public  class TitleDeedEnquiryMatchViewModel
    {

        [Display(Name = "Deeds Office")]
        public List<DeedOfficeTypes> DeedsOffice { get; set; }

      
        [Display(Name = "Bond Account Number")]   
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        [RequiredIf("(IsNullOrWhiteSpace(TitleDeedNo))", ErrorMessage = "Please enter Bond Account Number.")]
        public string BondAccNo { get; set; }


        [Display(Name = "Title Deed Number")]    
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        [RequiredIf("(IsNullOrWhiteSpace(BondAccNo))", ErrorMessage = "Please enter Title Deed Number.")]
        public string TitleDeedNo { get; set; }

        [Display(Name = "Voucher Code")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string VoucherCode { get; set; }

        [Display(Name = "Your Reference")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(ReferenceChars.Pattern, ErrorMessage = ReferenceChars.Message)]
        public string YourReference { get; set; }

        public int ProductID { get; set; }

        [Required(ErrorMessage = "Please select a Deeds Office.")]
        public string DeedsOfficeValue { get; set; }






    }
}
