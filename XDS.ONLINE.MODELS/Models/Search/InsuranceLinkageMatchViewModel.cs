﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;

using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;

namespace XDS.ONLINE.MODELS
{
    public class InsuranceLinkageMatchViewModel
    {
        [Display(Name = "ID Number")]
        [StringLength(13, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 13)]
        [RequiredIf("(IsNullOrWhiteSpace(POFirstName) && IsNullOrWhiteSpace(POSurname) && IsNullOrWhiteSpace(PODateOfBirth) && IsNullOrWhiteSpace(POPassportNumber))", ErrorMessage = "Either search by ID/Passport or Name, Surname and Date of Birth.")]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        public string POIdNumber { get; set; }

        [Display(Name = "Passport Number")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RequiredIf("(IsNullOrWhiteSpace(POFirstName) && IsNullOrWhiteSpace(POSurname) && IsNullOrWhiteSpace(PODateOfBirth) && IsNullOrWhiteSpace(POIdNumber))", ErrorMessage = "Either search by ID/Passport or Name, Surname and Date of Birth.")]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string POPassportNumber { get; set; }

        [Display(Name = "First Name")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RequiredIf("(IsNullOrWhiteSpace(POIdNumber) && IsNullOrWhiteSpace(POPassportNumber))", ErrorMessage = "Either search by ID/Passport or Name, Surname and Date of Birth.")]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        public string POFirstName { get; set; }

        [Display(Name = "Surname")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RequiredIf("(IsNullOrWhiteSpace(POIdNumber) && IsNullOrWhiteSpace(POPassportNumber))", ErrorMessage = "Either search by ID/Passport or Name, Surname and Date of Birth.")]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        public string POSurname { get; set; }

        [Display(Name = "Date of Birth")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [RequiredIf("(IsNullOrWhiteSpace(POIdNumber) && IsNullOrWhiteSpace(POPassportNumber))", ErrorMessage = "Either search by ID/Passport or Name, Surname and Date of Birth.")]
        public string PODateOfBirth { get; set; }

        [Display(Name = "Claim Number")]
        [StringLength(25, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [Required(ErrorMessage = "Claim Number is a required field.")]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string ClaimNumber { get; set; }

        [Display(Name = "ID Number")]
        [StringLength(13, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 13)]
        [RequiredIf("(IsNullOrWhiteSpace(TPFirstName) && IsNullOrWhiteSpace(TPSurname) && IsNullOrWhiteSpace(TPDateOfBirth) && IsNullOrWhiteSpace(TPPassportNumber))", ErrorMessage = "Either search by ID/Passport or Name, Surname and Date of Birth.")]
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        public string TPIdNumber { get; set; }

        [Display(Name = "Passport Number")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RequiredIf("(IsNullOrWhiteSpace(TPFirstName) && IsNullOrWhiteSpace(TPSurname) && IsNullOrWhiteSpace(TPDateOfBirth) && IsNullOrWhiteSpace(TPIdNumber))", ErrorMessage = "Either search by ID/Passport or Name, Surname and Date of Birth.")]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string TPPassportNumber { get; set; }

        [Display(Name = "First Name")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RequiredIf("(IsNullOrWhiteSpace(TPIdNumber) && IsNullOrWhiteSpace(TPPassportNumber))", ErrorMessage = "Either search by ID/Passport or Name, Surname and Date of Birth.")]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        public string TPFirstName { get; set; }

        [Display(Name = "Surname")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RequiredIf("(IsNullOrWhiteSpace(TPIdNumber) && IsNullOrWhiteSpace(TPPassportNumber))", ErrorMessage = "Either search by ID/Passport or Name, Surname and Date of Birth.")]
        [RegularExpression(NamePerson.Pattern, ErrorMessage = NamePerson.Message)]
        public string TPSurname { get; set; }

        [Display(Name = "Date of Birth")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [RequiredIf("(IsNullOrWhiteSpace(TPIdNumber) && IsNullOrWhiteSpace(TPPassportNumber))", ErrorMessage = "Either search by ID/Passport or Name, Surname and Date of Birth.")]
        public string TPDateOfBirth { get; set; }

        [Display(Name = "Voucher Code")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(Alphanumeric.Pattern, ErrorMessage = Alphanumeric.Message)]
        public string VoucherCode { get; set; }

        [Display(Name = "Your Reference")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [RegularExpression(ReferenceChars.Pattern, ErrorMessage = ReferenceChars.Message)]
        public string YourReference { get; set; }

        public int ProductID { get; set; }
    }
}
