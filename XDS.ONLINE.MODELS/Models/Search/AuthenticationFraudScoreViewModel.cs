﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using ExpressiveAnnotations.Attributes;
using ExpressiveAnnotations.MvcUnobtrusiveValidatorProvider.Validators;
using Shandon.Utils.RegExPatterns;

namespace XDS.ONLINE.MODELS
{
    public class AuthenticationEnquiryFraudScoreViewModel
    {
        public List<FraudScoreTitle> _fraudScoreTitleItems = new List<FraudScoreTitle>();
        public List<FraudScoreChannel> _fraudScoreChannelItems = new List<FraudScoreChannel>();
        public List<FraudScoreStore> _fraudScoreStoreItems = new List<FraudScoreStore>();

        public AuthenticationEnquiryFraudScoreViewModel()
        {
            _fraudScoreTitleItems.Add(new FraudScoreTitle { Text = "MR", Value = "MR" });
            _fraudScoreTitleItems.Add(new FraudScoreTitle { Text = "MRS", Value = "MRS" });
            _fraudScoreTitleItems.Add(new FraudScoreTitle { Text = "DR", Value = "DR" });
            _fraudScoreTitleItems.Add(new FraudScoreTitle { Text = "MISS", Value = "MISS" }); 
            _fraudScoreTitleItems.Add(new FraudScoreTitle { Text = "PROF", Value = "PROF" });

            _fraudScoreChannelItems.Add(new FraudScoreChannel { Text = "BUSINESS SOLUTIONS", Value = "BUSINESSSOLUTIONS" });
            _fraudScoreChannelItems.Add(new FraudScoreChannel { Text = "DEALERS", Value = "DEALERS" });
            _fraudScoreChannelItems.Add(new FraudScoreChannel { Text = "EMERGING MARKETS", Value = "EMERGINGMARKETS" });
            _fraudScoreChannelItems.Add(new FraudScoreChannel { Text = "FRANCHISE", Value = "FRANCHISE" });
            _fraudScoreChannelItems.Add(new FraudScoreChannel { Text = "NATIONAL CHAINS", Value = "NATIONALCHAINS" });
            _fraudScoreChannelItems.Add(new FraudScoreChannel { Text = "DIRECT", Value = "DIRECT" });
            _fraudScoreChannelItems.Add(new FraudScoreChannel { Text = "ONLINE", Value = "ONLINE" });

            _fraudScoreStoreItems.Add(new FraudScoreStore { Text = "GAUTENG", Value = "GAUTENG" });
            _fraudScoreStoreItems.Add(new FraudScoreStore { Text = "CENTRAL", Value = "CENTRAL" });
            _fraudScoreStoreItems.Add(new FraudScoreStore { Text = "EASTERN CAPE REGION", Value = "EASTERNCAPEREGION" });
            _fraudScoreStoreItems.Add(new FraudScoreStore { Text = "HEAD OFFICE", Value = "HEADOFFICE" });
            _fraudScoreStoreItems.Add(new FraudScoreStore { Text = "KWAZULU NATAL", Value = "KWAZULUNATAL" });
            _fraudScoreStoreItems.Add(new FraudScoreStore { Text = "LIMPOPO", Value = "LIMPOPO" });
            _fraudScoreStoreItems.Add(new FraudScoreStore { Text = "MPUMALANGA", Value = "MPUMALANGA" });
            _fraudScoreStoreItems.Add(new FraudScoreStore { Text = "NORTHERN GAUTENG", Value = "NORTHERNGAUTENG" });
            _fraudScoreStoreItems.Add(new FraudScoreStore { Text = "WESTERN CAPE", Value = "WESTERNCAPE" });
        }

        #region[ Confirmed Home Address ]
        [Display(Name = "Estate/Complex")]
        [Required(ErrorMessage = "Estate/Complex is a required field.")]
        public string Address1 { get; set; }

        [Display(Name = "Street")]
        [Required(ErrorMessage = "Street is a required field.")]
        public string Address2 { get; set; }

        [Display(Name = "Suburb")]
        [Required(ErrorMessage = "Suburb is a required field.")]
        public string Address3 { get; set; }

        [Display(Name = "City")]
        [Required(ErrorMessage = "City is a required field.")]
        public string Address4 { get; set; }

        [Display(Name = "Postal Code")]
        [Required(ErrorMessage = "Postal Code is a required field.")]
        public string PostalCode { get; set; }
        #endregion

        #region[ Fraud Variables ]
        [Display(Name = "Title:")]
        public List<FraudScoreTitle> FraudScoreTitles
        {
            get
            {
                return _fraudScoreTitleItems;
            }
            set
            {
                _fraudScoreTitleItems = value;
            }
        }

        [Display(Name = "Channel:")]
        public List<FraudScoreChannel> FraudScoreChannels
        {
            get
            {
                return _fraudScoreChannelItems;
            }
            set
            {
                _fraudScoreChannelItems = value;
            }
        }

        [Display(Name = "Store Region:")]
        public List<FraudScoreStore> FraudScoreStores
        {
            get
            {
                return _fraudScoreStoreItems;
            }
            set
            {
                _fraudScoreStoreItems = value;
            }
        }

        [Display(Name = "Gross Monthly Salary")] 
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [Required(ErrorMessage = "Gross Monthly Salary is a required field.")]
        public int GrossMonthlySalary { get; set; }

        [Display(Name = "Months Employed")] 
        [RegularExpression(NumericOnly.Pattern, ErrorMessage = NumericOnly.Message)]
        [Required(ErrorMessage = "Months Employed is a required field.")]
        public int MonthsEmployed { get; set; }

        public string FraudScoreTitleValue { get; set; }

        public string FraudScoreChannelValue { get; set; }

        public string FraudScoreStoreValue { get; set; }

        public long ConsumerID { get; set; }

        public int EnquiryID { get; set; }

        public int EnquiryResultID { get; set; }
        #endregion

        #region[ Next View? ]
        public bool AccountVerification { get; set; }
        public bool IDPhoto { get; set; }
        #endregion
    }

    public class FraudScoreTitle
    {
        string _text;
        string _value;

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }

    public class FraudScoreChannel
    {
        string _text;
        string _value;

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }

    public class FraudScoreStore
    {
        string _text;
        string _value;

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }

}
