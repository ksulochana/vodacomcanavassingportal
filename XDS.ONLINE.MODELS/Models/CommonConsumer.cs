﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Xml.Serialization;

namespace XDS.ONLINE.MODELS
{
    public class CommonConsumer
    {
    }

    
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class Consumer
    {
        public const string DateFormat = @"yyyy\/MM\/dd";
        private ConsumerReportInformation reportInformationField;
        private ConsumerConsumerDetail consumerDetailField;
        private ConsumerConsumerCreditAccountSummary consumerCreditAccountSummaryField;
        private ConsumerConsumerFraudIndicatorsSummary consumerFraudIndicatorsSummaryField;
        private ConsumerConsumerScoring consumerScoringField;
        private ConsumerConsumerPropertyInformationSummary consumerPropertyInformationSummaryField;
        private ConsumerConsumerDirectorSummary consumerDirectorSummaryField;
        private ConsumerConsumerAdverseInformation[] consumerAdverseInformationField;
        private ConsumerConsumerJudgement[] consumerJudgementField;
        private ConsumerConsumerAdminOrder[] consumerAdminOrderField;
        private ConsumerConsumerSequestration[] consumerSequestrationField;
        private ConsumerConsumerRehabilitationOrder[] consumerRehabilitationOrderField;
        private ConsumerConsumerDefaultAlert[] consumerDefaultAlertField;
        private ConsumerConsumerEnquiryHistory[] consumerEnquiryHistoryField;
        private ConsumerConsumerDebtReviewStatus[] consumerDebtReviewStatusField;
        private ConsumerConsumerFraudIndicators consumerFraudIndicatorsField;
        private ConsumerXDSConsumerAddressHistory[] xDSconsumerAddressHistoryField;
        private ConsumerXDSConsumerContactHistory[] xDSconsumerContactHistoryField; 
        private ConsumerConsumerAddressHistory[] consumerAddressHistoryField;
        private ConsumerConsumerTelephoneHistory[] consumerTelephoneHistoryField;
        private ConsumerConsumerEmploymentHistory[] consumerEmploymentHistoryField;
        private ConsumerEmployer consumerEmployer;
        private ConsumerConsumerTelephoneLinkageHome[] consumerTelephoneLinkageHomeField;
        private ConsumerConsumerTelephoneLinkageWork[] consumerTelephoneLinkageWorkField;
        private ConsumerConsumerTelephoneLinkageCellular[] consumerTelephoneLinkageCellularField;
        private ConsumerConsumerMaritalStatusEnquiry consumerMaritalStatusEnquiryField;
        private ConsumerXDSPaymentNotification[] xDSPaymentNotificationField;
        private ConsumerCommercialPossibleJudgment[] consumerCommercialPossibleJudgmentField;
        private ConsumerConsumerEmailHistory[] consumerEmailHistoryField;
        private ConsumerAccountTypeLegend[] accountTypeLegendField;
        private ConsumerConsumerAccountStatus[] consumerAccountStatusField;
        private ConsumerConsumer24MonthlyPaymentHeader consumer24MonthlyPaymentHeaderField;
        private ConsumerConsumer24MonthlyPayment[] consumer24MonthlyPaymentField;
        private ConsumerConsumerDefinition[] consumerDefinitionField;
        private ConsumerNLRAccountTypeLegend[] nLRAccountTypeLegendField;
        private ConsumerConsumerNLRAccountStatus[] consumerNLRAccountStatusField;
        private ConsumerConsumerNLR24MonthlyPaymentHeader consumerNLR24MonthlyPaymentHeaderField;
        private ConsumerConsumerNLR24MonthlyPayment[] consumerNLR24MonthlyPaymentField;
        private ConsumerConsumerNLRDefinition[] consumerNLRDefinitionField;
        private ConsumerConsumerCPANLRDebtSummary consumerCPANLRDebtSummaryField;
        private ConsumerConsumerAccountGoodBadSummary consumerAccountGoodBadSummaryField;
        private ConsumerSubscriberInputDetails subscriberInputDetailsField;
        private ConsumerConsumerBonusSelected[] consumerBonusSelectedField;

        private ConsumerConsumerDirectorShipLink[] consumerDirectorShipLinkField;
        private ConsumerConsumerIdentityVerification consumerIdentityVerificationField;
        private ConsumerConsumerAddressConfirmation[] consumerAddressConfirmationField;
        private ConsumerConsumerContactConfirmation[] consumerContactConfirmationField;

        private ConsumerConsumerOtherContactInfoAddress[] consumerOtherContactInfoAddressField;
        //private ConsumerConsumerOtherContactInfoTelephone consumerOtherContactInfoTelephoneField;

        private ConsumerConsumerPropertyInformation[] consumerPropertyInformationField;

        private ConsumerConsumerDirectorInformation[] commercialDirectorInformationField;
        private ConsumerConsumerAuditorInformation commercialAuditorInformationField;
        private ConsumerConsumerAddressInformation[] commercialAddressInformationField;
        private ConsumerConsumerBusinessInformation commercialBusinessInformationField;
        private ConsumerConsumerCompanyInformation commercialCompanyInformationField;
        private ConsumerActiveDirectorJudgments activeDirectorJudgmentsField;
        private ConsumerConsumerPrincipalInformation[] commercialPrincipalInformationField;
        private ConsumerConsumerActivePrincipalInfoSummary commercialActivePrincipalInfoSummaryField;
        private ConsumerConsumerInActivePrincipalInfoSummary commercialInActivePrincipalInfoSummaryField;
        private ConsumerConsumerActivePrincipalInformation[] commercialActivePrincipalInformationField;
        private ConsumerConsumerInactivePrincipalInformation[] commercialInactivePrincipalInformationField;
        private ConsumerConsumerInactiveDirectorJudgments inactiveDirectorJudgmentsField;
        private ConsumerCommercialSubscriberInputDetails commercialSubscriberInputDetailsField;
        private HomeAffairsIDPhotoDetails iDPhotoDetailsField;

        private CommercialVATInformation[] commercialVATInformationField;

        public CommercialVATInformation[] CommercialVATInformationField
        {
            get { return commercialVATInformationField; }
            set { commercialVATInformationField = value; }
        }

        public DirectorConsumerDetail directorConsumerDetailField;

        private ConsumerConsumerEmploymentConfirmation[] consumerEmploymentConfirmationField;

        private ConsumerConsumerOtherContactInfoTelephone[] consumerOtherContactInfoTelephoneField;

        private DirectorEnquiryHistory[] directorEnquiryHistory;


        private PropertyReportInformation propertyReportInformationField;

        private PropertyPropertyInformation propertyInformationField;

        private PropertyPropertyRegistrationInformation propertyRegistrationInformationField;

        private PropertyBuyerandSellerInformation buyerandSellerInformationField;

        private PropertyBondInformation bondInformationField;

        private PropertyAttorneyInformation attorneyInformationField;

        private PropertyPropertyHistory[] propertyHistoryField;

        private PropertyBuyerHistory[] buyerHistoryField;

        private PropertySellerHistory[] sellerHistoryField;

        private PropertySubscriberInputDetails propertySubscriberInputDetailsField;




        private CommercialChangeHistoryInformation[] changeHistoryInformationField;

        private CommercialCommercialVATInformation[] businessCommercialVATInformationField;

        private CommercialCommercialScoring[] commercialScoringField;

        private CommercialCommercialPropertyInformation[] commercialPropertyInformationField;

        private CommercialCommercialTradeReferencesInfo[] commercialTradeReferencesInfoField;
        private CommercialCommercialBankCodeHistory[] commercialBankCodeHistoryField;
        

        private CommercialCommercialJudgment[] commercialJudgmentField;

        private CommercialDirectorPropertyInterests[] directorPropertyInterestsField;

        public CommercialDirectorPropertyInterests[] DirectorPropertyInterestsField
        {
            get { return directorPropertyInterestsField; }
            set { directorPropertyInterestsField = value; }
        }

       private CommercialDirectorCurrentBusinessinterests[] directorCurrentBusinessinterestsField;

       private CommercialDirectorPreviousBusinessInterests[] directorPreviousBusinessInterestsField;

       [System.Xml.Serialization.XmlElementAttribute("DirectorCurrentBusinessinterests")]
       public CommercialDirectorCurrentBusinessinterests[] DirectorCurrentBusinessinterests
       {
           get
           {
               return this.directorCurrentBusinessinterestsField;
           }
           set
           {
               this.directorCurrentBusinessinterestsField = value;
           }
       }

       /// <remarks/>
       [System.Xml.Serialization.XmlElementAttribute("DirectorPreviousBusinessInterests")]
       public CommercialDirectorPreviousBusinessInterests[] DirectorPreviousBusinessInterests
       {
           get
           {
               return this.directorPreviousBusinessInterestsField;
           }
           set
           {
               this.directorPreviousBusinessInterestsField = value;
           }
       }







        //Director
        //private DirectorReportInformation reportInformationField;
        //private DirectorConsumerDetail consumerDetailField;
        //private DirectorConsumerFraudIndicatorsSummary consumerFraudIndicatorsSummaryField;
        //private DirectorConsumerDirectorSummary consumerDirectorSummaryField;
        //private DirectorConsumerDefaultAlert consumerDefaultAlertField;
        //private DirectorConsumerEnquiryHistory[] consumerEnquiryHistoryField;
        //private DirectorConsumerAddressHistory consumerAddressHistoryField;
        //private DirectorConsumerMaritalStatusEnquiry consumerMaritalStatusEnquiryField;
        //private DirectorSubscriberInputDetails subscriberInputDetailsField;
        public HomeAffairsIDPhotoDetails IDPhotoDetails
        {
            get
            {
                return this.iDPhotoDetailsField;
            }
            set
            {
                this.iDPhotoDetailsField = value;
            }
        }
        /// <remarks/>
        public ConsumerReportInformation ReportInformation
        {
            get
            {
                return this.reportInformationField;
            }
            set
            {
                this.reportInformationField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerDetail ConsumerDetail
        {
            get
            {
                return this.consumerDetailField;
            }
            set
            {
                this.consumerDetailField = value;
            }
        }

        public ConsumerConsumerCreditAccountSummary ConsumerCreditAccountSummary
        {
            get
            {
                return this.consumerCreditAccountSummaryField;
            }
            set
            {
                this.consumerCreditAccountSummaryField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerFraudIndicatorsSummary ConsumerFraudIndicatorsSummary
        {
            get
            {
                return this.consumerFraudIndicatorsSummaryField;
            }
            set
            {
                this.consumerFraudIndicatorsSummaryField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerScoring ConsumerScoring
        {
            get
            {
                return this.consumerScoringField;
            }
            set
            {
                this.consumerScoringField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerPropertyInformationSummary ConsumerPropertyInformationSummary
        {
            get
            {
                return this.consumerPropertyInformationSummaryField;
            }
            set
            {
                this.consumerPropertyInformationSummaryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElement("ConsumerDirectorSummary")]
        public ConsumerConsumerDirectorSummary ConsumerDirectorSummary
        {
            get
            {
                return this.consumerDirectorSummaryField;
            }
            set
            {
                this.consumerDirectorSummaryField = value;
            }
        }

        /// <remarks/>
        [XmlElement("ConsumerAdverseInfo")]
        public ConsumerConsumerAdverseInformation[] ConsumerAdverseInformation
        {
            get
            {
                return this.consumerAdverseInformationField;
            }
            set
            {
                this.consumerAdverseInformationField = value;
            }
        }

        /// <remarks/>
        [XmlElement("ConsumerJudgement")]
        public ConsumerConsumerJudgement[] ConsumerJudgement
        {
            get
            {
                return this.consumerJudgementField;
            }
            set
            {
                this.consumerJudgementField = value;
            }
        }

        public ConsumerConsumerAdminOrder[] ConsumerAdminOrder
        {
            get
            {
                return this.consumerAdminOrderField;
            }
            set
            {
                this.consumerAdminOrderField = value;
            }
        }

        /// <remarks/>
        [XmlElement("ConsumerSequestration")]
        public ConsumerConsumerSequestration[] ConsumerSequestration
        {
            get
            {
                return this.consumerSequestrationField;
            }
            set
            {
                this.consumerSequestrationField = value;
            }
        }

        public ConsumerConsumerRehabilitationOrder[] ConsumerRehabilitationOrder
        {
            get
            {
                return this.consumerRehabilitationOrderField;
            }
            set
            {
                this.consumerRehabilitationOrderField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute("ConsumerDefaultAlert")]
        [System.Xml.Serialization.XmlElement("ConsumerDefaultAlert")]
        public ConsumerConsumerDefaultAlert[] ConsumerDefaultAlert
        {
            get
            {
                return this.consumerDefaultAlertField;
            }
            set
            {
                this.consumerDefaultAlertField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute("ConsumerEnquiryHistory")]
        [XmlElement("ConsumerEnquiryHistory")]
        public ConsumerConsumerEnquiryHistory[] ConsumerEnquiryHistory
        {
            get
            {
                return this.consumerEnquiryHistoryField;
            }
            set
            {
                this.consumerEnquiryHistoryField = value;
            }
        }

        /// <remarks/>
        [XmlElement("ConsumerDebtReviewStatus")]
        public ConsumerConsumerDebtReviewStatus[] ConsumerDebtReviewStatus
        {
            get
            {
                return this.consumerDebtReviewStatusField;
            }
            set
            {
                this.consumerDebtReviewStatusField = value;
            }
        }

        public ConsumerConsumerFraudIndicators ConsumerFraudIndicators
        {
            get
            {
                return this.consumerFraudIndicatorsField;
            }
            set
            {
                this.consumerFraudIndicatorsField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute("ConsumerAddressHistory")]
        [XmlElement("ConsumerAddressHistory")]
        public ConsumerConsumerAddressHistory[] ConsumerAddressHistory
        {
            get
            {
                return this.consumerAddressHistoryField;
            }
            set
            {
                this.consumerAddressHistoryField = value;
            }
        }

        [XmlElement("XDSCommercialAddressHistory")]
        public ConsumerXDSConsumerAddressHistory[] XDSConsumerAddressHistory
        {
            get
            {
                return this.xDSconsumerAddressHistoryField;
            }
            set
            {
                this.xDSconsumerAddressHistoryField = value;
            }
        }

        [XmlElement("XDSCommercialContactHistory")]
        public ConsumerXDSConsumerContactHistory[] XDSConsumeContactHistory
        {
            get
            {
                return this.xDSconsumerContactHistoryField;
            }
            set
            {
                this.xDSconsumerContactHistoryField = value;
            }
        }



        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute("ConsumerTelephoneHistory")]
        [XmlElement("ConsumerTelephoneHistory")]
        public ConsumerConsumerTelephoneHistory[] ConsumerTelephoneHistory
        {
            get
            {
                return this.consumerTelephoneHistoryField;
            }
            set
            {
                this.consumerTelephoneHistoryField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute("ConsumerEmploymentHistory")]
        [XmlElement("ConsumerEmploymentHistory")]
        public ConsumerConsumerEmploymentHistory[] ConsumerEmploymentHistory
        {
            get
            {
                return this.consumerEmploymentHistoryField;
            }
            set
            {
                this.consumerEmploymentHistoryField = value;
            }
        }

        /// <remarks/>
        [XmlElement("ConsumerTelephoneLinkageHome")]
        public ConsumerConsumerTelephoneLinkageHome[] ConsumerTelephoneLinkageHome
        {
            get
            {
                return this.consumerTelephoneLinkageHomeField;
            }
            set
            {
                this.consumerTelephoneLinkageHomeField = value;
            }
        }

        /// <remarks/>
        [XmlElement("ConsumerTelephoneLinkageWork")]
        public ConsumerConsumerTelephoneLinkageWork[] ConsumerTelephoneLinkageWork
        {
            get
            {
                return this.consumerTelephoneLinkageWorkField;
            }
            set
            {
                this.consumerTelephoneLinkageWorkField = value;
            }
        }

        /// <remarks/>
        [XmlElement("ConsumerTelephoneLinkageCellular")]
        public ConsumerConsumerTelephoneLinkageCellular[] ConsumerTelephoneLinkageCellular
        {
            get
            {
                return this.consumerTelephoneLinkageCellularField;
            }
            set
            {
                this.consumerTelephoneLinkageCellularField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerMaritalStatusEnquiry ConsumerMaritalStatusEnquiry
        {
            get
            {
                return this.consumerMaritalStatusEnquiryField;
            }
            set
            {
                this.consumerMaritalStatusEnquiryField = value;
            }
        }
        
        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute("XDSPaymentNotification")]
        [System.Xml.Serialization.XmlElement("XDSPaymentNotification")]
        public ConsumerXDSPaymentNotification[] XDSPaymentNotification
        {
            get
            {
                return this.xDSPaymentNotificationField;
            }
            set
            {
                this.xDSPaymentNotificationField = value;
            }
        }

        [System.Xml.Serialization.XmlElement("CommercialPossibleJudgment")]
        public ConsumerCommercialPossibleJudgment[] ConsumerCommercialPossibleJudgment
        {
            get
            {
                return this.consumerCommercialPossibleJudgmentField;
            }
            set
            {
                this.consumerCommercialPossibleJudgmentField = value;
            }
        }

        /// <remarks/>
        [XmlElement("ConsumerEmailHistory")]
        public ConsumerConsumerEmailHistory[] ConsumerEmailHistory
        {
            get
            {
                return this.consumerEmailHistoryField;
            }
            set
            {
                this.consumerEmailHistoryField = value;
            }
        }

        /// <remarks/>
        [XmlElement("AccountTypeLegend")]
        public ConsumerAccountTypeLegend[] AccountTypeLegend
        {
            get
            {
                return this.accountTypeLegendField;
            }
            set
            {
                this.accountTypeLegendField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute("ConsumerAccountStatus")]
        [XmlElement("ConsumerAccountStatus")]
        public ConsumerConsumerAccountStatus[] ConsumerAccountStatus
        {
            get
            {
                return this.consumerAccountStatusField;
            }
            set
            {
                this.consumerAccountStatusField = value;
            }
        }

        /// <remarks/>
        [XmlElement("Consumer24MonthlyPaymentHeader")]
        public ConsumerConsumer24MonthlyPaymentHeader Consumer24MonthlyPaymentHeader
        {
            get
            {
                return this.consumer24MonthlyPaymentHeaderField;
            }
            set
            {
                this.consumer24MonthlyPaymentHeaderField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute("Consumer24MonthlyPayment")]
        [XmlElement("Consumer24MonthlyPayment")]
        public ConsumerConsumer24MonthlyPayment[] Consumer24MonthlyPayment
        {
            get
            {
                return this.consumer24MonthlyPaymentField;
            }
            set
            {
                this.consumer24MonthlyPaymentField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute("ConsumerDefinition")]
        [XmlElement("ConsumerDefinition")]
        public ConsumerConsumerDefinition[] ConsumerDefinition
        {
            get
            {
                return this.consumerDefinitionField;
            }
            set
            {
                this.consumerDefinitionField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute("NLRAccountTypeLegend")]
        [XmlElement("NLRAccountTypeLegend")]
        public ConsumerNLRAccountTypeLegend[] NLRAccountTypeLegend
        {
            get
            {
                return this.nLRAccountTypeLegendField;
            }
            set
            {
                this.nLRAccountTypeLegendField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute("ConsumerNLRAccountStatus")]
        [XmlElement("ConsumerNLRAccountStatus")]
        public ConsumerConsumerNLRAccountStatus[] ConsumerNLRAccountStatus
        {
            get
            {
                return this.consumerNLRAccountStatusField;
            }
            set
            {
                this.consumerNLRAccountStatusField = value;
            }
        }

        /// <remarks/>
        [XmlElement("ConsumerNLR24MonthlyPaymentHeader")]
        public ConsumerConsumerNLR24MonthlyPaymentHeader ConsumerNLR24MonthlyPaymentHeader
        {
            get
            {
                return this.consumerNLR24MonthlyPaymentHeaderField;
            }
            set
            {
                this.consumerNLR24MonthlyPaymentHeaderField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute("ConsumerNLR24MonthlyPayment")]
        [XmlElement("ConsumerNLR24MonthlyPayment")]
        public ConsumerConsumerNLR24MonthlyPayment[] ConsumerNLR24MonthlyPayment
        {
            get
            {
                return this.consumerNLR24MonthlyPaymentField;
            }
            set
            {
                this.consumerNLR24MonthlyPaymentField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute("ConsumerNLRDefinition")]
        [XmlElement("ConsumerNLRDefinition")]
        public ConsumerConsumerNLRDefinition[] ConsumerNLRDefinition
        {
            get
            {
                return this.consumerNLRDefinitionField;
            }
            set
            {
                this.consumerNLRDefinitionField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerCPANLRDebtSummary ConsumerCPANLRDebtSummary
        {
            get
            {
                return this.consumerCPANLRDebtSummaryField;
            }
            set
            {
                this.consumerCPANLRDebtSummaryField = value;
            }
        }

        public ConsumerConsumerAccountGoodBadSummary ConsumerAccountGoodBadSummary
        {
            get
            {
                return this.consumerAccountGoodBadSummaryField;
            }
            set
            {
                this.consumerAccountGoodBadSummaryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SubscriberInputDetails")]
        public ConsumerSubscriberInputDetails SubscriberInputDetails
        {
            get
            {
                return this.subscriberInputDetailsField;
            }
            set
            {
                this.subscriberInputDetailsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ConsumerBonusSelected")]
        public ConsumerConsumerBonusSelected[] ConsumerBonusSelected
        {
            get
            {
                return this.consumerBonusSelectedField;
            }
            set
            {
                this.consumerBonusSelectedField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute("ConsumerDirectorShipLink")]
        [XmlElement("ConsumerDirectorShipLink")]
        public ConsumerConsumerDirectorShipLink[] ConsumerDirectorShipLink
        {
            get
            {
                return this.consumerDirectorShipLinkField;
            }
            set
            {
                this.consumerDirectorShipLinkField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerIdentityVerification ConsumerIdentityVerification
        {
            get
            {
                return this.consumerIdentityVerificationField;
            }
            set
            {
                this.consumerIdentityVerificationField = value;
            }
        }

        /// <remarks/>
        [XmlElement("ConsumerAddressConfirmation")]
        public ConsumerConsumerAddressConfirmation[] ConsumerAddressConfirmation
        {
            get
            {
                return this.consumerAddressConfirmationField;
            }
            set
            {
                this.consumerAddressConfirmationField = value;
            }
        }

        /// <remarks/>
        [XmlElement("ConsumerContactConfirmation")]
        public ConsumerConsumerContactConfirmation[] ConsumerContactConfirmation
        {
            get
            {
                return this.consumerContactConfirmationField;
            }
            set
            {
                this.consumerContactConfirmationField = value;
            }
        }

        public ConsumerConsumerOtherContactInfoAddress[] ConsumerOtherContactInfoAddress
        {
            get
            {
                return this.consumerOtherContactInfoAddressField;
            }
            set
            {
                this.consumerOtherContactInfoAddressField = value;
            }
        }

        //public ConsumerConsumerOtherContactInfoTelephone ConsumerOtherContactInfoTelephone
        //{
        //    get
        //    {
        //        return this.consumerOtherContactInfoTelephoneField;
        //    }
        //    set
        //    {
        //        this.consumerOtherContactInfoTelephoneField = value;
        //    }
        //}

        [XmlElement("ConsumerPropertyInformation")]
        public ConsumerConsumerPropertyInformation[] ConsumerPropertyInformation
        {
            get
            {
                return this.consumerPropertyInformationField;
            }
            set
            {
                this.consumerPropertyInformationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CommercialDirectorInformation")]
        public ConsumerConsumerDirectorInformation[] CommercialDirectorInformation
        {
            get
            {
                return this.commercialDirectorInformationField;
    }
            set
            {
                this.commercialDirectorInformationField = value;
            }
        }

    /// <remarks/>
        public ConsumerConsumerAuditorInformation CommercialAuditorInformation
        {
            get
            {
                return this.commercialAuditorInformationField;
            }
            set
            {
                this.commercialAuditorInformationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CommercialAddressInformation")]
        public ConsumerConsumerAddressInformation[] CommercialAddressInformation
        {
            get
            {
                return this.commercialAddressInformationField;
            }
            set
            {
                this.commercialAddressInformationField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerBusinessInformation CommercialBusinessInformation
        {
            get
            {
                return this.commercialBusinessInformationField;
            }
            set
            {
                this.commercialBusinessInformationField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerCompanyInformation CommercialCompanyInformation
        {
            get
            {
                return this.commercialCompanyInformationField;
            }
            set
            {
                this.commercialCompanyInformationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ActiveDirectorJudgments")]
        public ConsumerActiveDirectorJudgments ActiveDirectorJudgments
        {
            get
            {
                return this.activeDirectorJudgmentsField;
            }
            set
            {
                this.activeDirectorJudgmentsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CommercialPrincipalInformation")]
        public ConsumerConsumerPrincipalInformation[] CommercialPrincipalInformation
        {
            get
            {
                return this.commercialPrincipalInformationField;
            }
            set
            {
                this.commercialPrincipalInformationField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerActivePrincipalInfoSummary CommercialActivePrincipalInfoSummary
        {
            get
            {
                return this.commercialActivePrincipalInfoSummaryField;
            }
            set
            {
                this.commercialActivePrincipalInfoSummaryField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerInActivePrincipalInfoSummary CommercialInActivePrincipalInfoSummary
        {
            get
            {
                return this.commercialInActivePrincipalInfoSummaryField;
            }
            set
            {
                this.commercialInActivePrincipalInfoSummaryField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerActivePrincipalInformation[] CommercialActivePrincipalInformation
        {
            get
            {
                return this.commercialActivePrincipalInformationField;
            }
            set
            {
                this.commercialActivePrincipalInformationField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerInactivePrincipalInformation[] CommercialInactivePrincipalInformation
        {
            get
            {
                return this.commercialInactivePrincipalInformationField;
            }
            set
            {
                this.commercialInactivePrincipalInformationField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerInactiveDirectorJudgments InactiveDirectorJudgments
        {
            get
            {
                return this.inactiveDirectorJudgmentsField;
            }
            set
            {
                this.inactiveDirectorJudgmentsField = value;
            }
        }

        public ConsumerCommercialSubscriberInputDetails CommercialSubscriberInputDetails
        {
            get
            {
                return this.commercialSubscriberInputDetailsField;
            }
            set
            {
                this.commercialSubscriberInputDetailsField = value;
            }
        }

        /// <remarks/>
        public DirectorConsumerDetail DirectorConsumerDetail
        {
            get
            {
                return this.directorConsumerDetailField;
    }
            set
            {
                this.directorConsumerDetailField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ConsumerEmploymentConfirmation")]
        public ConsumerConsumerEmploymentConfirmation[] ConsumerEmploymentConfirmation
        {
            get
            {
                return this.consumerEmploymentConfirmationField;
    }
            set
            {
                this.consumerEmploymentConfirmationField = value;
            }
        }

    /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute("ConsumerOtherContactInfoTelephone")]
        public ConsumerConsumerOtherContactInfoTelephone[] ConsumerOtherContactInfoTelephone
        {
            get
            {
                return this.consumerOtherContactInfoTelephoneField;
            }
            set
            {
                this.consumerOtherContactInfoTelephoneField = value;
            }
        }

        public DirectorEnquiryHistory[] DirectorEnquiryHistory
        {
            get
            {
                return this.directorEnquiryHistory;
            }
            set
            {
                this.directorEnquiryHistory = value;
            }
        }



        public PropertyReportInformation PropertyReportInformation
        {
            get
            {
                return this.propertyReportInformationField;
      
        }
            set
            {
                this.propertyReportInformationField = value;
            }
        }

          /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ChangeHistoryInformation")]
        public CommercialChangeHistoryInformation[] ChangeHistoryInformation
        {
            get
            {
                return this.changeHistoryInformationField;
             }
            set
            {
                this.changeHistoryInformationField = value;
            }
        }


        public CommercialCommercialVATInformation[] BusinessCommercialVATInformation
        {
            get
            {
                return this.businessCommercialVATInformationField;
            }
            set
            {
                this.businessCommercialVATInformationField = value;
            }
        }

    /// <remarks/>
        public PropertyPropertyInformation PropertyInformation
        {
            get
            {
                return this.propertyInformationField;
            }
            set
            {
                this.propertyInformationField = value;
            }
        }

        /// <remarks/>
        public PropertyPropertyRegistrationInformation PropertyRegistrationInformation
        {
            get
            {
                return this.propertyRegistrationInformationField;
            }
            set
            {
                this.propertyRegistrationInformationField = value;
            }
        }

        /// <remarks/>
        public PropertyBuyerandSellerInformation BuyerandSellerInformation
        {
            get
            {
                return this.buyerandSellerInformationField;
            }
            set
            {
                this.buyerandSellerInformationField = value;
            }
        }

        /// <remarks/>
        public PropertyBondInformation BondInformation
        {
            get
            {
                return this.bondInformationField;
            }
            set
            {
                this.bondInformationField = value;
            }
        }

        /// <remarks/>
        public PropertyAttorneyInformation AttorneyInformation
        {
            get
            {
                return this.attorneyInformationField;
            }
            set
            {
                this.attorneyInformationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PropertyHistory")]
        public PropertyPropertyHistory[] PropertyHistory
        {
            get
            {
                return this.propertyHistoryField;
            }
            set
            {
                this.propertyHistoryField = value;
            }
        }


        [System.Xml.Serialization.XmlElementAttribute("BuyerHistory")]
        public PropertyBuyerHistory[] BuyerHistory
        {
            get
            {
                return this.buyerHistoryField;
            }
            set
            {
                this.buyerHistoryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SellerHistory")]
        public PropertySellerHistory[] SellerHistory
        {
            get
            {
                return this.sellerHistoryField;
            }
            set
            {
                this.sellerHistoryField = value;
            }
        }

        /// <remarks/>
        public PropertySubscriberInputDetails PropertySubscriberInputDetails
        {
            get
            {
                return this.propertySubscriberInputDetailsField;
            }
            set
            {
                this.propertySubscriberInputDetailsField = value;
            }
        }

    /// <remarks/>
        public CommercialCommercialScoring[] CommercialScoring
        {
            get
            {
                return this.commercialScoringField;
            }
            set
            {
                this.commercialScoringField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CommercialPropertyInformation")]
        public CommercialCommercialPropertyInformation[] CommercialPropertyInformation
        {
            get
            {
                return this.commercialPropertyInformationField;
            }
            set
            {
                this.commercialPropertyInformationField = value;
            }
        }

         /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CommercialTradeReferencesInformation")]
        public CommercialCommercialTradeReferencesInfo[] CommercialTradeReferencesInfo
        {
            get
            {
                return this.commercialTradeReferencesInfoField;
            }
            set
            {
                this.commercialTradeReferencesInfoField = value;
            }
        }

          [System.Xml.Serialization.XmlElementAttribute("CommercialBankCodeHistory")]
        public CommercialCommercialBankCodeHistory[] CommercialCommercialBankCodeHistory
        {
            get
            {
                return this.commercialBankCodeHistoryField;
            }
            set
            {
                this.commercialBankCodeHistoryField = value;
            }
        }
        
        
        /// <remarks/>
        public CommercialCommercialJudgment[] CommercialJudgment
        {
            get
            {
                return this.commercialJudgmentField;
    }
            set
            {
                this.commercialJudgmentField = value;
            }
        }

    }
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerReportInformation
    {

        private byte reportIDField;

        private string reportNameField;

        /// <remarks/>
        public byte ReportID
        {
            get
            {
                return this.reportIDField;
            }
            set
            {
                this.reportIDField = value;
            }
        }

        /// <remarks/>
        public string ReportName
        {
            get
            {
                return this.reportNameField;
            }
            set
            {
                this.reportNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerDetail
    {
        private string displayTextField;
        private uint consumerIDField;
        private string initialsField;
        private string firstNameField;
        private string secondNameField;
        private object thirdNameField;
        private string surnameField;
        private string iDNoField;
        private string passportNoField;
        //private System.DateTime? birthDateField;
        private string birthDateField;
        private string genderField;
        private string titleDescField;
        private string maritalStatusDescField;
        private string privacyStatusField;
        private string residentialAddressField;
        private string postalAddressField;
        //private uint homeTelephoneNoField;
        //private uint workTelephoneNoField;
        //private uint cellularNoField;
        private string homeTelephoneNoField;
        private string workTelephoneNoField;
        private string cellularNoField;
        private string emailAddressField;
        private string employerDetailField;
        private string referenceNoField;
        private string externalReferenceField;
        private string xdsdeceasedStatus;
        private string xdsdeceasedDate;
        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public uint ConsumerID
        {
            get
            {
                return this.consumerIDField;
            }
            set
            {
                this.consumerIDField = value;
            }
        }

        /// <remarks/>
        public string Initials
        {
            get
            {
                return this.initialsField;
            }
            set
            {
                this.initialsField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string SecondName
        {
            get
            {
                return this.secondNameField;
            }
            set
            {
                this.secondNameField = value;
            }
        }

        /// <remarks/>
        public object ThirdName
        {
            get
            {
                return this.thirdNameField;
            }
            set
            {
                this.thirdNameField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string IDNo
        {
            get
            {
                return this.iDNoField;
            }
            set
            {
                this.iDNoField = value;
            }
        }

        /// <remarks/>
        public string PassportNo
        {
            get
            {
                return this.passportNoField;
            }
            set
            {
                this.passportNoField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        //public System.DateTime? BirthDate
        public string BirthDate
        {
            get
            {
                return this.birthDateField;

                //return BirthDate.HasValue ? BirthDate.Value : new DateTime(9999, 12, 31);
                //return string.IsNullOrEmpty(BirthDate.Value.ToString()) ? new DateTime(9999, 12, 31) : BirthDate.Value;
                //return BirthDate.HasValue ? BirthDate.Value : null;

            }
            set
            {
                this.birthDateField = value;
                //this.birthDateField = string.IsNullOrEmpty(BirthDate.Value.ToString()) ? new DateTime(9999, 12, 31) : value;
            }
        }

        /// <remarks/>
        public string Gender
        {
            get
            {
                return this.genderField;
            }
            set
            {
                this.genderField = value;
            }
        }

        /// <remarks/>
        public string TitleDesc
        {
            get
            {
                return this.titleDescField;
            }
            set
            {
                this.titleDescField = value;
            }
        }

        /// <remarks/>
        public string MaritalStatusDesc
        {
            get
            {
                return this.maritalStatusDescField;
            }
            set
            {
                this.maritalStatusDescField = value;
            }
        }

        /// <remarks/>
        public string PrivacyStatus
        {
            get
            {
                return this.privacyStatusField;
            }
            set
            {
                this.privacyStatusField = value;
            }
        }

        /// <remarks/>
        public string ResidentialAddress
        {
            get
            {
                return this.residentialAddressField;
            }
            set
            {
                this.residentialAddressField = value;
            }
        }

        /// <remarks/>
        public string PostalAddress
        {
            get
            {
                return this.postalAddressField;
            }
            set
            {
                this.postalAddressField = value;
            }
        }

        /// <remarks/>
        //public uint HomeTelephoneNo
        public string HomeTelephoneNo
        {
            get
            {
                return this.homeTelephoneNoField;
            }
            set
            {
                this.homeTelephoneNoField = value;
            }
        }

        /// <remarks/>
        //public uint WorkTelephoneNo
        public string WorkTelephoneNo
        {
            get
            {
                return this.workTelephoneNoField;
            }
            set
            {
                this.workTelephoneNoField = value;
            }
        }

        /// <remarks/>
        //public uint CellularNo
        public string CellularNo
        {
            get
            {
                return this.cellularNoField;
            }
            set
            {
                this.cellularNoField = value;
            }
        }

        /// <remarks/>
        public string EmailAddress
        {
            get
            {
                return this.emailAddressField;
            }
            set
            {
                this.emailAddressField = value;
            }
        }

        /// <remarks/>
        public string EmployerDetail
        {
            get
            {
                return this.employerDetailField;
            }
            set
            {
                this.employerDetailField = value;
            }
        }

        /// <remarks/>
        public string ReferenceNo
        {
            get
            {
                return this.referenceNoField;
            }
            set
            {
                this.referenceNoField = value;
            }
        }

        /// <remarks/>
        public string ExternalReference
        {
            get
            {
                return this.externalReferenceField;
            }
            set
            {
                this.externalReferenceField = value;
            }
        }

        public string XDSDeceasedStatus
        {
            get
            {
                return this.xdsdeceasedStatus; ;
    }
            set
            {
                this.xdsdeceasedStatus = value;
            }
        }

        public string XDSDeceasedDate
        {
            get
            {
                return this.xdsdeceasedDate; 
            }
            set
            {
                this.xdsdeceasedDate = value;
            }
        }


    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerCreditAccountSummary
    {
        public decimal TotalMonthlyInstallmentAmt { get; set; }

        public decimal TotalOutstandingDebtAmt { get; set; }

        public int TotalNoOfCreditAccounts { get; set; }

        public int NoOfBankAccounts { get; set; }

        public int NoOfRetailAccounts { get; set; }

        public int NoOfFurnitureAccounts { get; set; }

        public int NoOfTelecomAccounts { get; set; }

        public int NoOfCreditCardAccounts { get; set; }

        public int NoOfHomeLoanAccounts { get; set; }

        public int NoOfInsuranceAccounts { get; set; }

        public int NoOfMotorFinanceAccounts { get; set; }

        public int NoOfOtherAccounts { get; set; }

        public int NoOfPersonalFinAccounts { get; set; }

        public int NoOfAdverseAccounts { get; set; }

        public decimal TotalAdverseAmt { get; set; }

        public int NoOfAccountInArrears { get; set; }

        public decimal TotalArrearsAmt { get; set; }

        public int MonthsInArrears { get; set; }

        public int NoOfJudgements { get; set; }

        public decimal TotalJudgementAmt { get; set; }

        public DateTime LastJudgementDate { get; set; }

        public string AdminOrderYN { get; set; } //Should this be a Bool?

        public string DebtReviewStatusDesc { get; set; }

        public bool DisputeInd { get; set; }

        public string BankAccountsDesc { get; set; }

        public string RetailAccountsDesc { get; set; }

        public string FurnitureAccoutsDesc { get; set; }

        public string TelecomAccountsDesc { get; set; }

        public string CreditCardAccountsDesc { get; set; }

        public string HomeLoanAccountsDesc { get; set; }

        public string InsuranceAccountsDesc { get; set; }

        public string MotorFinanceAccountsDesc { get; set; }

        public string OtherAccountsDesc { get; set; }

        public string PersonalFinAccountsDesc { get; set; }

    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerFraudIndicatorsSummary
    {

        private string displayTextField;

        private string sAFPSListingYNField;

        private string homeAffairsVerificationYNField;

        private string homeAffairsDeceasedStatusField;

        private object employerFraudVerificationYNField;

        private object protectiveVerificationYNField;

        private object homeAffairsDeceasedDateField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string SAFPSListingYN
        {
            get
            {
                return this.sAFPSListingYNField;
            }
            set
            {
                this.sAFPSListingYNField = value;
            }
        }

        /// <remarks/>
        public string HomeAffairsVerificationYN
        {
            get
            {
                return this.homeAffairsVerificationYNField;
            }
            set
            {
                this.homeAffairsVerificationYNField = value;
            }
        }

        /// <remarks/>
        public string HomeAffairsDeceasedStatus
        {
            get
            {
                return this.homeAffairsDeceasedStatusField;
            }
            set
            {
                this.homeAffairsDeceasedStatusField = value;
            }
        }

        /// <remarks/>
        public object EmployerFraudVerificationYN
        {
            get
            {
                return this.employerFraudVerificationYNField;
            }
            set
            {
                this.employerFraudVerificationYNField = value;
            }
        }

        /// <remarks/>
        public object ProtectiveVerificationYN
        {
            get
            {
                return this.protectiveVerificationYNField;
            }
            set
            {
                this.protectiveVerificationYNField = value;
            }
        }

        /// <remarks/>
        public object HomeAffairsDeceasedDate
        {
            get
            {
                return this.homeAffairsDeceasedDateField;
            }
            set
            {
                this.homeAffairsDeceasedDateField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerScoring
    {

        private string displayTextField;

        private string subscriberIDField;

        private string unique_IdentifierField;

        private string scoreDateField;

        private string finalScoreField;

        private string exception_CodeField;

        private string reasonCode1Field;

        private string reasonCode2Field;

        private string reasonCode3Field;

        private string modelIDField;

        private string classificationField;

        private string descriptionField;

        private string riskCategoryField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string SubscriberID
        {
            get
            {
                return this.subscriberIDField;
            }
            set
            {
                this.subscriberIDField = value;
            }
        }

        /// <remarks/>
        public string Unique_Identifier
        {
            get
            {
                return this.unique_IdentifierField;
            }
            set
            {
                this.unique_IdentifierField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string ScoreDate
        {
            get
            {
                return this.scoreDateField;
            }
            set
            {
                this.scoreDateField = value;
            }
        }

        /// <remarks/>
        public string FinalScore
        {
            get
            {
                return this.finalScoreField;
            }
            set
            {
                this.finalScoreField = value;
            }
        }

        /// <remarks/>
        public string Exception_Code
        {
            get
            {
                return this.exception_CodeField;
            }
            set
            {
                this.exception_CodeField = value;
            }
        }

        /// <remarks/>
        public string ReasonCode1
        {
            get
            {
                return this.reasonCode1Field;
            }
            set
            {
                this.reasonCode1Field = value;
            }
        }

        /// <remarks/>
        public string ReasonCode2
        {
            get
            {
                return this.reasonCode2Field;
            }
            set
            {
                this.reasonCode2Field = value;
            }
        }

        /// <remarks/>
        public string ReasonCode3
        {
            get
            {
                return this.reasonCode3Field;
            }
            set
            {
                this.reasonCode3Field = value;
            }
        }

        /// <remarks/>
        public string ModelID
        {
            get
            {
                return this.modelIDField;
            }
            set
            {
                this.modelIDField = value;
            }
        }

        /// <remarks/>
        public string classification
        {
            get
            {
                return this.classificationField;
            }
            set
            {
                this.classificationField = value;
            }
        }

        /// <remarks/>
        public string Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        /// <remarks/>
        public string RiskCategory
        {
            get
            {
                return this.riskCategoryField;
            }
            set
            {
                this.riskCategoryField = value;
            }
        }
    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlType("ConsumerPropertyInformationSummary")]
    public partial class ConsumerConsumerPropertyInformationSummary
    {

        private string displayTextField;

        private string totalPropertyField;

        private string purchasePriceField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string TotalProperty
        {
            get
            {
                return this.totalPropertyField;
            }
            set
            {
                this.totalPropertyField = value;
            }
        }

        /// <remarks/>
        public string PurchasePrice
        {
            get
            {
                return this.purchasePriceField;
            }
            set
            {
                this.purchasePriceField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerDirectorSummary
    {

        private string displayTextField;

        private int numberOfCompanyDirectorField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public int NumberOfCompanyDirector
        {
            get
            {
                return this.numberOfCompanyDirectorField;
            }
            set
            {
                this.numberOfCompanyDirectorField = value;
            }
        }
    }




    

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [XmlType("ConsumerJudgement")]
    public partial class ConsumerConsumerJudgement
    {

        private string displayTextField;

        //private System.DateTime caseFilingDateField;
        private string caseFilingDateField;

        private string caseTypeField;

        //private System.DateTime lastUpdatedDateField;
        private string lastUpdatedDateField;

        private string plaintiffNameField;

        private string disputeAmtField;

        private string caseReasonField;

        private string courtNameField;

        private string caseNumberField;

        private string attorneyNameField;

        private string telephoneNoField;

        private string commentsField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string CaseFilingDate
        {
            get
            {
                return this.caseFilingDateField;
            }
            set
            {
                this.caseFilingDateField = value;
            }
        }

        /// <remarks/>
        public string CaseType
        {
            get
            {
                return this.caseTypeField;
            }
            set
            {
                this.caseTypeField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string LastUpdatedDate
        {
            get
            {
                return this.lastUpdatedDateField;
            }
            set
            {
                this.lastUpdatedDateField = value;
            }
        }

        /// <remarks/>
        public string PlaintiffName
        {
            get
            {
                return this.plaintiffNameField;
            }
            set
            {
                this.plaintiffNameField = value;
            }
        }

        /// <remarks/>
        public string DisputeAmt
        {
            get
            {
                return XDS.ONLINE.MODELS.General.TrimDecimalToTwoDecimalPoint(this.disputeAmtField);
            }
            set
            {
                this.disputeAmtField = value;
            }
        }

        /// <remarks/>
        public string CaseReason
        {
            get
            {
                return this.caseReasonField;
            }
            set
            {
                this.caseReasonField = value;
            }
        }

        /// <remarks/>
        public string CourtName
        {
            get
            {
                return this.courtNameField;
            }
            set
            {
                this.courtNameField = value;
            }
        }

        /// <remarks/>
        public string CaseNumber
        {
            get
            {
                return this.caseNumberField;
            }
            set
            {
                this.caseNumberField = value;
            }
        }

        /// <remarks/>
        public string AttorneyName
        {
            get
            {
                return this.attorneyNameField;
            }
            set
            {
                this.attorneyNameField = value;
            }
        }

        /// <remarks/>
        public string TelephoneNo
        {
            get
            {
                return this.telephoneNoField;
            }
            set
            {
                this.telephoneNoField = value;
            }
        }

        /// <remarks/>
        public string Comments
        {
            get
            {
                return this.commentsField;
            }
            set
            {
                this.commentsField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType("ConsumerAdminOrder")]
    public partial class ConsumerConsumerAdminOrder
    {

        private string displayTextField;

        private string caseFilingDateField;

        private string caseTypeField;

        private string lastUpdatedDateField;

        private string plaintiffNameField;

        private string disputeAmtField;

        private string caseReasonField;

        private string courtNameField;

        private string caseNumberField;

        private string attorneyNameField;

        private string telephoneNoField;

        private string commentsField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string CaseFilingDate
        {
            get
            {
                return this.caseFilingDateField;
            }
            set
            {
                this.caseFilingDateField = value;
            }
        }

        /// <remarks/>
        public string CaseType
        {
            get
            {
                return this.caseTypeField;
            }
            set
            {
                this.caseTypeField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string LastUpdatedDate
        {
            get
            {
                return this.lastUpdatedDateField;
            }
            set
            {
                this.lastUpdatedDateField = value;
            }
        }

        /// <remarks/>
        public string PlaintiffName
        {
            get
            {
                return this.plaintiffNameField;
            }
            set
            {
                this.plaintiffNameField = value;
            }
        }

        /// <remarks/>
        public string DisputeAmt
        {
            get
            {
                return this.disputeAmtField;
            }
            set
            {
                this.disputeAmtField = value;
            }
        }

        /// <remarks/>
        public string CaseReason
        {
            get
            {
                return this.caseReasonField;
            }
            set
            {
                this.caseReasonField = value;
            }
        }

        /// <remarks/>
        public string CourtName
        {
            get
            {
                return this.courtNameField;
            }
            set
            {
                this.courtNameField = value;
            }
        }

        /// <remarks/>
        public string CaseNumber
        {
            get
            {
                return this.caseNumberField;
            }
            set
            {
                this.caseNumberField = value;
            }
        }

        /// <remarks/>
        public string AttorneyName
        {
            get
            {
                return this.attorneyNameField;
            }
            set
            {
                this.attorneyNameField = value;
            }
        }

        /// <remarks/>
        public string TelephoneNo
        {
            get
            {
                return this.telephoneNoField;
            }
            set
            {
                this.telephoneNoField = value;
            }
        }

        /// <remarks/>
        public string Comments
        {
            get
            {
                return this.commentsField;
            }
            set
            {
                this.commentsField = value;
            }
        }
    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlType("ConsumerAddressHistory")]
    public partial class ConsumerConsumerAddressHistory
    {

        private string displayTextField;

        private string consumerAddressIDField;

        private string addressTypeField;

        private string addressTypeIndField;

        private string address1Field;

        private string address2Field;

        private string address3Field;

        private string address4Field;

        private string postalCodeField;

        private string addressField;

        private string lastUpdatedDateField;

        private string firstReportedDateField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string ConsumerAddressID
        {
            get
            {
                return this.consumerAddressIDField;
            }
            set
            {
                this.consumerAddressIDField = value;
            }
        }

        /// <remarks/>
        public string AddressType
        {
            get
            {
                return this.addressTypeField;
            }
            set
            {
                this.addressTypeField = value;
            }
        }

        /// <remarks/>
        public string AddressTypeInd
        {
            get
            {
                return this.addressTypeIndField;
            }
            set
            {
                this.addressTypeIndField = value;
            }
        }

        /// <remarks/>
        public string Address1
        {
            get
            {
                return this.address1Field;
            }
            set
            {
                this.address1Field = value;
            }
        }

        /// <remarks/>
        public string Address2
        {
            get
            {
                return this.address2Field;
            }
            set
            {
                this.address2Field = value;
            }
        }

        /// <remarks/>
        public string Address3
        {
            get
            {
                return this.address3Field;
            }
            set
            {
                this.address3Field = value;
            }
        }

        /// <remarks/>
        public string Address4
        {
            get
            {
                return this.address4Field;
            }
            set
            {
                this.address4Field = value;
            }
        }

        /// <remarks/>
        public string PostalCode
        {
            get
            {
                return this.postalCodeField;
            }
            set
            {
                this.postalCodeField = value;
            }
        }

        /// <remarks/>
        public string Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string LastUpdatedDate
        {
            get
            {
                return this.lastUpdatedDateField;
            }
            set
            {
                this.lastUpdatedDateField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string FirstReportedDate
        {
            get
            {
                return this.firstReportedDateField;
            }
            set
            {
                this.firstReportedDateField = value;
            }
        }
    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlType("XDSCommercialAddressHistory")]
    public partial class ConsumerXDSConsumerAddressHistory
    {

        private string displayTextField;

        private string registrationNoIDField;

        private string commercialNameField;

        private string addressTypeField;

        private string address1Field;

        private string address2Field;

        private string address3Field;

        private string address4Field;

        private string postalCodeField;

        private string createdOnDateField;

        private string occupiedDateDateField;

        private string sourceField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string RegistrationNo
        {
            get
            {
                return this.registrationNoIDField;
            }
            set
            {
                this.registrationNoIDField = value;
            }
        }

        /// <remarks/>
        public string CommercialName
        {
            get
            {
                return this.commercialNameField;
            }
            set
            {
                this.commercialNameField = value;
            }
        }

        /// <remarks/>
        public string AddressType
        {
            get
            {
                return this.addressTypeField;
            }
            set
            {
                this.addressTypeField = value;
            }
        }

        /// <remarks/>
        public string Address1
        {
            get
            {
                return this.address1Field;
            }
            set
            {
                this.address1Field = value;
            }
        }

        /// <remarks/>
        public string Address2
        {
            get
            {
                return this.address2Field;
            }
            set
            {
                this.address2Field = value;
            }
        }

        /// <remarks/>
        public string Address3
        {
            get
            {
                return this.address3Field;
            }
            set
            {
                this.address3Field = value;
            }
        }

        /// <remarks/>
        public string Address4
        {
            get
            {
                return this.address4Field;
            }
            set
            {
                this.address4Field = value;
            }
        }

        /// <remarks/>
        public string PostalCode
        {
            get
            {
                return this.postalCodeField;
            }
            set
            {
                this.postalCodeField = value;
            }
        }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public string CreatedOnDate
        {
            get
            {
                return this.createdOnDateField;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    this.createdOnDateField = DateTime.Parse(value).ToString(Consumer.DateFormat);
                }
                else
                {
                    this.createdOnDateField = ""; 
                }
                //this.createdOnDateField = value;
            }
        }

        /// <remarks/>
        public string OccupiedDate
        {
            get
            {
                return this.occupiedDateDateField;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    this.occupiedDateDateField = DateTime.Parse(value).ToString(Consumer.DateFormat);
                }
                else
                {
                    this.occupiedDateDateField = "";
                }
            }
        }

        /// <remarks/>
        public string Source
        {
            get
            {
                return this.sourceField;
            }
            set
            {
                this.sourceField = value;
            }
        }
    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlType("XDSCommercialContactHistory")]
    public partial class ConsumerXDSConsumerContactHistory
    {

        private string displayTextField;

        private string registrationNoIDField;

        private string commercialNameField;

        private string contactNumberTypeField;

        private string commercialIDField;

        private string detailField;

        private string createdOnDateField;

        private string occupiedDateDateField;

        private string sourceField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string RegistrationNo
        {
            get
            {
                return this.registrationNoIDField;
            }
            set
            {
                this.registrationNoIDField = value;
            }
        }

        /// <remarks/>
        public string CommercialName
        {
            get
            {
                return this.commercialNameField;
            }
            set
            {
                this.commercialNameField = value;
            }
        }

        /// <remarks/>
        public string ContactNumberType
        {
            get
            {
                return this.contactNumberTypeField;
            }
            set
            {
                this.contactNumberTypeField = value;
            }
        }

        /// <remarks/>
        public string commercialID
        {
            get
            {
                return this.commercialIDField;
            }
            set
            {
                this.commercialIDField = value;
            }
        }

        /// <remarks/>
        public string Detail
        {
            get
            {
                return this.detailField;
            }
            set
            {
                this.detailField = value;
            }
        }


        public string CreatedOnDate
        {
            get
            {
                return this.createdOnDateField;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    this.createdOnDateField = DateTime.Parse(value).ToString(Consumer.DateFormat);
                }
                else 
                {
                    this.createdOnDateField = ""; 
                }
                //this.createdOnDateField = value;
            }
        }

        /// <remarks/>
        public string OccupiedDate
        {
            get
            {
                return this.occupiedDateDateField;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    this.occupiedDateDateField = DateTime.Parse(value).ToString(Consumer.DateFormat);
                }
                else
                {
                    this.occupiedDateDateField = "";
                }
            }
        }

        /// <remarks/>
        public string Source
        {
            get
            {
                return this.sourceField;
            }
            set
            {
                this.sourceField = value;
            }
        }
    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [XmlType("ConsumerTelephoneHistory")]
    public partial class ConsumerConsumerTelephoneHistory
    {

        private string displayTextField;

        private string consumerTelephoneIDField;

        private string telephoneTypeField;

        private string telephoneTypeIndField;

        private string telCodeField;

        private string telNoField;

        private string telephoneNoField;

        private string emailAddressField;

        private string lastUpdatedDateField;

        private string firstReportedDateField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string ConsumerTelephoneID
        {
            get
            {
                return this.consumerTelephoneIDField;
            }
            set
            {
                this.consumerTelephoneIDField = value;
            }
        }

        /// <remarks/>
        public string TelephoneType
        {
            get
            {
                return this.telephoneTypeField;
            }
            set
            {
                this.telephoneTypeField = value;
            }
        }

        /// <remarks/>
        public string TelephoneTypeInd
        {
            get
            {
                return this.telephoneTypeIndField;
            }
            set
            {
                this.telephoneTypeIndField = value;
            }
        }

        /// <remarks/>
        public string TelCode
        {
            get
            {
                return this.telCodeField;
            }
            set
            {
                this.telCodeField = value;
            }
        }

        /// <remarks/>
        public string TelNo
        {
            get
            {
                return this.telNoField;
            }
            set
            {
                this.telNoField = value;
            }
        }

        /// <remarks/>
        public string TelephoneNo
        {
            get
            {
                return this.telephoneNoField;
            }
            set
            {
                this.telephoneNoField = value;
            }
        }

        /// <remarks/>
        public string EmailAddress
        {
            get
            {
                return this.emailAddressField;
            }
            set
            {
                this.emailAddressField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string LastUpdatedDate
        {
            get
            {
                return this.lastUpdatedDateField;
            }
            set
            {
                this.lastUpdatedDateField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string FirstReportedDate
        {
            get
            {
                return this.firstReportedDateField;
            }
            set
            {
                this.firstReportedDateField = value;
            }
        }
    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [XmlType("ConsumerEmploymentHistory")]
    public partial class ConsumerConsumerEmploymentHistory
    {

        private string displayTextField;

        private string employerDetailField;

        private string designationField;

        private string lastUpdatedDateField;

        private string firstReportedDateField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string EmployerDetail
        {
            get
            {
                return this.employerDetailField;
            }
            set
            {
                this.employerDetailField = value;
            }
        }

        /// <remarks/>
        public string Designation
        {
            get
            {
                return this.designationField;
            }
            set
            {
                this.designationField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string LastUpdatedDate
        {
            get
            {
                return this.lastUpdatedDateField;
            }
            set
            {
                this.lastUpdatedDateField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string FirstReportedDate
        {
            get
            {
                return this.firstReportedDateField;
            }
            set
            {
                this.firstReportedDateField = value;
            }
        }
    }

    /// <remarks/>
    public partial class ConsumerEmployer
    {

        private string employementDateField;

        private string employerNameField;

        private string employerTelephoneField;

      

        /// <remarks/>
        public string EmployementDate
        {
            get
            {
                return this.employementDateField;
            }
            set
            {
                this.employementDateField = value;
            }
        }

        /// <remarks/>
        public string EmployerName
        {
            get
            {
                return this.employerNameField;
            }
            set
            {
                this.employerNameField = value;
            }
        }

        /// <remarks/>
        public string EmployerTelephone
        {
            get
            {
                return this.employerTelephoneField;
            }
            set
            {
                this.employerTelephoneField = value;
            }
        }
    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [XmlType("ConsumerTelephoneLinkageHome")]
    public partial class ConsumerConsumerTelephoneLinkageHome
    {

        private string displayTextField;

        private string consumerIDField;

        private string iDNoField;

        private string passportNoField;

        private string consumerNameField;

        private string surnameField;

        private string homeTelephoneField;

        private string workTelephoneField;

        private string cellularNoField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string ConsumerID
        {
            get
            {
                return this.consumerIDField;
            }
            set
            {
                this.consumerIDField = value;
            }
        }

        /// <remarks/>
        public string IDNo
        {
            get
            {
                return this.iDNoField;
            }
            set
            {
                this.iDNoField = value;
            }
        }

        /// <remarks/>
        public string PassportNo
        {
            get
            {
                return this.passportNoField;
            }
            set
            {
                this.passportNoField = value;
            }
        }

        /// <remarks/>
        public string ConsumerName
        {
            get
            {
                return this.consumerNameField;
            }
            set
            {
                this.consumerNameField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string HomeTelephone
        {
            get
            {
                return this.homeTelephoneField;
            }
            set
            {
                this.homeTelephoneField = value;
            }
        }

        /// <remarks/>
        public string WorkTelephone
        {
            get
            {
                return this.workTelephoneField;
            }
            set
            {
                this.workTelephoneField = value;
            }
        }

        /// <remarks/>
        public string CellularNo
        {
            get
            {
                return this.cellularNoField;
            }
            set
            {
                this.cellularNoField = value;
            }
        }
    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [XmlType("ConsumerTelephoneLinkageWork")]
    public partial class ConsumerConsumerTelephoneLinkageWork
    {

        private string displayTextField;

        private string consumerIDField;

        private string iDNoField;

        private string passportNoField;

        private string consumerNameField;

        private string surnameField;

        private string homeTelephoneField;

        private string workTelephoneField;

        private string cellularNoField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string ConsumerID
        {
            get
            {
                return this.consumerIDField;
            }
            set
            {
                this.consumerIDField = value;
            }
        }

        /// <remarks/>
        public string IDNo
        {
            get
            {
                return this.iDNoField;
            }
            set
            {
                this.iDNoField = value;
            }
        }

        /// <remarks/>
        public string PassportNo
        {
            get
            {
                return this.passportNoField;
            }
            set
            {
                this.passportNoField = value;
            }
        }

        /// <remarks/>
        public string ConsumerName
        {
            get
            {
                return this.consumerNameField;
            }
            set
            {
                this.consumerNameField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string HomeTelephone
        {
            get
            {
                return this.homeTelephoneField;
            }
            set
            {
                this.homeTelephoneField = value;
            }
        }

        /// <remarks/>
        public string WorkTelephone
        {
            get
            {
                return this.workTelephoneField;
            }
            set
            {
                this.workTelephoneField = value;
            }
        }

        /// <remarks/>
        public string CellularNo
        {
            get
            {
                return this.cellularNoField;
            }
            set
            {
                this.cellularNoField = value;
            }
        }
    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [XmlType("ConsumerTelephoneLinkageCellular")]
    public partial class ConsumerConsumerTelephoneLinkageCellular
    {

        private string displayTextField;

        private string consumerIDField;

        private string iDNoField;

        private string passportNoField;

        private string consumerNameField;

        private string surnameField;

        private string homeTelephoneField;

        private string workTelephoneField;

        private string cellularNoField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string ConsumerID
        {
            get
            {
                return this.consumerIDField;
            }
            set
            {
                this.consumerIDField = value;
            }
        }

        /// <remarks/>
        public string IDNo
        {
            get
            {
                return this.iDNoField;
            }
            set
            {
                this.iDNoField = value;
            }
        }

        /// <remarks/>
        public string PassportNo
        {
            get
            {
                return this.passportNoField;
            }
            set
            {
                this.passportNoField = value;
            }
        }

        /// <remarks/>
        public string ConsumerName
        {
            get
            {
                return this.consumerNameField;
            }
            set
            {
                this.consumerNameField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string HomeTelephone
        {
            get
            {
                return this.homeTelephoneField;
            }
            set
            {
                this.homeTelephoneField = value;
            }
        }

        /// <remarks/>
        public string WorkTelephone
        {
            get
            {
                return this.workTelephoneField;
            }
            set
            {
                this.workTelephoneField = value;
            }
        }

        /// <remarks/>
        public string CellularNo
        {
            get
            {
                return this.cellularNoField;
            }
            set
            {
                this.cellularNoField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerMaritalStatusEnquiry
    {

        private string displayTextField;

        private string spouseIDnoField;

        private string idnoField;

        private string firstNameField;

        private string surnameField;

        private string spouseFirstNameField;

        private string spouseSurNameField;

        private uint consumerIDField;

        private string spouseConsumerIDField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string SpouseIDno
        {
            get
            {
                return this.spouseIDnoField;
            }
            set
            {
                this.spouseIDnoField = value;
            }
        }

        /// <remarks/>
        public string Idno
        {
            get
            {
                return this.idnoField;
            }
            set
            {
                this.idnoField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string SpouseFirstName
        {
            get
            {
                return this.spouseFirstNameField;
            }
            set
            {
                this.spouseFirstNameField = value;
            }
        }

        /// <remarks/>
        public string SpouseSurName
        {
            get
            {
                return this.spouseSurNameField;
            }
            set
            {
                this.spouseSurNameField = value;
            }
        }

        /// <remarks/>
        public uint ConsumerID
        {
            get
            {
                return this.consumerIDField;
            }
            set
            {
                this.consumerIDField = value;
            }
        }

        /// <remarks/>
        public string SpouseConsumerID
        {
            get
            {
                return this.spouseConsumerIDField;
            }
            set
            {
                this.spouseConsumerIDField = value;
            }
        }
    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [XmlType("AccountTypeLegend")]
    public partial class ConsumerAccountTypeLegend
    {

        private string displayTextField;

        private string accountTypeCodeField;

        private string accountTypeDescField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string AccountTypeCode
        {
            get
            {
                return this.accountTypeCodeField;
            }
            set
            {
                this.accountTypeCodeField = value;
            }
        }

        /// <remarks/>
        public string AccountTypeDesc
        {
            get
            {
                return this.accountTypeDescField;
            }
            set
            {
                this.accountTypeDescField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerAccountStatus
    {

        private string displayTextField;

        private string consumerAccountIDField;

        private string subscriberIDField;

        [Display(Name = "Account Number")]
        private string accountNoField;

        private string subAccountNoField;

        [Display(Name = "Date Account Opened")]
        private string accountOpenedDateField;

        [Display(Name = "Company")]
        private string subscriberNameField;

        [Display(Name = "Open Balance / Credit Limit")]
        private string creditLimitAmtField;

        [Display(Name = "Current Balance")]
        private string currentBalanceAmtField;

        [Display(Name = "Instalment Amount")]
        private string monthlyInstalmentAmtField;

        [Display(Name = "Arrears Amount")]
        private string arrearsAmtField;

        private string arrearsTypeIndField;

        [Display(Name = "Type of Account")]
        private string accountTypeField;

        [Display(Name = "Last Paid Date")]
        private string lastPaymentDateField;

        [Display(Name = "Current Status of Account")]
        private string statusCodeDescField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string ConsumerAccountID
        {
            get
            {
                return this.consumerAccountIDField;
            }
            set
            {
                this.consumerAccountIDField = value;
            }
        }

        /// <remarks/>
        public string SubscriberID
        {
            get
            {
                return this.subscriberIDField;
            }
            set
            {
                this.subscriberIDField = value;
            }
        }

        /// <remarks/>
        public string AccountNo
        {
            get
            {
                return this.accountNoField;
            }
            set
            {
                this.accountNoField = value;
            }
        }

        /// <remarks/>
        public string SubAccountNo
        {
            get
            {
                return this.subAccountNoField;
            }
            set
            {
                this.subAccountNoField = value;
            }
        }

        /// <remarks/>
        public string AccountOpenedDate
        {
            get
            {
                return this.accountOpenedDateField;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    this.accountOpenedDateField = DateTime.Parse(value).ToString(Consumer.DateFormat);
                }
                    else 
                {
                    this.accountOpenedDateField = ""; 
                }
            }
        }

        /// <remarks/>
        public string SubscriberName
        {
            get
            {
                return this.subscriberNameField;
            }
            set
            {
                this.subscriberNameField = value;
            }
        }

        /// <remarks/>
        public string CreditLimitAmt
        {
            get
            {
                return this.creditLimitAmtField;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    this.creditLimitAmtField =  General.FormatNumbers(value);
                }
                    else 
                {
                    this.creditLimitAmtField = ""; 
                }
            }
        }

        /// <remarks/>
        public string CurrentBalanceAmt
        {
            get
            {
                return this.currentBalanceAmtField;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    this.currentBalanceAmtField = General.FormatNumbers(value);
                }
                else
                {
                    this.currentBalanceAmtField = "";
                }
            }
        }

        /// <remarks/>
        public string MonthlyInstalmentAmt
        {
            get
            {
                return this.monthlyInstalmentAmtField;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    this.monthlyInstalmentAmtField = General.FormatNumbers(value);
                }
                  else 
                {
                    this.monthlyInstalmentAmtField = ""; 
                }
            }
        }

        /// <remarks/>
        public string ArrearsAmt
        {
            get
            {
                return this.arrearsAmtField;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    this.arrearsAmtField = General.FormatNumbers(value);
                }
                else
                {
                    this.arrearsAmtField = "";
                }
            }
        }

        /// <remarks/>
        public string ArrearsTypeInd
        {
            get
            {
                return this.arrearsTypeIndField;
            }
            set
            {
                this.arrearsTypeIndField = value;
            }
        }

        /// <remarks/>
        public string AccountType
        {
            get
            {
                return this.accountTypeField;
            }
            set
            {
                this.accountTypeField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string LastPaymentDate
        {
            get
            {
                return this.lastPaymentDateField.ToString();
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    this.lastPaymentDateField = DateTime.Parse(value).ToString(Consumer.DateFormat);
                }
                else
                {
                    this.lastPaymentDateField = "";
                }
            }
        }

        /// <remarks/>
        public string StatusCodeDesc
        {
            get
            {
                return this.statusCodeDescField;
            }
            set
            {
                this.statusCodeDescField = value;
            }
        }
    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [XmlType("Consumer24MonthlyPaymentHeader")]
    public partial class ConsumerConsumer24MonthlyPaymentHeader
    {

        private string displayTextField;

        private string companyField;

        private string m24Field;

        private string m23Field;

        private string m22Field;

        private string m21Field;

        private string m20Field;

        private string m19Field;

        private string m18Field;

        private string m17Field;

        private string m16Field;

        private string m15Field;

        private string m14Field;

        private string m13Field;

        private string m12Field;

        private string m11Field;

        private string m10Field;

        private string m09Field;

        private string m08Field;

        private string m07Field;

        private string m06Field;

        private string m05Field;

        private string m04Field;

        private string m03Field;

        private string m02Field;

        private string m01Field;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string Company
        {
            get
            {
                return this.companyField;
            }
            set
            {
                this.companyField = value;
            }
        }

        /// <remarks/>
        public string M24
        {
            get
            {
                return this.m24Field;
            }
            set
            {
                this.m24Field = value;
            }
        }

        /// <remarks/>
        public string M23
        {
            get
            {
                return this.m23Field;
            }
            set
            {
                this.m23Field = value;
            }
        }

        /// <remarks/>
        public string M22
        {
            get
            {
                return this.m22Field;
            }
            set
            {
                this.m22Field = value;
            }
        }

        /// <remarks/>
        public string M21
        {
            get
            {
                return this.m21Field;
            }
            set
            {
                this.m21Field = value;
            }
        }

        /// <remarks/>
        public string M20
        {
            get
            {
                return this.m20Field;
            }
            set
            {
                this.m20Field = value;
            }
        }

        /// <remarks/>
        public string M19
        {
            get
            {
                return this.m19Field;
            }
            set
            {
                this.m19Field = value;
            }
        }

        /// <remarks/>
        public string M18
        {
            get
            {
                return this.m18Field;
            }
            set
            {
                this.m18Field = value;
            }
        }

        /// <remarks/>
        public string M17
        {
            get
            {
                return this.m17Field;
            }
            set
            {
                this.m17Field = value;
            }
        }

        /// <remarks/>
        public string M16
        {
            get
            {
                return this.m16Field;
            }
            set
            {
                this.m16Field = value;
            }
        }

        /// <remarks/>
        public string M15
        {
            get
            {
                return this.m15Field;
            }
            set
            {
                this.m15Field = value;
            }
        }

        /// <remarks/>
        public string M14
        {
            get
            {
                return this.m14Field;
            }
            set
            {
                this.m14Field = value;
            }
        }

        /// <remarks/>
        public string M13
        {
            get
            {
                return this.m13Field;
            }
            set
            {
                this.m13Field = value;
            }
        }

        /// <remarks/>
        public string M12
        {
            get
            {
                return this.m12Field;
            }
            set
            {
                this.m12Field = value;
            }
        }

        /// <remarks/>
        public string M11
        {
            get
            {
                return this.m11Field;
            }
            set
            {
                this.m11Field = value;
            }
        }

        /// <remarks/>
        public string M10
        {
            get
            {
                return this.m10Field;
            }
            set
            {
                this.m10Field = value;
            }
        }

        /// <remarks/>
        public string M09
        {
            get
            {
                return this.m09Field;
            }
            set
            {
                this.m09Field = value;
            }
        }

        /// <remarks/>
        public string M08
        {
            get
            {
                return this.m08Field;
            }
            set
            {
                this.m08Field = value;
            }
        }

        /// <remarks/>
        public string M07
        {
            get
            {
                return this.m07Field;
            }
            set
            {
                this.m07Field = value;
            }
        }

        /// <remarks/>
        public string M06
        {
            get
            {
                return this.m06Field;
            }
            set
            {
                this.m06Field = value;
            }
        }

        /// <remarks/>
        public string M05
        {
            get
            {
                return this.m05Field;
            }
            set
            {
                this.m05Field = value;
            }
        }

        /// <remarks/>
        public string M04
        {
            get
            {
                return this.m04Field;
            }
            set
            {
                this.m04Field = value;
            }
        }

        /// <remarks/>
        public string M03
        {
            get
            {
                return this.m03Field;
            }
            set
            {
                this.m03Field = value;
            }
        }

        /// <remarks/>
        public string M02
        {
            get
            {
                return this.m02Field;
            }
            set
            {
                this.m02Field = value;
            }
        }

        /// <remarks/>
        public string M01
        {
            get
            {
                return this.m01Field;
            }
            set
            {
                this.m01Field = value;
            }
        }
    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [XmlType("Consumer24MonthlyPayment")]
    public partial class ConsumerConsumer24MonthlyPayment
    {

        private string displayTextField;

        private System.DateTime accountOpenedDateField;

        private string subscriberNameField;

        private string accountNoField;

        private object subAccountNoField;

        private string m24Field;

        private string m23Field;

        private string m22Field;

        private string m21Field;

        private string m20Field;

        private string m19Field;

        private string m18Field;

        private string m17Field;

        private string m16Field;

        private string m15Field;

        private string m14Field;

        private string m13Field;

        private string m12Field;

        private string m11Field;

        private string m10Field;

        private string m09Field;

        private string m08Field;

        private string m07Field;

        private string m06Field;

        private string m05Field;

        private string m04Field;

        private string m03Field;

        private string m02Field;

        private string m01Field;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public System.DateTime AccountOpenedDate
        {
            get
            {
                return this.accountOpenedDateField;
            }
            set
            {
               this.accountOpenedDateField = value;
            }
        }

        /// <remarks/>
        public string SubscriberName
        {
            get
            {
                return this.subscriberNameField;
            }
            set
            {
                this.subscriberNameField = value;
            }
        }

        /// <remarks/>
        public string AccountNo
        {
            get
            {
                return this.accountNoField;
            }
            set
            {
                this.accountNoField = value;
            }
        }

        /// <remarks/>
        public object SubAccountNo
        {
            get
            {
                return this.subAccountNoField;
            }
            set
            {
                this.subAccountNoField = value;
            }
        }

        /// <remarks/>
        public string M24
        {
            get
            {
                return this.m24Field;
            }
            set
            {
                this.m24Field = value;
            }
        }

        /// <remarks/>
        public string M23
        {
            get
            {
                return this.m23Field;
            }
            set
            {
                this.m23Field = value;
            }
        }

        /// <remarks/>
        public string M22
        {
            get
            {
                return this.m22Field;
            }
            set
            {
                this.m22Field = value;
            }
        }

        /// <remarks/>
        public string M21
        {
            get
            {
                return this.m21Field;
            }
            set
            {
                this.m21Field = value;
            }
        }

        /// <remarks/>
        public string M20
        {
            get
            {
                return this.m20Field;
            }
            set
            {
                this.m20Field = value;
            }
        }

        /// <remarks/>
        public string M19
        {
            get
            {
                return this.m19Field;
            }
            set
            {
                this.m19Field = value;
            }
        }

        /// <remarks/>
        public string M18
        {
            get
            {
                return this.m18Field;
            }
            set
            {
                this.m18Field = value;
            }
        }

        /// <remarks/>
        public string M17
        {
            get
            {
                return this.m17Field;
            }
            set
            {
                this.m17Field = value;
            }
        }

        /// <remarks/>
        public string M16
        {
            get
            {
                return this.m16Field;
            }
            set
            {
                this.m16Field = value;
            }
        }

        /// <remarks/>
        public string M15
        {
            get
            {
                return this.m15Field;
            }
            set
            {
                this.m15Field = value;
            }
        }

        /// <remarks/>
        public string M14
        {
            get
            {
                return this.m14Field;
            }
            set
            {
                this.m14Field = value;
            }
        }

        /// <remarks/>
        public string M13
        {
            get
            {
                return this.m13Field;
            }
            set
            {
                this.m13Field = value;
            }
        }

        /// <remarks/>
        public string M12
        {
            get
            {
                return this.m12Field;
            }
            set
            {
                this.m12Field = value;
            }
        }

        /// <remarks/>
        public string M11
        {
            get
            {
                return this.m11Field;
            }
            set
            {
                this.m11Field = value;
            }
        }

        /// <remarks/>
        public string M10
        {
            get
            {
                return this.m10Field;
            }
            set
            {
                this.m10Field = value;
            }
        }

        /// <remarks/>
        public string M09
        {
            get
            {
                return this.m09Field;
            }
            set
            {
                this.m09Field = value;
            }
        }

        /// <remarks/>
        public string M08
        {
            get
            {
                return this.m08Field;
            }
            set
            {
                this.m08Field = value;
            }
        }

        /// <remarks/>
        public string M07
        {
            get
            {
                return this.m07Field;
            }
            set
            {
                this.m07Field = value;
            }
        }

        /// <remarks/>
        public string M06
        {
            get
            {
                return this.m06Field;
            }
            set
            {
                this.m06Field = value;
            }
        }

        /// <remarks/>
        public string M05
        {
            get
            {
                return this.m05Field;
            }
            set
            {
                this.m05Field = value;
            }
        }

        /// <remarks/>
        public string M04
        {
            get
            {
                return this.m04Field;
            }
            set
            {
                this.m04Field = value;
            }
        }

        /// <remarks/>
        public string M03
        {
            get
            {
                return this.m03Field;
            }
            set
            {
                this.m03Field = value;
            }
        }

        /// <remarks/>
        public string M02
        {
            get
            {
                return this.m02Field;
            }
            set
            {
                this.m02Field = value;
            }
        }

        /// <remarks/>
        public string M01
        {
            get
            {
                return this.m01Field;
            }
            set
            {
                this.m01Field = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerDefinition
    {

        private string displayTextField;

        private string definitionCodeField;

        private string definitionDescField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string DefinitionCode
        {
            get
            {
                return this.definitionCodeField;
            }
            set
            {
                this.definitionCodeField = value;
            }
        }

        /// <remarks/>
        public string DefinitionDesc
        {
            get
            {
                return this.definitionDescField;
            }
            set
            {
                this.definitionDescField = value;
            }
        }
    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [XmlType("NLRAccountTypeLegend")]
    public partial class ConsumerNLRAccountTypeLegend
    {

        private string displayTextField;

        private string accountTypeCodeField;

        private string accountTypeDescField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string AccountTypeCode
        {
            get
            {
                return this.accountTypeCodeField;
            }
            set
            {
                this.accountTypeCodeField = value;
            }
        }

        /// <remarks/>
        public string AccountTypeDesc
        {
            get
            {
                return this.accountTypeDescField;
            }
            set
            {
                this.accountTypeDescField = value;
            }
        }
    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [XmlType("ConsumerNLRAccountStatus")]
    public partial class ConsumerConsumerNLRAccountStatus
    {

        private string displayTextField;

        private string consumerAccountIDField;

        private string subscriberIDField;

        private string accountNoField;

        private string subAccountNoField;

        private string accountOpenedDateField;

        private string subscriberNameField;

        private string creditLimitAmtField;

        private string currentBalanceAmtField;

        private string monthlyInstalmentAmtField;

        private string arrearsAmtField;

        private string arrearsTypeIndField;

        private string accountTypeField;

        private string lastPaymentDateField;

        private string statusCodeField;

        private string statusCodeDescField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string ConsumerAccountID
        {
            get
            {
                return this.consumerAccountIDField;
            }
            set
            {
                this.consumerAccountIDField = value;
            }
        }

        /// <remarks/>
        public string SubscriberID
        {
            get
            {
                return this.subscriberIDField;
            }
            set
            {
                this.subscriberIDField = value;
            }
        }

        /// <remarks/>
        public string AccountNo
        {
            get
            {
                return this.accountNoField;
            }
            set
            {
                this.accountNoField = value;
            }
        }

        /// <remarks/>
        public string SubAccountNo
        {
            get
            {
                return this.subAccountNoField;
            }
            set
            {
                this.subAccountNoField = value;
            }
        }

        /// <remarks/>
        public string AccountOpenedDate
        {
            get
            {
                return this.accountOpenedDateField;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    this.accountOpenedDateField = DateTime.Parse(value).ToString(Consumer.DateFormat);
                }
                else
                {
                    this.accountOpenedDateField = "";
                }
            }
        }

        /// <remarks/>
        public string SubscriberName
        {
            get
            {
                return this.subscriberNameField;
            }
            set
            {
                this.subscriberNameField = value;
            }
        }

        /// <remarks/>
        public string CreditLimitAmt
        {
            get
            {
                return this.creditLimitAmtField;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    this.creditLimitAmtField = General.FormatNumbers(value);
                }
               else
                {
                    this.creditLimitAmtField = "";
                }
            }
        }

        /// <remarks/>
        public string CurrentBalanceAmt
        {
            get
            {
                return this.currentBalanceAmtField;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    this.currentBalanceAmtField = General.FormatNumbers(value);
                }
               else
                {
                    this.currentBalanceAmtField = "";
                }
            }
        }

        /// <remarks/>
        public string MonthlyInstalmentAmt
        {
            get
            {
                return this.monthlyInstalmentAmtField;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    this.monthlyInstalmentAmtField =  General.FormatNumbers(value);
                }
                else
                {
                    this.monthlyInstalmentAmtField = "";
                }
            }
        }

        /// <remarks/>
        public string ArrearsAmt
        {
            get
            {
                return this.arrearsAmtField;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    this.arrearsAmtField =  General.FormatNumbers(value);
                }
                else
                {
                    this.arrearsAmtField = "";
                }
            }
        }

        /// <remarks/>
        public string ArrearsTypeInd
        {
            get
            {
                return this.arrearsTypeIndField;
            }
            set
            {
                this.arrearsTypeIndField = value;
            }
        }

        /// <remarks/>
        public string AccountType
        {
            get
            {
                return this.accountTypeField;
            }
            set
            {
                this.accountTypeField = value;
            }
        }
        public string LastPaymentDate
        {
            get
            {
                return this.lastPaymentDateField;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    this.lastPaymentDateField = DateTime.Parse(value).ToString(Consumer.DateFormat);
                }
                else
                {
                    this.lastPaymentDateField = "";
                }
            }
        }

        /// <remarks/>
        public string StatusCode
        {
            get
            {
                return this.statusCodeField;
            }
            set
            {
                this.statusCodeField = value;
            }
        }

        /// <remarks/>
        public string StatusCodeDesc
        {
            get
            {
                return this.statusCodeDescField;
            }
            set
            {
                this.statusCodeDescField = value;
            }
        }
    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [XmlType("ConsumerNLR24MonthlyPaymentHeader")]
    public partial class ConsumerConsumerNLR24MonthlyPaymentHeader
    {

        private string displayTextField;

        private string companyField;

        private string m24Field;

        private string m23Field;

        private string m22Field;

        private string m21Field;

        private string m20Field;

        private string m19Field;

        private string m18Field;

        private string m17Field;

        private string m16Field;

        private string m15Field;

        private string m14Field;

        private string m13Field;

        private string m12Field;

        private string m11Field;

        private string m10Field;

        private string m09Field;

        private string m08Field;

        private string m07Field;

        private string m06Field;

        private string m05Field;

        private string m04Field;

        private string m03Field;

        private string m02Field;

        private string m01Field;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string Company
        {
            get
            {
                return this.companyField;
            }
            set
            {
                this.companyField = value;
            }
        }

        /// <remarks/>
        public string M24
        {
            get
            {
                return this.m24Field;
            }
            set
            {
                this.m24Field = value;
            }
        }

        /// <remarks/>
        public string M23
        {
            get
            {
                return this.m23Field;
            }
            set
            {
                this.m23Field = value;
            }
        }

        /// <remarks/>
        public string M22
        {
            get
            {
                return this.m22Field;
            }
            set
            {
                this.m22Field = value;
            }
        }

        /// <remarks/>
        public string M21
        {
            get
            {
                return this.m21Field;
            }
            set
            {
                this.m21Field = value;
            }
        }

        /// <remarks/>
        public string M20
        {
            get
            {
                return this.m20Field;
            }
            set
            {
                this.m20Field = value;
            }
        }

        /// <remarks/>
        public string M19
        {
            get
            {
                return this.m19Field;
            }
            set
            {
                this.m19Field = value;
            }
        }

        /// <remarks/>
        public string M18
        {
            get
            {
                return this.m18Field;
            }
            set
            {
                this.m18Field = value;
            }
        }

        /// <remarks/>
        public string M17
        {
            get
            {
                return this.m17Field;
            }
            set
            {
                this.m17Field = value;
            }
        }

        /// <remarks/>
        public string M16
        {
            get
            {
                return this.m16Field;
            }
            set
            {
                this.m16Field = value;
            }
        }

        /// <remarks/>
        public string M15
        {
            get
            {
                return this.m15Field;
            }
            set
            {
                this.m15Field = value;
            }
        }

        /// <remarks/>
        public string M14
        {
            get
            {
                return this.m14Field;
            }
            set
            {
                this.m14Field = value;
            }
        }

        /// <remarks/>
        public string M13
        {
            get
            {
                return this.m13Field;
            }
            set
            {
                this.m13Field = value;
            }
        }

        /// <remarks/>
        public string M12
        {
            get
            {
                return this.m12Field;
            }
            set
            {
                this.m12Field = value;
            }
        }

        /// <remarks/>
        public string M11
        {
            get
            {
                return this.m11Field;
            }
            set
            {
                this.m11Field = value;
            }
        }

        /// <remarks/>
        public string M10
        {
            get
            {
                return this.m10Field;
            }
            set
            {
                this.m10Field = value;
            }
        }

        /// <remarks/>
        public string M09
        {
            get
            {
                return this.m09Field;
            }
            set
            {
                this.m09Field = value;
            }
        }

        /// <remarks/>
        public string M08
        {
            get
            {
                return this.m08Field;
            }
            set
            {
                this.m08Field = value;
            }
        }

        /// <remarks/>
        public string M07
        {
            get
            {
                return this.m07Field;
            }
            set
            {
                this.m07Field = value;
            }
        }

        /// <remarks/>
        public string M06
        {
            get
            {
                return this.m06Field;
            }
            set
            {
                this.m06Field = value;
            }
        }

        /// <remarks/>
        public string M05
        {
            get
            {
                return this.m05Field;
            }
            set
            {
                this.m05Field = value;
            }
        }

        /// <remarks/>
        public string M04
        {
            get
            {
                return this.m04Field;
            }
            set
            {
                this.m04Field = value;
            }
        }

        /// <remarks/>
        public string M03
        {
            get
            {
                return this.m03Field;
            }
            set
            {
                this.m03Field = value;
            }
        }

        /// <remarks/>
        public string M02
        {
            get
            {
                return this.m02Field;
            }
            set
            {
                this.m02Field = value;
            }
        }

        /// <remarks/>
        public string M01
        {
            get
            {
                return this.m01Field;
            }
            set
            {
                this.m01Field = value;
            }
        }
    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [XmlType("ConsumerNLR24MonthlyPayment")]
    public partial class ConsumerConsumerNLR24MonthlyPayment
    {

        private string displayTextField;

        private System.DateTime accountOpenedDateField;

        private string subscriberNameField;

        private string accountNoField;

        private object subAccountNoField;

        private string m24Field;

        private string m23Field;

        private string m22Field;

        private string m21Field;

        private string m20Field;

        private string m19Field;

        private string m18Field;

        private string m17Field;

        private string m16Field;

        private string m15Field;

        private string m14Field;

        private string m13Field;

        private string m12Field;

        private string m11Field;

        private string m10Field;

        private string m09Field;

        private string m08Field;

        private string m07Field;

        private string m06Field;

        private string m05Field;

        private string m04Field;

        private string m03Field;

        private string m02Field;

        private string m01Field;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public System.DateTime AccountOpenedDate
        {
            get
            {
                return this.accountOpenedDateField;
            }
            set
            {
               this.accountOpenedDateField = value;
            }
        }

        /// <remarks/>
        public string SubscriberName
        {
            get
            {
                return this.subscriberNameField;
            }
            set
            {
                this.subscriberNameField = value;
            }
        }

        /// <remarks/>
        public string AccountNo
        {
            get
            {
                return this.accountNoField;
            }
            set
            {
                this.accountNoField = value;
            }
        }

        /// <remarks/>
        public object SubAccountNo
        {
            get
            {
                return this.subAccountNoField;
            }
            set
            {
                this.subAccountNoField = value;
            }
        }

        /// <remarks/>
        public string M24
        {
            get
            {
                return this.m24Field;
            }
            set
            {
                this.m24Field = value;
            }
        }

        /// <remarks/>
        public string M23
        {
            get
            {
                return this.m23Field;
            }
            set
            {
                this.m23Field = value;
            }
        }

        /// <remarks/>
        public string M22
        {
            get
            {
                return this.m22Field;
            }
            set
            {
                this.m22Field = value;
            }
        }

        /// <remarks/>
        public string M21
        {
            get
            {
                return this.m21Field;
            }
            set
            {
                this.m21Field = value;
            }
        }

        /// <remarks/>
        public string M20
        {
            get
            {
                return this.m20Field;
            }
            set
            {
                this.m20Field = value;
            }
        }

        /// <remarks/>
        public string M19
        {
            get
            {
                return this.m19Field;
            }
            set
            {
                this.m19Field = value;
            }
        }

        /// <remarks/>
        public string M18
        {
            get
            {
                return this.m18Field;
            }
            set
            {
                this.m18Field = value;
            }
        }

        /// <remarks/>
        public string M17
        {
            get
            {
                return this.m17Field;
            }
            set
            {
                this.m17Field = value;
            }
        }

        /// <remarks/>
        public string M16
        {
            get
            {
                return this.m16Field;
            }
            set
            {
                this.m16Field = value;
            }
        }

        /// <remarks/>
        public string M15
        {
            get
            {
                return this.m15Field;
            }
            set
            {
                this.m15Field = value;
            }
        }

        /// <remarks/>
        public string M14
        {
            get
            {
                return this.m14Field;
            }
            set
            {
                this.m14Field = value;
            }
        }

        /// <remarks/>
        public string M13
        {
            get
            {
                return this.m13Field;
            }
            set
            {
                this.m13Field = value;
            }
        }

        /// <remarks/>
        public string M12
        {
            get
            {
                return this.m12Field;
            }
            set
            {
                this.m12Field = value;
            }
        }

        /// <remarks/>
        public string M11
        {
            get
            {
                return this.m11Field;
            }
            set
            {
                this.m11Field = value;
            }
        }

        /// <remarks/>
        public string M10
        {
            get
            {
                return this.m10Field;
            }
            set
            {
                this.m10Field = value;
            }
        }

        /// <remarks/>
        public string M09
        {
            get
            {
                return this.m09Field;
            }
            set
            {
                this.m09Field = value;
            }
        }

        /// <remarks/>
        public string M08
        {
            get
            {
                return this.m08Field;
            }
            set
            {
                this.m08Field = value;
            }
        }

        /// <remarks/>
        public string M07
        {
            get
            {
                return this.m07Field;
            }
            set
            {
                this.m07Field = value;
            }
        }

        /// <remarks/>
        public string M06
        {
            get
            {
                return this.m06Field;
            }
            set
            {
                this.m06Field = value;
            }
        }

        /// <remarks/>
        public string M05
        {
            get
            {
                return this.m05Field;
            }
            set
            {
                this.m05Field = value;
            }
        }

        /// <remarks/>
        public string M04
        {
            get
            {
                return this.m04Field;
            }
            set
            {
                this.m04Field = value;
            }
        }

        /// <remarks/>
        public string M03
        {
            get
            {
                return this.m03Field;
            }
            set
            {
                this.m03Field = value;
            }
        }

        /// <remarks/>
        public string M02
        {
            get
            {
                return this.m02Field;
            }
            set
            {
                this.m02Field = value;
            }
        }

        /// <remarks/>
        public string M01
        {
            get
            {
                return this.m01Field;
            }
            set
            {
                this.m01Field = value;
            }
        }
    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [XmlType("ConsumerNLRDefinition")]
    public partial class ConsumerConsumerNLRDefinition
    {

        private string displayTextField;

        private string definitionCodeField;

        private string definitionDescField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string DefinitionCode
        {
            get
            {
                return this.definitionCodeField;
            }
            set
            {
                this.definitionCodeField = value;
            }
        }

        /// <remarks/>
        public string DefinitionDesc
        {
            get
            {
                return this.definitionDescField;
            }
            set
            {
                this.definitionDescField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerCPANLRDebtSummary
    {

        private string displayTextField;

        private string totalMonthlyInstallmentCPAField;

        private string totalOutStandingDebtCPAField;

        private string noOFActiveAccountsCPAField;

        private string noOfAccountInGoodStandingCPAField;

        private string noOfAccountInBadStandingCPAField;

        private string totalArrearAmountCPAField;

        private string totalAdverseAmountCPAField;

        private string noOfAccountsOpenedinLast45DaysCPAField;

        private string noOfPaidUpOrClosedAccountsCPAField;

        private string noOfEnquiriesLast90DaysOWNCPAField;

        private string noOfEnquiriesLast90DaysOTHCPAField;

        private string highestMonthsinArrearsCPAField;

        private string totalMonthlyInstallmentNLRField;

        private string totalOutStandingDebtNLRField;

        private string noOFActiveAccountsNLRField;

        private string noOfAccountInGoodStandingNLRField;

        private string noOfAccountInBadStandingNLRField;

        private string totalArrearAmountNLRField;

        private string totalAdverseAmountNLRField;

        private string noOfAccountsOpenedinLast45DaysNLRField;

        private string noOfPaidUpOrClosedAccountsNLRField;

        private string noOfEnquiriesLast90DaysOWNNLRField;

        private string noOfEnquiriesLast90DaysOTHNLRField;

        private string highestMonthsinArrearsNLRField;

        private string totalMonthlyInstallmentField;

        private string totalOutStandingDebtField;

        private string noOFActiveAccountsField;

        private string noOfAccountInGoodStandingField;

        private string noOfAccountInBadStandingField;

        private string totalArrearAmountField;

        private string highestMonthsInArrearsField;

        private string noOfAccountsOpenedinLast45DaysField;

        private string noOfPaidUpOrClosedAccountsField;

        private string noOfEnquiriesLast90DaysOWNField;

        private string noOfEnquiriesLast90DaysOTHField;

        private string judgementCountField;

        private string totalJudgmentAmtField;

        private string mostRecentJudgmentDateField;

        private string courtNoticeCountField;

        private string totalCourtNoticeAmtField;

        //private System.DateTime mostRecentCourtNoticeDateField;
        private string mostRecentCourtNoticeDateField;

        private string noofAccountdefaultsField;

        private string totalAdverseAmtField;

        private string mostRecentAdverseDateField;

        private string defaultListingCountField;

        private string defaultListingAmtField;

        //private System.DateTime recentDefaultListingDateField;
        private string recentDefaultListingDateField;

        private string noOfEnqinLast24MonthsField;

        private string mostRecentEnqDateLast24MonthsField;

        private string debtReviewStatusField;

        private string disputeMessageField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string TotalMonthlyInstallmentCPA
        {
            get
            {
                return this.totalMonthlyInstallmentCPAField;
            }
            set
            {
                this.totalMonthlyInstallmentCPAField = value;
            }
        }

        /// <remarks/>
        public string TotalOutStandingDebtCPA
        {
            get
            {
                return this.totalOutStandingDebtCPAField;
            }
            set
            {
                this.totalOutStandingDebtCPAField = value;
            }
        }

        /// <remarks/>
        public string NoOFActiveAccountsCPA
        {
            get
            {
                return this.noOFActiveAccountsCPAField;
            }
            set
            {
                this.noOFActiveAccountsCPAField = value;
            }
        }

        /// <remarks/>
        public string NoOfAccountInGoodStandingCPA
        {
            get
            {
                return this.noOfAccountInGoodStandingCPAField;
            }
            set
            {
                this.noOfAccountInGoodStandingCPAField = value;
            }
        }

        /// <remarks/>
        public string NoOfAccountInBadStandingCPA
        {
            get
            {
                return this.noOfAccountInBadStandingCPAField;
            }
            set
            {
                this.noOfAccountInBadStandingCPAField = value;
            }
        }

        /// <remarks/>
        public string TotalArrearAmountCPA
        {
            get
            {
                return this.totalArrearAmountCPAField;
            }
            set
            {
                this.totalArrearAmountCPAField = value;
            }
        }

        /// <remarks/>
        public string TotalAdverseAmountCPA
        {
            get
            {
                return this.totalAdverseAmountCPAField;
            }
            set
            {
                this.totalAdverseAmountCPAField = value;
            }
        }

        /// <remarks/>
        public string NoOfAccountsOpenedinLast45DaysCPA
        {
            get
            {
                return this.noOfAccountsOpenedinLast45DaysCPAField;
            }
            set
            {
                this.noOfAccountsOpenedinLast45DaysCPAField = value;
            }
        }

        /// <remarks/>
        public string NoOfPaidUpOrClosedAccountsCPA
        {
            get
            {
                return this.noOfPaidUpOrClosedAccountsCPAField;
            }
            set
            {
                this.noOfPaidUpOrClosedAccountsCPAField = value;
            }
        }

        /// <remarks/>
        public string NoOfEnquiriesLast90DaysOWNCPA
        {
            get
            {
                return this.noOfEnquiriesLast90DaysOWNCPAField;
            }
            set
            {
                this.noOfEnquiriesLast90DaysOWNCPAField = value;
            }
        }

        /// <remarks/>
        public string NoOfEnquiriesLast90DaysOTHCPA
        {
            get
            {
                return this.noOfEnquiriesLast90DaysOTHCPAField;
            }
            set
            {
                this.noOfEnquiriesLast90DaysOTHCPAField = value;
            }
        }

        /// <remarks/>
        public string HighestMonthsinArrearsCPA
        {
            get
            {
                return this.highestMonthsinArrearsCPAField;
            }
            set
            {
                this.highestMonthsinArrearsCPAField = value;
            }
        }

        /// <remarks/>
        public string TotalMonthlyInstallmentNLR
        {
            get
            {
                return this.totalMonthlyInstallmentNLRField;
            }
            set
            {
                this.totalMonthlyInstallmentNLRField = value;
            }
        }

        /// <remarks/>
        public string TotalOutStandingDebtNLR
        {
            get
            {
                return this.totalOutStandingDebtNLRField;
            }
            set
            {
                this.totalOutStandingDebtNLRField = value;
            }
        }

        /// <remarks/>
        public string NoOFActiveAccountsNLR
        {
            get
            {
                return this.noOFActiveAccountsNLRField;
            }
            set
            {
                this.noOFActiveAccountsNLRField = value;
            }
        }

        /// <remarks/>
        public string NoOfAccountInGoodStandingNLR
        {
            get
            {
                return this.noOfAccountInGoodStandingNLRField;
            }
            set
            {
                this.noOfAccountInGoodStandingNLRField = value;
            }
        }

        /// <remarks/>
        public string NoOfAccountInBadStandingNLR
        {
            get
            {
                return this.noOfAccountInBadStandingNLRField;
            }
            set
            {
                this.noOfAccountInBadStandingNLRField = value;
            }
        }

        /// <remarks/>
        public string TotalArrearAmountNLR
        {
            get
            {
                return this.totalArrearAmountNLRField;
            }
            set
            {
                this.totalArrearAmountNLRField = value;
            }
        }

        /// <remarks/>
        public string TotalAdverseAmountNLR
        {
            get
            {
                return this.totalAdverseAmountNLRField;
            }
            set
            {
                this.totalAdverseAmountNLRField = value;
            }
        }

        /// <remarks/>
        public string NoOfAccountsOpenedinLast45DaysNLR
        {
            get
            {
                return this.noOfAccountsOpenedinLast45DaysNLRField;
            }
            set
            {
                this.noOfAccountsOpenedinLast45DaysNLRField = value;
            }
        }

        /// <remarks/>
        public string NoOfPaidUpOrClosedAccountsNLR
        {
            get
            {
                return this.noOfPaidUpOrClosedAccountsNLRField;
            }
            set
            {
                this.noOfPaidUpOrClosedAccountsNLRField = value;
            }
        }

        /// <remarks/>
        public string NoOfEnquiriesLast90DaysOWNNLR
        {
            get
            {
                return this.noOfEnquiriesLast90DaysOWNNLRField;
            }
            set
            {
                this.noOfEnquiriesLast90DaysOWNNLRField = value;
            }
        }

        /// <remarks/>
        public string NoOfEnquiriesLast90DaysOTHNLR
        {
            get
            {
                return this.noOfEnquiriesLast90DaysOTHNLRField;
            }
            set
            {
                this.noOfEnquiriesLast90DaysOTHNLRField = value;
            }
        }

        /// <remarks/>
        public string HighestMonthsinArrearsNLR
        {
            get
            {
                return this.highestMonthsinArrearsNLRField;
            }
            set
            {
                this.highestMonthsinArrearsNLRField = value;
            }
        }

        /// <remarks/>
        public string TotalMonthlyInstallment
        {
            get
            {
                return this.totalMonthlyInstallmentField;
            }
            set
            {
                this.totalMonthlyInstallmentField = value;
            }
        }

        /// <remarks/>
        public string TotalOutStandingDebt
        {
            get
            {
                return this.totalOutStandingDebtField;
            }
            set
            {
                this.totalOutStandingDebtField = value;
            }
        }

        /// <remarks/>
        public string NoOFActiveAccounts
        {
            get
            {
                return this.noOFActiveAccountsField;
            }
            set
            {
                this.noOFActiveAccountsField = value;
            }
        }

        /// <remarks/>
        public string NoOfAccountInGoodStanding
        {
            get
            {
                return this.noOfAccountInGoodStandingField;
            }
            set
            {
                this.noOfAccountInGoodStandingField = value;
            }
        }

        /// <remarks/>
        public string NoOfAccountInBadStanding
        {
            get
            {
                return this.noOfAccountInBadStandingField;
            }
            set
            {
                this.noOfAccountInBadStandingField = value;
            }
        }

        /// <remarks/>
        public string TotalArrearAmount
        {
            get
            {
                return this.totalArrearAmountField;
            }
            set
            {
                this.totalArrearAmountField = value;
            }
        }

        /// <remarks/>
        public string HighestMonthsInArrears
        {
            get
            {
                return this.highestMonthsInArrearsField;
            }
            set
            {
                this.highestMonthsInArrearsField = value;
            }
        }

        /// <remarks/>
        public string NoOfAccountsOpenedinLast45Days
        {
            get
            {
                return this.noOfAccountsOpenedinLast45DaysField;
            }
            set
            {
                this.noOfAccountsOpenedinLast45DaysField = value;
            }
        }

        /// <remarks/>
        public string NoOfPaidUpOrClosedAccounts
        {
            get
            {
                return this.noOfPaidUpOrClosedAccountsField;
            }
            set
            {
                this.noOfPaidUpOrClosedAccountsField = value;
            }
        }

        /// <remarks/>
        public string NoOfEnquiriesLast90DaysOWN
        {
            get
            {
                return this.noOfEnquiriesLast90DaysOWNField;
            }
            set
            {
                this.noOfEnquiriesLast90DaysOWNField = value;
            }
        }

        /// <remarks/>
        public string NoOfEnquiriesLast90DaysOTH
        {
            get
            {
                return this.noOfEnquiriesLast90DaysOTHField;
            }
            set
            {
                this.noOfEnquiriesLast90DaysOTHField = value;
            }
        }

        /// <remarks/>
        public string JudgementCount
        {
            get
            {
                return this.judgementCountField;
            }
            set
            {
                this.judgementCountField = value;
            }
        }

        /// <remarks/>
        public string TotalJudgmentAmt
        {
            get
            {
                return this.totalJudgmentAmtField;
            }
            set
            {
                this.totalJudgmentAmtField = value;
            }
        }

        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        //public DateTime MostRecentJudgmentDate
        public string MostRecentJudgmentDate
        {
            get
            {
                return this.mostRecentJudgmentDateField;
            }
            set
            {
                this.mostRecentJudgmentDateField = value;
            }
        }

        /// <remarks/>
        public string CourtNoticeCount
        {
            get
            {
                return this.courtNoticeCountField;
            }
            set
            {
                this.courtNoticeCountField = value;
            }
        }

        /// <remarks/>
        public string TotalCourtNoticeAmt
        {
            get
            {
                return this.totalCourtNoticeAmtField;
            }
            set
            {
                this.totalCourtNoticeAmtField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]    
        //public System.DateTime MostRecentCourtNoticeDate
        public string MostRecentCourtNoticeDate        
        {
            get
            {
                return this.mostRecentCourtNoticeDateField;
            }
            set
            {
                this.mostRecentCourtNoticeDateField = value;
            }
        }

        /// <remarks/>
        public string NoofAccountdefaults
        {
            get
            {
                return this.noofAccountdefaultsField;
            }
            set
            {
                this.noofAccountdefaultsField = value;
            }
        }

        /// <remarks/>
        public string TotalAdverseAmt
        {
            get
            {
                return this.totalAdverseAmtField;
            }
            set
            {
                this.totalAdverseAmtField = value;
            }
        }

        public string MostRecentAdverseDate
        {
            get
            {
                return this.mostRecentAdverseDateField;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    this.mostRecentAdverseDateField = DateTime.Parse(value).ToString(Consumer.DateFormat);
                }
                else
                {
                    this.mostRecentAdverseDateField = "";
                }
            }
        }

        /// <remarks/>
        public string DefaultListingCount
        {
            get
            {
                return this.defaultListingCountField;
            }
            set
            {
                this.defaultListingCountField = value;
            }
        }

        /// <remarks/>
        public string DefaultListingAmt
        {
            get
            {
                return this.defaultListingAmtField;
            }
            set
            {
                this.defaultListingAmtField = value;
            }
        }
        
       // [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string RecentDefaultListingDate        
        {
            get
            {
                return this.recentDefaultListingDateField;
            }
            set
            {
                this.recentDefaultListingDateField = value;
            }
        }

        /// <remarks/>
        public string NoOfEnqinLast24Months
        {
            get
            {
                return this.noOfEnqinLast24MonthsField;
            }
            set
            {
                this.noOfEnqinLast24MonthsField = value;
            }
        }

        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string MostRecentEnqDateLast24Months
        {
            get
            {
                return this.mostRecentEnqDateLast24MonthsField;
            }
            set
            {
                this.mostRecentEnqDateLast24MonthsField = value;
            }
        }

        /// <remarks/>
        public string DebtReviewStatus
        {
            get
            {
                return this.debtReviewStatusField;
            }
            set
            {
                this.debtReviewStatusField = value;
            }
        }

        /// <remarks/>
        public string DisputeMessage
        {
            get
            {
                return this.disputeMessageField;
            }
            set
            {
                this.disputeMessageField = value;
            }
        }


        //Added these for the DebtSummary section in Credit Enq. But don't think need in this class actually
        public ConsumerConsumerPropertyInformationSummary PropertySummary { get; set; }

        public ConsumerConsumerDirectorSummary DirectorSummary { get; set; }

    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerAccountGoodBadSummary
    {
        public int NoOfRetailAccounts { get; set; }
        public int NoOfRetailAccountsGood { get; set; }
        public int NoOfRetailAccountsBad { get; set; }

        public int NoOfCreditCardAccounts { get; set; }
        public int NoOfCreditCardAccountsGood { get; set; }
        public int NoOfCreditCardAccountsBad { get; set; }

        public int NoOfFurnitureAccounts { get; set; }
        public int NoOfFurnitureAccountsGood { get; set; }
        public int NoOfFurnitureAccountsBad { get; set; }

        public int NoOfInsuranceAccounts { get; set; }
        public int NoOfInsuranceAccountsGood { get; set; }
        public int NoOfInsuranceAccountsBad { get; set; }

        public int NoOfPersonalFinAccounts { get; set; }
        public int NoOfPersonalFinAccountsGood { get; set; }
        public int NoOfPersonalFinAccountsBad { get; set; }

        public int NoOfBankAccounts { get; set; }
        public int NoOfBankAccountsGood { get; set; }
        public int NoOfBankAccountsBad { get; set; }

        public int NoOfTelecomAccounts { get; set; }
        public int NoOfTelecomAccountsGood { get; set; }
        public int NoOfTelecomAccountsBad { get; set; }

        public int NoOfHomeLoanAccounts { get; set; }
        public int NoOfHomeLoanAccountsGood { get; set; }
        public int NoOfHomeLoanAccountsBad { get; set; }

        public int NoOfMotorFinanceAccounts { get; set; }
        public int NoOfMotorFinanceAccountsGood { get; set; }
        public int NoOfMotorFinanceAccountsBad { get; set; }

        public int NoOfOtherAccounts { get; set; }
        public int NoOfOtherAccountsGood { get; set; }
        public int NoOfOtherAccountsBad { get; set; }

    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerSubscriberInputDetails
    {

        private string enquiryDateField;

        private string enquiryTypeField;

        private string subscriberNameField;

        private string subscriberUserNameField;

        private string enquiryInputField;

        private string enquiryReasonField;

        /// <remarks/>
        public string EnquiryDate
        {
            get
            {
                return this.enquiryDateField;
            }
            set
            {
                this.enquiryDateField = value;
            }
        }

        /// <remarks/>
        public string EnquiryType
        {
            get
            {
                return this.enquiryTypeField;
            }
            set
            {
                this.enquiryTypeField = value;
            }
        }

        /// <remarks/>
        public string SubscriberName
        {
            get
            {
                return this.subscriberNameField;
            }
            set
            {
                this.subscriberNameField = value;
            }
        }

        /// <remarks/>
        public string SubscriberUserName
        {
            get
            {
                return this.subscriberUserNameField;
            }
            set
            {
                this.subscriberUserNameField = value;
            }
        }

        /// <remarks/>
        public string EnquiryInput
        {
            get
            {
                return this.enquiryInputField;
            }
            set
            {
                this.enquiryInputField = value;
            }
        }

        /// <remarks/>
        public string EnquiryReason
        {
            get
            {
                return this.enquiryReasonField;
            }
            set
            {
                this.enquiryReasonField = value;
            }
        }
    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [XmlType("ConsumerEnquiryHistory")]
    public partial class ConsumerConsumerEnquiryHistory
    {

        private string displayTextField;

        private string enquiryDateField;

        private string subscriberNameField;

        private string subscriberBusinessTypeDescField;

        private string creditGrantorEnquiryReasonDescField;

        private string subscriberContactField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string EnquiryDate
        {
            get
            {
                return this.enquiryDateField;
            }
            set
            {
                this.enquiryDateField = value;
            }
        }

        /// <remarks/>
        public string SubscriberName
        {
            get
            {
                return this.subscriberNameField;
            }
            set
            {
                this.subscriberNameField = value;
            }
        }

        /// <remarks/>
        public string SubscriberBusinessTypeDesc
        {
            get
            {
                return this.subscriberBusinessTypeDescField;
            }
            set
            {
                this.subscriberBusinessTypeDescField = value;
            }
        }

        /// <remarks/>
        public string CreditGrantorEnquiryReasonDesc
        {
            get
            {
                return this.creditGrantorEnquiryReasonDescField;
            }
            set
            {
                this.creditGrantorEnquiryReasonDescField = value;
            }
        }

        /// <remarks/>
        public string SubscriberContact
        {
            get
            {
                return this.subscriberContactField;
            }
            set
            {
                this.subscriberContactField = value;
            }
        }
    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [XmlType("ConsumerDirectorShipLink")]
    public partial class ConsumerConsumerDirectorShipLink
    {

        private string displayTextField;

        private string directorDesignationDescField;

        private string appointmentDateField;

        private string commercialNameField;

        private string registrationNoField;

        private string physicalAddressField;

        private string telephoneNoField;

        private string sICDescField;

        private string directorStatusField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string DirectorDesignationDesc
        {
            get
            {
                return this.directorDesignationDescField;
            }
            set
            {
                this.directorDesignationDescField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string AppointmentDate
        {
            get
            {
                return this.appointmentDateField;
            }
            set
            {
                this.appointmentDateField = value;
            }
        }

        /// <remarks/>
        public string CommercialName
        {
            get
            {
                return this.commercialNameField;
            }
            set
            {
                this.commercialNameField = value;
            }
        }

        /// <remarks/>
        public string RegistrationNo
        {
            get
            {
                return this.registrationNoField;
            }
            set
            {
                this.registrationNoField = value;
            }
        }

        /// <remarks/>
        public string PhysicalAddress
        {
            get
            {
                return this.physicalAddressField;
            }
            set
            {
                this.physicalAddressField = value;
            }
        }

        /// <remarks/>
        public string TelephoneNo
        {
            get
            {
                return this.telephoneNoField;
            }
            set
            {
                this.telephoneNoField = value;
            }
        }

        /// <remarks/>
        public string SICDesc
        {
            get
            {
                return this.sICDescField;
            }
            set
            {
                this.sICDescField = value;
            }
        }

        /// <remarks/>
        public string DirectorStatus
        {
            get
            {
                return this.directorStatusField;
            }
            set
            {
                this.directorStatusField = value;
            }
        }
    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [XmlType("ConsumerSequestration")]
    public partial class ConsumerConsumerSequestration
    {

        private string displayTextField;

        private System.DateTime caseFilingDateField;

        private string caseTypeField;

        private System.DateTime lastUpdatedDateField;

        private string plaintiffNameField;

        private ushort disputeAmtField;

        private string caseReasonField;

        private string courtNameField;

        private uint caseNumberField;

        private string attorneyNameField;

        private string telephoneNoField;

        private string commentsField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime CaseFilingDate
        {
            get
            {
                return this.caseFilingDateField;
            }
            set
            {
                this.caseFilingDateField = value;
            }
        }

        /// <remarks/>
        public string CaseType
        {
            get
            {
                return this.caseTypeField;
            }
            set
            {
                this.caseTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime LastUpdatedDate
        {
            get
            {
                return this.lastUpdatedDateField;
            }
            set
            {
                this.lastUpdatedDateField = value;
            }
        }

        /// <remarks/>
        public string PlaintiffName
        {
            get
            {
                return this.plaintiffNameField;
            }
            set
            {
                this.plaintiffNameField = value;
            }
        }

        /// <remarks/>
        public ushort DisputeAmt
        {
            get
            {
                return this.disputeAmtField;
            }
            set
            {
                this.disputeAmtField = value;
            }
        }

        /// <remarks/>
        public string CaseReason
        {
            get
            {
                return this.caseReasonField;
            }
            set
            {
                this.caseReasonField = value;
            }
        }

        /// <remarks/>
        public string CourtName
        {
            get
            {
                return this.courtNameField;
            }
            set
            {
                this.courtNameField = value;
            }
        }

        /// <remarks/>
        public uint CaseNumber
        {
            get
            {
                return this.caseNumberField;
            }
            set
            {
                this.caseNumberField = value;
            }
        }

        /// <remarks/>
        public string AttorneyName
        {
            get
            {
                return this.attorneyNameField;
            }
            set
            {
                this.attorneyNameField = value;
            }
        }

        /// <remarks/>
        public string TelephoneNo
        {
            get
            {
                return this.telephoneNoField;
            }
            set
            {
                this.telephoneNoField = value;
            }
        }

        /// <remarks/>
        public string Comments
        {
            get
            {
                return this.commentsField;
            }
            set
            {
                this.commentsField = value;
            }
        }
    }

    public partial class ConsumerConsumerRehabilitationOrder
    {
        private string displayTextField;

        private string caseFilingDateField;

        private string caseTypeField;

        private string lastUpdatedDateField;

        private string plaintiffNameField;

        private string disputeAmtField;

        private string caseReasonField;

        private string courtNameField;

        private string caseNumberField;

        private string attorneyNameField;

        private string telephoneNoField;

        private string commentsField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string CaseFilingDate
        {
            get
            {
                return this.caseFilingDateField;
            }
            set
            {
                this.caseFilingDateField = value;
            }
        }

        /// <remarks/>
        public string CaseType
        {
            get
            {
                return this.caseTypeField;
            }
            set
            {
                this.caseTypeField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string LastUpdatedDate
        {
            get
            {
                return this.lastUpdatedDateField;
            }
            set
            {
                this.lastUpdatedDateField = value;
            }
        }

        /// <remarks/>
        public string PlaintiffName
        {
            get
            {
                return this.plaintiffNameField;
            }
            set
            {
                this.plaintiffNameField = value;
            }
        }

        /// <remarks/>
        public string DisputeAmt
        {
            get
            {
                return this.disputeAmtField;
            }
            set
            {
                this.disputeAmtField = value;
            }
        }

        /// <remarks/>
        public string CaseReason
        {
            get
            {
                return this.caseReasonField;
            }
            set
            {
                this.caseReasonField = value;
            }
        }

        /// <remarks/>
        public string CourtName
        {
            get
            {
                return this.courtNameField;
            }
            set
            {
                this.courtNameField = value;
            }
        }

        /// <remarks/>
        public string CaseNumber
        {
            get
            {
                return this.caseNumberField;
            }
            set
            {
                this.caseNumberField = value;
            }
        }

        /// <remarks/>
        public string AttorneyName
        {
            get
            {
                return this.attorneyNameField;
            }
            set
            {
                this.attorneyNameField = value;
            }
        }

        /// <remarks/>
        public string TelephoneNo
        {
            get
            {
                return this.telephoneNoField;
            }
            set
            {
                this.telephoneNoField = value;
            }
        }

        /// <remarks/>
        public string Comments
        {
            get
            {
                return this.commentsField;
            }
            set
            {
                this.commentsField = value;
            }
        }

    }
    
    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlType("ConsumerDefaultAlert")]
    public partial class ConsumerConsumerDefaultAlert
    {

        private string displayTextField;

        private string subscriberIDField;

        private string companyField;

        private string accountNoField;

        private string statuscodeField;

        private string amountField;

        private string commentsField;

        private string dateLoadedField;

        private string statusDateField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string SubscriberID
        {
            get
            {
                return this.subscriberIDField;
            }
            set
            {
                this.subscriberIDField = value;
            }
        }

        /// <remarks/>
        public string Company
        {
            get
            {
                return this.companyField;
            }
            set
            {
                this.companyField = value;
            }
        }

        /// <remarks/>
        public string AccountNo
        {
            get
            {
                return this.accountNoField;
            }
            set
            {
                this.accountNoField = value;
            }
        }

        /// <remarks/>
        public string Statuscode
        {
            get
            {
                return this.statuscodeField;
            }
            set
            {
                this.statuscodeField = value;
            }
        }

        /// <remarks/>
        public string Amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        public string Comments
        {
            get
            {
                return this.commentsField;
            }
            set
            {
                this.commentsField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        //[System.Xml.Serialization.XmlElement(DataType = "date")]
        public string DateLoaded
        {
            get
            {
                return this.dateLoadedField;
            }
            set
            {
                this.dateLoadedField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        //[System.Xml.Serialization.XmlElement(DataType = "date")]
        public string StatusDate
        {
            get
            {
                return this.statusDateField;
            }
            set
            {
                this.statusDateField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerDebtReviewStatus
    {

        private string displayTextField;

        private string iDnoField;

        private string firstNameField;

        private string surNameField;

        private string birthDateField;

        private string debtReviewStatusDateField;

        private string debtCounsellorNameField;

        private string debtCounsellorFirstNameField;

        private string debtCounsellorSurNameField;

        private string debtCounsellorTelephoneNoField;

        private string debtReviewStatusCodeField;

        private string homeTelephoneNoField;

        private string workTelephoneNoField;

        private string cellularNoField;

        private string debtReviewStatusField;

        private string debtCounsellorRegistrationNoField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string IDno
        {
            get
            {
                return this.iDnoField;
            }
            set
            {
                this.iDnoField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string SurName
        {
            get
            {
                return this.surNameField;
            }
            set
            {
                this.surNameField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string BirthDate
        {
            get
            {
                return this.birthDateField;
            }
            set
            {
                this.birthDateField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string DebtReviewStatusDate
        {
            get
            {
                return this.debtReviewStatusDateField;
            }
            set
            {
                this.debtReviewStatusDateField = value;
            }
        }

        /// <remarks/>
        public string DebtCounsellorName
        {
            get
            {
                return this.debtCounsellorNameField;
            }
            set
            {
                this.debtCounsellorNameField = value;
            }
        }

        /// <remarks/>
        public string DebtCounsellorFirstName
        {
            get
            {
                return this.debtCounsellorFirstNameField;
            }
            set
            {
                this.debtCounsellorFirstNameField = value;
            }
        }

        /// <remarks/>
        public string DebtCounsellorSurName
        {
            get
            {
                return this.debtCounsellorSurNameField;
            }
            set
            {
                this.debtCounsellorSurNameField = value;
            }
        }

        /// <remarks/>
        public string DebtCounsellorTelephoneNo
        {
            get
            {
                return this.debtCounsellorTelephoneNoField;
            }
            set
            {
                this.debtCounsellorTelephoneNoField = value;
            }
        }

        /// <remarks/>
        public string DebtReviewStatusCode
        {
            get
            {
                return this.debtReviewStatusCodeField;
            }
            set
            {
                this.debtReviewStatusCodeField = value;
            }
        }

        /// <remarks/>
        public string HomeTelephoneNo
        {
            get
            {
                return this.homeTelephoneNoField;
            }
            set
            {
                this.homeTelephoneNoField = value;
            }
        }

        /// <remarks/>
        public string WorkTelephoneNo
        {
            get
            {
                return this.workTelephoneNoField;
            }
            set
            {
                this.workTelephoneNoField = value;
            }
        }

        /// <remarks/>
        public string CellularNo
        {
            get
            {
                return this.cellularNoField;
            }
            set
            {
                this.cellularNoField = value;
            }
        }

        /// <remarks/>
        public string DebtReviewStatus
        {
            get
            {
                return this.debtReviewStatusField;
            }
            set
            {
                this.debtReviewStatusField = value;
            }
        }

        /// <remarks/>
        public string DebtCounsellorRegistrationNo
        {
            get
            {
                return this.debtCounsellorRegistrationNoField;
            }
            set
            {
                this.debtCounsellorRegistrationNoField = value;
            }
        }
    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlType("XDSPaymentNotification")]
    public partial class ConsumerXDSPaymentNotification
    {

        private string displayTextField;

        private string subscriberIDField;

        private string companyField;

        private string accountNoField;

        private string statuscodeField;

        private string amountField;

        private string commentsField;

        private string dateLoadedField;

        private string statusDateField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string SubscriberID
        {
            get
            {
                return this.subscriberIDField;
            }
            set
            {
                this.subscriberIDField = value;
            }
        }

        /// <remarks/>
        public string Company
        {
            get
            {
                return this.companyField;
            }
            set
            {
                this.companyField = value;
            }
        }

        /// <remarks/>
        public string AccountNo
        {
            get
            {
                return this.accountNoField;
            }
            set
            {
                this.accountNoField = value;
            }
        }

        /// <remarks/>
        public string Statuscode
        {
            get
            {
                return this.statuscodeField;
            }
            set
            {
                this.statuscodeField = value;
            }
        }

        /// <remarks/>
        public string Amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        public string Comments
        {
            get
            {
                return this.commentsField;
            }
            set
            {
                this.commentsField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string DateLoaded
        {
            get
            {
                return this.dateLoadedField;
            }
            set
            {
                this.dateLoadedField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string StatusDate
        {
            get
            {
                return this.statusDateField;
            }
            set
            {
                this.statusDateField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlType("CommercialPossibleJudgment")]
    public partial class ConsumerCommercialPossibleJudgment
    {

        private string displayTextField;

        private string registrationNoField;

        private string commercialNameField;

        private string caseNumberField;

        private string disputeAmtField;

        private string caseReasonField;

        private string caseTypeField;

        private string caseFilingDateField;

        private string plaintiffNameField;

        private string courtNameField;

        private string attorneyNameField;
        
        private string telephoneNoField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string RegistrationNo
        {
            get
            {
                return this.registrationNoField;
            }
            set
            {
                this.registrationNoField = value;
            }
        }

        /// <remarks/>
        public string CommercialName
        {
            get
            {
                return this.commercialNameField;
            }
            set
            {
                this.commercialNameField = value;
            }
        }

        /// <remarks/>
        public string CaseNumber
        {
            get
            {
                return this.caseNumberField;
            }
            set
            {
                this.caseNumberField = value;
            }
        }

        /// <remarks/>
        public string DisputeAmt
        {
            get
            {
                return this.disputeAmtField;
            }
            set
            {
                this.disputeAmtField = value;
            }
        }

        /// <remarks/>
        public string CaseReason
        {
            get
            {
                return this.caseReasonField;
            }
            set
            {
                this.caseReasonField = value;
            }
        }

        /// <remarks/>
        public string CaseType
        {
            get
            {
                return this.caseTypeField;
            }
            set
            {
                this.caseTypeField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string CaseFilingDate
        {
            get
            {
                return this.caseFilingDateField;
            }
            set
            {
                this.caseFilingDateField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string PlaintiffName
        {
            get
            {
                return this.plaintiffNameField;
            }
            set
            {
                this.plaintiffNameField = value;
            }
        }

        public string CourtName
        {
            get { return courtNameField; }
            set { courtNameField = value; }
        }

        public string AttorneyName
        {
            get { return attorneyNameField; }
            set { attorneyNameField = value; }
        }

        public string TelephoneNo
        {
            get { return telephoneNoField; }
            set { telephoneNoField = value; }
        }

    }

    public partial class ConsumerConsumerFraudIndicators
    {
        public string FirstName { get; set; }

        public string SurName { get; set; }

        public string MemRefNo { get; set; }

        public string SAFPSIncidentDetails { get; set; }

        public string SAFPSIncidentSubDetails { get; set; }

        public string PoliceCaseNo { get; set; }

        public DateTime PoliceReportDate { get; set; }

        public string PoliceStation { get; set; }

    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [XmlType("ConsumerEmailHistory")]
    public partial class ConsumerConsumerEmailHistory
    {

        private string displayTextField;

        private string emailAddressField;

        private string lastUpdatedDateField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string EmailAddress
        {
            get
            {
                return this.emailAddressField;
            }
            set
            {
                this.emailAddressField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string LastUpdatedDate
        {
            get
            {
                return this.lastUpdatedDateField;
            }
            set
            {
                this.lastUpdatedDateField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerBonusSelected
    {

        private byte dataSegmentIDField;

        private string dataSegmentNameField;

        private string dataSegmentDisplayTextField;

        /// <remarks/>
        public byte DataSegmentID
        {
            get
            {
                return this.dataSegmentIDField;
            }
            set
            {
                this.dataSegmentIDField = value;
            }
        }

        /// <remarks/>
        public string DataSegmentName
        {
            get
            {
                return this.dataSegmentNameField;
            }
            set
            {
                this.dataSegmentNameField = value;
            }
        }

        /// <remarks/>
        public string DataSegmentDisplayText
        {
            get
            {
                return this.dataSegmentDisplayTextField;
            }
            set
            {
                this.dataSegmentDisplayTextField = value;
            }
        }
        
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerIdentityVerification
    {

        private string displayTextField;

        private string iDNoField;

        private string xDSDeceasedStatusField;

        private string xDSDeceasedDateField;

        private string homeAffairsFirstNameField;

        private string homeAffairsSurnameField;

        private string homeAffairsDeceasedStatusField;

        private string homeAffairsDeceasedDateField;

        private string homeAffairsCauseOfDeathField;

        private string homeAffairsMessageField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string IDNo
        {
            get
            {
                return this.iDNoField;
            }
            set
            {
                this.iDNoField = value;
            }
        }

        /// <remarks/>
        public string XDSDeceasedStatus
        {
            get
            {
                return this.xDSDeceasedStatusField;
            }
            set
            {
                this.xDSDeceasedStatusField = value;
            }
        }

        /// <remarks/>
        public string XDSDeceasedDate
        {
            get
            {
                return this.xDSDeceasedDateField;
            }
            set
            {
                this.xDSDeceasedDateField = value;
            }
        }

        /// <remarks/>
        public string HomeAffairsFirstName
        {
            get
            {
                return this.homeAffairsFirstNameField;
            }
            set
            {
                this.homeAffairsFirstNameField = value;
            }
        }

        /// <remarks/>
        public string HomeAffairsSurname
        {
            get
            {
                return this.homeAffairsSurnameField;
            }
            set
            {
                this.homeAffairsSurnameField = value;
            }
        }

        /// <remarks/>
        public string HomeAffairsDeceasedStatus
        {
            get
            {
                return this.homeAffairsDeceasedStatusField;
            }
            set
            {
                this.homeAffairsDeceasedStatusField = value;
            }
        }

        /// <remarks/>
        public string HomeAffairsDeceasedDate
        {
            get
            {
                return this.homeAffairsDeceasedDateField;
            }
            set
            {
                this.homeAffairsDeceasedDateField = value;
            }
        }

        /// <remarks/>
        public string HomeAffairsCauseOfDeath
        {
            get
            {
                return this.homeAffairsCauseOfDeathField;
            }
            set
            {
                this.homeAffairsCauseOfDeathField = value;
            }
        }

        /// <remarks/>
        public string HomeAffairsMessage
        {
            get
            {
                return this.homeAffairsMessageField;
            }
            set
            {
                this.homeAffairsMessageField = value;
            }
        }
    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [XmlType("ConsumerAdverseInfo")]
    public partial class ConsumerConsumerAdverseInformation
    {
        #region Private Fields

        private string consumerIdField;

        private string accountNoField;

        private string subAccountNoField;

        private string periodNumField;

        private string subscriberIdField;

        private string subscribeNameField;

        private string actionDateField;

        private string currentBalanceAmtField;

        private string dataStatusField;

        private string commentsField;

        #endregion Private Fields

        #region Public Properties

        public string ConsumerID
        {
            get
            {
                return this.consumerIdField;
            }
            set
            {
                this.consumerIdField = value;
            }
        }

        public string AccountNo
        {
            get
            {
                return this.accountNoField;
            }
            set
            {
                this.accountNoField = value;
            }
        }

        public string SubAccountNo
        {
            get
            {
                return this.subAccountNoField;
            }
            set
            {
                this.subAccountNoField = value;
            }
        }

        public string PeriodNum
        {
            get
            {
                return this.periodNumField;
            }
            set
            {
                this.periodNumField = value;
            }
        }

        public string SubscriberID
        {
            get
            {
                return this.subscriberIdField;
            }
            set
            {
                this.subscriberIdField = value;
            }
        }

        public string SubscriberName
        {
            get
            {
                return this.subscribeNameField;
            }
            set
            {
                this.subscribeNameField = value;
            }
        }

        public string ActionDate
        {
            get
            {
                return this.actionDateField;
            }
            set
            {
                this.actionDateField = value;
            }
        }

        public string CurrentBalanceAmt
        {
            get
            {
                return this.currentBalanceAmtField;
            }
            set
            {
                this.currentBalanceAmtField = value;
            }
        }

        public string DataStatus
        {
            get
            {
                return this.dataStatusField;
            }
            set
            {
                this.dataStatusField = value;
            }
        }

        public string Comments
        {
            get
            {
                return this.commentsField;
            }
            set
            {
                this.commentsField = value;
            }
        }

        #endregion

    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [XmlType("ConsumerAddressConfirmation")]
    public partial class ConsumerConsumerAddressConfirmation
    {

        private string displayTextField;

        private string addressTypeField;

        private string address1Field;

        private string address2Field;

        private string address3Field;

        private string address4Field;

        private string postalCodeField;

        private string dateConfirmedField;

        private string verifiedDateField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string AddressType
        {
            get
            {
                return this.addressTypeField;
            }
            set
            {
                this.addressTypeField = value;
            }
        }

        /// <remarks/>
        public string Address1
        {
            get
            {
                return this.address1Field;
            }
            set
            {
                this.address1Field = value;
            }
        }

        /// <remarks/>
        public string Address2
        {
            get
            {
                return this.address2Field;
            }
            set
            {
                this.address2Field = value;
            }
        }

        /// <remarks/>
        public string Address3
        {
            get
            {
                return this.address3Field;
            }
            set
            {
                this.address3Field = value;
            }
        }

        /// <remarks/>
        public string Address4
        {
            get
            {
                return this.address4Field;
            }
            set
            {
                this.address4Field = value;
            }
        }

        /// <remarks/>
        public string PostalCode
        {
            get
            {
                return this.postalCodeField;
            }
            set
            {
                this.postalCodeField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string DateConfirmed
        {
            get
            {
                return this.dateConfirmedField;
            }
            set
            {
                this.dateConfirmedField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string VerifiedDate
        {
            get
            {
                return this.verifiedDateField;
            }
            set
            {
                this.verifiedDateField = value;
            }
        }
    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [XmlType("ConsumerContactConfirmation")]
    public partial class ConsumerConsumerContactConfirmation
    {

        private string displayTextField;

        private string landLineNumberField;

        private string landLineNumberConfirmDateField;

        private string cellNumberField;

        private string cellNoConfirmDateField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string LandLineNumber
        {
            get
            {
                return this.landLineNumberField;
            }
            set
            {
                this.landLineNumberField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string LandLineNumberConfirmDate
        {
            get
            {
                return this.landLineNumberConfirmDateField;
            }
            set
            {
                this.landLineNumberConfirmDateField = value;
            }
        }

        /// <remarks/>
        public string CellNumber
        {
            get
            {
                return this.cellNumberField;
            }
            set
            {
                this.cellNumberField = value;
            }
        }

        /// <remarks/> Justin - check
         //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string CellNoConfirmDate
        {
            get
            {
                return this.cellNoConfirmDateField;
            }
            set
            {
                this.cellNoConfirmDateField = value;
            }
        }
    }


    public partial class ConsumerConsumerOtherContactInfoAddress
    {
        public string Address { get; set; }

        public string AddresstypeInd { get; set; }

        public string LastUpdatedDate { get; set; }
        }

    //public partial class ConsumerConsumerOtherContactInfoTelephone
    //{
    //    public string TelephoneNumber { get; set; }

    //    public string TelephoneTypeInd { get; set; }

    //    public DateTime LastUpdatedDate { get; set; }
    //}

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [XmlType("ConsumerPropertyInformation")]
    public partial class ConsumerConsumerPropertyInformation
    {

        private string displayTextField;

        private string authorityNameField;

        private string townshipNameField;

        private string standNoField;

        private string portionNoField;

        private string titleDeedNoField;

        private string buyerNameField;

        private string buyerTypeCodeField;

        private string buyerIDNoField;

        private string buyerMaritalStatusCodeField;

        private string sellerNameField;

        private string sellerTypeCodeField;

        private string sellerIDNoField;

        private string sellerMaritalStatusCodeField;

        private string transferDateField;

        private string registrarNameField;

        private string oldTitleDeedNoField;

        private string attorneyFirmNoField;

        private string attorneyFileNoField;

        private string titleDeedFeeAmtField;

        private string propertyTypeCodeField;

        private string streetNoField;

        private string streetNameField;

        private string suburbNameField;

        private string cityNameField;

        private string transferIDField;

        private string erfNoField;

        private string deedsOfficeField;

        //private ConsumerConsumerPropertyInformationPhysicalAddress physicalAddressField;
        private string physicalAddressField;

        private string propertyTypeDescField;

        private string erfSizeField;

        private string purchaseDateField;

        private string purchasePriceAmtField;

        private string buyerSharePercField;

        private string bondHolderNameField;

        private string bondAccountNoField;

        private string bondAmtField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string AuthorityName
        {
            get
            {
                return this.authorityNameField;
            }
            set
            {
                this.authorityNameField = value;
            }
        }

        /// <remarks/>
        public string TownshipName
        {
            get
            {
                return this.townshipNameField;
            }
            set
            {
                this.townshipNameField = value;
            }
        }

        /// <remarks/>
        public string StandNo
        {
            get
            {
                return this.standNoField;
            }
            set
            {
                this.standNoField = value;
            }
        }

        /// <remarks/>
        public string PortionNo
        {
            get
            {
                return this.portionNoField;
            }
            set
            {
                this.portionNoField = value;
            }
        }

        /// <remarks/>
        public string TitleDeedNo
        {
            get
            {
                return this.titleDeedNoField;
            }
            set
            {
                this.titleDeedNoField = value;
            }
        }

        /// <remarks/>
        public string BuyerName
        {
            get
            {
                return this.buyerNameField;
            }
            set
            {
                this.buyerNameField = value;
            }
        }

        /// <remarks/>
        public string BuyerTypeCode
        {
            get
            {
                return this.buyerTypeCodeField;
            }
            set
            {
                this.buyerTypeCodeField = value;
            }
        }

        /// <remarks/>
        public string BuyerIDNo
        {
            get
            {
                return this.buyerIDNoField;
            }
            set
            {
                this.buyerIDNoField = value;
            }
        }

        /// <remarks/>
        public string BuyerMaritalStatusCode
        {
            get
            {
                return this.buyerMaritalStatusCodeField;
            }
            set
            {
                this.buyerMaritalStatusCodeField = value;
            }
        }

        /// <remarks/>
        public string SellerName
        {
            get
            {
                return this.sellerNameField;
            }
            set
            {
                this.sellerNameField = value;
            }
        }

        /// <remarks/>
        public string SellerTypeCode
        {
            get
            {
                return this.sellerTypeCodeField;
            }
            set
            {
                this.sellerTypeCodeField = value;
            }
        }

        /// <remarks/>
        public string SellerIDNo
        {
            get
            {
                return this.sellerIDNoField;
            }
            set
            {
                this.sellerIDNoField = value;
            }
        }

        /// <remarks/>
        public string SellerMaritalStatusCode
        {
            get
            {
                return this.sellerMaritalStatusCodeField;
            }
            set
            {
                this.sellerMaritalStatusCodeField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string TransferDate
        {
            get
            {
                return this.transferDateField;
            }
            set
            {
                this.transferDateField = value;
            }
        }

        /// <remarks/>
        public string RegistrarName
        {
            get
            {
                return this.registrarNameField;
            }
            set
            {
                this.registrarNameField = value;
            }
        }

        /// <remarks/>
        public string OldTitleDeedNo
        {
            get
            {
                return this.oldTitleDeedNoField;
            }
            set
            {
                this.oldTitleDeedNoField = value;
            }
        }

        /// <remarks/>
        public string AttorneyFirmNo
        {
            get
            {
                return this.attorneyFirmNoField;
            }
            set
            {
                this.attorneyFirmNoField = value;
            }
        }

        /// <remarks/>
        public string AttorneyFileNo
        {
            get
            {
                return this.attorneyFileNoField;
            }
            set
            {
                this.attorneyFileNoField = value;
            }
        }

        /// <remarks/>
        public string TitleDeedFeeAmt
        {
            get
            {
                return this.titleDeedFeeAmtField;
            }
            set
            {
                this.titleDeedFeeAmtField = value;
            }
        }

        /// <remarks/>
        public string PropertyTypeCode
        {
            get
            {
                return this.propertyTypeCodeField;
            }
            set
            {
                this.propertyTypeCodeField = value;
            }
        }

        /// <remarks/>
        public string StreetNo
        {
            get
            {
                return this.streetNoField;
            }
            set
            {
                this.streetNoField = value;
            }
        }

        /// <remarks/>
        public string StreetName
        {
            get
            {
                return this.streetNameField;
            }
            set
            {
                this.streetNameField = value;
            }
        }

        /// <remarks/>
        public string SuburbName
        {
            get
            {
                return this.suburbNameField;
            }
            set
            {
                this.suburbNameField = value;
            }
        }

        /// <remarks/>
        public string CityName
        {
            get
            {
                return this.cityNameField;
            }
            set
            {
                this.cityNameField = value;
            }
        }

        /// <remarks/>
        public string TransferID
        {
            get
            {
                return this.transferIDField;
            }
            set
            {
                this.transferIDField = value;
            }
        }

        /// <remarks/>
        public string ErfNo
        {
            get
            {
                return this.erfNoField;
            }
            set
            {
                this.erfNoField = value;
            }
        }

        /// <remarks/>
        public string DeedsOffice
        {
            get
            {
                return this.deedsOfficeField;
            }
            set
            {
                this.deedsOfficeField = value;
            }
        }

        /// <remarks/>
        public string PhysicalAddress
        {
            get
            {
                return this.physicalAddressField;
            }
            set
            {
                this.physicalAddressField = value;
            }
        }

        /// <remarks/>
        public string PropertyTypeDesc
        {
            get
            {
                return this.propertyTypeDescField;
            }
            set
            {
                this.propertyTypeDescField = value;
            }
        }

        /// <remarks/>
        public string ErfSize
        {
            get
            {
                return this.erfSizeField;
            }
            set
            {
                this.erfSizeField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string PurchaseDate
        {
            get
            {
                return this.purchaseDateField;
            }
            set
            {
                this.purchaseDateField = value;
            }
        }

        /// <remarks/>
        public string PurchasePriceAmt
        {
            get
            {
                return this.purchasePriceAmtField;
            }
            set
            {
                this.purchasePriceAmtField = value;
            }
        }

        /// <remarks/>
        public string BuyerSharePerc
        {
            get
            {
                return this.buyerSharePercField;
            }
            set
            {
                this.buyerSharePercField = value;
            }
        }

        /// <remarks/>
        public string BondHolderName
        {
            get
            {
                return this.bondHolderNameField;
            }
            set
            {
                this.bondHolderNameField = value;
            }
        }

        /// <remarks/>
        public string BondAccountNo
        {
            get
            {
                return this.bondAccountNoField;
            }
            set
            {
                this.bondAccountNoField = value;
            }
        }

        /// <remarks/>
        public string BondAmt
        {
            get
            {
                return this.bondAmtField;
            }
            set
            {
                this.bondAmtField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerPropertyInformationPhysicalAddress
    {

        private string spaceField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://www.w3.org/XML/1998/namespace")]
        public string space
        {
            get
            {
                return this.spaceField;
            }
            set
            {
                this.spaceField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerDirectorInformation
    {

        private string displayTextField;

        private string iDNoField;

        private string firstNameField;

        private string initialsField;

        private string surnameField;

        private string fullNameField;

        private string birthDateField;

        private string directorStatusCodeField;

        private string appointmentDateField;

        private string directorStatusDateField;

        private string memberSizeField;

        private string physicalAddressField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string IDNo
        {
            get
            {
                return this.iDNoField;
            }
            set
            {
                this.iDNoField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string Initials
        {
            get
            {
                return this.initialsField;
            }
            set
            {
                this.initialsField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return this.fullNameField;
            }
            set
            {
                this.fullNameField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string BirthDate
        {
            get
            {
                return this.birthDateField;
            }
            set
            {
                this.birthDateField = value;
            }
        }

        /// <remarks/>
        public string DirectorStatusCode
        {
            get
            {
                return this.directorStatusCodeField;
            }
            set
            {
                this.directorStatusCodeField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string AppointmentDate
        {
            get
            {
                return this.appointmentDateField;
            }
            set
            {
                this.appointmentDateField = value;
            }
        }

        /// <remarks/>
        public string DirectorStatusDate
        {
            get
            {
                return this.directorStatusDateField;
            }
            set
            {
                this.directorStatusDateField = value;
            }
        }

        /// <remarks/>
        public string MemberSize
        {
            get
            {
                return this.memberSizeField;
            }
            set
            {
                this.memberSizeField = value;
            }
        }

        /// <remarks/>
        public string PhysicalAddress
        {
            get
            {
                return this.physicalAddressField;
            }
            set
            {
                this.physicalAddressField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerAuditorInformation
    {

        private string displayTextField;

        private string auditorNameField;

        private string professionNoField;

        private string professionDescField;

        private string auditorTypeDescField;

        private string auditorStatusDescField;

        private string lastUpdatedDateField;

        private string postalAddressField;

        private string physicalAddressField;

        private string telephoneNoField;

        private string yearswithAuditorField;

        private string actStartdateField;

        private string actEndDateField;

        private string noOfYearsInbBusinessField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string AuditorName
        {
            get
            {
                return this.auditorNameField;
            }
            set
            {
                this.auditorNameField = value;
            }
        }

        /// <remarks/>
        public string ProfessionNo
        {
            get
            {
                return this.professionNoField;
            }
            set
            {
                this.professionNoField = value;
            }
        }

        /// <remarks/>
        public string ProfessionDesc
        {
            get
            {
                return this.professionDescField;
            }
            set
            {
                this.professionDescField = value;
            }
        }

        /// <remarks/>
        public string AuditorTypeDesc
        {
            get
            {
                return this.auditorTypeDescField;
            }
            set
            {
                this.auditorTypeDescField = value;
            }
        }

        /// <remarks/>
        public string AuditorStatusDesc
        {
            get
            {
                return this.auditorStatusDescField;
            }
            set
            {
                this.auditorStatusDescField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string LastUpdatedDate
        {
            get
            {
                return this.lastUpdatedDateField;
            }
            set
            {
                this.lastUpdatedDateField = value;
            }
        }

        /// <remarks/>
        public string PostalAddress
        {
            get
            {
                return this.postalAddressField;
            }
            set
            {
                this.postalAddressField = value;
            }
        }

        /// <remarks/>
        public string PhysicalAddress
        {
            get
            {
                return this.physicalAddressField;
            }
            set
            {
                this.physicalAddressField = value;
            }
        }

        /// <remarks/>
        public string TelephoneNo
        {
            get
            {
                return this.telephoneNoField;
            }
            set
            {
                this.telephoneNoField = value;
            }
        }

        /// <remarks/>
        public string YearswithAuditor
        {
            get
            {
                return this.yearswithAuditorField;
            }
            set
            {
                this.yearswithAuditorField = value;
            }
        }

        /// <remarks/>
        public string ActStartdate
        {
            get
            {
                return this.actStartdateField;
            }
            set
            {
                this.actStartdateField = value;
            }
        }

        /// <remarks/>
        public string ActEndDate
        {
            get
            {
                return this.actEndDateField;
            }
            set
            {
                this.actEndDateField = value;
            }
        }

        /// <remarks/>
        public string NoOfYearsInbBusiness
        {
            get
            {
                return this.noOfYearsInbBusinessField;
            }
            set
            {
                this.noOfYearsInbBusinessField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerAddressInformation
    {

        private string displayTextField;

        private string addressTypeField;

        private string address1Field;

        private string address2Field;

        private ConsumerConsumerAddressInformationAddress3 address3Field;

        private ConsumerConsumerAddressInformationAddress4 address4Field;

        private string postalCodeField;

        private System.DateTime lastUpdatedDateField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string AddressType
        {
            get
            {
                return this.addressTypeField;
            }
            set
            {
                this.addressTypeField = value;
            }
        }

        /// <remarks/>
        public string Address1
        {
            get
            {
                return this.address1Field;
            }
            set
            {
                this.address1Field = value;
            }
        }

        /// <remarks/>
        public string Address2
        {
            get
            {
                return this.address2Field;
            }
            set
            {
                this.address2Field = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerAddressInformationAddress3 Address3
        {
            get
            {
                return this.address3Field;
            }
            set
            {
                this.address3Field = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerAddressInformationAddress4 Address4
        {
            get
            {
                return this.address4Field;
            }
            set
            {
                this.address4Field = value;
            }
        }

        /// <remarks/>
        public string PostalCode
        {
            get
            {
                return this.postalCodeField;
            }
            set
            {
                this.postalCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime LastUpdatedDate
        {
            get
            {
                return this.lastUpdatedDateField;
            }
            set
            {
                this.lastUpdatedDateField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerAddressInformationAddress3
    {

        private string spaceField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://www.w3.org/XML/1998/namespace")]
        public string space
        {
            get
            {
                return this.spaceField;
            }
            set
            {
                this.spaceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerAddressInformationAddress4
    {

        private string spaceField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://www.w3.org/XML/1998/namespace")]
        public string space
        {
            get
            {
                return this.spaceField;
            }
            set
            {
                this.spaceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerBusinessInformation
    {

        private string displayTextField;

        private string commercialNameField;

        private string registrationNoField;

        private System.DateTime businessStartDateField;

        private string financialYearEndField;

        private string registrationNoOldField;

        private string commercialStatusField;

        private string commercialTypeField;

        private string sICField;

        private string taxNoField;

        private string referenceNoField;

        private string externalReferenceField;

        private string tradeNameField;

        private string previousBussNameField;

        private string physicalAddressField;

        private string postalAddressField;

        private System.DateTime registrationDateField;

        private string businessDescField;

        private string telephoneNoField;

        private string faxNoField;

        private string bussEmailField;

        private string bussWebsiteField;

        private string noOfEnquiriesField;

        private string nameChangeDateField;

        private string ageofBusinessField;

        private string authorisedCapitalAmtField;

        private string issuedNoOfSharesField;

        private string registrationNoConvertedField;

        private string countryOfOriginField;

        //private System.DateTime commercialTypeDateField;
        //private System.DateTime financialEffectiveDateField;

        private string commercialTypeDateField;

        private string financialEffectiveDateField;

        private string authorisedNoOfSharesField;

        private string issuedCapitalAmtField;

        private string commercialStatusDateField;

        private string directorCountField;

        private string vATNoField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string CommercialName
        {
            get
            {
                return this.commercialNameField;
            }
            set
            {
                this.commercialNameField = value;
            }
        }

        /// <remarks/>
        public string RegistrationNo
        {
            get
            {
                return this.registrationNoField;
            }
            set
            {
                this.registrationNoField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime BusinessStartDate
        {
            get
            {
                return this.businessStartDateField;
            }
            set
            {
                this.businessStartDateField = value;
            }
        }

        /// <remarks/>
        public string FinancialYearEnd
        {
            get
            {
                return this.financialYearEndField;
            }
            set
            {
                this.financialYearEndField = value;
            }
        }

        /// <remarks/>
        public string RegistrationNoOld
        {
            get
            {
                return this.registrationNoOldField;
            }
            set
            {
                this.registrationNoOldField = value;
            }
        }

        /// <remarks/>
        public string CommercialStatus
        {
            get
            {
                return this.commercialStatusField;
            }
            set
            {
                this.commercialStatusField = value;
            }
        }

        /// <remarks/>
        public string CommercialType
        {
            get
            {
                return this.commercialTypeField;
            }
            set
            {
                this.commercialTypeField = value;
            }
        }

        /// <remarks/>
        public string SIC
        {
            get
            {
                return this.sICField;
            }
            set
            {
                this.sICField = value;
            }
        }

        /// <remarks/>
        public string TaxNo
        {
            get
            {
                return this.taxNoField;
            }
            set
            {
                this.taxNoField = value;
            }
        }

        /// <remarks/>
        public string ReferenceNo
        {
            get
            {
                return this.referenceNoField;
            }
            set
            {
                this.referenceNoField = value;
            }
        }

        /// <remarks/>
        public string ExternalReference
        {
            get
            {
                return this.externalReferenceField;
            }
            set
            {
                this.externalReferenceField = value;
            }
        }

        /// <remarks/>
        public string TradeName
        {
            get
            {
                return this.tradeNameField;
            }
            set
            {
                this.tradeNameField = value;
            }
        }

        /// <remarks/>
        public string PreviousBussName
        {
            get
            {
                return this.previousBussNameField;
            }
            set
            {
                this.previousBussNameField = value;
            }
        }

        /// <remarks/>
        public string PhysicalAddress
        {
            get
            {
                return this.physicalAddressField;
            }
            set
            {
                this.physicalAddressField = value;
            }
        }

        /// <remarks/>
        public string PostalAddress
        {
            get
            {
                return this.postalAddressField;
            }
            set
            {
                this.postalAddressField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime RegistrationDate
        {
            get
            {
                return this.registrationDateField;
            }
            set
            {
                this.registrationDateField = value;
            }
        }

        /// <remarks/>
        public string BusinessDesc
        {
            get
            {
                return this.businessDescField;
            }
            set
            {
                this.businessDescField = value;
            }
        }

        /// <remarks/>
        public string TelephoneNo
        {
            get
            {
                return this.telephoneNoField;
            }
            set
            {
                this.telephoneNoField = value;
            }
        }

        /// <remarks/>
        public string FaxNo
        {
            get
            {
                return this.faxNoField;
            }
            set
            {
                this.faxNoField = value;
            }
        }

        /// <remarks/>
        public string BussEmail
        {
            get
            {
                return this.bussEmailField;
            }
            set
            {
                this.bussEmailField = value;
            }
        }

        /// <remarks/>
        public string BussWebsite
        {
            get
            {
                return this.bussWebsiteField;
            }
            set
            {
                this.bussWebsiteField = value;
            }
        }

        /// <remarks/>
        public string NoOfEnquiries
        {
            get
            {
                return this.noOfEnquiriesField;
            }
            set
            {
                this.noOfEnquiriesField = value;
            }
        }

        /// <remarks/>
        public string NameChangeDate
        {
            get
            {
                return this.nameChangeDateField;
            }
            set
            {
                this.nameChangeDateField = value;
            }
        }

        /// <remarks/>
        public string AgeofBusiness
        {
            get
            {
                return this.ageofBusinessField;
            }
            set
            {
                this.ageofBusinessField = value;
            }
        }

        /// <remarks/>
        public string AuthorisedCapitalAmt
        {
            get
            {
                return this.authorisedCapitalAmtField;
            }
            set
            {
                this.authorisedCapitalAmtField = value;
            }
        }

        /// <remarks/>
        public string IssuedNoOfShares
        {
            get
            {
                return this.issuedNoOfSharesField;
            }
            set
            {
                this.issuedNoOfSharesField = value;
            }
        }

        /// <remarks/>
        public string RegistrationNoConverted
        {
            get
            {
                return this.registrationNoConvertedField;
            }
            set
            {
                this.registrationNoConvertedField = value;
            }
        }

        /// <remarks/>
        public string CountryOfOrigin
        {
            get
            {
                return this.countryOfOriginField;
            }
            set
            {
                this.countryOfOriginField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string CommercialTypeDate
        {
            get
            {
                return this.commercialTypeDateField;
            }
            set
            {
                this.commercialTypeDateField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string FinancialEffectiveDate
        {
            get
            {
                return this.financialEffectiveDateField;
            }
            set
            {
                this.financialEffectiveDateField = value;
            }
        }

        /// <remarks/>
        public string AuthorisedNoOfShares
        {
            get
            {
                return this.authorisedNoOfSharesField;
            }
            set
            {
                this.authorisedNoOfSharesField = value;
            }
        }

        /// <remarks/>
        public string IssuedCapitalAmt
        {
            get
            {
                return this.issuedCapitalAmtField;
            }
            set
            {
                this.issuedCapitalAmtField = value;
            }
        }

        /// <remarks/>
        public string CommercialStatusDate
        {
            get
            {
                return this.commercialStatusDateField;
            }
            set
            {
                this.commercialStatusDateField = value;
            }
        }

        /// <remarks/>
        public string DirectorCount
        {
            get
            {
                return this.directorCountField;
            }
            set
            {
                this.directorCountField = value;
            }
        }

        /// <remarks/>
        public string VATNo
        {
            get
            {
                return this.vATNoField;
            }
            set
            {
                this.vATNoField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerBusinessInformationTradeName
    {

        private string spaceField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://www.w3.org/XML/1998/namespace")]
        public string space
        {
            get
            {
                return this.spaceField;
            }
            set
            {
                this.spaceField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerCompanyInformation
    {

        private string displayTextField;

        private string commercialNameField;

        private string registrationNoField;

        private string registrationNoOldField;

        private System.DateTime businessStartDateField;

        private string financialYearEndField;

        private string commercialStatusField;

        private string commercialTypeField;

        private string sICField;

        private string taxNoField;

        private string directorCountField;

        private string referenceNoField;

        private string externalReferenceField;

        private ConsumerConsumerCompanyInformationTradeName tradeNameField;

        private string vATNoField;

        private string previousBussNameField;

        private string nameChangeDateField;

        private string bussEmailField;

        private string bussWebsiteField;

        private System.DateTime lastUpdatedDateField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string CommercialName
        {
            get
            {
                return this.commercialNameField;
            }
            set
            {
                this.commercialNameField = value;
            }
        }

        /// <remarks/>
        public string RegistrationNo
        {
            get
            {
                return this.registrationNoField;
            }
            set
            {
                this.registrationNoField = value;
            }
        }

        /// <remarks/>
        public string RegistrationNoOld
        {
            get
            {
                return this.registrationNoOldField;
            }
            set
            {
                this.registrationNoOldField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime BusinessStartDate
        {
            get
            {
                return this.businessStartDateField;
            }
            set
            {
                this.businessStartDateField = value;
            }
        }

        /// <remarks/>
        public string FinancialYearEnd
        {
            get
            {
                return this.financialYearEndField;
            }
            set
            {
                this.financialYearEndField = value;
            }
        }

        /// <remarks/>
        public string CommercialStatus
        {
            get
            {
                return this.commercialStatusField;
            }
            set
            {
                this.commercialStatusField = value;
            }
        }

        /// <remarks/>
        public string CommercialType
        {
            get
            {
                return this.commercialTypeField;
            }
            set
            {
                this.commercialTypeField = value;
            }
        }

        /// <remarks/>
        public string SIC
        {
            get
            {
                return this.sICField;
            }
            set
            {
                this.sICField = value;
            }
        }

        /// <remarks/>
        public string TaxNo
        {
            get
            {
                return this.taxNoField;
            }
            set
            {
                this.taxNoField = value;
            }
        }

        /// <remarks/>
        public string DirectorCount
        {
            get
            {
                return this.directorCountField;
            }
            set
            {
                this.directorCountField = value;
            }
        }

        /// <remarks/>
        public string ReferenceNo
        {
            get
            {
                return this.referenceNoField;
            }
            set
            {
                this.referenceNoField = value;
            }
        }

        /// <remarks/>
        public string ExternalReference
        {
            get
            {
                return this.externalReferenceField;
            }
            set
            {
                this.externalReferenceField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerCompanyInformationTradeName TradeName
        {
            get
            {
                return this.tradeNameField;
            }
            set
            {
                this.tradeNameField = value;
            }
        }

        /// <remarks/>
        public string VATNo
        {
            get
            {
                return this.vATNoField;
            }
            set
            {
                this.vATNoField = value;
            }
        }

        /// <remarks/>
        public string PreviousBussName
        {
            get
            {
                return this.previousBussNameField;
            }
            set
            {
                this.previousBussNameField = value;
            }
        }

        /// <remarks/>
        public string NameChangeDate
        {
            get
            {
                return this.nameChangeDateField;
            }
            set
            {
                this.nameChangeDateField = value;
            }
        }

        /// <remarks/>
        public string BussEmail
        {
            get
            {
                return this.bussEmailField;
            }
            set
            {
                this.bussEmailField = value;
            }
        }

        /// <remarks/>
        public string BussWebsite
        {
            get
            {
                return this.bussWebsiteField;
            }
            set
            {
                this.bussWebsiteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime LastUpdatedDate
        {
            get
            {
                return this.lastUpdatedDateField;
            }
            set
            {
                this.lastUpdatedDateField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerCompanyInformationTradeName
    {

        private string spaceField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://www.w3.org/XML/1998/namespace")]
        public string space
        {
            get
            {
                return this.spaceField;
            }
            set
            {
                this.spaceField = value;
            }
        }
    }

    /// <remarks/>
   
    public partial class ConsumerActiveDirectorJudgments
    {

        private string displayTextField;

        private string directorIDField;

        private string caseNumberField;

        private string disputeAmtField;

        private string courtNameField;

        private string caseTypeField;

        private string judgmentDateField;

        private string plaintiffNameField;

        private string attorneyNameField;

        private string telephoneNoField;

        private string caseReasonField;

        private string iDNoField;

        private string firstNameField;

        private string surnameField;

        private string commentField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string DirectorID
        {
            get
            {
                return this.directorIDField;
            }
            set
            {
                this.directorIDField = value;
            }
        }

        /// <remarks/>
        public string CaseNumber
        {
            get
            {
                return this.caseNumberField;
            }
            set
            {
                this.caseNumberField = value;
            }
        }

        /// <remarks/>
        public string DisputeAmt
        {
            get
            {
                return "R " + XDS.ONLINE.MODELS.General.TrimDecimalToTwoDecimalPoint(this.disputeAmtField);
            }
            set
            {
                this.disputeAmtField = value;
            }
        }

        /// <remarks/>
        public string CourtName
        {
            get
            {
                return this.courtNameField;
            }
            set
            {
                this.courtNameField = value;
            }
        }

        /// <remarks/>
        public string CaseType
        {
            get
            {
                return this.caseTypeField;
            }
            set
            {
                this.caseTypeField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string JudgmentDate
        {
            get
            {
                return this.judgmentDateField;
            }
            set
            {
                this.judgmentDateField = value;
            }
        }

        /// <remarks/>
        public string PlaintiffName
        {
            get
            {
                return this.plaintiffNameField;
            }
            set
            {
                this.plaintiffNameField = value;
            }
        }

        /// <remarks/>
        public string AttorneyName
        {
            get
            {
                return this.attorneyNameField;
            }
            set
            {
                this.attorneyNameField = value;
            }
        }

        /// <remarks/>
        public string TelephoneNo
        {
            get
            {
                return this.telephoneNoField;
            }
            set
            {
                this.telephoneNoField = value;
            }
        }

        /// <remarks/>
        public string CaseReason
        {
            get
            {
                return this.caseReasonField;
            }
            set
            {
                this.caseReasonField = value;
            }
        }

        /// <remarks/>
        public string IDNo
        {
            get
            {
                return this.iDNoField;
            }
            set
            {
                this.iDNoField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerPrincipalInformation
    {

        private string displayTextField;

        private string directorIDField;

        private string iDNoField;

        private string firstNameField;

        private string initialsField;

        private string surnameField;

        private string secondNameField;

        private string birthDateField;

        private string directorStatusCodeField;

        private string appointmentDateField;

        private string designationField;

        private string memberSizeField;

        private string memberControlPercField;

        private string directorIndicatorField;

        private string principalTypeField;

        private string cM29DateField;

        private string iSRSAResidentField;

        private string countryCodeField;

        private string iSIDVerifiedField;

        private string iSCIPROConfirmedField;

        private string physicalAddressField;

        private string postalAddressField;

        private string nameField;

        private string homeTelephoneNoField;

        private string workTelephoneNoField;

        private string cellularNoField;

        private string emailAddressField;

        private string ageField;

        private string yearsWithBusinessField;

        private string fullnameField;

        private string surnamePreviousField;

        private string directorStatusDateField;

        private ConsumerConsumerPrincipalInformationMemberControlType memberControlTypeField;

        private string executorField;

        private string executorAppointmentDateField;

        private string estateField;

        private string resignationDateField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string DirectorID
        {
            get
            {
                return this.directorIDField;
            }
            set
            {
                this.directorIDField = value;
            }
        }

        /// <remarks/>
        public string IDNo
        {
            get
            {
                return this.iDNoField;
            }
            set
            {
                this.iDNoField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string Initials
        {
            get
            {
                return this.initialsField;
            }
            set
            {
                this.initialsField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string SecondName
        {
            get
            {
                return this.secondNameField;
            }
            set
            {
                this.secondNameField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        //public System.DateTime BirthDate
        public string BirthDate
        {
            get
            {
                return this.birthDateField;
            }
            set
            {
                this.birthDateField = value;
            }
        }

        /// <remarks/>
        public string DirectorStatusCode
        {
            get
            {
                return this.directorStatusCodeField;
            }
            set
            {
                this.directorStatusCodeField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        //public System.DateTime AppointmentDate
        public string AppointmentDate
        {
            get
            {
                return this.appointmentDateField;
            }
            set
            {
                this.appointmentDateField = value;
            }
        }

        /// <remarks/>
        public string Designation
        {
            get
            {
                return this.designationField;
            }
            set
            {
                this.designationField = value;
            }
        }

        /// <remarks/>
        public string MemberSize
        {
            get
            {
                return this.memberSizeField;
            }
            set
            {
                this.memberSizeField = value;
            }
        }

        /// <remarks/>
        public string MemberControlPerc
        {
            get
            {
                return this.memberControlPercField;
            }
            set
            {
                this.memberControlPercField = value;
            }
        }

        /// <remarks/>
        public string DirectorIndicator
        {
            get
            {
                return this.directorIndicatorField;
            }
            set
            {
                this.directorIndicatorField = value;
            }
        }

        /// <remarks/>
        public string PrincipalType
        {
            get
            {
                return this.principalTypeField;
            }
            set
            {
                this.principalTypeField = value;
            }
        }

        /// <remarks/>
        public string CM29Date
        {
            get
            {
                return this.cM29DateField;
            }
            set
            {
                this.cM29DateField = value;
            }
        }

        /// <remarks/>
        public string ISRSAResident
        {
            get
            {
                return this.iSRSAResidentField;
            }
            set
            {
                this.iSRSAResidentField = value;
            }
        }

        /// <remarks/>
        public string CountryCode
        {
            get
            {
                return this.countryCodeField;
            }
            set
            {
                this.countryCodeField = value;
            }
        }

        /// <remarks/>
        public string ISIDVerified
        {
            get
            {
                return this.iSIDVerifiedField;
            }
            set
            {
                this.iSIDVerifiedField = value;
            }
        }

        /// <remarks/>
        public string ISCIPROConfirmed
        {
            get
            {
                return this.iSCIPROConfirmedField;
            }
            set
            {
                this.iSCIPROConfirmedField = value;
            }
        }

        /// <remarks/>
        public string PhysicalAddress
        {
            get
            {
                return this.physicalAddressField;
            }
            set
            {
                this.physicalAddressField = value;
            }
        }

        /// <remarks/>
        public string PostalAddress
        {
            get
            {
                return this.postalAddressField;
            }
            set
            {
                this.postalAddressField = value;
            }
        }

        /// <remarks/>
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public string HomeTelephoneNo
        {
            get
            {
                return this.homeTelephoneNoField;
            }
            set
            {
                this.homeTelephoneNoField = value;
            }
        }

        /// <remarks/>
        public string WorkTelephoneNo
        {
            get
            {
                return this.workTelephoneNoField;
            }
            set
            {
                this.workTelephoneNoField = value;
            }
        }

        /// <remarks/>
        public string CellularNo
        {
            get
            {
                return this.cellularNoField;
            }
            set
            {
                this.cellularNoField = value;
            }
        }

        /// <remarks/>
        public string EmailAddress
        {
            get
            {
                return this.emailAddressField;
            }
            set
            {
                this.emailAddressField = value;
            }
        }

        /// <remarks/>
        public string Age
        {
            get
            {
                return this.ageField;
            }
            set
            {
                this.ageField = value;
            }
        }

        /// <remarks/>
        public string YearsWithBusiness
        {
            get
            {
                return this.yearsWithBusinessField;
            }
            set
            {
                this.yearsWithBusinessField = value;
            }
        }

        /// <remarks/>
        public string Fullname
        {
            get
            {
                return this.fullnameField;
            }
            set
            {
                this.fullnameField = value;
            }
        }

        /// <remarks/>
        public string SurnamePrevious
        {
            get
            {
                return this.surnamePreviousField;
            }
            set
            {
                this.surnamePreviousField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        //public System.DateTime DirectorStatusDate
        public string DirectorStatusDate
        {
            get
            {
                return this.directorStatusDateField;
            }
            set
            {
                this.directorStatusDateField = value;
            }
        }

        /// <remarks/>
        public ConsumerConsumerPrincipalInformationMemberControlType MemberControlType
        {
            get
            {
                return this.memberControlTypeField;
            }
            set
            {
                this.memberControlTypeField = value;
            }
        }

        /// <remarks/>
        public string Executor
        {
            get
            {
                return this.executorField;
            }
            set
            {
                this.executorField = value;
            }
        }

        /// <remarks/>
        public string ExecutorAppointmentDate
        {
            get
            {
                return this.executorAppointmentDateField;
            }
            set
            {
                this.executorAppointmentDateField = value;
            }
        }

        /// <remarks/>
        public string Estate
        {
            get
            {
                return this.estateField;
            }
            set
            {
                this.estateField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        //public System.DateTime ResignationDate
        public string ResignationDate
        {
            get
            {
                return this.resignationDateField;
            }
            set
            {
                this.resignationDateField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerPrincipalInformationMemberControlType
    {

        private string spaceField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://www.w3.org/XML/1998/namespace")]
        public string space
        {
            get
            {
                return this.spaceField;
            }
            set
            {
                this.spaceField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerActivePrincipalInfoSummary
    {

        private string displayTextField;

        private string noOfPrincipalsField;

        private string noOfInactivePrincipalsField;

        private string averageAgeField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string NoOfPrincipals
        {
            get
            {
                return this.noOfPrincipalsField;
            }
            set
            {
                this.noOfPrincipalsField = value;
            }
        }

        /// <remarks/>
        public string NoOfInactivePrincipals
        {
            get
            {
                return this.noOfInactivePrincipalsField;
            }
            set
            {
                this.noOfInactivePrincipalsField = value;
            }
        }

        /// <remarks/>
        public string AverageAge
        {
            get
            {
                return this.averageAgeField;
            }
            set
            {
                this.averageAgeField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerInActivePrincipalInfoSummary
    {

        private string displayTextField;

        private string noOfPrincipalsField;

        private string noOfInactivePrincipalsField;

        private string averageAgeField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string NoOfPrincipals
        {
            get
            {
                return this.noOfPrincipalsField;
            }
            set
            {
                this.noOfPrincipalsField = value;
            }
        }

        /// <remarks/>
        public string NoOfInactivePrincipals
        {
            get
            {
                return this.noOfInactivePrincipalsField;
            }
            set
            {
                this.noOfInactivePrincipalsField = value;
            }
        }

        /// <remarks/>
        public string AverageAge
        {
            get
            {
                return this.averageAgeField;
            }
            set
            {
                this.averageAgeField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerActivePrincipalInformation
    {

        private string displayTextField;

        private string directorIDField;

        private string iDNoField;

        private string firstNameField;

        private string initialsField;

        private string surnameField;

        private string secondNameField;

        private string birthDateField;

        private string directorStatusCodeField;

        private string appointmentDateField;

        private string designationField;

        private string memberSizeField;

        private string memberControlPercField;

        private string directorIndicatorField;

        private string principalTypeField;

        private string cM29DateField;

        private string iSRSAResidentField;

        private string countryCodeField;

        private string iSIDVerifiedField;

        private string iSCIPROConfirmedField;

        private string physicalAddressField;

        private string postalAddressField;

        private string nameField;

        private string homeTelephoneNoField;

        private string workTelephoneNoField;

        private string cellularNoField;

        private string emailAddressField;

        private string ageField;

        private string yearsWithBusinessField;

        private string fullnameField;

        private string surnamePreviousField;

        private string directorStatusDateField;

        private string memberControlTypeField;

        private string executorField;

        private string executorAppointmentDateField;

        private string estateField;

        private string resignationDateField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string DirectorID
        {
            get
            {
                return this.directorIDField;
            }
            set
            {
                this.directorIDField = value;
            }
        }

        /// <remarks/>
        public string IDNo
        {
            get
            {
                return this.iDNoField;
            }
            set
            {
                this.iDNoField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string Initials
        {
            get
            {
                return this.initialsField;
            }
            set
            {
                this.initialsField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string SecondName
        {
            get
            {
                return this.secondNameField;
            }
            set
            {
                this.secondNameField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        //public System.DateTime BirthDate
        public string BirthDate
        {
            get
            {
                return this.birthDateField;
            }
            set
            {
                this.birthDateField = value;
            }
        }

        /// <remarks/>
        public string DirectorStatusCode
        {
            get
            {
                return this.directorStatusCodeField;
            }
            set
            {
                this.directorStatusCodeField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        //public System.DateTime AppointmentDate
        public string AppointmentDate
        {
            get
            {
                return this.appointmentDateField;
            }
            set
            {
                this.appointmentDateField = value;
            }
        }

        /// <remarks/>
        public string Designation
        {
            get
            {
                return this.designationField;
        }
            set
            {
                this.designationField = value;
            }
        }

        /// <remarks/>
        public string MemberSize
        {
            get
            {
                return this.memberSizeField;
            }
            set
            {
                this.memberSizeField = value;
    }
        }

        /// <remarks/>
        public string MemberControlPerc
        {
            get
            {
                return this.memberControlPercField;
            }
            set
            {
                this.memberControlPercField = value;
            }
        }

        /// <remarks/>
        public string DirectorIndicator
        {
            get
            {
                return this.directorIndicatorField;
            }
            set
            {
                this.directorIndicatorField = value;
            }
        }

        /// <remarks/>
        public string PrincipalType
        {
            get
            {
                return this.principalTypeField;
            }
            set
            {
                this.principalTypeField = value;
            }
        }

        /// <remarks/>
        public string CM29Date
        {
            get
            {
                return this.cM29DateField;
            }
            set
            {
                this.cM29DateField = value;
            }
        }

        /// <remarks/>
        public string ISRSAResident
        {
            get
            {
                return this.iSRSAResidentField;
            }
            set
            {
                this.iSRSAResidentField = value;
            }
        }

        /// <remarks/>
        public string CountryCode
        {
            get
            {
                return this.countryCodeField;
            }
            set
            {
                this.countryCodeField = value;
            }
        }

        /// <remarks/>
        public string ISIDVerified
        {
            get
            {
                return this.iSIDVerifiedField;
            }
            set
            {
                this.iSIDVerifiedField = value;
            }
        }

        /// <remarks/>
        public string ISCIPROConfirmed
        {
            get
            {
                return this.iSCIPROConfirmedField;
            }
            set
            {
                this.iSCIPROConfirmedField = value;
            }
        }

        /// <remarks/>
        public string PhysicalAddress
        {
            get
            {
                return this.physicalAddressField;
            }
            set
            {
                this.physicalAddressField = value;
            }
        }

        /// <remarks/>
        public string PostalAddress
        {
            get
            {
                return this.postalAddressField;
            }
            set
            {
                this.postalAddressField = value;
            }
        }

        /// <remarks/>
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public string HomeTelephoneNo
        {
            get
            {
                return this.homeTelephoneNoField;
            }
            set
            {
                this.homeTelephoneNoField = value;
            }
        }

        /// <remarks/>
        public string WorkTelephoneNo
        {
            get
            {
                return this.workTelephoneNoField;
            }
            set
            {
                this.workTelephoneNoField = value;
            }
        }

        /// <remarks/>
        public string CellularNo
        {
            get
            {
                return this.cellularNoField;
            }
            set
            {
                this.cellularNoField = value;
            }
        }

        /// <remarks/>
        public string EmailAddress
        {
            get
            {
                return this.emailAddressField;
            }
            set
            {
                this.emailAddressField = value;
            }
        }

        /// <remarks/>
        public string Age
        {
            get
            {
                return this.ageField;
            }
            set
            {
                this.ageField = value;
            }
        }

        /// <remarks/>
        public string YearsWithBusiness
        {
            get
            {
                return this.yearsWithBusinessField;
            }
            set
            {
                this.yearsWithBusinessField = value;
            }
        }

        /// <remarks/>
        public string Fullname
        {
            get
            {
                return this.fullnameField;
            }
            set
            {
                this.fullnameField = value;
            }
        }

        /// <remarks/>
        public string SurnamePrevious
        {
            get
            {
                return this.surnamePreviousField;
            }
            set
            {
                this.surnamePreviousField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        //public System.DateTime DirectorStatusDate
        public string DirectorStatusDate
        {
            get
            {
                return this.directorStatusDateField;
            }
            set
            {
                this.directorStatusDateField = value;
            }
        }

        /// <remarks/>
        public string MemberControlType
        {
            get
            {
                return this.memberControlTypeField;
            }
            set
            {
                this.memberControlTypeField = value;
            }
        }

        /// <remarks/>
        public string Executor
        {
            get
            {
                return this.executorField;
            }
            set
            {
                this.executorField = value;
            }
        }

        /// <remarks/>
        public string ExecutorAppointmentDate
        {
            get
            {
                return this.executorAppointmentDateField;
            }
            set
            {
                this.executorAppointmentDateField = value;
            }
        }

        /// <remarks/>
        public string Estate
        {
            get
            {
                return this.estateField;
            }
            set
            {
                this.estateField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        //public System.DateTime ResignationDate
        public string ResignationDate
        {
            get
            {
                return this.resignationDateField;
            }
            set
            {
                this.resignationDateField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerInactivePrincipalInformation
    {

        private string displayTextField;

        private string directorIDField;

        private string iDNoField;

        private string firstNameField;

        private string initialsField;

        private string surnameField;

        private string secondNameField;

        private string fullNameField;

        private string birthDateField;

        private string directorStatusCodeField;

        private string resignationDateField;

        private string designationField;

        private string memberSizeField;

        private string memberControlPercField;

        private string directorIndicatorField;

        private string principalTypeField;

        private string cM29DateField;

        private string iSRSAResidentField;

        private string countryCodeField;

        private string iSIDVerifiedField;

        private string iSCIPROConfirmedField;

        private string physicalAddressField;

        private string postalAddressField;

        private string nameField;

        private string homeTelephoneNoField;

        private string workTelephoneNoField;

        private string cellularNoField;

        private string emailAddressField;

        private string ageField;

        private string yearsWithBusField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string DirectorID
        {
            get
            {
                return this.directorIDField;
            }
            set
            {
                this.directorIDField = value;
            }
        }

        /// <remarks/>
        public string IDNo
        {
            get
            {
                return this.iDNoField;
            }
            set
            {
                this.iDNoField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string Initials
        {
            get
            {
                return this.initialsField;
            }
            set
            {
                this.initialsField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string SecondName
        {
            get
            {
                return this.secondNameField;
            }
            set
            {
                this.secondNameField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return this.fullNameField;
            }
            set
            {
                this.fullNameField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string BirthDate
        {
            get
            {
                return this.birthDateField;
            }
            set
            {
                this.birthDateField = value;
            }
        }

        /// <remarks/>
        public string DirectorStatusCode
        {
            get
            {
                return this.directorStatusCodeField;
            }
            set
            {
                this.directorStatusCodeField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string ResignationDate
        {
            get
            {
                return this.resignationDateField;
            }
            set
            {
                this.resignationDateField = value;
            }
        }

        /// <remarks/>
        public string Designation
        {
            get
            {
                return this.designationField;
            }
            set
            {
                this.designationField = value;
            }
        }

        /// <remarks/>
        public string MemberSize
        {
            get
            {
                return this.memberSizeField;
            }
            set
            {
                this.memberSizeField = value;
            }
        }

        /// <remarks/>
        public string MemberControlPerc
        {
            get
            {
                return this.memberControlPercField;
            }
            set
            {
                this.memberControlPercField = value;
            }
        }

        /// <remarks/>
        public string DirectorIndicator
        {
            get
            {
                return this.directorIndicatorField;
            }
            set
            {
                this.directorIndicatorField = value;
            }
        }

        /// <remarks/>
        public string PrincipalType
        {
            get
            {
                return this.principalTypeField;
            }
            set
            {
                this.principalTypeField = value;
            }
        }

        /// <remarks/>
        public string CM29Date
        {
            get
            {
                return this.cM29DateField;
            }
            set
            {
                this.cM29DateField = value;
            }
        }

        /// <remarks/>
        public string ISRSAResident
        {
            get
            {
                return this.iSRSAResidentField;
            }
            set
            {
                this.iSRSAResidentField = value;
            }
        }

        /// <remarks/>
        public string CountryCode
        {
            get
            {
                return this.countryCodeField;
            }
            set
            {
                this.countryCodeField = value;
            }
        }

        /// <remarks/>
        public string ISIDVerified
        {
            get
            {
                return this.iSIDVerifiedField;
            }
            set
            {
                this.iSIDVerifiedField = value;
            }
        }

        /// <remarks/>
        public string ISCIPROConfirmed
        {
            get
            {
                return this.iSCIPROConfirmedField;
            }
            set
            {
                this.iSCIPROConfirmedField = value;
            }
        }

        /// <remarks/>
        public string PhysicalAddress
        {
            get
            {
                return this.physicalAddressField;
            }
            set
            {
                this.physicalAddressField = value;
            }
        }

        /// <remarks/>
        public string PostalAddress
        {
            get
            {
                return this.postalAddressField;
            }
            set
            {
                this.postalAddressField = value;
            }
        }

        /// <remarks/>
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public string HomeTelephoneNo
        {
            get
            {
                return this.homeTelephoneNoField;
            }
            set
            {
                this.homeTelephoneNoField = value;
            }
        }

        /// <remarks/>
        public string WorkTelephoneNo
        {
            get
            {
                return this.workTelephoneNoField;
            }
            set
            {
                this.workTelephoneNoField = value;
            }
        }

        /// <remarks/>
        public string CellularNo
        {
            get
            {
                return this.cellularNoField;
            }
            set
            {
                this.cellularNoField = value;
            }
        }

        /// <remarks/>
        public string EmailAddress
        {
            get
            {
                return this.emailAddressField;
            }
            set
            {
                this.emailAddressField = value;
            }
        }

        /// <remarks/>
        public string Age
        {
            get
            {
                return this.ageField;
            }
            set
            {
                this.ageField = value;
            }
        }

        /// <remarks/>
        public string YearsWithBus
        {
            get
            {
                return this.yearsWithBusField;
            }
            set
            {
                this.yearsWithBusField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerInactiveDirectorJudgments
    {

        private string displayTextField;

        private string directorIDField;

        private string caseNumberField;

        private string disputeAmtField;

        private string courtNameField;

        private string caseTypeField;

        private string judgmentDateField;

        private string plaintiffNameField;

        private string attorneyNameField;

        private string telephoneNoField;

        private string caseReasonField;

        private string iDNoField;

        private string firstNameField;

        private string surnameField;

        private string commentField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string DirectorID
        {
            get
            {
                return this.directorIDField;
            }
            set
            {
                this.directorIDField = value;
            }
        }

        /// <remarks/>
        public string CaseNumber
        {
            get
            {
                return this.caseNumberField;
            }
            set
            {
                this.caseNumberField = value;
            }
        }

        /// <remarks/>
        public string DisputeAmt
        {
            get
            {
                return this.disputeAmtField;
            }
            set
            {
                this.disputeAmtField = value;
            }
        }

        /// <remarks/>
        public string CourtName
        {
            get
            {
                return this.courtNameField;
            }
            set
            {
                this.courtNameField = value;
            }
        }

        /// <remarks/>
        public string CaseType
        {
            get
            {
                return this.caseTypeField;
            }
            set
            {
                this.caseTypeField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string JudgmentDate
        {
            get
            {
                return this.judgmentDateField;
            }
            set
            {
                this.judgmentDateField = value;
            }
        }

        /// <remarks/>
        public string PlaintiffName
        {
            get
            {
                return this.plaintiffNameField;
            }
            set
            {
                this.plaintiffNameField = value;
            }
        }

        /// <remarks/>
        public string AttorneyName
        {
            get
            {
                return this.attorneyNameField;
            }
            set
            {
                this.attorneyNameField = value;
            }
        }

        /// <remarks/>
        public string TelephoneNo
        {
            get
            {
                return this.telephoneNoField;
            }
            set
            {
                this.telephoneNoField = value;
            }
        }

        /// <remarks/>
        public string CaseReason
        {
            get
            {
                return this.caseReasonField;
            }
            set
            {
                this.caseReasonField = value;
            }
        }

        /// <remarks/>
        public string IDNo
        {
            get
            {
                return this.iDNoField;
            }
            set
            {
                this.iDNoField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerCommercialSubscriberInputDetails
    {

        private System.DateTime enquiryDateField;

        private string enquiryTypeField;

        private string subscriberNameField;

        private string subscriberUserNameField;

        private string enquiryInputField;

        private string trustNumberField;

        private string prebuiltPackageField;

        private string countryField;

        private string registrationNumberField;

        private string businessNameField;

        private string vatNumberField;

        private string iDNumberField;

        private string nPONumberField;

        private string externalReferenceField;

        private string referenceField;

        private string legalEntityField;

        /// <remarks/>
        public System.DateTime EnquiryDate
        {
            get
            {
                return this.enquiryDateField;
            }
            set
            {
                this.enquiryDateField = value;
            }
        }

        /// <remarks/>
        public string EnquiryType
        {
            get
            {
                return this.enquiryTypeField;
            }
            set
            {
                this.enquiryTypeField = value;
            }
        }

        /// <remarks/>
        public string SubscriberName
        {
            get
            {
                return this.subscriberNameField;
            }
            set
            {
                this.subscriberNameField = value;
            }
        }

        /// <remarks/>
        public string SubscriberUserName
        {
            get
            {
                return this.subscriberUserNameField;
            }
            set
            {
                this.subscriberUserNameField = value;
            }
        }

        /// <remarks/>
        public string EnquiryInput
        {
            get
            {
                return this.enquiryInputField;
            }
            set
            {
                this.enquiryInputField = value;
            }
        }

        /// <remarks/>
        public string TrustNumber
        {
            get
            {
                return this.trustNumberField;
            }
            set
            {
                this.trustNumberField = value;
            }
        }

        /// <remarks/>
        public string PrebuiltPackage
        {
            get
            {
                return this.prebuiltPackageField;
            }
            set
            {
                this.prebuiltPackageField = value;
            }
        }

        /// <remarks/>
        public string Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>
        public string RegistrationNumber
        {
            get
            {
                return this.registrationNumberField;
            }
            set
            {
                this.registrationNumberField = value;
            }
        }

        /// <remarks/>
        public string BusinessName
        {
            get
            {
                return this.businessNameField;
            }
            set
            {
                this.businessNameField = value;
            }
        }

        /// <remarks/>
        public string VatNumber
        {
            get
            {
                return this.vatNumberField;
            }
            set
            {
                this.vatNumberField = value;
            }
        }

        /// <remarks/>
        public string IDNumber
        {
            get
            {
                return this.iDNumberField;
            }
            set
            {
                this.iDNumberField = value;
            }
        }

        /// <remarks/>
        public string NPONumber
        {
            get
            {
                return this.nPONumberField;
            }
            set
            {
                this.nPONumberField = value;
            }
        }

        /// <remarks/>
        public string ExternalReference
        {
            get
            {
                return this.externalReferenceField;
            }
            set
            {
                this.externalReferenceField = value;
            }
        }

        /// <remarks/>
        public string Reference
        {
            get
            {
                return this.referenceField;
            }
            set
            {
                this.referenceField = value;
            }
        }

        /// <remarks/>
        public string LegalEntity
        {
            get
            {
                return this.legalEntityField;
            }
            set
            {
                this.legalEntityField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CommercialVATInformation
    {
        private string directorID;
        private string caseNumber;
        private string disputeAmt;
        private string courtName;
        private string caseType;
        private string judgementDate;
        private string plaintiffName;
        private string attorneyName;
        private string telephoneNo;
        private string caseReason;

        public string CaseReason
        {
            get { return caseReason; }
            set { caseReason = value; }
        }

        public string TelephoneNo
        {
            get { return telephoneNo; }
            set { telephoneNo = value; }
        }

        public string AttorneyName
        {
            get { return attorneyName; }
            set { attorneyName = value; }
        }

        public string PlaintiffName
        {
            get { return plaintiffName; }
            set { plaintiffName = value; }
        }

        public string JudgementDate
        {
            get { return judgementDate; }
            set { judgementDate = value; }
        }

        public string CaseType
        {
            get { return caseType; }
            set { caseType = value; }
        }

        public string CourtName
        {
            get { return courtName; }
            set { courtName = value; }
        }

        public string DisputeAmt
        {
            get { return disputeAmt; }
            set { disputeAmt = value; }
        }

        public string CaseNumber
        {
            get { return caseNumber; }
            set { caseNumber = value; }
        }

        public string DirectorID
        {
            get { return directorID; }
            set { directorID = value; }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class DirectorConsumerDetail
    {
        private string displayTextField;
        private uint consumerIDField;
        private string initialsField;
        private string firstNameField;
        private string secondNameField;
        private object thirdNameField;
        private string surnameField;
        private string iDNoField;
        private string passportNoField;
        //private System.DateTime? birthDateField;
        private string birthDateField;
        private string genderField;
        private string titleDescField;
        private string maritalStatusDescField;
        private string privacyStatusField;
        private string residentialAddressField;
        private string postalAddressField;
        //private uint homeTelephoneNoField;
        //private uint workTelephoneNoField;
        //private uint cellularNoField;
        private string homeTelephoneNoField;
        private string workTelephoneNoField;
        private string cellularNoField;
        private string emailAddressField;
        private string employerDetailField;
        private string referenceNoField;
        private string externalReferenceField;
        private string noOfEnquiries;

        public string NoOfEnquiries
        {
            get { return noOfEnquiries; }
            set { noOfEnquiries = value; }
        }


        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public uint ConsumerID
        {
            get
            {
                return this.consumerIDField;
            }
            set
            {
                this.consumerIDField = value;
            }
        }

        /// <remarks/>
        public string Initials
        {
            get
            {
                return this.initialsField;
            }
            set
            {
                this.initialsField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string SecondName
        {
            get
            {
                return this.secondNameField;
            }
            set
            {
                this.secondNameField = value;
            }
        }

        /// <remarks/>
        public object ThirdName
        {
            get
            {
                return this.thirdNameField;
            }
            set
            {
                this.thirdNameField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string IDNo
        {
            get
            {
                return this.iDNoField;
            }
            set
            {
                this.iDNoField = value;
            }
        }

        /// <remarks/>
        public string PassportNo
        {
            get
            {
                return this.passportNoField;
            }
            set
            {
                this.passportNoField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        //public System.DateTime? BirthDate
        public string BirthDate
        {
            get
            {
                return this.birthDateField;

                //return BirthDate.HasValue ? BirthDate.Value : new DateTime(9999, 12, 31);
                //return string.IsNullOrEmpty(BirthDate.Value.ToString()) ? new DateTime(9999, 12, 31) : BirthDate.Value;
                //return BirthDate.HasValue ? BirthDate.Value : null;

            }
            set
            {
                this.birthDateField = value;
                //this.birthDateField = string.IsNullOrEmpty(BirthDate.Value.ToString()) ? new DateTime(9999, 12, 31) : value;
            }
        }

        /// <remarks/>
        public string Gender
        {
            get
            {
                return this.genderField;
            }
            set
            {
                this.genderField = value;
            }
        }

        /// <remarks/>
        public string TitleDesc
        {
            get
            {
                return this.titleDescField;
            }
            set
            {
                this.titleDescField = value;
            }
        }

        /// <remarks/>
        public string MaritalStatusDesc
        {
            get
            {
                return this.maritalStatusDescField;
            }
            set
            {
                this.maritalStatusDescField = value;
            }
        }

        /// <remarks/>
        public string PrivacyStatus
        {
            get
            {
                return this.privacyStatusField;
            }
            set
            {
                this.privacyStatusField = value;
            }
        }

        /// <remarks/>
        public string ResidentialAddress
        {
            get
            {
                return this.residentialAddressField;
            }
            set
            {
                this.residentialAddressField = value;
            }
        }

        /// <remarks/>
        public string PostalAddress
        {
            get
            {
                return this.postalAddressField;
            }
            set
            {
                this.postalAddressField = value;
            }
        }

        /// <remarks/>
        //public uint HomeTelephoneNo
        public string HomeTelephoneNo
        {
            get
            {
                return this.homeTelephoneNoField;
            }
            set
            {
                this.homeTelephoneNoField = value;
            }
        }

        /// <remarks/>
        //public uint WorkTelephoneNo
        public string WorkTelephoneNo
        {
            get
            {
                return this.workTelephoneNoField;
            }
            set
            {
                this.workTelephoneNoField = value;
            }
        }

        /// <remarks/>
        //public uint CellularNo
        public string CellularNo
        {
            get
            {
                return this.cellularNoField;
            }
            set
            {
                this.cellularNoField = value;
            }
        }

        /// <remarks/>
        public string EmailAddress
        {
            get
            {
                return this.emailAddressField;
            }
            set
            {
                this.emailAddressField = value;
            }
        }

        /// <remarks/>
        public string EmployerDetail
        {
            get
            {
                return this.employerDetailField;
            }
            set
            {
                this.employerDetailField = value;
            }
        }

        /// <remarks/>
        public string ReferenceNo
        {
            get
            {
                return this.referenceNoField;
            }
            set
            {
                this.referenceNoField = value;
            }
        }

        /// <remarks/>
        public string ExternalReference
        {
            get
            {
                return this.externalReferenceField;
            }
            set
            {
                this.externalReferenceField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerCreditActiveNess
    {

        private string displayTextField;

        private string categoryDescriptionField;

        private string lastActivity_Months_0_3Field;

        private string lastActivity_Months_3_6Field;

        private string lastActivity_Months_6_9Field;

        private string lastActivity_Months_12orMoreField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string CategoryDescription
        {
            get
            {
                return this.categoryDescriptionField;
            }
            set
            {
                this.categoryDescriptionField = value;
            }
        }

        /// <remarks/>
        public string LastActivity_Months_0_3
        {
            get
            {
                return this.lastActivity_Months_0_3Field;
            }
            set
            {
                this.lastActivity_Months_0_3Field = value;
            }
        }

        /// <remarks/>
        public string LastActivity_Months_3_6
        {
            get
            {
                return this.lastActivity_Months_3_6Field;
            }
            set
            {
                this.lastActivity_Months_3_6Field = value;
            }
        }

        /// <remarks/>
        public string LastActivity_Months_6_9
        {
            get
            {
                return this.lastActivity_Months_6_9Field;
            }
            set
            {
                this.lastActivity_Months_6_9Field = value;
            }
        }

        /// <remarks/>
        public string LastActivity_Months_12orMore
        {
            get
            {
                return this.lastActivity_Months_12orMoreField;
            }
            set
            {
                this.lastActivity_Months_12orMoreField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerIndustryPayments
    {

        private string displayTextField;

        private string industryDescriptionField;

        private string noofAccountsField;

        private string paid_Months_0_3Field;

        private string paid_Months_3_6Field;

        private string paid_Months_6_9Field;

        private string paid_Months_12orMoreField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string IndustryDescription
        {
            get
            {
                return this.industryDescriptionField;
            }
            set
            {
                this.industryDescriptionField = value;
            }
        }

        /// <remarks/>
        public string NoofAccounts
        {
            get
            {
                return this.noofAccountsField;
            }
            set
            {
                this.noofAccountsField = value;
            }
        }

        /// <remarks/>
        public string Paid_Months_0_3
        {
            get
            {
                return this.paid_Months_0_3Field;
            }
            set
            {
                this.paid_Months_0_3Field = value;
            }
        }

        /// <remarks/>
        public string Paid_Months_3_6
        {
            get
            {
                return this.paid_Months_3_6Field;
            }
            set
            {
                this.paid_Months_3_6Field = value;
            }
        }

        /// <remarks/>
        public string Paid_Months_6_9
        {
            get
            {
                return this.paid_Months_6_9Field;
            }
            set
            {
                this.paid_Months_6_9Field = value;
            }
        }

        /// <remarks/>
        public string Paid_Months_12orMore
        {
            get
            {
                return this.paid_Months_12orMoreField;
            }
            set
            {
                this.paid_Months_12orMoreField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerPaymentGAP
    {

        private string displayTextField;

        private int day1Field;

        private int day2Field;

        private int day3Field;

        private int day4Field;

        private int day5Field;

        private int day1PercentField;

        private int day2PercentField;

        private int day3PercentField;

        private int day4PercentField;

        private int day5PercentField;

        private string exclusionReasonField;

        private int reliabilityGroupField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public int Day1
        {
            get
            {
                return this.day1Field;
            }
            set
            {
                this.day1Field = value;
            }
        }

        /// <remarks/>
        public int Day2
        {
            get
            {
                return this.day2Field;
            }
            set
            {
                this.day2Field = value;
            }
        }

        /// <remarks/>
        public int Day3
        {
            get
            {
                return this.day3Field;
            }
            set
            {
                this.day3Field = value;
            }
        }

        /// <remarks/>
        public int Day4
        {
            get
            {
                return this.day4Field;
            }
            set
            {
                this.day4Field = value;
            }
        }

        /// <remarks/>
        public int Day5
        {
            get
            {
                return this.day5Field;
            }
            set
            {
                this.day5Field = value;
            }
        }

        /// <remarks/>
        public int Day1Percent
        {
            get
            {
                return this.day1PercentField;
            }
            set
            {
                this.day1PercentField = value;
            }
        }

        /// <remarks/>
        public int Day2Percent
        {
            get
            {
                return this.day2PercentField;
            }
            set
            {
                this.day2PercentField = value;
            }
        }

        /// <remarks/>
        public int Day3Percent
        {
            get
            {
                return this.day3PercentField;
            }
            set
            {
                this.day3PercentField = value;
            }
        }

        /// <remarks/>
        public int Day4Percent
        {
            get
            {
                return this.day4PercentField;
            }
            set
            {
                this.day4PercentField = value;
            }
        }

        /// <remarks/>
        public int Day5Percent
        {
            get
            {
                return this.day5PercentField;
            }
            set
            {
                this.day5PercentField = value;
            }
        }

        /// <remarks/>
        public string ExclusionReason
        {
            get
            {
                return this.exclusionReasonField;
            }
            set
            {
                this.exclusionReasonField = value;
            }
        }

        /// <remarks/>
        public int ReliabilityGroup
        {
            get
            {
                return this.reliabilityGroupField;
            }
            set
            {
                this.reliabilityGroupField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerCollectionConsumerSummary
    {

        private string displayTextField;

        private int possibleContactabilityGroupField;

        private string possibleContactabilityGroupDescField;

        private string bestPossibleTelephoneNoField;

        private string bestPossibleTelephoneTypeIndField;

        private int possibleRepmtGroupField;

        private string possibleRepmtGroupDescField;

        private decimal possibleRePmtAmtField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public int PossibleContactabilityGroup
        {
            get
            {
                return this.possibleContactabilityGroupField;
            }
            set
            {
                this.possibleContactabilityGroupField = value;
            }
        }

        /// <remarks/>
        public string PossibleContactabilityGroupDesc
        {
            get
            {
                return this.possibleContactabilityGroupDescField;
            }
            set
            {
                this.possibleContactabilityGroupDescField = value;
            }
        }

        /// <remarks/>
        public string BestPossibleTelephoneNo
        {
            get
            {
                return this.bestPossibleTelephoneNoField;
            }
            set
            {
                this.bestPossibleTelephoneNoField = value;
            }
        }

        /// <remarks/>
        public string BestPossibleTelephoneTypeInd
        {
            get
            {
                return this.bestPossibleTelephoneTypeIndField;
            }
            set
            {
                this.bestPossibleTelephoneTypeIndField = value;
            }
        }

        /// <remarks/>
        public int PossibleRepmtGroup
        {
            get
            {
                return this.possibleRepmtGroupField;
            }
            set
            {
                this.possibleRepmtGroupField = value;
            }
        }

        /// <remarks/>
        public string PossibleRepmtGroupDesc
        {
            get
            {
                return this.possibleRepmtGroupDescField;
            }
            set
            {
                this.possibleRepmtGroupDescField = value;
            }
        }

        /// <remarks/>
        public decimal PossibleRePmtAmt
        {
            get
            {
                return this.possibleRePmtAmtField;
            }
            set
            {
                this.possibleRePmtAmtField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerAffordability
    {

        private string displayTextField;

        private string consumerIDField;

        private string predictedIncomeField;

        private string exclusionReasonField;

        private string predAvailableinstalmentField;

        private string totalCommitmentsField;

        private string estimatedExpensesField;

        private string totalPredictedExpensesField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string ConsumerID
        {
            get
            {
                return this.consumerIDField;
            }
            set
            {
                this.consumerIDField = value;
            }
        }

        /// <remarks/>
        public string PredictedIncome
        {
            get
            {
                return this.predictedIncomeField;
            }
            set
            {
                this.predictedIncomeField = value;
            }
        }

        /// <remarks/>
        public string ExclusionReason
        {
            get
            {
                return this.exclusionReasonField;
            }
            set
            {
                this.exclusionReasonField = value;
            }
        }

        /// <remarks/>
        public string PredAvailableinstalment
        {
            get
            {
                return this.predAvailableinstalmentField;
            }
            set
            {
                this.predAvailableinstalmentField = value;
            }
        }

        /// <remarks/>
        public string TotalCommitments
        {
            get
            {
                return this.totalCommitmentsField;
            }
            set
            {
                this.totalCommitmentsField = value;
            }
        }

        /// <remarks/>
        public string EstimatedExpenses
        {
            get
            {
                return this.estimatedExpensesField;
            }
            set
            {
                this.estimatedExpensesField = value;
            }
        }

        /// <remarks/>
        public string TotalPredictedExpenses
        {
            get
            {
                return this.totalPredictedExpensesField;
            }
            set
            {
                this.totalPredictedExpensesField = value;
            }
        }
    }


    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerEmploymentConfirmation
    {

        private string displayTextField;

        private string employerDetailField;

        private string designationField;

        private string dateConfirmedField;

        private string verifiedDateField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string EmployerDetail
        {
            get
            {
                return this.employerDetailField;
            }
            set
            {
                this.employerDetailField = value;
            }
        }

        /// <remarks/>
        public string Designation
        {
            get
            {
                return this.designationField;
            }
            set
            {
                this.designationField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string DateConfirmed
        {
            get
            {
                return this.dateConfirmedField;
            }
            set
            {
                this.dateConfirmedField = value;
            }
        }

        /// <remarks/>
        public string VerifiedDate
        {
            get
            {
                return this.verifiedDateField;
            }
            set
            {
                this.verifiedDateField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerOtherContactInfoTelephone
    {

        private string displayTextField;

        private string telephoneNumberField;

        private string telephoneTypeIndField;

        private string lastUpdatedDateField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string TelephoneNumber
        {
            get
            {
                return this.telephoneNumberField;
            }
            set
            {
                this.telephoneNumberField = value;
            }
        }

        /// <remarks/>
        public string TelephoneTypeInd
        {
            get
            {
                return this.telephoneTypeIndField;
            }
            set
            {
                this.telephoneTypeIndField = value;
        }
    }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string LastUpdatedDate
        {
            get
            {
                return this.lastUpdatedDateField;
            }
            set
            {
                this.lastUpdatedDateField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class DirectorEnquiryHistory
    {

        private string displayTextField;

        //private System.DateTime enquiryDateField;
        private string enquiryDateField;

        private string subscriberNameField;

        private string subscriberBusinessTypeDescField;

        private string subscriberContactField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        //public System.DateTime EnquiryDate
        //{
        //    get
        //    {
        //        return this.enquiryDateField;
        //    }
        //    set
        //    {
        //        this.enquiryDateField = value;
        //    }
        //}

        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string EnquiryDate
        {
            get
            {
                return this.enquiryDateField;
            }
            set
            {
                this.enquiryDateField = value;
            }
        }

        /// <remarks/>
        public string SubscriberName
        {
            get
            {
                return this.subscriberNameField;
            }
            set
            {
                this.subscriberNameField = value;
            }
        }

        /// <remarks/>
        public string SubscriberBusinessTypeDesc
        {
            get
            {
                return this.subscriberBusinessTypeDescField;
            }
            set
            {
                this.subscriberBusinessTypeDescField = value;
            }
        }

        /// <remarks/>
        public string SubscriberContact
        {
            get
            {
                return this.subscriberContactField;
            }
            set
            {
                this.subscriberContactField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class PropertyReportInformation
    {

        private byte reportIDField;

        private string reportNameField;

        /// <remarks/>
        public byte ReportID
        {
            get
            {
                return this.reportIDField;
            }
            set
            {
                this.reportIDField = value;
            }
        }

        /// <remarks/>
        public string ReportName
        {
            get
            {
                return this.reportNameField;
            }
            set
            {
                this.reportNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class PropertyPropertyInformation
    {

        private string displayTextField;

        private string referenceNoField;

        private string externalReferenceNoField;

        private string propertyTypeField;

        private string standNoField;

        private string streetNumberField;

        private string streetNameField;

        private string suburbField;

        private string cityField;

        private string townshipNameField;

        private string provinceField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string ReferenceNo
        {
            get
            {
                return this.referenceNoField;
            }
            set
            {
                this.referenceNoField = value;
            }
        }

        /// <remarks/>
        public string ExternalReferenceNo
        {
            get
            {
                return this.externalReferenceNoField;
            }
            set
            {
                this.externalReferenceNoField = value;
            }
        }

        /// <remarks/>
        public string PropertyType
        {
            get
            {
                return this.propertyTypeField;
            }
            set
            {
                this.propertyTypeField = value;
            }
        }

        /// <remarks/>
        public string StandNo
        {
            get
            {
                return this.standNoField;
            }
            set
            {
                this.standNoField = value;
            }
        }

        /// <remarks/>
        public string StreetNumber
        {
            get
            {
                return this.streetNumberField;
            }
            set
            {
                this.streetNumberField = value;
            }
        }

        /// <remarks/>
        public string StreetName
        {
            get
            {
                return this.streetNameField;
            }
            set
            {
                this.streetNameField = value;
            }
        }

        /// <remarks/>
        public string Suburb
        {
            get
            {
                return this.suburbField;
            }
            set
            {
                this.suburbField = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        public string TownshipName
        {
            get
            {
                return this.townshipNameField;
            }
            set
            {
                this.townshipNameField = value;
            }
        }

        /// <remarks/>
        public string Province
        {
            get
            {
                return this.provinceField;
            }
            set
            {
                this.provinceField = value;
            }
        }
    }

    /// <remarks/>

    public partial class PropertyPropertyRegistrationInformation
    {

        private string displayTextField;

        private string transferIDField;

        private string titleDeedNoField;

        private string oldtitleDeednoField;

        private string transferDateField;

        private string purchaseDateField;

        private string  soldPriceField;

        private string erfSizeField;

        private string deedsOfficeField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string TransferID
        {
            get
            {
                return this.transferIDField;
            }
            set
            {
                this.transferIDField = value;
            }
        }

        /// <remarks/>
        public string TitleDeedNo
        {
            get
            {
                return this.titleDeedNoField;
            }
            set
            {
                this.titleDeedNoField = value;
            }
        }

        /// <remarks/>
        public string OldtitleDeedno
        {
            get
            {
                return this.oldtitleDeednoField;
            }
            set
            {
                this.oldtitleDeednoField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string TransferDate
        {
            get
            {
                return this.transferDateField;
            }
            set
            {
                this.transferDateField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string PurchaseDate
        {
            get
            {
                return this.purchaseDateField;
            }
            set
            {
                this.purchaseDateField = value;
            }
        }

        /// <remarks/>
        public string  SoldPrice
        {
            get
            {
                return this.soldPriceField;
            }
            set
            {
                this.soldPriceField = value;
            }
        }

        /// <remarks/>
        public string ErfSize
        {
            get
            {
                return this.erfSizeField;
            }
            set
            {
                this.erfSizeField = value;
            }
        }

        /// <remarks/>
        public string DeedsOffice
        {
            get
            {
                return this.deedsOfficeField;
            }
            set
            {
                this.deedsOfficeField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class PropertyBuyerandSellerInformation
    {

        private string displayTextField;

        private string buyerTypeCodeField;

        private string buyerIDNoField;

        private string buyerNameField;

        private string buyerMaritalStatusField;

        private string sellerTypeCodeField;

        private string sellerIDNoField;

        private string sellerNameField;

        private string sellerMaritalStatusField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string BuyerTypeCode
        {
            get
            {
                return this.buyerTypeCodeField;
            }
            set
            {
                this.buyerTypeCodeField = value;
            }
        }

        /// <remarks/>
        public string BuyerIDNo
        {
            get
            {
                return this.buyerIDNoField;
            }
            set
            {
                this.buyerIDNoField = value;
            }
        }

        /// <remarks/>
        public string BuyerName
        {
            get
            {
                return this.buyerNameField;
            }
            set
            {
                this.buyerNameField = value;
            }
        }

        /// <remarks/>
        public string BuyerMaritalStatus
        {
            get
            {
                return this.buyerMaritalStatusField;
            }
            set
            {
                this.buyerMaritalStatusField = value;
            }
        }

        /// <remarks/>
        public string SellerTypeCode
        {
            get
            {
                return this.sellerTypeCodeField;
            }
            set
            {
                this.sellerTypeCodeField = value;
            }
        }

        /// <remarks/>
        public string SellerIDNo
        {
            get
            {
                return this.sellerIDNoField;
            }
            set
            {
                this.sellerIDNoField = value;
            }
        }

        /// <remarks/>
        public string SellerName
        {
            get
            {
                return this.sellerNameField;
            }
            set
            {
                this.sellerNameField = value;
            }
        }

        /// <remarks/>
        public string SellerMaritalStatus
        {
            get
            {
                return this.sellerMaritalStatusField;
            }
            set
            {
                this.sellerMaritalStatusField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class PropertyBondInformation
    {

        private string displayTextField;

        private string bondHoldernameField;

        private string bondAmountField;

        private string bondAccountNoField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string BondHoldername
        {
            get
            {
                return this.bondHoldernameField;
            }
            set
            {
                this.bondHoldernameField = value;
            }
        }

        /// <remarks/>
        public string BondAmount
        {
            get
            {
                return this.bondAmountField;
            }
            set
            {
                this.bondAmountField = value;
            }
        }

        /// <remarks/>
        public string BondAccountNo
        {
            get
            {
                return this.bondAccountNoField;
            }
            set
            {
                this.bondAccountNoField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class PropertyAttorneyInformation
    {

        private string displayTextField;

        private string attorneyFirmNoField;

        private string attorneyFileNoField;

        private string externalSourceUpdatedDateField;

        private string titleDeedFeeField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string AttorneyFirmNo
        {
            get
            {
                return this.attorneyFirmNoField;
            }
            set
            {
                this.attorneyFirmNoField = value;
            }
        }

        /// <remarks/>
        public string AttorneyFileNo
        {
            get
            {
                return this.attorneyFileNoField;
            }
            set
            {
                this.attorneyFileNoField = value;
            }
        }

        /// <remarks/>
        public string ExternalSourceUpdatedDate
        {
            get
            {
                return this.externalSourceUpdatedDateField;
            }
            set
            {
                this.externalSourceUpdatedDateField = value;
            }
        }

        /// <remarks/>
        public string TitleDeedFee
        {
            get
            {
                return this.titleDeedFeeField;
            }
            set
            {
                this.titleDeedFeeField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class PropertyPropertyHistory
    {

        private string displayTextField;

        private string propertyTypeField;

        private string titleDeedNoField;

        private string buyerNameField;

        private string sellerNameField;

        private string soldPriceField;

        private string purchaseDateField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string PropertyType
        {
            get
            {
                return this.propertyTypeField;
            }
            set
            {
                this.propertyTypeField = value;
            }
        }

        /// <remarks/>
        public string TitleDeedNo
        {
            get
            {
                return this.titleDeedNoField;
            }
            set
            {
                this.titleDeedNoField = value;
            }
        }

        /// <remarks/>
        public string BuyerName
        {
            get
            {
                return this.buyerNameField;
            }
            set
            {
                this.buyerNameField = value;
            }
        }

        /// <remarks/>
        public string SellerName
        {
            get
            {
                return this.sellerNameField;
            }
            set
            {
                this.sellerNameField = value;
            }
        }

        /// <remarks/>
        public string SoldPrice
        {
            get
            {
                return this.soldPriceField;
            }
            set
            {
                this.soldPriceField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string PurchaseDate
        {
            get
            {
                return this.purchaseDateField;
            }
            set
            {
                this.purchaseDateField = value;
            }
        }
    }
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class PropertyBuyerHistory
    {

        private string displayTextField;

        private string propertyTypeField;

        private string provinceField;

        private string titleDeedNoField;

        private string buyerNameField;

        private string sellerNameField;

        private string standNoField;

        private string portionNoField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string PropertyType
        {
            get
            {
                return this.propertyTypeField;
            }
            set
            {
                this.propertyTypeField = value;
            }
        }

        /// <remarks/>
        public string Province
        {
            get
            {
                return this.provinceField;
            }
            set
            {
                this.provinceField = value;
            }
        }

        /// <remarks/>
        public string TitleDeedNo
        {
            get
            {
                return this.titleDeedNoField;
            }
            set
            {
                this.titleDeedNoField = value;
            }
        }

        /// <remarks/>
        public string BuyerName
        {
            get
            {
                return this.buyerNameField;
            }
            set
            {
                this.buyerNameField = value;
            }
        }

        /// <remarks/>
        public string SellerName
        {
            get
            {
                return this.sellerNameField;
            }
            set
            {
                this.sellerNameField = value;
            }
        }

        /// <remarks/>
        public string StandNo
        {
            get
            {
                return this.standNoField;
            }
            set
            {
                this.standNoField = value;
            }
        }

        /// <remarks/>
        public string PortionNo
        {
            get
            {
                return this.portionNoField;
            }
            set
            {
                this.portionNoField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class PropertySellerHistory
    {

        private string displayTextField;

        private string propertyTypeField;

        private string provinceField;

        private string titleDeedNoField;

        private string buyerNameField;

        private string sellerNameField;

        private string standNoField;

        private string portionNoField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string PropertyType
        {
            get
            {
                return this.propertyTypeField;
            }
            set
            {
                this.propertyTypeField = value;
            }
        }

        /// <remarks/>
        public string Province
        {
            get
            {
                return this.provinceField;
            }
            set
            {
                this.provinceField = value;
            }
        }

        /// <remarks/>
        public string TitleDeedNo
        {
            get
            {
                return this.titleDeedNoField;
            }
            set
            {
                this.titleDeedNoField = value;
            }
        }

        /// <remarks/>
        public string BuyerName
        {
            get
            {
                return this.buyerNameField;
            }
            set
            {
                this.buyerNameField = value;
            }
        }

        /// <remarks/>
        public string SellerName
        {
            get
            {
                return this.sellerNameField;
            }
            set
            {
                this.sellerNameField = value;
            }
        }

        /// <remarks/>
        public string StandNo
        {
            get
            {
                return this.standNoField;
            }
            set
            {
                this.standNoField = value;
            }
        }

        /// <remarks/>
        public string PortionNo
        {
            get
            {
                return this.portionNoField;
            }
            set
            {
                this.portionNoField = value;
            }
        }
    }

        
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class PropertySubscriberInputDetails
    {


        private System.DateTime enquiryDateField;

        private string enquiryTypeField;

        private string subscriberNameField;

        private string subscriberUserNameField;

        private string enquiryInputField;

        private string trustNumberField;

        private string prebuiltPackageField;

        private string countryField;

        private string registrationNumberField;

        private string businessNameField;

        private string vatNumberField;

        private string iDNumberField;

        private string nPONumberField;

        private string externalReferenceField;

        private string referenceField;

        private string legalEntityField;

        /// <remarks/>
        public System.DateTime EnquiryDate
        {
            get
            {
                return this.enquiryDateField;
            }
            set
            {
                this.enquiryDateField = value;
            }
        }

        /// <remarks/>
        public string EnquiryType
        {
            get
            {
                return this.enquiryTypeField;
            }
            set
            {
                this.enquiryTypeField = value;
            }
        }

        /// <remarks/>
        public string SubscriberName
        {
            get
            {
                return this.subscriberNameField;
            }
            set
            {
                this.subscriberNameField = value;
            }
        }

        /// <remarks/>
        public string SubscriberUserName
        {
            get
            {
                return this.subscriberUserNameField;
            }
            set
            {
                this.subscriberUserNameField = value;
            }
        }

        /// <remarks/>
        public string EnquiryInput
        {
            get
            {
                return this.enquiryInputField;
            }
            set
            {
                this.enquiryInputField = value;
            }
        }

        /// <remarks/>
        public string TrustNumber
        {
            get
            {
                return this.trustNumberField;
            }
            set
            {
                this.trustNumberField = value;
            }
        }

        /// <remarks/>
        public string PrebuiltPackage
        {
            get
            {
                return this.prebuiltPackageField;
            }
            set
            {
                this.prebuiltPackageField = value;
            }
        }

        /// <remarks/>
        public string Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>
        public string RegistrationNumber
        {
            get
            {
                return this.registrationNumberField;
            }
            set
            {
                this.registrationNumberField = value;
            }
        }

        /// <remarks/>
        public string BusinessName
        {
            get
            {
                return this.businessNameField;
            }
            set
            {
                this.businessNameField = value;
            }
        }

        /// <remarks/>
        public string VatNumber
        {
            get
            {
                return this.vatNumberField;
            }
            set
            {
                this.vatNumberField = value;
            }
        }

        /// <remarks/>
        public string IDNumber
        {
            get
            {
                return this.iDNumberField;
            }
            set
            {
                this.iDNumberField = value;
            }
        }

        /// <remarks/>
        public string NPONumber
        {
            get
            {
                return this.nPONumberField;
            }
            set
            {
                this.nPONumberField = value;
            }
        }

        /// <remarks/>
        public string ExternalReference
        {
            get
            {
                return this.externalReferenceField;
            }
            set
            {
                this.externalReferenceField = value;
            }
        }

        /// <remarks/>
        public string Reference
        {
            get
            {
                return this.referenceField;
            }
            set
            {
                this.referenceField = value;
            }
        }

        /// <remarks/>
        public string LegalEntity
        {
            get
            {
                return this.legalEntityField;
            }
            set
            {
                this.legalEntityField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CommercialChangeHistoryInformation
    {

        private string dispalyTextField;

        private string effectiveDateField;

        private string memoField;

        private string commercialChangeTypeCodeField;

        /// <remarks/>
        public string DispalyText
        {
            get
            {
                return this.dispalyTextField;
            }
            set
            {
                this.dispalyTextField = value;
            }
        }

        /// <remarks/>
        public string EffectiveDate
        {
            get
            {
                return this.effectiveDateField;
            }
            set
            {
                this.effectiveDateField = value;
            }
        }

        /// <remarks/>
        public string Memo
        {
            get
            {
                return this.memoField;
            }
            set
            {
                this.memoField = value;
            }
        }

        /// <remarks/>
        public string CommercialChangeTypeCode
        {
            get
            {
                return this.commercialChangeTypeCodeField;
            }
            set
            {
                this.commercialChangeTypeCodeField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CommercialCommercialVATInformation
    {

        private string displayTextField;

        private string vATNumberField;

        private string vATLiableDateField;

        private string statusField;

        private string commercialNameField;

        private string tradeNameField;

        //private System.DateTime lastUpdatedDateField;
        private string lastUpdatedDateField;

        private string statusChangeDateField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string VATNumber
        {
            get
            {
                return this.vATNumberField;
            }
            set
            {
                this.vATNumberField = value;
            }
        }

        /// <remarks/>
        public string VATLiableDate
        {
            get
            {
                return this.vATLiableDateField;
            }
            set
            {
                this.vATLiableDateField = value;
            }
        }

        /// <remarks/>
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        public string CommercialName
        {
            get
            {
                return this.commercialNameField;
            }
            set
            {
                this.commercialNameField = value;
            }
        }

        /// <remarks/>
        public string TradeName
        {
            get
            {
                return this.tradeNameField;
            }
            set
            {
                this.tradeNameField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        //public System.DateTime LastUpdatedDate
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string LastUpdatedDate
        {
            get
            {
                return this.lastUpdatedDateField;
            }
            set
            {
                this.lastUpdatedDateField = value;
            }
        }

        /// <remarks/>
        public string StatusChangeDate
        {
            get
            {
                return this.statusChangeDateField;
            }
            set
            {
                this.statusChangeDateField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CommercialCommercialScoring
    {

        private string displayTextField;

        private string commercialIDField;

        private string scoreDateField;

        private string finalScoreField;

        private string bandField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string CommercialID
        {
            get
            {
                return this.commercialIDField;
            }
            set
            {
                this.commercialIDField = value;
            }
        }

        /// <remarks/>
        public string ScoreDate
        {
            get
            {
                return this.scoreDateField;
            }
            set
            {
                this.scoreDateField = value;
            }
        }

        /// <remarks/>
        public string FinalScore
        {
            get
            {
                return this.finalScoreField;
            }
            set
            {
                this.finalScoreField = value;
            }
        }

        /// <remarks/>
        public string Band
        {
            get
            {
                return this.bandField;
            }
            set
            {
                this.bandField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CommercialCommercialPropertyInformation
    {

        private string displayTextField;

        private string authorityNameField;

        private string townshipNameField;

        private string standNoField;

        private string portionNoField;

        private string titleDeedNoField;

        private string buyerNameField;

        private string buyerTypeCodeField;

        private string buyerIDNoField;

        private string buyerMaritalStatusCodeField;

        private string sellerNameField;

        private string sellerTypeCodeField;

        private string sellerIDNoField;

        private string sellerMaritalStatusCodeField;

        //private System.DateTime transferDateField;
        private string transferDateField;

        private string registrarNameField;

        private string oldTitleDeedNoField;

        private string attorneyFirmNoField;

        private string attorneyFileNoField;

        private string titleDeedFeeAmtField;

        private string propertyTypeCodeField;

        private string streetNoField;

        private string streetNameField;

        private string suburbNameField;

        private string cityNameField;

        private string transferIDField;

        private string erfNoField;

        private string deedsOfficeField;

        private string physicalAddressField;

        private string propertyTypeDescField;

        private string erfSizeField;

        //private System.DateTime purchaseDateField;
        private string purchaseDateField;

        private string purchasePriceAmtField;

        private string buyerSharePercField;

        private string bondHolderNameField;

        private string bondAccountNoField;

        private string bondAmtField;

        private string farmNameField;

        private string schemeNameField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string AuthorityName
        {
            get
            {
                return this.authorityNameField;
            }
            set
            {
                this.authorityNameField = value;
            }
        }

        /// <remarks/>
        public string TownshipName
        {
            get
            {
                return this.townshipNameField;
            }
            set
            {
                this.townshipNameField = value;
            }
        }

        /// <remarks/>
        public string StandNo
        {
            get
            {
                return this.standNoField;
            }
            set
            {
                this.standNoField = value;
            }
        }

        /// <remarks/>
        public string PortionNo
        {
            get
            {
                return this.portionNoField;
            }
            set
            {
                this.portionNoField = value;
            }
        }

        /// <remarks/>
        public string TitleDeedNo
        {
            get
            {
                return this.titleDeedNoField;
            }
            set
            {
                this.titleDeedNoField = value;
            }
        }

        /// <remarks/>
        public string BuyerName
        {
            get
            {
                return this.buyerNameField;
            }
            set
            {
                this.buyerNameField = value;
            }
        }

        /// <remarks/>
        public string BuyerTypeCode
        {
            get
            {
                return this.buyerTypeCodeField;
            }
            set
            {
                this.buyerTypeCodeField = value;
            }
        }

        /// <remarks/>
        public string BuyerIDNo
        {
            get
            {
                return this.buyerIDNoField;
            }
            set
            {
                this.buyerIDNoField = value;
            }
        }

        /// <remarks/>
        public string BuyerMaritalStatusCode
        {
            get
            {
                return this.buyerMaritalStatusCodeField;
            }
            set
            {
                this.buyerMaritalStatusCodeField = value;
            }
        }

        /// <remarks/>
        public string SellerName
        {
            get
            {
                return this.sellerNameField;
            }
            set
            {
                this.sellerNameField = value;
            }
        }

        /// <remarks/>
        public string SellerTypeCode
        {
            get
            {
                return this.sellerTypeCodeField;
            }
            set
            {
                this.sellerTypeCodeField = value;
            }
        }

        /// <remarks/>
        public string SellerIDNo
        {
            get
            {
                return this.sellerIDNoField;
            }
            set
            {
                this.sellerIDNoField = value;
            }
        }

        /// <remarks/>
        public string SellerMaritalStatusCode
        {
            get
            {
                return this.sellerMaritalStatusCodeField;
            }
            set
            {
                this.sellerMaritalStatusCodeField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        //public System.DateTime TransferDate
        public string TransferDate
        {
            get
            {
                return this.transferDateField;
            }
            set
            {
                this.transferDateField = value;
            }
        }

        /// <remarks/>
        public string RegistrarName
        {
            get
            {
                return this.registrarNameField;
            }
            set
            {
                this.registrarNameField = value;
            }
        }

        /// <remarks/>
        public string OldTitleDeedNo
        {
            get
            {
                return this.oldTitleDeedNoField;
            }
            set
            {
                this.oldTitleDeedNoField = value;
            }
        }

        /// <remarks/>
        public string AttorneyFirmNo
        {
            get
            {
                return this.attorneyFirmNoField;
            }
            set
            {
                this.attorneyFirmNoField = value;
            }
        }

        /// <remarks/>
        public string AttorneyFileNo
        {
            get
            {
                return this.attorneyFileNoField;
            }
            set
            {
                this.attorneyFileNoField = value;
            }
        }

        /// <remarks/>
        public string TitleDeedFeeAmt
        {
            get
            {
                return this.titleDeedFeeAmtField;
            }
            set
            {
                this.titleDeedFeeAmtField = value;
            }
        }

        /// <remarks/>
        public string PropertyTypeCode
        {
            get
            {
                return this.propertyTypeCodeField;
            }
            set
            {
                this.propertyTypeCodeField = value;
            }
        }

        /// <remarks/>
        public string StreetNo
        {
            get
            {
                return this.streetNoField;
            }
            set
            {
                this.streetNoField = value;
            }
        }

        /// <remarks/>
        public string StreetName
        {
            get
            {
                return this.streetNameField;
            }
            set
            {
                this.streetNameField = value;
            }
        }

        /// <remarks/>
        public string SuburbName
        {
            get
            {
                return this.suburbNameField;
            }
            set
            {
                this.suburbNameField = value;
            }
        }

        /// <remarks/>
        public string CityName
        {
            get
            {
                return this.cityNameField;
            }
            set
            {
                this.cityNameField = value;
            }
        }

        /// <remarks/>
        public string TransferID
        {
            get
            {
                return this.transferIDField;
            }
            set
            {
                this.transferIDField = value;
            }
        }

        /// <remarks/>
        public string ErfNo
        {
            get
            {
                return this.erfNoField;
            }
            set
            {
                this.erfNoField = value;
            }
        }

        /// <remarks/>
        public string DeedsOffice
        {
            get
            {
                return this.deedsOfficeField;
            }
            set
            {
                this.deedsOfficeField = value;
            }
        }

        /// <remarks/>
        public string PhysicalAddress
        {
            get
            {
                return this.physicalAddressField;
            }
            set
            {
                this.physicalAddressField = value;
            }
        }

        /// <remarks/>
        public string PropertyTypeDesc
        {
            get
            {
                return this.propertyTypeDescField;
            }
            set
            {
                this.propertyTypeDescField = value;
            }
        }

        /// <remarks/>
        public string ErfSize
        {
            get
            {
                return this.erfSizeField;
            }
            set
            {
                this.erfSizeField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        //public System.DateTime PurchaseDate
        public string PurchaseDate
        {
            get
            {
                return this.purchaseDateField;
            }
            set
            {
                this.purchaseDateField = value;
            }
        }

        /// <remarks/>
        public string PurchasePriceAmt
        {
            get
            {
                return this.purchasePriceAmtField;
            }
            set
            {
                this.purchasePriceAmtField = value;
            }
        }

        /// <remarks/>
        public string BuyerSharePerc
        {
            get
            {
                return this.buyerSharePercField;
            }
            set
            {
                this.buyerSharePercField = value;
            }
        }

        /// <remarks/>
        public string BondHolderName
        {
            get
            {
                return this.bondHolderNameField;
            }
            set
            {
                this.bondHolderNameField = value;
            }
        }

        /// <remarks/>
        public string BondAccountNo
        {
            get
            {
                return this.bondAccountNoField;
            }
            set
            {
                this.bondAccountNoField = value;
            }
        }

        /// <remarks/>
        public string BondAmt
        {
            get
            {
                return this.bondAmtField;
            }
            set
            {
                this.bondAmtField = value;
            }
        }

        /// <remarks/>
        public string FarmName
        {
            get
            {
                return this.farmNameField;
            }
            set
            {
                this.farmNameField = value;
            }
        }

        /// <remarks/>
        public string SchemeName
        {
            get
            {
                return this.schemeNameField;
            }
            set
            {
                this.schemeNameField = value;
            }
        }
    }
    
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CommercialCommercialTradeReferencesInfo
    {

        private string displayTextField;

        private string createdondateField;

        private string supplierField;

        private string telephoneContactField;

        private string suretyValueField;

        private string nortarialField;

        private string commentField;

        private string ageOfAccountField;

        private string creditLimitField;

        private string maxLimitField;

        private string termsField;

        private string averagePurchasesField;

        private string termsTakenField;

        private string comment1Field;

        //private System.DateTime transferDateField;
        private string tradeReferenceNumberField;

        private string report_TradeRefIDField;

       

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string Createdondate
        {
            get
            {
                return this.createdondateField;
            }
            set
            {
                this.createdondateField = value;
            }
        }

        /// <remarks/>
        public string Supplier
        {
            get
            {
                return this.supplierField;
            }
            set
            {
                this.supplierField = value;
            }
        }

        /// <remarks/>
        public string TelephoneContact
        {
            get
            {
                return this.telephoneContactField;
            }
            set
            {
                this.telephoneContactField = value;
            }
        }

        /// <remarks/>
        public string SuretyValue
        {
            get
            {
                return this.suretyValueField;
            }
            set
            {
                this.suretyValueField = value;
            }
        }

        /// <remarks/>
        public string Nortarial
        {
            get
            {
                return this.nortarialField;
            }
            set
            {
                this.nortarialField = value;
            }
        }

        /// <remarks/>
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        public string AgeOfAccount
        {
            get
            {
                return this.ageOfAccountField;
            }
            set
            {
                this.ageOfAccountField = value;
            }
        }

        /// <remarks/>
        public string CreditLimit
        {
            get
            {
                return this.creditLimitField;
            }
            set
            {
                this.creditLimitField = value;
            }
        }

        /// <remarks/>
        public string MaxLimit
        {
            get
            {
                return this.maxLimitField;
            }
            set
            {
                this.maxLimitField = value;
            }
        }

        /// <remarks/>
        public string Terms
        {
            get
            {
                return this.termsField;
            }
            set
            {
                this.termsField = value;
            }
        }

        /// <remarks/>
        public string AveragePurchases
        {
            get
            {
                return this.averagePurchasesField;
            }
            set
            {
                this.averagePurchasesField = value;
            }
        }

        /// <remarks/>
        public string TermsTaken
        {
            get
            {
                return this.termsTakenField;
            }
            set
            {
                this.termsTakenField = value;
            }
        }

        /// <remarks/>
        public string Comment1
        {
            get
            {
                return this.comment1Field;
            }
            set
            {
                this.comment1Field = value;
            }
        }

        /// <remarks/>
      
        public string TradeReferenceNumber
        {
            get
            {
                return this.tradeReferenceNumberField;
            }
            set
            {
                this.tradeReferenceNumberField = value;
            }
        }

        /// <remarks/>
        public string Report_TradeRefID
        {
            get
            {
                return this.report_TradeRefIDField;
            }
            set
            {
                this.report_TradeRefIDField = value;
            }
        }

        /// <remarks/>
        
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CommercialCommercialBankCodeHistory
    {

        private string displayTextField;
        private string dateRequestedField;
        private string bankCodeField;
        private string bankField;
        private string equiryAmountField;
        private string currencyTypeField;
        private string bank_CommentsField;
        private string years_with_BankField;
        private string commentField;
        private string bank1Field;
        private string companyField;
        private string accountNumberField;
        private string termsField;
        private string dateOpenedField;
        private string branchNameField;
        private string report_TradeRefIDField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string DateRequested
        {
            get
            {
                return this.dateRequestedField;
            }
            set
            {
                this.dateRequestedField = value;
            }
        }

        /// <remarks/>
        public string BankCode
        {
            get
            {
                return this.bankCodeField;
            }
            set
            {
                this.bankCodeField = value;
            }
        }

        /// <remarks/>
        public string Bank
        {
            get
            {
                return this.bankField;
            }
            set
            {
                this.bankField = value;
            }
        }

        /// <remarks/>
        public string EnquiryAmount
        {
            get
            {
                return this.equiryAmountField;
            }
            set
            {
                this.equiryAmountField = value;
            }
        }

        /// <remarks/>
        public string CurrencyType
        {
            get
            {
                return this.currencyTypeField;
            }
            set
            {
                this.currencyTypeField = value;
            }
        }

        /// <remarks/>
        public string Bank_Comments
        {
            get
            {
                return this.bank_CommentsField;
            }
            set
            {
                this.bank_CommentsField = value;
            }
        }

        /// <remarks/>
        public string Years_with_Bank
        {
            get
            {
                return this.years_with_BankField;
            }
            set
            {
                this.years_with_BankField = value;
            }
        }

        /// <remarks/>
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        public string Bank1
        {
            get
            {
                return this.bank1Field;
            }
            set
            {
                this.bank1Field = value;
            }
        }

        /// <remarks/>
        public string Company
        {
            get
            {
                return this.companyField;
            }
            set
            {
                this.companyField = value;
            }
        }

        /// <remarks/>
        public string AccountNumber
        {
            get
            {
                return this.accountNumberField;
            }
            set
            {
                this.accountNumberField = value;
            }
        }

        /// <remarks/>
        public string terms
        {
            get
            {
                return this.termsField;
            }
            set
            {
                this.termsField = value;
            }
        }

        /// <remarks/>
        public string DateOpened
        {
            get
            {
                return this.dateOpenedField;
            }
            set
            {
                this.dateOpenedField = value;
            }
        }

        /// <remarks/>

        public string BranchName
        {
            get
            {
                return this.branchNameField;
            }
            set
            {
                this.branchNameField = value;
            }
        }

        /// <remarks/>
        public string Report_TradeRefID
        {
            get
            {
                return this.report_TradeRefIDField;
            }
            set
            {
                this.report_TradeRefIDField = value;
            }
        }

        /// <remarks/>
        
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class HomeAffairsIDPhotoDetails
    {

        private string iDPhotoField;

        /// <remarks/>
        public string IDPhoto
        {
            get
            {
                return this.iDPhotoField;
            }
            set
            {
                this.iDPhotoField = value;
            }
        }
    }



    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerVirginMobileCustomizedScoring
    {

        private string displayTextField;

        private decimal dMScoreField;

        private string vMExclusionReasonField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public decimal DMScore
        {
            get
            {
                return this.dMScoreField;
            }
            set
            {
                this.dMScoreField = value;
            }
        }

        /// <remarks/>
        public string VMExclusionReason
        {
            get
            {
                return this.vMExclusionReasonField;
            }
            set
            {
                this.vMExclusionReasonField = value;
            }
        }
    }


    /// <remarks/>


    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerAuthenticationHistory
    {

        private System.DateTime bureauDateField;

        private string subscriberNameField;

        private string cellularnumberField;

        private string authenticationStatusIndField;

        private string authenticatedPercField;

        private string emailAddressField;

        /// <remarks/>
        public System.DateTime BureauDate
        {
            get
            {
                return this.bureauDateField;
            }
            set
            {
                this.bureauDateField = value;
            }
        }

        /// <remarks/>
        public string SubscriberName
        {
            get
            {
                return this.subscriberNameField;
            }
            set
            {
                this.subscriberNameField = value;
            }
        }

        /// <remarks/>
        public string Cellularnumber
        {
            get
            {
                return this.cellularnumberField;
            }
            set
            {
                this.cellularnumberField = value;
            }
        }

        /// <remarks/>
        public string AuthenticationStatusInd
        {
            get
            {
                return this.authenticationStatusIndField;
            }
            set
            {
                this.authenticationStatusIndField = value;
            }
        }

        /// <remarks/>
        public string AuthenticatedPerc
        {
            get
            {
                return this.authenticatedPercField;
            }
            set
            {
                this.authenticatedPercField = value;
            }
        }

        /// <remarks/>
        public string EmailAddress
        {
            get
            {
                return this.emailAddressField;
            }
            set
            {
                this.emailAddressField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerSAFPSStatus
    {

        private string iDNumberField;

        private string passportNoField;

        private string statusField;

        /// <remarks/>
        public string IDNumber
        {
            get
            {
                return this.iDNumberField;
            }
            set
            {
                this.iDNumberField = value;
            }
        }

        public string PassportNo
        {
            get
            {
                return this.passportNoField;
            }
            set
            {
                this.passportNoField = value;
            }
        }

        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CommercialCommercialJudgment
    {

        private string displayTextField;

        private string caseNumberField;

        private string disputeAmtField;

        private string caseReasonField;

        private string caseTypeField;

        //private System.DateTime caseFilingDateField;
        private string caseFilingDateField;

        private string plaintiffNameField;

        private string courtNameField;

        private string attorneyNameField;

        private string telephoneNoField;

        private string commercialNameField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string CaseNumber
        {
            get
            {
                return this.caseNumberField;
            }
            set
            {
                this.caseNumberField = value;
            }
        }

        /// <remarks/>
        public string DisputeAmt
        {
            get
            {
                return this.disputeAmtField;
            }
            set
            {
                this.disputeAmtField = value;
            }
        }

        /// <remarks/>
        public string CaseReason
        {
            get
            {
                return this.caseReasonField;
            }
            set
            {
                this.caseReasonField = value;
            }
        }

        /// <remarks/>
        public string CaseType
        {
            get
            {
                return this.caseTypeField;
            }
            set
            {
                this.caseTypeField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string CaseFilingDate
        {
            get
            {
                return this.caseFilingDateField;
            }
            set
            {
                this.caseFilingDateField = value;
            }
        }

        /// <remarks/>
        public string PlaintiffName
        {
            get
            {
                return this.plaintiffNameField;
            }
            set
            {
                this.plaintiffNameField = value;
            }
        }

        /// <remarks/>
        public string CourtName
        {
            get
            {
                return this.courtNameField;
            }
            set
            {
                this.courtNameField = value;
            }
        }

        /// <remarks/>
        public string AttorneyName
        {
            get
            {
                return this.attorneyNameField;
            }
            set
            {
                this.attorneyNameField = value;
            }
        }

        /// <remarks/>
        public string TelephoneNo
        {
            get
            {
                return this.telephoneNoField;
            }
            set
            {
                this.telephoneNoField = value;
            }
        }

        /// <remarks/>
        public string CommercialName
        {
            get
            {
                return this.commercialNameField;
            }
            set
            {
                this.commercialNameField = value;
            }
        }
    }


    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerConsumerConfirmedLinkage
    {

        private string linkageCategoryField;

        private string consumerRecordDateField;

        private string consumerRecordTypeField;

        private string otherRecordDateField;

        private string otherRecordTypeField;

        private string matchedDataField;

        /// <remarks/>
        public string LinkageCategory
        {
            get
            {
                return this.linkageCategoryField;
            }
            set
            {
                this.linkageCategoryField = value;
            }
        }

        /// <remarks/>
        public string ConsumerRecordDate
        {
            get
            {
                return this.consumerRecordDateField;
            }
            set
            {
                this.consumerRecordDateField = value;
            }
        }

        /// <remarks/>
        public string ConsumerRecordType
        {
            get
            {
                return this.consumerRecordTypeField;
            }
            set
            {
                this.consumerRecordTypeField = value;
            }
        }

        /// <remarks/>
        public string OtherRecordDate
        {
            get
            {
                return this.otherRecordDateField;
            }
            set
            {
                this.otherRecordDateField = value;
            }
        }

        /// <remarks/>
        public string OtherRecordType
        {
            get
            {
                return this.otherRecordTypeField;
            }
            set
            {
                this.otherRecordTypeField = value;
            }
        }

        /// <remarks/>
        public string MatchedData
        {
            get
            {
                return this.matchedDataField;
            }
            set
            {
                this.matchedDataField = value;
            }
        }
    }


    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CommercialDirectorPropertyInterests
    {

        private string displayTextField;

        private string directorIDField;

        private string authorityNameField;

        private string townshipNameField;

        private string standNoField;

        private string portionNoField;

        private string titleDeedNoField;

        private string buyerNameField;

        private string buyerTypeCodeField;

        private string buyerIDNoField;

        private string buyerMaritalStatusCodeField;

        private string sellerNameField;

        private string sellerTypeCodeField;

        private string sellerIDNoField;

        private string sellerMaritalStatusCodeField;

        private string transferDateField;

        private string registrarNameField;

        private string oldTitleDeedNoField;

        private string attorneyFirmNoField;

        private string attorneyFileNoField;

        private string titleDeedFeeAmtField;

        private string propertyTypeCodeField;

        private string streetNoField;

        private string streetNameField;

        private string suburbNameField;

        private string cityNameField;

        private string transferIDField;

        private string erfNoField;

        private string deedsOfficeField;

        private string physicalAddressField;

        private string propertyTypeDescField;

        private string erfSizeField;

        private string purchaseDateField;

        private string purchasePriceAmtField;

        private string buyerSharePercField;

        private string bondHolderNameField;

        private string bondAccountNoField;

        private string bondAmtField;

        private string farmNameField;

        private string schemeNameField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string DirectorID
        {
            get
            {
                return this.directorIDField;
            }
            set
            {
                this.directorIDField = value;
            }
        }

        /// <remarks/>
        public string AuthorityName
        {
            get
            {
                return this.authorityNameField;
            }
            set
            {
                this.authorityNameField = value;
            }
        }

        /// <remarks/>
        public string TownshipName
        {
            get
            {
                return this.townshipNameField;
            }
            set
            {
                this.townshipNameField = value;
            }
        }

        /// <remarks/>
        public string StandNo
        {
            get
            {
                return this.standNoField;
            }
            set
            {
                this.standNoField = value;
            }
        }

        /// <remarks/>
        public string PortionNo
        {
            get
            {
                return this.portionNoField;
            }
            set
            {
                this.portionNoField = value;
            }
        }

        /// <remarks/>
        public string TitleDeedNo
        {
            get
            {
                return this.titleDeedNoField;
            }
            set
            {
                this.titleDeedNoField = value;
            }
        }

        /// <remarks/>
        public string BuyerName
        {
            get
            {
                return this.buyerNameField;
            }
            set
            {
                this.buyerNameField = value;
            }
        }

        /// <remarks/>
        public string BuyerTypeCode
        {
            get
            {
                return this.buyerTypeCodeField;
            }
            set
            {
                this.buyerTypeCodeField = value;
            }
        }

        /// <remarks/>
        public string BuyerIDNo
        {
            get
            {
                return this.buyerIDNoField;
            }
            set
            {
                this.buyerIDNoField = value;
            }
        }

        /// <remarks/>
        public string BuyerMaritalStatusCode
        {
            get
            {
                return this.buyerMaritalStatusCodeField;
            }
            set
            {
                this.buyerMaritalStatusCodeField = value;
            }
        }

        /// <remarks/>
        public string SellerName
        {
            get
            {
                return this.sellerNameField;
            }
            set
            {
                this.sellerNameField = value;
            }
        }

        /// <remarks/>
        public string SellerTypeCode
        {
            get
            {
                return this.sellerTypeCodeField;
            }
            set
            {
                this.sellerTypeCodeField = value;
            }
        }

        /// <remarks/>
        public string SellerIDNo
        {
            get
            {
                return this.sellerIDNoField;
            }
            set
            {
                this.sellerIDNoField = value;
            }
        }

        /// <remarks/>
        public string SellerMaritalStatusCode
        {
            get
            {
                return this.sellerMaritalStatusCodeField;
            }
            set
            {
                this.sellerMaritalStatusCodeField = value;
            }
        }

        /// <remarks/>
      
        public string TransferDate
        {
            get
            {
                return this.transferDateField;
            }
            set
            {
                this.transferDateField = value;
            }
        }

        /// <remarks/>
        public string RegistrarName
        {
            get
            {
                return this.registrarNameField;
            }
            set
            {
                this.registrarNameField = value;
            }
        }

        /// <remarks/>
        public string OldTitleDeedNo
        {
            get
            {
                return this.oldTitleDeedNoField;
            }
            set
            {
                this.oldTitleDeedNoField = value;
            }
        }

        /// <remarks/>
        public string AttorneyFirmNo
        {
            get
            {
                return this.attorneyFirmNoField;
            }
            set
            {
                this.attorneyFirmNoField = value;
            }
        }

        /// <remarks/>
        public string AttorneyFileNo
        {
            get
            {
                return this.attorneyFileNoField;
            }
            set
            {
                this.attorneyFileNoField = value;
            }
        }

        /// <remarks/>
        public string TitleDeedFeeAmt
        {
            get
            {
                return this.titleDeedFeeAmtField;
            }
            set
            {
                this.titleDeedFeeAmtField = value;
            }
        }

        /// <remarks/>
        public string PropertyTypeCode
        {
            get
            {
                return this.propertyTypeCodeField;
            }
            set
            {
                this.propertyTypeCodeField = value;
            }
        }

        /// <remarks/>
        public string StreetNo
        {
            get
            {
                return this.streetNoField;
            }
            set
            {
                this.streetNoField = value;
            }
        }

        /// <remarks/>
        public string StreetName
        {
            get
            {
                return this.streetNameField;
            }
            set
            {
                this.streetNameField = value;
            }
        }

        /// <remarks/>
        public string SuburbName
        {
            get
            {
                return this.suburbNameField;
            }
            set
            {
                this.suburbNameField = value;
            }
        }

        /// <remarks/>
        public string CityName
        {
            get
            {
                return this.cityNameField;
            }
            set
            {
                this.cityNameField = value;
            }
        }

        /// <remarks/>
        public string TransferID
        {
            get
            {
                return this.transferIDField;
            }
            set
            {
                this.transferIDField = value;
            }
        }

        /// <remarks/>
        public string ErfNo
        {
            get
            {
                return this.erfNoField;
            }
            set
            {
                this.erfNoField = value;
            }
        }

        /// <remarks/>
        public string DeedsOffice
        {
            get
            {
                return this.deedsOfficeField;
            }
            set
            {
                this.deedsOfficeField = value;
            }
        }

        /// <remarks/>
        public string PhysicalAddress
        {
            get
            {
                return this.physicalAddressField;
            }
            set
            {
                this.physicalAddressField = value;
            }
        }

        /// <remarks/>
        public string PropertyTypeDesc
        {
            get
            {
                return this.propertyTypeDescField;
            }
            set
            {
                this.propertyTypeDescField = value;
            }
        }

        /// <remarks/>
        public string ErfSize
        {
            get
            {
                return this.erfSizeField;
            }
            set
            {
                this.erfSizeField = value;
            }
        }

        /// <remarks/>
       
        public string PurchaseDate
        {
            get
            {
                return this.purchaseDateField;
            }
            set
            {
                this.purchaseDateField = value;
            }
        }

        /// <remarks/>
        public string PurchasePriceAmt
        {
            get
            {
                return this.purchasePriceAmtField;
            }
            set
            {
                this.purchasePriceAmtField = value;
            }
        }

        /// <remarks/>
        public string BuyerSharePerc
        {
            get
            {
                return this.buyerSharePercField;
            }
            set
            {
                this.buyerSharePercField = value;
            }
        }

        /// <remarks/>
        public string BondHolderName
        {
            get
            {
                return this.bondHolderNameField;
            }
            set
            {
                this.bondHolderNameField = value;
            }
        }

        /// <remarks/>
        public string BondAccountNo
        {
            get
            {
                return this.bondAccountNoField;
            }
            set
            {
                this.bondAccountNoField = value;
            }
        }

        /// <remarks/>
        public string BondAmt
        {
            get
            {
                return this.bondAmtField;
            }
            set
            {
                this.bondAmtField = value;
            }
        }

        /// <remarks/>
        public string FarmName
        {
            get
            {
                return this.farmNameField;
            }
            set
            {
                this.farmNameField = value;
            }
        }

        /// <remarks/>
        public string SchemeName
        {
            get
            {
                return this.schemeNameField;
            }
            set
            {
                this.schemeNameField = value;
            }
        }
    }



    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CommercialDirectorCurrentBusinessinterests
    {

        private string displayTextField;

        private string directorIDField;

        private string commercialNameField;

        private string registrationNoField;

        private string commercialStatusField;

        private string judgmentsCountField;

        private string defaultCountField;

        private string liquidationField;

        private string ageOfBusinessField;

        private string judgmentStatusField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string DirectorID
        {
            get
            {
                return this.directorIDField;
            }
            set
            {
                this.directorIDField = value;
        }
    }

        /// <remarks/>
        public string CommercialName
        {
            get
            {
                return this.commercialNameField;
            }
            set
            {
                this.commercialNameField = value;
            }
        }

        /// <remarks/>
        public string RegistrationNo
        {
            get
            {
                return this.registrationNoField;
            }
            set
            {
                this.registrationNoField = value;
            }
        }

        /// <remarks/>
        public string CommercialStatus
        {
            get
            {
                return this.commercialStatusField;
            }
            set
            {
                this.commercialStatusField = value;
            }
        }

        /// <remarks/>
        public string JudgmentsCount
        {
            get
            {
                return this.judgmentsCountField;
            }
            set
            {
                this.judgmentsCountField = value;
            }
        }

        /// <remarks/>
        public string DefaultCount
        {
            get
            {
                return this.defaultCountField;
            }
            set
            {
                this.defaultCountField = value;
            }
        }

        /// <remarks/>
        public string Liquidation
        {
            get
            {
                return this.liquidationField;
            }
            set
            {
                this.liquidationField = value;
            }
        }

        /// <remarks/>
        public string AgeOfBusiness
        {
            get
            {
                return this.ageOfBusinessField;
            }
            set
            {
                this.ageOfBusinessField = value;
            }
        }

        /// <remarks/>
        public string JudgmentStatus
        {
            get
            {
                return this.judgmentStatusField;
            }
            set
            {
                this.judgmentStatusField = value;
            }
        }
    }


    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CommercialDirectorPreviousBusinessInterests
    {

        private string displayTextField;

        private string directorIDField;

        private string commercialNameField;

        private string registrationNoField;

        private string commercialStatusField;

        private string judgmentsCountField;

        private string defaultCountField;

        private string liquidationField;

        private string ageOfBusinessField;

        private string judgmentStatusField;

        /// <remarks/>
        public string DisplayText
        {
            get
            {
                return this.displayTextField;
            }
            set
            {
                this.displayTextField = value;
            }
        }

        /// <remarks/>
        public string DirectorID
        {
            get
            {
                return this.directorIDField;
            }
            set
            {
                this.directorIDField = value;
            }
        }

        /// <remarks/>
        public string CommercialName
        {
            get
            {
                return this.commercialNameField;
            }
            set
            {
                this.commercialNameField = value;
            }
        }

        /// <remarks/>
        public string RegistrationNo
        {
            get
            {
                return this.registrationNoField;
            }
            set
            {
                this.registrationNoField = value;
            }
        }

        /// <remarks/>
        public string CommercialStatus
        {
            get
            {
                return this.commercialStatusField;
            }
            set
            {
                this.commercialStatusField = value;
            }
        }

        /// <remarks/>
        public string JudgmentsCount
        {
            get
            {
                return this.judgmentsCountField;
            }
            set
            {
                this.judgmentsCountField = value;
            }
        }

        /// <remarks/>
        public string DefaultCount
        {
            get
            {
                return this.defaultCountField;
            }
            set
            {
                this.defaultCountField = value;
            }
        }

        /// <remarks/>
        public string Liquidation
        {
            get
            {
                return this.liquidationField;
            }
            set
            {
                this.liquidationField = value;
            }
        }

        /// <remarks/>
        public string AgeOfBusiness
        {
            get
            {
                return this.ageOfBusinessField;
            }
            set
            {
                this.ageOfBusinessField = value;
            }
        }

        /// <remarks/>
        public string JudgmentStatus
        {
            get
            {
                return this.judgmentStatusField;
            }
            set
            {
                this.judgmentStatusField = value;
            }
        }
    }



    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerVehicleInformation
    {

        private string vINField;

        private string engineNoField;

        private string registrationNoField;

        private string manufacturerField;

        private string modelField;

        private string yearField;

        private string colourField;

        /// <remarks/>
        public string VIN
        {
            get
            {
                return this.vINField;
            }
            set
            {
                this.vINField = value;
            }
        }

        /// <remarks/>
        public string EngineNo
        {
            get
            {
                return this.engineNoField;
            }
            set
            {
                this.engineNoField = value;
            }
        }

        /// <remarks/>
        public string RegistrationNo
        {
            get
            {
                return this.registrationNoField;
            }
            set
            {
                this.registrationNoField = value;
            }
        }

        /// <remarks/>
        public string Manufacturer
        {
            get
            {
                return this.manufacturerField;
            }
            set
            {
                this.manufacturerField = value;
            }
        }

        /// <remarks/>
        public string Model
        {
            get
            {
                return this.modelField;
            }
            set
            {
                this.modelField = value;
            }
        }

        /// <remarks/>
        public string Year
        {
            get
            {
                return this.yearField;
            }
            set
            {
                this.yearField = value;
            }
        }

        /// <remarks/>
        public string Colour
        {
            get
            {
                return this.colourField;
            }
            set
            {
                this.colourField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerSAPInformation
    {

        private string resultFoundField;

        private string resultCodeField;

        private string enquiryDateField;

        private string vINField;

        private string chassisNoField;

        private string engineNoField;

        private string caseNoField;

        private string circulationNoField;

        private string policeStationField;

        /// <remarks/>
        public string ResultFound
        {
            get
            {
                return this.resultFoundField;
            }
            set
            {
                this.resultFoundField = value;
            }
        }

        /// <remarks/>
        public string ResultCode
        {
            get
            {
                return this.resultCodeField;
            }
            set
            {
                this.resultCodeField = value;
            }
        }

        /// <remarks/>
        public string EnquiryDate
        {
            get
            {
                return this.enquiryDateField;
            }
            set
            {
                this.enquiryDateField = value;
            }
        }

        /// <remarks/>
        public string VIN
        {
            get
            {
                return this.vINField;
            }
            set
            {
                this.vINField = value;
            }
        }

        /// <remarks/>
        public string ChassisNo
        {
            get
            {
                return this.chassisNoField;
            }
            set
            {
                this.chassisNoField = value;
            }
        }

        /// <remarks/>
        public string EngineNo
        {
            get
            {
                return this.engineNoField;
            }
            set
            {
                this.engineNoField = value;
            }
        }

        /// <remarks/>
        public string CaseNo
        {
            get
            {
                return this.caseNoField;
            }
            set
            {
                this.caseNoField = value;
            }
        }

        /// <remarks/>
        public string CirculationNo
        {
            get
            {
                return this.circulationNoField;
            }
            set
            {
                this.circulationNoField = value;
            }
        }

        /// <remarks/>
        public string PoliceStation
        {
            get
            {
                return this.policeStationField;
            }
            set
            {
                this.policeStationField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerTradeInformation
    {

        private ConsumerTradeInformationManufacturerSpecification manufacturerSpecificationField;

        private ConsumerTradeInformationMarketPrices marketPricesField;

        /// <remarks/>
        public ConsumerTradeInformationManufacturerSpecification ManufacturerSpecification
        {
            get
            {
                return this.manufacturerSpecificationField;
            }
            set
            {
                this.manufacturerSpecificationField = value;
            }
        }

        /// <remarks/>
        public ConsumerTradeInformationMarketPrices MarketPrices
        {
            get
            {
                return this.marketPricesField;
            }
            set
            {
                this.marketPricesField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerTradeInformationManufacturerSpecification
    {

        private string bodyStyleField;

        private string transmissionTypeField;

        private string engineCapacityField;

        private string fuelTypeField;

        private string powerOutputField;

        private string torqueField;

        private string acceleration0To100Field;

        private string topSpeedField;

        private string powerSteeringField;

        private string airConField;

        private string centralLockingField;

        private string leatherDetailField;

        /// <remarks/>
        public string BodyStyle
        {
            get
            {
                return this.bodyStyleField;
            }
            set
            {
                this.bodyStyleField = value;
            }
        }

        /// <remarks/>
        public string TransmissionType
        {
            get
            {
                return this.transmissionTypeField;
            }
            set
            {
                this.transmissionTypeField = value;
            }
        }

        /// <remarks/>
        public string EngineCapacity
        {
            get
            {
                return this.engineCapacityField;
            }
            set
            {
                this.engineCapacityField = value;
            }
        }

        /// <remarks/>
        public string FuelType
        {
            get
            {
                return this.fuelTypeField;
            }
            set
            {
                this.fuelTypeField = value;
            }
        }

        /// <remarks/>
        public string PowerOutput
        {
            get
            {
                return this.powerOutputField;
            }
            set
            {
                this.powerOutputField = value;
            }
        }

        /// <remarks/>
        public string Torque
        {
            get
            {
                return this.torqueField;
            }
            set
            {
                this.torqueField = value;
            }
        }

        /// <remarks/>
        public string Acceleration0To100
        {
            get
            {
                return this.acceleration0To100Field;
            }
            set
            {
                this.acceleration0To100Field = value;
            }
        }

        /// <remarks/>
        public string TopSpeed
        {
            get
            {
                return this.topSpeedField;
            }
            set
            {
                this.topSpeedField = value;
            }
        }

        /// <remarks/>
        public string PowerSteering
        {
            get
            {
                return this.powerSteeringField;
            }
            set
            {
                this.powerSteeringField = value;
            }
        }

        /// <remarks/>
        public string AirCon
        {
            get
            {
                return this.airConField;
            }
            set
            {
                this.airConField = value;
            }
        }

        /// <remarks/>
        public string CentralLocking
        {
            get
            {
                return this.centralLockingField;
            }
            set
            {
                this.centralLockingField = value;
            }
        }

        /// <remarks/>
        public string LeatherDetail
        {
            get
            {
                return this.leatherDetailField;
            }
            set
            {
                this.leatherDetailField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerTradeInformationMarketPrices
    {

        private string priceNewField;

        private string priceTradeField;

        private string priceRetailField;

        private string priceMarketField;

        private string priceHighField;

        private string priceLowField;

        private ConsumerTradeInformationMarketPricesMarketStat[] marketStatsField;

        /// <remarks/>
        public string PriceNew
        {
            get
            {
                return this.priceNewField;
            }
            set
            {
                this.priceNewField = value;
            }
        }

        /// <remarks/>
        public string PriceTrade
        {
            get
            {
                return this.priceTradeField;
            }
            set
            {
                this.priceTradeField = value;
            }
        }

        /// <remarks/>
        public string PriceRetail
        {
            get
            {
                return this.priceRetailField;
            }
            set
            {
                this.priceRetailField = value;
            }
        }

        /// <remarks/>
        public string PriceMarket
        {
            get
            {
                return this.priceMarketField;
            }
            set
            {
                this.priceMarketField = value;
            }
        }

        /// <remarks/>
        public string PriceHigh
        {
            get
            {
                return this.priceHighField;
            }
            set
            {
                this.priceHighField = value;
            }
        }

        /// <remarks/>
        public string PriceLow
        {
            get
            {
                return this.priceLowField;
            }
            set
            {
                this.priceLowField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("MarketStat", IsNullable = false)]
        public ConsumerTradeInformationMarketPricesMarketStat[] MarketStats
        {
            get
            {
                return this.marketStatsField;
            }
            set
            {
                this.marketStatsField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConsumerTradeInformationMarketPricesMarketStat
    {

        private string statsPeriodField;

        private string priceMarketField;

        private string vehicleYearField;

        /// <remarks/>
        public string StatsPeriod
        {
            get
            {
                return this.statsPeriodField;
            }
            set
            {
                this.statsPeriodField = value;
            }
        }

        /// <remarks/>
        public string PriceMarket
        {
            get
            {
                return this.priceMarketField;
            }
            set
            {
                this.priceMarketField = value;
            }
        }

        /// <remarks/>
        public string VehicleYear
        {
            get
            {
                return this.vehicleYearField;
            }
            set
            {
                this.vehicleYearField = value;
            }
        }
    }

}