﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Text.RegularExpressions;

namespace XDS.ONLINE.MODELS
{
    public class General
    {

        public static string DateFormatChange(string dateValue)
        {
            string returnedValue = string.Empty;

            if (!string.IsNullOrEmpty(dateValue))
            {
                try
                {
                    DateTime dt = DateTime.ParseExact(dateValue, "yyyy/MM/dd", CultureInfo.InvariantCulture);
                    returnedValue = dt.ToString(@"MM\/dd\/yyyy");
                }
                catch (Exception e)
                {
                    throw e;
                }

            }

            return returnedValue;
        }

        public static string DisplayDate(string dateValue)
        {
            string returnedValue = string.Empty;
          

            if (!string.IsNullOrEmpty(dateValue))
            {
                try
                {




//                  dateValue=  dateValue.Replace("-", "/"); 
       
//                    string[] formats = {"M/d/yyyy h:mm:ss tt", "M/d/yyyy h:mm tt", "MM/dd/yyyy hh:mm:ss", "M/d/yyyy h:mm:ss", "M/d/yyyy hh:mm tt", "M/d/yyyy hh tt",                        "M/d/yyyy h:mm", "M/d/yyyy h:mm", "MM/dd/yyyy hh:mm", "M/dd/yyyy hh:mm","d", "D", "f", "F", "g", "G", "m", "M", "o", "O", "s", "t", "T", "u", "U", "y", "Y","MM/dd/yyyy", "dddd, dd MMMM yyyy", "dddd, dd MMMM yyyy HH:mm", "dddd, dd MMMM yyyy hh:mm tt", "dddd, dd MMMM yyyy H:mm", "dddd, dd MMMM yyyy h:mm tt", "dddd, dd MMMM yyyy HH:mm:ss", "MM/dd/yyyy HH:mm", "MM/dd/yyyy hh:mm tt", "MM/dd/yyyy H:mm", "MM/dd/yyyy h:mm tt", "MM/dd/yyyy HH:mm:ss", "MMMM dd", "yyyy'-'MM'-'dd'T'HH':'mm':'ss.fffffffK", "ddd, dd MMM yyyy HH':'mm':'ss 'GMT'", "yyyy'-'MM'-'dd'T'HH':'mm':'ss", "HH:mm", "hh:mm tt", "H:mm", "h:mm tt", "HH:mm:ss", "yyyy'-'MM'-'dd HH':'mm':'ss'Z'", "dddd, dd MMMM yyyy HH:mm:ss", "yyyy MMMM", "yyyy-MM-dd", "M/dd/yyyy hh:mm:ss tt" ,"M/dd/yyyy hh:mm:ss tt" ,"MM/dd/yyyy hh:mm:ss tt","MM/dd/YYYY hh:mm:ss","M/dd/yyyy hh:mm:ss tt","MM/dd/yyyy hh:mm:ss tt","MM/dd/YYYY hh:mm:ss","dd/MM/yyyy", "MM/dd/YYYY", "yyyy/MM/dd","MM/dd/yyyy hh:mm:ss tt", "yyyy-MM-dd hh:mm:ss" ,
//@"MM/dd/yyyy", @"MM-dd-yyyy", @"MM.dd.yyyy", @"MM\dd\yyyy",
//@"M/dd/yyyy", @"M-dd-yyyy", @"M.d.yyyy", @"M\d\yyyy",
//@"MM/dd/yy", @"MM-dd-yy", @"MM.dd.yy", @"MM\dd\yy",
//@"M/dd/yy", @"M-dd-yy", @"M.dd.yy", @"M\dd\yy",
//@"MM/yyyy", @"MM-yyyy", @"MM.yyyy", @"MM\yyyy",
//@"MM/yy", @"MM-yy", @"MM.yy", @"MM\yy",
//@"M/yy", @"M-yy", @"M.YY", @"M\YY" };
//                    var dateTime = DateTime.ParseExact(dateValue,   formats, CultureInfo.InvariantCulture, DateTimeStyles.None);
//                    returnedValue = dateTime.ToString(@"dd\/MM\/yyyy");  
                    DateTime FormatedDate = DateTime.Parse(dateValue);
                    returnedValue = FormatedDate.ToString(@"yyyy\/MM\/dd");


                                     
                }
                catch (Exception e)
                {
                    return dateValue;
                  //  throw e;
                }

            }

            return returnedValue;
        }


        public static string DisplayDateWithTime(string dateValue)
        {
            string returnedValue = string.Empty;
           

       
            if (!string.IsNullOrEmpty(dateValue))
            {
                try
                {

                    DateTime FormatedDate = DateTime.Parse(dateValue);

                    if (FormatedDate < new DateTime(1900, 1, 1))
                        return "";
                    else
                        returnedValue = FormatedDate.ToString(@"yyyy\/MM\/dd HH:mm:ss");

                                     
                }
                catch (Exception e)
                {
                    return dateValue;
                  //  throw e;
                }

            }

            return returnedValue;
            }

        public static string DisplayDateAndTime(string dateValue)
        {
            string returnedValue = string.Empty;


            if (!string.IsNullOrEmpty(dateValue))
            {
                try
                {
                    dateValue = dateValue.Replace("-", "/");

                    string[] formats = { "yyyy/MM/ddTHH:mm:ss.fffzzz", "yyyy/MM/ddTHH:mm:ss.fffffffK" };
                    var dateTime = DateTime.ParseExact(dateValue, formats, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
                    returnedValue = dateTime.ToString(@"dd\/MM\/yyyy HH:mm:ss");

                }
                catch (Exception e)
                {
                    return dateValue;
                    //  throw e;
                }
            }
            return returnedValue;
        }



        public static string DoDecimal(string amountValue)
        {
            string result = "0.00";

            if (!string.IsNullOrEmpty(amountValue))
            {
                decimal outValue;
                if (Decimal.TryParse(amountValue, out outValue))
                {
                    result = outValue.ToString("0.00");
                }
            }

            return result;
        }

        public static string DoInt(string Value)
        {
            string result = "0";

            if (!string.IsNullOrEmpty(Value))
            {
                try
                {
                    result = Int32.Parse(Value).ToString(); 
                }
                catch (Exception)
                {
                    result = Value; 
                }
            }
            return result;
        }

        public static string FormatNumbers(string Value)
        {
            var result = Value;
            Value = Value.Replace('R', ' '); // caters for values with the RAND symbol
            Value = RemoveWhitespace(Value);  // prepares numbers for formatting
            NumberFormatInfo nfi = new NumberFormatInfo { NumberGroupSeparator = " ", NumberDecimalDigits = 0 };
            try
            {
                if (!string.IsNullOrEmpty(Value))
                {
                    var dec = decimal.Parse(Value, CultureInfo.InvariantCulture);
                    result = dec != 0 ? Convert.ToInt32(dec).ToString("#,#", nfi) : "0"; 
                }
                else
                {
                    result = "0";
                }
            }
            catch (Exception)
            {
                result = Value; // default to original value. 
            }

            return result;
        }

        public static string FormatNumbersWithDecimal(string Value)
        {
            var result = Value;
            Value = Value.Replace('R', ' '); // caters for values with the RAND symbol
            NumberFormatInfo nfi = new NumberFormatInfo { NumberGroupSeparator = " ", NumberDecimalDigits = 2 };
            try
            {
                if (!string.IsNullOrEmpty(Value))
                {
                    var dec = decimal.Parse(Value, CultureInfo.InvariantCulture);
                    result = Math.Round(Convert.ToDecimal(dec), 2).ToString("#,#.##", nfi);
                    result = (string.IsNullOrEmpty(result)) ? (result = "0.00") : (result);
                    result = (result.StartsWith(".")) ? (result = "0" + result) : (result);
                    result = (!result.Contains(".")) ? (result = result + ".00") : (result);
                }
                else
                {
                    result = "0.00";
                }
            }
            catch (Exception)
            {
                result = Value; // default to original value. 
            }
            return result.Replace(',', '.'); // revert to fullstop decimal point symbol
        }

        public static string TrimDecimalToInt(string Value)
        {
            string result = "0";
            int index = Value.LastIndexOf(".");
            if (index > 0)
                result = Value.Substring(0, index);
            return result;
        }

        public static string TrimDecimalToTwoDecimalPoint(string Value)
        {
            string result = "0";
            int index = Value.LastIndexOf(".");
            if (index != -1)// caters for non-decimal numbers
            {
                if (index > 0 && index + 3 < Value.Length)
                    result = Value.Substring(0, index + 3);
            }
            else
            {
                return Value + ".00"; 
            }
            return result;
        }

        public static string RemoveWhitespace(string input)
        {
            return new string(input.ToCharArray()
                .Where(c => !Char.IsWhiteSpace(c))
                .ToArray());
        }

        public static string ToCaps(string input)
        {
            return input.ToUpper(); 
        }

    }

}
