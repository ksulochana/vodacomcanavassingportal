﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XDS.ONLINE.MODELS;

namespace XDS.ONLINE.MODELS.Helpers
{
  public static class ToSelectListItemsHelper {
   
      public static IEnumerable<SelectListItem> IntegerSelectListItem(int maxCount) {
      List<SelectListItem> oList = new List<SelectListItem>();

      oList.Add(new SelectListItem {
        Value = string.Empty,
        Text = string.Empty
      });
      for (int nCounter = 1; nCounter <= maxCount; nCounter++) {
        oList.Add(new SelectListItem {
          Value = nCounter.ToString(),
          Text = nCounter.ToString()
        });
      }
      return oList;
    }

    public static IEnumerable<SelectListItem> SurveyAnswerOptionsToSelectListItem(List<string> surveyAnswerOptionsList, bool addBlankLine) {
      List<SelectListItem> oList = new List<SelectListItem>();

      oList = surveyAnswerOptionsList.Select(value => new SelectListItem {
        Value = value,
        Text = value
      }).ToList();
      if (addBlankLine) {
        oList.Insert(0, new SelectListItem {
          Value = string.Empty,
          Text = string.Empty
        });
      }

      return oList;
    }
  }
}
