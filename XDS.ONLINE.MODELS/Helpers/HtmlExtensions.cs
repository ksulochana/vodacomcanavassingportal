﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace System.Web.Mvc {
  public static class HtmlExtensions {
    public static MvcHtmlString RadioButtonSelectList(this HtmlHelper htmlHelper, string name, IEnumerable<SelectListItem> listOfValues) {
      var sb = new StringBuilder();

      if (listOfValues != null) {
        // Create a radio button for each item in the list 
        foreach (SelectListItem item in listOfValues) {
          // Generate an id to be given to the radio button field 
          var id = string.Format("{0}_{1}", name, item.Value);

          // Create and populate a radio button using the existing html helpers 
          //var label = htmlHelper.Label(id, HttpUtility.HtmlEncode(item.Text));
          var label = htmlHelper.Label(id, item.Text);
          var radio = htmlHelper.RadioButton(name, item.Value, new { id = id}).ToHtmlString();

          // Create the html string that will be returned to the client 
          // e.g. <input data-val="true" data-val-required="You must select an option" id="TestRadio_1" name="TestRadio" type="radio" value="1" /><label for="TestRadio_1">Line1</label> 
          sb.AppendFormat("<div class=\"RadioButton\" >{0}{1}</div>",label,radio);
        }
      }
      return MvcHtmlString.Create(sb.ToString());
    }

    public static MvcHtmlString RadioButtonForSelectList<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> listOfValues) {
      var metaData = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
      var sb = new StringBuilder();

      if (listOfValues != null) {
        // Create a radio button for each item in the list 
        foreach (SelectListItem item in listOfValues) {
          // Generate an id to be given to the radio button field 
          var id = string.Format("{0}_{1}", metaData.PropertyName, item.Value).Replace(" ", string.Empty).Replace(".", string.Empty); ;

          // Create and populate a radio button using the existing html helpers 
          //var label = htmlHelper.Label(id, HttpUtility.HtmlEncode(item.Text));
          var label = htmlHelper.Label(id, item.Text);
          var radio = htmlHelper.RadioButtonFor(expression, item.Value, new { id = id }).ToHtmlString();

          // Create the html string that will be returned to the client 
          // e.g. <input data-val="true" data-val-required="You must select an option" id="TestRadio_1" name="TestRadio" type="radio" value="1" /><label for="TestRadio_1">Line1</label> 
          sb.AppendFormat("<div class=\"RadioButton\">{0}{1}</div>", label, radio);
        }
      }
      return MvcHtmlString.Create(sb.ToString());
    }

    public static MvcHtmlString RadioButtonSelectList(this HtmlHelper htmlHelper, string name, IEnumerable<SelectListItem> listOfValues, bool bUse2Columns)
    {
      var sb = new StringBuilder();
      bool bUseABlock = true;

      if (listOfValues != null)
      {
        // It looks funky with multiple rows - the rounding doesn't quite fit together but Warren approved it so.....
        sb.Append("<table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" class=\"RadioButton\">");
        // Create a radio button for each item in the list 
        foreach (SelectListItem item in listOfValues)
        {
          if (item.Text != string.Empty)
          {
            // Generate an id to be given to the radio button field 
            var id = string.Format("{0}_{1}", name, item.Value).Replace(" ", string.Empty).Replace(".", string.Empty);

            // Create and populate a radio button using the existing html helpers 
            //var label = htmlHelper.Label(id, HttpUtility.HtmlEncode(item.Text));
            var label = htmlHelper.Label(id, item.Text);
            var radio = htmlHelper.RadioButton(name, item.Value, new { id = id }).ToHtmlString();

            // Create the html string that will be returned to the client 
            // e.g. <input data-val="true" data-val-required="You must select an option" id="TestRadio_1" name="TestRadio" type="radio" value="1" /><label for="TestRadio_1">Line1</label>           
            if (bUseABlock)
            { sb.AppendFormat("<tr><td>{0}{1}</td>", radio, label); }
            else
            { sb.AppendFormat("<td>{0}{1}</td></tr>", radio, label); }
            bUseABlock = !bUseABlock;
          }
        }
        sb.Append("</table>");
        if (!bUseABlock)
        { sb.Append("</td></tr></table>"); }
      }
      return MvcHtmlString.Create(sb.ToString());
    }

    public static MvcHtmlString GetDisplayName<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression) {
      var metaData = ModelMetadata.FromLambdaExpression<TModel, TProperty>(expression, htmlHelper.ViewData);
      string value = metaData.DisplayName ?? (metaData.PropertyName ?? ExpressionHelper.GetExpressionText(expression));

      return MvcHtmlString.Create(value);
    }

    public static MvcHtmlString CustomTextBoxFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression) {
      var attributes = new Dictionary<string, Object>();
      var memberAccessExpression = (MemberExpression)expression.Body;
      var stringLengthAttribs = memberAccessExpression.Member.GetCustomAttributes(
          typeof(System.ComponentModel.DataAnnotations.StringLengthAttribute), true);

      if (stringLengthAttribs.Length > 0) {
        var length = ((StringLengthAttribute)stringLengthAttribs[0]).MaximumLength;

        if (length > 0) {
          attributes.Add("maxlength", length);
        }
      }

      return helper.TextBoxFor(expression, attributes);
    }
  }
}