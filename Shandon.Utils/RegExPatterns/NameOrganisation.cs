﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shandon.Utils.RegExPatterns
{
    public class NameOrganisation
    {
        public const string Pattern = @"^([\u00c0-\u01ffa-zA-Z'\-\(\)\ )\d])+$";
        public const string Message = "Only alphanumeric characters allowed.";
    }
}
