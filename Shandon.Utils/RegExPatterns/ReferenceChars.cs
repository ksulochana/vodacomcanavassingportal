﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shandon.Utils.RegExPatterns
{
    public class ReferenceChars 
    {
        public const string Pattern = @"^[a-zA-Z0-9.\- _@#\*/\$\\()]*$";
        public const string Message = @"Only valid characters allowed.( _,@, #,$,*,/,\)";
    }
}
