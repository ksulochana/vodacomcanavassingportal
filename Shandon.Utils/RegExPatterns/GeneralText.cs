﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shandon.Utils.RegExPatterns
{
    public class GeneralText
    {
        public const string Pattern = @"^([\x00-\x7F\xA0-\xFF]|[•])+$";
        public const string Message = "Only valid characters allowed.";
    }
}
