﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shandon.Utils.RegExPatterns
{
   public class Alphabetic 
    {
        public const string Pattern = @"^[a-zA-Z]+$";
        public const string Message = "Only alphabetic characters allowed.";
    }
}
