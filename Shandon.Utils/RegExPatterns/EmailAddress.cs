﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shandon.Utils.RegExPatterns
{
    public class EmailAddress
    {
        public const string Pattern = @"^([\w+-.%]+@[\w-.]+\.[A-Za-z]{2,4})$";
        public const string Message = "Only valid email addresses allowed.";
    }
}
