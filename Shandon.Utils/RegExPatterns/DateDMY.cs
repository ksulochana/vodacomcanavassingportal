﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shandon.Utils.RegExPatterns
{
    public class DateDMY 
    {
        public const string Pattern = @"^\d{4}\/\d{1,2}\/\d{1,2}$";
        public const string Message = "Only dates in the following format allowed: yyyy/mm/dd.";
    }
}
