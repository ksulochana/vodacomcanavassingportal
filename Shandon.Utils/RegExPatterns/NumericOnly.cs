﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shandon.Utils.RegExPatterns
{
    public class NumericOnly 
    {
        public const string Pattern = @"^\d+$(\.\d+)?";
        public const string Message = "Only numeric characters allowed.";
    }
}
