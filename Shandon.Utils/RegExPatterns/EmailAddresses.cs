﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shandon.Utils.RegExPatterns
{
    public class EmailAddresses
    {
        public const string Pattern = @"^([\w+-.%]+@[\w-.]+\.[A-Za-z]{2,4}\s?;?\s?)+$";
        public const string Message = "Only valid email addresses seperated by a semicolon allowed.";
    }
}
