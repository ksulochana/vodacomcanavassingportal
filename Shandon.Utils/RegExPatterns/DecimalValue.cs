﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shandon.Utils.RegExPatterns
{
    public class DecimalValue
    {
        public const string Pattern = @"[\d]{1,99}([.]\d{1,2})?";
        public const string Message = "Only decimal values allowed.";
  
    }

}
