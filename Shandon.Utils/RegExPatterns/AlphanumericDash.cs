﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shandon.Utils.RegExPatterns
{
    public class AlphanumericDash
    {
        public const string Pattern = @"^[a-zA-Z0-9\-]+$";
        public const string Message = "Only alphanumeric characters allowed.";

    }
}
