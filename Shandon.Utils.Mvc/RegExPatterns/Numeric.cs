﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shandon.Utils.Mvc.RegExPatterns
{
    public class Numeric
    {
        public const string Pattern = @"^\d+$";
        public const string Message = "This value needs to be numeric.";
    }
}
