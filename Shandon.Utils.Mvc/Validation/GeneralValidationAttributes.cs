﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace Shandon.Utils.Mvc.Validation
{
    public class Constants{
        public const string RegExNumeric = @"^\d+$";

    }
    

    public class RegularExpressionBaseAttribute : RegularExpressionAttribute 
    {
        public RegularExpressionBaseAttribute(string Pattern) : base(Pattern) {

            ErrorMessage = "Invalid format.";
        }
    }

        #region General Type Validations

        public class GeneralTextFormatAttribute : RegularExpressionAttribute
        {
            public GeneralTextFormatAttribute() : base(@"^([\x00-\x7F\xA0-\xFF]|[•])+$") { }
        }

        public class AlphabeticFormatAttribute : RegularExpressionAttribute
        {
            public AlphabeticFormatAttribute() : base(@"^[a-zA-Z]+$") { }
        }

        public class AlphanumericFormatAttribute : RegularExpressionAttribute
        {
            public AlphanumericFormatAttribute() : base(@"^[a-zA-Z0-9]+$") { }
        }

        #endregion

        #region Name Validations

        public class PersonNameFormatAttribute : RegularExpressionAttribute
        {
            public PersonNameFormatAttribute() : base(@"^([\u00c0-\u01ffa-zA-Z'\-\ ])+$") { }
        }

        public class OrganisationNameFormatAttribute : RegularExpressionAttribute
        {
            public OrganisationNameFormatAttribute() : base(@"^([\u00c0-\u01ffa-zA-Z0-9'\-\(\)\ )])+$") { }
        }

        public class PlaceNameFormatAttribute : RegularExpressionAttribute
        {
            public PlaceNameFormatAttribute() : base(@"^([\u00c0-\u01ffa-zA-Z0-9'\-])+$") { }
        }

        #endregion

        #region Number Validations


        public class NumericFormatAttribute : RegularExpressionBaseAttribute
        {
            public NumericFormatAttribute() : base(@"^\d+$") { }
        }

        public class DecimalFormatAttribute : RegularExpressionAttribute
        {
            public DecimalFormatAttribute() : base(@"[\d]{1,50}([.]\d{1,2})?") { }
        }
        #endregion

        #region Number Validations


        public class DateDMYFormatAttribute : RegularExpressionAttribute
        {
            public DateDMYFormatAttribute() : base(@"^\d{1,2}\/\d{1,2}\/\d{4}$") { }
        }

        public class DateYMDFormatAttribute : RegularExpressionAttribute
        {
            public DateYMDFormatAttribute() : base(@"^\d{4}\/\d{1,2}\/\d{1,2}$") { }
        }
        #endregion

        #region Custom Field Validation
        public class EmailAddressFormatAttribute : RegularExpressionAttribute
        {
            public EmailAddressFormatAttribute() : base(@"^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$") { }
        }

        public class CompanyRegistrationFormatAttribute : RegularExpressionAttribute
        {
            public CompanyRegistrationFormatAttribute() : base(@"^\d{4}\/\d{6}\/\d{2}$") { }
        }
        #endregion


    //}
}
