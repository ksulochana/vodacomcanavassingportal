﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Shandon.Utils.Mvc.Validation;

namespace Shandon.Utils.Mvc
{
    public class Startup
    {
        public static void RegisterValidationAttributes()
        {
           // necessary to enable client side validation

           // DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(Shandon.Utils.Mvc.Validation.GeneralTextFormatAttribute), typeof(RegularExpressionAttributeAdapter));

            DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(RequiredIfAttribute), typeof(RequiredAttributeAdapter));
           
        }
    }
}
