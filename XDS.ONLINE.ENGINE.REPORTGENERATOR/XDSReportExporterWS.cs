﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

using XDS.ONLINE.ENGINE.REPORTGENERATOR.XDSExportService ;
using Shandon.Logging;

namespace XDS.ONLINE.ENGINE
{
    public class XDSReportExporterWS //: XDS.ONLINE.ENGINE.REPORTGENERATOR.IXDSReportExporter
    {

        public Byte[] GenerateReport(string ReportXML, XDS.ONLINE.ENGINE.REPORTGENERATOR.Types.ReportFormat ReportFormat, int ProductID)
        {
            Byte[] bytes = null;
            ReportWebServiceSoapClient  ws;

            ws = new ReportWebServiceSoapClient();

                bytes = ws.GetReportInBytes ( ProductID, ReportXML, ReportFormat.ToString());
                //string result = await _XDSReportService.ConnectGetResultAsync(ConnectTicket, EnquiryID, EnquiryResultID, ProductID, BonusXML);
                //rpt = (ConsumerTraceReportModel)CreateObject(result, rpt);
  
                ws.Close();
                try
                {
                    ws.Close();
                }
                catch (CommunicationException e)
                {
                    logerror(e);
                    ws.Abort();
                }
                catch (TimeoutException e)
                {
                    logerror(e);
                    ws.Abort();
                }
                catch (Exception e)
                {
                    logerror(e);
                    ws.Abort();
                    throw;
                }
                finally {
                    ws = null; 
                }
   
            return bytes;            
            
        }

        private void logerror(Exception e)
        {
            ExceptionLogger log = new ExceptionLogger();
            log.LogError(e,"XDS.ONLINE.ENGINE.REPORTGENERATOR", "XDSReportExporterWS", "GenerateReport", "");
        }
    }
}
