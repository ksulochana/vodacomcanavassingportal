﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.IO;
using System.Data;
using System.Drawing;

using Shandon.Logging;
using XdsPortalReports;
using DevExpress.XtraReports.UI;

using System.Net.Mail;
using System.Configuration;
using System.Xml;
using XDS.REPORTS;


namespace XDS.ONLINE.ENGINE.REPORTGENERATOR
{
    public class XDSReportExporter : IXDSReportExporter
    {
        public Byte[] GenerateReport(string ReportXML, Types.ReportFormat ReportFormat, int ProductID)
        {
            Byte[] bytes = null;

            try
            {
                string XmlData = ReportXML;
                int intproductid = ProductID;

                XtraReport oXdsReport = new XtraReport();
                MemoryStream oMemoryStream = new MemoryStream();

                oXdsReport = new CustomVetingReport(XmlData);
                
                switch (ReportFormat)
                {
                    case Types.ReportFormat.pdf:
                        oXdsReport.ExportToPdf(oMemoryStream);
                        oMemoryStream.Seek(0, System.IO.SeekOrigin.Begin);
                        break;

                    case Types.ReportFormat.html:
                        oXdsReport.ExportToMht(oMemoryStream);
                        oMemoryStream.Seek(0, System.IO.SeekOrigin.Begin);
                        break;

                    case Types.ReportFormat.image:
                        oXdsReport.ExportToImage(oMemoryStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                        oMemoryStream.Seek(0, System.IO.SeekOrigin.Begin);
                        break;
                }

                bytes = oMemoryStream.ToArray();

                oMemoryStream = null;
            }
            catch (Exception e)
            {
                logerror(e);
            }
            return bytes;
        }

        public bool EmailReport(string FromName, string ReplyToEmailAddress, string ToEmailAddress, string EmailBody, string ReportXML, int ProductID)
        {
            bool success = false;

            try
            {
                MemoryStream oMemoryStream = new MemoryStream(GenerateReport(ReportXML, Types.ReportFormat.pdf, ProductID));

                string ReportName = GetReferenceNum(ReportXML);

                SmtpClient smtp = new SmtpClient(ConfigurationManager.AppSettings["XDSSmtp"]);

                smtp.Host = ConfigurationManager.AppSettings["XDSSmtp"];
                smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["XDSSmtpPort"]);
                smtp.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["XDSSmtpUserLoginID"], ConfigurationManager.AppSettings["XDSSmtpUserPassword"]);
                smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["XDSSmtpEnableSSL"]);

                MailMessage mail = new MailMessage();

                //mail.From = new MailAddress(ReplyToEmailASPxTextBox.Text.Trim(), EmailFromASPxLabel.Text);
                mail.From = new MailAddress(ConfigurationManager.AppSettings["XDSSmtpReportsFromEmail"], ReplyToEmailAddress);

                string[] oEmailAddresses = (ToEmailAddress.Trim()).Split(';');

                foreach (string oEmail in oEmailAddresses)
                {
                    if (!string.IsNullOrEmpty(oEmail.Trim()))
                        mail.To.Add(new MailAddress(oEmail.Replace(";", "").Trim()));
                }
                mail.CC.Add(new MailAddress(ReplyToEmailAddress.Trim()));

                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["XDSSmtpReportsBccEmail"]))
                { mail.Bcc.Add(new MailAddress(ConfigurationManager.AppSettings["XDSSmtpReportsBccEmail"])); }


                mail.Attachments.Add(new Attachment(oMemoryStream, string.Concat(ReportName.Replace('-', '_'), ".pdf"), "application/pdf"));

                // Specify other e-mail options.
                mail.Subject = "XDS Enquiry Report - Ref#: " + ReportName;

                mail.IsBodyHtml = true;
                mail.BodyEncoding = Encoding.UTF8;
                mail.Body = EmailBody;

                //Send the e-mail message via the specified SMTP server.                                
                smtp.Send(mail);

                success = true;
            }
            catch (SmtpFailedRecipientsException ex)
            {
                logerror(ex);
            }
            catch (SmtpFailedRecipientException ex)
            {
                logerror(ex);
            }
            catch (SmtpException ex)
            {
                logerror(ex);
            }
            catch (Exception ex)
            {

                //Exception objErr = Server.GetLastError().GetBaseException();
                logerror(ex);
            }



            return success;
        }

        private string GetReferenceNum(string reportXml)
        {
            // temp function to get reference number
            string referenceNum = "report";
            try
            {
                XmlDocument dom = new XmlDocument();
                dom.LoadXml(reportXml);
                referenceNum = dom.DocumentElement.SelectSingleNode("/*/ConsumerDetail/ReferenceNo").InnerText;
            }
            catch (Exception ex)
            { }

            return referenceNum;
        }

        private void logerror(Exception e)
        {
            ExceptionLogger log = new ExceptionLogger();
            log.LogError(e, "XDS.ONLINE.ENGINE.REPORTGENERATOR", "XDSReportExporter", "GenerateReport", "");
        }
    }
}
