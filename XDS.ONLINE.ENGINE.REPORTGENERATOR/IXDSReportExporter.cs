﻿using System;
namespace XDS.ONLINE.ENGINE.REPORTGENERATOR
{
    public interface IXDSReportExporter
    {
        Byte[] GenerateReport(string ReportXML, XDS.ONLINE.ENGINE.REPORTGENERATOR.Types.ReportFormat ReportFormat, int ProductID);
        bool EmailReport(string FromName, string ReplyToEmailAddress, string ToEmailAddress, string EmailBody, string ReportXML, int ProductID);
    }
}
