﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using XDS.ONLINE.WEB;
using XDS.ONLINE.WEB.Controllers;
using XDS.ONLINE.ENGINE;
using XDS.ONLINE.MODELS;
using XDS.ONLINE.WEB.Models;
using Moq;
using System.Web;
using System.Web.Routing;
using System.Security.Principal;

namespace XDS.ONLINE.WEB.Tests.Controllers
{
    [TestClass]
    public class AccountControllerTest
    {
        protected static void CallSync(Action target)
        {
            var task = new Task(target);
            task.RunSynchronously();
        }

        [TestMethod]
        public void Login()
        {


            // Arrange
            var context = new Mock<HttpContextBase>();
            var session = new Mock<HttpSessionStateBase>();
            var user = new GenericPrincipal(new GenericIdentity("john"), new[] { "Contributor" });
            context.Setup(x => x.User).Returns(user);       
            context.Setup(x => x.Session).Returns(session.Object);
          
            var requestContext = new RequestContext(context.Object, new RouteData());

            XDSServiceStub engine = new XDSServiceStub();
            AccountController controller = new AccountController(engine);

            controller.ControllerContext = new ControllerContext(requestContext, controller);

            LoginViewModel model = new LoginViewModel();
            model.UserName = "";
            model.Password = "";

            // Act
            var result = controller.Login(model, "") as Task<ActionResult>;

            var viewresult = result.Result;

// Assert
            if (result.IsFaulted)
            { Assert.Fail(); }
            
            Assert.IsNotNull(result);
        }


    }
}
